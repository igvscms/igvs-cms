<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses' => [
        'key' => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => 'User',
        'secret' => '',
    ],

    'pilotoauth' => [
        'client_id' => 'id',
        'client_secret' => 'key',
        'redirect' => 'http://igvs/oauth/pilot/new/finish?a=1',
        'pilot_url_root' => 'http://dev.pilot.i-gvs.com',
    ],

    'pilotoauth_expertise' => [
        'client_id' => 'id',
        'client_secret' => 'key',
        'redirect' => 'http://igvs/oauth/pilot/new/finish?expertise=1',
        'pilot_url_root' => 'http://dev.pilot.i-gvs.com',
    ],

    'pilotoauth_inclusive' => [
        'client_id' => 'id',
        'client_secret' => 'key',
        'redirect' => 'http://igvs/oauth/pilot/new/finish?expertise=1',
        'pilot_url_root' => 'http://dev.pilot.i-gvs.com',
    ],


];