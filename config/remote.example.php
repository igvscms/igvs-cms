<?php

return [

    'default' => 'local',

    'connections' => [

        'production' => [
            'host'      => '',
            'username'  => '',
            'password'  => '',
            'key'       => '',
            'keyphrase' => '',
            'root'      => '',
        ],
        'develop' => [
            'host'      => '',
            'username'  => '',
            'password'  => '',
            'key'       => '',
            'keyphrase' => '',
            'root'      => '',
        ],
        'local' => [
            'host'      => '',
            'username'  => '',
            'password'  => '',
            'key'       => '',
            'keyphrase' => '',
            'root'      => '',
        ]
    ],

];