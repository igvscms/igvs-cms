var xhr = new XMLHttpRequest(),
	paramIdModule = null,
	paramSomeParam = null,
	saveBtns = null;

function findGetParameter(parameterName) {
	var result = null,
		tmp = [];

	location.search.substr(1).split("&").forEach(function (item) {
		tmp = item.split("=");
		if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	});

	return result;
}

function onstatechange(btn) {
	return function() {
		var that = this;
		btn.classList.remove('failture');
		btn.classList.add('disabled');
		btn.innerHTML = "processing...";
		if (that.readyState == 4) {
			if(that.status == 200) {
				if(that.responseText == '1') {
					btn.classList.remove('disabled');
					btn.classList.add('success');
					btn.innerHTML = "Added to project";	
				}
				else {
					// console.log(that.responseText);
					btn.classList.remove('disabled');
					btn.classList.add('failture');
					btn.innerHTML = "Add to project";
				}
			}
			else {
				btn.classList.remove('disabled');
				btn.innerHTML = "Add to project";
			}
			xhr.onreadystatechange = null;
		}
	}
}

function saveRequest(params) {
	// console.log('module_id='+params[0]+'&path='+params[1]);
	xhr.open('POST', '/cp/igvs/courses/api/copyfile?' + 'module_id='+params[0]+'&path='+params[1]);
	xhr.onreadystatechange = onstatechange(params[2]);
	xhr.send();
}

function onClickEvent(event) {
	event.preventDefault();
	if (event.target.hasAttribute('data-btn-save') && !event.target.classList.contains('disabled') && !event.target.classList.contains('success')) saveRequest([paramIdModule, event.target.getAttribute('data-btn-save'), event.target]);
}

function main() {
	saveBtns = document.querySelectorAll('[data-btn-save]');

	paramIdModule = findGetParameter('module_id');
	paramSomeParam = true;

	if(paramIdModule && paramSomeParam) {
		document.onclick = onClickEvent;

		for (var i = 0; i < saveBtns.length; i++) {
			saveBtns[i].classList.remove("disabled");
		}
	}

}

main();
