<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class CoursesDirections_1_0_0 extends Migration
{
    public function up()
    {
        \Schema::create('academy_courses_courses_directions', function($table) {
            $table->engine = 'InnoDB';
            $table->integer('course_id')->unsigned();
            $table->integer('direction_id')->unsigned();

            $table->primary(['course_id', 'direction_id'], 'p');

            $table->foreign('course_id', 'f_academyCoursesCoursesDirections_courseId')
                ->references('id')
                ->on('academy_courses')
                ->onDelete('cascade');

            $table->foreign('direction_id', 'f_academyCoursesCoursesDirections_directionId')
                ->references('id')
                ->on('academy_courses_directions')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_courses_courses_directions');
    }
}