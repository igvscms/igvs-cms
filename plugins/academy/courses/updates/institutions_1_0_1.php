<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class Institutions_1_0_1 extends Migration
{
    public function up()
    {
        \Schema::table('academy_courses_institutions', function($table) {
            $table->string('global_id', 36);
        });
    }

    public function down()
    {
        \Schema::table('academy_courses_institutions', function($table) {
            $table->dropColumn('global_id');
        });
    }
}