<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class CoursesCompetences_1_0_0 extends Migration
{
    public function up()
    {
        \Schema::create('academy_courses_courses_competences', function($table) {
            $table->engine = 'InnoDB';
            $table->integer('course_id')->unsigned();
            $table->integer('competence_id')->unsigned();

            $table->primary(['course_id', 'competence_id'], 'p');

            $table->foreign('course_id', 'f_academyCoursesCoursesCompetences_courseId')
                ->references('id')
                ->on('academy_courses')
                ->onDelete('cascade');

            $table->foreign('competence_id', 'f_academyCoursesCoursesCompetences_competenceId')
                ->references('id')
                ->on('academy_courses_competences')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_courses_courses_competences');
    }
}