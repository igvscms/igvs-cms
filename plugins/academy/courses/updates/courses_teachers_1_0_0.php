<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class CoursesTeachers_1_0_0 extends Migration
{
    public function up()
    {
        \Schema::create('academy_courses_courses_teachers', function($table) {
            $table->engine = 'InnoDB';
            $table->integer('course_id')->unsigned();
            $table->integer('teacher_id')->unsigned();

            $table->primary(['course_id', 'teacher_id']);

            $table->foreign('course_id', 'f_academyCoursesCoursesTeachers_courseId')
                ->references('id')
                ->on('academy_courses')
                ->onDelete('cascade');

            $table->foreign('teacher_id', 'f_academyCoursesCoursesTeachers_teacherId')
                ->references('id')
                ->on('academy_courses_teachers')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_courses_courses_teachers');
    }
}