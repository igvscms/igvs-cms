<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class Directions_1_0_0 extends Migration
{
    public function up()
    {
        \Schema::create('academy_courses_directions', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('code', 8);
            $table->string('title');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_courses_directions');
    }
}