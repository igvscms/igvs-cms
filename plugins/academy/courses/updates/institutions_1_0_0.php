<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class Institutions_1_0_0 extends Migration
{
    public function up()
    {
        \Schema::create('academy_courses_institutions', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('url');
            $table->text('description');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_courses_institutions');
    }
}