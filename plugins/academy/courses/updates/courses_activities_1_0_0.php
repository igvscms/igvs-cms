<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class CoursesActivities_1_0_0 extends Migration
{
    public function up()
    {
        \Schema::create('academy_courses_courses_activities', function($table) {
            $table->engine = 'InnoDB';
            $table->integer('course_id')->unsigned();
            $table->integer('activity_id')->unsigned();

            $table->primary(['course_id', 'activity_id'], 'p');

            $table->foreign('course_id', 'f_academyCoursesCoursesActivities_courseId')
                ->references('id')
                ->on('academy_courses')
                ->onDelete('cascade');

            $table->foreign('activity_id', 'f_academyCoursesCoursesActivities_activityId')
                ->references('id')
                ->on('academy_courses_activities')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_courses_courses_activities');
    }
}