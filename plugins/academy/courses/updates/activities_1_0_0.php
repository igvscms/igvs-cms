<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class Activities_1_0_0 extends Migration
{
    public function up()
    {
        \Schema::create('academy_courses_activities', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_courses_activities');
    }
}