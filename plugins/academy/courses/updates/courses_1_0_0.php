<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class Courses_1_0_0 extends Migration
{
    public function up()
    {
        \Schema::create('academy_courses', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('institution_id')->unsigned();
            $table->integer('partner_id')->unsigned();

            $table->string('global_id', 36);
            $table->string('title');
            $table->string('language', 2);
            $table->text('description');

            $table->integer('volume')->unsigned();
            $table->integer('intensity_per_week')->unsigned();
            $table->string('duration');
            $table->integer('lectures_number')->unsigned();
            $table->timestamp('started_at')->nullable();
            $table->timestamp('record_end_at')->nullable();
            $table->timestamp('finished_at')->nullable();

            $table->tinyInteger('rating')->unsigned();
            $table->tinyInteger('experts_rating')->unsigned();
            $table->integer('visitors_number')->unsigned();
            $table->integer('total_visitors_number')->unsigned();

            $table->text('content');
            $table->string('external_url');
            $table->boolean('has_certificate');
            $table->string('accreditation');

            $table->nullableTimestamps();
            $table->softDeletes();

            $table->foreign('institution_id', 'f_academyCourses_institutionId')
                ->references('id')
                ->on('academy_courses_institutions');

            $table->foreign('partner_id', 'f_academyCourses_partnerId')
                ->references('id')
                ->on('academy_courses_partners');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_courses');
    }
}