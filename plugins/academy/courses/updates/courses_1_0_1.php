<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class Courses_1_0_1 extends Migration
{
    public function up()
    {
        \Schema::table('academy_courses', function($table) {
            $table->integer('duration')->unsigned()->change();
            $table->string('duration_code', 10);
        });
    }

    public function down()
    {
        \Schema::table('academy_courses', function($table) {
            $table->string('duration')->change();
            $table->dropColumn('duration_code');
        });
    }
}