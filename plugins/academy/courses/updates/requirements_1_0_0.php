<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class Requirements_1_0_0 extends Migration
{
    public function up()
    {
        \Schema::create('academy_courses_requirements', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->string('title');

            $table->foreign('course_id', 'f_academyCoursesRequirements_courseId')
                ->references('id')
                ->on('academy_courses')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_courses_requirements');
    }
}