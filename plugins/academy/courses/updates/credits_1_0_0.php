<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class Credits_1_0_0 extends Migration
{
    public function up()
    {
        \Schema::create('academy_courses_credits', function($table) {
            $table->engine = 'InnoDB';
            $table->integer('institution_id')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->integer('direction_id')->unsigned();

            $table->primary(['institution_id', 'direction_id', 'course_id'],'p');

            $table->foreign('institution_id', 'f_academyCoursesCredits_institutionId')
                ->references('id')
                ->on('academy_courses_institutions')
                ->onDelete('cascade');

            $table->foreign('direction_id', 'f_academyCoursesCredits_directionId')
                ->references('id')
                ->on('academy_courses_directions')
                ->onDelete('cascade');

            $table->foreign('course_id', 'f_academyCoursesCredits_courseId')
                ->references('id')
                ->on('academy_courses')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_courses_credits');
    }
}