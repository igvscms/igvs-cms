<?php namespace Academy\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class Feedbacks_1_0_0 extends Migration
{
    public function up()
    {
        \Schema::create('academy_courses_feedbacks', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->tinyInteger('raiting')->unsigned();
            $table->string('image');
            $table->text('text');
            $table->boolean('is_expert');
            $table->timestamps();

            $table->foreign('course_id', 'f_academyCoursesFeedbacks_courseId')
                ->references('id')
                ->on('academy_courses')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_courses_feedbacks');
    }
}