<?php namespace Academy\Courses\Controllers;

use Backend\Classes\Controller;

use Symfony\Component\HttpFoundation\AcceptHeader;

use Academy\System\Classes\Curl;

use Academy\Courses\Models\Course;

class Courses extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = [
        'teachers' => 'teachers_relation.yaml',
        'competences' => 'competences_relation.yaml',
        'activities' => 'activities_relation.yaml',
        'directions' => 'directions_relation.yaml',
        'credits' => 'credits_relation.yaml',
        'learning_outcomes' => 'learning_outcomes_relation.yaml',
        'requirements' => 'requirements_relation.yaml',
    ];
    public $requiredPermissions = ['academy.courses.courses'];

    protected static $cookieParts = array(
        'domain'      => 'Domain',
        'path'        => 'Path',
        'max_age'     => 'Max-Age',
        'expires'     => 'Expires',
        'version'     => 'Version',
        'secure'      => 'Secure',
        'port'        => 'Port',
        'discard'     => 'Discard',
        'comment'     => 'Comment',
        'comment_url' => 'Comment-Url',
        'http_only'   => 'HttpOnly'
    );

    private function parseHeader($headers, $rowDel = "\r\n", $valDel = ':')
    {
        $result = [];
        foreach (explode($rowDel, $headers) as $row) {
            if (count($keys = explode($valDel, $row, 2)) == 2) {
                list($key, $value) = $keys;
                $result[trim($key)] = trim($value);
            }
        }
        return $result;
    }

    public function __construct()
    {
        /*$curl = Curl::init('https://test.portfolio.edu.ru/api/v1/connection/check')
            ->sslKey('1117746942906.key')
            ->sslCert('1117746942906.crt')
            ->header();
        $response = $curl->exec();
        unset($curl);
        $response = $this->parseHeader($response);
        if (isset($response['Set-Cookie'])) {
            $response['Set-Cookie'] = $this->parseHeader($response['Set-Cookie'], ';', '=');
            if (isset($response['Set-Cookie']['JSESSIONID'])) {
                \Session::put('JSESSIONID', $response['Set-Cookie']['JSESSIONID']);
            }
        }*/
        //'https://preprod.oeplatform.ru/realms/master/protocol/openid-connect/token'
        //'https://online.edu.ru/realms/master/protocol/openid-connect/token'
        //'https://test.portfolio.edu.ru/realms/master/protocol/openid-connect/token'
        //'https://sso.online.edu.ru/realms/master/protocol/openid-connect/token'
/*
        curl -v -X POST https://preprod.oeplatform.ru/api/courses/v0/course -H "authorization: Basic Z29sb3ZpbjoxMjM=" -H "cache-control: no-cache"  -H "content-type: application/json" -d @test_json.txt


        $domain = 'portfolio';
        $curl = Curl::init("https://sso.online.edu.ru/realms/$domain/protocol/openid-connect/token/")
            ->postFields([
                'grant_type' => 'password',
                'client_id' => 'elearning.academia.moscow',
                'client_secret' => 'f92d7cb9-5d68-4672-941d-0f612352f7b2',
                'username' => 'golovin.ev@academia-moscow.ru',
                'password' => 'hs63hdf',
            ])
            //->header()
            //->referer('https://preprod.oeplatform.ru')
            //->httpHeader(["Cookie: django_language=ru"])
            //->ssl_verifypeer(0)
            //->followLocation()
            ;
        $response = json_decode($curl->exec());
        unset($curl);

        // $response->access_token;
        // $response->expires_in;
        // $response->refresh_expires_in;
        // $response->refresh_token;
        // $response->token_type;
        // $response->id_token;
        // $response->{'not-before-policy'};
        // $response->session_state;
//<!-->*/


        //dd($response);

        // https://online.edu.ru/api/courses/v0/course
        // https://preprod.oeplatform.ru/api/courses/v0/course
        // https://test.portfolio.edu.ru



/*
        https://sso.online.edu.ru/realms/master/protocol/openid-connect/auth?
            response_type=code&
            client_id=security-admin-console&
            redirect_uri=https%3A%2F%2Fsso.online.edu.ru%2Fwebadmin%2Findex.xhtml


        curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d

        '

        
        
        


*/

        //f92d7cb9-5d68-4672-941d-0f612352f7b2
        
        // print_r($response);
        // die();
        parent::__construct();
        \BackendMenu::setContext('Academy.Courses', 'courses', 'courses');
    }

    protected function onPublish()
    {
        $apiUrl = \Config::get('app.edu.api', 'https://preprod.oeplatform.ru');
        $userLogin = \Config::get('app.edu.user.login');
        $userPassw = \Config::get('app.edu.user.password');

        foreach (Course::findMany(post('checked', [])) as $course) {
            $teachers = [];
            foreach ($course->teachers as $teacher) {
                $teachers[] = [
                    'image' => $teacher->image ? $_SERVER['HTTP_ORIGIN'] . $teacher->image->getPath() : '',
                    'display_name' => $teacher->title,
                    'description' => $teacher->description,
                ];
            }
            $directions = $course->directions->lists('code');

            $attributes = [
                "title" => $course->title,
                "finished_at" => $course->finished_at->toDateString(),
                "enrollment_finished_at" => $course->record_end_at->toDateString(),
                "image" => $course->image ? $_SERVER['HTTP_ORIGIN'] . $course->image->getPath() : '',
                "description" => $course->description,
                "competences" => implode('; ', $course->competences->lists('title')),
                "requirements" => implode('; ', $course->requirements->lists('title')),
                "content" => $course->content,
                "external_url" => $course->external_url == '' ? ' ' : $course->external_url,
                "direction" => count($directions) ? $directions : ["00.00.00"],
                "institution" => $course->institution->global_id,
                "duration" => [
                    "value" => $course->duration,
                    "code" => $course->duration_code ? $course->duration_code : 'week'
                ],
                "lectures" => $course->lectures_number,
                "language" => $course->language,
                "cert" => $course->has_certificate ? 'true' : 'false',
                "visitors" => $course->visitors_number,
                "teachers" => $teachers,
                "business_version" => 1
            ];
            if ($course->global_id != '') {
                $attributes['id'] = $course->global_id;
            }
            $response = Curl::url($apiUrl . '/api/courses/v0/course')
                ->httpAuth(CURLAUTH_BASIC)
                ->userPwd("$userLogin:$userPassw")
                ->httpHeader([
                    'cache-control' => 'no-cache',
                    'content-type' => 'application/json',
                ])
                ->postFields(json_encode($q = [
                    "partnerId" => $course->partner->global_id,
                    "package" => [
                        "items" => [
                            $attributes
                        ]
                    ]
                ]))
                ->put($course->global_id != '')
                ->fromJson();

            if (!isset($response->course_id)) {
                if (isset($response->faultstring)) {
                    \Flash::error($response->faultstring);
                    return;
                }
                if ($course->global_id == '') {
                    \Flash::error('Неизвестная ошибка публикации курса');
                    return;
                }
            }
            if ($course->global_id == '') {
                $course->global_id = $response->course_id;
            }
            $course->save();
        }
        \Flash::success('Выбранные курсы успешно опубликованы');
    }
}