<?php namespace Academy\Courses\Controllers;

use Backend\Classes\Controller;

class Institutions extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $requiredPermissions = ['academy.courses.institutions'];

    public function __construct()
    {
        parent::__construct();
        \BackendMenu::setContext('Academy.Courses', 'courses', 'institutions');
    }
}