<?php namespace Academy\Courses\Controllers;

use Backend\Classes\Controller;
use Backend\Classes\BackendController;
use October\Rain\Exception\SystemException;

use Academy\Courses\Models\Partner;
use Academy\Courses\Models\Institution;
use Academy\Courses\Models\Direction;
use Academy\Courses\Models\Activity;
use Academy\Courses\Models\Course;

class Api extends Controller
{
    private $format = 'json';

    private function setRealAction()
    {
        if (false !== $pos = strrpos($this->action, '.')) {
            $this->format = substr($this->action, $pos + 1);
            $this->action = substr($this->action, 0, $pos);
            if (!method_exists($this, $this->format . 'Response')) {
                $this->action = $this->action . '.' . $this->format;
                $this->format = 'json';
            }
        }
    }

    public function __construct()
    {
        parent::__construct();
        \BackendMenu::setContext('Academy.Courses', 'courses', 'api');
        $this->setRealAction();
    }

    public function actionExists($name, $internal = false)
    {
        return Parent::actionExists($this->action, $internal);
    }

    protected function execPageAction($actionName, $parameters)
    {
        $this->setRealAction();
        $actionName = $this->action;
        $result = null;

        if (!$this->actionExists($actionName)) {
            throw new SystemException(sprintf(
                "Action %s is not found in the controller %s",
                $actionName,
                get_class($this)
            ));
        }

        // Execute the action
        $result = call_user_func_array([$this, $actionName], $parameters);

        // Expecting \Response and \RedirectResponse
        if ($result instanceof \Symfony\Component\HttpFoundation\Response) {
            return $result;
        }

        // No page title
        if (!$this->pageTitle) {
            $this->pageTitle = 'backend::lang.page.untitled';
        }

        // Load the view
        if (!$this->suppressView && is_null($result)) {
            return $this->makeView($actionName);
        }
        return call_user_func_array([$this, $this->format . 'Response'], [$result]);
    }




    /*
        функции форматирования данных должны называться {format}Response
        тогда данные смогут возвращаться в нужном формате
    */
    private function jsonResponse($response)
    {
        if ($response instanceof \StdClass) {
            header('Content-Type: application/json');
            die(json_encode($response));
        }
        return \Response::make()->setContent($response);
    }






    public function partners()
    {
        $result = [];
        foreach (Partner::select(['id', 'title', 'url', 'description'])->get() as $partner) {
            $new = (object) $partner->attributes;
            if ($partner->image) {
                $new->image = $partner->image->getPath();
            }
            else {
                $new->image = '';
            }
            $result[] = $new;
        }
        return $result;
    }

    public function institutions()
    {
        $result = [];
        foreach (Institution::select(['id', 'title', 'url', 'description'])->get() as $institution) {
            $new = (object) $institution->attributes;
            if ($institution->image) {
                $new->image = $institution->image->getPath();
            }
            else {
                $new->image = '';
            }
            $result[] = $new;
        }
        return $result;
    }

    public function directions()
    {
        $result = [];
        foreach (Direction::select(['id', 'title', 'code'])->get() as $direction) {
            $result[] = (object) $direction->attributes;
        }
        return $result;
    }

    public function activities()
    {
        $result = [];
        foreach (Activity::select(['id', 'title'])->get() as $activity) {
            $result[] = (object) $activity->attributes;
        }
        return $result;
    }

    private static function getWhiteParams($keys)
    {
        if (!is_array($get = get())) {
            return [];
        }
        $intersect = array_intersect(array_keys($get), $keys);
        $result = [];
        foreach ($intersect as $key) {
            $result[$key] = $get[$key];
        }
        return $result;
    }

    public function courses($global_id = null)
    {
        if ($global_id) {
            if (!$course = Course::where('global_id', $global_id)->first()) {
                return;
            }
            $new = (object) $course->attributes;
            if ($course->image) {
                $new->image = $course->image->getPath();
            }
            else {
                $new->image = '';
            }

            $new->teachers = [];
            foreach ($course->teachers as $teacher) {
                $newTeacher = (object) $teacher->attributes;
                if ($teacher->image) {
                    $newTeacher->image = $teacher->image->getPath();
                }
                else {
                    $newTeacher->image = '';
                }
                $new->teachers[] = $newTeacher;
            }
            $new->competences = [];
            foreach ($course->competences as $competence) {
                $new->competences[] = $competence->title;
            }
            $new->requirements = [];
            foreach ($course->requirements as $requirement) {
                $new->requirements[] = $requirement->title;
            }
            $new->learning_outcomes = [];
            foreach ($course->learning_outcomes as $learning_outcome) {
                $new->learning_outcomes[] = $learning_outcome->title;
            }
            $new->directions = [];
            foreach ($course->directions as $direction) {
                $new->directions[] = $direction->id;
            }
            $new->activities = [];
            foreach ($course->activities as $activity) {
                $new->activities[] = $activity->id;
            }
            $new->credits = [];
            foreach ($course->credits()->select(['institution_id', 'direction_id'])->get() as $credit) {
                $new->credits[] = (object) $credit->attributes;
            }
            $new->feedback = [];
            return $new;
        }

        $query = Course::select([
            'global_id',
            'title',
            'language',
            'description',
            'started_at',
            'institution_id',
            'partner_id',
            'rating',
            'experts_rating',
            'visitors_number',
            'id',
        ]);
        $params = Self::getWhiteParams(['language', 'institution_id', 'partner_id', 'direction_id', 'activity_id']);
        foreach ($params as $field_name => $values) {
            if (is_string($values)) {
                $values = explode(',', $values);
            }
            $query->whereIn($field_name, $values);
        }
        $result = (object) [
            'count' => $query->count()
        ];
        $limit = 2;
        $page = max(get('page', 1), 1);
        $maxPage = ceil($result->count / $limit);
        $page = min($page, $maxPage);
        $offset = ($page - 1) * $limit;
        $query->limit($limit)->offset($offset);

        $url = $this->actionUrl();
        if ('' !== $urlParams = http_build_query($params)) {
            $urlParams = '&' . $urlParams;
        }
        if ($page > 1) {
            $result->previous = $url . '?page=' . ($page - 1) . $urlParams;
        }
        if ($page < $maxPage) {
            $result->next = $url . '?page=' . ($page + 1) . $urlParams;
        }
        $result->results = [];
        foreach ($query->get() as $course) {
            $new = (object) $course->attributes;
            if ($course->image) {
                $new->image = $course->image->getPath();
            }
            else {
                $new->image = '';
            }
            $result->results[] = $new;
        }
        return $result;
    }
}