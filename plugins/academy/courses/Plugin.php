<?php namespace Academy\Courses;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Academy.Courses',
            'description' => 'Academy.Courses',
            'author'      => 'Academy',
            'icon'        => 'icon-share-alt-square'
        ];
    }

    public function registerPermissions()
    {
        return [
            'academy.courses.courses' => [
                'tab' => 'edu.ru',
                'label' => trans('academy.courses::course.label'),
            ],
            'academy.courses.institutions' => [
                'tab' => 'edu.ru',
                'label' => trans('academy.courses::institution.label'),
            ],
            'academy.courses.partners' => [
                'tab' => 'edu.ru',
                'label' => trans('academy.courses::partner.label'),
            ],
            'academy.courses.teachers' => [
                'tab' => 'edu.ru',
                'label' => trans('academy.courses::teacher.label'),
            ],
            'academy.courses.directions' => [
                'tab' => 'edu.ru',
                'label' => trans('academy.courses::direction.label'),
            ],
            'academy.courses.activities' => [
                'tab' => 'edu.ru',
                'label' => trans('academy.courses::activity.label'),
            ],
            'academy.courses.competences' => [
                'tab' => 'edu.ru',
                'label' => trans('academy.courses::competence.label'),
            ],
        ];
    }

    public function registerNavigation()
    {
        if (in_array('Academy.Courses', \Config::get('cms.hiddenPlugins', []))) {
            return;
        }
        return [
            'courses' => [
                'label' => 'edu.ru',// trans('academy.courses::plugin.label'),
                'url' => \Backend::url('academy/courses/courses'),
                'icon' => 'icon-share-alt-square',
                'permissions' => ['academy.courses.*'],
                'order' => 500,
                'sideMenu' => [
                    'courses' => [
                        'label' => trans('academy.courses::course.label'),
                        'icon' => 'icon-files-o',
                        'url' => \Backend::url('academy/courses/courses'),
                        'permissions' => ['academy.courses.courses']
                    ],
                    'institutions' => [
                        'label' => trans('academy.courses::institution.label'),
                        'icon' => 'icon-university',
                        'url' => \Backend::url('academy/courses/institutions'),
                        'permissions' => ['academy.courses.institutions']
                    ],
                    'partners' => [
                        'label' => trans('academy.courses::partner.label'),
                        'icon' => 'icon-users',
                        'url' => \Backend::url('academy/courses/partners'),
                        'permissions' => ['academy.courses.partners']
                    ],
                    'teachers' => [
                        'label' => trans('academy.courses::teacher.label'),
                        'icon' => 'icon-graduation-cap',
                        'url' => \Backend::url('academy/courses/teachers'),
                        'permissions' => ['academy.courses.teachers']
                    ],
                    'directions' => [
                        'label' => trans('academy.courses::direction.label'),
                        'icon' => 'icon-map-signs',
                        'url' => \Backend::url('academy/courses/directions'),
                        'permissions' => ['academy.courses.directions']
                    ],
                    'activities' => [
                        'label' => trans('academy.courses::activity.label'),
                        'icon' => 'icon-suitcase',
                        'url' => \Backend::url('academy/courses/activities'),
                        'permissions' => ['academy.courses.activities']
                    ],
                    'competences' => [
                        'label' => trans('academy.courses::competence.label'),
                        'icon' => 'icon-trophy',
                        'url' => \Backend::url('academy/courses/competences'),
                        'permissions' => ['academy.courses.competences']
                    ],
                ]
            ]
        ];
    }
}