<?php namespace Academy\Courses\Models;

class Requirement extends \Model
{
    public $table = 'academy_courses_requirements';
    public $timestamps = false;
}