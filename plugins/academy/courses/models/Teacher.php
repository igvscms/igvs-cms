<?php namespace Academy\Courses\Models;

class Teacher extends \Model
{
    public $table = 'academy_courses_teachers';
    public $timestamps = false;

    public $attachOne = [
        'image' => 'System\Models\File',
    ];
}