<?php namespace Academy\Courses\Models;

class Partner extends \Model
{
    public $table = 'academy_courses_partners';
    public $timestamps = false;

    public $attachOne = [
        'image' => 'System\Models\File',
    ];
}