<?php namespace Academy\Courses\Models;

class Institution extends \Model
{
    public $table = 'academy_courses_institutions';
    public $timestamps = false;

    public $attachOne = [
        'image' => 'System\Models\File',
    ];
}