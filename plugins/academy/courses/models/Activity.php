<?php namespace Academy\Courses\Models;

class Activity extends \Model
{
    public $table = 'academy_courses_activities';
    public $timestamps = false;
}