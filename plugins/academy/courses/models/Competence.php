<?php namespace Academy\Courses\Models;

class Competence extends \Model
{
    public $table = 'academy_courses_competences';
    public $timestamps = false;
}