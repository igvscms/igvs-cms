<?php namespace Academy\Courses\Models;

class Credit extends \Model
{
    public $table = 'academy_courses_credits';
    public $timestamps = false;

    public $belongsTo = [
        'institution' => 'Academy\Courses\Models\Institution',
        'direction' => 'Academy\Courses\Models\Direction',
    ];

    public function getIdAttribute()
    {
        if (array_key_exists('id', $this->attributes)) {
            return $this->attributes['id'];
        }
        return $this->attributes['id'] = $this->course_id . '_' . $this->institution_id . '_' . $this->direction_id;
    }

    public function find($manage_id)
    {
        $keys = explode('_', $manage_id);
        $first = Self::where('course_id', $keys[0])
            ->where('institution_id', $keys[1])
            ->where('direction_id', $keys[2])
            ->first();
        if ($first) {
            $first->id = $manage_id;
        }
        return $first;
    }

    public function beforeUpdate()
    {
        if (isset($this->id)) {
            $keys = explode('_', $this->id);
            Self::where('course_id', $keys[0])
                ->where('institution_id', $keys[1])
                ->where('direction_id', $keys[2])
                ->update([
                    'course_id' => $this->course_id,
                    'institution_id' => $this->institution_id,
                    'direction_id' => $this->direction_id
                ]);
            return false;
        }
    }

    public function beforeDelete()
    {
        if (isset($this->id)) {
            $keys = explode('_', $this->id);
            Self::where('course_id', $keys[0])
                ->where('institution_id', $keys[1])
                ->where('direction_id', $keys[2])
                ->delete();
            return false;
        }
    }
}