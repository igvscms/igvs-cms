<?php namespace Academy\Courses\Models;

class Direction extends \Model
{
    public $table = 'academy_courses_directions';
    public $timestamps = false;
}