<?php namespace Academy\Courses\Models;

class LearningOutcome extends \Model
{
    public $table = 'academy_courses_learning_outcomes';
    public $timestamps = false;
}