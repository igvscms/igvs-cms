<?php namespace Academy\Courses\Models;

class Course extends \Model
{
    public $table = 'academy_courses';

    public $belongsTo = [
        'institution' => 'Academy\Courses\Models\Institution',
        'partner' => 'Academy\Courses\Models\Partner',
    ];

    public $attachOne = [
        'image' => 'System\Models\File',
    ];

    public $dates = ['finished_at', 'record_end_at'];

    public $belongsToMany = [
        'teachers' => [
            'Academy\Courses\Models\Teacher',
            'table' => 'academy_courses_courses_teachers'
        ],
        'directions' => [
            'Academy\Courses\Models\Direction',
            'table' => 'academy_courses_courses_directions'
        ],
        'activities' => [
            'Academy\Courses\Models\Activity',
            'table' => 'academy_courses_courses_activities'
        ],
        'competences' => [
            'Academy\Courses\Models\Competence',
            'table' => 'academy_courses_courses_competences'
        ]
    ];

    public $hasMany = [
        'credits' => 'Academy\Courses\Models\Credit',
        'requirements' => 'Academy\Courses\Models\Requirement',
        'learning_outcomes' => 'Academy\Courses\Models\LearningOutcome',
    ];

    public function beforeCreate()
    {
        /*$rb = str_split(md5(implode(rand(),array_merge($this->attributes, [time()]))), 4);
        $global_id = "{$rb[0]}{$rb[1]}-{$rb[2]}-{$rb[3]}-{$rb[4]}-{$rb[5]}{$rb[6]}{$rb[7]}";
        $global_id[14] = '4';
        $chois = ['8','9','a','b'];
        if (!in_array($global_id[19], $chois)) {
            $global_id[19] = $chois[rand(0, 3)];
        }
        $this->global_id = $global_id;*/
    }

    public function getLanguageOptions()
    {
        return trans('academy.courses::course.lang');
    }

    public static function declension($quantity, $endings = ['','а','ов'])
    {
        $q100 = $quantity % 100;
        $q10 = $q100 % 10;

        if (($q100 > 4 && $q100 < 21)
            || ($q10 > 4)
            || ($q10 == 0)
        ) {
            return $endings[2];
        }
        if ($q10 == 1) {
            return $endings[0];
        }
        return $endings[1];
    }

    public function setAutoDurationAttribute($value)
    {
        $this->duration = $value;
    }

    public function getWeekCount()
    {
        return ceil($this->volume / $this->intensity_per_week);
    }

    public function setIntensityPerWeekAttribute($value)
    {
        $this->attributes['intensity_per_week'] = (int) $value;
    }

    public function getIntensityPerWeekAttribute()
    {
        if (!empty($this->attributes['intensity_per_week'])) {
            return $this->attributes['intensity_per_week'];
        }
    }

    public function getVolumeAttribute()
    {
        if (!empty($this->attributes['volume'])) {
            return $this->attributes['volume'];
        }
    }

    public function getVisitorsNumberAttribute()
    {
        if (!empty($this->attributes['visitors_number'])) {
            return $this->attributes['visitors_number'];
        }
    }

    public function setVisitorsNumberAttribute($value)
    {
        $this->attributes['visitors_number'] = (int) $value;
    }

    public function getTotalVisitorsNumberAttribute()
    {
        if (!empty($this->attributes['total_visitors_number'])) {
            return $this->attributes['total_visitors_number'];
        }
    }

    public function setTotalVisitorsNumberAttribute($value)
    {
        $this->attributes['total_visitors_number'] = (int) $value;
    }

    public function setLecturesNumberAttribute($value)
    {
        $this->attributes['lectures_number'] = (int) $value;
    }

    public function setVolumeAttribute($value)
    {
        $this->attributes['volume'] = (int) $value;
    }

    public function setRatingAttribute($value)
    {
        $this->attributes['rating'] = (int) $value;
    }

    public function getRatingAttribute()
    {
        if (!empty($this->attributes['rating'])) {
            return $this->attributes['rating'];
        }
    }

    public function setExpertsRatingAttribute($value)
    {
        $this->attributes['experts_rating'] = (int) $value;
    }

    public function getExpertsRatingAttribute()
    {
        if (!empty($this->attributes['experts_rating'])) {
            return $this->attributes['experts_rating'];
        }
    }

    public function getContentAttribute()
    {
        if (!empty($this->attributes['content'])) {
            return $this->attributes['content'];
        }
    }

    public function getAccreditationAttribute()
    {
        if (!empty($this->attributes['accreditation'])) {
            return $this->attributes['accreditation'];
        }
    }

    public function getAutoDurationAttribute()
    {
        if (in_array('auto_duration', post('fields', []))
            && !empty($this->volume)
            && !empty($this->intensity_per_week)
        ) {
            $count = $this->getWeekCount();
            return $count;// . ' Недел' . Self::declension($count, ['я', 'и', 'ь']);
        }
        return $this->duration;
    }

    public function setAutoRecordEndAtAttribute($value)
    {
        $this->record_end_at = $value;
    }

    public function getAutoRecordEndAtAttribute()
    {
        if (in_array('auto_record_end_at', post('fields', []))
            && !empty($this->started_at)
        ) {
            return $this->started_at;
        }
        if (isset($this->attributes['record_end_at'])) {
            return $this->attributes['record_end_at'];
        }
    }

    public function setAutofinishedAtAttribute($value)
    {
        $this->finished_at = $value;
    }

    public function beforeSave()
    {
        if (empty($this->attributes['record_end_at'])) {
            $this->attributes['record_end_at'] = null;
        }
        if (empty($this->attributes['finished_at'])) {
            $this->attributes['finished_at'] = null;
        }
    }

    public function getAutofinishedAtAttribute()
    {
        if (in_array('auto_finished_at', post('fields', []))
            && !empty($this->started_at)
            && !empty($this->volume)
            && !empty($this->intensity_per_week)
        ) {
            $date = new \DateTime($this->started_at);
            $count = $this->getWeekCount();
            $date->modify("+ $count week");
            return $date->format('Y-m-d H:i:s');
        }
        if (isset($this->attributes['finished_at'])) {
            return $this->attributes['finished_at'];
        }
    }
}