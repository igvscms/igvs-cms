<?php return [
    'label' => 'Площадки',
    'create' => 'Создать площадку',
    'update' => 'Изменить площадку',
    'fields' => [
        'image' => 'Логотип',
        'title' => 'Название',
        'url' => 'Ссылка',
        'description' => 'Описание',
    ]
];