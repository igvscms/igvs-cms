<?php return [
    'label' => 'Лекторы',
    'create' => 'Добавить лектора',
    'update' => 'Изменить лектора',
    'relation_label' => 'лектора',
    'fields' => [
        'image' => 'Фото',
        'title' => 'ФИО',
        'description' => 'Описание',
    ]
];