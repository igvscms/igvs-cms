<?php return [
    'label' => 'Вузы',
    'create' => 'Создать вуз',
    'update' => 'Изменить вуз',
    'fields' => [
        'image' => 'Логотип',
        'title' => 'Название',
        'url' => 'Ссылка',
        'description' => 'Описание',
    ]
];