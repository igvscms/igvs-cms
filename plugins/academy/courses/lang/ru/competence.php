<?php return [
    'label' => 'Компетенции',
    'create' => 'Новая компетенция',
    'update' => 'Изменить компетенцию',
    'relation_label' => 'компетенцию',
    'fields' => [
        'title' => 'Название',
    ]
];