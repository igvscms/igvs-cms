<?php namespace Academy\System\Abstracts;

abstract class Driver {

    use \Academy\System\Traits\DriverInit;
    protected static $name = 'Driver';

    private function __construct() {}
    final private function __clone() {}
    final private function __wakeup() {}

    protected static function getSystemClass() {}
}