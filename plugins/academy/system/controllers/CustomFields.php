<?php namespace Academy\System\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

class CustomFields extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.RelationController',
        'Academy.System.Behaviors.CrudController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';
}