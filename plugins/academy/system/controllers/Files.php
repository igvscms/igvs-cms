<?php namespace Academy\System\Controllers;

use Backend\Classes\Controller;
use System\Models\File;

class Files extends Controller
{
    protected $publicActions = ['get', 'modal'];

    public function get($id)
    {
        File::find($id)->output();
        exit;
    }
    public function modal($id)
    {
        $this->vars['file_id'] = $id;
    }
}