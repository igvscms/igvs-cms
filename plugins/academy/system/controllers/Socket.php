<?php namespace Academy\System\Controllers;

use Backend\Classes\Controller;
use System\Classes\SettingsManager;
use Academy\System\Models\SocketPacket;
use Academy\System\Models\Socket as SocketModel;

class Socket extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.RelationController',
        'Academy.System.Behaviors.ListController',
    ];
    public $requiredPermissions = ['academy.system.socket'];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        \BackendMenu::setContext('Academy.System', 'system', 'socket');
        SettingsManager::setContext('Academy.system', 'socket');
    }

    public function listExtendQuery($query)
    {
        $packet = new SocketPacket();
        $packetTable = $packet->getTable();
        $socket = new SocketModel();
        $socketTable = $socket->getTable();
        $query->withTrashed()->leftJoin(\DB::raw("(
                SELECT
                    max(`created_at`)`max_created_at`,
                    count(*)`count`,
                    MAX(`dt`)`max_dt`,
                    SUM(`dt`)`sum_dt`,
                    AVG(`dt`)`avg_dt`,
                    MAX(LENGTH(`value`))`max_value`,
                    SUM(LENGTH(`value`))`sum_value`,
                    AVG(LENGTH(`value`))`avg_value`,
                    `session_id`
                FROM `$packetTable`
                GROUP BY `session_id`
                ORDER BY NULL
            )`$packetTable`"), "$packetTable.session_id", '=', "$socketTable.session_id")
            ->addSelect('max_created_at', 'count', 'max_dt', 'sum_dt', 'avg_dt', 'max_value', 'sum_value', 'avg_value');
    }

    public function onDelete()
    {
        SocketModel::whereIn('session_id', post('checked', []))->delete();
    }

    public function onRestore()
    {
        SocketModel::withTrashed()->whereIn('session_id', post('checked', []))->restore();
    }
}