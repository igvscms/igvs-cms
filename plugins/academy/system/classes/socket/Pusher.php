<?php namespace Academy\System\Classes\Socket;

use Academy\System\Classes\Socket\Base\BasePusher;
use ZMQContext;
use Academy\System\Models\Socket;
use Academy\System\Models\SocketPacket;

class Pusher extends BasePusher
{
    static function sentDataToServer(array $data)
    {
        $context = new ZMQContext();
        $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'my pusher');

        $socket->connect('tcp://127.0.0.1:5555');

        $data = json_encode($data);
        $socket->send($data);
    }

    public function dispatchByIp($topic, $data, $sessions, $is_ignoring, $ip, $is_ip = true)
    {

        foreach ($topic->getIterator() as $client) {
            if (($client->remoteAddress === $ip) !== $is_ip
                || $is_ignoring == isset($sessions[$client->WAMP->sessionId])
            ) {
                continue;
            }
            $socket = $this->addSocketToBase($client);
            if (!$socket->deleted_at) {
                $time = -microtime(true);
                $client->event($topic->getId(), $data);
                $time += microtime(true);
                $packet = new SocketPacket([
                    'value' => json_encode($data),
                    'session_id' => $client->WAMP->sessionId,
                    'dt' => round($time * 1000)
                ]);
                $packet->save();
            }
        }
    }

    public function dispatch($topic, $data, array $sessions = array(), $is_ignoring = true)
    {

        $ip = null;
        foreach ($topic->getIterator() as $client) {
            if (isset($data['data']['socket_session_id']) && $client->WAMP->sessionId === $data['data']['socket_session_id']) {
                $ip = $client->remoteAddress;
                break;
            }
        }
        $sessions = array_flip($sessions);
        $this->dispatchByIp($topic, $data, $sessions, $is_ignoring, $ip);
        $this->dispatchByIp($topic, $data, $sessions, $is_ignoring, $ip, false);
    }

    public function broadcast($jsonDataToSend)
    {

        $aDataToSend = json_decode($jsonDataToSend, true);

        $subscribedTopics = $this->getSubscribedTopics();

        if (isset($subscribedTopics[$aDataToSend['topic_id']])) {
            $topic = $subscribedTopics[$aDataToSend['topic_id']];

            if (isset($aDataToSend['socketActionsReceive']) && is_callable($aDataToSend['socketActionsReceive'])) {
                $eligible = call_user_func($aDataToSend['socketActionsReceive'], $aDataToSend['data'], $this->sockets);
                $is_ignoring = false;
                unset($aDataToSend['socketActionsReceive']);
            }
            else {
                $eligible = [];
                $is_ignoring = true;
            }
            $this->dispatch($topic, $aDataToSend, $eligible, $is_ignoring);
        }
    }
}