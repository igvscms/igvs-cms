<?php namespace Academy\System\Classes\Socket\Base;

use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;
use Illuminate\Session\FileSessionHandler;
use Illuminate\Session\SessionManager;
use Academy\System\Models\Socket;

class BasePusher implements WampServerInterface {

    protected $subscribedTopics = [];
    public $sockets = [];

    public function getSubscribedTopics()
    {
        return $this->subscribedTopics;
    }

    public function addSocketToBase(ConnectionInterface $conn, $page = 'undefined')
    {
        if (!$socket = Socket::withTrashed()->find($conn->WAMP->sessionId)) {
            $session = self::getSessionByConnect($conn);
            $user_id = self::getUserIdByConnect($session, $conn);
            $socket = new Socket([
                'session_id' => $conn->WAMP->sessionId,
                'user_id' => $user_id,
                'topic_id' => '',
                'page' => $page,
                'ip' => $conn->remoteAddress

            ]);
            $socket->save();
        }
        return $socket;
    }

    public function addSubscribedTopic($topic)
    {
        $this->subscribedTopics[$topic->getId()] = $topic;
    }

    public function onSubscribe(ConnectionInterface $conn, $topic)
    {
        $this->addSubscribedTopic($topic);
    }

    public function onUnSubscribe(ConnectionInterface $conn, $topic)
    {
    }

    private static function getCookieByConnect($conn, $key)
    {
        return $conn
            ->WebSocket
            ->request
            ->getCookie($key);
    }

    private static function getGetByConnect($conn, $key)
    {
        return $conn
            ->WebSocket
            ->request
            ->getUrl(true)
            ->getQuery()
            ->get($key);
    }

    private static function getSessionByConnect($conn)
    {
        if (!$october_session = self::getCookieByConnect($conn, \Config::get('session.cookie'))) {
            return null;
        }
        $session_id = \Crypt::decrypt(urldecode($october_session));

        $session_file = \Session::driver()
            ->getHandler()
            ->read($session_id);

        if (!$session_file) {
            return null;
        }
        return unserialize($session_file);
    }

    private static function getUserIdByConnect($session, $conn)
    {
        if ($admin_auth = self::getCookieByConnect($conn, 'admin_auth')) {
            $admin_auth = \Crypt::decrypt(urldecode($admin_auth));
            if (isset($admin_auth[0])) {
                return $admin_auth[0];
            }
        }
        if (isset($session['admin_auth'][0])) {
            return $session['admin_auth'][0];
        }
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $session = self::getSessionByConnect($conn);
        $user_id = self::getUserIdByConnect($session, $conn);

        $this->sockets[$conn->WAMP->sessionId] = [
            'action'  => self::getGetByConnect($conn, 'class') . '::' . self::getGetByConnect($conn, 'action'),
            'params'  => json_decode(self::getGetByConnect($conn, 'params'), true),
            'user_id' => $user_id,
            'session' => $session,
            'cookie'  => $conn->WebSocket->request->getCookies(),
            // \Cookie::getFacadeApplication()['request']->cookies->replace('new cookies array'); - присваиваем новые куки
            // \Session::clear();\Session::replace(array()); - присваивает новую сессию
        ];
        $this->addSocketToBase($conn, $session['_previous']['url']);
        echo "New connection! ({$conn->WAMP->sessionId})\n";
    }

    public function onClose(ConnectionInterface $conn)
    {
        unset($this->sockets[$conn->WAMP->sessionId]);
        if ($socket = Socket::find($conn->WAMP->sessionId)) {
            $socket->forceDelete();
        }
        echo "Connection {$conn->WAMP->sessionId} has disconnected\n";
    }

    public function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        $this->callError($id, $topic, 'You are not allowed to make calls')->close();
    }

    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        $this->close();
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occured: {$e->getMessage()}\n";
        $conn->close();
    }
}