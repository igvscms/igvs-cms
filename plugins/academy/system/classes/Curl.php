<?php namespace Academy\System\Classes;

class Curl
{
    private $options = [];
    private $getFields = [];
    private $info = [];
    private $headerFunction;

    private function __construct() {}

    private static function nameToConst($name)
    {
        if (defined($option = 'CURLOPT_' . strtoupper($name))) {
            return constant($option);
        }
    }

    private function setOpt($name, $value)
    {
        if ($const = Self::nameToConst($name)) {
             $this->options[$const] = $value;
        }
    }

    private function getOpt($name, $default = null)
    {
        if (isset($this->options[$const = Self::nameToConst($name)])) {
             return $this->options[$const];
        }
        return $default;
    }

    public function getInfo($opt)
    {
        if (defined($info = 'CURLINFO_' . strtoupper($opt))) {
            if (CURLINFO_HEADER_OUT === $info = constant($info)) {
                $this->header_out();
            }
        }
    }

    public function exec()
    {
        $curl = curl_init();
        $options = $this->options;
        if ($this->headerFunction) {
            $options[CURLOPT_HEADERFUNCTION] = function($curl, $header) {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                $function = $this->headerFunction;
                $function(isset($header[1]) ? trim($header[0]) : null, trim(end($header)));
                return $len;
            };
        }
        if ($this->httpHeader) {
            $options[CURLOPT_HTTPHEADER] = [];
            foreach ($this->httpHeader as $key => $value) {
                $options[CURLOPT_HTTPHEADER][] = is_int($key) ? $value : "$key: $value";
            }
        }
        if (isset($this->postFields)) {
            if (!is_string($this->postFields)) {
                if (isset($this->httpHeader['content-type'])
                    && $this->httpHeader['content-type'] === 'application/json'
                ) {
                    $options[CURLOPT_POSTFIELDS] = json_encode($this->postFields);
                }
                else {
                    $options[CURLOPT_POSTFIELDS] = http_build_query($this->postFields);
                }
            }
            if (!$this->put) {
                $options[CURLOPT_POST] = true;
            }
            elseif (isset($this->infile)) {
                unset($options[CURLOPT_POST]);
            }
            else {
                unset($options[CURLOPT_PUT]);
                $options[CURLOPT_CUSTOMREQUEST] = 'PUT';
            }
        }
        if (count($this->getFields) && $this->url) {
            $options[CURLOPT_URL] = $this->url . '?' . http_build_query($this->getFields);
        }
        curl_setopt_array($curl, $options);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    public function once()
    {
        $this->returnTransfer = true;
        return $this->exec();
    }

    public function fromJson()
    {
        return json_decode($this->once());
    }

    public function setGetFieldsAttributes(array $value)
    {
        $this->getFields = $value;
    }

    public function getGetFieldsAttributes()
    {
        return $this->getFields;
    }

    public function __set($name, $value)
    {
        if (method_exists($this, "set{$name}Attribute")) {
            $this->{"set{$name}Attribute"}($value);
        }
        else {
            $this->setOpt($name, $value);
        }
        return $this;
    }

    public function __call($name, $args)
    {
        if (method_exists($this, "call{$name}Attribute")) {
            $this->{"call{$name}Attribute"}($args);
        }
        else {
            $this->$name = isset($args[0]) ? $args[0] : true;
        }
        return $this;
    }

    public function setHeaderFunctionAttribute(\Closure $callback)
    {
        $this->headerFunction = $callback;
    }

    public function getHeaderFunctionAttribute()
    {
        return $this->headerFunction;
    }

    public function __get($name)
    {
        if (method_exists($this, "get{$name}Attribute")) {
            return $this->{"get{$name}Attribute"}();
        }
        return $this->getOpt($name);
    }

    public function __unset($name)
    {
        unset($this->options[Self::nameToConst($name)]);
    }

    public function __isset($name)
    {
        return isset($this->options[Self::nameToConst($name)]);
    }

    public static function __callStatic($name, $args)
    {
        return call_user_func_array([new Self(), $name], $args);
    }
}