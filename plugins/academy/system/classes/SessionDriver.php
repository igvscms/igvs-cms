<?php namespace Academy\System\Classes;

use Academy\System\Abstracts\Driver;

class SessionDriver extends Driver implements \SessionHandlerInterface
{
    protected static $name = 'sessionDriver';

    public function open($savePath, $sessionName)
    {
        return true;
    }
    public function close()
    {
        return true;
    }
    public function read($sessionId) {}
    public function write($sessionId, $data) {}
    public function destroy($sessionId) {}
    public function gc($maxlifetime) {}

    final protected static function getSystemClass() {
        return \Session::class;
    }
}