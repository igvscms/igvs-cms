<?php namespace Academy\System\Classes;

class Differ
{
    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    private static function mb_substr_replace($string, $replacement, $start, $length)
    {
        return mb_substr($string, 0, $start) . $replacement . mb_substr($string, $start + $length); 
    }

    private static function get(&$value, $default = null)
    {
        return isset($value) ? $value : $default;
    }

    public static function setDiff($text, $diffArray = array())
    {
        $start = 0;
        foreach ($diffArray as $options) {
            $text = self::mb_substr_replace($text, self::get($options['r'], ''), $start + self::get($options['s'], 0), self::get($options['l'], 0));
            $start += self::get($options['s'], 0) + mb_strlen(self::get($options['r'], ''));
        }
        return $text;
    }

    public static function setMoreDiff($text, $diffArrayOfArray = array())
    {
        foreach ($diffArrayOfArray as $diffArray) {
            $text = self::setDiff($text, $diffArray);
        }
        return $text;
    }

    public static function getDiff($text1, $text2, $efficiency = false)
    {
        $dmp = new \DiffMatchPatch();
        $diffs = $dmp->diff_main($text1, $text2, $efficiency);

        $resDiff = [];
        $num = 0;
        $types = [0 => 0, -1 => 1, 1 => 2];
        $fields = ['s','l','r'];
        $type = 0;
        foreach ($diffs as $options) {
            if ($type > $types[$options[0]]) {
                $num++;
            }
            switch ($type = $types[$options[0]]) {
                case 0:
                case 1:
                    $resDiff[$num][$fields[$type]] = mb_strlen($options[1]);
                    break;
                case 2:
                    $resDiff[$num][$fields[$type]] = $options[1];
            }
        }
        return $resDiff;
    }
}