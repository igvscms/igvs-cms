<?php namespace Academy\System\Classes;

class PseudoController
{
    use \System\Traits\ViewMaker;

    private $controller;
    public $widget;

    public function __construct($controller, $viewPath)
    {
        $this->controller = $controller;
        $this->viewPath = $viewPath;
        if (is_object($this->controller)) {
            $this->widget = &$controller->widget;
        }
    }
    public function addCss()
    {
        if (is_object($this->controller)) {
            return call_user_func_array([$this->controller, 'addCss'], func_get_args());
        }
    }
    public function addJs()
    {
        if (is_object($this->controller)) {
            return call_user_func_array([$this->controller, 'addJs'], func_get_args());
        }
    }
}