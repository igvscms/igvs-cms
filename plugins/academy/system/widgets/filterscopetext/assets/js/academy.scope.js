Vue.component('filerScopeText', {
    template:
        '<span class="filter-scope filerScopeText oc-icon-filter"\
            @click="show = true"\
            tabindex="-1"\
            :class="{active: scope.widgetValue.length}"\
            @blur.capture="blur"\
            @focus.capture="focus">\
            <span class="filter-label">{{ scope.label }}</span>\
            <span class="filter-setting" v-if="scope.widgetValue.length">{{ scope.widgetValue.length }}</span>\
            <div class="control-popover placement-bottom in" v-if="show">\
                <div class="control-filter-popover">\
                    <div class="filter-search">\
                        <button class="close" @click.stop="show = false">×</button>\
                        <input class="form-control icon search" v-model="search">\
                    </div>\
                    <ul class="filter-items" @scroll="loaded">\
                        <li v-for="val in values" v-if="isDeactive(val)" @click="active(val)">\
                            {{ val.value }}\
                        </li>\
                        <li class="loading" v-if="loading">\
                            <span></span>\
                        </li>\
                    </ul>\
                    <button v-if="scope.widgetValue.length" class="close" @click.stop="clear">×</button>\
                    <ul class="filter-active-items">\
                        <li v-for="val in values" @click="deactive(val)" v-if="isActive(val)">\
                            {{ val.value }}\
                        </li>\
                    </ul>\
                </div>\
            </div>\
        </span>',
    props: {
        scope: Object
    },
    data: function() {
        return {
            show: false,
            values: [],
            search: '',
            timeout: 0,
            editable: false,
            finish: false,
            limit: 20,
            loading: true,
            defaultValue: {}
        }
    },
    beforeMount: function() {
        document.body.setAttribute('tabindex', '-1');
        document.body.addEventListener('blur', this.blur);
        if (Object.keys(this.scope.widgetValue).length) {
            this.on('getDefaultOptions', {
                keys: this.scope.widgetValue
            })
        }
    },
    watch: {
        search: function() {
            this.finish = true;
            this.values = [];
            this.loading = true;
            this.on('getOptions', {
                search: this.search,
                limit: this.limit
            });
        },
        show: function(show) {
            if (!show && this.editable) {
                this.on('setValue', {
                    value: this.scope.widgetValue
                })
            }
            if (show) {
                this.editable = false;
                if (!this.finish && !this.values.length) {
                    this.finish = true;
                    this.loading = true;
                    this.on('getOptions', {
                        search: this.search,
                        limit: this.limit
                    });
                }
            }
        }
    },
    computed: {
        alias: function() {
            return this.scope.widgetAlias;
        },
        isArray: function() {
            return typeof this.value !== 'number';
        },
        count: function() {
            if (this.isArray) {
                return this.value.length;
            }
            return this.value;
        }
    },
    methods: {
        getDefaultOptions: function(values) {
            this.defaultValue = values.options;
        },
        loaded: function(event) {
            if (!this.loading && !this.finish) {
                this.loading = true;
                this.on('getOptions', {
                    search: this.search,
                    offset: this.values.length,
                    limit: this.limit
                });
            }
        },
        refresh: function() {
            this.values = [];
            this.finish = false;
        },
        setValue: function() {
            this.$emit('scopes');
        },
        focus: function() {
            clearTimeout(this.timeout);
            this.show = true;
        },
        realBlur: function() {
            this.show = false;
        },
        blur: function(event) {
            clearTimeout(this.timeout);
            this.timeout = setTimeout(this.realBlur, 0);
        },
        isDeactive: function(val) {
            if (this.scope.widgetValue.indexOf(val.id) >= 0) {
                return false;
            }
            if (this.search === '') {
                return true;
            }
            return val.value.toLowerCase().indexOf(this.search.toLowerCase()) >= 0;
        },
        isActive: function(val) {
            if (this.scope.widgetValue.indexOf(val.id) < 0) {
                return false;
            }
            if (this.search === '') {
                return true;
            }
            return val.value.toLowerCase().indexOf(this.search.toLowerCase()) >= 0;
        },
        deactive: function(val) {
            this.editable = true;
            this.scope.widgetValue.splice(this.scope.widgetValue.indexOf(val.id), 1)
        },
        clear: function() {
            this.editable = true;
            Vue.set(this.scope, 'widgetValue', []);
        },
        active: function(val) {
            this.editable = true;
            this.$set(this.defaultValue, val.id, val.value);
            this.scope.widgetValue.push(val.id);
        },
        getOptions: function(data) {
            this.loading = false;
            if (this.finish) {
                this.values = [];
            }
            this.finish = Object.keys(data.options).length < this.limit;
            console.log('finish', Object.keys(data.options).length, '<', this.limit, this.finish);
            var ids = [];
            var i;
            for (i = 0; i < this.values.length; i++) {
                ids.push(this.values[i].id);
            }
            for (var i in data.options) {
                ids.push(i);
                this.values.push({
                    id: i,
                    value: data.options[i]
                });
            }
            for (i in this.scope.widgetValue) {
                if (ids.indexOf(this.scope.widgetValue[i]) < 0) {
                    this.values.push({
                        id: this.scope.widgetValue[i],
                        value: this.scope.widgetValue[i] in this.defaultValue ? this.defaultValue[this.scope.widgetValue[i]] : this.scope.widgetValue[i]
                    });
                }
            }
        }
    },
});