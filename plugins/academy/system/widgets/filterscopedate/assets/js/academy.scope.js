Vue.component('filerScopeDate', {
    template:
        '<span class="filter-scope filerScopeText oc-icon-filter"\
            @click="show = true"\
            :class="{active: scope.widgetValue.before || scope.widgetValue.after}"\
            tabindex="-1"\
            @blur.capture="blur"\
            @focus.capture="focus">\
            <span class="filter-label">{{ scope.label }}</span>\
            <span class="filter-setting" v-if="scope.widgetValue.before || scope.widgetValue.after">\
                <template v-if="scope.widgetValue.before">{{ scope.widgetValue.before }}</template>\
                <template v-if="scope.widgetValue.before && scope.widgetValue.after">-</template>\
                <template v-if="scope.widgetValue.after">{{ scope.widgetValue.after }}</template>\
            </span>\
            <div class="control-popover placement-bottom in" v-if="show">\
                <div class="control-filter-popover">\
                    <div class="filter-search">\
                        <div class="input-with-icon right-align" @click="showModal = \'before\'">\
                            <i class="icon icon-calendar-o"></i>\
                            <i class="icon icon-times" style="margin-right: 20px;pointer-events:all" @click.stop="showModal = false;before = \'\'"></i>\
                            <input v-model="before" class="form-control align-right" style="padding-right: 53px!important">\
                            <div v-if="showModal === \'before\'" is="pikaday" v-model="before" format="YYYY-MM-DD" @input="showModal = false"></div>\
                        </div>\
                        <div class="input-with-icon right-align" @click="showModal = \'after\'">\
                            <i class="icon icon-calendar-o"></i>\
                            <i class="icon icon-times" style="margin-right: 20px;pointer-events:all" @click.stop="showModal = false;after = \'\'"></i>\
                            <input v-model="after" class="form-control align-right" style="padding-right: 53px!important">\
                            <div v-if="showModal === \'after\'" is="pikaday" v-model="after" format="YYYY-MM-DD" @input="showModal = false"></div>\
                        </div>\
                    </div>\
                    <div class="filter-buttons" style="text-align: right">\
                        <button class="btn btn-sm btn-primary oc-icon-check empty" @click="apply"></button>\
                    </div>\
                </div>\
            </div>\
        </span>',
    props: {
        scope: Object
    },
    data: function() {
        return {
            show: false,
            showModal: false,
            before: '',
            after: ''
        }
    },
    watch: {
        'scope.widgetValue': {
            handler: function(widgetValue) {
                this.before = widgetValue.before;
                this.after = widgetValue.after;
            },
            immediate: true
        }
    },
    computed: {
        alias: function() {
            return this.scope.widgetAlias;
        }
    },
    methods: {
        setValue: function(result, widget) {
            this.show = false;
            this.scope.widgetValue = widget.value;
            this.$emit('scopes');
        },
        apply: function() {
            this.on('setValue', {
                value: {
                    before: this.before,
                    after: this.after
                }
            })
        },
        clear: function() {
            this.before = '';
            this.after = '';
            this.apply();
        },
        focus: function() {
            console.log('focus is find');
            clearTimeout(this.timeout);
            this.show = true;
        },
        realBlur: function() {
            if (!this.showModal) {
                console.log('focus is last');
                this.show = false;
                this.showModal = false;
            }
            else {
                this.$el.focus();
            }
        },
        blur: function(event) {
            clearTimeout(this.timeout);
            this.timeout = setTimeout(this.realBlur, 0);
        },
    }
});