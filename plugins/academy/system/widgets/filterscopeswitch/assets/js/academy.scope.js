Vue.component('filerScopeSwitch', {
    template:
        '<span class="filter-scope filerScopeSwitch oc-icon-filter"\
            :class="{active: scope.widgetValue !== null}">\
            <span class="filter-label">{{ scope.label }}:</span>\
            <span is="check-widget" type="switch" v-model="scope.widgetValue" @input="change"></span>\
        </span>',
    props: {
        scope: Object
    },
    computed: {
        alias: function() {
            return this.scope.widgetAlias;
        }
    },
    methods: {
        setValue: function() {
            this.$emit('scopes');
        },
        change: function() {
            this.on('setValue', {
                value: this.scope.widgetValue
            })
        }
    },
});