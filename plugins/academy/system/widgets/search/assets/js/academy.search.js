Vue.component('AcademySystemWidgetsSearch', {
    template:
        '<div class="loading-indicator-container size-input-text">\
            <input\
                :placeholder="placeholder"\
                v-model="text"\
                @change="change"\
                class="form-control"\
                :class="cssclasses"\
                autocomplete="off"/>\
        </div>',
    props: {
        cssclasses: String,
        value: String,
        placeholder: String,
        alias: String
    },
    methods: {
        submit: function(data) {
            this.$parent.$parent.$slots.list[0].componentInstance.search();
        },
        change: function() {
            this.on('submit', {
                value: this.text
            })
        }
    },
    watch: {
        value: {
            handler: function(value) {
                this.text = value;
            },
            immediate: true
        }
    }
});