<?php namespace Academy\System\Widgets;

use Backend\Classes\WidgetBase;

class FilterScopeText extends WidgetBase
{
    public $scope;
    public $model;
    public $list;
    protected $defaultAlias = 'filerScopeText';

    public function init()
    {
        $this->fillFromConfig([
            'scope',
            'model',
            'list',
        ]);
        $this->scope->widgetAlias = $this->alias;
        $this->bindToController();
    }

    public function getColumnValue($value, $column, $record)
    {
        if (is_array($value) && count($value) == count($value, COUNT_RECURSIVE)) {
            $value = implode(', ', $value);
        }
        return htmlentities($value, ENT_QUOTES, 'UTF-8', false);
    }

    public function getValue()
    {
        return $this->getSession('value', []);
    }

    public function setValue($value)
    {
        $this->putSession('value', $value);
    }

    public function getType()
    {
        return $this->defaultAlias;
    }

    public function render()
    {
        $this->addCss('css/academy.scope.css', 'core');
        $this->addJs('js/academy.scope.js', 'core');
    }

    public function apply($query)
    {
        if (!count($value = $this->getValue())) {
            return;
        }
        $columnName = $this->scope->sqlSelect ? \DB::raw($this->scope->sqlSelect) : $query->getModel()->getTable() . '.' . $this->scope->columnName;
        if ($this->scope->relation) {
            $query->whereHas($this->scope->relation, function($query) use($value) {
                $model = $this->model->makeRelation($this->scope->relation);
                $query->whereIn($model->getTable() . '.' . $model->getKeyName(), $value);
            });
        }
        else {
            if (in_array('', $value)) {
                $query->where(function($query) use($value, $columnName) {
                    $query->whereIn($columnName, $value)
                        ->orWhere($columnName);
                });
            }
            else {
                $query->whereIn($columnName, $value);
            }
        }
        return $query;
    }

    private static function strip_tags($text)
    {
        $tagsForClear = ['head', 'style', 'script', 'object', 'embed', 'applet', 'noframes', 'noscript', 'noembed'];
        $wtfToClear = [
            ['address', 'blockquote', 'center', 'del'],
            ['div', 'h[1-9]', 'ins', 'isindex', 'p', 'pre'],
            ['dir', 'dl', 'dt', 'dd', 'li', 'menu', 'ol', 'ul'],
            ['table', 'th', 'td', 'caption'],
            ['form', 'button', 'fieldset', 'legend', 'input'],
            ['label', 'select', 'optgroup', 'option', 'textarea'],
            ['frameset', 'frame', 'iframe'],
        ];
        $fromClear = [];
        $toClear = [];
        foreach ($tagsForClear as $tag) {
            $fromClear[] = '@<' . $tag . '[^>]*?>.*?</' . $tag . '>@siu';
            $toClear[] = ' ';
        }
        foreach ($wtfToClear as $wtf) {
            $fromClear[] =  '@</?((' . implode(')|(', $wtf) . '))@iu';
            $toClear[] = "\n\$0";
        }

        $text = preg_replace($fromClear, $toClear, $text);
        return trim(strip_tags($text));
    }

    private function getAvailableOptions($scope, $searchQuery = null, $offset = 0, $limit = null, $valueFromIgnore = false)
    {
        if ($scope->relation) {
            if ($scope->valueFrom) {
                $scope->sqlSelect = $scope->valueFrom;
                $scope->columnName = $scope->valueFrom;
            }
            switch ($this->model->getRelationType($scope->relation)) {
                case 'belongsTo':
                    $fk = $this->model->getRelationDefinition($scope->relation);
                    $key = isset($fk['key']) ? $fk['key'] : snake_case($scope->relation).'_id';
                    $table = $this->model->getTable();
                    $relation = $this->model->makeRelation($scope->relation);
                    $relateionTable = $relation->getTable();
                    $relationKey = $relation->getKeyname();

                    $query = $this->list->getQueryOutScopes();
                    $query->getQuery()->orders = [];

                    $query
                        ->join(\DB::raw("(
                                SELECT
                                    `$relationKey`,
                                    $scope->sqlSelect AS `$scope->columnName`
                                FROM `$relateionTable`
                            )`{$scope->relation}_filter`"), "{$scope->relation}_filter.$relationKey", '=', "$table.$key")
                        ->select("$table.$key", "{$scope->relation}_filter.$scope->columnName")
                        ->groupBy("$table.$key")
                        ->orderBy("{$scope->relation}_filter.$scope->columnName");

                    if (is_array($limit)) {
                        $query->whereIn("$table.$key", $limit);
                    }
                    else {
                        $query->limit($limit)->offset($offset);
                    }
                    $result = $query->get()->lists($scope->columnName, $key);
                    break;
                default:
                    $relation = $this->model->makeRelation($scope->relation);
                    $result = $relation::select($relation->getKeyName())
                        ->orderBy(\DB::raw($scope->sqlSelect))
                        ->addSelect(\DB::raw("$scope->sqlSelect AS `$scope->columnName`"))
                        ->addSelect($relation->getKeyName());
                    if (is_array($limit)) {
                        $result->whereIn($relation->getKeyName(), $limit);
                    }
                    else {
                        $result->limit($limit)->offset($offset);
                    }
                    if (!is_null($searchQuery)) {
                        $result->searchWhere($searchQuery, [$scope->sqlSelect]);
                    }
                    $result = $result->lists($scope->columnName, $relation->getKeyName());
            }
        }
        else {
            if (!$valueFromIgnore && $scope->valueFrom) {
                if ($this->list->getColumn($scope->valueFrom)) {
                    return $this->getAvailableOptions($this->list->getColumn($scope->valueFrom), $searchQuery, $offset, $limit, true);
                }
                $scope->columnName = $scope->valueFrom;
                $scope->sqlSelect = null;
            }
            $columnName = $scope->sqlSelect ? \DB::raw($scope->sqlSelect) : $scope->columnName;
            $sqlSelect = $scope->sqlSelect ? \DB::raw("$scope->sqlSelect AS `$scope->columnName`") : $scope->columnName;
            $result = $this
                ->list
                ->getQueryOutScopes()
                ->groupBy($columnName)
                ->orderBy($columnName);

                if (is_array($limit)) {
                    $result->whereIn($columnName, $limit);
                }
                else {
                    $result->limit($limit)->offset($offset);
                }
            if (!is_null($searchQuery)) {
                $result->searchWhere($searchQuery, [$columnName]);
            }

            if ($scope->type == 'partial') {
                $return = [];
                foreach ($result->get() as $record) {
                    $return[$record->{$scope->columnName}] = self::strip_tags($this->list->getColumnValue($record, $scope, true));
                }
                $result = $return;
            }
            else {
                $query = $result;
                $result = $query->lists($scope->columnName, $scope->columnName);
                foreach ($result as $key => $value) {
                    if ($key != $value) {
                        $values = $query->get()->lists($scope->columnName);
                        $result = array_combine(array_keys($result), $values);
                        break;
                    }
                }
            }
        }

        unset($result[null]);
        unset($result['']);
        
        return $result;
    }

    public function onGetOptions()
    {
        return ['options' => $this->getAvailableOptions($this->scope, input('search'), input('offset', 0), input('limit'))];
    }

    public function onGetDefaultOptions()
    {
        return ['options' => $this->getAvailableOptions($this->scope, null, 0, input('keys', []))];
    }

    public function onSetValue()
    {
        $this->setValue((array) input('value'));
    }
}