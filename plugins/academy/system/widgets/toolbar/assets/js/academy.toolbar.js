Vue.component('AcademySystemWidgetsToolbar', {
    template:
        '<div class="AcademySystemWidgetsToolbar toolbar-widget control-toolbar">\
            <div class="toolbar-item toolbar-primary">\
                <slot name="controlPanel"></slot>\
            </div>\
            <slot name="search"></slot>\
        </div>',
    props: {
        alias: String,
    },
    data: function() {
        return {
            
        }
    },
    methods: {
        test: function() {
            this.$parent.rescopes();
        }
    }
});