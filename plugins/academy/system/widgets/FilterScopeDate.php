<?php namespace Academy\System\Widgets;

use Backend\Classes\WidgetBase;

class FilterScopeDate extends WidgetBase
{
    public $scope;
    public $model;
    public $list;
    protected $defaultAlias = 'filerScopeDate';

    public function init()
    {
        $this->fillFromConfig([
            'scope',
            'model',
            'list',
        ]);
        $this->scope->widgetAlias = $this->alias;
        $this->bindToController();
    }

    public function getValue()
    {
        return $this->getSession('value', []);
    }

    public function setValue($value)
    {
        $this->putSession('value', $value);
    }

    public function getType()
    {
        return $this->defaultAlias;
    }

    public function render()
    {
        $this->addJs('/plugins/academy/system/assets/js/vue-pikaday.js', 'core');
        $this->addCss('css/academy.scope.css', 'core');
        $this->addJs('js/academy.scope.js', 'core');
    }

    public function apply($query)
    {
        $value = $this->getValue();
        if (empty($value['before']) && empty($value['after'])) {
            return;
        }
        $table = $this->model->getTable();
        $columnName = $this->scope->sqlSelect ? \DB::raw($this->scope->sqlSelect) : "$table.{$this->scope->columnName}";
        if ($this->scope->relation) {
            $query->whereHas($this->scope->relation, function($query) use($value) {
                if (!empty($value['before'])) {
                    $query->where($this->scope->sqlSelect, '>=', $value['before']);
                }
                if (!empty($value['after'])) {
                    $query->where($this->scope->sqlSelect, '<=', $value['after']);
                }
            });
        }
        else {
            if (!empty($value['before'])) {
                $query->where($columnName, '>=', $value['before']);
            }
            if (!empty($value['after'])) {
                $query->where($columnName, '<=', $value['after']);
            }
        }
    }

    public function onSetValue()
    {
        $this->setValue((array) input('value'));
    }
}