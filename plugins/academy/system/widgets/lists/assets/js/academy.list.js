Vue.component('tree-to-list-widget', {
    props: {
        childrens: Function,
        getnode: Function
    },
    data: function() {
        return {
            h: null
        }
    },
    methods: {
        renderParentTemplate: function(template) {
            return Vue.compile(template)
                .render
                .call(this.$parent._renderProxy, this.h);
        },
        renderChildrens: function(result, record) {
            var childrens = this.childrens(record);
            if (childrens) {
                for (var i = 0; i < childrens.length; i++) {
                    result.push(this.getnode(childrens[i], this.renderParentTemplate, this.h));
                    this.renderChildrens(result, childrens[i]);
                }
            }
        }
    },
    render: function(h) {
        var childrens = [];
        this.h = h;
        this.renderChildrens(childrens);
        return h(this.$vnode.data.tag, childrens);
    }
});
Vue.component('AcademySystemWidgetsLists', {
    template:
        '<div class="list-widget" :class="[cssClass]">\
            <div class="out-filters">\
                <span\
                    :class=""\
                    v-if="column.value.invisible && scopeType(column)!==\'span\'"\
                    v-for="column, key in columns"\
                    :is="scopeType(column)"\
                    :scope="column.value"\
                    @scopes="on(\'rescopes\');loaded=true"\
                    ref="scope">\
                    {{ column.value.label }}\
                </span>\
            </div>\
            <div class="scrollable">\
                <span v-if="loaded" class="loader"></span>\
                <table v-else class="table data">\
                    <thead>\
                        <tr>\
                            <th v-if="showCheckboxes" class="list-checkbox">\
                                <div is="check-widget" v-model="checked" @input="checkAll()"></div>\
                            </th>\
                            <th v-for="column, key in columns"\
                                :colspan="headSpan(key)"\
                                v-if="!column.value.invisible"\
                                :style="{width: column.value.width}"\
                                :class="columnClass(column)">\
                                <span>\
                                <span :is="scopeType(column)" :scope="column.value" @scopes="on(\'rescopes\');loaded=true" ref="scope">{{ column.value.label }}</span>\
                                <a v-if="showSorting && column.value.sortable" class="sorter" @click="on(\'sort\', {\
                                    sortColumn: column.value.columnName,\
                                    page: pageCurrent,\
                                    direction: getDirection(column.value.columnName)\
                                })"></a>\
                                </span>\
                            </th>\
                            <th class="list-setup" v-if="setup.show">\
                                    <!--$n(\'loadSetup\')-->\
                                <a @click="showLoadSetup()"\
                                    :title="setup.title">\
                                </a>\
                            </th>\
                        </tr>\
                    </thead>\
                    <tbody is="tree-to-list-widget"\
                        :getnode="getNode"\
                        :childrens="getChildrens">\
                    </tbody>\
                </table>\
            </div>\
            <div class="list-footer" v-if="showPagination">\
                <div class="list-pagination">\
                    <div class="loading-indicator-container size-small pull-right">\
                        <div class="control-pagination">\
                            <div class="loading-indicator" v-if="showPaginateLoader"><span></span></div>\
                            <span class="page-iteration">\
                                {{ pageFrom }}-{{ pageTo}} / {{ recordTotal }}\
                            </span>\
                            <template v-if="pageLast > 1">\
                                <a v-if="pageCurrent > 1"\
                                    class="page-first"\
                                    @click="pageCurrent = 1">\
                                </a>\
                                <span v-else\
                                    class="page-first">\
                                </span>\
                                <a v-if="pageCurrent > 1"\
                                    class="page-back"\
                                    @click="pageCurrent--">\
                                </a>\
                                <span v-else\
                                    class="page-back">\
                                </span>\
                                <select\
                                    name="page"\
                                    class="form-control input-sm custom-select select-no-search"\
                                    v-model="pageCurrent">\
                                    <option v-for="page in pages">{{ page }}</option>\
                                </select>\
                                <a v-if="pageLast > pageCurrent"\
                                    class="page-next"\
                                    @click="pageCurrent++">\
                                </a>\
                                <span v-else\
                                    class="page-next">\
                                </span>\
                                <a v-if="pageLast > pageCurrent"\
                                    class="page-last"\
                                    @click="pageCurrent = pageLast">\
                                </a>\
                                <span v-else\
                                    class="page-last">\
                                </span>\
                            </template>\
                        </div>\
                    </div>\
                </div>\
            </div>\
            <div is="modal-widget" v-model="setup.recordsPerPage" v-if="setup.recordsPerPage">\
                <div class="modal-header">\
                    <h4 class="modal-title">{{ setup.title }}</h4>\
                </div>\
                <div class="modal-body">\
                    <p class="help-block before-field">{{ setup.help }}</p>\
                    <div class="control-simplelist with-checkboxes is-sortable">\
                        <ul>\
                            <li v-for="column, num in setup.columns" @mousedown="takeColumn($event, num)" :style="movedClass(num)">\
                                <div is="check-widget" :value="!column.invisible" @input="column.invisible = !$event">{{ column.column.value.label }}</div>\
                            </li>\
                        </ul>\
                    </div>\
                    <div class="form-group" v-if="showPagination">\
                        <label>{{ setup.per_page }}</label>\
                        <p class="help-block before-field">\
                            {{ setup.per_page_help }}\
                        </p>\
                        <select class="form-control custom-select select-no-search" v-model="setup.recordsPerPage">\
                            <option v-for="optionValue in perPageOptions">{{ optionValue }}</option>\
                        </select>\
                    </div>\
                    <template v-if="setup.groups.length">\
                    <p class="help-block before-field">{{ setup.group_help }}</p>\
                        <div class="control-simplelist with-checkboxes is-sortable">\
                            <ul>\
                                <li v-for="column, num in setup.groups" @mousedown="takeGroup($event, num)" :style="movedClassGroup(num)">\
                                    <div is="check-widget" v-model="column.active">{{ column.label }}</div>\
                                </li>\
                            </ul>\
                        </div>\
                    </template>\
                </div>\
                <div class="modal-footer">\
                    <button\
                        type="button"\
                        @click=applyOptions\
                        class="btn btn-primary">\
                        {{ setup.apply }}\
                    </button>\
                    <button\
                        type="button"\
                        @click="setup.recordsPerPage = 0"\
                        class="btn btn-default">\
                        {{ setup.cancel }}\
                    </button>\
                </div>\
            </div>\
        </div>',
    props: {
        alias: String
    },
    data: function() {
        return {
            columns: [],
            showCheckboxes: false,
            showTree: false,
            cssClass: false,
            pageCurrent: 1,
            recordsPerPage: 0,
            setup: {
                show: false,
                title: '',
                help: '',
                per_page: '',
                per_page_help: '',
                cancel: '',
                apply: '',
                recordsPerPage: 0,
                columns: [],
                moved: null,
                movedGroup: null,
                groups: [],
                sort: {
                    column: null,
                    direction: 'desc',
                },
            },
            showPagination: false,
            recordTotal: 0,
            pages: [],
            records: [],
            checked: false,
            searchText: '',
            value: [],
            showPaginateLoader: false,
            recordList: [],
            opened: [],
            loaded: true
        }
    },
    beforeMount: function() {
        window.AcademySystemWidgetsLists = this;
        this.on('refresh');
        document.addEventListener('mousemove', this.moveColumn);
        document.addEventListener('mouseup', this.dropColumn);
    },
    methods: {
        headSpan: function(key) {
            if (!this.showTree || key != this.firstVisibleColumnKey) {
                return false;
            }
            return this.maxLevel + 2;
        },
        search: function() {
            this.loaded = true;
            this.on('rescopes');
            for (var i in this.$refs.scope) {
                if (this.$refs.scope[i].refresh) {
                    this.$refs.scope[i].refresh();
                }
            }
        },
        rescopes: function(data) {
            this.loaded = false;
            for (var i = 0; i < this.columns.length; i++) {
                Vue.set(this.columns[i], 'loaded', !this.columns[i].value.invisible);
            }
            this.records = data.records;
            this.setParents(this.records);
            this.recordTotal = data.recordTotal;
        },
        scopeType: function(column) {
            if (column.value.widgetType) {
                return column.value.widgetType;
            }
            return 'span';
        },
        getDirection: function(columnName) {
            if (this.setup.sort.column != columnName || this.setup.sort.direction == 'asc') {
                return 'desc';
            }
            return 'asc';

        },
        movedClass: function(num) {
            if (this.setup.moved && num === this.setup.moved.num) {
                return {
                    '-khtml-user-select': 'none',
                    '-moz-user-select': 'none',
                    'user-select': 'none',
                    opacity: 0.5
                };
            }
        },
        movedClassGroup: function(num) {
            if (this.setup.movedGroup && num === this.setup.movedGroup.num) {
                return {
                    '-khtml-user-select': 'none',
                    '-moz-user-select': 'none',
                    'user-select': 'none',
                    opacity: 0.5
                };
            }
        },
        moveColumn: function(event) {
            if (this.setup.moved) {
                for(var i = 0; i < this.setup.moved.list.length; i++) {
                    var item = this.setup.moved.list[i];
                    var rect = item.getBoundingClientRect();
                    if (event.clientY < rect.top) {
                        break;
                    }
                }
                if (this.setup.moved.num == i) {
                    return;
                }
                if (this.setup.moved.num < i) {
                    i--;
                }
                var columns = this.setup.columns.splice(this.setup.moved.num, 1);
                this.setup.columns.splice(i, 0, columns[0]);
                this.setup.moved.num = i;
            }
            if (this.setup.movedGroup) {
                for(var i = 0; i < this.setup.movedGroup.list.length; i++) {
                    var item = this.setup.movedGroup.list[i];
                    var rect = item.getBoundingClientRect();
                    if (event.clientY < rect.top) {
                        break;
                    }
                }
                if (this.setup.movedGroup.num == i) {
                    return;
                }
                if (this.setup.movedGroup.num < i) {
                    i--;
                }
                var columns = this.setup.groups.splice(this.setup.movedGroup.num, 1);
                this.setup.groups.splice(i, 0, columns[0]);
                this.setup.movedGroup.num = i;
            }
        },
        takeColumn: function(event, num) {
            this.setup.moved = {
                num: num,
                list: event.currentTarget.parentNode.children
            };
        },
        takeGroup: function(event, num) {
            this.setup.movedGroup = {
                num: num,
                list: event.currentTarget.parentNode.children
            };
        },
        dropColumn: function() {
            this.setup.moved = null;
            this.setup.movedGroup = null;
        },
        applySetup: function(data) {
            //data. values || records || columns
            this.columns = [];
            var addVisible = false;
            for (var i in this.setup.columns) {
                if (!this.setup.columns[i].invisible && !this.setup.columns[i].column.loaded) {
                    addVisible = true;
                }
                this.columns[i] = {
                    name: this.setup.columns[i].column.name,
                    value: this.setup.columns[i].column.value,
                    loaded: this.setup.columns[i].column.loaded || !this.setup.columns[i].invisible
                }
                this.columns[i].value.invisible = this.setup.columns[i].invisible;
                if (!this.columns[i].value.invisible) {
                    this.columns[i].loaded = true;
                }
            }
            this.recordTotal = data.recordTotal;
            /*if (this.setup.recordsPerPage == this.recordsPerPage) {
                if (addVisible) {
                    this.records = data.records;
                    this.setParents(this.records);
                }
            }
            else */{
                this.pageCurrent = null;
                this.records = data.records;
                this.setParents(this.records);
            }
            this.recordsPerPage = this.setup.recordsPerPage;
            this.setup.recordsPerPage = 0;
        },
        applyOptions: function() {
            var columns = [];
            for (var i in this.setup.columns) {
                columns.push({
                    name: this.setup.columns[i].column.name,
                    invisible: this.setup.columns[i].invisible,
                    loaded: this.setup.columns[i].column.loaded
                });
            }
            groups = [];
            for (var i in this.setup.groups) {
                groups.push({
                    group: this.setup.groups[i].group,
                    active: this.setup.groups[i].active
                });
            }

            this.on('applySetup', {
                records_per_page: this.setup.recordsPerPage,
                page: this.setup.recordsPerPage == this.recordsPerPage ? this.pageCurrent : 1,
                groups: groups,
                columns: columns
            });
        },
        showLoadSetup: function() {
            var columns = [];
            for (var i = 0; i <  this.columns.length; i++) {
                columns[i] = {
                    column: this.columns[i],
                    invisible: this.columns[i].value.invisible
                };
            }
            this.setup.columns = columns;
            this.setup.recordsPerPage = this.recordsPerPage;
        },
        checkAll: function(record) {
            var childrens;
            if (record) {
                childrens = record.childrens;
                Vue.set(record, 'checked', this.checked);
                if (!childrens) {
                    return;
                }
            }
            else {
                childrens = this.records;
            }
            for (var i = 0; i < childrens.length; i++) {
                this.checkAll(childrens[i]);
            }
        },
        checkRecord: function(event) {
            alert(event);
        },
        expanded: function(data, params) {
            if (data.childrens) {
                Vue.set(params ,'childrens', data.childrens);
                this.setParents(params.childrens, params);
            }
            console.log('finish loaded');

            this.opened.splice(this.opened.indexOf(params), 1);
        },
        expandedRecord: function(record, refresh) {
            if (typeof record.values === 'string') {
                var next = record;
                var level = [];
                while (next = next.parent) {
                    level.push(next.id);
                }
            }
            else {
                var level = record.level;
            }
            if (refresh || (!record.expanded && typeof record.childrens === 'number')) {
                this.opened.push(record);
            }
            console.log('start loaded');
            this.on('expanded', {
                id: record.id,
                level: level,
                expanded: record.expanded = refresh || !record.expanded,
                childrens: refresh || typeof record.childrens === 'number'
            }, record);
        },
        linearFromArray: function(lines) {
            var result = [];
            for (var i = 0; i < lines.length; i++) {
                if (lines[i]) {
                    result.push('#00000000 ' + (i * 21 + 15) + 'px,#000,#00000000 '+ (i * 21 + 16) + 'px');
                }
            }
            if (!result.length) {
                return 'none!important';
            }
            return 'linear-gradient(to right,' + result.join(',') + ')!important';
        },
        getNode: function(record, byTemplate, node) {
            if (!record.checked) {
                Vue.set(record, 'checked', 0);
            }
            var self = this;
            var content = [];
            if (this.showCheckboxes) {
                content.push(node('td', {attrs: {class: 'list-checkbox nolink'}}, [
                    node('check-widget', {
                        props: {
                            value: record.checked
                        },
                        on: {
                            input: function(event) {
                                self.checked = false;
                                record.checked = event;
                            }
                        }
                    })
                ]))
            }
            if (this.showTree) {
                if (record.level > 0) {
                    content.push(node('td', {
                        class: 'nolink tree-icon clear',
                        attrs: {
                            colSpan: record.level > 1? record.level : false
                        },
                        style: {
                            "background": this.linearFromArray(record.descendants)
                        }
                    }));
                }
                var child = [];
                if ((typeof record.childrens === 'object' && Object.keys(record.childrens).length)
                    || (typeof record.childrens === 'number' && record.childrens > 0)
                ) {
                    child.push(node('i', {
                        attrs: {
                            class: (record.expanded ? 'icon-minus-square-o' : 'icon-plus-square-o')
                        },
                        on: {
                            click: function() {
                                self.expandedRecord(record);
                            }
                        }
                    }));
                }
                content.push(node('td', {
                    class: ['click','nolink','tree-icon']
                }, child));
            }
            if (typeof record.values !== 'object') {
                content.push(node('td', {
                    attrs: {
                        colSpan: this.visibleColumn + this.maxLevel - record.level > 1 ? this.visibleColumn + this.maxLevel - record.level : undefined
                    },
                    class: [{'loaded': self.opened.indexOf(record) >= 0}, 'group'],
                    on: {
                        click: function() {
                            self.expandedRecord(record);
                        }
                    }
                }, [
                    record.values,
                    node('span', {
                        class: 'loader',
                        on: {
                            click: function(event) {
                                self.expandedRecord(record, true);
                                event.stopPropagation();
                            }
                        }
                    })
                ]));
            }
            else {
                var first = true;
                this.columns.forEach(function(column, index) {
                    if (column.value.invisible) {
                        return;
                    }
                    var on;
                    var classes = ['list-cell-index-' + index, 'list-cell-type-' + column.value.type, column.value.cssClass, 'list-cell-name-' + column.value.columnName];
                    if (record.values[column.name].function) {
                        on = {
                            click: function(event) {
                                eval(record.values[column.name].function);
                            }
                        };
                    }
                    else if (column.value.clickable) {
                        on = {
                            click: function(event) {
                                eval(typeof column.value.clickable === 'string' ? column.value.clickable : record.function);
                            }
                        };
                    }
                    else {
                        on = {};
                        classes.push('nolink');
                    }
                    content.push(node('td', {
                        class: classes,
                        on: on,
                        domProps: {
                            innerHTML: record.values[column.name].value
                        },
                        attrs: {
                            colSpan: (first && this.showTree && this.maxLevel > record.level) ? this.maxLevel - record.level + 1 : undefined
                        }
                    }));
                    first = false;
                });
            }
            if (this.setup.show) {
                content.push(node('td', {attrs: {class: 'list-setup'}}));
            }
            var classes = [record.class, 'rowlink'];
            if (this.showTree) {
                classes.push('list-tree-level-' + record.level);
            }
            if (record.checked) {
                classes.push('active');
            }
            if (record.expanded) {
                classes.push('expanded');
            }
            return node('tr', {
                class: classes,
                //key: record.id
            }, content);
        },
        getChildrens: function(record) {
            if (!record) {
                return this.records;
            }
            if (record.expanded) {
                return record.childrens;
            }
            return [];
        },
        setParents: function(records, parent) {
            for (var i = 0; i < records.length; i++) {
                records[i].parent = parent;
                records[i].descendants = [];
                if (parent) {

                    //parent.descendants.push(records.length);
                    for (var i2 = 0; i2 < parent.descendants.length; i2++) {
                        records[i].descendants[i2] = parent.descendants[i2];
                    }
                    records[i].descendants.push(i < records.length - 1);
                }
                if (records[i].expanded) {
                    this.expandedRecord(records[i], true);
                }
                if (typeof records[i].childrens === 'object'
                    && records[i].childrens !== null
                ) {
                    this.setParents(records[i].childrens, records[i]);
                }
            }
        },
        refreshOne: function(data, record) {
            record.values = data.values;
        },
        refresh: function(data) {
            this.loaded = false;
            this.columns = data.columns;
            for (var i = 0; i < this.columns.length; i++) {
                Vue.set(this.columns[i], 'loaded', !this.columns[i].value.invisible);
            }
            this.showCheckboxes = data.showCheckboxes;
            this.showTree = data.showTree;
            this.cssClass = data.cssClass;
            this.showSorting = data.showSorting;
            this.showPagination = data.showPagination
            this.recordTotal = data.recordTotal;
            this.records = data.records;
            this.setParents(this.records);
            this.recordsPerPage =  parseInt(data.recordsPerPage);

            this.setup.show = data.showSetup;
            this.setup.title = data.setup_title;
            this.setup.help = data.setup_help;
            this.setup.group_help = data.setup_group_help;
            this.setup.per_page = data.records_per_page;
            this.setup.per_page_help = data.records_per_page_help;
            this.setup.apply = data.apply;
            this.setup.sort = data.sort;
            this.setup.cancel = data.cancel;
            this.setup.groups = data.groups;
        },
        columnClass: function(column) {
            var clases = ['list-cell-name-' + column.value.columnName, 'list-cell-type-' + column.value.type];
            if (this.showSorting && column.value.sortable) {
                if (column.name === this.setup.sort.column) {
                    clases.push('active sort-' + this.setup.sort.direction);
                }
                else {
                    clases.push('sort-desc');
                }
            }
            return clases;
        },
        sort: function(data, sortOptions) {
            for (var i = 0; i < this.columns.length; i++) {
                Vue.set(this.columns[i], 'loaded', !this.columns[i].value.invisible);
            }
            this.records = data.records;
            this.setParents(this.records);
            this.setup.sort.direction = sortOptions.direction;
            this.setup.sort.column = sortOptions.sortColumn;
        },
        paginate: function(data) {
            this.showPaginateLoader = false;
            for (var i = 0; i < this.columns.length; i++) {
                Vue.set(this.columns[i], 'loaded', !this.columns[i].value.invisible);
            }
            this.records = data.records;
            this.setParents(this.records);
        },
        maxLevelRecords: function(records) {
            var level = 0;
            for (var i in records) {
                level = Math.max(records[i].level, level);
                if (typeof records[i].childrens === 'object' && records[i].expanded) {
                    level = Math.max(this.maxLevelRecords(records[i].childrens), level);
                }
            }
            return level;
        }
    },
    watch: {
        records: {
            handler: function(records) {
                value = [];
                for (i = 0; i < records.length; i++) {
                    if (records[i].checked) {
                        value.push(records[i].id);
                    }
                }
                //this.$emit('input', value);
                this.value = value;
            },
            deep: true
        },
        pageLast: function() {
            this.pages = [];
            for (var i = 1; i <= this.pageLast; i++) {
                this.pages.push(i);
            }
        },
        pageCurrent: function(pageCurrent, prev) {
            if (pageCurrent !== null && prev !== null) {
                this.showPaginateLoader = true;
                this.on('paginate', {page: pageCurrent});
            }
            if (pageCurrent === null) {
                this.pageCurrent = 1;
            }
        }
    },
    computed: {
        firstVisibleColumnKey: function() {
            for (key in this.columns) {
                if (!this.columns[key].value.invisible) {
                    return key;
                }
            }
        },
        visibleColumn: function() {
            result = 0;
            for (var i in this.columns) {
                if (!this.columns[i].value.invisible) {
                    result++;
                }
            }
            return result;
        },
        maxLevel: function() {
            if (!this.showTree) {
                return 0;
            }
            return this.maxLevelRecords(this.records);
        },
        pageFrom: function() {
            return (this.pageCurrent - 1) * (this.recordsPerPage) + 1;
        },
        pageTo: function() {
            return Math.min(this.pageCurrent * this.recordsPerPage, this.recordTotal);
        },
        pageLast: function() {
            return Math.ceil(this.recordTotal / this.recordsPerPage);
        },
        perPageOptions: function() {
            var perPageOptions = [20, 40, 80, 100, 120];
            if (-1 === perPageOptions.indexOf(this.recordsPerPage)) {
                perPageOptions.push(this.recordsPerPage);
            }
            perPageOptions.sort(function(a, b) {
                return a > b;
            });
            return perPageOptions;
        }
    }
});