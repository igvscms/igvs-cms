Vue.component('AcademySystemWidgetsFilter', {
    template:
        '<div class="control-filter">\
            <div v-for="scope in scopes"\
                :is="scope.type"\
                :scope="scope"\
                @scopes="on(\'rescopes\')">\
                {{ scope.label }}\
            </div>\
        </div>',
    data: function() {
        return {
            scopes: []
        };
    },
    beforeMount: function() {
        this.on('refresh');
    },
    methods: {
        refresh: function(data) {
            this.scopes = data.scopes;
        }
    }
});