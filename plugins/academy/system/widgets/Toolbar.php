<?php namespace Academy\System\Widgets;

use Backend\Widgets\Toolbar as WidgetBase;

class Toolbar extends WidgetBase
{
    public function getComponentName()
    {
        return str_replace('\\', '', get_class($this));
    }

    public function render()
    {
        $this->addCss('css/academy.toolbar.css', 'core');
        $this->addJs('js/academy.toolbar.js', 'core');
        return Parent::render();
    }

    public function init()
    {
        $this->fillFromConfig([
            'buttons',
            'search',
        ]);
        if (isset($this->search)) {

            if (is_string($this->search)) {
                $searchConfig = $this->makeConfig(['partial' => $this->search]);
            }
            else {
                $searchConfig = $this->makeConfig($this->search);
            }

            $searchConfig->alias = $this->alias . 'Search';
            if (!isset($searchConfig->widget)) {
                $widget = 'Academy\System\Widgets\Search';
            }
            else {
                $widget = $searchConfig->widget;
            }
            $this->searchWidget = $this->makeWidget($widget, $searchConfig);
            $this->searchWidget->bindToController();
        }
    }
}