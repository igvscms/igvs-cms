<?php namespace Academy\System\Widgets;

use Backend\Widgets\Search as WidgetBase;

class Search extends WidgetBase
{
    public function getComponentName()
    {
        return str_replace('\\', '', get_class($this));
    }

    public function render()
    {
        $this->addCss('css/academy.search.css', 'core');
        $this->addJs('js/academy.search.js', 'core');
        return Parent::render();
    }

    public function onSubmit()
    {
        $this->setActiveTerm(input('value'));
    }
}