<?php namespace Academy\System\Widgets;

use Db;
use App;
use Html;
use Lang;
use Input;
use Backend;
use DbDongle;
use Carbon\Carbon;
use October\Rain\Html\Helper as HtmlHelper;
use October\Rain\Router\Helper as RouterHelper;
use System\Helpers\DateTime as DateTimeHelper;
use System\Classes\PluginManager;
use Backend\Classes\ListColumn;
use Backend\Classes\WidgetBase;
use October\Rain\Database\Model;
use ApplicationException;
use DateTime;
use October\Rain\Database\Builder;

use Backend\Classes\BackendController;

/**
 * List Widget
 * Used for building back end lists, renders a list of model objects
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Lists extends WidgetBase
{
    public $columns;
    public $model;
    public $recordUrl;
    public $recordOnClick;
    public $noRecordsMessage = 'backend::lang.list.no_records';

    public $recordsPerPage;
    public $showSorting = true;
    public $defaultSort;
    public $showCheckboxes = false;
    public $showSetup = false;
    public $showTree = false;
    public $treeExpanded = false;
    public $showPagination = 'auto';
    public $customViewPath;
    public $optionsByCurrentPage = false;
    protected $defaultAlias = 'list';
    protected $allColumns;
    protected $columnOverride;
    protected $visibleColumns;
    protected $records;
    protected $currentPageNumber;
    protected $searchTerm;
    protected $searchMode;
    protected $searchScope;
    protected $filterCallbacks = [];
    protected $sortableColumns;
    protected $sortColumn;
    protected $sortDirection;
    public $cssClasses = [];
    private $filterNotApply = false;
    private $widgets = [];
    private $groups = [];
    private $groupsValue = [];
    private $groupsAllValue = [];
    private $recordsTotal = 0;

    public function getComponentName()
    {
        return str_replace('\\', '', get_class($this));
    }

    public function init()
    {
        $this->fillFromConfig([
            'columns',
            'model',
            'recordUrl',
            'recordOnClick',
            'noRecordsMessage',
            'recordsPerPage',
            'showSorting',
            'defaultSort',
            'showCheckboxes',
            'showSetup',
            'showTree',
            'treeExpanded',
            'showPagination',
            'customViewPath',
            'optionsByCurrentPage'
        ]);
        /*
         * Configure the list widget
         */
        $this->recordsPerPage = $this->getSession('per_page', $this->recordsPerPage);

        if ($this->showPagination == 'auto') {
            $this->showPagination = $this->recordsPerPage && $this->recordsPerPage > 0;
        }

        if ($this->customViewPath) {
            $this->addViewPath($this->customViewPath);
        }

        $this->validateModel();
        $this->getColumns();
        $this->getGroups();
    }

    public function getQueryOutScopes()
    {
        static $queryOutScopes;
        if (!$queryOutScopes) {
            $queryOutScopes = $this->prepareModel(false);
        }
        return $queryOutScopes;
    }

    public function applyAllScopesToQuery($query)
    {
        foreach ($this->getColumns() as $scopeName => $scope) {
            if (isset($this->widgets[$scopeName])) {
                $this->widgets[$scopeName]->apply($query);
            }
        }
        return $query;
    }

    protected function loadAssets()
    {
        //то что подключится после аякса
    }

    public function render()
    {
        foreach ($this->widgets as $widget) {
            $widget->render();
        }
        $this->addCss('css/academy.list.css', 'core');
        $this->addJs('js/academy.list.js', 'core');
        return '<div is="'. $this->getComponentName() .'" alias="'. $this->alias .'"></div>';
    }

    private static function ifnull(&$value, $default = null)
    {
        return isset($value) ? $value : $default;
    }

    public function getCountByGroup($group, $scope = null)
    {
        $query = $this->prepareModel();
        if (isset($scope)) {
            $condition = is_string($scope) ? $scope . 'Count' : $scope['count'];
            return $query->$condition($group);
        }
        $realModel = $query->getModel();
        $relationType = $realModel->getRelationType($group);
        switch ($relationType) {
            case 'belongsToMany':
                $relation = $query->getRelation($group);
                $table = $relation->getTable();
                $field = $relation->getForeignKey();
                $keys = implode(',', $query->lists($realModel->getKeyName()));
                if ($keys === '') {
                    return 0;
                }
                $result = \DB::select("
                    SELECT
                        MAX(`count`)`count`
                    FROM(
                        SELECT
                            COUNT(*)`count`
                        FROM $table
                        WHERE $field in ($keys)
                        GROUP BY $field
                    )`t`
                ");
                return $result[0]->count;
            case 'belongsTo':
            default:
                return 1;
        }
    }

    private function getQueryByGroups(array $groups = [])
    {
        $query = $this->prepareModel();
        $realModel = $query->getModel();
        $groupsOptions = $this->getGroups();
        foreach ($groups as $group => $values) {
            $values = array_unique($values);
            if (isset($groupsOptions[$group]->scopes)) {
                $condition = is_string($groupsOptions[$group]->scopes) ? $groupsOptions[$group]->scopes . 'Condition' : $groupsOptions[$group]->scopes['condition'];
                $query->$condition($values, $group);
                continue;
            }
            $relationType = $realModel->getRelationType($group);
            switch ($relationType) {
                case 'belongsToMany':
                    if (!in_array(null, $values, true)) {
                        $relation = $query->getRelation($group);
                        $related = $relation->getRelated();
                        $table = $related->getTable();
                        $field = $table . '.' . $related->getKeyName();
                        $query->whereHas($group, function($query) use($group, $field, $values) {
                                $query->whereIn($field, $values);
                            }, '>=', count($values));
                        break;
                    }
                    if (count($values) === 1) {
                        $query->doesnthave($group);
                    }
                    else {
                        $query->whereRaw('0');
                    }
                    break;
                case 'belongsTo':
                    $fk = $realModel->getRelationDefinition($group);
                    $field = \DB::raw($realModel->getTable() . '.' . Self::ifnull($fk['key'], snake_case($group).'_id'));
                    if ($values[0] === null) {
                        $query->whereNull($field);
                    }
                    else {
                        $query->where($field, $values[0]);
                    }
                    break;
                default:
                    $field = \DB::raw(Self::ifnull($groupsOptions[$group]->select, $group));
                    if ($values[0] === null) {
                        $query->whereNull($field);
                    }
                    else {
                        $query->where($field, $values[0]);
                    }
            }
        }
        return $query;
    }

    private function getRecordValues($record)
    {
        $values = [];
        foreach ($this->getColumns() as $name => $column) {
            if (empty($column->invisible)) {
                $values[$name] = (object) [
                    'value' => $this->getColumnValue($record, $column),
                    'function' => $this->getColumnOnClick($column, $record),
                ];
            }
        }
        return $values;
    }

    private static function getSubQuery($parentQuery, $query, $alias, \Closure $condition = null, $join = null)
    {
        if ($query instanceof Builder) {
            $bindings = $query->getBindings();
            $query = $query->toSql();
        }
        elseif (is_string($query)) {
            $bindings = [];
        }
        else {
            throw new \InvalidArgumentException;
        }
        $parentQuery->addBinding($bindings);
        if (!$condition) {
            $condition = function($query) {};
            $join = 'from';
        }
        $alias = $parentQuery->getGrammar()->wrap($alias);
        switch ($join) {
            case null:
            case 'inner':
            case 'left':
            case 'cross':
                $parentQuery->join(\DB::raw("($query) as $alias"), $condition, null, null, $join);
                break;
            case 'from':
            default:
                $parentQuery->from(\DB::raw("($query) as $alias"));
                $condition($parentQuery);
        }
        return $parentQuery;
    }

    private function getRecordsByGroups($groups, array &$pagination = [], $level = 0)
    {
        /*
            возвращаем массив рекордсов
            если ферст левел, устанавливаем тотал рекорд
        */
        $autoLoad = true;

        $groupsValue = $this->getGroupsValue();
        $result = [];
        $subPagination = [];
        if (!isset($groupsValue[$level])) {
            $query = is_array($groups) ? $this->getQueryByGroups($groups) : $groups;
            if (!empty($pagination['recordsPerPage'])) {
                $currentPageNumber = max(1, isset($pagination['currentPageNumber']) ? (int) $pagination['currentPageNumber'] : 1);
                $records = $query->paginate($pagination['recordsPerPage'], $currentPageNumber);
                $pagination['recordsTotal'] = $records->total();
            }
            else {
                $records = $query->get();
            }
            foreach ($records as $record) {
                if (!$this->showTree || !is_bool($this->showTree)) {
                    $childrens = 0;
                    $isTreeNodeExpanded = false;
                }
                else {
                    $subQuery = $this->prepareModel()
                        ->getNested($record->getKey());

                    if (($isTreeNodeExpanded = $this->isTreeNodeExpanded($record)) && !$autoLoad) {
                        $childrens = $this->getRecordsByGroups($subQuery, $subPagination, $level + 1);
                    }
                    else {
                        $childrens = $subQuery->count();
                    }
                }
                $result[] = (object) [
                    'expanded' => $isTreeNodeExpanded,
                    'id' => $record->getKey(),
                    'values' => $this->getRecordValues($record),
                    'level' => $level,
                    'class' => $this->getRowClass($record),
                    'childrens' => $childrens,
                    'function' => $this->getRecordOnClick($record),
                ];
            }
            return $result;
        }
        $query = $this->getQueryByGroups($groups);
        $group = $groupsValue[$level];
        $groupsOptions = $this->getGroups();
        $values = isset($groups[$group]) ? array_unique($groups[$group]) : [];
        if (isset($groupsOptions[$group]->scopes)) {
            $grouped = is_string($groupsOptions[$group]->scopes) ? $groupsOptions[$group]->scopes . 'Group' : $groupsOptions[$group]->scopes['group'];
            $records = $query->$grouped($values, $group, $pagination);
            foreach ($records as $record) {
                $subGroups = $groups;
                $subGroups[$group][] = $record->field;
                if (!($isTreeNodeExpanded = $this->isTreeNodeExpanded($subGroups)) || $autoLoad) {
                    $childrens = (int) $record->count;
                }
                else {
                    $childrens = $this->getRecordsByGroups($subGroups, $subPagination, $level + 1);
                }
                $result[] = (object) [
                    'expanded' => $isTreeNodeExpanded,
                    'id' => $record->field,
                    'values' => $record->title . ' ('. (is_array($childrens) ? count($childrens) : $childrens) .')',
                    'level' => $level,
                    'class' => $this->getRowClass($subGroups),
                    'childrens' => $childrens,
                    'function' => null,
                ];
            }
            return $result;
        }
        $realModel = $query->getModel();
        $relationType = $realModel->getRelationType($group);
        switch ($relationType) {
            case 'belongsToMany':
                $relation = $query->getRelation($group);
                $related = $relation->getRelated();
                $table = $related->getTable();
                $field = $related->getKeyName();
                $tableField = "$table.$field";
                $relationQuery = $relation->getQuery();
                $keys = $query->lists($realModel->getKeyName());

                if (count($values)) {
                    $relationQuery->whereNotIn($tableField, $values);
                }
                $select = Self::ifnull($groupsOptions[$group]->select, $tableField);
                $relationQuery
                    ->select($tableField)
                    ->selectRaw($select . ' `title`')
                    ->selectRaw('count(*)`count`')
                    ->groupBy($tableField)
                    ->whereIn($relation->getForeignKey(), $keys);

                $doesnthaveGroup = $query->doesnthave($group)
                    ->addSelect(\DB::raw('count(*)`count`'))
                    ->first();

                if (!empty($pagination['recordsPerPage'])) {
                    $currentPageNumber = max(1, isset($pagination['currentPageNumber']) ? (int) $pagination['currentPageNumber'] : 1);
                    $records = $relationQuery->paginate($pagination['recordsPerPage'], $currentPageNumber);
                    $pagination['recordsTotal'] = $records->total() + !!$doesnthaveGroup->count;
                }
                else {
                    $records = $relationQuery->get();
                }
                foreach ($records as $record) {
                    $subGroups = $groups;
                    $subGroups[$group][] = $record->$field;
                    if (!($isTreeNodeExpanded = $this->isTreeNodeExpanded($subGroups)) || $autoLoad) {
                        $childrens = (int) $record->count;
                    }
                    else {
                        $childrens = $this->getRecordsByGroups($subGroups, $subPagination, $level + 1);
                    }
                    $result[] = (object) [
                        'expanded' => $isTreeNodeExpanded,
                        'id' => $record->$field,
                        'values' => $record->title . ' ('. (is_array($childrens) ? count($childrens) : $childrens) .')',
                        'level' => $level,
                        'class' => $this->getRowClass($subGroups),
                        'childrens' => $childrens,
                        'function' => null,
                    ];
                }
                if ($doesnthaveGroup->count
                    && (empty($pagination['recordsPerPage'])
                        || count($result) < $pagination['recordsPerPage'])
                ) {
                    $subGroups = $groups;
                    $subGroups[$group][] = null;
                    if (!($isTreeNodeExpanded = $this->isTreeNodeExpanded($subGroups)) || $autoLoad) {
                        $childrens = (int) $doesnthaveGroup->count;
                    }
                    else {
                        $childrens = $this->getRecordsByGroups($subGroups, $subPagination, $level + 1);
                    }
                    $result[] = (object) [
                        'expanded' => $isTreeNodeExpanded,
                        'id' => null,
                        'values' => Self::ifnull($groupsOptions[$group]->default, $group) . ' ('. (is_array($childrens) ? count($childrens) : $childrens) .')',
                        'level' => $level,
                        'class' => $this->getRowClass($subGroups),
                        'childrens' => $childrens,
                        'function' => null,
                    ];
                }
                return $result;
            case 'belongsTo':

                /*
                select
                    `count`,
                    `topic_id`,
                    `title`
                from(
                    select
                        `academy_tasks`.`topic_id`,
                        concat(`igvs_courses_categories`.`code`, ': ', `igvs_courses_categories`.`name`) `title`,
                        count(*)`count`
                    from `academy_tasks`
                    left join `igvs_courses_categories`
                        on `igvs_courses_categories`.`id` = `academy_tasks`.`topic_id`
                    where `igvs_courses_categories`.`deleted_at` is null
                        and `academy_tasks`.`project_id` = 2
                    group by `academy_tasks`.`topic_id`
                ) as `query`
                order by `title` asc
                limit 20
                */



                $relationObject = $realModel->$group();
                $relationModel = $relationObject->getRelated();
                $fk = $realModel->getRelationDefinition($group);

                $field = Self::ifnull($fk['key'], snake_case($group).'_id');

                $tableField = $realModel->getTable() . '.' . $field;

                $key = $relationModel->getKeyname();

                $select = Self::ifnull($groupsOptions[$group]->select);
                $default = Self::ifnull($groupsOptions[$group]->default, $group);

                $subQuery = $relationModel::where($key, \DB::raw($tableField))->selectRaw($select)->getQuery();

                $relationTable = $relationModel->getTable();

                $query->getQuery()->orders = null;


                //`academy_tasks`.`topic_id`, `igvs_courses_categories`.`title`, count(*)`count` - сейчас
                /*
                    `academy_tasks`.`topic_id`,
                    concat(`igvs_courses_categories`.`code`, ': ', `igvs_courses_categories`.`name`) `title`,
                    count(*)`count`
                    
                    так надо
                */
                $query->select(
                    $tableField,
                    \DB::raw("$select `title`"),
                    \DB::raw('count(*)`count`')
                )
                ->leftJoin($relationTable, "$relationTable.$key", '=', $tableField)
                ->groupBy($tableField);

                $query = self::getSubQuery(\DB::query(), $query, 'query')
                    ->select('count', $field, 'title')
                    ->orderBy('title');

                if (!empty($pagination['recordsPerPage'])) {
                    $currentPageNumber = max(1, isset($pagination['currentPageNumber']) ? (int) $pagination['currentPageNumber'] : 1);
                    $pagination['recordsTotal'] = $query->count();//$records->total();
                    $records = $query->limit($pagination['recordsPerPage'] * $currentPageNumber, $pagination['recordsPerPage'])->get();
                    //$records = $query->paginate($pagination['recordsPerPage'], $currentPageNumber);
                }
                else {
                    $records = $query->get();
                }
                foreach ($records as $record) {
                    $subGroups = $groups;
                    $subGroups[$group][] = $record->$field;
                    if (!($isTreeNodeExpanded = $this->isTreeNodeExpanded($subGroups)) || $autoLoad) {
                        $childrens = (int) $record->count;
                    }
                    else {
                        $childrens = $this->getRecordsByGroups($subGroups, $subPagination, $level + 1);
                    }
                    if (is_null($select) || is_null($record->title)) {
                        $label = $default . ($record->$field ? ' №' . $record->$field : '');
                    }
                    else {
                        $label = $record->title;
                    }
                    $result[] = [
                        'expanded' => $isTreeNodeExpanded,
                        'id' => $record->$field,
                        'values' => $label . ' ('. (is_array($childrens) ? count($childrens) : $childrens) .')',
                        'level' => $level,
                        'class' => $this->getRowClass($subGroups),
                        'childrens' => $childrens,
                        'function' => null,
                    ];
                }
                return $result;
            default:
                $select = $group = Self::ifnull($groupsOptions[$group]->select, $group);
                $query->addSelect(\DB::raw('count(*)`count`'))
                    ->addSelect(\DB::raw($select . ' `title`'))
                    ->groupBy(\DB::raw($group));

                if (!empty($pagination['recordsPerPage'])) {
                    $currentPageNumber = max(1, isset($pagination['currentPageNumber']) ? (int) $pagination['currentPageNumber'] : 1);
                    $records = $query->paginate($pagination['recordsPerPage'], $currentPageNumber);
                    $pagination['recordsTotal'] = $records->total();
                }
                else {
                    $records = $query->get();
                }
                $default = Self::ifnull($groupsOptions[$group]->default, $group);
                foreach ($records as $record) {
                    $subGroups = $groups;
                    $subGroups[$group][] = $record->title;
                    if (!($isTreeNodeExpanded = $this->isTreeNodeExpanded($subGroups)) || $autoLoad) {
                        $childrens = (int) $record->count;
                    }
                    else {
                        $childrens = $this->getRecordsByGroups($subGroups, $subPagination, $level + 1);
                    }
                    $title = empty($record->title) ? $default : $record->title;
                    $result[] = [
                        'expanded' => $isTreeNodeExpanded,
                        'id' => $record->title,
                        'values' => $title . ' ('. (is_array($childrens) ? count($childrens) : $childrens) .')',
                        'level' => $level,
                        'class' => $this->getRowClass($subGroups),
                        'childrens' => $childrens,
                        'function' => null,
                    ];
                }
                return $result;
        }
    }

    protected function getRecords()
    {
        $model = $this->prepareModel();

        if (!is_bool($this->showTree)) {
            return $this->records = $model;
        }
        if ($this->showTree) {
            $records = $model->getNested();
        }
        if ($this->showPagination) {
            $records = $model->paginate($this->recordsPerPage, $this->currentPageNumber);
        }
        else {
            $records = $model->get();
        }
        return $this->records = $records;
    }

    public function onRefreshOne()
    {
        $record = $this->prepareModel()->where($this->model->getTable() .'.' . $this->model->getKeyName(), input('id'))->first();
        return [
            'values' => $this->getRecordValues($record)
        ];
    }

    public function onExpanded()
    {
        $level = input('level');
        $expanded = input('expanded');
        $needChildren = $expanded && input('childrens');
        $pagination = [];
        if (is_array($level)) {
            $groupsValue = $this->getGroupsValue();
            $level = array_reverse($level);
            $level[] = input('id');
            $groups = [];
            foreach ($level as $num => $value) {
                $groups[$groupsValue[$num]][] = $value;
            }
            $level = count($level);
            $record = $groups;
            if ($needChildren) {
                $query = $groups;
            }
        }
        else {
            $level++;
            $class = $this->model;
            $record = $class::find(input('id'));
            if ($needChildren) {
                $query = $this->prepareModel()
                    ->getNested($record->getKey());
            }
        }
        $this->setTreeNodeExpanded($record, $expanded);
        if ($needChildren) {
            return [
                'childrens' => $this->getRecordsByGroups($query, $pagination, $level)
            ];
        }
    }

    public function getGroupsAllValue()
    {
        if (count($this->groupsAllValue)) {
            return $this->groupsAllValue;
        }
        $groups = $this->getGroups();
        $counts = [];
        $active = [];
        foreach ($groups as $group => $option) {
            $counts[$group] = max(0, $option->count);
            $active[$group] = max(0, (int) $option->active);
        }
        $this->groupsAllValue = [];
        $sessionGroups = $this->getSession('group');
        $hasSession = !is_null($sessionGroups);
        if ($hasSession) {
            foreach ($sessionGroups as $next) {
                if (!is_array($next)) {
                    $next = [
                        'active' => true,
                        'group' => $next,
                    ];
                }
                if (!empty($counts[$next['group']])) {
                    if ($next['active'] && !empty($active[$next['group']])) {
                        $active[$next['group']]--;
                    }
                    $this->groupsAllValue[] = (object) $next;
                    $counts[$next['group']]--;
                }
            }
        }
        foreach ($counts as $group => $count) {
            for ($i = 0; $i < $count; $i++) {
                if ($act = $active[$group] > 0) {
                    $active[$group]--;
                }
                $this->groupsAllValue[] = (object) [
                    'active' => !$hasSession && $act,
                    'group' => $group,
                ];
            }
        }
        return $this->groupsAllValue;
    }

    public function getGroupsValue()
    {
        if (count($this->groupsValue)) {
            return $this->groupsValue;
        }
        $this->groupsValue = [];
        foreach ($this->getGroupsAllValue() as $option) {
            if ($option->active) {
                $this->groupsValue[] = $option->group;
            }
        }
        return $this->groupsValue;
    }

    public function getGroups()
    {
        /*return [
            'parent' => (object) [
                'label' => 'Группировать по родителю',
                'default' => 'Родитель не выбран',
                'select' => 'title',
                'active' => true,//будет числом ограниченным количеством(сколько раз фильтровать по умолчанию)
                'count' => 1,
            ],
            'language' => (object) [
                'label' => 'Группировать по Языку',
                'default' => 'Язык не выбран',
                'count' => 1,
                'active' => true,
            ],
            'expertise' => (object) [
                'label' => 'Группировать по Экспертизе',
                'default' => '"Экспертиза" не выбрана',
                'count' => 2,
                'active' => true,
                'select' => 'name',
            ],
        ];*/
        if (count($this->groups)) {
            return $this->groups;
        }
        $this->groups = [];
        foreach (is_bool($this->showTree) ? [] : $this->makeConfig($this->showTree) as $group => $option) {
            if (!is_array($option)) {
                $option = ['label' => $option];
            }
            elseif (!isset($option['label'])) {
                $option['label'] = $group;
            }
            $option = (object) $option;
            if (isset($option->dependsOn) && is_string($option->dependsOn)) {
                $option->dependsOn = [$option->dependsOn];
            }
            $option->active = !empty($option->active);
            if (!isset($option->count)) {
                $option->count = $this->getCountByGroup($group, isset($option->scopes) ? $option->scopes : null);
            }
            $this->groups[$group] = $option;
        }
        return $this->groups;
    }

    public function onRefresh()
    {
        $columns =  $this->getColumns();
        $pagination = [
            'recordsPerPage' => $this->showPagination ? $this->recordsPerPage : 0,
            'currentPageNumber' => $this->currentPageNumber,
        ];
        $records = $this->getRecordsByGroups([], $pagination);

        $groups = [];
        $groupsOptions = $this->getGroups();
        foreach ($this->getGroupsAllValue() as $option) {
            $option->label = $groupsOptions[$option->group]->label;
            $groups[] = $option;
        }
        $result = [
            'cssClasses' => implode(' ', $this->cssClasses),
            'noRecordsMessage' => trans($this->noRecordsMessage),
            'showCheckboxes' => $this->showCheckboxes,
            'showSetup' => $this->showSetup,
            'showPagination' => $this->showPagination,
            'showSorting' => $this->showSorting,
            'showTree' => $this->showTree,
            'treeLevel' => 0,
            'recordTotal' => isset($pagination['recordsTotal']) ? $pagination['recordsTotal'] : count($records),
            'columns' => [],
            'records' => $records,
            'sort' => [
                'column' => $this->getSortColumn(),
                'direction' => $this->sortDirection,
            ],
            'setup_title' => trans('backend::lang.list.setup_title'),
            'setup_help' => trans('backend::lang.list.setup_help'),
            'setup_group_help' => 'Используйте флажки для выбора групп, по которым вы хотите объединить элементы списка. Вы можете изменить порядок группировки, перетаскивая их вверх или вниз.',
            'records_per_page' => trans('backend::lang.list.records_per_page'),
            'records_per_page_help' => trans('backend::lang.list.records_per_page_help'),
            'recordsPerPage' => $this->recordsPerPage,
            'apply' => trans('backend::lang.form.apply'),
            'cancel' => trans('backend::lang.form.cancel'),
            'groups' => $groups,
        ];
        foreach ($columns as $name => $column) {
            $result['columns'][] = [
                'name' => $name,
                'value' => $column,
            ];
            $column->label = $this->getHeaderValue($column);
        }
        return $result;
    }

    public function onRescopes()
    {
        $pagination = [
            'recordsPerPage' => $this->showPagination ? $this->recordsPerPage : 0,
            'currentPageNumber' => $this->currentPageNumber,
        ];
        $records = $this->getRecordsByGroups([], $pagination);
        return [
            'recordTotal' => isset($pagination['recordsTotal']) ? $pagination['recordsTotal'] : count($records),
            'records' => $records,
        ];
    }

    public function onPaginate()
    {
        $this->currentPageNumber = input('page');
        $pagination = [
            'recordsPerPage' => $this->showPagination ? $this->recordsPerPage : 0,
            'currentPageNumber' => $this->currentPageNumber,
        ];
        return ['records' => $this->getRecordsByGroups([], $pagination)];
    }

    protected function validateModel()
    {
        if (!$this->model) {
            throw new ApplicationException(Lang::get(
                'backend::lang.list.missing_model',
                ['class'=>get_class($this->controller)]
            ));
        }
        if (!$this->model instanceof Model) {
            throw new ApplicationException(Lang::get(
                'backend::lang.model.invalid_class',
                ['model'=>get_class($this->model), 'class'=>get_class($this->controller)]
            ));
        }
        if ($this->showTree && is_bool($this->showTree) && !$this->model->methodExists('scopeGetNested')) {
            throw new ApplicationException(
                'To display list as a tree, the specified model must have a method "scopeGetNested"'
            );
        }
        return $this->model;
    }

    /**
     * Replaces the @ symbol with a table name in a model
     * @param  string $sql
     * @param  string $table
     * @return string
     */
    protected function parseTableName($sql, $table)
    {
        return str_replace('@', $table.'.', $sql);
    }

    /**
     * Applies any filters to the model.
     */
    public function prepareModel($needFilter = true)
    {
        $query = $this->model->newQuery();
        $primaryTable = $this->model->getTable();
        $selects = [$primaryTable.'.*'];
        $joins = [];
        $withs = [];

        /*
         * Extensibility
         */
        $this->fireSystemEvent('backend.list.extendQueryBefore', [$query]);

        /*
         * Prepare searchable column names
         */
        $primarySearchable = [];
        $relationSearchable = [];

        $columnsToSearch = [];
        if (!empty($this->searchTerm) && ($searchableColumns = $this->getSearchableColumns())) {
            foreach ($searchableColumns as $column) {
                /*
                 * Related
                 */
                if ($this->isColumnRelated($column)) {
                    $table = $this->model->makeRelation($column->relation)->getTable();
                    $columnName = isset($column->sqlSelect)
                        ? DbDongle::raw($this->parseTableName($column->sqlSelect, $table))
                        : $table . '.' . $column->valueFrom;

                    $relationSearchable[$column->relation][] = $columnName;
                }
                /*
                 * Primary
                 */
                else {
                    $columnName = isset($column->sqlSelect)
                        ? DbDongle::raw($this->parseTableName($column->sqlSelect, $primaryTable))
                        : DbDongle::cast(Db::getTablePrefix() . $primaryTable . '.' . $column->columnName, 'TEXT');

                    $primarySearchable[] = $columnName;
                }
            }
        }

        /*
         * Prepare related eager loads (withs) and custom selects (joins)
         */
        foreach ($this->getColumns() as $column) {

            if (!$this->isColumnRelated($column) || (!isset($column->sqlSelect) && !isset($column->valueFrom))) {
                continue;
            }

            if (isset($column->valueFrom)) {
                $withs[] = $column->relation;
            }

            $joins[] = $column->relation;
        }

        /*
         * Add eager loads to the query
         */
        if ($withs) {
            $query->with(array_unique($withs));
        }

        /*
         * Apply search term
         */
        $query->where(function ($innerQuery) use ($primarySearchable, $relationSearchable, $joins) {

            /*
             * Search primary columns
             */
            if (count($primarySearchable) > 0) {
                $this->applySearchToQuery($innerQuery, $primarySearchable, 'or');
            }

            /*
             * Search relation columns
             */
            if ($joins) {
                foreach (array_unique($joins) as $join) {
                    /*
                     * Apply a supplied search term for relation columns and
                     * constrain the query only if there is something to search for
                     */
                    $columnsToSearch = array_get($relationSearchable, $join, []);

                    if (count($columnsToSearch) > 0) {
                        $innerQuery->orWhereHas($join, function ($_query) use ($columnsToSearch) {
                            $this->applySearchToQuery($_query, $columnsToSearch);
                        });
                    }
                }
            }

        });

        /*
         * Custom select queries
         */
        foreach ($this->getColumns() as $column) {
            if (!isset($column->sqlSelect)) {
                continue;
            }

            $alias = $query->getQuery()->getGrammar()->wrap($column->columnName);

            /*
             * Relation column
             */
            if (isset($column->relation)) {

                // @todo Find a way...
                $relationType = $this->model->getRelationType($column->relation);
                if ($relationType == 'morphTo') {
                    throw new ApplicationException('The relationship morphTo is not supported for list columns.');
                }

                $table =  $this->model->makeRelation($column->relation)->getTable();
                $sqlSelect = $this->parseTableName($column->sqlSelect, $table);

                /*
                 * Manipulate a count query for the sub query
                 */
                $relationObj = $this->model->{$column->relation}();
                $countQuery = $relationObj->getRelationCountQuery($relationObj->getRelated()->newQueryWithoutScopes(), $query);

                $joinSql = $this->isColumnRelated($column, true)
                    ? DbDongle::raw("group_concat(" . $sqlSelect . " separator ', ')")
                    : DbDongle::raw($sqlSelect);

                $joinSql = $countQuery->select($joinSql)->toSql();

                $selects[] = Db::raw("(".$joinSql.") as ".$alias);
            }
            /*
             * Primary column
             */
            else {
                $sqlSelect = $this->parseTableName($column->sqlSelect, $primaryTable);
                $selects[] = DbDongle::raw($sqlSelect . ' as '. $alias);
            }
        }

        /*
         * Apply sorting
         */

        if ($sortColumn = $this->getSortColumn()) {
            if (($column = array_get($this->allColumns, $sortColumn)) && $column->valueFrom) {
                $sortColumn = $this->isColumnPivot($column)
                    ? 'pivot_' . $column->valueFrom
                    : $column->valueFrom;
            }

            $query->orderBy($sortColumn, $this->sortDirection);
        }

        /*
         * Apply filters
         */

        if ($needFilter) {
            $this->applyAllScopesToQuery($query);
        }
        foreach ($this->filterCallbacks as $callback) {
            $callback($query);
        }

        /*
         * Add custom selects
         */
        $query->select($selects);

        /*
         * Extensibility
         */
        if ($event = $this->fireSystemEvent('backend.list.extendQuery', [$query])) {
            return $event;
        }

        return $query;
    }

    private static function ended($string, $substring)
    {
        return substr($string, strlen($string) - strlen($substring)) == $substring;
    }

    private static function sprepareString($string, $record)
    {
        $data = $record->toArray();
        $data += [$record->getKeyName() => $record->getKey()];
        $columns = array_keys($data);
        $methods = get_class_methods(get_class($record));
        foreach ($methods as $name) {
            if ($name !== 'getAttribute' && Self::ended($name, 'Attribute') && strpos($name, 'get') === 0) {
                $value = $record->$name();
                $attribute = lcfirst(substr($name, 3, -9));
                $data[$attribute] = $value;
                $data[snake_case($attribute)] = $value;
            }
        }
        array_walk($data, function(&$value) {
            $value = is_null($value) ? '' : $value;

        });
        $result = RouterHelper::parseValues($data, $columns, $string);
        return RouterHelper::parseValues($data, $columns, $string);
    }

    public function getColumnOnClick($column, $record)
    {
        if (is_string($column->clickable)) {
            $result = Self::sprepareString($column->clickable, $record);
            return $result === $column->clickable ? null : $result;
        }
    }

    public function getRecordOnClick($record)
    {
        if (isset($this->recordOnClick)) {
            return Self::sprepareString($this->recordOnClick, $record);
        }
        if (!isset($this->recordUrl)) {
            return null;
        }
        $url = Self::sprepareString($this->recordUrl, $record);
        return 'if(event.ctrlKey)open("' . Backend::url($url) . '"); else location.href="' . Backend::url($url) . '";console.log(record)';
    }

    /**
     * Get all the registered columns for the instance.
     * @return array
     */
    public function getColumns()
    {
        if ($this->allColumns) {
            return $this->allColumns;
        }
        if (empty($this->columns) || !is_array($this->columns)) {
            $class = get_class($this->model instanceof Model ? $this->model : $this->controller);
            throw new ApplicationException(Lang::get('backend::lang.list.missing_columns', compact('class')));
        }
        $this->addColumns($this->columns);
        $this->fireSystemEvent('backend.list.extendColumns');

        if ($columnOrder = $this->getSession('order', null)) {
            $orderedDefinitions = [];
            foreach ($columnOrder as $column) {
                if (isset($this->allColumns[$column])) {
                    $orderedDefinitions[$column] = $this->allColumns[$column];
                }
            }

            $this->allColumns = array_merge($orderedDefinitions, $this->allColumns);
        }
        $definitions = $this->allColumns;
        if (!$this->columnOverride) {
            $this->columnOverride = $this->getSession('visible', null);
        }
        if ($this->columnOverride && is_array($this->columnOverride)) {
            foreach ($this->allColumns as $column) {
                $column->invisible = true;
            }
            foreach ($this->columnOverride as $columnName) {
                if (isset($this->allColumns[$columnName])) {
                    $this->allColumns[$columnName]->invisible = false;
                }
            }
        }
        return $this->allColumns;
    }

    /**
     * Get a specified column object
     * @param  string $column
     * @return mixed
     */
    public function getColumn($column)
    {
        if (isset($this->allColumns[$column])) {
            return $this->allColumns[$column];
        }
    }

    /**
     * Returns the list columns that are visible by list settings or default
     */
    public function getVisibleColumns()
    {
        $this->visibleColumns = [];
        foreach ($this->getColumns() as $columnName => $column) {
            if (!$column->invisible) {
                $this->visibleColumns[$columnName] = $column;
            }
        }
        return $this->visibleColumns;
    }

    /**
     * Programatically add columns, used internally and for extensibility.
     * @param array $columns Column definitions
     */
    public function addColumns(array $columns)
    {
        /*
         * Build a final collection of list column objects
         */
        foreach ($columns as $columnName => $config) {
            $this->allColumns[$columnName] = $this->makeListColumn($columnName, $config);
        }
    }

    /**
     * Programatically remove a column, used for extensibility.
     * @param string $column Column name
     */
    public function removeColumn($columnName)
    {
        if (isset($this->allColumns[$columnName])) {
            unset($this->allColumns[$columnName]);
        }
    }

    /**
     * Creates a list column object from it's name and configuration.
     */
    protected function makeListColumn($name, $config)
    {
        if (is_string($config)) {
            $label = $config;
        }
        elseif (isset($config['label'])) {
            $label = $config['label'];
        }
        else {
            $label = studly_case($name);
        }

        /*
         * Auto configure pivot relation
         */
        if (starts_with($name, 'pivot[') && strpos($name, ']') !== false) {
            $_name = HtmlHelper::nameToArray($name);
            $relationName = array_shift($_name);
            $valueFrom = array_shift($_name);

            if (count($_name) > 0) {
                $valueFrom  .= '['.implode('][', $_name).']';
            }

            $config['relation'] = $relationName;
            $config['valueFrom'] = $valueFrom;
            $config['searchable'] = false;
        }
        /*
         * Auto configure standard relation
         */
        elseif (strpos($name, '[') !== false && strpos($name, ']') !== false) {
            $config['valueFrom'] = $name;
            $config['sortable'] = false;
            $config['searchable'] = false;
        }

        $columnType = isset($config['type']) ? $config['type'] : null;

        $column = new ListColumn($name, $label);
        $column->displayAs($columnType, $config);
        $column->invisible = !empty($column->invisible);
        $default = isset($column->config['defaultValue']) ? $column->config['defaultValue'] : null;


        if ($this->optionsByCurrentPage) {
            $postfix = 'by_page_' . implode('_', BackendController::$params);
        }
        else {
            $postfix = '';
        }

        if ($column->sortable) {
            if (is_bool($column->sortable)) {
                switch ($column->type) {
                    case 'switch':
                        $column->sortable = 'Academy\System\Widgets\FilterScopeSwitch';
                        break;
                    case 'number':
                        $column->sortable = 'Academy\System\Widgets\FilterScopeNumber';
                        break;
                    case 'date':
                    case 'datetime':
                    case 'timesince':
                        $column->sortable = 'Academy\System\Widgets\FilterScopeDate';
                        break;
                    default:
                        $column->sortable = 'Academy\System\Widgets\FilterScopeText';
                }
            }
            $this->widgets[$name] = $this->makeWidget($column->sortable, (object) [
                'scope' => $column,
                'model' => $this->model,
                'alias' => $this->alias . 'Scope' . $name . $postfix,
                'list' => $this,
            ]);
            if (!isset($config['filterable']) || !empty($config['filterable'])) {
                $column->widgetType = $this->widgets[$name]->getType();
                $column->widgetValue = $this->widgets[$name]->getValue($default);
            }
        }
        return $column;
    }

    /**
     * Calculates the total columns used in the list, including checkboxes
     * and other additions.
     */
    /*protected function getTotalColumns()
    {
        $columns = $this->visibleColumns ?: $this->getVisibleColumns();
        $total = count($columns);

        if ($this->showCheckboxes) {
            $total++;
        }

        if ($this->showSetup) {
            $total++;
        }

        return $total;
    }*/

    /**
     * Looks up the column header
     */
    public function getHeaderValue($column)
    {
        $value = Lang::get($column->label);

        /*
         * Extensibility
         */
        if ($response = $this->fireSystemEvent('backend.list.overrideHeaderValue', [$column, $value])) {
            $value = $response;
        }

        return $value;
    }

    /**
     * Returns a raw column value
     * @return string
     */
    public function getColumnValueRaw($record, $column)
    {
        $columnName = $column->columnName;

        /*
         * Handle taking value from model relation.
         */
        if ($column->valueFrom && $column->relation) {
            $columnName = $column->relation;

            if (!array_key_exists($columnName, $record->getRelations())) {
                $value = null;
            }
            elseif ($this->isColumnRelated($column, true)) {
                $value = $record->{$columnName}->lists($column->valueFrom);
            }
            elseif ($this->isColumnRelated($column) || $this->isColumnPivot($column)) {
                $value = $record->{$columnName}
                    ? $column->getValueFromData($record->{$columnName})
                    : null;
            }
            else {
                $value = null;
            }
        }
        /*
         * Handle taking value from model attribute.
         */
        elseif ($column->valueFrom) {
            $value = $column->getValueFromData($record);
        }
        /*
         * Otherwise, if the column is a relation, it will be a custom select,
         * so prevent the Model from attempting to load the relation
         * if the value is NULL.
         */
        else {
            if ($record->hasRelation($columnName) && array_key_exists($columnName, $record->attributes)) {
                $value = $record->attributes[$columnName];
            }
            else {
                $value = $record->{$columnName};
            }
        }
        if (isset($this->widgets[$columnName])
            && method_exists($this->widgets[$columnName], 'getColumnValue')
        ) {
            return $this->widgets[$columnName]->getColumnValue($value, $column, $record);
        }
        return $value;
    }

    /**
     * Returns a column value, with filters applied
     * @return string
     */
    public function getColumnValue($record, $column, $forFilter = false)
    {
        $value = $this->getColumnValueRaw($record, $column);

        if (method_exists($this, 'eval'. studly_case($column->type) .'TypeValue')) {
            $value = $this->{'eval'. studly_case($column->type) .'TypeValue'}($record, $column, $value, $forFilter);
        }
        else {
            $value = $this->evalCustomListType($column->type, $record, $column, $value, $forFilter);
        }

        /*
         * Apply default value.
         */
        if ($value === '' || $value === null) {
            $value = $column->defaults;
        }

        /*
         * Extensibility
         */
        if ($response = $this->fireSystemEvent('backend.list.overrideColumnValue', [$record, $column, &$value])) {
            $value = $response;
        }
        return $value;
    }

    /**
     * Adds a custom CSS class string to a record row
     * @param  Model $record Populated model
     * @return string
     */
    public function getRowClass($record)
    {
        $value = '';

        /*
         * Extensibility
         */
        if ($response = $this->fireSystemEvent('backend.list.injectRowClass', [$record])) {
            $value = $response;
        }

        return $value;
    }

    //
    // Value processing
    //

    /**
     * Process a custom list types registered by plugins.
     */
    protected function evalCustomListType($type, $record, $column, $value)
    {
        $plugins = PluginManager::instance()->getRegistrationMethodValues('registerListColumnTypes');

        foreach ($plugins as $availableTypes) {
            if (!isset($availableTypes[$type])) {
                continue;
            }

            $callback = $availableTypes[$type];

            if (is_callable($callback)) {
                return call_user_func_array($callback, [$value, $column, $record]);
            }
        }
        return $value;
    }

    /**
     * Common mistake, relation is not a valid list column.
     * @deprecated Remove if year >= 2018
     */
    protected function evalRelationTypeValue($record, $column, $value)
    {
        traceLog(sprintf('Warning: List column type "relation" for class "%s" is not valid.', get_class($record)));
        return $this->evalTextTypeValue($record, $column, $value);
    }

    /**
     * Process as partial reference
     */
    protected function evalPartialTypeValue($record, $column, $value, $forFilter = false)
    {
        return $this->controller->makePartial($column->path ?: $column->columnName, [
            'listColumn' => $column,
            'listRecord' => $record,
            'listValue'  => $value,
            'column'     => $column,
            'record'     => $record,
            'value'      => $value,
            'forFilter'  => $forFilter,
        ]);
    }

    /**
     * Process as a datetime value
     */
    protected function evalDatetimeTypeValue($record, $column, $value)
    {
        if ($value === null) {
            return null;
        }

        $dateTime = $this->validateDateTimeValue($value, $column);

        if ($column->format !== null) {
            $value = $dateTime->format($column->format);
        }
        else {
            $value = $dateTime->toDayDateTimeString();
        }

        return Backend::dateTime($dateTime, [
            'defaultValue' => $value,
            'format' => $column->format,
            'formatAlias' => 'dateTimeLongMin'
        ]);
    }

    /**
     * Process as a time value
     */
    protected function evalTimeTypeValue($record, $column, $value)
    {
        if ($value === null) {
            return null;
        }

        $dateTime = $this->validateDateTimeValue($value, $column);

        $format = $column->format !== null ? $column->format : 'g:i A';

        $value = $dateTime->format($format);

        return Backend::dateTime($dateTime, [
            'defaultValue' => $value,
            'format' => $column->format,
            'formatAlias' => 'time'
        ]);
    }

    /**
     * Process as a date value
     */
    protected function evalDateTypeValue($record, $column, $value)
    {
        if ($value === null) {
            return null;
        }

        $dateTime = $this->validateDateTimeValue($value, $column);

        if ($column->format !== null) {
            $value = $dateTime->format($column->format);
        }
        else {
            $value = $dateTime->toFormattedDateString();
        }

        return Backend::dateTime($dateTime, [
            'defaultValue' => $value,
            'format' => $column->format,
            'formatAlias' => 'dateLongMin'
        ]);
    }

    /**
     * Process as diff for humans (1 min ago)
     */
    protected function evalTimesinceTypeValue($record, $column, $value)
    {
        if ($value === null) {
            return null;
        }

        $dateTime = $this->validateDateTimeValue($value, $column);

        $value = DateTimeHelper::timeSince($dateTime);

        return Backend::dateTime($dateTime, [
            'defaultValue' => $value,
            'timeSince' => true
        ]);
    }

    /**
     * Process as time as current tense (Today at 0:00)
     */
    protected function evalTimetenseTypeValue($record, $column, $value)
    {
        if ($value === null) {
            return null;
        }

        $dateTime = $this->validateDateTimeValue($value, $column);

        $value = DateTimeHelper::timeTense($dateTime);

        return Backend::dateTime($dateTime, [
            'defaultValue' => $value,
            'timeTense' => true
        ]);
    }

    /**
     * Validates a column type as a date
     */
    protected function validateDateTimeValue($value, $column)
    {
        $value = DateTimeHelper::makeCarbon($value, false);

        if (!$value instanceof Carbon) {
            throw new ApplicationException(Lang::get(
                'backend::lang.list.invalid_column_datetime',
                ['column' => $column->columnName]
            ));
        }

        return $value;
    }

    //
    // Filtering
    //

    public function addFilter(callable $filter)
    {
        $this->filterCallbacks[] = $filter;
    }

    //
    // Searching
    //

    /**
     * Applies a search term to the list results, searching will disable tree
     * view if a value is supplied.
     * @param string $term
     */
    public function setSearchTerm($term)
    {
        if (!empty($term)) {
            //$this->showTree = false;
        }

        $this->searchTerm = $term;
    }

    /**
     * Applies a search options to the list search.
     * @param array $options
     */
    public function setSearchOptions($options = [])
    {
        extract(array_merge([
            'mode' => null,
            'scope' => null
        ], $options));

        $this->searchMode = $mode;
        $this->searchScope = $scope;
    }

    /**
     * Returns a collection of columns which can be searched.
     * @return array
     */
    protected function getSearchableColumns()
    {
        $columns = $this->getColumns();
        $searchable = [];

        foreach ($columns as $column) {
            if (!$column->searchable) {
                continue;
            }

            $searchable[] = $column;
        }

        return $searchable;
    }

    /**
     * Applies the search constraint to a query.
     */
    protected function applySearchToQuery($query, $columns, $boolean = 'and')
    {
        $term = $this->searchTerm;

        if ($scopeMethod = $this->searchScope) {
            $searchMethod = $boolean == 'and' ? 'where' : 'orWhere';
            $query->$searchMethod(function($q) use ($term, $columns, $scopeMethod) {
                $q->$scopeMethod($term, $columns);
            });
        }
        else {
            $searchMethod = $boolean == 'and' ? 'searchWhere' : 'orSearchWhere';
            $query->$searchMethod($term, $columns, $this->searchMode);
        }
    }

    public function onSort()
    {
        if ($column = input('sortColumn')) {
            $this->putSession('sort', [
                'column' => $this->sortColumn = input('sortColumn'),
                'direction' => $this->sortDirection = input('direction')
            ]);
            $this->currentPageNumber = input('page');
            $pagination = [
                'recordsPerPage' => $this->showPagination ? $this->recordsPerPage : 0,
                'currentPageNumber' => $this->currentPageNumber,
            ];
            return ['records' => $this->getRecordsByGroups([], $pagination)];
        }
    }

    /**
     * Returns the current sorting column, saved in a session or cached.
     */
    protected function getSortColumn()
    {
        if (!$this->isSortable()) {
            return false;
        }
        if ($this->sortColumn !== null) {
            return $this->sortColumn;
        }
        if ($this->showSorting && ($sortOptions = $this->getSession('sort'))) {
            $this->sortColumn = $sortOptions['column'];
            $this->sortDirection = $sortOptions['direction'];
        }
        else if (is_string($this->defaultSort)) {
            $this->sortColumn = $this->defaultSort;
            $this->sortDirection = 'desc';
        }
        elseif (is_array($this->defaultSort) && isset($this->defaultSort['column'])) {
            $this->sortColumn = $this->defaultSort['column'];
            $this->sortDirection = (isset($this->defaultSort['direction'])) ?
                $this->defaultSort['direction'] :
                'desc';
        }
        if ($this->sortColumn === null || !$this->isSortable($this->sortColumn)) {
            $columns = $this->visibleColumns ?: $this->getVisibleColumns();
            $columns = array_filter($columns, function($column){ return $column->sortable; });
            $this->sortColumn = key($columns);
            $this->sortDirection = 'desc';
        }
        return $this->sortColumn;
    }

    protected function isSortable($column = null)
    {
        if ($column === null) {
            return !!count($this->getSortableColumns());
        }
        return array_key_exists($column, $this->getSortableColumns());
    }

    protected function getSortableColumns()
    {
        if ($this->sortableColumns !== null) {
            return $this->sortableColumns;
        }
        $columns = $this->getColumns();
        $sortable = array_filter($columns, function($column){
            return !!$column->sortable;
        });
        return $this->sortableColumns = $sortable;
    }

    public function onApplySetup()
    {
        $this->columnOverride = [];
        $orders = [];
        $addVisible = false;
        foreach ($this->allColumns as $column) {
            $column->invisible = true;
        }
        foreach (input('columns') as $column) {
            if (!$column['invisible']) {
                $this->columnOverride[] = $column['name'];
                if (!$column['loaded']) {
                    $addVisible = true;
                }
                $this->allColumns[$column['name']]->invisible = false;
            }
            $orders[] = $column['name'];
        }
        $newRecordsPerPage = input('records_per_page', $this->recordsPerPage);
        $this->putSession('visible', $this->columnOverride);
        $this->putSession('order', $orders);
        $this->putSession('per_page', $newRecordsPerPage);
        /*if ($newRecordsPerPage == $this->recordsPerPage && !$addVisible) {
            return;
        }*/
        $this->recordsPerPage = $newRecordsPerPage;
        $pagination = [
            'recordsPerPage' => $this->showPagination ? $this->recordsPerPage : 0,
            'currentPageNumber' => $this->currentPageNumber,
        ];
        $this->putSession('group', input('groups', []));
        $this->groups = null;
        $records = $this->getRecordsByGroups([], $pagination);
        return [
            'records' => $records,
            'recordTotal' => isset($pagination['recordsTotal']) ? $pagination['recordsTotal'] : count($records)
        ];
    }

    //
    // Tree
    //

    /**
     * Checks if a node (model) is expanded in the session.
     * @param  Model $node
     * @return boolean
     */
    private static function getKeyFromArray($array)
    {
        ksort($array);
        foreach ($array as $key => $values) {
            foreach ($values as $num => $value) {
                if (is_null($value)) {
                    $values[$num] = '';
                }
            }
            sort($values);
            $array[$key] = $values;
        }
        return http_build_query($array);
    }

    public function isTreeNodeExpanded($node)
    {
        if ($node instanceof \Model) {
            $key = $node->getKey();
        }
        elseif (is_string($node)) {
            $key = $node;
        }
        elseif (is_array($node)) {
            $key = self::getKeyFromArray($node);
        }
        return $this->showTree && $this->getSession('tree_node_status_' . $key, $this->treeExpanded);
    }

    public function setTreeNodeExpanded($node, $value)
    {
        if ($node instanceof \Model) {
            $key = $node->getKey();
        }
        elseif (is_string($node)) {
            $key = $node;
        }
        elseif (is_array($node)) {
            $key = self::getKeyFromArray($node);
        }
        return $this->showTree && $this->putSession('tree_node_status_' . $key, $value);
    }

    /**
     * Sets a node (model) to an expanded or collapsed state, stored in the
     * session, then renders the list again.
     * @return string List HTML contents.
     */
    public function onToggleTreeNode()
    {
        $this->putSession('tree_node_status_' . input('node_id'), input('status') ? 0 : 1);
        return $this->onRefresh();
    }

    //
    // Helpers
    //

    /**
     * Check if column refers to a relation of the model
     * @param  ListColumn  $column List column object
     * @param  boolean     $multi  If set, returns true only if the relation is a "multiple relation type"
     * @return boolean
     */
    protected function isColumnRelated($column, $multi = false)
    {
        if (!isset($column->relation) || $this->isColumnPivot($column)) {
            return false;
        }

        if (!$this->model->hasRelation($column->relation)) {
            throw new ApplicationException(Lang::get(
                'backend::lang.model.missing_relation',
                ['class'=>get_class($this->model), 'relation'=>$column->relation]
            ));
        }

        if (!$multi) {
            return true;
        }

        $relationType = $this->model->getRelationType($column->relation);

        return in_array($relationType, [
            'hasMany',
            'belongsToMany',
            'morphToMany',
            'morphedByMany',
            'morphMany',
            'attachMany',
            'hasManyThrough'
        ]);
    }

    /**
     * Checks if a column refers to a pivot model specifically.
     * @param  ListColumn  $column List column object
     * @return boolean
     */
    protected function isColumnPivot($column)
    {
        if (!isset($column->relation) || $column->relation != 'pivot') {
            return false;
        }

        return true;
    }
}