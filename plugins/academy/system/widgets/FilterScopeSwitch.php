<?php namespace Academy\System\Widgets;

use Backend\Classes\WidgetBase;

class FilterScopeSwitch extends WidgetBase
{
    public $scope;
    public $model;
    public $list;
    protected $defaultAlias = 'filerScopeSwitch';

    public function init()
    {
        $this->fillFromConfig([
            'scope',
            'model',
            'list',
        ]);
        $this->scope->widgetAlias = $this->alias;
        $this->bindToController();
    }

    public function getColumnValue($value, $column, $record)
    {
        return \Lang::get('backend::lang.list.column_switch_' . ($value ? 'true' : 'false'));
    }

    public function getValue($default = null)
    {
        return $this->getSession('value', $default);
    }

    public function setValue($value)
    {
        $this->putSession('value', $value);
    }

    public function getType()
    {
        return $this->defaultAlias;
    }

    public function render()
    {
        $this->addCss('css/academy.scope.css', 'core');
        $this->addJs('js/academy.scope.js', 'core');
    }

    public function apply($query)
    {
        if (!in_array($value = $this->getValue(), [false, true], true)) {
            return;
        }
        $columnName = $this->scope->sqlSelect ? \DB::raw($this->scope->sqlSelect) : $this->scope->columnName;
        if ($this->scope->relation) {
            $query->whereHas($this->scope->relation, function($query) use($value) {
                $query->where($this->scope->sqlSelect, !!$value);
            });
        }
        else {
            $query->where($columnName, $value);
        }
    }

    public function onSetValue()
    {
        $this->setValue(input('value'));
    }
}