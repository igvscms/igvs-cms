<?php namespace Academy\System\Widgets;

use Backend\Classes\WidgetBase;

class FilterScopeNumber extends WidgetBase
{
    public $scope;
    public $model;
    public $list;
    protected $defaultAlias = 'filerScopeNumber';

    public function init()
    {
        $this->fillFromConfig([
            'scope',
            'model',
            'list',
        ]);
        $this->scope->widgetAlias = $this->alias;
        $this->bindToController();
    }

    public function getColumnValue($value, $column, $record)
    {
        if (is_array($value) && count($value) == count($value, COUNT_RECURSIVE)) {
            $value = implode(', ', $value);
        }
        return htmlentities($value, ENT_QUOTES, 'UTF-8', false);
    }

    public function getValue()
    {
        return $this->getSession('value', []);
    }

    public function setValue($value)
    {
        $this->putSession('value', $value);
    }

    public function getType()
    {
        return $this->defaultAlias;
    }

    public function render()
    {
        $this->addCss('css/academy.scope.css', 'core');
        $this->addJs('js/academy.scope.js', 'core');
    }

    public function apply($query)
    {
        $value = $this->getValue();
        if (empty($value['before']) && empty($value['after'])) {
            return;
        }
        $table = $this->model->getTable();
        $columnName = $this->scope->sqlSelect ? \DB::raw($this->scope->sqlSelect) : "$table.{$this->scope->columnName}";
        if ($this->scope->relation) {
            $query->whereHas($this->scope->relation, function($query) use($value) {
                if (!empty($value['before'])) {
                    $query->where($this->scope->sqlSelect, '>=', $value['before']);
                }
                if (!empty($value['after'])) {
                    $query->where($this->scope->sqlSelect, '<=', $value['after']);
                }
            });
        }
        else {
            if (!empty($value['before'])) {
                $query->where($columnName, '>=', $value['before']);
            }
            if (!empty($value['after'])) {
                $query->where($columnName, '<=', $value['after']);
            }
        }
    }

    public function onSetValue()
    {
        $this->setValue((array) input('value'));
    }
}