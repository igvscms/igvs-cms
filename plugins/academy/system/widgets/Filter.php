<?php namespace Academy\System\Widgets;

use Backend\Classes\WidgetBase;

class Filter extends WidgetBase
{
    public $scopes;
    public $context = null;
    public $cssClasses = [];
    protected $defaultAlias = 'filter';
    protected $scopesDefined = false;
    protected $allScopes = [];
    protected $scopeModels = [];

    public function init()
    {
        $this->fillFromConfig([
            'scopes',
            'context',
        ]);
    }

    public function render()
    {
        $this->addJs('assets/js/academy.filter.js', 'core');
        $this->addJs('assets/css/academy.filter.css', 'core');
    }
}