<?php return [
    'relation_label' => 'custom fields',
    'span' => [
        'full' => 'full',
        'auto' => 'auto',
        'left' => 'left',
        'right' => 'right',
    ],
    'field' => [
        'type' => 'Type',
        'name' => 'Name',
        'is_required' => 'Required',
        'span' => 'Span',
        'values' => 'Values',
    ],
];