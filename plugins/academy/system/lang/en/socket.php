<?php return [
    'label' => 'Sockets',
    'preview' => 'View connection details',
    'field' => [
        'user_id' => 'User',
        'page' => 'Page',
        'created_at' => 'Created at',
        'max_created_at' => 'Updated at',
        'count' => 'number of packets',
        'sum_dt' => 'Total send time',
        'avg_dt' => 'Average send time',
        'max_dt' => 'Maximum send time',
        'sum_value' => 'Total send size',
        'avg_value' => 'Average send size',
        'max_value' => 'Maximum send size',
        'deleted' => 'Active compound',
    ],
    'command' => [
        'delete' => 'Cancel sending packages',
        'restore' => 'Resume package sending',
    ],
    'dialog' => [
        'delete' => 'Cancel sending packets for selected connections?',
        'restore' => 'Resume sending packets for selected connections?'
    ]
];