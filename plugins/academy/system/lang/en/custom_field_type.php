<?php return [
    'name' => [
        'dropdown' => 'dropdown',
        'checkbox' => 'checkboxlist',
        'radio' => 'radiolist',
        'string' => 'string',
        'text' => 'text',
        'datetime' => 'datetime',
    ]
];