<?php namespace Academy\System\Models;

class CustomFieldType extends \Model
{
    public $table = 'academy_system_custom_fields_types';
    public $hasMany = ['field' => 'Academy\System\Models\CustomField'];

    public function getNameAttribute()
    {
        return trans('academy.system::custom_field_type.name.' . $this->code);
    }
}