<?php namespace Academy\System\Models;

class History extends \Model
{
    public $table = 'academy_system_history';
    public $belongsTo = ['user' => 'Backend\Models\User'];
    public $data_save;

    public function afterFetch()
    {
        $this->data_save = isset($this->data) ? json_decode(bzdecompress($this->data), true) : [];
    }

    public function beforeSave()
    {
        $this->user = \BackendAuth::getUser();
        $this->data = bzcompress(json_encode($this->data_save), 9);
    }
}