<?php namespace Academy\System\Models;

class CustomFieldResult extends \Model
{
    use \October\Rain\Database\Traits\Validation;

    public $table = 'academy_system_custom_fields_results';
    public $belongsTo = [
        'field' => 'Academy\System\Models\CustomField',
        'value' => 'Academy\System\Models\CustomFieldValue',
    ];
    public $timestamps = false;
    protected $fillable = ['attachment_id', 'attachment_type'];

    private $saveFields = [];
    protected $rules = [];

    public function getDropdownOptions($field_id, $value, $data)
    {
        $result = [];
        $field = CustomField::find($field_id);
        if ($field && $field->type->code == 'dropdown') {
            $result[''] = '-- ' . trans('backend::lang.form.select_placeholder') . ' ' . $field->name;
        }
        return $result + CustomFieldValue::where('field_id', $field_id)
            ->lists('name', 'id');
    }

    public function hasGetMutator($key)
    {
        return !in_array($key, ['field', 'value']);
    }

    public function getAttributeValue($key)
    {
        if (isset($this->attributes[$key])) {
            return $this->attributes[$key];
        }
        if (!is_numeric($key)) {
            return;
        }
        $values = Self::where('field_id', $key)
            ->where('attachment_id', $this->attachment_id)
            ->where('attachment_type', $this->attachment_type)
            ->get(['value_id', 'data']);

        if (!$values->count()) {
            return;
        }
        $field = CustomField::find($key);

        switch ($field->type->code) {
            case 'dropdown':
            case 'radio':
            case 'range':
                return $values[0]->value_id;
            case 'checkbox':
                $result = [];
                foreach ($values as $value) {
                    $result[] = $value->value_id;
                }
                return $result;
            case 'text':
            case 'string':
            case 'datetime':
                return $values[0]->data;
            default:
                return;
        }
    }

    public function beforeValidate()
    {
        foreach ($this->saveFields as $record) {
            if ($record->required) {
                $this->rules[$record->id] = 'required';
            }
        }
    }

    public function setAttribute($key, $value)
    {
        if (is_numeric($key) && ($field = CustomField::find($key))) {
            if ($field->type->code == 'checkbox' && !is_array($value)) {
                $value = null;
            }
            elseif (in_array($field->type->code, ['text', 'string']) && $value == '') {
                $value = null;
            }
            elseif ($field->type->code == 'dropdown' && empty($value)) {
                $value = null;
            }
            $this->saveFields[] = (object)[
                'type' => $field->type->code,
                'id' => $key,
                'value' => $value,
                'required' => $field->is_required,
            ];
            if (!is_null($value)) {
                $value = $key;
            }
        }
        return $this->attributes[$key] = $value;
    }

    public function beforeSave()
    {
        return false;
    }

    public function afterSave()
    {
        $delete = [];
        foreach ($this->saveFields as $record) {
            $delete[] = $record->id;
        }
        Self::whereIn('field_id', $delete)
            ->where('attachment_id', $this->attachment_id)
            ->where('attachment_type', $this->attachment_type)
            ->delete();
        $values = [];
        foreach ($this->saveFields as $record) {
            switch ($record->type) {
                case 'dropdown':
                case 'radio':
                case 'range':
                    $values[] = [
                        'field_id' => $record->id,
                        'value_id' => $record->value,
                        'data' => '',
                        'attachment_id' => $this->attachment_id,
                        'attachment_type' => $this->attachment_type,
                    ];
                    break;
                case 'checkbox':
                    foreach ($record->value as $value) {
                        $values[] = [
                            'field_id' => $record->id,
                            'value_id' => $value,
                            'data' => '',
                            'attachment_id' => $this->attachment_id,
                            'attachment_type' => $this->attachment_type,
                        ];
                    }
                    break;
                case 'text':
                case 'string':
                case 'datetime':
                    $values[] = [
                        'field_id' => $record->id,
                        'value_id' => null,
                        'data' => $record->value,
                        'attachment_id' => $this->attachment_id,
                        'attachment_type' => $this->attachment_type,
                    ];
            }
        }
        if (count($values)) {
            Self::insert($values);
        }
    }
}