<?php namespace Academy\System\Models;

class Session extends \Model
{
    public $table = 'academy_system_session';
    public $belongsTo = ['user' => 'Backend\Models\User'];
    public $timestamps = false;
    public $primaryKey = 'session_id';
}