<?php namespace Academy\System\Models;

class CustomField extends \Model
{
    protected $table = 'academy_system_custom_fields';
    protected $guarded = ['*'];
    public $belongsTo = ['type' => 'Academy\System\Models\CustomFieldType'];
    public $hasMany = [
        'values' => [
            'Academy\System\Models\CustomFieldValue',
            'key' => 'field_id',
        ],
    ];
    public $timestamps = false;

    public function getSpanTranslateAttribute($span = null)
    {
        return trans('academy.system::custom_field.span.' . ($span ?: $this->span));
    }

    public function getSpanOptions()
    {
        $result = [];
        foreach (['full', 'auto', 'left', 'right'] as $item) {
            $result[$item] = $this->getSpanTranslateAttribute($item);
        }
        return $result;
    }
}