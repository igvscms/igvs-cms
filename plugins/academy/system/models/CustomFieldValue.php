<?php namespace Academy\System\Models;

class CustomFieldValue extends \Model
{
    public $table = 'academy_system_custom_fields_values';
    public $belongsTo = ['field' => 'Academy\System\Models\CustomField'];
    public $timestamps = false;
}