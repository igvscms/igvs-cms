<?php namespace Academy\System\Models;

class SocketLog extends \Model
{
    public $table = 'academy_system_sockets';
    public $belongsTo = ['user' => 'Backend\Models\User'];
    public $hasMany = [
        'posts' => ['Academy\System\Models\SocketPost', 'key' => 'socket_id']
    ];

    public $result = [];
    public $option = [];

    private static function tableToArray($posts)
    {
        $result = [];
        foreach ($posts as $post) {

            if ($post->array == '') {
                $result[$post->key] = $post->value;
                continue;
            }
            $sub = &$result;
            foreach (explode('][', $post->array . '[' . $post->key) as $key) {
                if (!isset($sub[$key])) {
                    $sub[$key] = null;
                }
                $sub = $sub[$key];
            }
            $sub = $value;
        }
        return $result;
    }

    public function afterFetch()
    {
        $this->option = explode(',', $this->params);
        $this->result = isset($this->data) ? json_decode(bzdecompress($this->data), true) : [];
    }

    public function getPostAttribute()
    {
        if (is_null($this->attributes['post'])) {
            $this->attributes['post'] = self::tableToArray(SocketPost::where('socket_id', $this->id)->get());
        }
        return $this->attributes['post'];
    }

    public $post = [];

    public function beforeCreate()
    {
        $this->user = \BackendAuth::getUser();
        $this->params = implode(',', $this->option);
        $this->data = bzcompress(json_encode($this->result), 9);
    }

    private function arrayToTable($array = [], &$result = [], $path = '')
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $this->arrayToTable($value, $result, $path == '' ? $key : $path . '[' . $key . ']');
            }
            else {
                $result[] = [
                    'socket_id' => $this->id,
                    'array' => $path,
                    'key' => $key,
                    'value' => $value
                ];
            }
        }
        return $result;
    }

    private static function clearArray($array = [], $template = [])
    {
        if (!is_array($template)) {
            if(!isset($array[$template])) {
                return [];
            }
            return [$template => $array[$template]];
        }
        foreach ($array as $key => $value) {

            if(isset($template[$key])) {
                $array[$key] = self::clearArray($value, $template[$key]);
            }
            elseif(($pos = array_search($key, $template)) === false || !is_numeric($pos)) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    public function afterCreate()
    {
        $this->post = is_null($this->post) ? post() : self::clearArray(post(), $this->post);
        SocketPost::insert($this->arrayToTable($this->post));
    }
}