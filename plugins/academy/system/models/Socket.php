<?php namespace Academy\System\Models;

class Socket extends \Model
{
    use \October\Rain\Database\Traits\SoftDeleting;

    public $table = 'academy_system_socket_topics';
    public $belongsTo = ['user' => 'Backend\Models\User'];
    public $hasMany = [
        'packets' => [
            'Academy\System\Models\SocketPacket',
            'key' => 'session_id',
            'otherKey' => 'session_id'
        ],
    ];
    public $dates = ['max_created_ats'];
    public $fillable = ['session_id', 'user_id', 'topic_id', 'page', 'created_at', 'updated_at', 'deleted_at', 'ip'];
    public $primaryKey = 'session_id';
}