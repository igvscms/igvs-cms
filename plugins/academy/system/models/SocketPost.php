<?php namespace Academy\System\Models;

class SocketPost extends \Model
{
    public $table = 'academy_system_sockets_post';
    public $belongsTo = ['socket' => 'Backend\Models\User'];

    public function scopeArray_key_exists($query, $key, $array)
    {
        $query->where('key', $key)->where('array', $array);
    }

    public function scopeIn_array($query, $value, $array)
    {
        $query->where('value', $value)->where('array', $array);
    }

    public function scopeArray_search($query, $value, $array)
    {
        $query->where('value', $value)->where('array', $array);
    }

    private static function arrayToArrayKey($key)
    {
        $array = explode('[', $key);
        if(count($array) == 1) {
            $array = '';
        }
        else {
            $key = substr(array_pop($array), 0, -1);
            $array = implode('[', $array);
        }
        return [$array, $key];
    }

    public function scopeIsset($query, $key)
    {
        $arrayKey = self::arrayToArrayKey($key);
        $query->where('key', $arrayKey[1])->where('array', $arrayKey[0]);
    }
}