<?php namespace Academy\System\Models;

class SocketPacket extends \Model
{
    public $table = 'academy_system_socket_packets';
    public $fillable = ['value', 'session_id', 'dt'];
}