<?php namespace Academy\System\Models;

class Notifier extends \Model
{
    public $table = 'academy_system_notifier';

    final public static function classRegistration($object)
    {
        if (is_string($object) && class_exists($object)) {
            $class = $object;
        }
        else {
            $class = get_class($object);
        }
        if (!self::where('class', $class)->first()) {
            try {
                self::insert(['class' => $class]);
            }
            catch (\Exception $e) {
            }
        }
    }
}