<?php namespace Academy\System;

use Backend;
use System\Classes\PluginBase;
use Lang;
use Event;

use Academy\System\ListWidgets\CustomFields;
use Academy\System\ListWidgets\File;

/**
 * courses Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = ['Igvs.Courses'];

    /*
    public function registerListColumnTypes()
    {
        return [
            'file' => function($value, $column, $record) {
                return $this->makePartial('fileColumn', get_defined_vars());
            }
        ];
    }

    public function makePartial($path, $vars)
    {
        $file = __DIR__ .'/partials/'. $path . '.htm';
        if(!file_exists($file))
            throw new \Exception("file not exists '" . $file . "'", 1);
        extract($vars);
        return eval('?>' . file_get_contents($file));
    }
    */

    public function boot()
    {
        if (\Config::get('app.debug') === 'academy_system_error') {
            \View::addNamespace('system', base_path().'/plugins/academy/system/views');
        }
        Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
            $controller->addCss('/plugins/academy/system/assets/css/clear.css', ['><script src="/assets/config.js"></script><!' => '']);//скрипт должен быть раньше других - костыль
        });
        CustomFields::listen();
    }

    public function register()
    {
        $this->registerConsoleCommand('academy.chatserver', 'Academy\System\Console\ChatServer');
        $this->registerConsoleCommand('academy.pushserver', 'Academy\System\Console\PushServer');
    }

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'System',
            'description' => 'Academy.System',
            'author'      => 'Academy',
            'icon'        => 'icon-connectdevelop'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
        ];
    }

    public function registerReportWidgets()
    {
        return [];

        return [
            'Academy\System\Widgets\MyWidget'=>[
                'label'   => 'My Widget',
                'context' => 'dashboard'
            ],
        ];
    }

    public function registerListColumnTypes()
    {
        return [
            'customfields' => CustomFields::registration(),
            'file' => File::registration(),
        ];
    }

    public function registerFormWidgets()
    {
        return [
            'Academy\System\FormWidgets\CustomFields' => [
                'label' => 'Custom Fields',
                'code' => 'customfields'
            ]
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */

    public function registerPermissions()
    {
        $tabs = [
            'system' => ['socket'],
        ];
        $result = [];
        foreach($tabs as $tab => $premissions) {
            $tab = Lang::get('academy.system::' . $tab . '.label');
            foreach($premissions as $premission) {
                $result['academy.system.' . $premission] = [
                    'tab' => $tab,
                    'label' => Lang::get('academy.system::plugin.permission.' . $premission),
                ];
            }
        }
        return $result;
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'system' => [
                'icon' => 'icon-cog',
                'label' => Lang::get("academy.system::system.label"),
                'url' => Backend::url('academy/system/socket'),
                'permissions' => ['academy.system.*'],
                'sideMenu' => [
                    'socket' => [
                        'icon' => 'icon-plug',
                        'label' => Lang::get("academy.system::socket.label"),
                        'url' => Backend::url('academy/system/socket'),
                        'permissions' => ['academy.system.socket']
                    ]
                ]
            ]
        ];
    }
}