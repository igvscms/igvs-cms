<?php namespace Academy\System\ListWidgets;

use Backend\Controllers\Files;

class File {

    public static function init($value, $column, $record)
    {
        $result = '';
        foreach ($record->{$column->columnName} as $file) {
            $result .= '<a target="_blank" href="' . Files::getDownloadUrl($file) . '">' . ($file->isImage() ? '<img style="max-width:500px;max-height:200px" src="' . $file->path . '">': e($file->file_name)) . '</a>';
        }
        return $result;
    }

    public static function registration()
    {
        return [Self::class, 'init'];
    }
}