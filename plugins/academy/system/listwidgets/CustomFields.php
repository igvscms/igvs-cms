<?php namespace Academy\System\ListWidgets;

use Academy\System\Models\CustomField;
use Academy\System\Models\CustomFieldValue;
use Academy\System\Models\CustomFieldResult;

class CustomFields {

    public static function init($value, $column, $record)
    {
        if ($field = CustomField::find($column->valueFrom)) {
            $query = CustomFieldResult::where('attachment_id', $record->getkey())
                ->where('attachment_type', get_class($record))
                ->where('field_id', $column->valueFrom);

            switch ($field->type->code) {
                case 'dropdown':
                case 'radio':
                    return e(($first = $query->first()) && $first->value ? $first->value->name : '');
                case 'checkbox':
                    $result = [];
                    $query->select('value_id');
                    foreach ($query->get() as $item) {
                        if ($item->value) {
                            $result[] = $item->value->name;
                        }
                    }
                    return e(implode(', ', $result));
                case 'text':
                case 'string':
                case 'datetime':
                    $query->select('data');
                    return e(implode(', ', $query->lists('data')));
                default:
                    return;
            }
        }
        $values = [];
        foreach ($value as $item) {
            $values[$item->field->name][] = $item->value ? $item->value->name : $item->data;
        }
        $results = '<table>';
        foreach ($values as $name => $value) {
            $results .= '<tr><td>' . e($name) . '</td><td>' . e(implode(', ', $value)) . '</td></tr>';
        }
        $results .= '</table>';
        return $results;
    }

    public static function addColumns($widget, $column)
    {
        $modelQuery = $widget->prepareModel();
        $widget->removeColumn($column->columnName);
        if (!$model = $modelQuery->getModel()) {
            return;
        }
        if (isset($column->config['scope'])) {
            $scope = $column->config['scope'];
        }
        else {
            $scope = $column->columnName;
        }
        $query = CustomField::whereRaw(1);
        $model->{"scope$scope"}($query);

        $columns = [];
        foreach ($query->get() as $record) {

            $columns['f' . $record->id] = [
                'label' => $record->name,
                'type' => 'customfields',
                'sortable' => false,
                'valueFrom' => $record->id,
            ];
        }
        $widget->addColumns($columns);
    }

    public static function listen()
    {
        \Event::listen('backend.list.extendColumns', function($widget)
        {
            static $widgets = [];
            if (!isset($widgets[$hash = spl_object_hash($widget)])) {
                $widgets[$hash] = true;
                foreach ($widget->getColumns() as $column) {
                    if ($column->type == 'customfields') {
                        Self::addColumns($widget, $column);
                    }
                }
            }
        });
    }

    public static function registration()
    {
        return [Self::class, 'init'];
    }
}