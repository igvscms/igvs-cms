<?php namespace Academy\System\Behaviors;

use Backend\Classes\ControllerBehavior;
use Flash;
use Str;

class CrudController extends ControllerBehavior
{
    //use \Academy\System\Traits\AntiCache;

    private $modelClass;
    public $dataValues;
    public $runButton = null;

    public function __construct($controller)
    {
        parent::__construct($controller);
        # config

        $yaml = $this->controller->formConfig ?: $this->controller->listConfig;
        $config = $this->makeConfig($yaml, ['modelClass']);
        $this->modelClass = Str::normalizeClassName($config->modelClass);

        $this->addJs('js/main.js');
        $this->addCss('css/main.css');

        $this->dataValues = $this->input_encode(is_array(get('data')) ? get('data') : []);
    }

    private function json_encode($array, $brace = true)
    {
        if(is_array($array)) {

            foreach($array as $key => $value)
                $res[] = htmlspecialchars($key).':'.$this->json_encode($value);

            if($brace)
                $result = '{'.implode(', ', $res).'}';
            else
                $result = implode(', ', $res);
        }
        else
            $result = '\''.htmlspecialchars($array).'\'';

        return $result;
    }

    private function input_encode($array, $parent_key = '')
    {
        $result = '';
        foreach($array as $key => $value) {

            if($parent_key != '')
                    $key = $parent_key.'['.htmlspecialchars($key).']';

            if(is_array($value))
                $result .= $this->input_encode($value,$key);
            else
                $result .= '<input type="hidden" name="'.$key.'" value="'.htmlspecialchars($value).'">';
        }

        return $result;
    }

    public function formExtendModel($model)
    {
        $data = get('dataModel');
        if(is_array($data))
            foreach($data as $filed => $value)
                $model->$filed = $value;

        return $model;
    }

    public function crudcreate($context = null)
    {
        $this->controller->layout = __dir__.'/crudcontroller/layouts/default';
        $this->addCss('css/layout.css',['><base target' => '_blank']);

        $this->controller->asExtension('FormController')
            ->create($context);

        if (is_null($this->runButton)) {
            $this->runButton = 'backend::lang.form.create';
        }

        return $this->makePartial('edit');
    }

    public function crudcreate_onSave()
    {
        $this->controller->asExtension('FormController')
            ->create_onSave();

        return [
            'model' => $this->controller->formGetModel()->toArray()
        ];
    }
 
    public function crudupdate($recordId = null, $context = null)
    {

        $this->controller->layout = __dir__.'/crudcontroller/layouts/default';
        $this->addCss('css/layout.css',['><base target' => '_blank']);

        $var = $this->controller->asExtension('FormController')
            ->update($recordId, $context);

        if (post('_relation_field')) {
            return $var;
        }

        if (is_null($this->runButton)) {
            $this->runButton = 'backend::lang.form.save';
        }

        return $this->makePartial('edit');
    }

    public function crudupdate_onSave($recordId = null, $context = null)
    {
        $this->controller->asExtension('FormController')
            ->update_onSave($recordId, $context);

        return [
            'model' => $this->controller->formGetModel()->toArray()
        ];
    }

    public function onDelete()
    {
        $checked = post('checked');
        $class = $this->modelClass;

        if (is_array($checked) && $checked) {

            $class::whereIn('id', $checked)
                ->delete();

            Flash::success('Записи успешно удалены');

            return $this->controller->listRefresh();
        }
    }
}