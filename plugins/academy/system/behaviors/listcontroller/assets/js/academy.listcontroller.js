Vue.component('academy-systsem-list-behavior', {
    template:
        '<div>\
            <slot name="toolbar" :records="records" @search="rescopes"></slot>\
            <slot name="filter" @filter="rescopes"></slot>\
            <slot name="list" v-model="records"></slot>\
        </div>',
    data: function() {
        return {
            records: []
        };
    },
    methods: {
        rescopes: function() {
            alert('req');
            //this.$slots.list.on('rescopes');
        }
    }
});