if (window == window.top) {
    function switchNotifier() {
        $(".notifier").toggleClass('mini');
    }
    function ntfBodyScroll(el, $class, $definition) {
        $count = $(el).children('.count').text() - 0;
        if ($(el).children('.content').height() - $(el).scrollTop() <= 2 * $(el).height() && !$(el).prop('wait') && $count != -1) {
            
            $(el).prop('wait', true);
            $.request('onNotifierPaginate', {
                data: {
                    class: $class,
                    definition: $definition,
                    offset: $count
                },
                success: function(data) {
                    $(el).prop('wait', false);
                    return this.success(data);
                }
            });
        }
    }

    var notifierGetGroupWait = false;
    function notifierGetGroup(el, $class, $definition) {
        if (!notifierGetGroupWait) {
            notifierGetGroupWait = true;
            $.request('onNotifierPaginate', {
                data: {class: $class, definition: $definition},
                success: function(data) {
                    notifierGetGroupWait = false;
                    return this.success(data);
                }
            });
        }
        $(el).attr('onClick', '');
    };

    function notifierUpdate() {
        $.request('onTopicalNotificationsCount', {
            success: function(data) {
                $allCount = 0;
                for (i = 0; i < data.topical.length; i++) {
                    $objectClass = data.topical[i].class;
                    $title = data.topical[i].title;
                    $definition = data.topical[i].definition;
                    $count = data.topical[i].count;
                    $allCount += $count;

                    $classDefinition = $objectClass + '@' + $definition;
                    $classDefinition = $classDefinition.replace(/&/, "&amp;").replace(/"/g, "&quot;");

                    $class = $objectClass.replace(/&/, "&amp;").replace(/"/g, "&quot;").replace(/\\/g, '\\\\');
                    $definition = $definition.replace(/&/, "&amp;").replace(/"/g, "&quot;").replace(/\\/g, '\\\\');

                    $('.ntf-body-block').append(
                        '<div id="' + $classDefinition + '">\
                            <label class="ntf-item" for="spoiler-' + $classDefinition + '" onClick="notifierGetGroup(this, \'' + $class + '\',\'' + $definition + '\')">' + 
                                $title +
                                '<span class="ntf-item-count" id="count-' + $classDefinition + '">' + $count + '</span>\
                                <span id="update-' + $classDefinition + '"></span>\
                            </label>\
                            <input type="radio" name="ntf-radio" class="ntf-radio" id="spoiler-' + $classDefinition + '">\
                            <div>\
                                <div onScroll="ntfBodyScroll(this, \'' + $class + '\',\'' + $definition + '\')" style="height:290px;overflow:auto">\
                                    <div class="count" style="display:none"></div>\
                                    <table><tbody class="content"></tbody></table>\
                                </div>\
                                <form class="filters" style="position:absolute;right:100%;top:0;min-width:50%;z-index:-1;" data-request-data="class: \'' + $class + '\', definition: \'' + $definition + '\'"></form>\
                            </div>\
                        </div>'
                    );
                }
                if ($allCount) {
                    $('.ntf-signal').text($allCount);
                }
            }
        });
    }

    function getCoords(elem) {
        return box = elem.getBoundingClientRect();
    }

    $(document).ready(function () {

        if ($.cookie('notifier_position_right') || $.cookie('notifier_position_bottom')) {
            $style = ' style="right:' + $.cookie('notifier_position_right') + 'px;bottom:' + $.cookie('notifier_position_bottom') + 'px"';
        }
        else {
            $style = '';
        }

        $('body').append('\
        <div id="ball" class="notifier mini"'+ $style +'>\
            <div class="ntf-header">\
                <span class="ntf-caption">Notifier</span>\
                <span class="ntf-info"><span class="ntf-signal"></span></span>\
                <span class="ntf-setup icon-cog" data-control="popup" data-handler="onNotifierSetup"></span>\
                <span class="ntf-close" onClick="switchNotifier()"></span>\
            </div>\
            <div class="ntf-body">\
                <div class="ntf-body-block"></div>\
            </div>\
        </div>'); // onScroll="ntfBodyScroll(this)"




        var ball = document.getElementById('ball');
        var ballOption;

        var onDown = function(e) {
            if (e.touches) {
                e = e.touches[0];
            }
            var coords = getCoords(ball);
            ballOption = {
                shiftX: e.clientX - coords.right,
                shiftY: e.clientY - coords.bottom
            };
        };
        var onMove = function(e) {
            if (e.touches) {
                e = e.touches[0];
            }
            if (ballOption) {
                ball.style.right = document.documentElement.clientWidth - (e.clientX - ballOption.shiftX) + 'px';
                ball.style.bottom = document.documentElement.clientHeight - (e.clientY - ballOption.shiftY) + 'px';
            }
        }

        var onUp = function(e) {
            if (ballOption) {
                if (e.changedTouches) {
                    e = e.changedTouches[0];
                }
                $.cookie('notifier_position_right', document.documentElement.clientWidth - (e.clientX - ballOption.shiftX), {path: '/'});
                $.cookie('notifier_position_bottom', document.documentElement.clientHeight - (e.clientY - ballOption.shiftY), {path: '/'});
                ballOption = null;
            }
        };

        ball.addEventListener('mousedown', onDown);
        ball.addEventListener('touchstart', onDown);

        document.addEventListener('mousemove', onMove);
        document.addEventListener('touchmove', onMove);

        document.addEventListener('mouseup', onUp);
        document.addEventListener('touchend', onUp);

        ball.ondragstart = function() {
          return false;
        };

        notifierUpdate();
    });
}