<?php namespace Academy\System\Behaviors;

use Backend\Behaviors\ListController as ControllerBehavior;

class ListController extends ControllerBehavior
{
    public function getComponentName($object)
    {
        return str_replace('\\', '', get_class($object));
    }

    public function listRender($definition = null)
    {
        $this->addJs("https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js");
        $this->addJs('https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js');
        $this->addJs('/plugins/academy/system/assets/js/axios.min.js', 'core');
        $this->addJs('/plugins/academy/system/assets/js/vue-activate.js', 'core');
        $this->addCss('/plugins/academy/system/assets/css/vue-activate.css', 'core');
        $this->addJs('js/academy.listcontroller.js', 'core');

        if (!$definition || !isset($this->listDefinitions[$definition])) {
            $definition = $this->primaryDefinition;
        }
        /*if (isset($this->filterWidgets[$definition])) {
            $this->filterWidgets[$definition]->render();
        }
        $this->listWidgets[$definition]->render();*/

        return Parent::listRender($definition);
    }

    public function makeList($definition = null)
    {
        if (!$definition || !isset($this->listDefinitions[$definition])) {
            $definition = $this->primaryDefinition;
        }
        $listConfig = $this->controller->listGetConfig($definition);

        $class = $listConfig->modelClass;
        $model = new $class;
        $model = $this->controller->listExtendModel($model, $definition);

        $columnConfig = $this->makeConfig($listConfig->list);
        $columnConfig->model = $model;
        $columnConfig->alias = $definition;

        $configFieldsToTransfer = [
            'recordUrl',
            'recordOnClick',
            'recordsPerPage',
            'noRecordsMessage',
            'defaultSort',
            'showSorting',
            'showSetup',
            'showCheckboxes',
            'showTree',
            'treeExpanded',
            'customViewPath',
            'optionsByCurrentPage',
        ];

        foreach ($configFieldsToTransfer as $field) {
            if (isset($listConfig->{$field})) {
                $columnConfig->{$field} = $listConfig->{$field};
            }
        }
        if (isset($listConfig->widgets['list'])) {
            $list = $listConfig->widgets['list'];
        }
        else {
            $list = 'Academy\System\Widgets\Lists';
        }
        if (isset($listConfig->widgets['filter'])) {
            $filter = $listConfig->widgets['filter'];
        }
        else {
            $filter = 'Backend\Widgets\Filter';
        }
        if (isset($listConfig->widgets['toolbar'])) {
            $toolbar = $listConfig->widgets['toolbar'];
        }
        else {
            $toolbar = 'Academy\System\Widgets\Toolbar';
        }
        $widget = $this->makeWidget($list, $columnConfig);

        $widget->bindEvent('filter.update', function() use($widget) {
            return $widget->onRefresh();
        });
        $widget->bindEvent('filter.extendQuery', function($query, $scope) {
            $this->controller->listFilterExtendQuery($query, $scope);
        });

        $widget->bindEvent('list.extendColumns', function() use($widget) {
            $this->controller->listExtendColumns($widget);
        });

        $widget->bindEvent('list.extendQueryBefore', function($query) use($definition) {
            $this->controller->listExtendQueryBefore($query, $definition);
        });

        $widget->bindEvent('list.extendQuery', function($query) use($definition) {
            $this->controller->listExtendQuery($query, $definition);
        });

        $widget->bindEvent('list.injectRowClass', function($record) use($definition) {
            return $this->controller->listInjectRowClass($record, $definition);
        });

        $widget->bindEvent('list.overrideColumnValue', function($record, $column, $value) use($definition) {
            return $this->controller->listOverrideColumnValue($record, $column->columnName, $definition);
        });

        $widget->bindEvent('list.overrideHeaderValue', function($column, $value) use($definition) {
            return $this->controller->listOverrideHeaderValue($column->columnName, $definition);
        });
        $widget->bindToController();

        if (isset($listConfig->toolbar)) {
            $toolbarConfig = $this->makeConfig($listConfig->toolbar);
            $toolbarConfig->alias = $widget->alias . 'Toolbar';
            $toolbarWidget = $this->makeWidget($toolbar, $toolbarConfig);
            $toolbarWidget->bindToController();
            $toolbarWidget->cssClasses[] = 'list-header';

            if ($searchWidget = $toolbarWidget->getSearchWidget()) {
                $searchWidget->bindEvent('search.submit', function() use($widget, $searchWidget) {
                    $widget->setSearchTerm($searchWidget->getActiveTerm());
                    return $widget->onRefresh();
                });

                $widget->setSearchOptions([
                    'mode' => $searchWidget->mode,
                    'scope' => $searchWidget->scope,
                ]);

                $widget->setSearchTerm($searchWidget->getActiveTerm());
            }

            $this->toolbarWidgets[$definition] = $toolbarWidget;
        }
        /*if (isset($listConfig->filter)) {
            $widget->cssClasses[] = 'list-flush';

            $filterConfig = $this->makeConfig($listConfig->filter);
            $filterConfig->alias = $widget->alias . 'Filter';
            $filterWidget = $this->makeWidget($filter, $filterConfig);
            $filterWidget->bindToController();

            $filterWidget->bindEvent('filter.update', function() use($widget, $filterWidget) {
                return $widget->onRefresh();
            });

            $filterWidget->bindEvent('filter.extendQuery', function($query, $scope) {
                $this->controller->listFilterExtendQuery($query, $scope);
            });

            $widget->addFilter([$filterWidget, 'applyAllScopesToQuery']);

            $this->filterWidgets[$definition] = $filterWidget;
        }*/
        return $widget;
    }
}