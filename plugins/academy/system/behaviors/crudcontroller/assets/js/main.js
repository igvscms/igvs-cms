function Crud() {

    var DEFAULT = {
        size: null,
        success: function(data) {
            crud.refreshList();
        },
        complete: function (data) {
        }
    };

    // init
    var $dialog = $("<div>", {
        class: "crud-dialog hidden",
    }).appendTo("body");

    $dialog.html('\
        <div class="popup-backdrop fade in loading">\
            <div class="modal-content popup-loading-indicator"></div>\
        </div>'
    );

    var $iframe = $("<iframe>", {
        name: "crudIframe",
        class: "hidden",
        src: "",
        frameborder: 0,
        load: function () {},
        scrolling: "yes"
    }).appendTo($dialog);

    $iframe.load(function() {
        if ($(this).attr('src')) {
            $(this).removeClass('hidden');
        }
    });

    crud.options = {};

    crud.close = function() {
        frames.crudIframe.$("body").css({"overflow": "hidden"});
        $dialog.addClass("dialog-close");

        setTimeout(function() {
            $dialog.addClass("hidden");
            $dialog.removeClass("dialog-close");
            $iframe.attr("src", "");
            $("body").css("overflow", "auto");
            $iframe.addClass("hidden");
        }, 300);

        // frames.crudIframe.contentWindow.document.close();
    };

    crud.refreshList = function () {

        var pageCurrent = 1;

        if ($(".page-next").length) {
            var data = $(".page-next").data('request-data');
            pageCurrent = parseInt(data.replace("page:", "")) - 1;
        } else if ($(".page-back").length) {
            var data = $(".page-back").data('request-data');
            pageCurrent = parseInt(data.replace("page:", "")) + 1;
        }

        $.request('onPaginate', {
            data: {page: pageCurrent}
        });
    }

    function crud(src, options) {

        crud.options = $.extend({}, DEFAULT, options);

        $("body").css("overflow", "hidden");
        $dialog.removeClass("hidden");

        var size = crud.options.size || 'huge';
        var dataModel = {dataModel: crud.options.dataModel || []};
        var data = {data: crud.options.data || []};

        if (!isNaN(parseFloat(size)) && isFinite(size)) {
            size = parseInt(size) + 100;
        } else {
            size = crud.options.size;
        }

        var baseUrl = $.oc.backendUrl ? $.oc.backendUrl(src)
            : '/cp/' + src

        $iframe
            .attr('src', baseUrl + '?size=' + size + '&' + $.param(dataModel) +'&' + $.param(data) + "#");
    };

    return crud;
}

$(document).ready(function() {
    $.crud = Crud();
})