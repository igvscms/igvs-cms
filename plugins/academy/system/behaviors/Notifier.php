<?php namespace Academy\System\Behaviors;

use Backend\Classes\ControllerBehavior;
use File;

use Academy\System\Models\Notifier as NotifierModel;
use October\Rain\Html\Helper as HtmlHelper;

use Academy\System\Classes\PseudoController;

class Notifier extends ControllerBehavior
{
    protected $requiredProperties = ['notifierConfig'];
    protected $requiredConfig = ['modelClass', 'list', 'title'];
    private $filterWidgets;
    private $ignoreFilters = false;
    private $listWidgets = [];
    private $controllers = [];
    private $viewPaths = [];
    public $widget = null;
    private $realController;

    public function __construct($controller = null)
    {
        if (!is_null($controller) && isset($controller::$notifierConfig)) {
            NotifierModel::classRegistration($controller);
        }
        $this->viewPath = $this->configPath = $this->guessViewPath('/partials');
        $this->assetPath = $this->guessViewPath('/assets', true);

        foreach (NotifierModel::lists('class') as $class) {
            if (!class_exists($class)) {
                continue;
            }
            foreach ($this->requiredProperties as $property) {
                if (!isset($class::$$property)) {
                    continue(2);
                }
            }
            if (!is_array($class::$notifierConfig)) {
                $notifierConfig = ['default' => $class::$notifierConfig];
            }
            else {
                $notifierConfig = $class::$notifierConfig;
            }
            foreach ($notifierConfig as $key => $fileName) {
                if (!is_array($fileName) && !is_object($fileName)) {
                    $fileName = $fileName . '@' . $class;
                }
                $this->controllers[$class][$key] = $this->makeConfig($fileName, $this->requiredConfig);
            }
            $this->viewPaths[$class] = $this->guessViewPathFrom($class);
        }
        $this->controller = $this->realController = $controller;
        if (is_object($controller)) {
            $this->addCss('css/main.css');
            $this->addJs('js/main.js');
            $this->widget = &$controller->widget;
        }
        $this->controller = null;
    }

    public function makeFileContents($filePath, $extraParams = [])
    {
        return $this->localMakeFileContents($filePath, $extraParams);
    }

    public function getConfigPath($fileName, $configPath = null)
    {
        list($fileName, $class) = explode('@', $fileName, 2);

        if (File::isLocalPath($fileName) || realpath($fileName) !== false) {
            return $fileName;
        }
        if (!$configPath) {
            $fileName = File::symbolizePath($fileName);
            $configPath = !empty($class) ? $this->guessConfigPathFrom($class) : $this->guessConfigPath();
        }
        foreach ((array) $configPath as $path) {
            if (File::isFile($_fileName = $path . '/' . $fileName)) {
                return $_fileName;
            }
        }
        return $fileName;
    }

    private function getDefinition($class, $definition = null)
    {
        if (!$definition || !isset($this->controllers[$class][$definition])) {
            reset($this->controllers[$class]);
            return key($this->controllers[$class]);
        }
        return $definition;
    }

    private function runNotifierFunction($class, $function, $args, $default = null)
    {
        if (method_exists($class, $function)) {
            return call_user_func_array([$class, $function], $args);
        }
        return $default;
    }

    public function makeList($class, $definition = null)
    {
        $this->controller = new PseudoController($this->realController,  $this->viewPaths[$class]);
        $definition = $this->getDefinition($class, $definition);
        $listConfig = $this->runNotifierFunction($class, 'notifierGetConfig', [$definition], $this->controllers[$class][$definition]);

        $modelClass = $listConfig->modelClass;
        $model = new $modelClass;
        $model = $this->runNotifierFunction($class, 'notifierExtendModel', [$model, $definition], $model);

        if (is_string($listConfig->list)) {
            $list = $listConfig->list . '@' . $class;
        }
        else {
            $list = $listConfig->list;
        }
        $columnConfig = $this->makeConfig($list);
        $columnConfig->model = $model;
        $columnConfig->alias = "$class@$definition";

        $configFieldsToTransfer = [
            'recordUrl',
            'recordOnClick',
            'defaultSort',
            'showSorting',
            'customViewPath',
        ];

        foreach ($configFieldsToTransfer as $field) {
            if (isset($listConfig->{$field})) {
                $columnConfig->{$field} = $listConfig->{$field};
            }
        }
        $columnConfig->showPagination = true;
        $columnConfig->recordsPerPage = 20;

        $widget = $this->makeWidget('Backend\Widgets\Lists', $columnConfig);

        $widget->bindEvent('list.extendColumns', function () use ($class, $widget) {
            $this->runNotifierFunction($class, 'notifierExtendColumns', [$widget]);
        });
        $widget->bindEvent('list.extendQueryBefore', function ($query) use ($class, $definition) {
            $this->runNotifierFunction($class, 'notifierExtendQueryBefore', [$definition]);
        });
        $widget->bindEvent('list.extendQuery', function ($query) use ($class, $definition) {
            $this->runNotifierFunction($class, 'notifierExtendQuery', [$query, $definition]);
        });
        $widget->bindEvent('list.injectRowClass', function ($record) use ($class, $definition) {
            return $this->runNotifierFunction($class, 'notifierInjectRowClass', [$record, $definition]);
        });
        $widget->bindEvent('list.overrideColumnValue', function ($record, $column, $value) use ($class, $definition) {
            return $this->runNotifierFunction($class, 'notifierOverrideColumnValue', [$record, $column->columnName, $definition]);
        });
        $widget->bindEvent('list.overrideHeaderValue', function ($column, $value) use ($class, $definition) {
            return $this->runNotifierFunction($class, 'notifierOverrideHeaderValue', [$column->columnName, $definition]);
        });
        $widget->bindToController();

/*        if (isset($listConfig->toolbar)) {//потом разобраться
            $toolbarConfig = $this->makeConfig($listConfig->toolbar);
            $toolbarConfig->alias = $widget->alias . 'Toolbar';
            $toolbarWidget = $this->makeWidget('Backend\Widgets\Toolbar', $toolbarConfig);
            $toolbarWidget->bindToController();
            $toolbarWidget->cssClasses[] = 'list-header';

            if ($searchWidget = $toolbarWidget->getSearchWidget()) {
                $searchWidget->bindEvent('search.submit', function () use ($widget, $searchWidget) {
                    $widget->setSearchTerm($searchWidget->getActiveTerm());
                    return $widget->onRefresh();
                });
                $widget->setSearchOptions([
                    'mode' => $searchWidget->mode,
                    'scope' => $searchWidget->scope,
                ]);
                $widget->setSearchTerm($searchWidget->getActiveTerm());
            }

            $this->toolbarWidgets[$definition] = $toolbarWidget;
        }*/
        if (!$this->ignoreFilters && isset($listConfig->filter)) {
            if (is_string($listConfig->filter)) {
                $filter = $listConfig->filter . '@' . $class;
            }
            else {
                $filter = $listConfig->filter;
            }
            $filterConfig = $this->makeConfig($filter);
            $filterConfig->alias = $widget->alias . 'Filter';
            $filterConfig->list = $widget;
            $filterWidget = $this->filterWidgets[$class][$definition] = $this->makeWidget('Academy\Cms\Widgets\Filter', $filterConfig);
            $filterWidget->bindToController();

            $filterWidget->bindEvent('filter.update', function () {
                return $this->onNotifierPaginate();
            });
            $filterWidget->bindEvent('filter.extendQuery', function($query, $scope) use($class) {
                $this->runNotifierFunction($class, 'notifierFilterExtendQuery', [$query, $scope]);
            });
            $widget->addFilter([$filterWidget, 'applyAllScopesToQuery']);
        }

        return $this->listWidgets[$class][$definition] = $widget;
    }

    public function makeConfig($configFile = [], $requiredConfig = [])
    {
        $controller = $this->controller;
        $this->controller = null;
        $result = parent::makeConfig($configFile, $requiredConfig);
        $this->controller = $controller;
        return $result;
    }

    public $debag = false;
    public function makeLists()
    {
        foreach ($this->controllers as $class => $options) {
            foreach ($options as $definition => $config) {
                $this->debag = true;
                $this->makeList($class, $definition);
            }
        }
        return $this->listWidgets;
    }

    private function getNotificationsRows($class, $definition, &$offset = 0, $limit = 20)
    {
        $listWidget = $this->listWidgets[$class][$definition];

        $prevOffset = $offset;
        $records = $listWidget->prepareModel()
                              ->offset($prevOffset)
                              ->limit($limit)
                              ->get();
        if (count($records) == 0) {
            $offset = -1;
        }
        else {
            $offset = $prevOffset + count($records);
        }
        $listWidget->vars = [
            'records' => $records,
            'treeLevel' => 0,
            'showTree' => false,
            'showCheckboxes' => false,
            'columns' => $listWidget->getVisibleColumns(),
            'showSetup' => false
        ];
        return $listWidget->makePartial('list_body_rows');
    }

    private function getNotificationsHead($class, $definition)
    {
        $listWidget = $this->listWidgets[$class][$definition];
        return $this->makePartial('list_head_row', [
            'listWidget' => $listWidget,
            'columns' => $listWidget->getVisibleColumns(),
            'showSorting' => $listWidget->showSorting,
            'class' => $class,
            'definition' => $definition,
        ]);
    }

    public function getFilters($class, $definition)
    {
        if (!isset($this->filterWidgets[$class][$definition])) {
            return;
        }
        $widget = $this->filterWidgets[$class][$definition];
        $widget->prepareVars();
        return $this->makePartial('filter', ['widget' => $widget]);
    }

    /*public function makePartial($partial, $params = [], $throwException = true)
    {
        $this->viewPath = $this->viewPaths[post('class')];
        return parent::makePartial($partial, $params, $throwException);
    }*/

    public function onNotifierPaginate()
    {
        if (($class = post('class'))
            && ($definition = post('definition'))
        ) {
            $this->makeList($class, $definition);
            $offset = post('offset', 0);
            $id = "#" . str_replace(['\\', '@'], ['\\\\', '\@'], $class . '@' . $definition);
            if ($offset) {
                return [
                    "@" . $id . " .content" => $this->getNotificationsRows($class, $definition, $offset),
                    $id . " .count" => $offset,
                ];
            }
            return [
                    $id . " .content" => $this->getNotificationsHead($class, $definition) . $this->getNotificationsRows($class, $definition, $offset),
                    $id . " .count" => $offset,
                    $id . ' .filters' => $this->getFilters($class, $definition)
                ];
        }
    }

    public function onTopicalNotificationsCount()
    {
        $this->ignoreFilters = true;
        $this->makeLists();
        $result = $this->getSetupDefinition();
        foreach ($result as $num => $option) {
            if (!$option['visible']) {
                unset($result[$num]);
                continue;
            }
            $result[$num]['count'] = $this->listWidgets[$option['class']][$option['definition']]
                ->prepareModel()
                ->count();
        }
        return ['topical' => array_values($result)];
    }

    public function onGetNotificationCount($class, $definition = null)
    {
        $allCounts = $this->onTopicalNotificationsCount();
        $allCounts = $allCounts['topical'];
        $definition = $this->getDefinition($class, $definition);
        $mainCount = 0;
        $allCount = 0;

        foreach ($allCounts as $options) {
            if ($options['class'] == $class && $options['definition'] == $definition) { 
                $mainCount = $options['count'];
            }
            $allCount += $options['count'];
        }
        $rClass = str_replace('\\', '\\\\', $class);
        $rDefinition = str_replace('\\', '\\\\', $definition);
        $id = $rClass . '\@' . $rDefinition;
        return [
            "#count-" . $id => $mainCount,
            "#update-" . $id => '<span class="ntf-item-update oc-icon-refresh" onclick="notifierGetGroup(this, \'' . $rClass . '\',\'' . $rDefinition . '\')"></span>',
            '.ntf-info' => '<span class="ntf-signal">' . $allCount . '</span>'
        ];
    }

    public function onNotifierSort()
    {
        if (($class = post('class'))
            && ($definition = post('definition'))
        ) {
            $this->makeList($class, $definition);
            $listWidget = $this->makeList($class, $definition);
            $listWidget->onSort();
            return $this->onNotifierPaginate();
        }
    }

    public function onNotifierFilterGetOptions()
    {
        if (($class = post('class'))
            && ($definition = post('definition'))
        ) {
            $this->makeList($class, $definition);
            return $this->filterWidgets[$class][$definition]->onFilterGetOptions();
        }
    }

    public function onNotifierFilterUpdate()
    {
        if (($class = post('class'))
            && ($definition = post('definition'))
        ) {
            $this->makeList($class, $definition);
            return $this->filterWidgets[$class][$definition]->onFilterUpdate();
        }
    }

    private function getDefinitionSort(&$setupDefinitions)
    {
        $setup = [];
        $sort = 0;
        foreach ($setupDefinitions as $num => $option) {
            if (!isset($this->controllers[$option['class']][$option['definition']])) {
                unset($setupDefinitions[$num]);
                continue;
            }
            $setup[$option['class']][$option['definition']] = $sort++;
            $setupDefinitions[$num]['title'] = $this->controllers[$option['class']][$option['definition']]->title;
        }
        $setupDefinitions = array_values($setupDefinitions);
        return $setup;
    }

    private function getSetupDefinition()
    {
        $setupDefinitions = $this->getSession('setup', []);
        $setup = $this->getDefinitionSort($setupDefinitions);

        $sort = 0;
        foreach ($this->controllers as $class => $definitions) {
            foreach ($definitions as $definition => $option) {
                if (!isset($setup[$class][$definition])) {
                    $setup[$class][$definition] = $sort - 0.5;
                    $setupDefinitions[] = [
                        'definition' => $definition,
                        'class' => $class,
                        'visible' => empty($option->invisible),
                        'title' => $option->title
                    ];
                }
                $sort++;
            }
        }
        usort($setupDefinitions, function($a, $b) use ($setup) {
            $res = $setup[$a['class']][$a['definition']] - $setup[$b['class']][$b['definition']];
            if ($res >= 0) {
                return !!$res;
            }
            return -1;
        });
        return $setupDefinitions;
    }

    public function onNotifierSetup()
    {
        $this->vars['setupDefinition'] = $this->getSetupDefinition();
        return $this->makePartial('setup_form');
    }

    protected function putSession($key, $value)
    {
        $sessionId = $this->makeSessionId();

        $currentStore = $this->getSession();
        $currentStore[$key] = $value;

        \Session::put($sessionId, base64_encode(serialize($currentStore)));
    }

    /**
     * Retrieves a widget related key/value pair from session data.
     * @param string $key Unique key for the data store.
     * @param string $default A default value to use when value is not found.
     * @return string
     */
    protected function getSession($key = null, $default = null)
    {
        $sessionId = $this->makeSessionId();
        $currentStore = [];

        if (
            \Session::has($sessionId) &&
            ($cached = @unserialize(@base64_decode(\Session::get($sessionId)))) !== false
        ) {
            $currentStore = $cached;
        }

        if ($key === null) {
            return $currentStore;
        }

        return isset($currentStore[$key]) ? $currentStore[$key] : $default;
    }

    /**
     * Returns a unique session identifier for this widget and controller action.
     * @return string
     */
    protected function makeSessionId()
    {
        return $this->getId();
    }

    /**
     * Resets all session data related to this widget.
     * @return void
     */
    public function resetSession()
    {
        $sessionId = $this->makeSessionId();
        \Session::forget($sessionId);
    }

    public function getId($suffix = null)
    {
        $id = class_basename(get_called_class());
        if ($suffix !== null) {
            $id .= '-' . $suffix;
        }
        return HtmlHelper::nameToId($id);
    }

    public function onApplyNotifierSetup()
    {
        $setup = [];
        $visible_columns = post('visible_columns', []);
        foreach (post('column_order') as $controller) {
            $setup[] = [
                'definition' => $definition = reset($controller),
                'class' => $class = key($controller),
                'visible' => isset($visible_columns[$class][$definition]),
            ];
        }
        $this->putSession('setup', $setup);
        return ['.ntf-body-block' => '<script>notifierUpdate()</script>'];
    }
}