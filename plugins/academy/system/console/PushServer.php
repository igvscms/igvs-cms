<?php namespace Academy\System\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

use Academy\System\Classes\Socket\Pusher;
use Academy\System\Models\Socket;

use React\EventLoop\Factory as ReactLoop;
use React\ZMQ\Context as ReactContext;
use React\Socket\Server as ReactServer;
use React\Socket\SecureServer;
use Ratchet\Wamp\WampServer;

class PushServer extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'academy.pushserver';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        Socket::whereRaw(true)->forceDelete();
        $loop = ReactLoop::create();
        $pusher = new Pusher;
        $context = new ReactContext($loop);

        $pull = $context->getSocket(\ZMQ::SOCKET_PULL);
        $pull->bind('tcp://0.0.0.0:5555');
        $pull->on('message', [$pusher, 'broadcast']);
        $pull->on('error', function(\Exception $e) {
            $this->info('error: ' . $e->getMessage());
        });

        $port = $this->option('port');

        $webSoc = new ReactServer($loop);

        if ($cert = $this->option('cert')) {
            $options = [
                'local_cert' => $cert,
                'verify_peer' => false,
            ];
            if ($key = $this->option('key')) {
                $options['local_pk'] = $key;
            }
            $webSoc = new SecureServer(
                $webSoc,
                $loop,
                $options
            );
        }
        $webSoc->listen($port, '0.0.0.0');

        $webServer = new IoServer(
            new HttpServer(
                new WsServer(
                    new WampServer(
                        $pusher
                    )
                )
            ),
            $webSoc
        );

        $cert_contents = @file_get_contents($cert);
        $key_contents = @file_get_contents($key);

        $this->info('Run PusherServer: "' . $cert . ' ' . $key . '"'.', lens: '.strlen($cert_contents).', '.strlen($key_contents));


        $loop->run();
        // $this->output->writeln('Hello world!');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['cert', null, InputOption::VALUE_OPTIONAL, 'local certificate address \',\'', ''],
            ['key', null, InputOption::VALUE_OPTIONAL, 'local certificate private key', ''],
            ['port', null, InputOption::VALUE_OPTIONAL, 'service port \',\'', 8080],
        ];
    }
}
