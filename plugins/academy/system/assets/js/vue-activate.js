Vue.use(function(Vue, options) {
    var csrf = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

    function ucFirst(str) {
        if (!str) {
            return str;
        }
        return str[0].toUpperCase() + str.slice(1);
    }

    function run(handler, params, then, other) {
        axios.request({
                method: 'post',
                url: window.location.href,
                data: params,
                headers: {
                    'X-CSRF-TOKEN': csrf,
                    'X-OCTOBER-REQUEST-HANDLER': handler,
                    'X-OCTOBER-REQUEST-PARTIALS': '',
                    'X-Requested-With': 'XMLHttpRequest',
                }
            })
            .then(function(response) {
                then(response.data, other || params);
            })
            .catch(function(error) {
                console.log('error', error);
            });
    }
    Vue.prototype.on = function(handler, params, other) {
        run(this.alias + '::on' + ucFirst(handler), params, this[handler], other);
    }
    Vue.prototype.controller = function(handler, params) {
        run('on' + ucFirst(handler), params, this[handler]);
    }
});

Vue.component('partial-widget', {
    props: {
        template: String
    },
    render: function(h) {
        return Vue.compile(this.template)
            .render
            .call(this.$parent._renderProxy, h);
    }
});

Vue.component('native', {
    props: {
        tag: String
    },
    render: function(h) {
        return h(this.tag, this.$slots.default);
    }
});

Vue.component('vuejs', {
    props: {
        name: String,
        options: {
            type: [Object, String],
            default: function() {
                return {}
            }
        },
        config: {
            type: [Object, String],
            default: function() {
                return null
            }
        }
    },
    beforeMount: function() {
        Vue.component(this.name, this.realOptions);
    },
    render: function(h) {
        return h(
            this.$vnode.data.tag,
            [h(this.name, this.realConfig, this.$children)]
        );
    },
    computed: {
        realOptions: function() {
            if (typeof this.options === 'string') {
                return eval('(' + this.options + ')');
            }
            return this.options;
        },
        realConfig: function() {
            if (typeof this.config === 'string') {
                return eval('(' + this.config + ')');
            }
            return this.config;
        }
    }
});

Vue.component('modal-widget', {
    template:
        '<div class="modal modal-widget" :style="{display: value ? \'block\' : null}" v-if="value">\
            <div class="modal-dialog">\
                <div class="modal-content">\
                    <button type="button" class="close" @click="$emit(\'input\', null)">×</button>\
                    <slot></slot>\
                </div>\
            </div>\
        </div>',
    props: ['value']
});

Vue.component('check-widget', {
    template:
        '<div class="check-widget" @click="click" :class="{checked: value, radio: isRadio, uncheck: isSwitch && status === false}" tabindex="0">\
            <slot></slot>\
        </div>',
    sisters: {},
    props: {
        value: {
            default: null
        },
        type: {
            type: String,
            validator: function(type) {
                return ['radio', 'switch', 'checkbox'].indexOf(type) !== -1;
            },
            default: 'checkbox'
        },
        maxCount: {
            type: Number,
            default: null
        },
        name: {
            type: String,
            default: null
        }
    },
    methods: {
        click: function() {
            if (this.isSwitch) {
                switch (this.status) {
                    case false:
                        this.$emit('input', true);
                        break;
                    case true:
                        this.$emit('input', null);
                        break;
                    case null:
                        this.$emit('input', false);
                        break;
                }
            }
            else {
                this.$emit('input', this.isRadio || !this.value);
            }
        }
    },
    computed: {
        status: function() {
            if (!this.isSwitch) {
                return !!this.value;
            }
            if (this.value) {
                return true;
            }
            return [null, undefined, -1].indexOf(this.value) < 0 ? false : null;
        },
        isRadio: function() {
            return this.type === 'radio';
        },
        isSwitch: function() {
            return this.type === 'switch';
        },
        isCheckbox: function() {
            return this.type === 'checkbox';
        },
        mc: function() {
            if (this.maxCount !== null) {
                return this.maxCount;
            }
            return this.isRadio;
        }
    },
    watch: {
        name: {
            handler: function(name, prev) {
                if (!this.value) {
                    return;
                }
                if (prev) {
                    var sisters = this.$options.sisters[prev];
                    if (sisters) {
                        for (var i = 0; i < sisters.length; i++) {
                            if (sisters[i] === this) {
                                sisters[i].$emit('input', !sisters[i].value);
                                sisters.splice(i, 1);
                                break;
                            }
                        }
                    }
                }
                if (name) {
                    if (!this.$options.sisters[name]) {
                        Vue.set(this.$options.sisters, name, []);
                    }
                    this.$options.sisters[name].push(this);
                }
            },
            immediate: true
        },
        value: {
            handler: function(value, prev) {
                if (!prev === !value && this.name) {
                    return;
                }
                if (!this.$options.sisters[this.name]) {
                    Vue.set(this.$options.sisters, this.name, []);
                }
                var sisters = this.$options.sisters[this.name];
                if (prev) {
                    for (var i = 0; i < sisters.length; i++) {
                        if (sisters[i] === this) {
                            sisters.splice(i, 1);
                            break;
                        }
                    }
                }
                else {
                    sisters.push(this);
                    if (this.name
                        && this.mc != 0
                        && sisters
                        && sisters.length > Math.abs(this.mc)
                    ) {
                        var del;
                        if (this.mc < 0) {
                            del = sisters.splice( - sisters.length - this.mc);
                        }
                        else {
                            del = sisters.splice(0, sisters.length - this.mc);
                        }
                        for (var i = 0; i < del.length; i++) {
                            del[i].$emit('input', !del[i].value);
                        }
                    }
                    
                }
            },
            immediate: true
        }
    }
});

document.addEventListener('DOMContentLoaded', function() {
    window.app = new Vue({el: '#layout-canvas'});
});