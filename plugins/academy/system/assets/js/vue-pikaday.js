Vue.component('pikaday', {
    template: '<div class="pikaday"></div>',
    props: {
        value: {
            type: [Date, String, Number],
            default: null
        },
        format: String,
        firstDay: {
            type: Number,
            default: 1
        },
        lang: Object
    },
    data: function() {
        return {
            pickaday: null
        }
    },
    mounted: function() {
        var option = {
            trigger: this.$el,
            onSelect: this.select,
            defaultDate: this.date,
            firstDay: this.firstDay,
            
        }
        console.log($.oc.lang);
        if (this.lang) {
            option.i18n = this.lang;
        }
        else if ($.oc.lang.locale === 'ru') {
            option.i18n = {
                previousMonth : 'Предыдущий месяц',
                nextMonth     : 'Следующий месяц',
                months        : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                weekdays      : ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
                weekdaysShort : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб']
            }
        }
        this.pickaday = new Pikaday(option);
        this.$el.appendChild(this.pickaday.el);
    },
    computed: {
        clearFormat: function() {
            return ['object', 'string', 'number'].indexOf(this.format) >= 0;
        },
        type: function() {
            if (this.format) {
                if (this.clearFormat) {
                    return this.format;
                }
                return 'string';
            }
            return typeof this.value;
        },
        date: function() {
            switch (typeof this.value) {
                case 'object':
                    if (this.value instanceof Date) {
                        return this.value;
                    }
                    return new Date();
                case 'string':
                    return Date.parse(this.value);
                case 'number':
                    var date = new Date();
                    date.setTime(this.value);
                    return date;
            }
        }
    },
    methods: {
        dateToValue: function(date) {
            switch (this.type) {
                case 'object':
                    if (date instanceof Date) {
                        return date;
                    }
                    return new Date();
                case 'string':
                    if (typeof moment !== 'function' || this.clearFormat) {
                        return date.toDateString();
                    }
                    return moment(date).format(this.format);
                case 'number':
                    return date.getTime();
            }
        },
        select: function(date) {
            this.$emit('input', this.dateToValue(date));
        }
    }
});