function Socket() {

    if (!document.currentScript) {
        scripts = document.getElementsByTagName('script');
        scriptHref = scripts[scripts.length-1].src;
    }
    else {
        scriptHref = document.currentScript.src;
    }
    socket = new Object()
    socket.functions = {}
    socket.special;

    socket.on = function(type, callback) {
        socket.functions[type] = callback
    }

    socket.on('getClientResult', function(result) {
        $.request('onGetSocketClientResult', {
            data: result,
            success: function(data) {
                return this.success(data.original);
            }
        });
    });

    socket.newAbConnect = function() {
        socket.obj.subscribe('socket_sender', function (topic, packet) {
            if(socket.functions[packet.type]) {
                return socket.functions[packet.type](packet);
            }
            else if (!socket.special) {
                $.request('onThisHandlerShouldNotExist', {
                    error: function() {
                        socket.special = this;
                        return this.success(packet.result.original);
                    }
                })
            }
            else {
                return socket.special.success(packet.result.original);
            }
        });
    }

    socket.connect = function(_socket) {
        socket.obj = _socket;

        $(document).on('ajaxSend', function(event, context, p3) {
            p3.data = p3.data + "&socket_session_id=" + socket.obj._session_id;
        });
        socket.newAbConnect();
    }

    socket.loadFromLog = function($data, $callback) {

        $.request('onLoadSocketsFromLog', {
            data: $data,
            success: function(data) {
                for (i = 0; i < data.length; i++) {
                    if ($callback) {
                        $callback(data);
                    }
                    else if (socket.functions[data[i].type]) {
                        socket.functions[data[i].type](data[i].result, data.length, i);
                    }
                    else {
                        this.success(data[i].result.original);
                    }
                }
            }
        });
    }

    var conn = new ab.connect(
        window.clientConfig.socketLocation + scriptHref.substr(scriptHref.indexOf('?')),
        socket.connect,
        function (code, reason, detail) {
            //console.wran('WebSocket connection closed: code=' + code + '; reasone=' + reason + '; detail=' + detail);
        },
        {
            maxRetries: 60,
            retryDelay: 4000,
            skipSubprotocolCheck: true
        }
    );
    return socket;
}
$.socket = Socket();