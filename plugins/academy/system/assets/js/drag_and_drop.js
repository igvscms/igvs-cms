$(document).on('mousedown', '.dnd-draggable',//устанавливаем класс - активация, при нажатии
    function()
    {
        $(this).addClass('dndact');
    }
);

$(document).on('mouseup', '.dnd-draggable',//снимаем класс - активация, при отжатии
    function()
    {
        $(this).removeClass('dndact');
    }
);

$(document).on('mouseleave', '.dnd-draggable.dndact',//когда покидаем элемент
    function(e)
    {
        p = $(this).offset();
        old = this;
        find = false;
        $(this).siblings().each(
            function()
            {
                pp = $(this).offset();

                in_pp = pp.top <= e.pageY && pp.top + $(this).height() >= e.pageY;

                if(p.top > e.pageY && (pp.top > e.pageY || in_pp)) {

                    $(this).before(old);
                    $.request('onMove' + $(old).attr('name'), {
                        data: {
                            move: $(old).attr('record_id'),
                            to: $(this).attr('record_id'),
                        }
                    });
                    find = true;
                    return false;
                }

                else if(p.top + $(old).height() < e.pageY && in_pp) {

                    $(this).after(old);
                    $.request('onMove' + $(old).attr('name'), {
                        data: {
                            move: $(old).attr('record_id'),
                            to: $(this).attr('record_id'),
                        }
                    });
                    find = true;
                    return false;
                }
            }
        );
        if(!find) {
            last = $(this).siblings(':last')
            $(last).after(this);
            $.request('onMove' + $(this).attr('name'), {
                data: {
                    move: $(this).attr('record_id'),
                    to: $(last).attr('record_id'),
                }
            });
        }
        $(this).removeClass('dndact');//снимаем класс - активация
    }
);