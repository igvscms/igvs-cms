<?php namespace Academy\System\Drivers;

use Academy\System\Classes\SessionDriver;
use Academy\System\Models\Session;

class UserSession extends SessionDriver
{
    public function read($sessionId)
    {
        if (!\Schema::hasTable('academy_system_session')) {
            return;
        }
        $session_by_session = Session::where('session_id', $sessionId)->first();
        if ($user = \BackendAuth::getUser()) {
            $session_by_user = Session::where('user_id', $user->id)->first();
        }
        else {
            $session_by_user = null;
        }
        if ($session_by_user
            && $session_by_session
                && $session_by_user->session_id != $session_by_session->session_id
        ) {
            $session_by_session->session_id = null;
            $session_by_session->save();
            $session_by_session = null;
        }
        if ($session = $session_by_user ?: $session_by_session) {
            return $session->payload;
        }
    }

    public function write($sessionId, $data)
    {
        if (!\Schema::hasTable('academy_system_session')) {
            return;
        }
        $session_by_session = Session::where('session_id', $sessionId)->first();
        if ($user = \BackendAuth::getUser()) {
            $session_by_user = Session::where('user_id', $user->id)->first();
        }
        else {
            $session_by_user = null;
        }
        if ($session_by_user
            && $session_by_session
                && $session_by_user->session_id != $session_by_session->session_id
        ) {
            $session_by_session->session_id = null;
            $session_by_session->save();
            $session_by_session = null;
            $data = $session_by_user->payload;
        }
        if (!$session = $session_by_user ?: $session_by_session) {
            $session = new Session();
        }
        if ($session->user && !$user) {
            $session->session_id = null;
        }
        else {
            if (is_null($session->session_id)) {
                $session->primaryKey = 'user_id';
            }
            $session->session_id = $sessionId;
            $session->user = $user;
            $session->payload = $data;
        }
        $session->last_activity = time();
        $session->save();
    }

    public function gc($lifetime)
    {
        Session::where('user_id')
            ->where('last_activity', '<=', time() - $lifetime)
            ->delete();
    }
    public function destroy($sessionId)
    {
        Session::where('session_id', $sessionId)
            ->where('user_id')
            ->limit(1)
            ->delete();
    }
}