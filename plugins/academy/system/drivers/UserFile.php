<?php namespace Academy\System\Drivers;

use Academy\System\Abstracts\Driver;
use League\Flysystem\Adapter\Local;
use Illuminate\Support\Arr;
use League\Flysystem\Filesystem as Flysystem;
use SplFileInfo;

class UserFile extends Local
{
    use \Academy\System\Traits\DriverInit;

    protected static $name = 'fileDriver';

    protected static function forExtends()
    {
        list($app, $config) = func_get_args();
        $permissions = isset($config['permissions']) ? $config['permissions'] : [];

        $links = Arr::get($config, 'links') === 'skip'
            ? Local::SKIP_LINKS
            : Local::DISALLOW_LINKS;

        $config2 = Arr::only($config, ['visibility', 'disable_asserts']);

        return new Flysystem(new Static($config['root'], LOCK_EX, $links, $permissions), count($config2) > 0 ? $config2 : null);
    }

    final protected static function getSystemClass()
    {
        return \Storage::class;
    }

    protected function normalizeFileInfo(\SplFileInfo $file)
    {
        if (is_null($result = parent::normalizeFileInfo($file))) {
            $result = $this->mapFileInfo($file);
            if (is_dir($file->getPathname())) {
                $result['type'] = 'dir';
            }
            else {
                $result['type'] = 'file';
            }
        }
        return $result;
    }
}