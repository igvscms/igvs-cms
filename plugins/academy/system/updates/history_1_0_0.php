<?php namespace Academy\System\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class History_1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('academy_system_history', function($table)
        {
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('attachment_id')->unsigned();
            $table->string('attachment_type');
            $table->binary('data');
            $table->timestamps();

            $table->foreign('user_id','f_userId_academySystemHistory')
                ->references('id')
                ->on('backend_users')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_system_history');
    }
}