<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Academy\System\Models\CustomFieldType;

class SeedCustomFieldType_1_0_5 extends Seeder
{
    public function run()
    {
        CustomFieldType::insert([
            ['code' => 'datetime'],
        ]);
    }
}