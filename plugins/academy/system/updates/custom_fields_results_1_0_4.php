<?php namespace Academy\System\Updates;

use October\Rain\Database\Updates\Migration;

class CustomFieldsResults_1_0_4 extends Migration
{
    public function up()
    {
        \Schema::create('academy_system_custom_fields_results', function($table)
        {
            $table->text('data');
            $table->integer('value_id')->unsigned()->nullable();
            $table->integer('field_id')->unsigned();
            $table->integer('attachment_id')->unsigned();
            $table->string('attachment_type');

            $table->foreign('field_id','f_fieldId_academySystemCustomFieldsResults')
                ->references('id')
                ->on('academy_system_custom_fields')
                ->onDelete('cascade');

            $table->foreign('value_id','f_valueId_academySystemCustomFieldsResults')
                ->references('id')
                ->on('academy_system_custom_fields_values')
                ->onDelete('set null');
        });
    }
    public function down()
    {
        \Schema::dropIfExists('academy_system_custom_fields_results');
    }
}