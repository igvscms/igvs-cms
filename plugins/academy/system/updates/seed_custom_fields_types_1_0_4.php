<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Academy\System\Models\CustomFieldType;

class SeedCustomFieldType_1_0_4 extends Seeder
{
    public function run()
    {
        CustomFieldType::insert([
            ['code' => 'checkbox'],//значения будут означать варианты выбора
            ['code' => 'radio'],//значения будут означать варианты выбора
            ['code' => 'dropdown'],//значения будут означать варианты выбора
            //['code' => 'range'],//значения будут означать варианты выбора

            ['code' => 'text'],//значения будут означать варианты масок
            ['code' => 'string'],//значения будут означать варианты масок
            //['code' => 'file'],//значения будут означать варианты масок
        ]);
    }
}