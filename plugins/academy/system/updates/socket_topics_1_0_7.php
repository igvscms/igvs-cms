<?php namespace Academy\System\Updates;

use October\Rain\Database\Updates\Migration;

class Socket_topics_1_0_7 extends Migration
{
    public function up()
    {
        \Schema::table('academy_system_socket_topics', function($table) {
            $table->string('ip');
        });
    }
    public function down() {
        \Schema::table('academy_system_socket_topics', function($table) {
            $table->dropColumn('ip');
        });
    }
}