<?php namespace Academy\System\Updates;

use October\Rain\Database\Updates\Migration;

class CustomFieldsTypes_1_0_4 extends Migration
{
    public function up()
    {
        \Schema::create('academy_system_custom_fields_types', function($table)
        {
            $table->increments('id');
            $table->string('code')->unique();
        });
    }
    public function down()
    {
        \Schema::dropIfExists('academy_system_custom_fields_types');
    }
}