<?php namespace Academy\System\Updates;

use October\Rain\Database\Updates\Migration;

class CustomFields_1_0_4 extends Migration
{
    public function up()
    {
        \Schema::create('academy_system_custom_fields', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->integer('type_id')->unsigned();
            $table->enum('span', ['full', 'auto', 'left', 'right']);
            $table->boolean('is_required');
            $table->integer('attachment_id')->unsigned();
            $table->string('attachment_type');
            $table->foreign('type_id','f_typeId_academySystemCustomFields')
                ->references('id')
                ->on('academy_system_custom_fields_types');
        });
    }
    public function down()
    {
        \Schema::dropIfExists('academy_system_custom_fields');
    }
}