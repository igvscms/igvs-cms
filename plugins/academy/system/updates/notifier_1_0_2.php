<?php namespace Academy\System\Updates;

use October\Rain\Database\Updates\Migration;

class Notifier_1_0_2 extends Migration {
    public function up() {
        \Schema::create('academy_system_notifier', function($table) {
            $table->string('class');
            $table->primary('class');
        });
    }
    public function down() {
        \Schema::dropIfExists('academy_system_notifier');
    }
}