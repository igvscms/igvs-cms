<?php namespace Academy\System\Updates;

use October\Rain\Database\Updates\Migration;

class Socket_packets_1_0_6 extends Migration
{
    public function up()
    {
        \Schema::create('academy_system_socket_packets', function($table)
        {
            $table->string('session_id');
            $table->text('value');
            $table->integer('dt')->unsigned();
            $table->timestamps();

            $table->foreign('session_id','f_sessionId_academySystemSocketpackts')
                ->references('session_id')
                ->on('academy_system_socket_topics')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_system_socket_packets');
    }
}