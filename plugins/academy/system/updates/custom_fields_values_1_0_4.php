<?php namespace Academy\System\Updates;

use October\Rain\Database\Updates\Migration;

class CustomFieldsValues_1_0_4 extends Migration
{
    public function up()
    {
        \Schema::create('academy_system_custom_fields_values', function($table)
        {
            $table->increments('id');
            $table->integer('field_id')->unsigned();
            $table->string('name');
            $table->unique(['field_id', 'name'], 'u_fieldId_name_academySystemCustomFieldsValues');

            $table->foreign('field_id','f_fieldId_academySystemCustomFieldsValues')
                ->references('id')
                ->on('academy_system_custom_fields')
                ->onDelete('cascade');
        });
    }
    public function down()
    {
        \Schema::dropIfExists('academy_system_custom_fields_values');
    }
}