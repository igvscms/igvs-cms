<?php namespace Academy\System\Updates;

use October\Rain\Database\Updates\Migration;

class Session_1_0_3 extends Migration {
    public function up() {
        \Schema::create('academy_system_session', function($table) {
            $table->integer('user_id')->unsigned()->nullable()->unique();
            $table->string('session_id')->nullable()->unique();
            $table->integer('last_activity')->nullable();
            $table->text('payload')->nullable();

            $table->foreign('user_id','f_userId_academySystemSession')
                ->references('id')
                ->on('backend_users')
                ->onDelete('set null');
        });
    }
    public function down() {
        \Schema::dropIfExists('academy_system_session');
    }
}