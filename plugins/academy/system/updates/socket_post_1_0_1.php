<?php namespace Academy\System\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Sockets_post_1_0_1 extends Migration
{
    public function up()
    {
        Schema::create('academy_system_sockets_post', function($table)
        {
            $table->integer('socket_id')->unsigned();
            $table->string('array');
            $table->string('key');
            $table->string('value');

            $table->foreign('socket_id')
                ->references('id')
                ->on('academy_system_sockets')
                ->onDelete('cascade');

            $table->primary(['socket_id','array','key']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_system_sockets_post');
    }
}