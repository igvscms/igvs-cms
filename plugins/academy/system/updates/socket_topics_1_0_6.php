<?php namespace Academy\System\Updates;

use October\Rain\Database\Updates\Migration;

class Socket_topics_1_0_6 extends Migration
{
    public function up()
    {
        \Schema::create('academy_system_socket_topics', function($table)
        {
            $table->string('session_id')->primary();
            $table->string('topic_id');
            $table->string('page')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id','f_userId_academySystemSocketTopics')
                ->references('id')
                ->on('backend_users')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_system_socket_topics');
    }
}