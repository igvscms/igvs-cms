<?php namespace Academy\System\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Sockets_1_0_1 extends Migration
{
    public function up()
    {
        Schema::create('academy_system_sockets', function($table)
        {
            $table->increments('id');
            
            $table->binary('data');
            $table->string('handler');
            $table->string('action');
            $table->string('params');
            $table->string('class');

            $table->string('type');
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('backend_users')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_system_sockets');
    }
}