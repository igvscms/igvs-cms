<?php namespace Academy\System\Traits;

use Academy\System\Classes\Socket\Pusher;
use Academy\System\Models\SocketLog;

trait SocketSender
{
    private $fakeHandler = null;

    //const RESULT_FROM_CLIENT = 'getClientResult';

    public function onGetSocketClientResult()
    {
        return $this->socketMessageResult(post('data'), post('result'));
    }

    final public function getAjaxHandler()
    {
        return $this->fakeHandler ?: parent::getAjaxHandler();
    }

    private function getHandlerLogPost($handler)
    {
        if (!property_exists($this, 'socketActionsLog')) {
            return false;
        }
        if (isset($this->socketActionsLog[$handler])) {
            return $this->socketActionsLog[$handler];
        }
        if (($key = array_search($handler, $this->socketActionsLog)) !== false
            && is_numeric($key)) {
            return null;
        }
        return false;
    }

    private static function getFromFunctionArray($key, $function, $class = null)
    {
        if (is_null($class)) {
            $class = static::class;
        }
        if (method_exists($class, $function)
            && is_array($array = $class::$function())
                && isset($array[$key])
        ) {
            return $array[$key];
        }
        return null;
    }

    final private static function authUserById($id)
    {
        if ($user = \BackendAuth::findUserById($id)) {
            \BackendAuth::login($user, false);
        }
    }

    final private static function setNormalCookie($cookie)
    {
        $urls = [];
        foreach ($cookie as $key => $cook) {
            $urls[] = $key . '=' . $cook;
        }
        parse_str(implode($urls, '&'), $_COOKIE);
    }

    final public function socketMessageResult($data, $result)
    {
        if (is_null($function = self::getFromFunctionArray($data['handler'], 'socketMessageResultHandler'))) {
            return $result;
        }
        return $function($data, $result);
    }

    final public static function socketActionsReceive($data, $users)
    {
        $function = self::getFromFunctionArray($data['handler'], 'rulesForSendingSocketsMessages');

        $curent_socket_id = $data['socket_session_id'];
        unset($data['socket_session_id']);

        $result = [];
        foreach ($users as $socket_id => $user) {

            $default = $data['action'] == $user['action'] && !count(array_diff_assoc($data['params'], $user['params']));
            $res = is_null($function) ? null : $function($data, $user, $curent_socket_id == $socket_id, $default);
            if (!empty($res) || (is_null($res) && $default)) {
                $result[] = $socket_id;
            }
        }
        return $result;
    }

    final private function sentDataToServer($handler, $action, $params, $result, $type = '')
    {
        $class = get_class($this);
        Pusher::sentDataToServer([
            'topic_id' => 'socket_sender',
            'data' => [
                'action' => $class . '::' . $action,
                'handler' => $handler,
                'params' => $params,
                'post' => post(),
                'socket_session_id' => post('socket_session_id'),
                'user_id' => $this->user->id,
            ],
            'socketActionsReceive' => [$class, 'socketActionsReceive'],
            'type' => $type,
            'result' => $result,
        ]);

        if(($post = $this->getHandlerLogPost($handler)) !== false) {
            $log = new SocketLog();
            $log->post = $post;
            $log->result = $result;
            $log->handler = $handler;
            $log->option = $params;
            $log->type = $type;
            $log->action = $action;
            $log->class = $class;
            $log->save();
        }
    }

    private static function inFunctionArray($value, $function)
    {
        return method_exists(static::class, $function)
            && is_array($array = static::$function())
                && (in_array($value, $array)
                || isset($array[$value]));
    }

    public function run($action = null, $params = [])
    {
        if (!$handler = $this->getAjaxHandler()) {
            $this->addJs('/plugins/academy/system/assets/vendors/autobahnjs/autobahn.min.js');

            $option = [
                'class' => get_class($this),
                'action' => $action,
                'params' => json_encode($params)
            ];

            $this->addJs('/plugins/academy/system/assets/js/socket-sender.js?'. http_build_query($option) .'&');
            //$this->addCss('/plugins/academy/system/assets/css/academy.notifier.css');
            //$this->addJs('/plugins/academy/system/assets/js/academy.notifier.js');
        }
        $result = parent::run($action, $params);

        if (self::inFunctionArray($handler, 'rulesForSendingSocketsMessages')) {
            $this->sentDataToServer($handler, $action, $params, $result);
            return;
        }
        return $result;
    }

    private static function get(&$value, $default)
    {
        return empty($value) ? $default : $value;
    }

    public function socketNewSend($handler = null, $options = [])
    {
        $action = self::get($options['action'], $this->action);
        $params = self::get($options['params'], $this->params);
        $type = self::get($options['type'], 'getClientResult');
        $handler = self::get($handler, $this->getAjaxHandler());

        if (empty($options['result'])) {

            $this->fakeHandler = $handler;
            $result = parent::run($action, $params);
            $this->fakeHandler = null;
        }
        else {
            $result = $options['result'];
        }

        $this->sentDataToServer($handler, $action, $params, $result, $type);
    }
}