<?php namespace Academy\System\Traits;

trait DriverInit {

    protected static function forExtends()
    {
        $class = get_called_class();
        if (!count($args = func_get_args())) {
            return new $class();
        }
        $argNames = [];
        foreach ($args as $num => $arg) {
            $argNames[] = $num;
        }
        return eval('return new $class($args[' . implode('],$args[', $argNames) . ']);');
    }

    final public static function init()
    {
        $class = get_called_class();
        $systemClass = $class::getSystemClass();
        $systemClass::extend(self::$name, function() use($class) {
            return call_user_func_array([$class, 'forExtends'], func_get_args());
        });
        return self::$name;
    }
}