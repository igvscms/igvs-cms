<?php namespace Academy\System\Traits;

use Model;
use Yaml;

trait TransAttributes
{
    public $attributeNames = [];

    public static $transAttributes = [];
    
    public static function bootTransAttributes()
    {
        static::extend(function($model) {
            $model->bindEvent('model.beforeValidate', function () use ($model) {
                $model->bootTrans();
            });
        });
    }

    public function bootTrans()
    {
        if (!empty(self::$transAttributes)) {
            $this->attributeNames = self::$transAttributes;
        }
        else {
            $class = str_replace('\\', '/', strtolower(get_class($this)));
            $path = plugins_path() . "/{$class}/fields.yaml";

            if (!file_exists($path))
                return;

            $config = Yaml::parseFile($path);
            $this->attributeNames = [];
            $this->parseConfig($config);
            self::$transAttributes = $this->attributeNames;
        }
    }

    protected function parseConfig($config)
    {
        if (is_array($config)) {
            foreach ($config as $k => $v) {
                if ($k == 'fields') {
                    $this->attributeNames += $this->getConfigFields($v);
                }
                else if (is_array($v)) {
                    $this->parseConfig($v);
                }
            }
        }
    }

    protected function getConfigFields ($fields)
    {
        $attributeNames = [];

        if (is_array($fields)) {
            foreach ($fields as $field => $options) {

                if (empty($options['label']))
                        continue;

                $attributeNames[$field] = $options['label'];
            }
        }

        return $attributeNames;
    }
} 