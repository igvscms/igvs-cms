<?php namespace Academy\System\Traits;

use Academy\System\Models\History as ModelHistory;

trait History
{
    public function SaveHistory()
    {
        if(!$this->id)
            return;

        $history = new ModelHistory();
        $history->attachment_id = $this->id;
        $history->attachment_type = get_class($this);

        foreach($this->original as $key => $value) {

            if($value == $this->$key)
                continue;

            $history->data_save[$key] = [$value, $this->$key];
        }

        if(count($history->data_save))
            $history->save();
    }
}
