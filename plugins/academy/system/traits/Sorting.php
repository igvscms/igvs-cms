<?php namespace Academy\System\Traits;

use Config;
use Illuminate\Database\Eloquent\Builder;
// use Illuminate\Database\Query\Builder as QueryBuilder;

trait Sorting
{
    /*
    public function onMoveStatus()
    {
        $this->onMove(Status::where('project_id', $this->params[0]), 'sort_order');
    }

    public function onMove($query, $sort)
    {
        $class = $query->getModel();
        $move = $class::find(post('move'));
        $moveTo = $class::find(post('to'));

        if($moveTo->$sort > $move->$sort) {

            $from = $move->$sort;
            $to = $moveTo->$sort + 1;
            $function = 'decrement';
        }
        else {

            $from = $moveTo->$sort - 1;
            $to = $move->$sort;
            $function = 'increment';
        }

        $query->where($sort, '>', $from)
            ->where($sort, '<', $to)
            ->$function($sort);

        $move->$sort = $moveTo->$sort;
        $move->save();
    }
    */

    /*
    public function moveBefore($model)
    {
        $this->sort = $model->sort;
        $this->parent_id = $model->parent_id;
        $this->save();

        $stmt = \Db::raw("@i := ifnull(@i, " . intval($this->sort) . ")+1");

        self::where('course_id', $this->course_id)
            ->where('parent_id', $this->parent_id)
            ->where('sort', '>=', $this->sort)
            ->where('id', '<>', $this->id)
            ->orderBy('sort', 'asc')
            ->update(['sort' => $stmt]);
    }

    public function moveAfter($model)
    {
        $this->sort = $model->sort+1;
        $this->parent_id = $model->parent_id;
        $this->save();

        $stmt = \Db::raw("@i := ifnull(@i, 0)+1");

        self::where('course_id', $this->course_id)
            ->where('parent_id', $this->parent_id)
            ->orderBy('sort', 'asc')
            ->update(['sort' => $stmt]);
    }
    */

    public function buildSortingConditions($cnf)
    {
        $stmt = $this->newQuery();

        if (!empty($cnf->conditions)) {
            foreach ($cnf->conditions as $field) {
                $stmt->where($field, $this->{$field});
            }
        }

        if (!empty($cnf->fldParent)) {
            $stmt->where($cnf->fldParent, $this->{$cnf->fldParent});
        }

        return $stmt;
    }

    public function resort()
    {
        $cnf = (object) $this->configSorting;
        $stmt1 = \Db::raw("@i := ifnull(@i, 0)+1");
        $stmt2 = $this->buildSortingConditions($cnf);
        $stmt2->orderBy($cnf->fldSort, 'asc')
            ->update([$cnf->fldSort => $stmt1]);
    }

    public function moveBefore($model)
    {
        $cnf = (object) $this->configSorting;

        // Изменяем сортировку текущего элемента
        $this->{$cnf->fldSort} = $model->{$cnf->fldSort};

        // Синхронизируем с родителем (если он есть)
        if (!empty($cnf->fldParent)) {
            $this->{$cnf->fldParent} = $model->{$cnf->fldParent};
        }

        // Костыль
        if (isset($this->ignore_validation))
            $this->ignore_validation = true;

        // Сохраняем
        $this->save();

        // Выполняем пересортировку всех элементов с позиции $this->{$cnf->fldSort}
        $stmt1 = \Db::raw("@i := ifnull(@i, " . intval($this->{$cnf->fldSort}) . ")+1");
        $stmt2 = $this->buildSortingConditions($cnf);
        $stmt2->where($cnf->fldSort, '>=', $this->{$cnf->fldSort})
            ->where('id', '<>', $this->id)
            ->orderBy($cnf->fldSort, 'asc')
            ->update([$cnf->fldSort => $stmt1]);
    }

    public function moveAfter($model)
    {
        $cnf = (object) $this->configSorting;
        $this->{$cnf->fldSort} = $model->{$cnf->fldSort}+1;

        if (!empty($cnf->fldParent)) {
            $this->{$cnf->fldParent} = $model->{$cnf->fldParent};
        }

        // Костыль
        if (isset($this->ignore_validation))
            $this->ignore_validation = true;

        // Сохраняем
        $this->save();

        $stmt1 = \Db::raw("@i := ifnull(@i, " . intval($this->{$cnf->fldSort}) . ")+1");
        $stmt2 = $this->buildSortingConditions($cnf);
        $stmt2->where($cnf->fldSort, '>=', $this->{$cnf->fldSort})
            ->where('id', '<>', $this->id)
            ->orderBy($cnf->fldSort, 'asc')
            ->update([$cnf->fldSort => $stmt1]);
    }

    public function getChildren()
    {
        $cnf = (object) $this->configSorting;

        return empty($cnf->fldParent) ? []
            : self::where($cnf->fldParent, $this->id)->get();
    }

    public function makeChildOf($model)
    {
        $cnf = (object) $this->configSorting;

        if (!empty($cnf->fldParent)) {
            $this->{$cnf->fldSort} = 1;
            $this->{$cnf->fldParent} = $model->id;
            $this->save();
        }
    }
}