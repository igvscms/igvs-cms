<?php namespace Academy\System\FormWidgets;

use Db;
use Lang;
use Backend\Classes\FormField;
use Backend\Classes\FormWidgetBase;
use ApplicationException;
use SystemException;
use Illuminate\Database\Eloquent\Relations\Relation as RelationBase;

use Academy\System\Models\CustomFieldResult;
use Academy\System\Models\CustomField;

class CustomFields extends FormWidgetBase
{
    use \Backend\Traits\FormModelWidget;
    use \Backend\Traits\FormModelSaver;

    public $scope;
    private $formWidget;
    protected $defaultAlias = 'customfields';

    public $renderFormField;

    public function init()
    {
        $this->fillFromConfig(['scope']);
    }

    public function getFormWidget()
    {
        $query = CustomField::getQuery();
        if (!$this->scope) {
            $this->scope = $this->valueFrom;
        }
        $this->model->{'scope' . $this->scope}($query);
        $config = new \StdClass;
        $config->model = new CustomFieldResult([
            'attachment_id' => $this->model->getKey(),
            'attachment_type' => get_class($this->model),
        ]);
        $config->fields = [];
        $config->arrayName = class_basename($this->model) . '[' . $this->valueFrom . ']';
        $types = [
            'checkbox' => 'checkboxlist',
            'dropdown' => 'dropdown',
            'radio'    => 'radio',
            'range'    => 'range',
            'string'   => 'text',
            'text'     => 'textarea',
            'file'     => 'text',
            'datetime' => 'datepicker',
        ];
        foreach ($query->get() as $record) {
            $type = $types[$record->type->code];

            $config->fields[$record->id] = [
                'label' => $record->name,
                'required' => $record->is_required,
                'type' => $type,
                'disabled' => $this->previewMode,
                'span' => $record->span,
            ];
        }
        return $this->formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
    }

    public function render()
    {
        return $this->getFormWidget()->render(['preview' => $this->previewMode]);
    }

    public function getSaveValue($value)
    {
        $this->getFormWidget();
        $class = $this->model;
        $class::saving(function($model) use($value) {//beforeSave
            if ($model === $this->model) {
                $modelsToSave = $this->prepareModelsToSave($this->formWidget->model, $value);
                foreach ($modelsToSave as $modelToSave) {
                    $modelToSave->save(null, $this->formWidget->getSessionKey());//валидация
                }
            }
        });
        $class::saved(function($model) {//afterSave
            if ($model === $this->model) {
                $this->formWidget->model->attachment_id = $this->model->getKey();
                $this->formWidget->model->afterSave();//сохранение
            }
        });
        return FormField::NO_SAVE_DATA;
    }
}