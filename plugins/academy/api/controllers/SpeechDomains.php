<?php namespace Academy\Api\Controllers;

use Backend\Classes\Controller;
use System\Classes\SettingsManager;

class SpeechDomains extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Academy.System.Behaviors.CrudController'
    ];

    public $requiredPermissions = ['academy.api.clients'];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        \BackendMenu::setContext('Academy.Api', 'api', 'speechdomains');
        SettingsManager::setContext('Academy.Api', 'speechdomains');
    }
}