<?php namespace Academy\Api\Controllers;

use Backend\Classes\Controller;
use Academy\System\Classes\Curl;
use Academy\Api\Models\Speech as Modelspeech;
use Academy\Api\Models\SpeechDomain;
use System\Classes\SettingsManager;

class Speech extends Controller
{
    protected $publicActions = ['speech'];

    public $implement = [
        'Backend.Behaviors.ListController',
    ];

    public $requiredPermissions = ['academy.api.speech'];
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        \BackendMenu::setContext('Academy.Api', 'api', 'speech');
        SettingsManager::setContext('Academy.Api', 'speech');
    }

    private static function getIamToken($new = false)
    {
        $fileName = __DIR__ . '/speech/token.txt';
        $time = !$new && file_exists($fileName) ? filemtime($fileName) : false;
        if (!$time || time() > $time + 1000 || $new) {
            $content = Curl::url('https://iam.api.cloud.yandex.net/iam/v1/tokens')
                ->httpHeader([
                    'content-type' => 'application/json',
                ])
                // oauth token, valid 1 year, account/login acad-api
                // https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token
                // https://oauth.yandex.ru/authorize?response_type=token&client_id=1a6990aa636648e9b2ef855fa7bec2fb
                ->postFields([
                    'yandexPassportOauthToken' => 'AQAAAAAzUeeFAATuwdJSQDWrGUC9tGglbJzVPHA'
                ])
                ->fromJson();
            file_put_contents($fileName, $content->iamToken);
            touch($fileName, strtotime($content->expiresAt));
            return $content->iamToken;
        }
        return file_get_contents($fileName);
    }

    public function speech()
    {
        $speech = new Modelspeech();
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            $speech->domain = parse_url($_SERVER['HTTP_ORIGIN'], PHP_URL_HOST);
        }
        elseif (isset($_SERVER['HTTP_REFERER'])) {
            $speech->domain = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
        }
        else {
            $speech->domain = '';
        }
        $speech->ip = $_SERVER['REMOTE_ADDR'];
        $speech->text = post('text', '');
        $speech->lang = post('lang', 'ru');

        $query = SpeechDomain::whereIn('ip', [$speech->ip, '']);
        if ($speech->domain === '') {
            $query->where('domain', '');
        }
        else {
            $query->where(function($query) use($speech) {
                $query->where('domain', '')
                    ->orWhereRaw('? LIKE `domain`', [$speech->domain]);
            });
        }
        $speech->result = !!$query->first();
        $speech->result = true;
        $speech->save();
        if ($speech->result) {
            header('Access-Control-Allow-Origin: *');
        }
        else {
            die();
        }
        header('Content-Disposition: inline; filename="audio.ogg"');
        Curl::url('https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize')
            ->httpHeader([
                'Authorization' => 'Bearer ' . self::getIamToken(),
            ])
            ->postFields([
                'text' => post('text'),
                'folderId' => 'b1g5arlt4cb3gn5kvj7r',
                'lang' => post('lang'),
                'speed' => '0.9'
            ])
            ->headerFunction(function($key, $value) {
                if (!in_array(mb_strtolower($key), ['content-disposition', 'transfer-encoding', null])) {
                    header("$key: $value");
                }
            })
            ->exec();
        die();
    }
}
