<?php namespace Academy\Api;

use Backend;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

/**
 * api Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = ['Igvs.Courses'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'api',
            'description' => 'No description provided yet...',
            'author'      => 'academy',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Academy\Api\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'academy.api.clients' => [
                'tab' => 'ACADEMY API',
                'label' => 'Manager clients'
            ],
            'academy.api.speech' => [
                'tab' => 'ACADEMY API',
                'label' => 'Manager speech'
            ],
            'academy.api.speechdomains' => [
                'tab' => 'ACADEMY API',
                'label' => 'Manager speech domains'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'api' => [
                'label'       => trans('academy.api::plugin.label'),
                'url'         => Backend::url('academy/api/speech'),
                'icon'        => 'icon-leaf',
                'permissions' => ['academy.api.speech'],
                'order'       => 500,
                'sideMenu' => [
                    'speech' => [
                        'label' => 'Озвучка',
                        'icon' => 'icon-microphone',
                        'url' => \Backend::url('academy/api/speech'),
                        'permissions' => ['academy.api.speech']
                    ],
                    'speechdomains' => [
                        'label' => 'Домены озвучки',
                        'icon' => 'icon-asterisk',
                        'url' => \Backend::url('academy/api/speechdomains'),
                        'permissions' => ['academy.api.speechdomains']
                    ],
                ],
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'clients' => [
                'category'    => 'ACADEMY API',
                'label'       => 'Manager clients',
                'description' => 'Management of client applications.',
                'icon'        => 'icon-globe',
                'url'         => Backend::url('academy/api/clients'),
                'order'       => 500,
                'permissions' => ['academy.api.clients'],
                'keywords'    => ''
            ],
            'oauthlog' => [
                'category'    => SettingsManager::CATEGORY_LOGS,
                'label'       => 'academy.api::plugin.oauth_log_label',
                'description' => '',
                'icon'        => 'icon-file-o',
                'url'         => Backend::url('academy/api/oauthlog'),
                'order'       => 500,
                'permissions' => ['*'],
                'keywords'    => 'oauth log'
            ]
        ];
    }
}
