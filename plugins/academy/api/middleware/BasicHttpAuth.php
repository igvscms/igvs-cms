<?php

namespace Academy\Api\Middleware;

use Closure;
use Request;
use Hash;
use Academy\Api\Models\Client;

class BasicHttpAuth
{
    public function handle($request, Closure $next)
    {
        if (!Request::secure()) {
            exit('Error! Use SSL!');
        }

        $authenticate = function () {
            Header("WWW-Authenticate: Basic realm=Website");
                Header("HTTP/1.0 401 Unauthorized");
                // error401();
                exit("401 Unauthorized");
        };

        if (empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW'])) { // Not logged in using basic authentication
            $authenticate(); // Send basic authentication headers
        }

        $client = Client::where('user', $_SERVER['PHP_AUTH_USER'])
            ->first();

        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/basic_auth.log', $_SERVER['PHP_AUTH_USER'] . "\r\n" . $_SERVER['PHP_AUTH_PW'] . "\r\n", FILE_APPEND);
//        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/basic_auth.log', Hash::make($_SERVER['PHP_AUTH_PW']) . "\r\n" . $client->password . "\r\n", FILE_APPEND);

        // Check username and password
        $access = $client
            && $_SERVER['PHP_AUTH_USER'] == $client->user
            && Hash::check($_SERVER['PHP_AUTH_PW'], $client->password);

        if (!$access) {
            $authenticate(); // Send basic authentication headers because username and/or password didnot match
        }

//        var_dump($client);
//        exit();

        $client = $client ? $client->id : 0;
//        $request->session()->push('academy.api.client', $client);
        \Session::put('academy.api.client_id', $client);

        return $next($request);
    }
}
