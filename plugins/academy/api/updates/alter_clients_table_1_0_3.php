<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterClientsTable_1_0_3 extends Migration
{
    public function up()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->string('export_path', 255)
                ->after('user')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->dropColumn('export_path');
        });
    }
}