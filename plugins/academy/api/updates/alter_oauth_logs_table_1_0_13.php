<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AlterOauthLogsTable_1_0_13 extends Migration
{
    public function up()
    {
        Schema::table('academy_api_oauth_logs', function($table) {
            $table->dropColumn('headers');
            $table->dropColumn('data');
            $table->dropColumn('answer');
        });

        Schema::table('academy_api_oauth_logs', function($table) {
            $table->binary('headers')->nullable()->default(null)->after('client');
            $table->binary('data')->nullable()->default(null)->after('headers');
            $table->binary('answer')->nullable()->default(null)->after('data');
        });
    }

    public function down()
    {
        Schema::table('academy_api_oauth_logs', function($table) {
            $table->dropColumn('headers');
            $table->dropColumn('data');
            $table->dropColumn('answer');
        });

        Schema::table('academy_api_oauth_logs', function($table) {
            $table->text('headers')->nullable()->default(null)->after('client');
            $table->text('data')->nullable()->default(null)->after('headers');
            $table->text('answer')->nullable()->default(null)->after('data');
        });
    }
}
