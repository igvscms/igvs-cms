<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterClientsTable_1_0_8 extends Migration
{
    public function up()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->dropColumn('flow');

            $table->text('virtual_group')
                ->after('version')
                ->nullable();
            $table->text('virtual_group_inclusive')
                ->after('version')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->dropColumn('virtual_group');
            $table->dropColumn('virtual_group_inclusive');

            $table->text('flow', 50)
                ->after('version')
                ->nullable();
        });
    }
}