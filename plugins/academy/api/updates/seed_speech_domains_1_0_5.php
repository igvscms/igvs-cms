<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;

use Academy\Api\Models\SpeechDomain;

class SeedSpeechDomains_1_0_5 extends Seeder
{
    public function run()
    {
        SpeechDomain::insert([
            [
                'domain' => '%i-gvs.com',
                'ip' => '',
            ],
            [
                'domain' => 'i-gvs-cms-dev-1',
                'ip' => '',
            ],
            [
                'domain' => '%academia-moscow.ru',
                'ip' => '',
            ],
        ]);
    }
}