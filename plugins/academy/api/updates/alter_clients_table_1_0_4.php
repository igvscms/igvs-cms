<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterClientsTable_1_0_4 extends Migration
{
    public function up()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->string('oauth_secret', 255)
                ->after('export_path')
                ->nullable();
            $table->string('oauth_key', 255)
                ->after('export_path')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->dropColumn('oauth_key');
            $table->dropColumn('oauth_secret');
        });
    }
}