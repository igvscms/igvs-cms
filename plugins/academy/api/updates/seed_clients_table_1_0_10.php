<?php namespace Academy\Api\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;
use Academy\Api\Models\Client;

class SeedClientsTable_1_0_10 extends Seeder
{
    public function run()
    {
        $inclusive_group = \Backend\Models\UserGroup::where('name', 'Inclusive')->first();

        if (!$inclusive_group)
            return;

        $clients = Client::all();

        foreach ($clients as $client) {
            if (!is_null($client->virtual_group_inclusive) && is_array($client->virtual_group_inclusive)) {
                $client->virtual_group_inclusive = array_merge($client->virtual_group_inclusive, [$inclusive_group->id]);
            } else {
                $client->virtual_group_inclusive = [$inclusive_group->id];
            }

            $client->save();
        }
    }
}