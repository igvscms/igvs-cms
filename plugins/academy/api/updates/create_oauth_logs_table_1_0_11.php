<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOauthLogsTable_1_0_11 extends Migration
{
    public function up()
    {
        Schema::create('academy_api_oauth_logs', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->boolean('income')->nullable()->default(null);
            $table->string('type', 10)->nullable()->default(null);
            $table->integer('client')->nullable()->default(null);
            $table->text('headers')->nullable()->default(null);
            $table->text('data')->nullable()->default(null);
            $table->text('answer')->nullable()->default(null);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_api_oauth_logs');
    }
}
