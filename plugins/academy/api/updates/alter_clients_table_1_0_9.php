<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterClientsTable_1_0_9 extends Migration
{
    public function up()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->string('oauth_key_inclusive', 255)
                ->after('version')
                ->nullable();
            $table->string('oauth_secret_inclusive', 255)
                ->after('version')
                ->nullable();
            $table->string('version_inclusive', 255)
                ->after('version')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->dropColumn('oauth_key_inclusive');
            $table->dropColumn('oauth_secret_inclusive');
            $table->dropColumn('version_inclusive');
        });
    }
}