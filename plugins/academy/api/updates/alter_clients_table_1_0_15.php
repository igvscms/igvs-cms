<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterClientsTable_1_0_15 extends Migration
{
    public function up()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->string('endpoint_method', 255)
                ->after('version')
                ->nullable();
            $table->string('endpoint_method_inclusive', 255)
                ->after('version')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->dropColumn('endpoint_method');
            $table->dropColumn('endpoint_method_inclusive');
        });
    }
}