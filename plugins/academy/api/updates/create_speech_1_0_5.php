<?php namespace Academy\Api\Updates;

use October\Rain\Database\Updates\Migration;

class CreateSpeech_1_0_5 extends Migration
{
    public function up()
    {
        \Schema::create('academy_api_speech', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('text');
            $table->string('lang', 3);
            $table->string('ip');
            $table->boolean('result');
            $table->string('domain');
            $table->timestamps();
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_api_speech');
    }
}