<?php namespace Academy\Api\Updates;

use October\Rain\Database\Updates\Migration;

class CreateSpeechDomains_1_0_5 extends Migration
{
    public function up()
    {
        \Schema::create('academy_api_speech_domains', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('ip');
            $table->string('domain');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_api_speech_domains');
    }
}