<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterClientsTable_1_0_7 extends Migration
{
    public function up()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->string('flow', 50)
                ->after('version')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->dropColumn('flow');
        });
    }
}