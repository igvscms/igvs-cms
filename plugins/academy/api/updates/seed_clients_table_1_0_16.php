<?php namespace Academy\Api\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;
use Academy\Api\Models\Client;

class SeedClientsTable_1_0_16 extends Seeder
{
    public function run()
    {
        $clients = Client::all();

        foreach ($clients as $client) {
            $client->endpoint_method = 'endpoint_export';
            $client->endpoint_method_inclusive = 'inclsv_export';
            $client->save();
        }
    }
}