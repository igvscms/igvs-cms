<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AlterOauthLogsTable_1_0_17 extends Migration
{
    public function up()
    {
        Schema::table('academy_api_oauth_logs', function($table) {
            $table->integer('request_duration')
                ->after('client')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('academy_api_oauth_logs', function($table) {
            $table->dropColumn('request_duration');
        });
    }
}
