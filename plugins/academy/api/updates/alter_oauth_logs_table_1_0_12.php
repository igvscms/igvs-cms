<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AlterOauthLogsTable_1_0_12 extends Migration
{
    public function up()
    {
        Schema::table('academy_api_oauth_logs', function($table) {
            $table->string('action', 50)->nullable()->default(null)->after('type');
        });
    }

    public function down()
    {
        Schema::table('academy_api_oauth_logs', function($table) {
            $table->dropColumn('action');
        });
    }
}
