<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterClientsTable_1_0_6 extends Migration
{
    public function up()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->string('version', 255)
                ->after('oauth_secret')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('academy_api_clients', function($table) {
            $table->dropColumn('version');
        });
    }
}