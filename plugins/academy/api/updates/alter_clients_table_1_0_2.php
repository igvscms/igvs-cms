<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterClientsTable_1_0_2 extends Migration
{
    public function up()
    {
        Schema::table('academy_api_clients', function ($table) {
            $table->string('domain', 255)
                ->after('app_name')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('academy_api_clients', function ($table) {
            $table->dropColumn('credits_qualification_title');
        });
    }
}