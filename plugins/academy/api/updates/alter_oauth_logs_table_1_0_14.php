<?php namespace Academy\Api\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AlterOauthLogsTable_1_0_14 extends Migration
{
    public function up()
    {
        Schema::table('academy_api_oauth_logs', function($table) {
            $table->boolean('is_error')->nullable()->default(null)->after('income');
        });
    }

    public function down()
    {
        Schema::table('academy_api_oauth_logs', function($table) {
            $table->dropColumn('is_error');
        });
    }
}
