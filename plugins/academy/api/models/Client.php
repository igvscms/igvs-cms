<?php namespace Academy\Api\Models;

use Model;
use Hash;

/**
 * Client Model
 */
class Client extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;

    protected $rules =[
        'app_id' => ['required', 'between:3,255'],
        'user' => 'required|unique:academy_api_clients',
        'app_name' => 'required',
        'password' => 'min:6',
    ];


    protected $hashable = ['password'];

    /**
     * Protects the password from being reset to null.
     */
    public function setPasswordAttribute($value)
    {
        if ($this->exists && empty($value)) {
            unset($this->attributes['password']);
        }
        else {
            $this->attributes['password'] = $value;
        }
    }

    public function beforeValidate()
    {
        if (empty($this->id)) {
            $this->rules = array_merge($this->rules, [
                'password' => 'required',
            ]);
        }
    }

    /**
     * @var string The database table used by the model.
     */
    public $table = 'academy_api_clients';

    public $_password = null;

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $jsonable = [
        'virtual_group',
        'virtual_group_inclusive',
    ];

    public function beforeSave()
    {
        if (trim($this->password)) {
            $this->password = Hash::make(trim($this->password));
        }
    }

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function virtualGroups()
    {
        return \Igvs\Courses\Models\UserGroup::lists('name', 'id');
    }

    public function getClients()
    {
        $return = [];

        $result = self::select('id', 'app_name', 'domain')->orderBy('app_name')->get();

        foreach ($result as $v) {
            $return[$v->id] = "{$v->app_name} ({$v->domain})";
        }

        return $return;
    }
}
