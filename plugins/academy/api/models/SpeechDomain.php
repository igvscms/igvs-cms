<?php namespace Academy\Api\Models;

class SpeechDomain extends \Model
{
    public $table = 'academy_api_speech_domains';

    public $timestamps = false;
}