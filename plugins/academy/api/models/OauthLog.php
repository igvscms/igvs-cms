<?php namespace Academy\Api\Models;

use Model;
use Academy\Api\Models\Client;

/**
 * OauthLog Model
 */
class OauthLog extends Model
{
//    use \October\Rain\Support\Traits\Singleton;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'academy_api_oauth_logs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'income',
        'type',
        'client',
        'headers',
        'data',
        'answer',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $types = [
        'auth' => 'авторизация',
        'soap' => '',
    ];

    public function beforeSave()
    {
        $this->headers = bzcompress($this->headers);
        $this->data = bzcompress($this->data);
        $this->answer = bzcompress($this->answer);
    }

    public function getClientAttribute($value)
    {
        if (!$value || !strlen($value))
            return '';

        $client = Client::find($value);

        if (!$client)
            return '';

        return "{$client->app_name} ({$client->domain})";
    }

    public function afterFetch()
    {
        $this->headers = bzdecompress($this->headers);
        $this->data = bzdecompress($this->data);
        $this->answer = bzdecompress($this->answer);
    }

    static public function saveData($data)
    {
        $model = new self();
        foreach ($data as $k => $v) {
            $model->{$k} = $v;
        }

        $model->save();
    }
}
