<?php return [
    'field' => [
        'name' => 'Название',
        'date' => 'Дата',
        'number' => 'Номер',
    ],
    'label' => 'Стандарты',
    'title' => 'Стандарты',
    'update' => 'Изменение',
    'create' => 'Создание',
];