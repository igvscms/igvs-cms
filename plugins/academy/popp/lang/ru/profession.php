<?php return [
    'field' => [
        'profession' => 'Профессия',
        'position' => 'Должность',
        'name' => 'Название',
        'is_position' => 'Является должностью',
        'standarts' => 'Стандарты',
    ],
    'label' => 'Профессии',
    'title' => 'Профессии',
    'update' => 'Изменение',
    'create' => 'Создание',
];