<?php namespace Academy\Popp\Controllers;

use Backend\Classes\Controller;
use System\Classes\SettingsManager;

use Academy\Popp\Models\Popp as PoppModel;

class Popp extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];
    public $requiredPermissions = ['academy.popp.popp'];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        \BackendMenu::setContext('Academy.Popp', 'popp', 'popp');
        SettingsManager::setContext('Academy.Popp', 'popp');
    }

    private static function getSyllabusList($syllabus, &$result = [])
    {
        foreach ($syllabus->children as $number => $child) {
            $result[] = $child;
            $child->level = $syllabus->level + 1;
            if (!isset($syllabus->number)) {
                $child->number = $number + 1;
            }
            else {
                $child->number = $syllabus->number . '.' . ($number + 1);
            }
            self::getSyllabusList($child, $result);
        }
        return $result;
    }

    public function printdoc($id)
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/msword');
        header("Content-Disposition: attachment; filename=report.doc");
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $this->layout = 'plugins/academy/system/layouts/body';

        $popp = PoppModel::find($id);
        $popp->syllabus->level = 0;
        $plane = self::getSyllabusList($popp->syllabus);
        $plane[] = $popp->syllabus;
        $popp->syllabus->name = 'ИТОГО';
        $popp->syllabus->number = '';

        $methods = [
            'tutorials' => [],
            'regulations' => [],
            'technical' => [],
            'profile' => [],
            'electronic' => [],
            'digital' => [],
            'tasks' => [],
            'demonstration' => [],
            'other' => [],
        ];
        foreach ($plane as $record) {
            if ($record->level == 2) {
                foreach ($methods as $type => $value) {
                    if (!empty($record->$type)) {
                        $methods[$type][] = $record->$type;
                    }
                }
            }
        }

        $this->vars = [
            'popp' => $popp,
            'plane' => $plane,
            'methods' => $methods,
        ];
    }
}