<?php namespace Academy\Popp\Controllers;

use Backend\Classes\Controller;
use System\Classes\SettingsManager;

class Standarts extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];
    public $requiredPermissions = ['academy.popp.standarts'];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        \BackendMenu::setContext('Academy.Popp', 'popp', 'standarts');
        SettingsManager::setContext('Academy.Popp', 'standarts');
    }
}