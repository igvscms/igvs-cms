<?php namespace Academy\Popp\Controllers;

use Backend\Classes\Controller;
use System\Classes\SettingsManager;
use Academy\Popp\Models\Syllabus;

class Syllabuses extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];
    public $requiredPermissions = ['academy.popp.syllabuses'];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        \BackendMenu::setContext('Academy.Popp', 'popp', 'syllabuses');
        SettingsManager::setContext('Academy.Popp', 'syllabuses');
    }

    public function listExtendQuery($query, $definition = null)
    {
        $query->where('is_template', true);
    }

    public function index_onDelete()
    {
        $result = parent::index_onDelete();
        $checkedIds = post('checked');
        if (count($checkedIds)) {
            Syllabus::whereIn('id', $checkedIds)->update(['is_template' => false]);
        }
        return $result;
    }

    public function listExtendQueryBefore($query, $definition = null)
    {
        if ($this->getAjaxHandler() === 'onDelete') {
            $query->where('parent_id');
        }
    }
}