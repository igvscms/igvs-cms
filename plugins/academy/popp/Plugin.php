<?php namespace Academy\Popp;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Popp',
            'description' => 'Academy.Popp',
            'author'      => 'Academy',
            'icon'        => 'icon-sliders'
        ];
    }

    private static function ifThen(&$value, $default = null)
    {
        return isset($value) ? $value : $default;
    }

    public function registerNavigation()
    {
        $menu = [
            'popp' => 'icon-sliders',
            'syllabuses' => ['icon' => 'icon-cubes', 'label' => "academy.popp::syllabus.label"],
            'professions' => ['icon' => 'icon-black-tie', 'label' => "academy.popp::profession.label"],
            'standarts' => ['icon' => 'icon-check', 'label' => "academy.popp::standart.label"],
            'competencies' => ['icon' => 'icon-graduation-cap', 'label' => "academy.popp::competence.label"],
            'cities' => ['icon' => 'icon-globe', 'label' => "academy.popp::city.label"],
        ];
        $sideMenu = [];
        foreach ($menu as $plugin => $option) {
            if (!is_array($option)) {
                $option = (object) ['icon' => $option];
            }
            else {
                $option = (object) $option;
            }
            $sideMenu[$plugin] = [
                'icon' => $option->icon,
                'label' => trans(self::ifThen($option->label, "academy.popp::$plugin.label")),
                'url' => \Backend::url(self::ifThen($option->url, "academy/popp/$plugin")),
                'permissions' => [self::ifThen($option->permissions, "academy.popp.$plugin")]
            ];
        }
        return [
            'popp' => [
                'icon' => 'icon-sliders',
                'label' => trans("academy.popp::popp.label"),
                'url' => \Backend::url('academy/popp/popp'),
                'permissions' => ['academy.popp.*'],
                'sideMenu' => $sideMenu
            ]
        ];
    }

    public function registerPermissions()
    {
        $tabs = [
            'popp' => [
                'popp',
                'syllabuses',
                'professions',
                'standarts',
                'competencies',
                'cities',
            ],
        ];
        $result = [];
        foreach($tabs as $tab => $premissions) {
            $tab = trans('academy.popp::' . $tab . '.label');
            foreach($premissions as $premission) {
                $result['academy.popp.' . $premission] = [
                    'tab' => $tab,
                    'label' => trans('academy.popp::plugin.' . $premission),
                ];
            }
        }
        return $result;
    }
}