<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class Popp_1_0_1 extends Migration
{
    public function up()
    {
        \Schema::create('academy_popp', function($table)
        {
            $table->increments('id')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->integer('profession_id')->unsigned();
            $table->integer('competence_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->smallInteger('year')->unsigned();
            $table->integer('syllabus_id')->unsigned();
            //$table->integer('thematic_id')->unsigned();
            $table->integer('study_id')->unsigned();
            $table->text('conditions');//json
            $table->text('assessment');
            $table->text('compilers');//json

            $table->foreign('program_id', 'f_academyPopp_programId')
                ->references('id')
                ->on('academy_popp_programs');

            $table->foreign('profession_id', 'f_academyPopp_professionId')
                ->references('id')
                ->on('academy_popp_professions');

            $table->foreign('competence_id', 'f_academyPopp_competenceId')
                ->references('id')
                ->on('academy_popp_competencies');

            $table->foreign('city_id', 'f_academyPopp_cityId')
                ->references('id')
                ->on('academy_popp_cities');

            $table->foreign('syllabus_id', 'f_academyPopp_syllabusId')
                ->references('id')
                ->on('academy_popp_syllabuses');

            /*$table->foreign('thematic_id', 'f_academyPopp_thematicId')
                ->references('id')
                ->on('academy_popp_syllabuses');*/

            $table->foreign('study_id', 'f_academyPopp_studyId')
                ->references('id')
                ->on('academy_popp_studies');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_popp');
    }
}