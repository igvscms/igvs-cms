<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Seeder;
use Academy\Popp\Models\Listener;

class SeedListeners_1_0_1 extends Seeder
{
    public function run()
    {
        Listener::insert([
            ['name' => 'Учащихся общеобразовательных организаций'],
            ['name' => 'Обучающихся профессиональных образовательных организаций'],
            ['name' => 'Служащих'],
            ['name' => 'Преподавателей'],
            ['name' => 'Мастеров производственного обучения'],
            ['name' => 'Лиц предпенсионного возраста'],
        ]);
    }
}