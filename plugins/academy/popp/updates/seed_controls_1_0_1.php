<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Seeder;
use Academy\Popp\Models\Control;

class SeedControls_1_0_1 extends Seeder
{
    public function run()
    {
        Control::insert([
            ['name' => 'Зачет'],
            ['name' => 'Тест'],
            ['name' => 'Экзамен']
        ]);
    }
}