<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class SyllabusesControls_1_0_1 extends Migration
{
    public function up()
    {
        \Schema::create('academy_popp_syllabuses_controls', function($table)
        {
            $table->integer('controls_id')->unsigned();
            $table->integer('syllabus_id')->unsigned();

            $table->primary(['controls_id', 'syllabus_id'], 'p');

            $table->foreign('controls_id', 'f_academySyllabusesControls_controlId')
                ->references('id')
                ->on('academy_popp_controls')
                ->onDelete('cascade');

            $table->foreign('syllabus_id', 'f_academySyllabusesControls_syllabusId')
                ->references('id')
                ->on('academy_popp_syllabuses')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_popp_syllabuses_controls');
    }
}