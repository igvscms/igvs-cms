<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class studies_1_0_1 extends Migration
{
    public function up()
    {
        \Schema::create('academy_popp_studies', function($table)
        {
            $table->increments('id')->unsigned();
            $table->string('name');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_popp_studies');
    }
}