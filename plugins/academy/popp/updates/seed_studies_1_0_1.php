<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Seeder;
use Academy\Popp\Models\Study;

class SeedStudies_1_0_1 extends Seeder
{
    public function run()
    {
        Study::insert([
            ['name' => 'Очная'],
            ['name' => 'Очно-заочная'],
            ['name' => 'Заочная']
        ]);
    }
}