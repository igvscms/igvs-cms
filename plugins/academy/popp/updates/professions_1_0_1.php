<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class Professions_1_0_1 extends Migration
{
    public function up()
    {
        \Schema::create('academy_popp_professions', function($table)
        {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->boolean('is_position');//должность
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_popp_professions');
    }
}