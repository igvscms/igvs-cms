<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class Controls_1_0_1 extends Migration
{
    public function up()
    {
        \Schema::create('academy_popp_controls', function($table)
        {
            $table->increments('id')->unsigned();
            $table->string('name');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_popp_controls');
    }
}