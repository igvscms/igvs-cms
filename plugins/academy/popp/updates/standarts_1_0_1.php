<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class Standarts_1_0_1 extends Migration
{
    public function up()
    {
        \Schema::create('academy_popp_standarts', function($table)
        {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->integer('number')->unsigned();
            $table->date('date');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_popp_standarts');
    }
}