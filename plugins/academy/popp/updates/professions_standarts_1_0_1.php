<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class ProfessionsStandarts_1_0_1 extends Migration
{
    public function up()
    {
        \Schema::create('academy_popp_professions_standarts', function($table)
        {
            $table->integer('profession_id')->unsigned();
            $table->integer('standart_id')->unsigned();

            $table->primary(['profession_id', 'standart_id'], 'p');

            $table->foreign('profession_id', 'f_academyPoppStandarts_professionId')
                ->references('id')
                ->on('academy_popp_professions')
                ->onDelete('cascade');

            $table->foreign('standart_id', 'f_academyPoppStandarts_standartId')
                ->references('id')
                ->on('academy_popp_standarts')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_popp_professions_standarts');
    }
}