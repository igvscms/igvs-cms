<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class Popp_1_0_2 extends Migration
{
    public function up()
    {
        \Schema::table('academy_popp', function($table)
        {
            $table->string('name')->default('');
        });
    }

    public function down()
    {
        Schema::table('academy_popp', function($table)
        {
            $table->dropColumn('name');
        });
    }
}