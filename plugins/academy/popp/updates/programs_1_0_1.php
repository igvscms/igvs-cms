<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class Programs_1_0_1 extends Migration
{
    public function up()
    {
        \Schema::create('academy_popp_programs', function($table)
        {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('target', 512);
            $table->string('requirement', 512);
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_popp_programs');
    }
}