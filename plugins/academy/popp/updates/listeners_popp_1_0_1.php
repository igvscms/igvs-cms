<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class ListenersPopp_1_0_1 extends Migration
{
    public function up()
    {
        \Schema::create('academy_popp_listeners_popp', function($table)
        {
            $table->integer('listener_id')->unsigned();
            $table->integer('popp_id')->unsigned();

            $table->primary(['listener_id', 'popp_id'], 'p');

            $table->foreign('listener_id', 'f_academyPoppListenersPopp_listenerId')
                ->references('id')
                ->on('academy_popp_listeners')
                ->onDelete('cascade');

            $table->foreign('popp_id', 'f_academyPoppListenersPopp_poppId')
                ->references('id')
                ->on('academy_popp')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_popp_listeners_popp');
    }
}