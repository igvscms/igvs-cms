<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class Syllabuses_1_0_5 extends Migration
{
    public function up()
    {
        \Schema::table('academy_popp_syllabuses', function($table)
        {
            $table->integer('user_id')->unsigned()->nullable();

            $table->foreign('user_id', 'f_academyPoppSyllabuses_userId')
                ->references('id')
                ->on('backend_users')
                ->onDelete('set null');

            $table->decimal('lectures', 6, 1)->change();
            $table->decimal('practice', 6, 1)->change();
            $table->decimal('controls', 6, 1)->change();
        });
    }

    public function down()
    {
        \Schema::table('academy_popp_syllabuses', function($table)
        {
            $table->dropForeign('f_academyPoppSyllabuses_userId');
            $table->dropColumn('user_id');
        });
    }
}