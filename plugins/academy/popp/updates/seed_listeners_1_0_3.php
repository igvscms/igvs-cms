<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Seeder;
use Academy\Popp\Models\Listener;

class SeedListeners_1_0_3 extends Seeder
{
    public function run()
    {
        Listener::whereRaw('1')->update(['name' => \DB::raw('LOWER(`name`)')]);
    }
}