<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class Syllabuses_1_0_4 extends Migration
{
    public function up()
    {
        \Schema::table('academy_popp_syllabuses', function($table)
        {
            $table->boolean('is_template')->default(false);
        });
    }

    public function down()
    {
        \Schema::table('academy_popp_syllabuses', function($table)
        {
            $table->dropColumn('is_template');
        });
    }
}