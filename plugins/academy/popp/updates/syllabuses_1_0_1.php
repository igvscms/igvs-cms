<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class Syllabuses_1_0_1 extends Migration
{
    public function up()
    {
        \Schema::create('academy_popp_syllabuses', function($table)
        {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->tinyInteger('lectures')->unsigned();
            $table->tinyInteger('practice')->unsigned();
            $table->tinyInteger('controls')->unsigned();
            $table->tinyInteger('order')->unsigned();
            $table->tinyInteger('period')->unsigned();

            $table->text('tutorials');//Учебные пособия
            $table->text('regulations');//Нормативные документы
            $table->text('technical');//Техническая документация по компетенции
            $table->text('profile');//Профильная литература
            $table->text('electronic');//Электронные ресурсы
            $table->text('digital');//Цифровые учебные материалы
            $table->text('tasks');//Конкурсные задания по компетенции
            $table->text('demonstration');//Задание демонстрационного экзамена по компетенции по компетенции
            
            $table->text('other');//Другие источники

            $table->foreign('parent_id', 'f_academyPoppSyllabuses_parentId')
                ->references('id')
                ->on('academy_popp_syllabuses')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_popp_syllabuses');
    }
}