<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Seeder;
use Academy\Popp\Models\Control;

class SeedControls_1_0_3 extends Seeder
{
    public function run()
    {
        Control::where('name', 'Тест')->update(['name' => 'Дифференцированный зачет']);
    }
}