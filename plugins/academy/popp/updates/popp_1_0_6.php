<?php namespace Academy\Popp\Updates;

use October\Rain\Database\Updates\Migration;

class Popp_1_0_6 extends Migration
{
    public function up()
    {
        \Schema::table('academy_popp', function($table)
        {
            $table->integer('user_id')->unsigned()->nullable();

            $table->foreign('user_id', 'f_academyPopp_userId')
                ->references('id')
                ->on('backend_users')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        \Schema::table('academy_popp', function($table)
        {
            $table->dropForeign('f_academy_userId');
            $table->dropColumn('user_id');
        });
    }
}