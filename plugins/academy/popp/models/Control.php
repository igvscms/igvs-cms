<?php namespace Academy\Popp\Models;

class Control extends \Model
{
    public $table = 'academy_popp_controls';
    public $fillable = ['name'];
    public $timestamps = false;

    function scopeByName($query)
    {
        $query->orderBy('name');
    }
}