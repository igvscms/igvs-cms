<?php namespace Academy\Popp\Models;

class Listener extends \Model
{
    public $table = 'academy_popp_listeners';
    public $fillable = ['name'];
    public $timestamps = false;

    function scopeByName($query)
    {
        $query->orderBy('name');
    }
}