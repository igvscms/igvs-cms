<?php namespace Academy\Popp\Models;

class Standart extends \Model
{
    public $table = 'academy_popp_standarts';
    public $timestamps = false;
    protected $dates = ['date'];

    function scopeByName($query)
    {
        $query->orderBy('name');
    }
}