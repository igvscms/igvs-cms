<?php namespace Academy\Popp\Models;

class Profession extends \Model
{
    public $table = 'academy_popp_professions';
    public $timestamps = false;
    
    public $belongsToMany = [
        'standarts' => [
            'Academy\Popp\Models\Standart',
            'table' => 'academy_popp_professions_standarts'
        ],
    ];

    public function beforeCreate()
    {
        if (!isset($this->is_position)) {
            $this->is_position = false;
        }
    }

    public function getStandartsOptions()
    {
        return Standart::lists('name', 'id');
    }

    function scopeByName($query)
    {
        $query->orderBy('name');
    }
}