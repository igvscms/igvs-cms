<?php namespace Academy\Popp\Models;

class Study extends \Model
{
    public $table = 'academy_popp_studies';
    public $fillable = ['name'];

    function scopeByName($query)
    {
        $query->orderBy('name');
    }
}