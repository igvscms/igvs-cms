<?php namespace Academy\Popp\Models;

class Competence extends \Model
{
    public $table = 'academy_popp_competencies';
    public $timestamps = false;

    function scopeByName($query)
    {
        $query->orderBy('name');
    }
}