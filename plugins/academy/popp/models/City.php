<?php namespace Academy\Popp\Models;

class City extends \Model
{
    public $table = 'academy_popp_cities';
    public $fillable = ['name'];
    public $timestamps = false;

    function scopeByName($query)
    {
        $query->orderBy('name');
    }
}