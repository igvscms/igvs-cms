<?php namespace Academy\Popp\Models;

class Popp extends \Model
{
    use \October\Rain\Database\Traits\Purgeable;

    public $table = 'academy_popp';
    public $timestamps = false;

    public $belongsTo = [
        'program' => 'Academy\Popp\Models\Program',
        'profession' => 'Academy\Popp\Models\Profession',
        'competence' => 'Academy\Popp\Models\Competence',
        'city' => 'Academy\Popp\Models\City',
        'study' => 'Academy\Popp\Models\Study',
        'syllabus' => 'Academy\Popp\Models\Syllabus',
        'select_child' => 'Academy\Popp\Models\Syllabus',
        'user' => 'Backend\Models\User',
    ];

    public $belongsToMany = [
        'listeners' => [
            'Academy\Popp\Models\Listener',
            'table' => 'academy_popp_listeners_popp'
        ],
    ];
    public $jsonable = ['conditions', 'compilers'];
    protected $purgeable = ['default_assessment', 'select_child'];

    public function getProfessionOptions()
    {
        return Profession::where('is_position', !!$this->is_position)->lists('name', 'id');
    }

    public function getYearAttribute()
    {
        if (isset($this->attributes['year'])) {
            return $this->attributes['year'];
        }
        return date('Y');
    }

    public function getIsPositionAttribute()
    {
        if (post('Popp[is_position]') !== null) {
            return post('Popp[is_position]');
        }
        if (isset($this->attributes['is_position'])) {
            return $this->attributes['is_position'];
        }
        if ($this->profession) {
            return $this->profession->is_position;
        }
        return 0;
    }

    public function setIsPositionAttribute($value)
    {

    }

    public function setStandartsAttribute($value)
    {

    }

    public function getStandartsAttribute()
    {
        if ($this->profession && null === post('Popp[is_position]')) {
            $profession = $this->profession;
        }
        else {
            $profession = Profession::where('is_position', !!post('Popp[is_position]', 0))->first();
        }
        if ($profession) {
            return implode(', ', $profession->standarts()->lists('name'));
        }
    }

    public function getFormControlsOptions()
    {
        return Control::lists('name', 'id');
    }

    private function copySyllabus($model, $ignoreId = false)
    {
        $new = $model->attributes;
        if ($ignoreId) {
            unset($new['id']);
            unset($new['is_template']);
        }
        $new['has_children'] = !!count($model->children);
        if ($new['has_children']) {
            $new['children'] = $this->getTree($model, $ignoreId);
        }
        else {
            $new['children'] = [];
        }
        $new['form_controls'] = $model->form_controls()->lists('id');
        return $new;
    }

    private function getTree($item, $ignoreId = false)
    {
        $result = [];
        foreach ($item->children as $record) {
            $result[] = $this->copySyllabus($record, $ignoreId);
        }
        return $result;
    }

    public function getSyllabusesAttribute()
    {
        $fromPost = post('Popp[syllabuses]');
        if ($fromPost) {
            $result = [];
            foreach ($fromPost as $num => $block) {
                if ($block['select_child']) {
                    if (!isset($block['children'])) {
                        $block['children'] = [];
                    }
                    $block['children'][] = $this->copySyllabus(Syllabus::find($block['select_child']), true);
                }
                $result[$num] = $block;
            }
            return $result;
        }
        if ($this->syllabus) {
            return $this->getTree($this->syllabus);
        }
    }

    private function applySyllabus($record, $value, $update)
    {
        $record->name = $value->name;
        $record->order = $value->order;
        $fields = ['tutorials', 'regulations', 'technical', 'profile', 'electronic', 'digital', 'tasks', 'demonstration', 'other'];
        foreach ($fields as $field) {
            $record->$field = isset($value->$field) ? $value->$field : '';
        }
        if (isset($value->period)) {
            $record->period = (int) $value->period;
        }
        else {
            $record->period = 0;
        }
        $record->is_template = !empty($value->is_template);
        if (empty($value->has_children)) {
            $record->lectures = (float) str_replace(',', '.', $value->lectures);
            $record->practice = (float) str_replace(',', '.', $value->practice);
            $record->controls = (float) str_replace(',', '.', $value->controls);
            $record->form_controls = $value->form_controls;
        }
        if (isset($value->has_children)) {
            if (!$record->id) {
                $record->save();
            }
            $has_children = $value->has_children;
            $value = $this->updateSyllabus($record, $value->has_children ? $value->children : []);
        }
        if (!empty($has_children)) {
            $record->lectures = (float) str_replace(',', '.', $value->lectures);
            $record->practice = (float) str_replace(',', '.', $value->practice);
            $record->controls = (float) str_replace(',', '.', $value->controls);
            $record->form_controls = $value->form_controls;
        }
        $update->lectures += $record->lectures;
        $update->practice += $record->practice;
        $update->controls += $record->controls;
        foreach ($record->form_controls as $control) {
            if (!in_array($control, $update->form_controls)) {
                $update->form_controls[] = $control;
            }
        }
        $record->save();
    }

    private function updateSyllabus($item, $values)
    {
        $result = (object) [
            'lectures' => 0,
            'practice' => 0,
            'controls' => 0,
            'form_controls' => [],
        ];
        $records = $item->children;
        $create = [];
        $update = [];
        foreach ($values as $order => $value) {
            $value = (object) $value;
            $value->order = $order;
            if ($value->id) {
                $update[$value->id] = $value;
            }
            else {
                $create[] = $value;
            }
        }
        $delete = [];
        $saved = [];
        foreach ($records as $record) {
            if (isset($update[$record->id])) {
                $saved[$record->id] = $record;
            }
            elseif (count($create)) {
                $update[$record->id] = array_pop($create);
                $saved[$record->id] = $record;
            }
            else {
                $delete[] = $record->id;
            }
        }
        foreach ($create as $record) {
            $new = new Syllabus();
            $new->parent_id = $item->id;
            $this->applySyllabus($new, $record, $result);
        }
        foreach ($update as $id => $record) {
            $new = $saved[$id];
            $this->applySyllabus($new, $record, $result);
        }
        Syllabus::whereIn('id', $delete)->delete();
        return $result;
    }

    public function setSyllabusesAttribute($value)
    {
        if (!$this->syllabus) {
            $this->syllabus = new Syllabus();
            $this->syllabus->save();
        }
        $result = $this->updateSyllabus($this->syllabus, $value);
        $this->syllabus->lectures = $result->lectures;
        $this->syllabus->practice = $result->practice;
        $this->syllabus->controls = $result->controls;
        $this->syllabus->form_controls = $result->form_controls;
        $this->syllabus->save();
    }

    public function beforeCreate()
    {
        if (!$this->syllabus) {
            $this->syllabus = new Syllabus();
            $this->syllabus->save();
        }
        $this->user = \BackendAuth::getUser();
    }

    public function getPeriodOptions()
    {
        return trans('academy.popp::syllabus.module.period');
    }

    public function getDefaultAssessmentAttribute()
    {
        return 'Промежуточная аттестация по программе предназначена для оценки освоения слушателем модулей программы и проводится в виде зачетов и (или) экзаменов. По результатам любого из видов итоговых промежуточных испытаний, выставляются отметки по двухбалльной («удовлетворительно» («зачтено»), «неудовлетворительно» («не зачтено») или четырех балльной системе («отлично», «хорошо», «удовлетворительно», «неудовлетворительно»). Итоговая аттестация проводится в форме квалификационного экзамена, который включает в себя практическую квалификационную работу (демонстрационный экзамен, КОД No_____) и проверку теоретических знаний (тестирование).';
    }
}