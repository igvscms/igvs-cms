<?php namespace Academy\Popp\Models;

class Program extends \Model
{
    public $table = 'academy_popp_programs';
    public $fillable = ['name', 'target', 'requirement'];
    public $timestamps = false;

    function scopeByName($query)
    {
        $query->orderBy('name');
    }
}