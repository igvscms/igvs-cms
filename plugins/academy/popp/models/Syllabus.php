<?php namespace Academy\Popp\Models;

class Syllabus extends \Model
{
    //use \October\Rain\Database\Traits\Purgeable;

    public $table = 'academy_popp_syllabuses';
    public $timestamps = false;

    public $hasMany = [
        'children' => [
            self::class,
            'key' => 'parent_id',
            'order' => 'order asc',
        ]
    ];
    public $belongsToMany = [
        'form_controls' => [
            'Academy\Popp\Models\Control',
            'table' => 'academy_popp_syllabuses_controls',
            'key'      => 'syllabus_id',
            'otherKey' => 'controls_id'
        ],
    ];
    public $belongsTo = [
        'user' => 'Backend\Models\User',
    ];

    public $has_children;

    public function getPeriodOptions()
    {
        return trans('academy.popp::syllabus.module.period');
    }

    public function getFormControlsOptions()
    {
        return Control::lists('name', 'id');
    }

    private function applySyllabus($value, $update)
    {
        $this->name = $value->name;
        $this->order = $value->order;
        $fields = ['tutorials', 'regulations', 'technical', 'profile', 'electronic', 'digital', 'tasks', 'demonstration', 'other'];
        foreach ($fields as $field) {
            $this->$field = isset($value->$field) ? $value->$field : '';
        }
        if (isset($value->period)) {
            $this->period = (int) $value->period;
        }
        else {
            $this->period = 0;
        }
        if (empty($value->has_children)) {
            $this->lectures = (int) $value->lectures;
            $this->practice = (int) $value->practice;
            $this->controls = (int) $value->controls;
            $this->form_controls = $value->form_controls;
        }
        if (isset($value->has_children)) {
            $has_children = $value->has_children;
            $value = $this->setChildrenArrayAttribute($value->has_children ? $value->children : []);
        }
        if (!empty($has_children)) {
            $this->lectures = (int) $value->lectures;
            $this->practice = (int) $value->practice;
            $this->controls = (int) $value->controls;
            $this->form_controls = $value->form_controls;
        }
        $update->lectures += $this->lectures;
        $update->practice += $this->practice;
        $update->controls += $this->controls;
        foreach ($this->form_controls as $control) {
            if (!in_array($control, $update->form_controls)) {
                $update->form_controls[] = $control;
            }
        }
        $this->save();
    }

    public function setChildrenArrayAttribute($values)
    {
        if (!$this->has_children) {
            if ($this->id) {
                $this->children()->delete();
            }
            return;
        }
        $result = (object) [
            'lectures' => 0,
            'practice' => 0,
            'controls' => 0,
            'form_controls' => [],
        ];
        $records = $this->children;
        $create = [];
        $update = [];
        foreach ($values as $order => $value) {
            $value = (object) $value;
            $value->order = $order;
            if ($value->id) {
                $update[$value->id] = $value;
            }
            else {
                $create[] = $value;
            }
        }
        $delete = [];
        $saved = [];
        foreach ($records as $record) {
            if (isset($update[$record->id])) {
                $saved[$record->id] = $record;
            }
            elseif (count($create)) {
                $update[$record->id] = array_pop($create);
                $saved[$record->id] = $record;
            }
            else {
                $delete[] = $record->id;
            }
        }
        if (count($values) && !$this->id) {
            $this->save();
        }
        foreach ($create as $record) {
            $new = new Syllabus();
            $new->parent_id = $this->id;
            $new->applySyllabus($record, $result);
        }
        foreach ($update as $id => $record) {
            $new = $saved[$id];
            $new->parent_id = $this->id;
            $new->applySyllabus($record, $result);
        }
        Syllabus::whereIn('id', $delete)->delete();
        if ($this->is_template && $this->has_children) {
            $this->lectures = $result->lectures;
            $this->practice = $result->practice;
            $this->controls = $result->controls;
            $this->form_controls = $result->form_controls;
        }
        return $result;
    }

    public function afterFetch()
    {
        if (is_null($this->has_children)) {
            $this->has_children = !!count($this->children);
        }
    }

    public function getChildrenArrayAttribute()
    {
        $result = [];
        foreach ($this->children as $record) {
            $new = $record->attributes;
            $new['has_children'] = $record->has_children;
            if ($new['has_children']) {
                $new['children_array'] = $record->children_array;
            }
            else {
                $new['children_array'] = [];
            }
            $new['form_controls'] = $record->form_controls()->lists('id');
            $result[] = $new;
        }
        return $result;
    }

    public function beforeCreate()
    {
        $this->user = \BackendAuth::getUser();
    }

    public function beforeSave()
    {
        $notNull = [
            'practice',
            'lectures',
            'controls',
        ];
        foreach ($notNull as $prop) {
            if ($this->$prop === null) {
                $this->$prop = 0;
            }
        }
    }
}