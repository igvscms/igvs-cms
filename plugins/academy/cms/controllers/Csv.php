<?php namespace Academy\Cms\Controllers;

use Backend\Classes\Controller;

class Csv extends Controller {
    //protected $publicActions = ['export'];
    public function __construct() {}

    public static function quotes() {
        $arr = func_get_args();
        if (count($arr) == 1 && is_array($arr[0])) {
            $arr = $arr[0];
        }
        foreach ($arr as &$elem) {
            $elem = str_replace('"', '""', mb_convert_encoding($elem, 'Windows-1251'));
            //какие переносы может учитывать spss ??
        }
        return '"' . implode('";"', $arr) . '"' . "\r\n";
    }

    public static function strip_tags($text) {

        $tagsForClear = ['head', 'style', 'script', 'object', 'embed', 'applet', 'noframes', 'noscript', 'noembed'];
        $wtfToClear = [
            ['address', 'blockquote', 'center', 'del'],
            ['div', 'h[1-9]', 'ins', 'isindex', 'p', 'pre'],
            ['dir', 'dl', 'dt', 'dd', 'li', 'menu', 'ol', 'ul'],
            ['table', 'th', 'td', 'caption'],
            ['form', 'button', 'fieldset', 'legend', 'input'],
            ['label', 'select', 'optgroup', 'option', 'textarea'],
            ['frameset', 'frame', 'iframe'],
        ];
        $fromClear = [];
        $toClear = [];
        foreach ($tagsForClear as $tag) {
            $fromClear[] = '@<' . $tag . '[^>]*?>.*?</' . $tag . '>@siu';
            $toClear[] = ' ';
        }
        foreach ($wtfToClear as $wtf) {
            $fromClear[] =  '@</?((' . implode(')|(', $wtf) . '))@iu';
            $toClear[] = "\n\$0";
        }

        $text = preg_replace($fromClear, $toClear, $text);
        return strip_tags($text);
    }

    public static function flush_quotes() {
        echo call_user_func_array('self::quotes', func_get_args());
        flush();
    }

    public function export($id = null, $relation = null) {
        if (!$class = get('controller')) {
            return;
        }
        $controller = new $class();

        if ($id && $relation) {
            $model = $controller->formFindModelObject($id);
            $controller->initRelation($model, $relation);
            $widget = camel_case('relation ' . $relation) . 'ViewList';
            $list = $controller->widget->$widget;
        }
        else {
            $list = $controller->makeList('list');
        }
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="file.csv"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $columns = $list->getVisibleColumns();
        $row = [];
        foreach ($columns as $column) {
            $result = trim(self::strip_tags(trans($column->label)));
            if (method_exists($controller, 'getHeadValueForExportCsv')) {
                $row[] = $controller->getHeadValueForExportCsv($column, $result, $relation);
            }
            else {
                $row[] = $result;
            }
        }
        self::flush_quotes($row);

        foreach ($list->prepareModel()->get() as $record) {
            $row = [];
            foreach ($columns as $column) {
                $result = trim(self::strip_tags($list->getColumnValue($record, $column)));
                if (method_exists($controller, 'getColumnValueForExportCsv')) {
                    $row[] = $controller->getColumnValueForExportCsv($column, $record, $result, $relation);
                }
                else {
                    $row[] = $result;
                }
            }
            self::flush_quotes($row);
        }
    }
}