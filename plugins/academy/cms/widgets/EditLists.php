<?php namespace Academy\Cms\Widgets;

use Backend\Widgets\Lists;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\Relation;

class EditLists extends Lists
{ 
    use \Backend\Traits\FormModelSaver;

    public $groups = null;
    public $groupCount = 0;
    private $tempalte;
    public $model;

    protected function loadAssets()
    {
        $this->addJs('js/october.list.js', 'core');
        $this->addCss('css/academy.list.css', 'core');
    }

    private function columnEditable($column)
    {
        return (isset($this->config->form) && isset($column->config['context'])) || isset($column->config['form']);
    }

    private static function stringToTemplate($record, $string)
    {
        $result = [];
        foreach ($record->getAttributes() as $column => $value) {
            if (isset($value) &&
                !is_array($value) &&
                (!is_object($value) || method_exists($value, '__toString')) &&
                mb_strpos($string, ':' . $column) !== false
            ) {
                $result[$column] = ':' . $column;
            }
        }
        return $result;
    }

    private static function templateToString($record, $string, $columns)
    {
        $values = [];
        foreach ($columns as $num => $key) {
            $values[] = urlencode((string) $record->$num);
        }
        return str_replace($columns, $values, $string);
    }

    public function getRecordUrl($record)
    {
        if (isset($this->recordOnClick)) {
            return 'javascript:;';
        }
        if (!isset($this->recordUrl)) {
            return null;
        }
        if (is_null($this->tempalte)) {
            $this->tempalte = self::stringToTemplate($record, $this->recordUrl);
        }
        return \Backend::url(self::templateToString($record, $this->recordUrl, $this->tempalte));
    }

    public function getRecordOnClick($record)
    {
        if (!isset($this->recordOnClick)) {
            return null;
        }
        if (is_null($this->tempalte)) {
            $this->tempalte = self::stringToTemplate($record, $this->recordOnClick);
        }
        return \Html::attributes(['onclick' => self::templateToString($record, $this->recordOnClick, $this->tempalte)]);
    }

    public function getColumnValue($record, $column)
    {
        $value = parent::getColumnValue($record, $column);
        if ($this->columnEditable($column)) {
            return $this->makePartial('editable', [
                'columnName' => $column->columnName,
                'record_id' => $record->getKey(),
                'value' => $value
            ]);
        }
        return $value;
    }

    protected function makeListColumn($name, $config)
    {
        $column = parent::makeListColumn($name, $config);
        if ($this->columnEditable($column)) {
            $column->clickable = false;
        }
        return $column;
    }

    protected function modifyColumn($record, $column)
    {
        $result = $this->fireSystemEvent('backend.list.overrideColumn', [
            $record,
            $column->columnName,
            $column->config
        ]);
        if (is_array($result) && count($result)) {
            return $this->makeListColumn($column->columnName, $result);
        }
        return $column;
    }

    public function validateTree()
    {
        if (!$this->showTree) {
            return;
        }
        $this->showPagination = false;
        if (is_numeric($this->showTree)) {
            $showSorting = $this->showSorting;
            //$showPagination = $this->showPagination;
            parent::validateTree();
            $this->showSorting = $showSorting;
            //$this->showPagination = $showPagination;
        }
    }

    protected function getRecords()
    {
        $model = $this->prepareModel();

        if ($this->showTree && !is_numeric($this->showTree)) {
            $records = $this->getChildren([
                'level' => -1,
                'query' => $model,
                'level_key' => '',
            ]);
        }
        elseif ($this->showTree) {
            $records = $model->getNested();
        }
        elseif ($this->showPagination) {
            $records = $model->paginate($this->recordsPerPage, $this->currentPageNumber);
        }
        else {
            $records = $model->get();
        }

        return $this->records = $records;
    }

    public function isTreeNodeExpanded($node)
    {
        if (is_array($node)) {
            return $this->getSession('tree_node_status_' . $node['level_key'] . '_' . $node['key'], $this->treeExpanded);
        }
        return parent::isTreeNodeExpanded($node);
    }

    public function getSelectGroup()
    {
        if ($this->groups) {
            return $this->groups;
        }

        $groups = $this->makeConfig($this->showTree);
        $select = [null => []];
        $maxGroupCount = [];
        foreach ($groups as $name => $group) {
            if (!isset($group['dependsOn'])) {
                $group['dependsOn'] = [null];
            }
            foreach ($group['dependsOn'] as $depends) {
                $select[$depends][$name] = $group;
            }
            if (!empty($group['multi'])) {
                $maxGroupCount[$name] = $this->controller->{"get{$name}MaxCount"}(clone $this->model);
            }
        }
        $this->groupCount = 0;
        $prev = null;
        $prevs = [];
        $result = [];
        while (true) {
            $selects = [];
            $active = null;
            foreach ((isset($select[$prev]) ? $select[$prev] : []) + $select[null] as $name => $options) {
                if (!empty($options['active']) && is_null($active)) {
                    $active = $name;
                }
                if (isset($maxGroupCount[$name])) {
                    if ($maxGroupCount[$name] > 0) {
                        $selects[$name] = $options;
                        $maxGroupCount[$name]--;
                    }
                }
                elseif (!in_array($name, $prevs)
                    || in_array($this->model->getModel()->getRelationType($name), ['belongsToMany', 'hasMany'])
                ) {
                    $selects[$name] = $options;
                }
            }
            if (!count($selects)) {
                break;
            }
            $prev = $this->getSession('group_' . $this->groupCount, $active);
            $prevs[] = $prev;
            $result[] = $selects;
            if (!isset($selects[$prev])) {
                break;
            }
            $this->groupCount++;
        }
        return $this->groups = $result;
    }

    public function setSearchTerm($term)
    {
        $this->searchTerm = $term;
    }

    public function renderGroupSelect()
    {
        if ($this->showTree && !is_numeric($this->showTree)) {
            $groups = $this->getSelectGroup();
            $result = [];
            foreach ($groups as $level => $group) {
                $select = [];
                $active = null;
                foreach ($group as $name => $options) {
                    if (!empty($options['active']) && is_null($active)) {
                        $active = $name;
                    }
                    if (isset($options['label'])) {
                        $select[$name] = trans($options['label']);
                    }
                    else {
                        $select[$name] = $name;
                    }
                }

                $result[] = \Form::select('taskGroup',
                    ['' => 'Выберите группировку'] + $select,
                    $this->getSession('group_' . $level, $active), [
                    'data-request' => $this->getEventHandler('onTaskGroup'),
                    'data-request-data' => 'level: ' . $level,
                    'class' => 'custom-select select-no-search'
                ]);
            }
            return implode('', $result);
        }
    }

    public function onTaskGroup()
    {
        $this->putSession('group_' . post('level'), post('taskGroup'));
        return $this->onRefresh();
    }

    public function getChildren($record)
    {
        $groups = $this->getSelectGroup();
        $level = $record['level'] + 1;
        $level_key = $record['level_key'];

        $active = null;
        if (isset($groups[$level])) {
            foreach ($groups[$level] as $name => $options) {
                if (!empty($options['active'])) {
                    $active = $name;
                    break;
                }
            }
        }

        $relationName = $this->getSession('group_' . $level, $active);

        if (!isset($groups[$level][$relationName])) {
            return $record['query']->get();//возвращаем найденные модели
        }

        $realModel = $record['query']->getModel();
        $relationType = $realModel->getRelationType($relationName);

        if (in_array($relationType, [/*'hasMany', /**/'belongsToMany'])) {

            $multi = isset($record['multi']) ? $record['multi'] : [];
            if (!isset($multi[$relationName])) {
                $multi[$relationName] = [];
            }

            $relation = $record['query']->getRelation($relationName);

            $related = $relation->getRelated();
            $table = $related->getTable();
            $field = $related->getKeyName();
            $relationQuery = $relation->getQuery();
            $recordQuery = $record['query'];

            $group = $table . '.' . $field;
            $select = isset($groups[$level][$relationName]['select']) ? $groups[$level][$relationName]['select'] : $group;

            $subQuery = clone $recordQuery;
            $keys = $subQuery->lists($realModel->getKeyName());
            $subQuery = clone $relationQuery;

            if (count($multi[$relationName])) {
                $subQuery->whereNotIn($group, $multi[$relationName]);
            }
            $records = $subQuery
                ->select($group)
                ->selectRaw($select . ' `title`')
                ->selectRaw('count(*)`count`')
                ->groupBy($group)
                ->whereIn($relation->getForeignKey(), $keys)
                ->get();

            $num = -1;
            foreach ($records as $num => $record) {
                $query = clone $recordQuery;
                $newMulti = $multi;
                $newMulti[$relationName][] = $record->$field;

                $records[$num] = [
                    'key' => $record->$field,
                    'count' => $record->count,
                    'query' => $query->whereHas($relationName, function($query) use($newMulti, $relationName, $group) {
                        $query->whereIn($group, $newMulti[$relationName]);
                    }, '>=', count($newMulti[$relationName])),
                    'label' => $record->title,
                    'level' => $level,
                    'level_key' => $level_key . '_' . $record->$field,
                    'multi' => $newMulti,
                ];
            }

            $subQuery = clone $recordQuery;

            $count = $subQuery
                ->has($relationName, count($multi[$relationName]))
                ->count();

            if ($count) {
                $default = isset($groups[$level][$relationName]['default']) ? $groups[$level][$relationName]['default'] : $relationName;

                $records[++$num] = [
                    'key' => 0,
                    'count' => $count,
                    'query' => $subQuery,
                    'label' => $default,
                    'level' => $level,
                    'level_key' => $level_key . '_0',
                    'multi' => $multi,
                ];
            }
        }
        elseif ($relationType == 'belongsTo') {
            $relationObject = $realModel->{$relationName}();
            $relationModel = $relationObject->getRelated();

            $fk = $realModel->getRelationDefinition($relationName);

            if (isset($fk['key'])) {
                $field = $fk['key'];
            }
            else {
                $field = snake_case($relationName).'_id';
            }

            $group = $realModel->getTable() . '.' . $field;

            $key = $relationModel->getKeyname();

            $subQuery = clone $record['query'];
            $select = isset($groups[$level][$relationName]['select']) ? $groups[$level][$relationName]['select'] : null;
            $default = isset($groups[$level][$relationName]['default']) ? $groups[$level][$relationName]['default'] : $relationName;

            $subQuery->addSelect(\DB::raw('count(*)`count`'));
            if (!empty($groups[$level][$relationName]['multi'])) {
                $count = 0;
                for ($i = 0; $i < $level; $i++) {
                    if (isset($groups[$i][$relationName])) {
                        $count++;
                    }
                }
                $this->controller->{"get{$relationName}GroupQuery"}($subQuery, $count);
            }
            else {
                $subQuery->groupBy($group);
            }
            $records = $subQuery->get();

            $recordQuery = $record['query'];
            $multi = isset($record['multi']) ? $record['multi'] : [];
            foreach ($records as $num => $record) {
                if (is_null($select) || !$relation = $relationModel::where($key, $record->$field)->selectRaw($select . ' `title`')->first()) {
                    $label = $default;
                }
                else {
                    $label = $relation->title;
                }
                $query = clone $recordQuery;
                if (empty($groups[$level][$relationName]['multi'])) {
                    $query->where(\DB::raw($group), $record->$field);
                }
                else {
                    $this->controller->{"get{$relationName}FilterQuery"}($query, $record->$field);
                }
                $records[$num] = [
                    'key' => $record->$field,
                    'count' => $record->count,
                    'query' => $query,
                    'label' => $label,
                    'level' => $level,
                    'level_key' => $level_key . '_' . $record->$field,
                    'multi' => $multi,
                ];
            }
        }

        else {
            $field = 'title';

            $select = $group = isset($groups[$level][$relationName]['select']) ? $groups[$level][$relationName]['select'] : $relationName;

            $subQuery = clone $record['query'];

            $records = $subQuery->addSelect(\DB::raw('count(*)`count`'))->addSelect(\DB::raw($select . ' `title`'))->groupBy(\DB::raw($group))->get();
            $recordQuery = $record['query'];
            $multi = isset($record['multi']) ? $record['multi'] : [];
            foreach ($records as $num => $record) {
                $query = clone $recordQuery;
                $records[$num] = [
                    'key' => $record->$field,
                    'count' => $record->count,
                    'query' => $query->where(\DB::raw($group), $record->$field),
                    'label' => $record->title,
                    'level' => $level,
                    'level_key' => $level_key . '_' . $record->$field,
                    'multi' => $multi,
                ];
            }
        }

        return $records->sort(function($a, $b) {
            return strnatcmp($a['label'],$b['label']);
        });
    }

    private $filterNotApply = false;

    public function renderScopeElement($scope)
    {
        $params = ['scope' => $scope];
        $scope->value = $this->getScopeValue($scope->columnName);

        switch ($scope->type) {
            case 'date':
            case 'datetime':
            case 'timesince':
                if ($scope->value && is_array($scope->value) && count($scope->value) === 2 &&
                    $scope->value[0] && $scope->value[0] instanceof Carbon &&
                    $scope->value[1] && $scope->value[1] instanceof Carbon
                ) {
                    $after = $scope->value[0]->format('Y-m-d H:i:s');
                    $before = $scope->value[1]->format('Y-m-d H:i:s');

                    if(strcasecmp($after, '0000-00-00 00:00:00') > 0) {
                        $params['afterStr'] = \Backend::dateTime($scope->value[0], ['formatAlias' => 'dateMin']);
                        $params['after']    = $after;
                    }
                    else {
                        $params['afterStr'] = '∞';
                        $params['after']    = null;
                    }

                    if(strcasecmp($before, '2999-12-31 23:59:59') < 0) {
                        $params['beforeStr'] = \Backend::dateTime($scope->value[1], ['formatAlias' => 'dateMin']);
                        $params['before']    = $before;
                    }
                    else {
                        $params['beforeStr'] = '∞';
                        $params['before']    = null;
                    }
                }
                break;
        }
        try {
            $result = $this->makePartial('scope_'.$scope->type, $params);
        }
        catch(\Exception $e) {
            $result = $this->makePartial('scope_partial', $params);
        }
        return $result;
    }

    protected function datesFromAjax($ajaxDates)
    {
        $dates = [];
        $dateRegex = '/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/';

        if (null !== $ajaxDates) {
            if (!is_array($ajaxDates)) {
                if(preg_match($dateRegex, $ajaxDates)) {
                    $dates = [$ajaxDates];
                }
            } else {
                foreach ($ajaxDates as $i => $date) {
                    if (preg_match($dateRegex, $date)) {
                        $dates[] = Carbon::createFromFormat('Y-m-d H:i:s', $date);
                    } elseif (empty($date)) {
                        if($i == 0) {
                            $dates[] = Carbon::createFromFormat('Y-m-d H:i:s', '0000-00-00 00:00:00');
                        } else {
                            $dates[] = Carbon::createFromFormat('Y-m-d H:i:s', '2999-12-31 23:59:59');
                        }
                    } else {
                        $dates = [];
                        break;
                    }
                }
            }
        }
        return $dates;
    }

    public function applyScopeToQuery($scope, $query)
    {
        if ($this->filterNotApply) {
            return $query;
        }
        if (is_string($scope)) {
            $scope = $this->getScope($scope);
        }
        if (!$scope->value) {
            return;
        }
        $table = $query->getModel()->getTable();
        $columnName = $scope->sqlSelect ? \DB::raw($scope->sqlSelect) : $table.'.'.$scope->columnName;
        switch ($scope->type) {
            case 'date':
            case 'datetime':
            case 'timesince':
                if (is_array($scope->value) && count($scope->value) > 1) {
                    list($after, $before) = array_values($scope->value);
                    if ($after && $after instanceof Carbon && $before && $before instanceof Carbon) {

                        if ($scope->relation) {
                            $query->whereHas($scope->relation, function($query) use ($scope) {
                                $query->where($scope->sqlSelect, '>=', $after->format('Y-m-d'));
                                $query->where($scope->sqlSelect, '<=', $before->format('Y-m-d'));
                            });
                        }
                        else {
                            $query->where($columnName, '>=', $after->format('Y-m-d'));
                            $query->where($columnName, '<=', $before->format('Y-m-d'));
                        }
                    }
                }
                break;
            case 'switch':
                if ($scope->relation) {
                    $query->whereHas($scope->relation, function($query) use ($scope) {
                        $query->where($scope->sqlSelect, $scope->value - 1);
                    });
                }
                else {
                    $query->where($columnName, $scope->value - 1);
                }
                break;
            default:
                if ($scope->relation) {
                    $query->whereHas($scope->relation, function($query) use ($scope) {
                        $query->whereIn($this->model->makeRelation($scope->relation)->getKeyName(), array_keys($scope->value));
                    });
                }
                else {
                    $query->whereIn($columnName, array_keys($scope->value));
                }
        }

        return $query;
    }

    public function applyAllScopesToQuery($query)
    {
        $this->defineListColumns();

        foreach ($this->allColumns as $scopeName => $scope) {
            $scope = $this->getScope($scopeName);
            $this->applyScopeToQuery($scope, $query);
        }
        return $query;
    }

    public function getScope($scopeName)
    {
        $scope = $this->getColumn($scopeName);
        $scope->value = $this->getScopeValue($scopeName);
        return $scope;
    }

    public function getScopeValue($scope, $default = null)
    {
        $scopeName = is_string($scope) ? $scope : $scope->columnName;
        $cacheKey = 'scope-'.$scopeName;
        return $this->getSession($cacheKey, $default);
    }

    public function setScopeValue($scope, $value)
    {
        $scopeName = is_string($scope) ? $scope : $scope->columnName;
        $cacheKey = 'scope-'.$scopeName;
        $this->putSession($cacheKey, $value);
        return $scope->value = $value;
    }

    protected function getOptionsFromArray($scope, $searchQuery = null)
    {
        /*
         * Load the data
         */
        $options = $scope->options;

        if (is_scalar($options)) {
            $model = $this->scopeModels[$scope->scopeName];
            $methodName = $options;

            if (!$model->methodExists($methodName)) {
                throw new ApplicationException(Lang::get('backend::lang.filter.options_method_not_exists', [
                    'model'  => get_class($model),
                    'method' => $methodName,
                    'filter' => $scope->scopeName
                ]));
            }

            $options = $model->$methodName();
        }
        elseif (!is_array($options)) {
            $options = [];
        }

        /*
         * Apply the search
         */
        $searchQuery = Str::lower($searchQuery);
        if (strlen($searchQuery)) {
            $options = $this->filterOptionsBySearch($options, $searchQuery);
        }

        return $options;
    }

    protected function optionsFromAjax($options)
    {
        $processed = [];
        if (!is_array($options)) {
            return $processed;
        }

        foreach ($options as $option) {
            if (($id = array_get($option, 'id')) === '') {
                continue;
            }
            $processed[$id] = array_get($option, 'name');
        }
        return $processed;
    }

    public function onFilterUpdate()
    {
        $this->defineListColumns();

        if (!$scope = post('scopeName')) {
            return;
        }

        $scope = $this->getScope($scope);

        switch ($scope->type) {
            case 'switch':
                $value = post('value');
                $this->setScopeValue($scope, $value);
                break;

            case 'date':
            case 'datetime':
            case 'timesince':
                $dates = $this->datesFromAjax(post('options.dates'));

                if (!empty($dates)) {
                    list($after, $before) = $dates;
                    $dates = [$after, $before];
                }
                else {
                    $dates = null;
                }

                $this->setScopeValue($scope, $dates);
                break;
            default:
                $active = $this->optionsFromAjax(post('options.active'));
                $this->setScopeValue($scope, $active);
                break;
        }

        /*
         * Trigger class event, merge results as viewable array
         */
        $params = func_get_args();

        $result = $this->fireEvent('filter.update', [$params]);

        if ($result && is_array($result)) {
            return call_user_func_array('array_merge', $result);
        }
    }

    protected function getSortColumn()
    {
        if ($this->filterNotApply) {
            return false;
        }
        return parent::getSortColumn();
    }

    private static function strip_tags($text) {

        $tagsForClear = ['head', 'style', 'script', 'object', 'embed', 'applet', 'noframes', 'noscript', 'noembed'];
        $wtfToClear = [
            ['address', 'blockquote', 'center', 'del'],
            ['div', 'h[1-9]', 'ins', 'isindex', 'p', 'pre'],
            ['dir', 'dl', 'dt', 'dd', 'li', 'menu', 'ol', 'ul'],
            ['table', 'th', 'td', 'caption'],
            ['form', 'button', 'fieldset', 'legend', 'input'],
            ['label', 'select', 'optgroup', 'option', 'textarea'],
            ['frameset', 'frame', 'iframe'],
        ];
        $fromClear = [];
        $toClear = [];
        foreach ($tagsForClear as $tag) {
            $fromClear[] = '@<' . $tag . '[^>]*?>.*?</' . $tag . '>@siu';
            $toClear[] = ' ';
        }
        foreach ($wtfToClear as $wtf) {
            $fromClear[] =  '@</?((' . implode(')|(', $wtf) . '))@iu';
            $toClear[] = "\n\$0";
        }

        $text = preg_replace($fromClear, $toClear, $text);
        return trim(strip_tags($text));
    }

    protected function getAvailableOptions($scope, $searchQuery = null, $valueFromIgnore = false)
    {
        $this->filterNotApply = true;

        if ($scope->relation) {
            //$relationType = $this->model->getRelationType($scope->relation));*/
            if ($scope->valueFrom) {
                $scope->sqlSelect = $scope->valueFrom;
                $scope->columnName = $scope->valueFrom;
            }

            $query = $this->prepareModel();

            $relation = $query->getRelation($scope->relation);

            if (method_exists($relation, 'getQualifiedForeignKey')) {
                $key = $relation->getQualifiedForeignKey();
            }
            else {
                $key = $relation->getHasCompareKey();
            }
            $relationKeys = $query->select($key . ' as ' . $scope->relation)
                ->distinct()
                ->lists($scope->relation);

            $relation = $this->model->makeRelation($scope->relation);

            $result = $relation::select($relation->getKeyName())
                ->orderBy(\DB::raw($scope->sqlSelect))
                ->addSelect(\DB::raw($scope->sqlSelect . ' as `' . $scope->columnName . '`'))
                ->addSelect($relation->getKeyName())
                ->whereIn($relation->getKeyName(), $relationKeys);

            if (!is_null($searchQuery)) {
                $result->searchWhere($searchQuery, [$scope->sqlSelect]);
            }
            $result = $result->lists($scope->columnName, $relation->getKeyName());
        }
        else {
            if (!$valueFromIgnore && $scope->valueFrom) {
                if (isset($this->allColumns[$scope->valueFrom])) {
                    return $this->getAvailableOptions($this->getScope($scope->valueFrom), $searchQuery, true);
                }
                $scope->columnName = $scope->valueFrom;
                $scope->sqlSelect = null;
            }
            $columnName = $scope->sqlSelect ? \DB::raw($scope->sqlSelect) : $scope->columnName;
            $sqlSelect = $scope->sqlSelect ? \DB::raw($scope->sqlSelect . ' AS ' . $scope->columnName) : $scope->columnName;
            $result = $this
                ->prepareModel()
                //->addSelect($sqlSelect)
                ->groupBy($columnName)
                ->orderBy($columnName);

            if (!is_null($searchQuery)) {
                $result->searchWhere($searchQuery, [$columnName]);
            }

            if ($scope->type == 'partial') {
                $return = [];
                foreach ($result->get() as $record) {
                    $return[$record->{$scope->columnName}] = self::strip_tags($this->getColumnValue($record, $scope));
                }
                $result = $return;
            }
            else {
                $result = $result->lists($scope->columnName, $scope->columnName);
            }
        }
        $this->filterNotApply = false;

        return $result;
    }

    protected function optionsToAjax($options)
    {
        $processed = [];
        foreach ($options as $id => $result) {
            $processed[] = ['id' => $id, 'name' => $result];
        }
        return $processed;
    }

    protected function filterActiveOptions(array $activeKeys, array &$availableOptions)
    {
        $active = [];
        foreach ($availableOptions as $id => $option) {
            if (!in_array($id, $activeKeys)) {
                continue;
            }

            $active[$id] = $option;
            unset($availableOptions[$id]);
        }

        return $active;
    }

    public function onFilterGetOptions()
    {
        $this->defineListColumns();

        $searchQuery = post('search');
        if (!$scopeName = post('scopeName')) {
            return;
        }

        $scope = $this->getScope($scopeName);
        $activeKeys = $scope->value ? array_keys($scope->value) : [];
        $available = $this->getAvailableOptions($scope, $searchQuery);
        $active = $searchQuery ? [] : $this->filterActiveOptions($activeKeys, $available);

        return [
            'scopeName' => $scopeName,
            'options' => [
                'available' => $this->optionsToAjax($available),
                'active'    => $this->optionsToAjax($active),
            ]
        ];
    }
}