<?php namespace Academy\Cms\Widgets;

use Backend\Widgets\Form as WidgetBase;
use Backend\Classes\BackendController;
use Backend\Classes\FormTabs;

class Form extends WidgetBase
{
    public $realPreviewMode = false;
    public $previewMode = true;
    private $oneRealFieldForSave = null;

    public function render($options = [])
    {
        $this->realPreviewMode = !empty($options['preview']);
        $options['preview'] = true;
        return parent::render($options);
    }

    protected function loadAssets()
    {
        parent::loadAssets();
        $this->addCss('css/october.form.css', 'core');
    }

    private function isNewModel()
    {
        return !$this->model->getKey();
    }

    public function renderFieldElement($field)
    {
        if ($originalField = ((isset($field->config['editable']) && $field->config['editable'] == false) || $this->isNewModel())) {
            $prevPreviewMode = $this->previewMode;
            $this->previewMode = $this->realPreviewMode;
        }
        $content = parent::renderFieldElement($field);

        if ($originalField) {
            $this->previewMode = $prevPreviewMode;
        }
        if ($this->realPreviewMode || $originalField) {
            return $content;
        }
        if ($this->previewMode) {
            return $this->makePartial('not_editable', [
                'field' => $field,
                'content' => $content
            ]);
        }
        return $this->makePartial('editable', [
            'field' => $field,
            'content' => $content
        ]);
    }

    public function onEdit()
    {
        if (post('editable_field_active')) {
            $field = $this->getField(post('editable_field_active'));
            $result = ['#' . $field->getId('group') => $this->makePartial('field', ['field' => $field])];
        }
        else {
            $result = [];
        }
        if (!$this->realPreviewMode) {
            $this->previewMode = false;
        }
        $field = $this->getField(post('fieldname'));
        return $result + ['#' . $field->getId('group') => $this->makePartial('field', ['field' => $field])];
    }

    public function onCancel()
    {
        $field = $this->getField(post('fieldname'));
        return ['#' . $field->getId('group') => $this->makePartial('field', ['field' => $field])];
    }

    public function addFields(array $fields, $addToArea = null)
    {
        if (!post('fields')) {
            $names = [];
            if ($saveField = post('editable_field_active')) {
                $names[] = $saveField;
            }
            if ($editField = post('fieldname')) {
                $names[] = $editField;
            }
            if (count($names)) {
                foreach ($fields as $fieldName => $field) {
                    foreach ($names as $name) {
                        if ($name == $fieldName || strpos($fieldName, $name . '@') !== false) {
                            continue(2);
                        }
                    }
                    unset($fields[$fieldName]);
                }
            }
        }
        foreach ($fields as $name => $config) {

            $fieldObj = $this->makeFormField($name, $config);
            $fieldTab = is_array($config) ? array_get($config, 'tab') : null;

            /*
             * Check that the form field matches the active context
             */
            if ($fieldObj->context !== null) {
                $context = (is_array($fieldObj->context)) ? $fieldObj->context : [$fieldObj->context];
                if (!in_array($this->getContext(), $context)) {
                    continue;
                }
            }

            $this->allFields[$fieldObj->fieldName] = $fieldObj;//!!!!

            switch (strtolower($addToArea)) {
                case FormTabs::SECTION_PRIMARY:
                    $this->allTabs->primary->addField($name, $fieldObj, $fieldTab);
                    break;
                case FormTabs::SECTION_SECONDARY:
                    $this->allTabs->secondary->addField($name, $fieldObj, $fieldTab);
                    break;
                default:
                    $this->allTabs->outside->addField($name, $fieldObj);
                    break;
            }
        }
    }

    public function getSaveData()
    {
        if (!post('editable_field_active') && !$this->isNewModel()) {
            die('die');
        }
        $saveData = parent::getSaveData();
        if ($this->isNewModel()) {
            return $saveData;
        }
        foreach ($saveData as $key => $value) {
            if (!array_key_exists($key, $this->model->attributes)
                || $this->model->attributes[$key] !== $value
            ) {
                return $saveData;
            }
        }
        die();
    }
}