<?php namespace Academy\Cms\Widgets;

use Carbon\Carbon;

class Lists extends \Backend\Widgets\Lists {

    private $filterNotApply = false;

    public function renderScopeElement($scope)
    {
        $params = ['scope' => $scope];
        $scope->value = $this->getScopeValue($scope->columnName);

        switch ($scope->type) {
            case 'date':
            case 'datetime':
            case 'timesince':
                if ($scope->value && is_array($scope->value) && count($scope->value) === 2 &&
                    $scope->value[0] && $scope->value[0] instanceof Carbon &&
                    $scope->value[1] && $scope->value[1] instanceof Carbon
                ) {
                    $after = $scope->value[0]->format('Y-m-d H:i:s');
                    $before = $scope->value[1]->format('Y-m-d H:i:s');

                    if(strcasecmp($after, '0000-00-00 00:00:00') > 0) {
                        $params['afterStr'] = \Backend::dateTime($scope->value[0], ['formatAlias' => 'dateMin']);
                        $params['after']    = $after;
                    }
                    else {
                        $params['afterStr'] = '∞';
                        $params['after']    = null;
                    }

                    if(strcasecmp($before, '2999-12-31 23:59:59') < 0) {
                        $params['beforeStr'] = \Backend::dateTime($scope->value[1], ['formatAlias' => 'dateMin']);
                        $params['before']    = $before;
                    }
                    else {
                        $params['beforeStr'] = '∞';
                        $params['before']    = null;
                    }
                }
                break;
        }
        try {
            $result = $this->makePartial('scope_'.$scope->type, $params);
        }
        catch(\Exception $e) {
            $result = $this->makePartial('scope_partial', $params);
        }
        return $result;
    }

    protected function datesFromAjax($ajaxDates)
    {
        $dates = [];
        $dateRegex = '/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/';

        if (null !== $ajaxDates) {
            if (!is_array($ajaxDates)) {
                if(preg_match($dateRegex, $ajaxDates)) {
                    $dates = [$ajaxDates];
                }
            } else {
                foreach ($ajaxDates as $i => $date) {
                    if (preg_match($dateRegex, $date)) {
                        $dates[] = Carbon::createFromFormat('Y-m-d H:i:s', $date);
                    } elseif (empty($date)) {
                        if($i == 0) {
                            $dates[] = Carbon::createFromFormat('Y-m-d H:i:s', '0000-00-00 00:00:00');
                        } else {
                            $dates[] = Carbon::createFromFormat('Y-m-d H:i:s', '2999-12-31 23:59:59');
                        }
                    } else {
                        $dates = [];
                        break;
                    }
                }
            }
        }
        return $dates;
    }

    public function applyScopeToQuery($scope, $query)
    {
        if ($this->filterNotApply) {
            return $query;
        }
        if (is_string($scope)) {
            $scope = $this->getScope($scope);
        }
        if (!$scope->value) {
            return;
        }
        $columnName = $scope->sqlSelect ? \DB::raw($scope->sqlSelect) : $scope->columnName;
        switch ($scope->type) {
            case 'date':
            case 'datetime':
            case 'timesince':
                if (is_array($scope->value) && count($scope->value) > 1) {
                    list($after, $before) = array_values($scope->value);
                    if ($after && $after instanceof Carbon && $before && $before instanceof Carbon) {

                        if ($scope->relation) {
                            $query->whereHas($scope->relation, function($query) use ($scope) {
                                $query->where($scope->sqlSelect, '>=', $after->format('Y-m-d'));
                                $query->where($scope->sqlSelect, '<=', $before->format('Y-m-d'));
                            });
                        }
                        else {
                            $query->where($columnName, '>=', $after->format('Y-m-d'));
                            $query->where($columnName, '<=', $before->format('Y-m-d'));
                        }
                    }
                }
                break;
            case 'switch':
                if ($scope->relation) {
                    $query->whereHas($scope->relation, function($query) use ($scope) {
                        $query->where($scope->sqlSelect, $scope->value - 1);
                    });
                }
                else {
                    $query->where($columnName, $scope->value - 1);
                }
                break;
            default:
                if ($scope->relation) {
                    $query->whereHas($scope->relation, function($query) use ($scope) {
                        $query->whereIn($this->model->makeRelation($scope->relation)->getKeyName(), array_keys($scope->value));
                    });
                }
                else {
                    $query->whereIn($columnName, array_keys($scope->value));
                }
        }

        return $query;
    }

    public function applyAllScopesToQuery($query)
    {
        $this->defineListColumns();

        foreach ($this->getVisibleColumns() as $scopeName => $scope) {
            $scope = $this->getScope($scopeName);
            $this->applyScopeToQuery($scope, $query);
        }
        return $query;
    }

    public function getScope($scopeName)
    {
        $scope = $this->getColumn($scopeName);
        $scope->value = $this->getScopeValue($scopeName);
        return $scope;
    }

    public function getScopeValue($scope, $default = null)
    {
        $scopeName = is_string($scope) ? $scope : $scope->columnName;
        $cacheKey = 'scope-'.$scopeName;
        return $this->getSession($cacheKey, $default);
    }

    public function setScopeValue($scope, $value)
    {
        $scopeName = is_string($scope) ? $scope : $scope->columnName;
        $cacheKey = 'scope-'.$scopeName;
        $this->putSession($cacheKey, $value);
        return $scope->value = $value;
    }

    protected function getOptionsFromArray($scope, $searchQuery = null)
    {
        /*
         * Load the data
         */
        $options = $scope->options;

        if (is_scalar($options)) {
            $model = $this->scopeModels[$scope->scopeName];
            $methodName = $options;

            if (!$model->methodExists($methodName)) {
                throw new ApplicationException(Lang::get('backend::lang.filter.options_method_not_exists', [
                    'model'  => get_class($model),
                    'method' => $methodName,
                    'filter' => $scope->scopeName
                ]));
            }

            $options = $model->$methodName();
        }
        elseif (!is_array($options)) {
            $options = [];
        }

        /*
         * Apply the search
         */
        $searchQuery = Str::lower($searchQuery);
        if (strlen($searchQuery)) {
            $options = $this->filterOptionsBySearch($options, $searchQuery);
        }

        return $options;
    }

    protected function optionsFromAjax($options)
    {
        $processed = [];
        if (!is_array($options)) {
            return $processed;
        }

        foreach ($options as $option) {
            if (($id = array_get($option, 'id')) === '') {
                continue;
            }
            $processed[$id] = array_get($option, 'name');
        }
        return $processed;
    }

    public function onFilterUpdate()
    {
        $this->defineListColumns();

        if (!$scope = post('scopeName')) {
            return;
        }

        $scope = $this->getScope($scope);

        switch ($scope->type) {
            case 'switch':
                $value = post('value');
                $this->setScopeValue($scope, $value);
                break;

            case 'date':
            case 'datetime':
            case 'timesince':
                $dates = $this->datesFromAjax(post('options.dates'));

                if (!empty($dates)) {
                    list($after, $before) = $dates;
                    $dates = [$after, $before];
                }
                else {
                    $dates = null;
                }

                $this->setScopeValue($scope, $dates);
                break;
            default:
                $active = $this->optionsFromAjax(post('options.active'));
                $this->setScopeValue($scope, $active);
                break;
        }

        /*
         * Trigger class event, merge results as viewable array
         */
        $params = func_get_args();

        $result = $this->fireEvent('filter.update', [$params]);

        if ($result && is_array($result)) {
            return call_user_func_array('array_merge', $result);
        }
    }

    protected function getSortColumn()
    {
        if ($this->filterNotApply) {
            return false;
        }
        return parent::getSortColumn();
    }

    private static function strip_tags($text) {

        $tagsForClear = ['head', 'style', 'script', 'object', 'embed', 'applet', 'noframes', 'noscript', 'noembed'];
        $wtfToClear = [
            ['address', 'blockquote', 'center', 'del'],
            ['div', 'h[1-9]', 'ins', 'isindex', 'p', 'pre'],
            ['dir', 'dl', 'dt', 'dd', 'li', 'menu', 'ol', 'ul'],
            ['table', 'th', 'td', 'caption'],
            ['form', 'button', 'fieldset', 'legend', 'input'],
            ['label', 'select', 'optgroup', 'option', 'textarea'],
            ['frameset', 'frame', 'iframe'],
        ];
        $fromClear = [];
        $toClear = [];
        foreach ($tagsForClear as $tag) {
            $fromClear[] = '@<' . $tag . '[^>]*?>.*?</' . $tag . '>@siu';
            $toClear[] = ' ';
        }
        foreach ($wtfToClear as $wtf) {
            $fromClear[] =  '@</?((' . implode(')|(', $wtf) . '))@iu';
            $toClear[] = "\n\$0";
        }

        $text = preg_replace($fromClear, $toClear, $text);
        return trim(strip_tags($text));
    }

    protected function getAvailableOptions($scope, $searchQuery = null, $valueFromIgnore = false)
    {
        $this->filterNotApply = true;

        if ($scope->relation) {
            /*$relationType = $this->model->getRelationType($scope->relation));*/
            if ($scope->valueFrom) {
                $scope->sqlSelect = $scope->valueFrom;
                $scope->columnName = $scope->valueFrom;
            }
            $relation = $this->model->makeRelation($scope->relation);
            $result = $relation::select($relation->getKeyName())
                ->orderBy(\DB::raw($scope->sqlSelect))
                ->addSelect(\DB::raw($scope->sqlSelect . ' as ' . $scope->columnName))
                ->addSelect($relation->getKeyName());

            if (!is_null($searchQuery)) {
                $result->searchWhere($searchQuery, [$scope->sqlSelect]);
            }
            $result = $result->lists($scope->columnName, $relation->getKeyName());
        }
        else {
            if (!$valueFromIgnore && $scope->valueFrom) {
                if (isset($this->allColumns[$scope->valueFrom])) {
                    return $this->getAvailableOptions($this->getScope($scope->valueFrom), $searchQuery, true);
                }
                $scope->columnName = $scope->valueFrom;
                $scope->sqlSelect = null;
            }
            $columnName = $scope->sqlSelect ? \DB::raw($scope->sqlSelect) : $scope->columnName;
            $sqlSelect = $scope->sqlSelect ? \DB::raw($scope->sqlSelect . ' AS ' . $scope->columnName) : $scope->columnName;
            $result = $this
                ->prepareModel()
                //->addSelect($sqlSelect)
                ->groupBy($columnName)
                ->orderBy($columnName);

            if (!is_null($searchQuery)) {
                $result->searchWhere($searchQuery, [$columnName]);
            }

            if ($scope->type == 'partial') {
                $return = [];
                foreach ($result->get() as $record) {
                    $return[$record->{$scope->columnName}] = self::strip_tags($this->getColumnValue($record, $scope));
                }
                $result = $return;
            }
            else {
                $result = $result->lists($scope->columnName, $scope->columnName);
            }
        }
        $this->filterNotApply = false;

        return $result;
    }

    protected function optionsToAjax($options)
    {
        $processed = [];
        foreach ($options as $id => $result) {
            $processed[] = ['id' => $id, 'name' => $result];
        }
        return $processed;
    }

    protected function filterActiveOptions(array $activeKeys, array &$availableOptions)
    {
        $active = [];
        foreach ($availableOptions as $id => $option) {
            if (!in_array($id, $activeKeys)) {
                continue;
            }

            $active[$id] = $option;
            unset($availableOptions[$id]);
        }

        return $active;
    }

    public function onFilterGetOptions()
    {
        $this->defineListColumns();

        $searchQuery = post('search');
        if (!$scopeName = post('scopeName')) {
            return;
        }

        $scope = $this->getScope($scopeName);
        $activeKeys = $scope->value ? array_keys($scope->value) : [];
        $available = $this->getAvailableOptions($scope, $searchQuery);
        $active = $searchQuery ? [] : $this->filterActiveOptions($activeKeys, $available);

        return [
            'scopeName' => $scopeName,
            'options' => [
                'available' => $this->optionsToAjax($available),
                'active'    => $this->optionsToAjax($active),
            ]
        ];
    }
}