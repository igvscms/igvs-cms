<?php namespace Academy\Cms;

use Backend;
use System\Classes\PluginBase;
use Lang;
use Event;
use Backend\Classes\WidgetManager;

class Plugin extends PluginBase
{
    public $require = ['Igvs.Courses'];

    public function pluginDetails()
    {
        return [
            'name'        => 'Academy.Cms',
            'description' => 'Academy.Cms',
            'author'      => 'Academy',
            'icon'        => 'icon-delicious'
        ];
    }

    public function registerFormWidgets()
    {
        return [
            'Academy\Cms\FormWidgets\FormInclude' => [
                'label' => 'Form Include',
                'code' => 'forminclude'
            ],
            'Academy\Cms\FormWidgets\FileUpload' => [
                'label' => 'File Upload',
                'code' => 'fileupload'
            ],
            'Academy\Cms\FormWidgets\DropDown' => [
                'label' => 'dropdown with multiple',
                'code' => 'mDropdown'
            ]
        ];
    }

    public function registerListColumnTypes()
    {
        return [
            'case' => function($value, $column, $record) {

                if (is_array($column->defaults)) {
                    $values = $column->defaults;
                }
                else {
                    $method = 'get'.studly_case(is_null($column->defaults) ? $column->columnName : $column->defaults).'Options';
                    if (method_exists($record, $method)) {
                        $values = $record->$method($value);
                    }
                    elseif (method_exists($record, 'getDropdownOptions')) {
                        $values = $record->getDropdownOptions($column->columnName, $value);
                    }
                    else {
                        $values = [];
                    }
                }
                if (array_key_exists($value, $values)) {
                    return $values[$value];
                }
                return $value;
            },
        ];
    }
}