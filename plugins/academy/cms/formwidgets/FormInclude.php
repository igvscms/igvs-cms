<?php namespace Academy\Cms\FormWidgets;

use Db;
use Lang;
use Backend\Classes\FormField;
use Backend\Classes\FormWidgetBase;
use ApplicationException;
use SystemException;
use Illuminate\Database\Eloquent\Relations\Relation as RelationBase;

class FormInclude extends FormWidgetBase
{
    use \Backend\Traits\FormModelWidget;
    use \Backend\Traits\FormModelSaver;

    public $form;
    public $formContext;
    public $modelKeyFrom;
    private $relationObject;
    private $relationModel;
    private $formWidget;
    protected $defaultAlias = 'forminclude';
    private $action;

    public $renderFormField;

    public function init()
    {
        $this->fillFromConfig([
            'form',
            'formContext',
            'modelKeyFrom',
        ]);
    }

    public function getFormWidget()
    {
        $this->relationObject = $this->getRelationObject();
        $relationModel = $this->relationModel = $this->relationObject->getModel();

        if (!is_null($modelKeyId = $this->modelKeyFrom)
            && !is_null($relation_id = $this->model->$modelKeyId)
        ) {
            $this->relationModel = $relationModel::find($relation_id);
            $this->action = 'update';
        }
        else {
            $this->action = 'create';
        }
        //нужно приаттачить релэйшен к модели
        if ($this->getRelationType($this->valueFrom) == 'hasMany') {
            $relationModel->setAttribute(
                $this->relationObject->getPlainForeignKey(),
                $this->relationObject->getParentKey()
            );
        }

        //$this->formField->readOnly = true; в конфиге если текущее поле задизаблено, тоже дизаблить
        //$this->formField->disabled = true;
        $config = $this->makeConfig($this->form);
        $config->model = $this->relationModel;
        $config->arrayName = class_basename($this->relationModel);
        $config->context = $this->formContext;

        return $this->formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
    }

    public function render()
    {
        return $this->getFormWidget()->render();
    }

    public function getViewMode($model, $field)
    {
        switch ($model->getRelationType($field)) {
            case 'hasMany':
            case 'morphMany':
            case 'morphToMany':
            case 'morphedByMany':
            case 'belongsToMany':
                return 'multi';
            case 'hasOne':
            case 'morphOne':
            case 'belongsTo':
                return 'single';
        }
    }

    private function onRelationManageCreate()
    {
        $saveData = $this->formWidget->getSaveData();
        $sessionKey = $this->formWidget->getSessionKey();
        $newModel = $this->relationModel;
        $doSave = is_null(post('fields'));//костыль

        switch ($this->getViewMode($this->model, $this->valueFrom)) {
            case 'multi':
                if ($this->getRelationType($this->valueFrom) == 'hasMany') {
                    $newModel->setAttribute(
                        $this->relationObject->getPlainForeignKey(),
                        $this->relationObject->getParentKey()
                    );
                }
                $modelsToSave = $this->prepareModelsToSave($newModel, $saveData);
                if ($doSave) {
                    foreach ($modelsToSave as $modelToSave) {
                        $modelToSave->save(null, $sessionKey);
                    }

                    $this->relationObject->add($newModel);
                }
            break;
            case 'single':
                $modelsToSave = $this->prepareModelsToSave($newModel, $saveData);
                if ($doSave) {
                    if ($this->getRelationType($this->valueFrom) != 'hasOne') {
                        $newModel->save();
                    }

                    $this->relationObject->add($newModel);

                    if ($this->relationType == 'belongsTo') {
                        $parentModel = $this->relationObject->getParent();
                        if ($parentModel->exists) {
                            $parentModel->save();
                        }
                    }
                }
        }
    }

    public function getSaveValue($value)
    {
        $this->getFormWidget();
        if (!count($saveData = $this->formWidget->getSaveData())) {
            return FormField::NO_SAVE_DATA;//-1
        }
        if ($this->action == 'update') {
            $modelsToSave = $this->prepareModelsToSave($this->formWidget->model, $saveData);
            if (is_null(post('fields'))) {
                foreach ($modelsToSave as $modelToSave) {
                    $modelToSave->save(null, $this->formWidget->getSessionKey());
                }
            }
        }
        else {
            $this->onRelationManageCreate();
        }
        return FormField::NO_SAVE_DATA;//-1
    }
}