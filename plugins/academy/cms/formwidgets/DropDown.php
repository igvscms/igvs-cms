<?php namespace Academy\Cms\FormWidgets;

use Backend\Classes\FormWidgetBase;

class DropDown extends FormWidgetBase
{
    public $multiple;
    public $emptyOption;
    public $options;
    protected $defaultAlias = 'dropdown';

    public function init()
    {
        $this->fillFromConfig([
            'multiple',
            'emptyOption',
            'options',
        ]);
    }

    public function render()
    {
        return $this->makePartial('index', [
            'field' => $this->formField,
            'fieldOptions' => $this->getOptionsFromModel()
        ]);
    }

    protected function objectMethodExists($object, $method)
    {
        if (method_exists($object, 'methodExists')) {
            return $object->methodExists($method);
        }
        return method_exists($object, $method);
    }

    protected function getOptionsFromModel()
    {
        if (!$this->options) {
            try {
                list($model, $attribute) = $this->formField->resolveModelAttribute($this->model, $this->formField->fieldName);
            }
            catch (\Exception $ex) {
                throw new \ApplicationException(\Lang::get('backend::lang.field.options_method_invalid_model', [
                    'model' => get_class($this->model),
                    'field' => $this->formField->fieldName
                ]));
            }

            $methodName = 'get'.studly_case($attribute).'Options';
            if (
                !$this->objectMethodExists($model, $methodName) &&
                !$this->objectMethodExists($model, 'getDropdownOptions')
            ) {
                throw new \ApplicationException(\Lang::get('backend::lang.field.options_method_not_exists', [
                    'model'  => get_class($model),
                    'method' => $methodName,
                    'field'  => $this->formField->fieldName
                ]));
            }

            if ($this->objectMethodExists($model, $methodName)) {
                return $model->$methodName($this->formField->value, $this->data);
            }
            return $model->getDropdownOptions($attribute, $this->formField->value, $this->data);
        }
        if (is_array($this->options)) {
            return $this->options;
        }
        if (!$this->objectMethodExists($this->model, $this->options)) {
            throw new \ApplicationException(\Lang::get('backend::lang.field.options_method_not_exists', [
                'model'  => get_class($this->model),
                'method' => $this->options,
                'field'  => $this->formField->fieldName
            ]));
        }
        $fieldOptions = $this->options;
        return $this->model->$fieldOptions($this->formField->value, $this->formField->fieldName, $this->data);
    }
}