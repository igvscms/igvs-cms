<?php namespace Academy\Cms\Behaviors;

use Backend\Behaviors\FormController as ControllerBehavior;

class FormController extends ControllerBehavior
{
    public function __construct($arg) {
        parent::__construct($arg);

        $this->addJs('scripts/main.js', 'core');
        $this->addCss('styles/main.css', 'core');
    }

    public function initForm($model, $context = null)
    {
        if ($context !== null)
            $this->context = $context;

        $context = $this->formGetContext();

        /*
         * Each page can supply a unique form definition, if desired
         */
        $formFields = $this->config->form;

        if ($context == self::CONTEXT_CREATE) {
            $formFields = $this->getConfig('create[form]', $formFields);
        }
        elseif ($context == self::CONTEXT_UPDATE) {
            $formFields = $this->getConfig('update[form]', $formFields);
        }
        elseif ($context == self::CONTEXT_PREVIEW) {
            $formFields = $this->getConfig('preview[form]', $formFields);
        }

        $config = $this->makeConfig($formFields);
        $config->model = $model;
        $config->arrayName = class_basename($model);
        $config->context = $context;

        /*
         * Form Widget with extensibility
         */
        $this->formWidget = $this->makeWidget('Academy\Cms\Widgets\Form', $config);

        $this->formWidget->bindEvent('form.extendFieldsBefore', function () {
            $this->controller->formExtendFieldsBefore($this->formWidget);
        });

        $this->formWidget->bindEvent('form.extendFields', function ($fields) {
            $this->controller->formExtendFields($this->formWidget, $fields);
        });

        $this->formWidget->bindEvent('form.beforeRefresh', function ($holder) {
            $result = $this->controller->formExtendRefreshData($this->formWidget, $holder->data);
            if (is_array($result)) $holder->data = $result;
        });

        $this->formWidget->bindEvent('form.refreshFields', function ($fields) {
            return $this->controller->formExtendRefreshFields($this->formWidget, $fields);
        });

        $this->formWidget->bindEvent('form.refresh', function ($result) {
            return $this->controller->formExtendRefreshResults($this->formWidget, $result);
        });

        $this->formWidget->bindToController();

        /*
         * Detected Relation controller behavior
         */
        if ($this->controller->isClassExtendedWith('Backend.Behaviors.RelationController')) {
            $this->controller->initRelation($model);
        }

        $this->prepareVars($model);
        $this->model = $model;
    }
}