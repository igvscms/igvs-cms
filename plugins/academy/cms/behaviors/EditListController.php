<?php namespace Academy\Cms\Behaviors;

use Backend\Behaviors\ListController as ControllerBehavior;

class EditListController extends ControllerBehavior
{
    use \Backend\Traits\FormModelSaver;

    const PARAM_COLUMN = '_column_name';

    private $formWidget = array();

    public function __construct($controller)
    {
        parent::__construct($controller);
        $this->addCss('main.css');
    }

    public function formGetContext($columnConfig)
    {
        if (!isset($columnConfig->columns[$columnName = post(self::PARAM_COLUMN)])) {
            return;
        }
        $column = $columnConfig->columns[$columnName];
        return [
            isset($column['form'])    ? $column['form']    : $columnConfig->form,
            isset($column['context']) ? $column['context'] : 'update'
        ];
    }

    public function makeList($definition = null)
    {
        $this->lastDefinition = $definition;
        if (!$definition || !isset($this->listDefinitions[$definition])) {
            $definition = $this->primaryDefinition;
        }

        $listConfig = $this->controller->listGetConfig($definition);

        /*
         * Create the model
         */
        $class = $listConfig->modelClass;
        $model = new $class;
        $model = $this->controller->listExtendModel($model, $definition);

        /*
         * Prepare the list widget
         */
        $columnConfig = $this->makeConfig($listConfig->list);
        $columnConfig->model = $model;
        $columnConfig->alias = $definition;

        /*
         * Prepare the columns configuration
         */
        $configFieldsToTransfer = [
            'form',
            'recordUrl',
            'recordOnClick',
            'recordsPerPage',
            'noRecordsMessage',
            'defaultSort',
            'showSorting',
            'showSetup',
            'showCheckboxes',
            'showTree',
            'treeExpanded',
            'customViewPath',
        ];

        foreach ($configFieldsToTransfer as $field) {
            if (isset($listConfig->{$field})) {
                $columnConfig->{$field} = $listConfig->{$field};
            }
        }

        /*
         * List Widget with extensibility
         */
        $widget = $this->makeWidget('Academy\Cms\Widgets\EditLists', $columnConfig);

        $widget->bindEvent('filter.update', function () use ($widget) {
            return $widget->onRefresh();
        });

        $widget->bindEvent('filter.extendQuery', function($query, $scope) {
            $this->controller->listFilterExtendQuery($query, $scope);
        });

        $widget->addFilter([$widget, 'applyAllScopesToQuery']);
        $widget->bindEvent('list.extendColumns', function () use ($widget) {
            $this->controller->listExtendColumns($widget);
        });

        $widget->bindEvent('list.extendQueryBefore', function ($query) use ($definition) {
            $this->lastDefinition = $definition;
            $this->controller->listExtendQueryBefore($query, $definition);
        });

        $widget->bindEvent('list.extendQuery', function ($query) use ($definition) {
            $this->lastDefinition = $definition;
            $this->controller->listExtendQuery($query, $definition);
        });

        $widget->bindEvent('list.injectRowClass', function ($record) use ($definition) {
            $this->lastDefinition = $definition;
            return $this->controller->listInjectRowClass($record, $definition);
        });

        $widget->bindEvent('list.overrideColumnValue', function ($record, $column, $value) use ($definition) {
            $this->lastDefinition = $definition;
            return $this->controller->listOverrideColumnValue($record, $column->columnName, $definition);
        });

        $widget->bindEvent('list.overrideColumn', function ($record, $columnName, $config) use ($definition) {
            $this->lastDefinition = $definition;
            return $this->controller->listOverrideColumn($record, $columnName, $config, $definition);
        });

        $widget->bindEvent('list.overrideHeaderValue', function ($column, $value) use ($definition) {
            $this->lastDefinition = $definition;
            return $this->controller->listOverrideHeaderValue($column->columnName, $definition);
        });

        $widget->bindToController();

        /*
         * Prepare the toolbar widget (optional)
         */
        if (isset($listConfig->toolbar)) {
            $toolbarConfig = $this->makeConfig($listConfig->toolbar);
            $toolbarConfig->alias = $widget->alias . 'Toolbar';
            $toolbarWidget = $this->makeWidget('Backend\Widgets\Toolbar', $toolbarConfig);
            $toolbarWidget->bindToController();
            $toolbarWidget->cssClasses[] = 'list-header';

            /*
             * Link the Search Widget to the List Widget
             */
            if ($searchWidget = $toolbarWidget->getSearchWidget()) {
                $searchWidget->bindEvent('search.submit', function () use ($widget, $searchWidget) {
                    $widget->setSearchTerm($searchWidget->getActiveTerm());
                    return $widget->onRefresh();
                });

                $widget->setSearchOptions([
                    'mode' => $searchWidget->mode,
                    'scope' => $searchWidget->scope,
                ]);

                // Find predefined search term
                $widget->setSearchTerm($searchWidget->getActiveTerm());
            }

            $this->toolbarWidgets[$definition] = $toolbarWidget;
        }

        /*
         * Prepare the filter widget (optional)
         */
        if (isset($listConfig->filter)) {
            $widget->cssClasses[] = 'list-flush';

            $filterConfig = $this->makeConfig($listConfig->filter);
            $filterConfig->alias = $widget->alias . 'Filter';
            $filterWidget = $this->makeWidget('Backend\Widgets\Filter', $filterConfig);
            $filterWidget->bindToController();

            /*
             * Filter the list when the scopes are changed
             */
            $filterWidget->bindEvent('filter.update', function () use ($widget, $filterWidget) {
                return $widget->onRefresh();
            });

            /*
             * Extend the query of the list of options
             */
            $filterWidget->bindEvent('filter.extendQuery', function($query, $scope) {
                $this->controller->listFilterExtendQuery($query, $scope);
            });

            // Apply predefined filter values
            $widget->addFilter([$filterWidget, 'applyAllScopesToQuery']);

            $this->filterWidgets[$definition] = $filterWidget;
        }

        if (isset($listConfig->form) && !is_null($formContext = $this->formGetContext($columnConfig))) {

            list($formFields, $context) = $formContext;

            if ($context == 'update') {
                $formFields = $this->getConfig('update[form]', $formFields);
            }

            $config = $this->makeConfig($formFields);

            $tempModel = new $class();
            $config->model = $widget->prepareModel()->where($tempModel->getTable() . '.' . $tempModel->getkeyName(), post('record_id'))->first();
            //$config->model = $class::withTrashed()->find(post('record_id'));
            $config->arrayName = class_basename($config->model);
            $config->context = $context;
            $this->formWidget[$definition] = $this->makeWidget('Backend\Widgets\Form', $config);
            //$this->formWidget[$definition]->alias = $this->alias . 'Form';
            $this->formWidget[$definition]->bindToController();
        }

        return $widget;
    }

    private function getRelationManageTitle($model, $fieldName)
    {
        list($author, $plugin, $modelPath, $model) = explode('\\', get_class($model));
        return strtolower(join('.', [$author, $plugin]) . '::' . strtolower(str_singular($model))) . '.formlist.' . $fieldName;
    }

    public function onClickListColumn()
    {
        $this->controller->pageAction();
        if (!count($this->formWidget)) {
            throw new \ApplicationException(\Lang::get('backend::lang.relation.missing_definition', compact('field')));
        }
        $definition = $this->lastDefinition ?: $this->primaryDefinition;
        $formWidget = $this->formWidget[$definition];
        $this->vars['relationManageWidget'] = $formWidget;
        $this->vars['relationManageId'] = $formWidget->model->getKey();
        $this->vars['newSessionKey'] = str_random(40);
        $this->vars['relationField'] = post(self::PARAM_COLUMN);
        $this->vars['relationManageTitle'] = $this->getRelationManageTitle($formWidget->model, post(self::PARAM_COLUMN));
        $this->vars['relationLabel'] = 'relationLabel';
        return $this->makePartial('manage_form');
    }

    protected function getLang($name, $default = null, $extras = [])
    {
        $name = $this->getConfig($name, $default);
        $vars = [
            'name' => \Lang::get($this->getConfig('name', 'backend::lang.model.name'))
        ];
        $vars = array_merge($vars, $extras);
        return \Lang::get($name, $vars);
    }

    public function onUpdateColumn()
    {
        $this->controller->pageAction();
        if (!count($this->formWidget)) {
            throw new \ApplicationException(\Lang::get('backend::lang.relation.missing_definition', compact('field')));
        }
        $definition = $this->lastDefinition ?: $this->primaryDefinition;
        $formWidget = $this->formWidget[$definition];
        $modelsToSave = $this->prepareModelsToSave($formWidget->model, $formWidget->getSaveData());

        \Db::transaction(function() use ($modelsToSave, $formWidget) {
            foreach ($modelsToSave as $modelToSave) {
                $modelToSave->save(null, $formWidget->getSessionKey());
            }
        });

        \Flash::success($this->getLang('update[flashSave]', 'backend::lang.form.update_success'));
        $listWidget = $this->listWidgets[$definition];

        $model = $listWidget->prepareModel()->where($formWidget->model->getTable() . '.' . $formWidget->model->getkeyName(), post('record_id'))->first();
        if (!$model) {
            return ['#EditLists-'.post('record_id') => ''];
        }

        $listWidget->getSelectGroup();

        return ['#EditLists-'.post('record_id') =>$listWidget->makePartial('list_body_row_container', [
            'expanded' => $listWidget->showTree ? $listWidget->isTreeNodeExpanded($model) : null,
            'showCheckboxes' => $listWidget->showCheckboxes,
            'record' => $model,
            'showTree' => $listWidget->showTree,
            'columns' => $listWidget->getVisibleColumns(),
            'showSetup' => $listWidget->showSetup,
            'treeLevel' => $listWidget->groupCount
        ])];
    }
    public function listOverrideColumn($record, $columnName, $config, $definition = null) {}

    public function listRender($definition = null)
    {
        $this->lastDefinition = $definition;
        return parent::listRender($definition);
    }
    public function listRefresh($definition = null)
    {
        $this->lastDefinition = $definition;
        return parent::listRefresh($definition);
    }
    public function listGetWidget($definition = null)
    {
        $this->lastDefinition = $definition;
        return parent::listGetWidget($definition);
    }
    public function index_onDelete()
    {
        $this->lastDefinition = post('definition');
        return parent::index_onDelete();
    }
}