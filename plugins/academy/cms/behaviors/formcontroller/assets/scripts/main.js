function is_an_ancestor(el, ancestor) {
    while (el !== document.body && el !== ancestor && el !== null) {
        el = el.parentNode;
    }
    return el === ancestor;
}

function listenEventsBlur(el, callback) {
    var focusTimer;
    var blurTimer;
    var mouseinelement = true;
    var mouseTimer;

    document.body.setAttribute('tabindex', -1);
    document.body.focus();

    var remove = function(log) {
        document.removeEventListener('mouseover', mouseover);
        document.removeEventListener('mouseout', mouseout);
        document.removeEventListener('mousedown', docClick);
        document.removeEventListener('blur', onBlur, true);
        el.removeEventListener('focus', onFocus, true);
        callback(el);
        console.log(log);
    }
    var onBlur = function(event) {
        if (!mouseinelement) {
            remove('кликнули за пределами документа');
        }
        blurTimer = setTimeout(function() {
            document.body.focus();
        }, 0);
        return false;
    }
    var mouseover = function() {
        clearTimeout(mouseTimer);
        mouseinelement = true;
    }
    var mouseout = function() {
        mouseTimer = setTimeout(function() {
            mouseinelement = false;
        }, 0);
    }
    var docClick = function(event) {
        if (!is_an_ancestor(event.target, el)) {
            clearTimeout(focusTimer);
            focusTimer = setTimeout(function() {
                remove('кликнули за пределами элемента');
            }, 200);
        }
    }
    var onFocus = function(event) {
        clearTimeout(blurTimer);
        clearTimeout(focusTimer);
    }

    document.addEventListener('mouseover', mouseover);
    document.addEventListener('mouseout', mouseout);
    document.addEventListener('mousedown', docClick);
    document.addEventListener('blur', onBlur, true);
    el.addEventListener('focus', onFocus, true);
}

function closeEditElement(el) {
    listenEventsBlur(el, function(el) {
        el.querySelector('.save').click();
    });
}