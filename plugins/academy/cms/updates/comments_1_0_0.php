<?php namespace Academy\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Comments_1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('academy_cms_comments', function($table)
        {
            $table->increments('id');

            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();

            $table->string('title');
            $table->text('description');
            $table->string('field');

            $table->integer('attachment_id')->unsigned();
            $table->string('attachment_type');
            $table->boolean('is_public');

            $table->timestamps();

            $table->foreign('user_id','f_userId_academyCmsComments')
                ->references('id')
                ->on('backend_users')
                ->onDelete('set null');//or cascade??

            $table->foreign('parent_id','f_parentId_academyCmsComments')
                ->references('id')
                ->on('academy_cms_comments')
                ->onDelete('set null');//or cascade??
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_cms_comments');
    }
}