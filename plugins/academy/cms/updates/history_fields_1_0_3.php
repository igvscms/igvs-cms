<?php namespace Academy\Cms\Updates;

use October\Rain\Database\Updates\Migration;

class History_fields_1_0_3 extends Migration
{
    public function up()
    {
        \Schema::create('academy_cms_history_fields', function($table)
        {
            //id истории поля
            $table->integer('history_id')->unsigned();
            //название поля измененной таблицы
            $table->string('name');
            //присвоенное значение поля
            $table->binary('value')->nullable();
            //считаем шаги до следующего сжатия, если = 0, данные хранятся без зжатия
            $table->tinyInteger('compressed_step')->unsigned();
            //Связываем поле с таблицей истории
            $table->foreign('history_id','f_historyId_academyCmsHistoryFiles')
                ->references('id')
                ->on('academy_cms_histories')
                ->onDelete('cascade');
        });
    }
    public function down()
    {
        Schema::dropIfExists('academy_cms_history_fields');
    }
}