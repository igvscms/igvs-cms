<?php namespace Academy\Cms\Updates;

use October\Rain\Database\Updates\Migration;

class Histories_1_0_3 extends Migration
{
    public function up()
    {
        \Schema::create('academy_cms_histories', function($table)
        {
            $table->increments('id');
            //пользователь, внесший изменения
            $table->integer('user_id')->unsigned()->nullable();
            //id таблицы, изменения которой фиксируем
            $table->integer('attachment_id')->unsigned();
            //название класса таблицы, изменения которой фиксируем
            $table->string('attachment_type');
            //флаг требует октобер, иначе не работает
            $table->boolean('is_public');
            //дата внесения изменений
            $table->timestamps();
            //Связываем поле с таблицей пользователей
            $table->foreign('user_id','f_userId_academyCmsHistories')
                ->references('id')
                ->on('backend_users')
                ->onDelete('set null');
        });
    }
    public function down()
    {
        Schema::dropIfExists('academy_cms_histories');
    }
}