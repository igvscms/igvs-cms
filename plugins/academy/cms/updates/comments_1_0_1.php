<?php namespace Academy\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Comments_1_0_1 extends Migration
{
    public function up()
    {
        Schema::table('academy_cms_comments', function($table)
        {
            $table->binary('data')->nullable();
        });
    }

    public function down()
    {
        Schema::table('academy_cms_comments', function($table)
        {
            $table->dropColumn('data');
        });
    }
}