<?php namespace Academy\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Beenread_1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('academy_cms_beenread', function($table)
        {
            $table->integer('user_id')->unsigned()->nullable();

            $table->string('field');

            $table->integer('attachment_id')->unsigned();
            $table->string('attachment_type');
            $table->boolean('sort_order');
            $table->boolean('is_public');

            $table->timestamps();

            $table->foreign('user_id','f_userId_academyCmsBeenread')
                ->references('id')
                ->on('backend_users')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_cms_beenread');
    }
}