<?php namespace Academy\Cms\Updates;

use October\Rain\Database\Updates\Migration;

class Comments_1_0_2 extends Migration {
    public function up() {
        \Schema::table('academy_cms_comments', function($table) {
            $table->string('relation');
        });
    }
    public function down() {
        \Schema::table('academy_cms_comments', function($table) {
            $table->dropColumn('relation');
        });
    }
}