<?php namespace Academy\Cms\Models;

use Academy\System\Classes\Differ;

class HistoryField extends \Model
{
    protected $table = 'academy_cms_history_fields';
    protected $guarded = ['*'];
    public $belongsTo = ['history' => 'Academy\Cms\Models\History'];
    protected $fillable = ['name', 'value', 'compressed_step'];
    public $timestamps = false;

    public $prevValue;

    public function afterFetch()
    {
        if ($this->compressed_step == 0) {
            return;
        }
        $fieldTable = $this->table;
        $history = $this->history()->select('attachment_type', 'created_at', 'attachment_id')->first();
        $historyTable = $history->getTable();
        $values = History::join($fieldTable, "$fieldTable.history_id", '=', "$historyTable.id")
            ->where("$historyTable.attachment_id", $history->attachment_id)
            ->where("$historyTable.attachment_type", $history->attachment_type)
            ->where("$historyTable.created_at", '<=', $history->created_at)
            ->where("$historyTable.id", '!=', $this->history_id)
            ->where("$fieldTable.name", $this->name)
            ->where("$fieldTable.compressed_step", '<', $this->compressed_step)
            ->orderBy("$historyTable.created_at", 'desc')
            ->limit($this->compressed_step)
            ->select("$fieldTable.value", "$fieldTable.compressed_step")
            ->lists('value', 'compressed_step');

        if (count($values) < $this->compressed_step) {
            throw new \Exception("Не удалось восстановить значения поля {$this->name} для модели класса {$history->attachment_type} с id {$history->attachment_id} за {$history->created_at}");
        }

        $clearValue = $values[0];
        unset($values[0]);

        $values[$this->compressed_step] = $this->value;

        foreach ($values as $step => $value) {
            $values[$step] = json_decode(bzdecompress($value), true);
        }
        ksort($values);

        $lastDiff = array_pop($values);

        $this->prevValue = Differ::setMoreDiff($clearValue, $values);

        $this->value = Differ::setDiff($this->prevValue, $lastDiff);
    }

    public function beforeUpdate()
    {
        return false;//запрещаем обновлять модель
    }

    public function beforeDelete()
    {
        return false;//запрещаем удалять модель
    }
}