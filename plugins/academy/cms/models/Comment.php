<?php namespace Academy\Cms\Models;

class Comment extends \Model
{
    public $table = 'academy_cms_comments';
    protected $guarded = ['*'];
    public $belongsTo = [
        'user' => 'Backend\Models\User',
        'parent' => 'Academy\Cms\Comment',
    ];
    public $fillable = [
        'user_id',
        'description',
        'attachment_id',
        'attachment_type',
        'files',
    ];
    public $attachMany = ['files' => 'System\Models\File'];

    public function beforeCreate()
    {
        $this->user = \BackendAuth::getUser();
        //$this->field = post('_relation_field', $this->field);
    }

    public function getChildren()
    {
        return self::where('parent_id', $this->id)->get();
    }

    public function getChildCount()
    {
        return self::where('parent_id', $this->id)->count();
    }

    public function scopeGetNested($query)
    {
        return $query->where('parent_id', null)->get();
    }

    public function getSelfAttribute()
    {
        $class = $this->attachment_type;
        if (!$object = $class::find($this->attachment_id)) {
            return null;
        }
        $object->return_comment = $this;
        return $object;
    }
}
