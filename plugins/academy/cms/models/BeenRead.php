<?php namespace Academy\Cms\Models;

/* for attachMany relations */

use Igvs\Courses\Models\ModuleContent;

class BeenRead extends \Model
{
    public $table = 'academy_cms_beenread';
    protected $guarded = ['*'];
    protected $fillable = ['is_public','field'];
    public $belongsTo = [
        'user' => 'Backend\Models\User',
    ];

    public function beforeSave()
    {
        if (!$this->user = \BackendAuth::getUser())
            return false;

        $attributes = $this->attributes;
        $updated_at = &$attributes['updated_at'];

        unset($attributes['updated_at']);
        unset($attributes['sort_order']);
        unset($attributes['created_at']);
        unset($attributes['is_public']);

        if (self::where($attributes)->first()) {
            self::where($attributes)
                ->update(['updated_at' => $updated_at]);
            return false;
        }
    }

    private static function get(&$value, $default = null)
    {
        return isset($value) ? $value : $default;
    }

    private static function getValuesFromParams($need, $params)
    {
        $result = [];
        foreach ($params as $param) {
            if (($pos = array_search($param['column'], $need)) === false) {
                continue;
            }
            $result[$need[$pos]] = $param['value'];
            unset($need[$pos]);
            if (!count($need)) {
                break;
            }
        }
        return $result;
    }

    public function scopeCount($query, $attr = [], $user_id = null)
    {
        //dd(debug_backtrace());
        $fields = self::getValuesFromParams([
                $type  = $this->table . '.attachment_type',
                $field = 'field',
                $id    = $this->table . '.attachment_id'
            ], $query->getQuery()->wheres);

        if (is_null($user_id)) {
            if (!$user = \BackendAuth::getUser()) {
                return $query = '0 ';
            }
            $user_id = $user->id;
        }

        $class = $fields[$type];
        $options = explode('_', $fields[$field], 2);
        $model = $class::find($fields[$id]);

        $read_field = self::get($attr['read'], $options[1]);
        $user_field = self::get($attr['user'], 'user_id');
        $time_field = self::get($attr['time'], 'created_at');

        $count = $model->$read_field()
                       ->where($user_field, '!=', $user_id);

        $self = self::where('user_id',         $user_id)
                    ->where('field',           $fields[$field])
                    ->where('attachment_id',   $fields[$id])
                    ->where('attachment_type', $fields[$type])
                    ->first();

        if ($self) {
            $count->where($time_field, '>', $self->updated_at);
        }

        return $query = $count->count() . ' ';
    }

    public function scopeGetBy($query, $class, $field)
    {
        if (!$user = \BackendAuth::getUser()) {
            return $query->whereRaw('0');
        }
        $beenTable = $this->getTable();
        return $query->where("$beenTable.attachment_type", $class)
            ->where("$beenTable.field", $field)
            ->where("$beenTable.user_id", $user->id);
    }

    public static function getBy($class, $field, $query = null)
    {
        $been = new self();
        if (is_null($query)) {
            $query = self::whereRaw(1);
        }
        return $been->scopeGetBy($query, $class, $field);
    }

    public static function getReadCommentsByCourseId($user_id, $course_id)
    {
        return self::select(\Db::raw('academy_cms_beenread.attachment_id as attachment_id,
  SUM(CASE WHEN current_record.user_id IS NULL AND (academy_cms_beenread.created_at > current_record.updated_at OR current_record.updated_at IS NULL) THEN 1 ELSE 0 END) as count'))
            ->leftjoin('igvs_courses_modules_contents', 'igvs_courses_modules_contents.id', '=', 'academy_cms_beenread.attachment_id')
            ->leftjoin('academy_cms_beenread as current_record', function($join) use($user_id) {
                $join->on('current_record.attachment_id', '=', 'academy_cms_beenread.attachment_id')
                    ->where('current_record.user_id', '=', $user_id);
            })
            ->where('academy_cms_beenread.field', 'read_comments')
            ->where('academy_cms_beenread.attachment_type', 'Igvs\Courses\Models\ModuleContent')
            ->where('igvs_courses_modules_contents.course_id', $course_id)
            ->groupBy('academy_cms_beenread.attachment_id')
            ->lists('count', 'attachment_id');
    }
}
