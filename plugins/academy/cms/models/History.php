<?php namespace Academy\Cms\Models;

use Academy\System\Classes\Differ;

/*
    подключение истории:
    public $morphMany = [
        'history' => [
            'Academy\Cms\Models\History',
            'name' => 'attachment',
        ]
    ];

    обновление истории:
    $this->history()->add(new History([
        'values' => $this->attributes,//передаем значения полей, которые нужно сохранить
        'compress' => 1,//передаем шаг с которым сохраняется точное значение, остальные шаги будут хранить сжатую разницу, можно передать массив значение => шаг компресса
        'logfields' => 30//передаем количество секунд которое должно быть промежутком установки значений, если передать массив полей, то будут обновлены только указанные
    ]));
    по сохраненным полям, если у них нет сжатия можно вести поиск, выборку и т д, так как они сохраняются в виде связи и можно применить join

    поля в ямле будут доступны как "_{название поля}"

    //функция restore восстанавливает текущую историю для указанных полей модели, или для всех, если поля не указанны, а также записывает это изменение в историю

    //в ямле листа сортировка по всем дополнительным поям будет работать, если названия полей указывать как "_{fieldName}"
*/

class History extends \Model
{
    use \October\Rain\Database\Traits\Validation;

    protected $table = 'academy_cms_histories';
    protected $guarded = ['*'];
    public $belongsTo = ['user' => 'Backend\Models\User'];
    public $hasMany = ['fields' => 'Academy\Cms\Models\HistoryField'];
    protected $fillable = ['logfields', 'compress', 'values'];

    public $field = [];
    public $rules = [];
    public $logfields;
    public $compress;
    public $values;
    public $saveValues = array();

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        \Event::listen('backend.list.extendQuery', [$this, 'orderByField']);
    }

    public function orderByField($lists, $query)
    {
        if ($lists->model !== $this) {
            return;//отозвалась чужая модель
        }
        $subQuery = $query->getQuery();

        $field = new HistoryField();
        $fieldTable = $field->getTable();
        $historyTable = $this->getTable();

        foreach ($subQuery->orders as $num => $config) {
            if ($config['column'][0] != '_') {
                continue;
            }
            $orderBy = mb_substr($config['column'], 1);
            $subQuery->leftJoin(\DB::raw("`$fieldTable` `$num`"), "$num.history_id", '=', \DB::raw("`$historyTable`.`id` AND `$num`.`name`='$orderBy'"));
            $subQuery->orders[$num]['column'] = "$num.value";
        }
    }

    public function getCompressField($fieldName)
    {
        if (!isset($this->compress)) {
            return 0;
        }
        if (!is_array($this->compress)) {
            return $this->compress;
        }
        if (empty($this->compress[$fieldname])) {
            return 0;
        }
        return $this->compress[$fieldname];
    }

    public function findLastField($fieldName, $byThis = false)
    {
        $field = new HistoryField();
        $fieldTable = $field->getTable();
        $historyTable = $this->getTable();
        $query = HistoryField::join($historyTable, "$historyTable.id", '=', "$fieldTable.history_id")
            ->where("$historyTable.attachment_id", $this->attachment_id)
            ->where("$historyTable.attachment_type", $this->attachment_type)
            ->where("$fieldTable.name", $fieldName);
        if ($byThis) {
            $query->where("$historyTable.created_at", '<=', $this->created_at)
                ->where("$historyTable.id", '<=', $this->id);
        }
        return $query->select(
            "$fieldTable.compressed_step",
            "$fieldTable.value",
            "$fieldTable.name",
            "$fieldTable.history_id",
            "$historyTable.updated_at",
            "$historyTable.user_id"
        )
        ->orderBy("$historyTable.created_at", 'desc')
        ->orderBy("$historyTable.id", 'desc')
        ->first();
    }

    public function restore($fields = [])
    {
        $class = $this->attachment_type;
        $model = $class::find($this->attachment_id);
        if (!count($fields)) {
            $fields = array_keys($model->attributes);
        }
        foreach ($fields as $fieldName) {
            if ($field = $this->findLastField($fieldName, true)) {
                $model->{$fieldName} = $field->value;
            }
        }
        $model->save();
    }

    public function getNextCompressStep($field)
    {
        if (is_null($field)
            || $field->compressed_step + 1 > $this->getCompressField($field->name)
        ) {
            return 0;
        }
        return $field->compressed_step + 1;
    }

    public function beforeCreate()
    {
        $this->user = \BackendAuth::getUser();

        $class = $this->attachment_type;
        $original = $class::withTrashed()->find($this->attachment_id);

        if (!isset($this->logfields)) {
            $this->logfields = array_keys($this->values);
        }
        elseif (is_numeric($this->logfields)) {
            $second = $this->logfields;
            $this->logfields = [];
            foreach ($this->values as $fieldName => $value) {
                $this->logfields[$fieldName] = $second;
            }
        }
        $this->saveValues = [];
        foreach ((array)$this->logfields as $fieldname => $second) {
            if (is_numeric($fieldname)) {
                $fieldname = $second;
                $second = 0;
            }
            if (!array_key_exists($fieldname, $this->values)
                || $this->values[$fieldname] === $original->{$fieldname}
            ) {
                continue;
            }
            $prevFields = $this->findLastField($fieldname);
            //если время изменения меньше чем установленное для логирования, то обновляем значение, иначе создаем

            if (is_null($prevFields)
                || $prevFields->user_id != $this->user_id
                    || time() - strtotime($prevFields->updated_at) >= $second
            ) {
                //тогда создаем новое значение
                if ($this->getCompressField($fieldname) == 0
                    || is_null($this->values[$fieldname])
                        || is_null($original->{$fieldname})
                            || (0 == $newStep = $this->getNextCompressStep($prevFields))
                ) {
                    $this->saveValues[] = new HistoryField(['compressed_step' => 0, 'value' => $this->values[$fieldname], 'name' => $fieldname]);
                    continue;
                }
                $newValue = bzcompress(json_encode(Differ::getDiff($original->{$fieldname}, $this->values[$fieldname])));
                if (mb_strlen($newValue) >= mb_strlen($this->values[$fieldname])) {
                    $this->saveValues[] = new HistoryField(['compressed_step' => 0, 'value' => $this->values[$fieldname], 'name' => $fieldname]);
                }
                else {
                    $this->saveValues[] = new HistoryField(['compressed_step' => $newStep, 'value' => $newValue, 'name' => $fieldname]);
                }
            }
            elseif ($prevFields->compressed_step == 0
                || is_null($this->values[$fieldname])
                    || is_null($prevFields->value)
            ) {
                //сохраняем значение без изменений
                HistoryField::where('name', $prevFields->name)
                    ->where('history_id', $prevFields->history_id)
                    ->update([
                        'value' => $this->values[$fieldname],
                        'compressed_step' => 0
                    ]);
            }
            else {
                //сохраняем разницу с предыдущим значением
                HistoryField::where('name', $prevFields->name)
                    ->where('history_id', $prevFields->history_id)
                    ->update([
                        'value' => bzcompress(json_encode(Differ::getDiff($prevFields->prevValue, $this->values[$fieldname])))
                    ]);
            }
        }
        if (!count($this->saveValues)) {
            return false;
        }
    }

    public function beforeValidate()
    {
        if (isset($this->attributes['values'])) {
            $this->values = $this->attributes['values'];
            unset($this->attributes['values']);
        }
        if (isset($this->attributes['compress'])) {
            $this->compress = $this->attributes['compress'];
            unset($this->attributes['compress']);
        }
        if (isset($this->attributes['logfields'])) {
            $this->logfields = $this->attributes['logfields'];
            unset($this->attributes['logfields']);
        }
    }

    public function afterCreate()
    {
        if (count($this->saveValues)) {
            $this->fields()->addMany($this->saveValues);
        }
    }

    public function __get($name)
    {
        if (!is_null($result = parent::__get($name))) {
            return $result;
        }
        if ($name[0] == '_' && isset($this->field[mb_substr($name, 1)])) {
            return $this->field[mb_substr($name, 1)];
        }
    }

    public function afterFetch()
    {
        foreach ($this->fields as $field) {
            $this->field[$field->name] = $field->value;
        }
    }

    public function beforeUpdate()
    {
        return false;//запрещаем обновлять модель
    }

    public function beforeDelete()
    {
        return false;//запрещаем удалять модель
    }
}