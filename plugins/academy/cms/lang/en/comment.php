<?php return [
    'relation_label' => 'comment',
    'label' => 'Comment',
    'fields' => [
        'description' => 'Comment text',
        'created_at' => 'Date',
    ]
];