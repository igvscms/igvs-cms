<?php return [
    'relation_label' => 'комментария',
    'label' => 'Комментарий',
    'fields' => [
        'description' => 'Текст комментария',
        'created_at' => 'Дата',
    ]
];