$(document).ready(function () {
    $(document).on('click', 'img.fr-dib', function(el) {
        $.popup({ ajax: '/' + window.location.pathname.split('/')[1] + '/academy/tasks/tasks/getImage', extraData: {src: el.target.currentSrc}});
    });
});

function exitFullscreen() {

    if(document.exitFullscreen)
        document.exitFullscreen();

    else if(document.cancelFullScreen)
        document.cancelFullScreen();

    else if(document.mozCancelFullScreen)
        document.mozCancelFullScreen();

    else if(document.webkitCancelFullScreen)
        document.webkitCancelFullScreen();
}

function fullscreenElement() {
    return document.fullscreenElement
        || document.webkitCurrentFullScreenElement
        || document.mozFullScreenElement
        || document.msFullscreenElement;
}

function requestFullScreen(element) {
    if (element.requestFullScreen)
        element.requestFullScreen();

    else if (element.mozRequestFullScreen)
        element.mozRequestFullScreen();

    else if (element.webkitRequestFullScreen)
        element.webkitRequestFullScreen();
}

function launchFullScreen(element) {

    if(fullscreenElement() != element)
        requestFullScreen(element);

    else
        exitFullscreen();
}

if (window == window.top) {
$(document).ready(function() {
    updown = document.createElement('div');
    updown.id = 'updown';

    window.onscroll = function() {
        var pageY = window.pageYOffset || document.documentElement.scrollTop;
        var innerHeight = document.documentElement.clientHeight;

        if (updown.className == 'up') {
            if (pageY < innerHeight) {
                updown.className = '';
            }
        }
        else if (pageY > innerHeight) {
            updown.className = 'up';
        }
    }
    var pageYLabel = 0;

    updown.onclick = function() {
        var pageY = window.pageYOffset || document.documentElement.scrollTop;

        if (this.className == 'up') {
            pageYLabel = pageY;
            window.scrollTo(0, 0);
            this.className = 'down';
        }
        else {
            window.scrollTo(0, pageYLabel);
            this.className = 'up';
        }
    }
    document.body.appendChild(updown);
});
}