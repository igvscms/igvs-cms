<?php
    return [
        'names' => [
            'task_change' => 'Tasks Change',
            'file_delete' => 'Remove Attachments',
            'task_delete' => 'Remove Tasks',
            'show_changes' => 'Notify about task changes',
            'auto_executor' => 'Selected as the default executor',
            'project_change' => 'Project change',
       ],
       'message' => 'You do not have access to this operation',
    ];