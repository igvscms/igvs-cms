<?php
    return [
        'relation_label' => 'Comment',
        'create' => 'Create comment',
        'field' => [
            'user' => 'Author',
            'text' => 'Text',
            'task' => 'Task',
            'created_at' => 'Date of creation',
       ]
    ];