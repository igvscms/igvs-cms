<?php
    return [
        'relation_label' => 'Nonworking time',
        'field' => [
            'from_time' => 'Start time',
            'to_time' => 'Completion time',
        ],
    ];