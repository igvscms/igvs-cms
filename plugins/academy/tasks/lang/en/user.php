<?php return [
    'label' => 'Users',
    'relation_label' => 'User',
    'field' => [
        'responsible' => 'Executor',
        'behavior_priority' => 'Setting priorities',
        'excluded_times' => 'Nonworking time',
        'name' => 'Username',
        'roles' => 'Roles',
        'email' => 'Email',
        'user' => 'User',
    ],
];