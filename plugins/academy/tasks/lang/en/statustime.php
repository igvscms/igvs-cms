<?php
    return [
        'update' => 'Update task execution time',
        'relation_label' => 'task execution time',
        'field' => [
            'status_id' => 'Status',
            'user' => 'User',
            'time' => 'Time passed',
            'second' => '{1,21,31,41,51} second|{2,3,4,22,23,24,32,33,34,42,43,44,52,53,54} seconds|[0,Inf] seconds',
            'minute' => '{1,21,31,41,51} minute|{2,3,4,22,23,24,32,33,34,42,43,44,52,53,54} minutes|[0,Inf] minutes',
            'hour' => '{1,21} hour|{2,3,4,22,23} hours|[0,Inf] hours',
            'day' => '{1,21,31,41,51,61,71,81,91} day|{2,3,4,22,23,24,32,33,34,42,43,44,52,53,54,62,63,64,72,73,74,82,83,84,92,93,94} days|[0,Inf] days',
            'year' => '{1,21,31,41,51,61,71,81,91} year|{2,3,4,22,23,24,32,33,34,42,43,44,52,53,54,62,63,64,72,73,74,82,83,84,92,93,94} years|[0,Inf] years',
            'minutes' => 'minutes',
            'hours' => 'hours',
            'days' => 'days',
       ]
    ];