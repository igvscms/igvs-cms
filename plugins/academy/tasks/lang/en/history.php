<?php
    return [
        'field' => [
            'user' => 'User',
            'updated_at' => 'Date of update',
            'data' => 'Data',
            'data_param' => 'Field name',
            'data_value' => 'Value',
            'data_before' => 'Before',
            'data_after' => 'After',
       ]
    ];