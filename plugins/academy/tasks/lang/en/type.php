<?php
    return [
        'select_label' => 'type',
        'relation_label' => 'task type',
        'field' => [
            'name' => 'Name',
            'related' => 'Related type',
        ],
    ];