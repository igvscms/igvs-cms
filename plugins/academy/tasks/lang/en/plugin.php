<?php return [
    'permission' => [
        'projects' => 'Access to project management',
        'tasks' => 'Access to task management',
        'viewall' => 'Access to view all tasks',
        'changeall' => 'Access to edit all tasks',
        'create_tasks' => 'Access to create tasks in any project',
        'create_project' => 'Access to create projects',
    ]
];