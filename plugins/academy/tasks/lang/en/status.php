<?php
    return [
        'label' => 'Statuses',
        'relation_label' => 'Status',
        'select_label' => 'Status',
        'create' => 'Create status',
        'update' => 'Change status',
        'field' => [
            'name' => 'Name status',
            'action' => 'Action',
            'color' => 'Status color',
            'icon' => 'Logo status',
            'is_finish' => 'Completes the task',
            'is_logged' => 'Log when modified',
       ]
    ];