<?php return [
    'title' => 'Projects',
    'label' => 'Work Planner',
    'name' => 'Project',
    'create' => 'Create project',
    'update' => 'Setting up project',
    'field' => [
        'name' => 'Project name',
        'roles' => 'Roles management',
        'statuses' => 'Status management',
        'users' => 'Project members',
        'status' => 'Statuses',
        'types' => 'Task types',
        'courses' => 'Course Management',
        'groups' => 'Groups',
        'custom_fields' => 'Custom fields',
        'course_count' => 'Product Count',
   ]
];