<?php
    return [
        'label' => 'Roles',
        'relation_label' => 'Roles',
        'select_label' => 'Role',
        'create' => 'Create project',
        'field' => [
            'name' => 'Role',
            'statuses' => 'Status management',
            'buttons' => 'Actions',
            'users' => 'Users management',
            'rights' => 'Rights',
       ],
       'update' => 'Settings',
    ];