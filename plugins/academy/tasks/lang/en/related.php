<?php
    return [
        'relation_label' => 'Related',
        'field' => [
            'related' => 'Related task',
            'type' => 'Connection type'
        ],
    ];