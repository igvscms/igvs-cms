<?php
    return [
        'relation_label' => 'Priority',
        'field' => [
            'behavior' => 'Behavior',
            'sort' => 'Priority',
        ],
    ];