<?php
    return [
        'relation_label' => 'комментария',
        'create' => 'Создание комментария',
        'field' => [
            'user' => 'Автор',
            'text' => 'текст',
            'task' => 'Задача',
            'created_at' => 'Дата создания',
       ]
    ];