<?php
    return [
        'label' => 'Роли',
        'relation_label' => 'роли',
        'select_label' => 'роль',
        'create' => 'Создание роли',
        'field' => [
            'name' => 'Роль',
            'statuses' => 'Управление статусами',
            'buttons' => 'Действия',
            'users' => 'Управление пользователями',
            'rights' => 'Права'
       ],
       'update' => 'Настройки',
    ];