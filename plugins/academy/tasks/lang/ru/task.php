<?php $lang = [
    'label' => 'Задачи',
    'relation_label' => 'дочерней задачи',
    'print' => 'Экспорт',
    'pdf' => 'Отчет в формате PDF',
    'html' => 'Отчет в формате HTML',
    'doc' => 'Отчет в формате DOC',
    'xml'=> 'Отчет в формате EXCEL',
    'config' => 'Параметры настроек',
    'field' => [
        'id' => 'Задача',
        'responsible' => 'Исполнитель',
        'priority' => 'Приоритет',
        'status' => 'Статус',
        'parent' => 'Родительская задача',
        'user' => 'Автор',
        'course' => 'Продукт',
        'topic' => 'Тема продукта',
        'act' => 'Акт',
        'course_code' => 'Код курса',
        'topic_code' => 'Код топика',
        'act_code' => 'Код I-ACT',
        'module_code' => 'Код модуля',
        'deadline' => 'Дата завершения задачи',
        'childs' => 'Дочерние задачи',
        'comment' => 'Комментарии',
        'name' => 'Тема',
        'created_at' => 'Дата создания',
        'updated_at' => 'Дата Изменения',
        'description' => 'Описание',
        'set_course' => 'Выбрать курс',
        'set_topic' => 'Выбрать топик',
        'set_act' => 'Выбрать I-ACT',
        'set_module' => 'Выбрать Модуль',
        'module' => 'Модуль',
        'progress' => 'Готовность',
        'related' => 'Связанные задачи',
        'beginning' => 'Начало выполнения',
        'type' => 'Тип',
        'course_section' => 'Информацию по продукту',
        'details_section' => 'Подробности',
        'attachments_section' => 'Прикрепленные файлы',
        'time_section' => 'Учет времени',
        'group' => 'Группа',
        'projectcustomfields_section' => 'Дополнительные поля',
    ],
    'tab' => [
        'comment' => 'Комментарии',
        'history' => 'История изменений',
        'times' => 'Время статусов',
    ],
    'filter' => [
        'completed' => 'Завершенные',
        'personal' => 'Личные',
        'deleted' => 'Удаленные',
    ],
    'update' => 'Изменение задачи',
    'create' => 'Создание задачи',
    'message' => [
        'new_task' => 'Новая задача',
        'delete' => 'Вы действительно хотите удалить эту задачу?'
    ],
    'formlist' => [
        'status_id' => 'Изменение статуса',
    ],
    'groups' => [
        'actual_task' => 'Актуальные задачи'
    ],
    'priority' => [
        'critical' => 'Критичный',
        'high' => 'Высокий',
        'medium' => 'Средний',
        'low' => 'Низкий',
    ],
];

if (($user = BackendAuth::getUser())
    && $user->hasPermission('igvs.courses.bag_method')
) {
    $lang['field']['topic_code'] = 'Код раздела';
    $lang['field']['set_topic'] = 'Выбрать раздел';
}

return $lang;