<?php
    return [
        'relation_label' => 'связи',
        'select_label' => 'связанную задачу',
        'field' => [
            'related' => 'Связанная задача',
            'type' => 'Тип связи'
        ],
    ];