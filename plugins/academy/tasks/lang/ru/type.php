<?php
    return [
        'select_label' => 'тип',
        'relation_label' => 'типа задачи',
        'field' => [
            'name' => 'Название',
            'related' => 'Связанный тип',
        ],
    ];