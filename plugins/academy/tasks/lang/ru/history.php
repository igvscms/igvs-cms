<?php
    return [
        'field' => [
            'user' => 'Пользователь',
            'updated_at' => 'Дата изменения',
            'data' => 'Изменения',
            'data_param' => 'Название поля',
            'data_value' => 'Значение',
            'data_before' => 'до',
            'data_after' => 'после',
       ]
    ];