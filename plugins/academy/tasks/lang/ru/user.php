<?php return [
    'label' => 'Участники',
    'relation_label' => 'участника',
    'field' => [
        'responsible' => 'Исполнитель',
        'behavior_priority' => 'Настройка приоритетов',
        'excluded_times' => 'Нерабочее время',
        'name' => 'Имя пользователя',
        'roles' => 'Занимаемые роли',
        'email' => 'Почтовый адрес',
        'user' => 'Пользователь',
    ],
];