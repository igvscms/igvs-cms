<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Statuses_1_0_3 extends Migration
{
    public function up()
    {
        Schema::table('academy_tasks_statuses', function($table)
        {
            $table->tinyInteger('progress')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('academy_tasks_statuses', function($table)
        {
            $table->dropColumn('progress');
        });
    }
}