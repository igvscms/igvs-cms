<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class History_1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_history', function($table)
        {
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('task_id')->unsigned();
            $table->binary('data');
            $table->timestamps();

            $table->foreign('user_id','f_userId_academyTasksHistory')
                ->references('id')
                ->on('backend_users')
                ->onDelete('set null');

            $table->foreign('task_id','f_taskId_academyTasksHistory')
                ->references('id')
                ->on('academy_tasks')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_history');
    }
}