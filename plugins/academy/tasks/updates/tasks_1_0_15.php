<?php namespace Academy\Tasks\Updates;

use October\Rain\Database\Updates\Migration;

class Tasks_1_0_15 extends Migration
{
    public function up()
    {
        \Schema::table('academy_tasks', function($table)
        {
            $table->dropForeign('f_groupId_academyTasks');
            $table->dropColumn('group_id');
        });
    }

    public function down()
    {
        \Schema::table('academy_tasks', function($table)
        {
            $table->dropForeign('f_groupId_academyTasks');
            $table->dropColumn('group_id');
        });
    }
}