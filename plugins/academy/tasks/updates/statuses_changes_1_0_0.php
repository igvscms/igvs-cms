<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class StatusesChanges_1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_statuses_changes', function($table)
        {
            $table->integer('role_id')->unsigned();
            $table->integer('status_id')->unsigned()->nullable();//null - при создании задачи
            $table->integer('status_new_id')->unsigned();

            $table->foreign('role_id','f_roleId_academyTasksStatusesChanges')
                ->references('id')
                ->on('academy_tasks_roles')
                ->onDelete('cascade');

            $table->foreign('status_id','f_statusId_academyTasksStatusesChanges')
                ->references('id')
                ->on('academy_tasks_statuses')
                ->onDelete('cascade');

            $table->foreign('status_new_id','f_statusNewId_academyTasksStatusesChanges')
                ->references('id')
                ->on('academy_tasks_statuses')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_statuses_changes');
    }
}