<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class StatusesChanges_1_0_3 extends Migration
{
    public function up()
    {
        Schema::table('academy_tasks_statuses_changes', function($table)
        {
            $table->integer('type_id')->unsigned();

            $table->foreign('type_id','f_typeId_academyTasksStatusesChanges')
                ->references('id')
                ->on('academy_tasks_types')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('academy_tasks_statuses_changes', function($table)
        {
            $table->dropForeign('f_typeId_academyTasksStatusesChanges');
            $table->dropColumn('type_id')->unsigned();
        });
    }
}