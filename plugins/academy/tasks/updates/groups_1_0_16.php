<?php namespace Academy\Tasks\Updates;

use October\Rain\Database\Updates\Migration;

class Groups_1_0_16 extends Migration
{
    public function up()
    {
        \Schema::table('academy_tasks_groups', function($table)
        {
            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id','f_parentId_academyTasksGroups')
                ->references('id')
                ->on('academy_tasks_groups')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        \Schema::table('academy_tasks_groups', function($table)
        {
            $table->dropForeign('f_parentId_academyTasksGroups');
            $table->dropColumn('parent_id');
        });
    }
}