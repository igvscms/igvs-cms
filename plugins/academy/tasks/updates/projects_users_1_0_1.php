<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ProjectsUsers_1_0_1 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_projects_users', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('project_id')->unsigned();

            $table->foreign('user_id','f_userId_academyTasksProjectsUsers')
                ->references('id')
                ->on('backend_users')
                ->onDelete('cascade');

            $table->foreign('project_id','f_projectId_academyTasksProjectsUsers')
                ->references('id')
                ->on('academy_tasks_projects')
                ->onDelete('cascade');

            $table->unique(['user_id','project_id'],'u_u_p_academyTasksProjectsUsers');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_projects_users');
    }
}