<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;
use DB;

use Academy\Tasks\Models\Type;

class Seed_1_0_6 extends Seeder
{
    public function run()
    {
        Type::orderBy('id')->update(['sort_order' => DB::raw('`id`')]);
    }
}