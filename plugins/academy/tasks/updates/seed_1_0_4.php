<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;
use DB;

use Academy\Tasks\Models\Right;

class Seed_1_0_4 extends Seeder
{
    public function run()
    {
        Right::insert([
            ['code' => 'task_delete'],
            ['code' => 'show_changes'],
        ]);
    }
}