<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Connections_1_0_3 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_connections', function($table)
        {
            $table->increments('id');
            $table->string('code');
            $table->integer('related_id')->unsigned()->nullable();

            $table->foreign('related_id','f_relatedId_academyTasksConnections')
                ->references('id')
                ->on('academy_tasks_connections')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_connections');
    }
}