<?php namespace Academy\Tasks\Updates;

use October\Rain\Database\Updates\Migration;

class Tasks_1_0_14 extends Migration
{
    public function up()
    {
        \Schema::table('academy_tasks', function($table)
        {
            $table->integer('group_id')
                ->unsigned()
                ->nullable();
            $table->foreign('group_id','f_groupId_academyTasks')
                ->references('id')
                ->on('academy_tasks_groups');
        });
    }

    public function down()
    {
        \Schema::table('academy_tasks', function($table)
        {
            $table->dropForeign('f_groupId_academyTasks');
            $table->dropColumn('group_id');
        });
    }
}