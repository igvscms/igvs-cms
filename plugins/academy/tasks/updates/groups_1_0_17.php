<?php namespace Academy\Tasks\Updates;

use October\Rain\Database\Updates\Migration;

class Groups_1_0_17 extends Migration
{
    public function up()
    {
        \Schema::table('academy_tasks_groups', function($table)
        {
            $table->dropUnique('academy_tasks_groups_name_unique');
            $table->unique(['name', 'project_id'],'u_pname_projectId_academyTasksGroups');
        });
    }

    public function down()
    {
        \Schema::table('academy_tasks_groups', function($table)
        {
            $table->dropUnique('u_pname_projectId_academyTasksGroups');
        });
    }
}