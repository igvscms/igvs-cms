<?php namespace Academy\Tasks\Updates;

use October\Rain\Database\Updates\Migration;

class Сomments_1_0_9 extends Migration {
    public function up() {
        \Schema::table('academy_tasks_comments', function($table) {
            $table->integer('status_id')->unsigned()->nullable();
            $table->foreign('status_id','f_statusId_academyTasksComments')
                ->references('id')
                ->on('academy_tasks_statuses')
                ->onDelete('set null');
        });
    }
    public function down() {
        \Schema::table('academy_tasks_comments', function($table) {
            $table->dropColumn('status_id');
        });
    }
}