<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Types_1_0_6 extends Migration
{
    public function up()
    {
        Schema::table('academy_tasks_types', function($table)
        {
            $table->integer('sort_order')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('academy_tasks_types', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}