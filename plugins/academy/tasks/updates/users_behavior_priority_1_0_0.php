<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UsersBehaviorPriority_1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_users_behavior_priority', function($table)
        {
            $table->integer('user_id')->unsigned();
            $table->integer('behavior')->unsigned();
            $table->integer('sort')->unsigned();

            $table->foreign('user_id','f_userId_academyTasksUsersBehaviorPriority')
                ->references('id')
                ->on('backend_users')
                ->onDelete('cascade');

            $table->primary(['user_id','behavior'],'p_u_b_academyTasksUsersBehaviorPriority');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_users_behavior_priority');
    }
}