<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class HistoryReads_1_0_4 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_history_reads', function($table)
        {
            $table->integer('user_id')->unsigned();
            $table->integer('task_id')->unsigned();
            $table->timestamp('updated_at');

            $table->foreign('user_id','f_userId_academyTasksHistoryReads')
                ->references('id')
                ->on('backend_users')
                ->onDelete('cascade');

            $table->foreign('task_id','f_taskId_academyTasksHistoryReads')
                ->references('id')
                ->on('academy_tasks')
                ->onDelete('cascade');

            $table->primary(['user_id','task_id'],'p_userId_taskId_academyTasksHistoryReads');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_history_reads');
    }
}