<?php namespace Academy\Tasks\Updates;

use October\Rain\Database\Updates\Migration;

class Groups_tasks_1_0_15 extends Migration
{
    public function up()
    {
        \Schema::create('academy_tasks_groups_tasks', function($table)
        {
            $table->integer('task_id')->unsigned();
            $table->integer('group_id')->unsigned();

            $table->foreign('task_id','f_taskId_academyTasksGroupsTasks')
                ->references('id')
                ->on('academy_tasks')
                ->onDelete('cascade');

            $table->foreign('group_id','f_groupId_academyTasksGroupsTasks')
                ->references('id')
                ->on('academy_tasks_groups')
                ->onDelete('cascade');

            $table->primary(['task_id','group_id']);
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_tasks_groups_tasks');
    }
}