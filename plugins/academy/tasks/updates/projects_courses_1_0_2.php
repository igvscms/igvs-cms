<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ProjectsCourses_1_0_2 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_projects_courses', function($table)
        {
            $table->integer('course_id')->unsigned();
            $table->integer('project_id')->unsigned();

            $table->foreign('course_id','f_courseId_academyTasksProjectsCourses')
                ->references('id')
                ->on('igvs_courses_courses')
                ->onDelete('cascade');

            $table->foreign('project_id','f_projectId_academyTasksProjectsCourses')
                ->references('id')
                ->on('academy_tasks_projects')
                ->onDelete('cascade');

            $table->primary(['course_id','project_id'],'p_c_p_academyTasksProjectsCourses');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_projects_courses');
    }
}