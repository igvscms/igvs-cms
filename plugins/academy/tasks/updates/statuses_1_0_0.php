<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Statuses_1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_statuses', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('color',16);
            $table->string('icon',32);
            $table->integer('project_id')->unsigned();

            $table->foreign('project_id','f_projectId_academyTasksStatuses')
                ->references('id')
                ->on('academy_tasks_projects')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_statuses');
    }
}