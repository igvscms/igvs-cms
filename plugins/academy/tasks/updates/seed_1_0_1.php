<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;
use DB;

use Academy\Tasks\Models\Right;

class Seed_1_0_1 extends Seeder
{
    public function run()
    {
        Right::insert([
            ['code' => 'task_change'],
            ['code' => 'file_delete'],
        ]);
    }
}