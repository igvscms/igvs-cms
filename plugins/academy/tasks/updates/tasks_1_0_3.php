<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Tasks_1_0_3 extends Migration
{
    public function up()
    {
        Schema::table('academy_tasks', function($table)
        {
            $table->timestamp('beginning');
            $table->tinyInteger('progress')->unsigned();
            $table->integer('type_id')->unsigned();

            $table->foreign('type_id','f_typeId_academyTasks')
                ->references('id')
                ->on('academy_tasks_types');
        });
    }

    public function down()
    {
        Schema::table('academy_tasks', function($table)
        {
            $table->dropColumn('beginning');
            $table->dropColumn('progress');
            $table->dropForeign('f_typeId_academyTasks');
            $table->dropColumn('type_id');
        });
    }
}