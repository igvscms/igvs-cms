<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Tasks_1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks', function($table)
        {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();

            $table->string('name');
            $table->text('description');

            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('responsible_id')->unsigned()->nullable();
            
            $table->integer('status_id')->unsigned()->nullable();
            $table->boolean('priority');//целое от 0 до 9
            $table->integer('steps')->unsigned();
            $table->boolean('behavior')->unsigned();//0-3

            $table->integer('module_id')->unsigned()->nullable();
            $table->string('module_code');

            $table->integer('course_id')->unsigned()->nullable();
            $table->string('course_code');

            $table->integer('topic_id')->unsigned()->nullable();
            $table->string('topic_code');

            $table->integer('act_id')->unsigned()->nullable();
            $table->string('act_code');

            $table->timestamps();//дата создания и дата последнего изменения статуса
            $table->softDeletes();
            $table->timestamp('deadline')->nullable();//крайний срок

            $table->integer('project_id')->unsigned();

            $table->foreign('project_id','f_projectId_academyTasks')
                ->references('id')
                ->on('academy_tasks_projects')
                ->onDelete('cascade');

            $table->foreign('parent_id','f_parentId_academyTasks')
                ->references('id')
                ->on('academy_tasks')
                ->onDelete('set null');

            $table->foreign('user_id','f_userId_academyTasks')
                ->references('id')
                ->on('backend_users')
                ->onDelete('set null');

            $table->foreign('responsible_id','f_responsibleId_academyTasks')
                ->references('id')
                ->on('backend_users')
                ->onDelete('set null');

            $table->foreign('module_id','f_moduleId_academyTasks')
                ->references('id')
                ->on('igvs_courses_modules_contents')
                ->onDelete('set null');

            $table->foreign('course_id','f_courseId_academyTasks')
                ->references('id')
                ->on('igvs_courses_courses')
                ->onDelete('set null');

            $table->foreign('topic_id','f_topicId_academyTasks')
                ->references('id')
                ->on('igvs_courses_categories')
                ->onDelete('set null');

            $table->foreign('act_id','f_actId_academyTasks')
                ->references('id')
                ->on('igvs_courses_categories')
                ->onDelete('set null');

            $table->foreign('status_id','f_statusId_academyTasks')
                ->references('id')
                ->on('academy_tasks_statuses')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks');
    }
}