<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class StatusesTimes_1_0_13 extends Migration
{
    public function up()
    {
        Schema::table('academy_tasks_statuses_times', function($table)
        {
            $table->index('user_id','f_userId_academyTasksStatusesTimes');
            $table->dropUnique('p_u_t_s_Id_academyTasksStatusesTimes');
            $table->timestamp('updated_at');
        });
    }

    public function down()
    {
        Schema::table('academy_tasks_statuses_times', function($table)
        {
            $table->dropColumn('updated_at');
            $table->unique(['user_id', 'task_id', 'status_id'], 'p_u_t_s_Id_academyTasksStatusesTimes');
            $table->dropIndex('f_userId_academyTasksStatusesTimes');
        });
    }
}