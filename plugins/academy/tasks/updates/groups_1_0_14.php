<?php namespace Academy\Tasks\Updates;

use October\Rain\Database\Updates\Migration;

class Groups_1_0_14 extends Migration
{
    public function up()
    {
        \Schema::create('academy_tasks_groups', function($table)
        {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('project_id')->unsigned();

            $table->foreign('project_id','f_projectId_academyTasksGroups')
                ->references('id')
                ->on('academy_tasks_projects')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        \Schema::dropIfExists('academy_tasks_groups');
    }
}