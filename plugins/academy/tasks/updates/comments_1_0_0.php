<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Comments_1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_comments', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('task_id')->unsigned();
            $table->text('text');
            $table->timestamps();

            $table->foreign('user_id','f_userId_academyTasksComments')
                ->references('id')
                ->on('backend_users')
                ->onDelete('cascade');

            $table->foreign('task_id','f_taskId_academyTasksComments')
                ->references('id')
                ->on('academy_tasks')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_comments');
    }
}