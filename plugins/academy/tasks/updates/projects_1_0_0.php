<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Projects_1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_projects', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('name');
            $table->timestamps();

            $table->foreign('user_id','f_userId_academyTasksProjects')
                ->references('id')
                ->on('backend_users')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_projects');
    }
}