<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;
use DB;

use Academy\Tasks\Models\Connection;

class Seed_1_0_3 extends Seeder
{
    public function run()
    {
        $connections = [
            'duplicated' => 'duplicates',
            'blocked' => 'blocks',
            'follows' => 'precedes',
            'copied_from' => 'copied_to',
        ];

        foreach($connections as $type => $type2) {

            $connections[$type] = new Connection();
            $connections[$type]->code = $type;
            $connections[$type]->save();

            $connections[$type2] = new Connection();
            $connections[$type2]->code = $type2;
            $connections[$type2]->related_id = $connections[$type]->id;
            $connections[$type2]->save();

            $connections[$type]->related_id = $connections[$type2]->id;
            $connections[$type]->save();
        }
    }
}