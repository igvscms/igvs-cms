<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UsersRoles_1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_users_roles', function($table)
        {
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('role_id','f_roleId_academyTasksUsersRoles')
                ->references('id')
                ->on('academy_tasks_roles')
                ->onDelete('cascade');

            $table->foreign('user_id','f_userId_academyTasksUsersRoles')
                ->references('id')
                ->on('backend_users')
                ->onDelete('cascade');

            $table->primary(['user_id','role_id'],'p_u_r_academyTasksUsersRoles');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_users_roles');
    }
}