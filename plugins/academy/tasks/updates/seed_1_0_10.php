<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Academy\Tasks\Models\Right;

class Seed_1_0_10 extends Seeder
{
    public function run()
    {
        Right::insert([
            ['code' => 'auto_executor'],
        ]);
    }
}