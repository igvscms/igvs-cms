<?php namespace Academy\Tasks\Updates;

class Statuses_1_0_8 extends \October\Rain\Database\Updates\Migration {
    public function up() {
        \Schema::table('academy_tasks_statuses', function($table) {
            $table->string('action');
        });
    }
    public function down() {
        \Schema::table('academy_tasks_statuses', function($table) {
            $table->dropColumn('action');
        });
    }
}