<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;

use Academy\Tasks\Models\Comment;

class Seed_1_0_11 extends Seeder
{
    public function run()
    {
        Comment::where('text', '')->delete();
    }
}