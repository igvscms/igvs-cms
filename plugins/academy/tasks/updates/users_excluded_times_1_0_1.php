<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UsersExcludedTimes_1_0_1 extends Migration
{
    public function up()
    {
        Schema::dropIfExists('academy_tasks_users_excluded_times');

        Schema::create('academy_tasks_users_excluded_times', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->timestamp('from_time');
            $table->timestamp('to_time')->nullable();

            $table->foreign('user_id','f_userId_academyTasksUsersExcludedTimes')
                ->references('id')
                ->on('academy_tasks_projects_users')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_users_excluded_times');

        Schema::create('academy_tasks_users_excluded_times', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->timestamp('from_time');
            $table->timestamp('to_time')->nullable();

            $table->foreign('user_id','f_userId_academyTasksUsersExcludedTimes')
                ->references('id')
                ->on('backend_users')
                ->onDelete('cascade');
        });
    }
}