<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Statuses_1_0_7 extends Migration
{
    public function up()
    {
        Schema::table('academy_tasks_statuses', function($table)
        {
            $table->boolean('is_logged');
        });
    }

    public function down()
    {
        Schema::table('academy_tasks_statuses', function($table)
        {
            $table->dropColumn('is_logged');
        });
    }
}