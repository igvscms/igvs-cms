<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class StatusesTimes_1_0_0 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_statuses_times', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('task_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->integer('time')->unsigned();

            $table->foreign('user_id','f_userId_academyTasksStatusesTimes')
                ->references('id')
                ->on('backend_users')
                ->onDelete('cascade');

            $table->foreign('task_id','f_taskId_academyTasksStatusesTimes')
                ->references('id')
                ->on('academy_tasks')
                ->onDelete('cascade');

            $table->foreign('status_id','f_statusId_academyTasksStatusesTimes')
                ->references('id')
                ->on('academy_tasks_statuses')
                ->onDelete('cascade');

            $table->unique(['user_id', 'task_id', 'status_id'], 'p_u_t_s_Id_academyTasksStatusesTimes');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_statuses_times');
    }
}