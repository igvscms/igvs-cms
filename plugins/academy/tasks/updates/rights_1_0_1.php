<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Rights_1_0_1 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_rights', function($table)
        {
            $table->increments('id');
            $table->string('code');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_rights');
    }
}