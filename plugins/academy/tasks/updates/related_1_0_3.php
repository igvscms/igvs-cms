<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Related_1_0_3 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_related', function($table)
        {
            $table->increments('id');
            $table->integer('task_id')->unsigned();
            $table->integer('connection_id')->unsigned();
            $table->integer('related_id')->unsigned()->nullable();

            $table->foreign('related_id','f_relatedId_academyTasksRelated')
                ->references('id')
                ->on('academy_tasks_related')
                ->onDelete('cascade');

            $table->foreign('connection_id','f_typeId_academyTasksRelated')
                ->references('id')
                ->on('academy_tasks_connections')
                ->onDelete('cascade');

            $table->foreign('task_id','f_taskId_academyTasksRelated')
                ->references('id')
                ->on('academy_tasks')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_related');
    }
}