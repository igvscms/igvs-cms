<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class RoleRights_1_0_1 extends Migration
{
    public function up()
    {
        Schema::create('academy_tasks_role_rights', function($table)
        {
            $table->integer('role_id')->unsigned();
            $table->integer('right_id')->unsigned();

            $table->foreign('role_id','f_roleId_academyTasksRoleRights')
                ->references('id')
                ->on('academy_tasks_roles')
                ->onDelete('cascade');

            $table->foreign('right_id','f_rightId_academyTasksRoleRights')
                ->references('id')
                ->on('academy_tasks_rights')
                ->onDelete('cascade');

            $table->primary(['role_id','right_id'],'p_r_r_academyTasksRoleRights');
        });
    }

    public function down()
    {
        Schema::dropIfExists('academy_tasks_role_rights');
    }
}