<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Roles_1_0_2 extends Migration
{
    public function up()
    {
        Schema::table('academy_tasks_roles', function($table)
        {
            $table->integer('sort_order')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('academy_tasks_roles', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}