<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class StatusesTimes_1_0_5 extends Migration
{
    public function up()
    {
        Schema::table('academy_tasks_statuses_times', function($table)
        {
            $table->integer('time')->change();
        });
    }

    public function down()
    {
        Schema::table('academy_tasks_statuses_times', function($table)
        {
            $table->integer('time')->unsigned()->change();
        });
    }
}