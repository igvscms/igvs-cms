<?php
    return [
        'sitePath' => dirname(__FILE__) . '/../../../..',
        'resourcesPath' => dirname(__FILE__) . '/../resources',
        'modulesContent' => [
            'path' => dirname(__FILE__) . '/../../../../storage/app/igvs/module-content',
            'baseUrl' => '/storage/app/igvs/module-content'
        ],
        'modulesRelease' => [
            'path' => dirname(__FILE__) . '/../resources/modules',
            'baseUrl' => '/plugins/igvs/courses/resources/modules'
        ],
        'scorm' => [
            'scormfiles' => dirname(__FILE__) . '/../resources/scormfiles',
            'tmp' => dirname(__FILE__) . '/../../../../storage/app/igvs/scorm/tmp',
            'arhive' => dirname(__FILE__) . '/../../../../storage/app/igvs/scorm/arhive',
            'url' => '/storage/app/igvs/scorm/arhive'
        ],
        'prepareForFtp' => [
            'basePath' => dirname(__FILE__) . '/../../../../storage/app/igvs/export-igvs',
            'modulesDir' => '__modules',
        ],
        'prepareForFtpBeta' => [
            'basePath' => dirname(__FILE__) . '/../../../../storage/app/igvs/export-beta',
            'betaPath' => dirname(__FILE__) . '/../../../../../beta.i-gvs.com/www/b/content',
            'betaUrl' => 'http://beta.i-gvs.com/b/',
        ],
        'prepareReleaseCloud' => [
            'releaseDir' => '/mnt/release_win',
            'tmp' => dirname(__FILE__) . '/../../../../storage/app/igvs/release-cloud-tmp',
            'displayReleaseDir' => '\\\\004-1\gvs\release\test',
            'betaPath' => dirname(__FILE__) . '/../../../../../beta.local/b/content',
            'betaUrl' => 'http://beta.local/b/',
        ],
    ];