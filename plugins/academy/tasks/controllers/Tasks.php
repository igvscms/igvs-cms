<?php namespace Academy\Tasks\Controllers;

use Academy\Tasks\Models\CommentRead;
use Academy\Tasks\Models\HistoryRead;
use Academy\Tasks\Models\Project;
use Academy\Tasks\Models\Right;
use Academy\Tasks\Models\Status;
use Academy\Tasks\Models\StatusChange;
use Academy\Tasks\Models\Task;
use Academy\Tasks\Models\User;
use Academy\Tasks\Models\Group;

use Academy\System\Behaviors\Notifier;

use BackendMenu;
use BackendAuth;

use Backend\Behaviors\RelationController;
use Backend\Classes\Controller;
use Backend\Classes\BackendController;

use DB;

use Igvs\Courses\Controllers\Contents;

use Igvs\Courses\Models\ModuleContent;

class Tasks extends Controller
{
    use \Academy\System\Traits\SocketSender;

    protected $publicActions = ['getImage'];

    //условия мнгновенной отправки, определяем кому отправим
    public static function rulesForSendingSocketsMessages() { return [
        'newTask' => function($data, $user, $is_current_cocket, $default)
        {
            $task = Task::find($data['params']['task_id']);
            return $task && in_array($user['user_id'], [$task->responsible_id, $task->user_id]) && $task->progress < 100;
        },
        'onUpdateRubricator' => Contents::rulesForSendingSocketsMessages()['onUpdateRubricator'],
    ];}

    //преобразование результата
    public static function socketMessageResultHandler() { return [
        'newTask' => function($data, $result)
        {
            $notifier = new Notifier();
            return ['original' => $notifier->onGetNotificationCount(get_called_class())];
        },
        'onUpdateRubricator' => Contents::socketMessageResultHandler()['onUpdateRubricator'],
    ];}

    public $implement = [
        'form' => 'Academy.Cms.Behaviors.FormController',
        'list' => 'Academy.System.Behaviors.ListController',
        //'Backend.Behaviors.ListController',
        'Academy.System.Behaviors.Notifier',
        'Backend.Behaviors.RelationController',
        'Academy.System.Behaviors.CrudController',
    ];

    public $formConfig = 'config_form.yaml';
    public static $notifierConfig = [
        'actual' => 'config_notifier.yaml',
        'created' => 'config_notifier_created.yaml'
    ];
    public $listConfig = [
        'pdf' => 'config_list_pdf.yaml',
        'list' => 'config_list.yaml',
    ];
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = ['academy.tasks.tasks'];

    public $project_id = null;
    public $project = null;

    public function __construct()
    {
        if (in_array($this->getAjaxHandler(), ['pdf::onApplySetup', 'pdf::onLoadSetup'])) {
            $this->implement['list'] = 'Academy.Cms.Behaviors.EditListController';
        }
        if (BackendController::$action === 'statuschange') {
            $this->implement['form'] = 'Backend.Behaviors.FormController';
        }

        parent::__construct();
        BackendMenu::setContext('Academy.Tasks', 'tasks', 'projects');
        if(($this->action == 'update') && ($user = BackendAuth::getUser())) {

            $find = ['user_id' => $user->id, 'task_id' =>$this->params[0]];
            $set = ['updated_at' => time()];
            CommentRead::where($find)->delete();
            CommentRead::create($find + $set);

            HistoryRead::where($find)->delete();
            HistoryRead::create($find + $set);
        }
        if(isset($this->params[0]) && !in_array($this->action, ['filter','filter_clear'])) {
            $this->project_id = $this->params[0];
            $this->project = Project::where('id', $this->project_id)->first();
        }
        if (get('clear')) {
            $this->layout = 'plugins/academy/system/layouts/clear';
        }
        if ($this->action == 'crudupdate') {
            $this->asExtension('CrudController')->runButton = '';
            $this->pageTitle = \Lang::get('academy.tasks::task.update',[],\Session::get('locale')) . ' #' . $this->params[0];
        }
        if (in_array($this->action, ['crudupdate', 'update'])) {
            $this->addCss('/plugins/academy/tasks/assets/css/jira.css');
        }
        $this->addJs('/plugins/academy/tasks/assets/js/list_select_row.js');
    }

    function getImage()
    {
        die($this->makePartial('get_image', ['src' => get('src')]));
    }

    function sendNotifierMessage($handler, $task_id)
    {
        $options['type'] = 'newNotifierMessage';
        $options['result'] = 1;
        $options['params'] = ['task_id' => $task_id];
        $this->socketNewSend($handler, $options);
    }

    function newTaskNotifier($task_id = null) {
        $this->sendNotifierMessage('newTask', $task_id ?: $this->formGetModel()->id);
    }

    function onUpdateRubricator($task_id = null) {
        if (is_null($task_id)) {
            $model = $this->formGetModel();
        }
        else {
            $model = Task::find($task_id);
        }
        if ($model && $model->course_id) {
            $options = [
                'result' => 1,
                'params' => ['id' => $model->course_id]
            ];
            $this->socketNewSend('onUpdateRubricator', $options);
        }
    }

    public function sub($recordId)
    {
        return $this->asExtension('FormController')->update($recordId,'sub');
    }

    public function comments($recordId)
    {
        $this->pageTitle = trans('academy.tasks::task.field.comment');
        return $this->asExtension('CrudController')->crudupdate($recordId,'comments');
    }

    public function comments_onSave($recordId)
    {
        return $this->asExtension('CrudController')->crudupdate_onSave($recordId, 'comments');
    }

    public function update_onSave($recordId, $context = null)
    {
        $result = parent::update_onSave($recordId, $context);
        $this->newTaskNotifier();
        $this->onUpdateRubricator();
        return $result;
    }

    public function preview_onSave($recordId)
    {
        return $this->update_onSave($recordId);
    }

    public function related($recordId)
    {
        $this->pageTitle = trans('academy.tasks::task.field.related');
        return $this->asExtension('CrudController')->crudupdate($recordId,'related');
    }

    public function related_onSave($recordId)
    {
        return $this->asExtension('CrudController')->crudupdate_onSave($recordId, 'related');
    }

    public function childs($recordId)
    {
        $this->pageTitle = trans('academy.tasks::task.field.childs');
        return $this->asExtension('CrudController')->crudupdate($recordId,'childs');
    }

    public function childs_onSave($recordId)
    {
        return $this->asExtension('CrudController')->crudupdate_onSave($recordId, 'childs');
    }

    public function history($recordId)
    {
        $this->pageTitle = trans('academy.tasks::task.tab.history');
        return $this->asExtension('CrudController')->crudupdate($recordId,'history');
    }

    public function history_onSave($recordId)
    {
        return $this->asExtension('CrudController')->crudupdate_onSave($recordId, 'history');
    }

    public function filter($taskId = null)
    {
        $this->makeLists();
        return $this->makePartial('filter');
    }

    public function clear($recordId)
    {
        $this->layout = 'plugins/academy/system/layouts/clear';
        parent::index($recordId);
        return $this->makeFileContents($this->getViewPath('index.htm'));
    }

    public function create_clear()
    {
        $this->layout = 'plugins/academy/system/layouts/clear';
        $this->asExtension('FormController')->create('create');
        return $this->makePartial('create_clear');
    }
    public function create_clear_onSave()
    {
        $result = $this->asExtension('FormController')->create_onSave('create');
        return $result;
    }

    public function filter_clear($taskId = null)
    {
        $this->layout = 'plugins/academy/system/layouts/clear';
        $this->makeLists();
        return $this->makePartial('filter');
    }

    public function initRelation($model, $field = null)
    {
        if ($field == null) {
            $field = post(RelationController::PARAM_FIELD);
        }
        $return = parent::initRelation($model, $field);

        if ($this->relationModel && get_class($this->relationModel) == 'Academy\Tasks\Models\Task') {
            $this->relationModel->project_id = $model->project_id;
        }
        if ($this->relationModel && get_class($this->relationModel) == 'Academy\Tasks\Models\Related') {
            $this->relationModel->task_id = $model->id;
        }
        if ($this->relationModel && get_class($this->relationModel) == 'Academy\Tasks\Models\Comment') {
            $rc = $this->extensionData['extensions']['Backend\Behaviors\RelationController'];
            $prop = new \ReflectionProperty($rc, 'manageTitle');
            $prop->setAccessible(true);
            $prop->setValue($rc, 'backend::lang.relation.add_name');
            $prop->setAccessible(false);
        }

        return $return;
    }

    public function create()
    {
        return $this->asExtension('FormController')->create('create');
    }

    public function create_onSave()
    {
        $result = $this->asExtension('FormController')->create_onSave('create');
        $this->newTaskNotifier();
        $this->onUpdateRubricator();
        return $result;
    }

    public function onUpdateColumn()
    {
        $result = parent::onUpdateColumn();
        $this->newTaskNotifier(post('Task[status]'));
        $this->onUpdateRubricator(post('record_id'));
        return $result;
    }

    public function relationExtendQuery($query, $field)
    {
        $task = 'academy_tasks.';

        if ($field == 'childs') {
            $this->listExtendQuery($query);
        }
        if (post('_relation_field') && $field == 'childs') {

            $query->where(function($query) use($task) {

                $query->where($task.'parent_id');

                if (!empty($this->params[0])) {
                    $query->orWhere($task.'parent_id', $this->params[0]);
                }
            });
            if (!empty($this->params[0])) {
                $query->where($task.'id', '!=', $this->params[0]);
            }
        }
    }

    public function formExtendQuery($query)
    {
        $query->withTrashed();
    }

    public function listExtendQuery($query, $definition = null)//relationExtendQuery
    {
        $query->withTrashed();

        if(in_array($this->action, ['filter','filter_clear','preview']))
            foreach(get('filters',[]) as $param => $value)
                $query->where('academy_tasks.' . $param, $value);

        elseif(isset($this->params[0]))
            $query->where('academy_tasks.project_id', $this->params[0]);
        //$query->whereRaw('`academy_tasks`.`responsible_id`='.$this->user->id);

        $task = 'academy_tasks.';

        $query
            ->leftjoin('igvs_courses_modules_contents as md', $task.'module_id', '=', 'md.id')
            ->leftjoin('igvs_courses_courses as cs', 'md.course_id', '=', 'cs.id')
            ->leftjoin('igvs_courses_categories as act', 'md.category_id', '=', 'act.id')
            ->leftjoin('igvs_courses_categories as top', 'act.parent_id', '=', 'top.id')
            ->leftjoin('academy_tasks_statuses as stat', 'stat.id', '=', $task.'status_id')
            ->leftjoin(DB::raw('(select count(*) as `count`, `parent_id` from `academy_tasks` group by `parent_id`) as `child`'), $task.'id', '=', 'child.parent_id');

        $personal = $this->user ? $this->user->id : 'NULL';

        if($this->user)
            $query
                ->leftjoin(DB::raw('
        (
            select
                count(*) as `count`,
                `cm`.`task_id`
            from
                `academy_tasks_comments` as `cm`
            left join
                `academy_tasks_comments_reads` as `cr`
            on
                `cr`.`user_id`='.(int)$this->user->id.'
            and
                `cr`.`task_id`=`cm`.`task_id`
            where (
                    `cr`.`updated_at` < `cm`.`updated_at`
                or
                    `cr`.`updated_at` is null
            )
            and
                `cm`.`user_id` != '.(int)$this->user->id.'
            group by
                `cm`.`task_id`
        ) as `comment`'), $task.'id', '=', 'comment.task_id');
    }

    private $ignoreListExtendColumns = false;
    public function listExtendColumns($lists)
    {
        if ($this->ignoreListExtendColumns) {
            return;
        }
        if (!$this->project || !Right::hasAccess($this->project->id, $this->user->id, 'task_change')) {
            $lists->removeColumn('buttons');
        }
        $this->ignoreListExtendColumns = true;
        $visibleColumns = $lists->getVisibleColumns();
        $this->ignoreListExtendColumns = false;
        foreach ($lists->columns as $name => $column) {
            if (!isset($visibleColumns[$name])) {
                $lists->columns[$name]['sortable'] = false;
            }
        }
    }

    public function listFilterExtendQuery($query, $scope)
    {
        switch($scope->scopeName)
        {
            case 'responsible':
            case 'user':
                $query->select('id')->selectRaw("concat(`first_name`,' ',`last_name`,' (',`login`,')')");
                break;
            case 'course':
                $query->select('id')->selectRaw("concat(`code`,': ',`title`)");
                break;
            case 'act':
            case 'topic':
                $query->select('id')->selectRaw("concat(`code`,': ',`name`)");

                if($scope->scopeName == 'act')
                    $query->whereNotNull('parent_id');

                else
                    $query->whereNull('parent_id');
                break;
        }
    }

    public function formExtendModel($model)
    {
        if(!$model->id)//только при создании
            $model->project_id = $this->project_id;

        return $model;
    }

    public function onModalStatusChange()
    {

        return $this->makePartial('modal_update', [
            'record_id' => (int) post('id')
        ]);
    }

    public function statuschange($recordId)
    {
        return $this->asExtension('CrudController')->crudupdate($recordId, 'status_change');
    }

    public function statuschange_onSave($recordId)
    {
        return $this->asExtension('CrudController')->crudupdate_onSave($recordId, 'status_change');
    }

    public function onStatusChange()
    {
        if(!$task = Task::where('id', post('task_id'))->withTrashed()->first())
            return;

        $statuses = $task->getStatuses();
        if(!isset($statuses[$status_id = post('status_id')]) || $task->status_id == $status_id)
            return;

        $task->status_id = $status_id;

        $responsibles = $task->getResponsibleOptions();
        unset($responsibles['']);
        reset($responsibles);

        $task->responsible = User::where('id', key($responsibles))->first();

        $task->save();

        if($task->status_id == $status_id) {

            return [
                '#status' . $task->id => $this->makePartial('status_class', [
                    'statuses' => $task->getNewStatuses(),
                    'value' => $status_id,
                ]),
                '#statusresp' . $task->id => is_null($task->responsible) ? '' : ($task->responsible->first_name . ' ' . $task->responsible->last_name . ' (' . $task->responsible->login . ')'),
            ];
        }
    }

    public function createWithoutProject()
    {
        $this->layout = 'plugins/academy/system/layouts/clear';
        return $this->asExtension('FormController')->create('createWithoutProject');
    }

    public function createWithoutProject_onSave()
    {
        $this->asExtension('FormController')->create_onSave('createWithoutProject');
    }

    public function onPreview()
    {
        if ($task = Task::find(post('task_id'))) {
            return ['#spoiler-academy-partial1' => $this->makePartial('preview2', ['task' => $task])];
        }
    }

    public function listOverrideColumn($record, $columnName, $config, $definition)
    {
        if (!$record || $record->deleted_at || $record->progress == 100 || $record->groupCount) {
            unset($config['context']);
        }
        return $config;
    }

    public function onTaskGroup()
    {
        \Session::put('taskGroup', post('taskGroup'));
        return $this->makeList()->onRefresh();
    }

    public static function notifierExtendQuery($query, $definition)
    {
        if (!$user = BackendAuth::getUser()) {
            $query->whereRaw(0);
            return;
        }
        $t = new Task();
        $tTable = $t->getTable();
        $query->where('progress', '<', '100');

        if ($definition == 'actual') {
            $query->where("$tTable.responsible_id", $user->id);
        }
        else {
            $query->where("$tTable.user_id", $user->id);
        }
    }
    public static function notifierFilterExtendQuery($query, $scope)
    {
        switch($scope->scopeName) {
            case 'course':
                $query->select('id')->selectRaw("concat(`code`,': ',`title`)");
                break;
            case 'act':
            case 'topic':
                $query->select('id')->selectRaw("concat(`code`,': ',`name`)");

                if($scope->scopeName == 'act')
                    $query->whereNotNull('parent_id');

                else
                    $query->whereNull('parent_id');
                break;
        }
    }

    private static function getGroupLevel($group_id, &$groups)
    {
        $group = Group::where('id', $group_id)->select('parent_id')->first();
        if (isset($groups[$group->parent_id])) {
            return $groups[$group->parent_id] + 1;
        }
        if (array_key_exists($group->parent_id, $groups)) {
            unset($groups[$group->parent_id]);
        }
        return self::getGroupLevel($group->parent_id, $groups) + 1;
    }

    private static function getNextResult($result, $level, $group_id)
    {
        if ($result > 0) {
            return $result;
        }
        if (-$result == $level) {
            return $group_id;
        }
        return $result - 1;
    }

    private static function getPrevResult($result, $level, $group_id)
    {
        if ($result > 0) {
            return $result;
        }
        if (-$result == $level) {
            return $group_id;
        }
        return $result - 1;
    }

    private static function getGroupByLevel($group_id, &$groups, $level)
    {
        $group = Group::where('id', $group_id)->select('parent_id')->first();
        if (isset($groups[$group->parent_id])) {
            $result = $groups[$group->parent_id];
        }
        else {
            $result = self::getGroupByLevel($group->parent_id, $groups, $level);
        }
        $result = self::getNextResult($result, $level, $group_id);
        if (!isset($groups[$group_id]) && array_key_exists($group_id, $groups)) {
            $groups[$group_id] = $result;
        }
        return $result;
    }

    private static function getCurrentGroups($query)
    {
        return $groups = $query
            ->addSelect('group_id')
            ->groupBy('group_id')
            ->lists('group_id', 'group_id');
    }

    public function getGroupMaxCount($query)
    {
        $groups = [];
        foreach (self::getCurrentGroups($query) as $group => $count) {
            $groups[$group] = null;
        }
        $groups[null] = 0;
        foreach ($groups as $group => $count) {
            if (isset($groups[$group])
                || !array_key_exists($group, $groups)
            ) {
                continue;
            }
            $groups[$group] = self::getGroupLevel($group, $groups);
        }
        return max($groups);
    }

    private $currentGroups;

    public function getGroupGroupQuery($query, $level)
    {
        $groups = [];
        foreach (self::getCurrentGroups(clone $query) as $group => $count) {
            $groups[$group] = null;
        }
        $groups[null] = 0;
        foreach ($groups as $group => $count) {
            if (isset($groups[$group])
                || !array_key_exists($group, $groups)
            ) {
                continue;
            }
            $groups[$group] = self::getGroupByLevel($group, $groups, $level);
        }
        $this->currentGroups = $groups;
        foreach ($groups as $group => $id) {
            if ($id <= 0) {
                unset($groups[$group]);
            }
        }
        if (count($groups)) {
            $group = 'ELT(FIELD(`group_id`, ' . implode(',', array_keys($groups)) . '), ' . implode(',', $groups) . ')';
        }
        else {
            $group = 'NULL';
        }
        $query
            ->selectRaw($group . ' `group_id`')
            ->groupBy(DB::raw($group));
    }

    public function getGroupFilterQuery($query, $index)
    {
        if ($index > 0) {
            $query->whereIn('group_id', array_keys($this->currentGroups, $index));
            return;
        }
        $query->where(function($query) {
            $query
                ->whereIn('group_id', array_keys(array_filter($this->currentGroups, function($item) {
                    return $item <= 0;
                })))
                ->orWhere('group_id', null);
            });
    }

    public function onPdfOptions()
    {
        return $this->makePartial('pdf_options');
    }

    private function getPdfHtml($project_id = null, $template = 'pdf')
    {
        $columns = $this->asExtension('ListController')
            ->makeList('pdf')
            ->getVisibleColumns();
        $widget = $this->asExtension('ListController')->makeList('list');
        $columnOverride = new \ReflectionProperty($widget, 'columnOverride');
        $columnOverride->setAccessible(true);
        $columnOverride->setValue($widget, array_keys($columns));
        $headers = [];
        foreach ($columns as $num => $column) {
            $headers[$num] = $widget->getHeaderValue($column);
        }
        return $this->makePartial($template, [
            'columns' => $columns,
            'headers' => $headers,
            'records' => $widget->prepareModel()->get(),
            'widget' => $widget,
            'project' => Project::find($project_id),
        ]);
    }

    public function html($project_id = null)
    {
        die($this->getPdfHtml($project_id, 'html'));
    }

    public function doc($project_id = null)
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/msword');
        header("Content-Disposition: attachment; filename='report.doc");
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        die($this->getPdfHtml($project_id, 'doc'));
    }

    public function pdf($project_id = null)
    {
        $pdf = \App::make('dompdf.wrapper');
        $pdf->setPaper('letter', 'landscape');
        $pdf->loadHTML($this->getPdfHtml($project_id));
        return $pdf->stream();
    }

    public function quotes() {
        $arr = func_get_args();
        if (count($arr) == 1 && is_array($arr[0])) {
            $arr = $arr[0];
        }
        foreach ($arr as &$elem) {
            $elem = str_replace('"', '""', mb_convert_encoding($elem, 'Windows-1251'));
            //какие переносы может учитывать spss ??
        }
        return '"' . implode('";"', $arr) . '"' . "\r\n";
    }

    public function strip_tags($text) {

        $tagsForClear = ['head', 'style', 'script', 'object', 'embed', 'applet', 'noframes', 'noscript', 'noembed'];
        $wtfToClear = [
            ['address', 'blockquote', 'center', 'del'],
            ['div', 'h[1-9]', 'ins', 'isindex', 'p', 'pre'],
            ['dir', 'dl', 'dt', 'dd', 'li', 'menu', 'ol', 'ul'],
            ['table', 'th', 'td', 'caption'],
            ['form', 'button', 'fieldset', 'legend', 'input'],
            ['label', 'select', 'optgroup', 'option', 'textarea'],
            ['frameset', 'frame', 'iframe'],
        ];
        $fromClear = [];
        $toClear = [];
        foreach ($tagsForClear as $tag) {
            $fromClear[] = '@<' . $tag . '[^>]*?>.*?</' . $tag . '>@siu';
            $toClear[] = ' ';
        }
        foreach ($wtfToClear as $wtf) {
            $fromClear[] =  '@</?((' . implode(')|(', $wtf) . '))@iu';
            $toClear[] = "\n\$0";
        }

        $text = preg_replace($fromClear, $toClear, $text);
        return strip_tags($text);
    }

    public function flush_quotes() {
        echo call_user_func_array('self::quotes', func_get_args());
        flush();
    }

    public function xml($project_id = null)
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="file.csv"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        die($this->getPdfHtml($project_id, 'xml'));
    }

    public function listInjectRowClass($record)
    {
        $templates = $this->getCachedTemplatesId();

        if (is_object($record) && !is_null($record->module_id)) {
            $result = [];
            if ($module = ModuleContent::select('behavior', 'module_id')->withTrashed()->find($record->module_id)) {
                $result[] = 'module-content';
                $result[] = "behavior-{$module->behavior}";
                if (isset($templates[$module->module_id])) {
                    $result[] = "template-{$templates[$module->module_id]}";
                }
            }
            return implode(' ', $result);
        }
    }

    /*
     * кеширование списка модулей
     */
    private function getCachedTemplatesId()
    {
        return \Cache::rememberForever('igvs.modules.all', function() {
            return \Igvs\Courses\Models\Module::lists('path', 'id');
        });
    }
}
