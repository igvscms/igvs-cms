<?php namespace Academy\Tasks\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

class Users extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.RelationController',
        'Academy.System.Behaviors.CrudController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = ['academy.tasks.tasks'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Academy.Tasks', 'tasks', 'users');
    }
}