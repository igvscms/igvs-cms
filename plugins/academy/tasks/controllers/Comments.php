<?php namespace Academy\Tasks\Controllers;

use Backend\Models\User as BackendUser;

use Backend\Classes\Controller;

use Academy\Tasks\Models\Task;
use Academy\Tasks\Models\Project;
use Academy\Tasks\Models\HistoryRead;
use Academy\Tasks\Models\CommentRead;

class Comments extends Controller
{
    use \Academy\System\Traits\SocketSender;

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Academy.System.Behaviors.CrudController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['academy.tasks.tasks'];

    public function __construct()
    {
        parent::__construct();
        \BackendMenu::setContext('Academy.Tasks', 'tasks', 'comments');
    }

    public static function rulesForSendingSocketsMessages() { return [
        'onCommentsUpdate' => function($data, $user, $is_current_cocket, $default)
        {
            $task = Task::find($data['post']['Comment']['task']);
            return $user['action'] == 'Igvs\Courses\Controllers\Categories::index' && isset($user['params']['id']) && $user['params']['id'] == $task->course_id;
        },
    ];}

    //преобразование результата
    public static function socketMessageResultHandler() { return [
        'onCommentsUpdate' => function($data, $result)
        {
            $task = Task::find($data['post']['Comment']['task']);
            $count = $task->module->tasks()->where('progress','<',100)->whereNotNull('responsible_id');

            if (!$user = BackendUser::find($user['user_id'])) {
                return;
            }

            if(empty($showAll) && !$user->is_superuser) {
                $count->where(function($query) use ($user) {
                    $query->whereIn('project_id', Project::where('user_id', $user->id)->lists('id'))
                        ->orWhere('user_id', $user->id)
                        ->orWhere('responsible_id', $user->id);
                });
            }
            $allTasks = $count->lists('updated_at', 'id');
            $count = count(HistoryRead::newTaskLists($allTasks, $user->id)) + count(CommentRead::newTaskLists($allTasks, $user->id));

            return ['original' => [
                '#module_' . $task->module_id . ' + * > .newMessages' => (int)$count ?: ''
            ]];
        }
    ];}

    private static function get(&$value, $default = null)
    {
        return isset($value) ? $value : $default;
    }

    public function issetTasks()
    {
        return Task::getBy(
                self::get($this->params[0]),
                self::get($this->params[1])
            )
            ->count();
    }

    public function listExtendQuery($query)
    {
        $task = new Task();
        $taskTable = $task->getTable();

        $model = $query->getModel();
        $modelTable = $model->getTable();

        Task::getBy(
            self::get($this->params[0]),
            self::get($this->params[1]),
            $query->join($taskTable, "$taskTable.id", '=', "$modelTable.task_id")
        );
    }

    public function crudcreate_onSave()
    {
        $result = parent::crudcreate_onSave();
        $this->socketNewSend('onCommentsUpdate',['result' => true]);
        return $result;
    }

    public function index()
    {
        $this->layout = 'plugins/academy/system/layouts/clear';

        $tasks = Task::getBy(
            self::get($this->params[0]),
            self::get($this->params[1])
        )->lists('id');

        $user = \BackendAuth::getUser();
        CommentRead::where('user_id', $user->id)->whereIn('task_id', $tasks)->delete();

        $create = [];
        foreach ($tasks as $task_id) {
            $create[] = ['user_id' => $user->id, 'task_id' => $task_id, 'updated_at' => date("Y-m-d H:i:s")];
        }
        CommentRead::insert($create);

        return parent::index();
    }
}