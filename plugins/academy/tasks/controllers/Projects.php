<?php namespace Academy\Tasks\Controllers;

use BackendMenu;
use Lang;
use Backend\Classes\Controller;
use Academy\System\FormWidgets\RelationDropdown;
use Academy\tasks\models\Project;
use Academy\tasks\models\Right;
use Academy\tasks\models\Role;
use Academy\tasks\models\Status;
use Academy\tasks\models\Type;

class Projects extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
        'Academy.System.Behaviors.CrudController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = ['academy.tasks.tasks'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Academy.Tasks', 'tasks', 'projects');

        // if(isset($this->params[0]))
        //     $project = Project::find($this->params[0]);

        // else
        //     $project = null;
        // $RelationDropdown = new RelationDropdown($this, $project);
        // $RelationDropdown->bindToController();
    }

    public function initRelation($model, $field = null)
    {
        if($field == null)
            $field = post('_relation_field');

        if($field == 'users' && $role_id = post('Project[roles_dropdown]'))
            $model = Role::find(post('Project[roles_dropdown]'));

        $return = parent::initRelation($model, $field);
        //dd($return);
        //if($field)
            //dd($field);

        if ($this->relationModel && !in_array(get_class($this->relationModel), ['Igvs\Courses\Models\Course', 'Academy\System\Models\CustomField'])) {
            $this->relationModel->project_id = $model->id;
        }
        return $return;
    }

    public function onMove($query, $sort)
    {
        $class = $query->getModel();
        $move = $class::find(post('move'));
        $moveTo = $class::find(post('to'));

        if($moveTo->$sort > $move->$sort) {

            $from = $move->$sort;
            $to = $moveTo->$sort + 1;
            $function = 'decrement';
        }

        else {

            $from = $moveTo->$sort - 1;
            $to = $move->$sort;
            $function = 'increment';
        }

        $query->where($sort, '>', $from)->where($sort, '<', $to)->$function($sort);
        $move->$sort = $moveTo->$sort;
        $move->save();
    }

    public function onMoveStatus()
    {
        $this->onMove(Status::where('project_id', $this->params[0]), 'sort_order');
    }

    public function onMoveRole()
    {
        $this->onMove(Role::where('project_id', $this->params[0]), 'sort_order');
    }

    public function onMoveType()
    {
        $this->onMove(Type::where('project_id', $this->params[0]), 'sort_order');
    }
}