<?php namespace Academy\Tasks\Controllers;

use Backend\Classes\Controller;

class StatusTimes extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
    ];

    public $formConfig = 'config_form.yaml';
    public $requiredPermissions = ['academy.tasks.tasks'];

    public function __construct()
    {
        parent::__construct();
        \BackendMenu::setContext('Academy.Tasks', 'tasks', 'status_times');
    }

    public function update($id, $context = null)
    {
        $this->asExtension('FormController')->update($id, $context);
        $this->layout = 'plugins/academy/system/layouts/modal';
    }
}