<?php namespace Academy\Tasks;

use Backend;
use System\Classes\PluginBase;
use Lang;

class Plugin extends PluginBase
{
    public $require = ['Igvs.Courses'];

    public function boot()
    {
        \Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
            $controller->addCss('/plugins/academy/tasks/assets/css/richedit-img.css');
            $controller->addJs('/plugins/academy/tasks/assets/js/richedit-img.js');
        });
    }

    public function pluginDetails()
    {
        return [
            'name'        => 'Tasks',
            'description' => 'Create, control and analysis tasks',
            'author'      => 'Academy',
            'icon'        => 'icon-bell'
        ];
    }

    public function registerPermissions()
    {
        $tabs = [
            'task' => ['create_project','tasks'],
        ];

        $result = [];
        foreach($tabs as $tab => $premissions) {

            $tab = Lang::get('academy.tasks::' . $tab . '.label');

            foreach($premissions as $premission) {

                $result['academy.tasks.' . $premission] = [

                    'tab' => $tab,
                    'label' => Lang::get('academy.tasks::plugin.permission.' . $premission),
                ];
            }
        }

        return $result;
    }

    public function registerNavigation()
    {
        if (in_array('Academy.tasks', \Config::get('cms.hiddenPlugins', []))) {
            return;
        }
        return [
            'tasks' => [
                'label' => Lang::get('academy.tasks::project.label'),
                'url' => Backend::url('academy/tasks/projects'),
                'icon' => 'icon-university',
                'permissions' => ['academy.tasks.*'],
                'order' => 500,
                'sideMenu' => [
                    'projects' => [
                        'label' => Lang::get('academy.tasks::project.label'),
                        'icon' => 'icon-university',
                        'url' => Backend::url('academy/tasks/projects'),
                        'permissions' => ['academy.tasks.projects']
                    ],
                ]
            ],
        ];
    }
}