<?php namespace Academy\Tasks\Models;

class Group extends \Model
{
    public $table = 'academy_tasks_groups';
    public $timestamps = false;
    public $belongsTo = [
        'project' => 'Academy\Tasks\Models\Project',
        'parent' => 'Academy\Tasks\Models\Group',
    ];
    public $hasMany = [
        'childs' => ['Academy\Tasks\Models\Group', 'key' => 'parent_id'],
    ];

    public function getParentOptions() {
        if (!$this->id) {
            return Group::where('project_id', $this->project_id)
                ->lists('name', 'id');
        }
        return $query = Group::where('id', '!=', $this->id)
            ->where('project_id', $this->project_id)
            ->lists('name', 'id');
    }

    public function getChildren() {
        return $this->childs;
    }
    public function getChildCount() {
        return $this->childs()->count();
    }
    public function scopeGetNested($query) {
        return $query->where('parent_id', null)->get();
    }
}