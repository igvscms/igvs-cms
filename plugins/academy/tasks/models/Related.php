<?php namespace Academy\Tasks\Models;

use Model;
use Lang;
use DB;

use Academy\Tasks\Models\Connection;
use Academy\Tasks\Models\Task;

class Related extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    public $table = 'academy_tasks_related';

    protected $guarded = ['*'];
    public $timestamps = false;

    protected $fillable = [];

    protected $rules =[
        'connection' => 'required',
    ];

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'connection' => 'Academy\Tasks\Models\Connection',
        'task' => 'Academy\Tasks\Models\Task',
    ];

    public $belongsToMany = [
       'related' => [
            'Academy\Tasks\Models\Task',
            'table' => 'academy_tasks_related',//не работает, так как совпадает с названием текущей таблицы
            'key' => 'related_id',
            'otherKey' => 'task_id',
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getRelatedAttribute()
    {
        $tasks = Related::where('related_id', $this->id)->groupBy('task_id')->lists('task_id');
        return Task::withTrashed()->whereIn('id', $tasks);
    }

    public function getOtherTaskAttribute()
    {
        if(isset($this->attributes['other_task']))
            return $this->attributes['other_task'];

        if($this->related_id)
            return Related::find($this->related_id)->task_id;

        return $this->task_id;
    }

    public $other_task_id = null;

    public function afterCreate()
    {
        if(isset($this->other_task_id)) {

            if($this->related_id)
                $related = Related::find($this->related_id);

            else {
                $related = new Related();
                $related->related_id = $this->id;
            }

            $related->task_id = $this->other_task_id;
            $related->connection_id = Connection::find($this->connection_id)->related_id;
            $related->save();
            $this->related_id = $related->id;
            $this->save();
        }
    }

    public function beforeUpdate()
    {
        if(isset($this->attributes['other_task'])) {
            if($related = Related::find($this->related_id)) {
                $related->task_id = $this->attributes['other_task'];
                $related->connection_id = Connection::find($this->connection_id)->related_id;
                $related->save();
            }
            unset($this->attributes['other_task']);
        }
    }

    public function beforeCreate()
    {
        if(isset($this->attributes['other_task'])) {
            $this->other_task_id = $this->attributes['other_task'];
            unset($this->attributes['other_task']);
        }
    }

    public function beforeValidate()
    {
        if(array_key_exists('other_task', $this->attributes) && !$this->attributes['other_task']) {
            unset($this->related_id);
            $this->rules['related_id'] = 'required';
        }
    }

    public function getConnectionOptions()
    {
        $relations = Related::where('task_id', $this->task_id);

        if($this->id)
            $relations->where('id','!=',$this->id);

        $relations = $relations->lists('related_id');

        $connections = Related::whereIn('id', $relations)->where('task_id', $this->other_task)->lists('connection_id');

        return  ['' => Lang::get('backend::lang.form.select_placeholder') . ' ' . Lang::get('academy.tasks::connection.select_label')]
                + Connection::whereNotIn('id', $connections)
                    ->whereNotIn('related_id', $connections)
                    ->lists('code', 'id');
    }

    public function getOtherTaskOptions()
    {
        return ['' => Lang::get('backend::lang.form.select_placeholder') . ' ' . Lang::get('academy.tasks::related.select_label')] + $this
                ->task()
                ->withTrashed()
                ->first()
                ->project()
                ->first()
                ->tasks()
                ->withTrashed()
                ->where('id', '!=', $this->task_id)
                ->lists('name', 'id');
    }
}