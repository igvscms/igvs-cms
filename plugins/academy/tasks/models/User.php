<?php namespace Academy\Tasks\Models;

use Model;
use Backend\Models\User as BackendUser;

class User extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    public $table = 'academy_tasks_projects_users';
    public $timestamps = false;

    protected $rules =[
        'user' => 'required',

    ];

    public $hasMany = [
        'behavior_priority' => ['Academy\Tasks\Models\UserBehaviorPriority', 'key' => 'user_id'],
        'excluded_times' => ['Academy\Tasks\Models\UserExcludedTime', 'key' => 'user_id'],
    ];
    public $belongsToMany = [
        'roles' => [
            'Academy\Tasks\Models\Role',
            'table'    => 'academy_tasks_users_roles',
            'key'      => 'user_id',
            'otherKey' => 'role_id'
        ],
    ];
    public $belongsTo = [
        'user' => 'Backend\Models\User',
        'project' => 'Academy\Tasks\Models\Project',
    ];

    public function getUserOptions()
    {
        $users = $this->project()->first()->users();

        if($this->user)
            $users->where('user_id', '!=', $this->user->id);

        $users = $users->lists('user_id');

        return ['' => trans('backend::lang.form.select_placeholder') . ' ' . trans('academy.tasks::user.relation_label')] + BackendUser::whereNotIn('id', $users)->lists('login', 'id');
    }

    public function getRolesQuery()
    {
        return $this->project()->first()->roles();
    }

    public function getRolesOptions()
    {
        return $this->getRolesQuery()->lists('name', 'id');
    }
}