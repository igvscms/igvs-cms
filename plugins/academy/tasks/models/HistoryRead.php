<?php namespace Academy\Tasks\Models;

use Model;
use BackendAuth;

use Backend\Models\User as BackendUser;

use Academy\Tasks\Models\Task;
use Academy\Tasks\Models\Right;
use Academy\Tasks\Models\History;
use Academy\Tasks\Models\Project;

class HistoryRead extends Model
{
    public $table = 'academy_tasks_history_reads';
    protected $guarded = ['*'];
    protected $fillable = ['user_id','task_id','updated_at'];
    public $timestamps = false;

    final public static function _new($task_id, $user_id = null)
    {
        $task = Task::find($task_id);

        //если пользователь не создатель задачи, не исполнитель и не имеет прав видеть изменения всех задач, то 0
        if(!($user = is_null($user_id) ? BackendAuth::getUser() : BackendUser::find($user_id))
        || !in_array($user->id, $task->user_id, $task->getResponsibleId()) && !Right::hasAccess($task->project_id, $user->id, 'show_changes'))
            return 0;

        $history = History::where('user_id','!=',$user->id);

        if($read = self::where('task_id', $task->id)->where('user_id', $user->id)->select('updated_at')->first())
            $history->where('updated_at','>',$read->updated_at);

        return $history->count();
    }

    final public static function newTaskLists($tasks, $user_id = null)
    {
        //если пользователь не создатель задачи, не исполнитель и не имеет прав видеть изменения всех задач, то 0
        if(!($user = is_null($user_id) ? BackendAuth::getUser() : BackendUser::find($user_id)))
            $reads = [];

        else
            $reads = self::where('user_id', $user->id)->whereIn('task_id', array_keys($tasks))->lists('updated_at', 'task_id');

        foreach($reads as $task_id => $updated_at)
            if($tasks[$task_id] < $updated_at)
                unset($tasks[$task_id]);

        return $tasks;
    }
}
