<?php namespace Academy\Tasks\Models;

use Model;
use Lang;
use BackendAuth;

use Backend\Models\User as BackendUser;

use Academy\Tasks\Models\User;
use Academy\Tasks\Models\Project;
use Academy\Tasks\Models\Role;

class Right extends Model
{
    public $table = 'academy_tasks_rights';

    protected $guarded = ['*'];
    public $timestamps = false;

    protected $fillable = [];

    protected $rules =[];

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];

    public $belongsToMany = [
        'roles' => [
            'Academy\Tasks\Models\Role',
            'table' => 'academy_tasks_role_rights',
            'key' => 'right_id',
            'otherKey' => 'role_id',
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getNameAttribute()
    {
        return Lang::get('academy.tasks::right.names.' . $this->code);
    }

    public static function hasAccess($project_id, $user_id = null, $code = '')
    {
        //всеми правами обладает админ и создатель проекта, остальные права завязаны на ролях
        $project = Project::find($project_id);

        if (!$user = is_null($user_id) ? BackendAuth::getUser() : BackendUser::find($user_id)) {
            return false;
        }
        if ($user->is_superuser || $project->user_id == $user->id) {
           return true;
        }
        if (!($user = User::where('project_id', $project->id)->where('user_id', $user->id)->first())
            || !($right = self::where('code', $code)->first())
        ) {
            return false;
        }
        $roles = $user->roles()->lists('id');

        return !!($right->roles()->whereIn('id', $roles)->first());
    }
}