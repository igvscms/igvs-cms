<?php namespace Academy\Tasks\Models;

use Model;
use Lang;
use Academy\Tasks\Models\Task;

class StatusTime extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    protected $rules =[];

    public $table = 'academy_tasks_statuses_times';

    protected $guarded = ['*'];

    protected $fillable = ['task_id', 'status_id', 'time', 'user_id', 'updated_at'];

    public $timestamps = false;

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => 'Backend\Models\User',
        'status' => 'Academy\Tasks\Models\Status',
        'task' => 'Academy\Tasks\Models\Task',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    private function getTime()
    {
        $times = ['year' => 31536000, 'day' => 86400, 'hour' => 3600, 'minute' => 60, 'second' => 1];
        if ($this->time) {
            $value = $this->time;
        }
        elseif ($this->task) {
            $value = time() - strtotime($this->task->updated_at);
        }
        else {
            $value = 0;
        }

        /*if ($task = Task::where(['id' => $this->task_id, 'status_id' => $this->status_id, 'responsible_id' => $this->user_id])->select('updated_at')->first()) {
            $value += time() - strtotime($task->updated_at);
        }*/

        foreach($times as $key => $time) {

            if($value < $time)
                unset($times[$key]);

            else
                $times[$key] = floor($value / $time);

            $value = $value % $time;
        }

        return $times;
    }

    public function getStringTimeAttribute()
    {
        $result = [];
        foreach ($this->getTime() as $code => $count) {
            $result[] = $count . substr($code, 0, 1);
        }
        return implode(' ', $result);
    }

    public function getRealTimeAttribute()
    {
        $times = $this->getTime();
        $result = '';

        foreach ($times as $key => $time) {
            $result .= $time . ' ' . Lang::choice('academy.tasks::statustime.field.' . $key, $time % 100) . ' ';
        }

        return $result;
    }

    public function beforeCreate()
    {
        $this->updated_at = date("Y-m-d H:i:s");
    }

    public function beforeSave()
    {
        if (isset($this->attributes['string_time'])) {
            $this->time = 0;
            $stringTime = explode(' ', str_replace(
                ['г', 'л', 'у', 'д', 'ч', 'м', 'с', 'c'],
                ['y', 'y', 'y', 'd', 'h', 'm', 's', 's'],
                mb_strtolower($this->attributes['string_time'])
            ));
            foreach ($stringTime as $time) {
                switch (substr($time, -1)) {
                    case 'y':
                        $this->time += 31536000 * substr($time, 0, -1);
                        break;
                    case 'd':
                        $this->time += 86400 * substr($time, 0, -1);
                        break;
                    case 'h':
                        $this->time += 3600 * substr($time, 0, -1);
                        break;
                    case 'm':
                        $this->time += 60 * substr($time, 0, -1);
                        break;
                    case 's':
                        $this->time += substr($time, 0, -1);
                        break;
                    default:
                        $this->time += $time;
                        break;
                }
            }
            unset($this->attributes['string_time']);
        }
        if ($task = Task::find($this->task_id)) {
            if (!$this->user_id) {
                $this->user_id = $task->getResponsibleId();
            }
            if (!$this->status_id) {
                $this->status_id = $task->status_id;
            }
        }
        /*if ($task = Task::where(['id' => $this->task_id, 'status_id' => $this->status_id, 'responsible_id' => $this->user_id])->select('updated_at')->first()) {
            $this->time -= time() - strtotime($task->updated_at);
        }*/
    }
}
