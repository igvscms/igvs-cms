<?php namespace Academy\Tasks\Models;

class Comment extends \Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    protected $rules = [];

    public $table = 'academy_tasks_comments';

    protected $guarded = ['*'];

    protected $fillable = [];

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => 'Backend\Models\User',
        'status' => 'Academy\Tasks\Models\Status',
        'task' => 'Academy\Tasks\Models\Task',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [
        'files' => 'System\Models\File'
    ];

    public function beforeCreate()
    {
        if ($this->text == '' && !$this->getDeferredBindingRecords(post('_session_key'))->count()) {
            return false;
        }
        $this->user = \BackendAuth::getUser();
        $this->status_id = $this->task->status_id;
    }

    public function getTaskOptions()
    {
        if ($this->task_id) {
            return [$this->task_id => ''];
        }
        return Task::getBy(
                $this->context,
                $this->record_id
            )
            ->select('name', 'id')
            ->lists('name', 'id');
    }

    private static function fieldUse($fields, $names, $key, $value = true)
    {
        foreach ((array)$names as $name) {
            if (isset($fields->$name)) {
                $fields->$name->$key = $value;
            }
        }
    }

    public function filterFields($fields, $condition = null)
    {
        if (count($this->getTaskOptions()) < 2) {
            self::fieldUse($fields, 'task', 'cssClass', 'hidden');
        }
    }

    public function getUserRolesAttribute()
    {
        return $this->task->project->getRolesByUser($this->user_id)->lists('name');
    }
}