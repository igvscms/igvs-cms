<?php namespace Academy\Tasks\Models;

class Status extends \Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    public $table = 'academy_tasks_statuses';

    protected $guarded = ['*'];
    public $timestamps = false;

    protected $fillable = [];

    protected $rules =[
        'color' => 'required',
        'icon' => 'required',
        'name' => 'required',
        'action' => 'required',
    ];

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeCreate() {
        if ($last = $this::where('project_id', $this->project_id)->orderBy('sort_order', 'desc')->first()) {
            $this->sort_order = $last->sort_order + 1;
        }
    }
    public function getProgressOptions() {
        $result = [];
        for ($i = 0; $i <= 100; $i++) {
            $result[$i] = $i . '%';
        }
        return $result;
    }
    public function beforeSave() {
        if (isset($this->attributes['is_finish'])) {
            $this->progress = $this->attributes['is_finish'] ? 100 : 0;
            unset($this->attributes['is_finish']);
        }
    }
    public function getIsFinishAttribute() {
        if (isset($this->attributes['is_finish'])) {
            return $this->attributes['is_finish'];
        }
        return $this->progress == 100;
    }
}