<?php namespace Academy\Tasks\Models;

use Model;

class History extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    public $table = 'academy_tasks_history';

    protected $guarded = ['*'];
    protected $fillable = ['data'];
    //public $attributeNames = [];
    protected $rules =[];

    public $belongsTo = [
        'task' => ['Academy\Tasks\Models\Task'],
        'user' => ['Backend\Models\User'],
    ];

    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
    public $hasOne = [];
    public $hasMany = [];

    public function afterFetch()
    {
        $this->data = (isset($this->data) ? json_decode(bzdecompress($this->data),true) : []);
    }

    public function afterSave()
    {
        $this->afterFetch();
    }

    public function beforeValidate()
    {
        $this->data = bzcompress(json_encode($this->data),4);
    }
}