<?php namespace Academy\Tasks\Models;

use Model;
use Lang;
use DB;
use BackendAuth;

use Backend\Models\User as BackendUser;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\ModuleContent;

use Academy\Tasks\Models\User;
use Academy\Tasks\Models\History;
use Academy\Tasks\Models\Status;
use Academy\Tasks\Models\StatusTime;
use Academy\Tasks\Models\StatusChange;
use Academy\Tasks\Models\UserExcludedTime;
use Academy\Tasks\Models\Related;
use Academy\Tasks\Models\Right;
use Academy\Tasks\Models\Type;
use Academy\Tasks\Models\Project;

use Academy\Cms\Models\History as CmsHistory;

class Task extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;
    use \October\Rain\Database\Traits\SoftDeleting;

    private $responsible_id_custom = false;

    protected $rules =[
        'name' => 'between:3,255'
    ];

    public $table = 'academy_tasks';

    protected $guarded = ['*'];

    protected $fillable = [];

    public $hasOne = [];
    public $hasMany = [
        'childs' => ['Academy\Tasks\Models\Task', 'key' => 'parent_id'],
        'comments' => ['Academy\Tasks\Models\Comment', 'key' => 'task_id'],
        'history' => ['Academy\Tasks\Models\History', 'key' => 'task_id'],
        'times' => ['Academy\Tasks\Models\StatusTime', 'key' => 'task_id'],
        'related' => ['Academy\Tasks\Models\Related', 'key' => 'task_id'],
    ];
    public $belongsTo = [
        'responsible' => 'Backend\Models\User',
        'user' => 'Backend\Models\User',
        'status' => 'Academy\Tasks\Models\Status',
        'parent' => 'Academy\Tasks\Models\Task',
        'project' => 'Academy\Tasks\Models\Project',
        'type' => 'Academy\Tasks\Models\Type',
        'group' => 'Academy\Tasks\Models\Group',

        'module' => 'Igvs\Courses\Models\ModuleContent',
        'act' => 'Igvs\Courses\Models\Category',
        'topic' => 'Igvs\Courses\Models\Category',
        'course' => 'Igvs\Courses\Models\Course',

    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [
        'cmshistory' => [
            'Academy\Cms\Models\History',
            'name' => 'attachment',
        ],
        'projectCustomFields' => [
            'Academy\System\Models\CustomFieldResult',
            'name' => 'attachment',
        ]
    ];
    public $attachOne = [];
    public $attachMany = [
        'files' => ['System\Models\File', 'public' => true]
    ];

    private static function get(&$value, $default = null)
    {
        return isset($value) ? $value : $default;
    }

    public static function getPrioryteCodeByKey($key = null)
    {
        $priority = ['low', 'medium', 'high', 'critical'];
        if (is_null($key)) {
            return $priority;
        }
        if (isset($priority[$key])) {
            return $priority[$key];
        }
    }

    public static function getPriorityNameByCode($code)
    {
        return trans('academy.tasks::task.priority.' . $code);
    }

    public static function getPriorityNameByKey($key)
    {
        return Self::getPriorityNameByCode(Self::getPrioryteCodeByKey($key));
    }

    public function getPriorityNameAttribute()
    {
        return Self::getPriorityNameByKey($this->priority);
    }

    public function getPriorityOptions()
    {
        foreach (Self::getPrioryteCodeByKey() as $key => $code) {
            $priority[$key] = Self::getPriorityNameByCode($code);
        }
        return $priority;
    }

    public function getTypeOptions()
    {
        return ['' => Lang::get('backend::lang.form.select_placeholder') . ' ' . Lang::get('academy.tasks::type.select_label')]
                + Type::where('project_id', $this->project_id)->orderBy('sort_order','desc')->lists('name', 'id');
    }

    public function getProjectOptions()
    {
        if(!$user = BackendAuth::getUser())
            return [];

        $projects = \Cache::remember('task.get_project_options.projects.' . $user->id, 1, function() use ($user) {
            return User::where('user_id', $user->id)->lists('project_id', 'id');//в каких проектах под каким id участвует
        });

        $roles = \Cache::remember('task.get_project_options.roles.' . $user->id, 1, function() {
            return StatusChange::where('status_id', null)->lists('role_id');//какие роли могут создавать задачи
        });

        $needProject = \Cache::remember('task.get_project_options.need_project.' . $user->id, 1, function() use ($projects, $roles) {
            $members = DB::table('academy_tasks_users_roles')
                ->whereIn('user_id', array_keys($projects))
                ->whereIn('role_id', $roles)
                ->lists('user_id', 'user_id');

            return array_intersect_key($projects, $members);
        });

        $result = \Cache::remember('task.get_project_options.result.' . $user->id, 1, function() use ($needProject) {
            return Project::whereIn('id', $needProject)->orderBy('name')->lists('name', 'id');
        });

        if(!count($result) || isset($this->project_id))
            return $result;

        reset($result);
        $this->project_id = key($result);
        return $result;
    }

    public function getResponsibleId($refresh_data = false)
    {
        if (self::get($this->attributes['responsible_id']) === self::get($this->original['responsible_id'])
            && self::get($this->attributes['status_id']) === self::get($this->original['status_id'])
        ) {
            return $this->attributes['responsible_id'];
        }

        if ($this->responsible_id_custom === false || $refresh_data) {
            $responsible_id = self::get($this->attributes['responsible_id']);
            $responsibles = $this->getResponsibleOptions();
            if (isset($responsibles[$responsible_id])) {
                $this->responsible_id_custom = $responsible_id;
                return $responsible_id;
            }
            reset($responsibles);

            $this->attributes['responsible_id']
                = $this->responsible_id_custom
                = key($responsibles);

            return $this->responsible_id_custom;
        } else {
            return $this->responsible_id_custom;
        }
    }

    public function getResponsibleOptions()
    {
        if (is_null($this->status_id)
            && count($statuses = $this->getStatusOptions())) {
                reset($statuses);
                $status_id = key($statuses);
            }
        else {
            $status_id = $this->status_id;
        }
        $role_ids = StatusChange::where('status_id', $status_id)
            ->where('type_id', $this->type_id)
            ->groupBy('role_id')
            ->lists('role_id');//группы, которые могут изменять данный статус

        $roles = Role::whereIn('id', $role_ids)
            ->with(['users' => function($users) {
                $users->select('id', 'academy_tasks_projects_users.user_id');
            }])
            ->select('id')
            ->get();

        $users = [];//претенденты на роль исполнителей
        foreach ($roles as $role) {
            foreach ($role->users as $user) {
                $users[$user->id] = $user->user_id;
            }
        }

        $userIds = array_keys($users);

        $notUsers = UserExcludedTime::whereIn('user_id', $userIds)
            ->where('from_time', '<=', date("Y-m-d H:i:s"))
            ->where(function($q) {
                $q->where('to_time',null)
                    ->orWhere('to_time','>=',date("Y-m-d H:i:s"));
            })
            ->groupBy('user_id')
            ->lists('user_id');

        $needUsers = array_diff($userIds, $notUsers);//убрали пользователей, которые отдыхают

        $userSort = array_flip(StatusTime::whereIn('user_id', $needUsers)->where('task_id', $this->id)->orderBy('id','desc')->lists('user_id'));//не учитывая текущий статус ->where('status_id', $this->status_id)

        if (!count($userSort) && ($user = BackendAuth::getUser()) && in_array($user->id, $needUsers)) {
            $userSort[$user->id] = 0;
        }

        $needUsers = array_diff($needUsers, $userSort);

        $userPoints = $this::whereIn('responsible_id', $needUsers)//сколько очков у каждого пользователя(сколько статусов, в которых пользователь задействован)
            //->selectRaw('SUM(IF(`behavior`=1,2,1) * if(`steps`=0,1,`steps`)) as `points`')
            ->selectRaw('COUNT(*) as `count`')
            ->where('progress','<',100)
            ->select('responsible_id','module_id')
            ->groupBy('responsible_id')
            ->groupBy('module_id');

        if (!is_null($this->id)) {
            $userPoints->where('id','!=',$this->id);
        }

        $modules = ModuleContent::whereIn('id', $userPoints
            ->lists('module_id'))
            ->select('behavior','id','steps')
            ->get();

        $mds = [];

        foreach ($modules as $md) {
            $mds[$md->id] = ($md->steps ?: 10) * ($md->behavior == 'practice' ? 2 : 1);
        }

        $ups = [];

        foreach ($userPoints->get() as $up) {
            if (!isset($ups[$up->responsible_id])) {
                $ups[$up->responsible_id] = $up->count * isset($mds[$up->module_id]) ? $mds[$up->module_id] : 30;
            }
            else {
                $ups[$up->responsible_id] += $up->count * isset($mds[$up->module_id]) ? $mds[$up->module_id] : 30;
            }
        }

        $userPoints = $ups;

        asort($userPoints);

        foreach ($userPoints as $key => $val) {
            if (is_null($val)) {
                unset($userPoints[$key]);
            }
        }

        $needUsers = array_diff($needUsers, $userPoints);

        if ($module = $this->module()->first()) {

            $behaviors = ['practice' => 0,'ask' => 1,'check' => 2,'test' => 3,'' => null];

            $userBehav = UserBehaviorPriority::whereIn('user_id', $needUsers)
                ->where('behavior', $behaviors[$module->behavior])
                ->orderBy('sort', 'asc')
                ->lists('user_id');

            $needUsers = array_diff($needUsers, $userBehav);
        }
        else {
            $userBehav = [];
        }


        foreach (BackendUser::whereIn('id', $users)->select('id')->selectRaw("concat(`first_name`,' ',`last_name`,' (',`login`,')') as `full_user_name`")->get() as $name
        ) {
            $names[$name->id] = $name->full_user_name;
        }

        $result = [];
        foreach ($userSort as $user => $sort) {//пользователи, решавшие данную задачу
            $result[$users[$user]] = $names[$users[$user]];
        }
        foreach ($userPoints as $user => $sort) {//пользователи, на которых меньше задач
            if (isset($users[$user])) {
                $result[$users[$user]] = $names[$users[$user]];
            }
        }
        foreach ($needUsers as $sort => $user) {//остальные пользователи, которые могут решать данную задачу
            $result[$users[$user]] = $names[$users[$user]];
        }
        foreach ($userBehav as $sort => $user) {//остальные пользователи, которые могут решать данную задачу
            $result[$users[$user]] = $names[$users[$user]];
        }

        $result_keys = array_keys($result);

        $right = Right::where('code', 'auto_executor')->first();

        $auto_executor_roles = $right->roles()->where('project_id', $this->project_id)->lists('id');

        $lastUsers = [];
//        foreach ($result as $user_id => $user_name) {
//            $auto_executor = User::where('user_id', $user_id)
//                ->where('project_id', $this->project_id)
//                ->first()
//                ->roles()
//                ->whereIn('id', $auto_executor_roles)
//                ->first();
//            if (!$auto_executor) {
//                unset($result[$user_id]);
//                $lastUsers[$user_id] = $user_name;
//            }
//        }

        $auto_executor_result = User::whereIn('academy_tasks_projects_users.user_id', array_keys($result))
            ->leftjoin('academy_tasks_users_roles as pivot_role_id', 'pivot_role_id.role_id', '=', 'academy_tasks_projects_users.user_id')
            ->where('project_id', $this->project_id)
            ->whereIn('pivot_role_id.role_id', $auto_executor_roles)
            ->lists('academy_tasks_projects_users.user_id');

        foreach ($result as $user_id => $user_name) {
            if (!in_array($user_id, $auto_executor_result)) {
                unset($result[$user_id]);
                $lastUsers[$user_id] = $user_name;
            }
        }

        return $result + $lastUsers;
    }

    public function getCourseOptions()
    {
        if(!$this->project_id && !count($this->getProjectOptions()))
            return [];

        $return = \Cache::remember("task.get_course_options.courses.{$this->project_id}", 1, function() {
            $select = ['' => Lang::get('backend::lang.form.select_placeholder') . ' ' . Lang::get('igvs.courses::lang.tasks.s.course')];

            $courses = $this->project()->first()->courses()->select('id')->selectRaw("concat(`id`,'# ',`code`,': ',`title`) as `full_course_name`")->lists('full_course_name','id');

            if(!isset($courses[$this->course_id]))
                $this->course_id = null;

            foreach ($courses as &$item) {
                $item = str_replace('<br>', ' ', $item);
            }

            return $select + $courses;
        });

        return $return;
    }

    public function getTopicOptions()
    {
        if (!($user = BackendAuth::getUser())) {
            return [];
        }

        $select = ['' => Lang::get('backend::lang.form.select_placeholder') . ' ' . Lang::get('igvs.courses::lang.tasks.s.topic')];

        $courses = array_keys($this->getCourseOptions());

        if(!$this->course_id || count($courses) < 2) {
            $this->topic_id = null;
            return $select;
        }


        $task_projects = \Cache::get('task.get_topic_options.courses.' . $user->id, []);
        if (!in_array($this->project_id, $task_projects)) {
            $task_projects[] = $this->course_id;
            \Cache::put('task.get_topic_options.courses.' . $user->id, $task_projects, 1);
        }

        $return = \Cache::remember("task.get_topic_options.{$user->id}_{$this->course_id}", 1, function() use($select, $courses) {


//            $topic = \Db::table('igvs_courses_categories')->where('parent_id', null)->select('id')->selectRaw('concat(`code`,\': \',`name`) as `full_topic_name`');
            $topic = Category::where('parent_id', null)->select('id')->selectRaw('concat(`code`,\': \',`name`) as `full_topic_name`');

            if($this->course_id)
                $topic->where('course_id', $this->course_id);

            else
                $topic->whereRaw('course_id in (' . \Db::raw(implode(', ', array_filter($courses, function($v) { return (boolean)strlen($v) && !in_array($v, ['add_topic','add_act','add_module']); }))) . ')');
//                $topic->whereIn('course_id', [1,2]);
//                $topic->whereIn('course_id', $courses);

            $topics = $topic->lists('full_topic_name','id');

            if(!isset($topics[$this->topic_id]))
                $this->topic_id = null;

            $newTopic = $this->course_id ? ['add_topic' => trans('igvs.courses::lang.category.createtopic_title')] : [];

            return $select + $newTopic + $topics;
        });

        return $return;
    }

    public function getActOptions()
    {
        if (!($user = BackendAuth::getUser())) {
            return [];
        }

        $select = ['' => Lang::get('backend::lang.form.select_placeholder') . ' ' . Lang::get('igvs.courses::lang.tasks.s.act')];

        if(!$this->topic_id || count($this->getTopicOptions()) < 2) {
            $this->act_id = null;
            return $select;
        }

        $task_projects = \Cache::get('task.get_act_options.topics.' . $user->id, []);
        if (!in_array($this->project_id, $task_projects)) {
            $task_projects[] = $this->topic_id;
            \Cache::put('task.get_act_options.topics.' . $user->id, $task_projects, 1);
        }

        $return = \Cache::remember("task.get_act_options.{$user->id}_{$this->topic_id}", 1, function() use($select) {
            $act = Category::whereNotNull('parent_id');

            if($this->topic_id)
                $act->where('parent_id', $this->topic_id);

            else
                $act->whereRaw('parent_id IN (' . implode(', ', array_filter(array_keys($this->getTopicOptions()), function($v) { return strlen($v) && !in_array($v, ['add_topic','add_act','add_module']); })) . ')');

            $acts = $act->lists('name','id');

            if(!isset($acts[$this->act_id]))
                $this->act_id = null;

            $newAct = $this->topic_id ? ['add_act' => trans('igvs.courses::lang.category.createact_title')] : [];

            return $select + $newAct + $acts;
        });

        return $return;
    }

    public function getModuleOptions()
    {
        if (!($user = BackendAuth::getUser())) {
            return [];
        }

        $select = ['' => Lang::get('backend::lang.form.select_placeholder') . ' ' . Lang::get('igvs.courses::lang.tasks.s.module')];

        $act_options = $this->getActOptions();

        if(!$this->act_id || count($act_options) < 2) {
            $this->module_id = null;
            return $select;
        }

        $module = ModuleContent::select('id')->selectRaw('concat(`code`,\': \',`name`) as `full_module_name`');

        $topics_ids = array_filter(array_keys($act_options), function($v) { return (boolean)strlen($v) && !in_array($v, ['add_topic','add_act','add_module']); });

        $modules = [];

        if($this->act_id)
            $modules = $module->where('category_id', $this->act_id)
                ->lists('full_module_name','id');
        else if (count($topics_ids))
            $modules = $module->whereRaw('category_id in (' . \Db::raw(implode(', ', $topics_ids)) . ')')
                ->lists('full_module_name','id');
        
        if(!isset($modules[$this->module_id]))
            $this->module_id = null;

        $newModule = $this->act_id ? ['add_module' => 'Добавить модуль'] : [];

        return $select + $newModule + $modules;
    }

    public function getStatusOptions()//статусы, доступные текущему пользователю
    {
        if (!($user = BackendAuth::getUser())) {
            return [];
        }
        if (!$this->project_id && !count($this->getProjectOptions())) {
            return [];
        }

        $task_projects = \Cache::get('task.get_status_options.projects.' . $user->id, []);
        if (!in_array($this->project_id, $task_projects)) {
            $task_projects[] = $this->project_id;
            \Cache::put('task.get_status_options.projects.' . $user->id, $task_projects, 1);
        }

        $project_id = $this->project_id;

        $roles = \Cache::remember("task.get_project_options.project.roles.{$user->id}_{$this->project_id}", 1, function() use ($project_id, $user) {
            $user_proj = User::where('project_id', $project_id)->where('user_id', $user->id)->select('id')->first();

            if (!$user_proj) {
                return [];
            }

            $roles = $this->project()->first()->roles()->lists('id');
            $roles = $user_proj->roles()->whereIn('id', $roles)->lists('id');

            return $roles;
        });

        $status_id = isset($this->original['status_id']) ? $this->original['status_id'] : '';
        $status_cache_code = "{$this->type_id}_{$status_id}";
        $statuses_cache_code = \Cache::get('task.get_status_options.statuses_cache_code.' . $user->id, []);
        if (!in_array($status_cache_code, $statuses_cache_code)) {
            $statuses_cache_code[] = $status_cache_code;
            \Cache::put('task.get_status_options.statuses_cache_code.' . $user->id, $task_projects, 1);
        }

        $statuses = \Cache::remember("task.get_status_options.statuses.{$user->id}_{$status_cache_code}", 1, function() use ($roles) {
            $statuses = StatusChange::whereIn('role_id', $roles)
                ->where('type_id', $this->type_id)
                ->where('status_id', isset($this->original['status_id']) ? $this->original['status_id'] : null)
                ->groupBy('status_new_id')
                ->lists('status_new_id');

            if (!empty($this->original['status_id'])) {
                $statuses[] = $this->original['status_id'];
            }
            return Status::whereIn('id', $statuses)->lists('name', 'id');
        });

        return $statuses;
    }

    public function beforeCreate()
    {
        $this->user_id = BackendAuth::getUser()->id;
    }

    public function updateHistory()
    {
        $this->cmshistory()->add(new CmsHistory([
            'values' => $this->attributes,
            'logfields' => 'status_id'
        ]));
    }

    public function afterCreate()
    {
        $history = new History();

        foreach($this->attributes as $key => $value) {
            if(in_array($key, ['id', 'user_id']) || $value === '' || $value === null)
                continue;

            $history->attributes['data'][$key] = [null, $value];
        }
        $history->task_id = $this->id;
        $history->user_id = BackendAuth::getUser()->id;

        $history->save();

        $this->updateHistory();
    }

    public function afterSave()
    {
        if (isset($this->original['status_id'])
            && $this->original['status_id'] != $this->status_id
                && isset($this->original['responsible_id'])
        ) {
            StatusTime::create([
                'status_id' => $this->original['status_id'],
                'user_id' => $this->original['responsible_id'],
                'task_id' => $this->id,
                'updated_at' => $this->updated_at,
                'time' => strtotime($this->updated_at) - strtotime($this->original['updated_at']),
            ]);
        }
    }

    public function beforeUpdate()
    {
        if(is_null($this->responsible)) {

            $responsibles = $this->getResponsibleOptions();
            reset($responsibles);
            $this->responsible_id = key($responsibles);
        }

        $user_id = ($user = BackendAuth::getUser()) ? $user->id : null;

        $history = new History();

        foreach($this->original as $key => $value) {
            if($value == $this->$key || $key == 'user_id')
                continue;

            $history->attributes['data'][$key] = [$value, $this->$key];
        }
        $history->task_id = $this->id;
        $history->user_id = $user_id;

        $history->save();

        $this->updateHistory();
    }

    public function beforeSave()
    {
        if(!in_array($this->module_id, array_keys($this->getModuleOptions())))
            $this->module_id = null;

        elseif($this->module_id)
            $this->act_id = $this->module()->first()->category_id;

        if(!in_array($this->act_id, array_keys($this->getActOptions()))) {
            $this->module_id = null;
            $this->act_id = null;
        }

        elseif($this->act_id)
            $this->topic_id = Category::find($this->act_id)->parent_id;

        if(!in_array($this->topic_id, array_keys($this->getTopicOptions()))) {
            $this->module_id = null;
            $this->act_id = null;
            $this->topic_id = null;
        }

        elseif($this->topic_id)
            $this->course_id = Category::find($this->topic_id)->course_id;

        if (!in_array($this->course_id, array_keys($this->getCourseOptions()))) {
            $this->module_id = null;
            $this->act_id = null;
            $this->topic_id = null;
            $this->course_id = null;
        }
        $progresses = $this->getProgressOptions();
        reset($progresses);
        $this->progress = max(key($progresses), $this->progress);
        $responsibles = $this->getResponsibleOptions();
        if (!isset($responsibles[$this->getResponsibleId()])) {
            if (count($responsibles)) {
                reset($responsibles);
                $this->responsible_id = end($responsibles);
            }
            else {
                $this->responsible_id = null;
            }
        }
    }

    public function beforeValidate()
    {
        $statuses = $this->getStatuses();

        if(!isset($statuses[$this->status_id]))
            $this->status_id = null;

        $this->rules['type'] = 'required';

        //if($this->module_id)
            //$this->rules['steps'] = 'required';
    }

    public function translate($key, $values)
    {
        switch($key) {

            case 'name':
            case 'deadline':
            case 'priority':
            case 'description':
                $result = [Lang::get('academy.tasks::task.field.' . $key)];
                foreach($values as $val)
                    $result[] = $val;
                return $result;

            case 'parent_id':
            case 'responsible_id':

                $key = substr($key, 0, -3);
                $result = [Lang::get('academy.tasks::task.field.' . $key)];

                $class = $this->belongsTo[$key];

                $class = $class::whereIn('id', $values);

                if($key == 'responsible')
                    $class->select('id')->selectRaw("concat(`first_name`,' ',`last_name`,' (',`login`,')') as `name`");

                $newValues = $class->lists('name','id');

                foreach($values as $val)
                    $result[] = isset($newValues[$val]) ? $newValues[$val] : $val;

                return $result;

            case 'status_id':
            case 'module_id':
            case 'act_id':
            case 'topic_id':
            case 'course_id':

                $key = substr($key, 0, -3);
                $result = [Lang::get('academy.tasks::task.field.' . $key)];

                $class = $this->belongsTo[$key];

                $class = $class::whereIn('id', $values);

                $name = [
                    'status' => 'name',
                    'module' => "concat(`code`,': ', `name`)",
                    'topic' => "concat(`code`,': ', `name`)",
                    'act' => 'name',
                    'course' => "concat(`code`,': ', `title`)",
                ];

                $newValues = $class->select('id')->selectRaw($name[$key] . ' AS `full_name`')->lists('full_name','id');

                foreach($values as $val)
                    $result[] = isset($newValues[$val]) ? $newValues[$val] : $val;

                return $result;
        }
    }

    public function getStatuses()
    {
        $statuses = array_keys($this->getStatusOptions());
        $statuses[] = $this->status_id;

        $statuses_hash = md5(implode('_', $statuses));
        $result = \Cache::remember("task.get_statuses.{$statuses_hash}", 1, function() use ($statuses) {
            $result = [];
            foreach(Status::whereIn('id', $statuses)->get() as $status)
                $result[$status->id] = (object)$status->toArray();

            return $result;
        });

        return $result;
    }

    public function getNewStatuses()
    {
        return $this->getStatuses();
    }

    public function scopePersonal($query)
    {
        if($user = BackendAuth::getUser())
            $query->where('academy_tasks.responsible_id', $user->id);
    }

    private static function fieldUse($fields, $names, $key, $value = true, $besides = false)
    {
        $names = (array) $names;
        foreach ($fields as $name => $obj) {
            if (in_array($name, $names) != $besides) {
                $fields->$name->$key = $value;
            }
        }
    }
    public function filterFields($fields, $context = null)
    {
        if (count($this->getProjectOptions()) == 1) {
            self::fieldUse($fields, 'project', 'hidden');
        }
        if(count($this->getCourseOptions()) < 2) {
            self::fieldUse($fields, [
                'course',
                'topic',
                'act',
                'module',
                'course_section'
            ], 'hidden');
        }

        if ($this->status_id
            && !$this->status->is_logged
        ) {
            self::fieldUse($fields, 'times@status_change', 'hidden');
        }

        if (!$this->id) {
            return;
        }
        if (!$user = BackendAuth::getUser()) {
            //блокируем все поля
            self::fieldUse($fields, [], 'disabled', true, true);
            return;
        }
        if ($user->id == $this->user_id || Right::hasAccess($this->project_id, $user->id, 'task_change')) {
            return;//доступно всё
        }
        if ($this->original['responsible_id'] != $user->id) {
            //блокируем все поля
            self::fieldUse($fields, [], 'disabled', true, true);
            return;
        }
        //блокируем все, кроме стастуса, исполнителя и готовности
        self::fieldUse($fields, ['status', 'status@status_change', 'responsible', 'progress'], 'disabled', true, true);
    }

    public function getProgressOptions()
    {
        $progress = ($status = Status::find($this->status_id)) ? $status->progress : 0;

        $result = [];

        for($i = $progress; $i <= 100; $i++)
            $result[$i] = $i . '%';

        return $result;
    }

    public function scopeGetBy($query, $context, $id, $editable = false)
    {
        $contexes = ['course', 'topic', 'act', 'module'];
        if ((false === $contextPos = array_search($context, $contexes))
            || !$project = Project::getBy($context, $id)
        ) {
            return $query->whereRaw('0');
        }
        $taskTable = $this->getTable();

        $query->where("$taskTable.{$context}_id", $id)
            ->where("$taskTable.project_id", $project->id);
        if ($editable) {
            for ($i = $contextPos + 1; $i < count($contexes); $i++) {
                $query->where("$taskTable.{$contexes[$i]}_id", null);
            }
        }
        return $query;
    }

    public static function getBy($context, $id, $query = null, $editable = true)
    {
        $task = new Task();
        if (is_null($query)) {
            $query = Task::whereRaw(1);
        }
        return $task->scopeGetBy($query, $context, $id, $editable);
    }

    public function getChildren()
    {
        if (!$this->groupCount) {
            return [];
        }
        return $this->groupQuery->where($this->groupField, $this->id)->get();
    }

    private function getTreeGroups(&$result, $parent_id = null, $level = 0)
    {
        $childs = $this
            ->project
            ->groups()
            ->where('parent_id', $parent_id)
            ->lists('name', 'id');
        foreach ($childs as $id => $name) {
            $result[$id] = str_repeat('━', $level) . ' '. $name;
            $this->getTreeGroups($result, $id, $level + 1);
        }
    }

    public function getGroupOptions()
    {
        $result = [];
        $this->getTreeGroups($result);
        return $result;
    }
/*
    public function getChildCount()
    {
        return $this->groupCount;
    }

    public static function getGroups($key = null, $value = null)
    {
        $fields = [
            'status_id' => [
                'class' => 'Academy\Tasks\Models\Status',
                'title' => 'name `title`',
                'default' => 'Без статуса',
                'select' => 'Группировать по статусу',
            ],
            'course_id' => [
                'class' => 'Igvs\Courses\Models\Course',
                'title' => 'concat(code, ": ", title) `title`',
                'default' => 'Курс не выбран',
                'select' => 'Группировать по курсу',
            ],
            'topic_id' => [
                'class' => 'Igvs\Courses\Models\Category',
                'title' => 'concat(code, ": ", name) `title`',
                'default' => 'Топик не выбран',
                'select' => 'Группировать по топику',
            ],
            'act_id' => [
                'class' => 'Igvs\Courses\Models\Category',
                'title' => 'name `title`',
                'default' => 'Акт не выбран',
                'select' => 'Группировать по акту',
            ],
            'user_id' => [
                'class' => 'Backend\Models\User',
                'title' => 'concat_ws(" ", first_name, last_name, "(", login, ")")`title`',
                'default' => 'Без автора',
                'select' => 'Группировать по автору',
            ],
            'responsible_id' => [
                'class' =>'Backend\Models\User',
                'title' => 'concat_ws(" ", first_name, last_name, "(", login, ")")`title`',
                'default' => 'Без исполнителя',
                'select' => 'Группировать по исполнителю'
            ]
        ];
        if (is_null($key) && is_null($value)) {
            return $fields;
        }
        if (is_null($key)) {
            $result = [];
            foreach ($fields as $field => $options) {
                $result[$field] = self::get($options[$value]);
            }
            return $result;
        }
        if (is_null($value)) {
            return self::get($fields[$key]);
        }
        return self::get($fields[$key][$value]);
    }

    public function scopeGetNested($query)
    {
        $subQuery = clone $query;

        if (is_null($group = self::getGroups($field = \Session::get('taskGroup'))) || !$field) {
            return $query->get();
        }
        $class = $group['class'];
        $title = $group['title'];
        $default = $group['default'];

        $records = $subQuery->addSelect(\DB::raw('count(*)`count`'))->groupBy($field)->get();
        foreach ($records as $record) {
            $fieldValue = $record->$field;
            $count = $record->count;
            $record->attributes = [];
            $record->groupCount = $count;
            $record->id = $fieldValue;
            $record->groupQuery = clone $query;
            $record->groupField = 'academy_tasks.' . $field;
            if ($model = $class::where('id', $record->id)->selectRaw($title)->first()) {
                $record->groupName = $model->title;
            }
            else {
                $record->groupName = $default;
            }
            $record->groupName .= ' (' . $count . ')';
        }
        return $records;
    }*/

    public function scopeProjectCustomFields(&$query)
    {
        if ($this->project) {
            $query = $this->project->custom_fields();
        }
        else {
            $query->whereRaw('0');
        }
    }
}
