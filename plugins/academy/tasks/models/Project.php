<?php namespace Academy\Tasks\Models;

use Model;
use BackendAuth;
use Backend\Models\User as BackendUser;
use Academy\tasks\Models\User;
use Academy\tasks\Models\StatusChange;
use Academy\tasks\Models\Status;
use Academy\tasks\Models\Role;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\ModuleContent;

use Academy\System\Models\CustomFieldValue;

class Project extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;
    
    protected $rules =[
        'name' => 'required',
    ];
    public $table = 'academy_tasks_projects';
    public $hasMany = [
        'tasks' => ['Academy\Tasks\Models\Task', 'key' => 'project_id'],
        'statuses' => ['Academy\Tasks\Models\Status', 'key' => 'project_id', 'order' => 'sort_order desc'],
        'types' => ['Academy\Tasks\Models\Type', 'key' => 'project_id', 'order' => 'sort_order desc'],
        'roles' => ['Academy\Tasks\Models\Role', 'key' => 'project_id', 'order' => 'sort_order desc'],
        'users' => ['Academy\Tasks\Models\User', 'key' => 'project_id'],
        'courses' => ['Igvs\Courses\Models\Course', 'key' => 'project_id'],
        'course_count' => ['Igvs\Courses\Models\Course', 'key' => 'project_id', 'count' => true],
        'groups' => ['Academy\Tasks\Models\Group', 'key' => 'project_id'],
    ];
    public $morphMany = [
        'custom_fields' => [
            'Academy\System\Models\CustomField',
            'name' => 'attachment',
        ],
    ];
    public $belongsTo = [
        'user' => 'Backend\Models\User',
    ];
    public $belongsToMany = [];

    public function beforeCreate()
    {
        $this->user = BackendAuth::getUser();
    }

    public function scopeCustom_fields_values($query)
    {
        if (func_num_args() > 1) {
            die();
        }
        return $this->custom_fields();
    }

    public function isCanCreateTasks()
    {
        $roles = $this->getRolesByUser()->lists('id');
        return StatusChange::whereIn('role_id', $roles)
            ->where('status_id', null)
            ->limit(1)
            ->count();
    }

    public $newStatuses = null;

    public function getStatusesRepeaterAttribute()
    {
        $result = [];
        foreach($this->statuses()->orderBy('sort')->get() as $status)
            $result[] = $status;
        
        return $result;
    }

    public $role = null;
    public $type = null;
    public $status_table = [];

    public function beforeSave()
    {
        if(!($user = BackendAuth::getUser()))
            return false;

        if(!$user->hasAccess('academy.tasks.create_project'))
            return false;

        if(!is_null($this->newStatuses)) {

            $create = [];

            $oldStatuses = [];
            foreach($this->statuses()->orderBy('sort')->get() as $status)
                $oldStatuses[$status->id] = $status;

            $delete = array_flip(array_keys($oldStatuses));

            foreach($this->newStatuses as $sort => $status) {

                unset($status['switch']);
                $status['icon'] = &$status['icon'];
                $status['sort'] = $sort;

                if(isset($oldStatuses[$status['id']])) {
                    unset($delete[$status['id']]);
                    $newStatus = $oldStatuses[$status['id']];
                }

                else {
                    $newStatus = new Status();
                    $newStatus->project_id = $this->id;
                }
                $newStatus->name = $status['name'];
                $newStatus->color = $status['color'];
                $newStatus->icon = $status['icon'];
                $newStatus->sort = $status['sort'];
                $newStatus->save();
            }

            Status::whereIn('id', array_flip($delete))->delete();
        }

        if(isset($this->role) && $role = Role::find($this->role)) {
            $role->status_table = $this->status_table;
            $role->type = $this->type;
            $role->save();
        }
    }

    public function beforeValidate()
    {
        if(isset($this->attributes['statuses_repeater'])) {

            $this->newStatuses = array_values($this->attributes['statuses_repeater']);
            unset($this->attributes['statuses_repeater']);
        }
        unset($this->attributes['roles_dropdown']);
    }

    public function getRolesDropdownOptions()
    {
        return ['' => trans('backend::lang.form.select_placeholder') . ' ' . trans('academy.tasks::role.select_label')] + $this->roles()->lists('name', 'id');
    }

    public function getTypeOptions()
    {
        return ['' => trans('backend::lang.form.select_placeholder') . ' ' . trans('academy.tasks::type.select_label')] + $this->types()->lists('name', 'id');
    }

    public static function getAvailable($user_id = null)
    {
        if (!is_null($user_id)) {
            $user = BackendUser::find($user_id);
        }
        elseif (!$user = BackendAuth::getUser()) {
            return self::whereRaw(0);
        }
        if ($user->hasAccess('academy.tasks.create_project')) {
            return new self();
        }
        return self::where(function($self) use($user) {
            $self->where('user_id', $user->id)
                ->orWhereIn('id', User::where('user_id', $user->id)->lists('id'));
        });//те проекты, в которых задействован пользователь
    }

    public function getRolesByUser($user_id = null)
    {
        if (is_null($user_id)) {
            if (!$user = BackendAuth::getUser()) {
                return Role::whereRaw('0');
            }
            $user_id = $user->id;
        }
        if (!$member = $this->users()->where('user_id', $user_id)->first()) {
            return Role::whereRaw('0');
        }
        return $member->roles();
    }

    public static function getBy($context, $id)
    {
        switch ($context) {
            case 'module':
                if (!$module = ModuleContent::where('id', $id)->select('course_id')->first()) {
                    return;
                }
                $course_id = $module->course_id;
                break;
            case 'topic':
            case 'act':
                if (!$category = Category::where('id', $id)->select('course_id')->first()) {
                    return;
                }
                $course_id = $category->course_id;
                break;
            case 'course':
                $course_id = $id;
                break;
            default: 
                return;
        }
        if ($course = Course::where('id', $course_id)->select('project_id')->first()) {
            return $course->project;
        }
    }

    public function scopeProjectCustomFields(&$query)
    {
        $query = $this->custom_fields();
    }
}