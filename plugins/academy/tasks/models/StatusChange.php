<?php namespace Academy\Tasks\Models;

use Model;
use Lang;
use Backend\Models\UserGroup;
use Academy\Tasks\Models\Status;

class StatusChange extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    protected $rules =[];

    public $table = 'academy_tasks_statuses_changes';

    protected $guarded = ['*'];

    protected $fillable = ['group_id','status_before','status_after'];

    public $timestamps = false;
    #protected $primaryKey = 'before';

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'status' => ['Academy\Tasks\Models\Status', 'key' => 'status_id'],
        'status_new' => ['Academy\Tasks\Models\Status', 'key' => 'status_new_id'],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
    public $belongsToMany = [];
}