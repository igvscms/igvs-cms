<?php namespace Academy\Tasks\Models;

use Model;

class Type extends Model
{
    public $table = 'academy_tasks_types';
    public $timestamps = false;
}