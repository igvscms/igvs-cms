<?php namespace Academy\Tasks\Models;

use Model;
use BackendAuth;

use Backend\Models\User as BackendUser;

use Academy\Tasks\Models\Task;
use Academy\Tasks\Models\Right;
use Academy\Tasks\Models\Comment;

class CommentRead extends Model
{
    protected $rules =[];

    public $table = 'academy_tasks_comments_reads';

    protected $guarded = ['*'];

    protected $fillable = ['user_id','task_id','updated_at'];

    public $timestamps = false;

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    final public static function _new($task_id, $user_id = null)
    {
        if (!$user = is_null($user_id) ? BackendAuth::getUser() : BackendUser::find($user_id)) {
            return 0;
        }
        $read = new self();
        $readTable = $read->getTable();

        $comment = new Comment();
        $commentTable = $comment->getTable();

        $comment = Comment::where("$commentTable.user_id",'!=',$user->id)
            ->whereIn("$commentTable.task_id", (array) $task_id)
            ->join($readTable, \DB::raw("$readTable.user_id={$user->id} && $readTable.task_id"),'=',"$commentTable.task_id")
            ->where(function($comment) use($commentTable,$readTable) {
                $comment->where("$commentTable.updated_at",'>',"$readTable.updated_at")
                    ->orWhere("$readTable.updated_at");
            });
        //if(!Right::hasAccess($task->project_id, $user->id, 'show_changes')) {
            $comment->whereHas('task', function($task) use($user) {
                $task->where('user_id', $user->id)
                    ->orWhere('responsible_id', $user->id);
            });
        //}
        return $comment->count();
    }

    final public static function newTaskLists($tasks, $user_id = null)
    {
        //если пользователь не создатель задачи, не исполнитель и не имеет прав видеть изменения всех задач, то 0
        if (!($user = is_null($user_id) ? BackendAuth::getUser() : BackendUser::find($user_id))) {
            $reads = [];
        }
        else {
            $reads = self::where('user_id', $user->id)
                ->whereIn('task_id', array_keys($tasks))
                ->lists('updated_at', 'task_id');
        }
        foreach ($reads as $task_id => $updated_at) {
            if ($tasks[$task_id] < $updated_at) {
                unset($tasks[$task_id]);
            }
        }
        return $tasks;
    }
}