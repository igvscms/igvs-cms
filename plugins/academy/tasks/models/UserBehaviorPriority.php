<?php namespace Academy\Tasks\Models;

use Model;
use Lang;
use Academy\tasks\Models\User;

class UserBehaviorPriority extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    protected $rules =[];

    public $table = 'academy_tasks_users_behavior_priority';

    protected $guarded = ['*'];

    protected $fillable = [];

    public $timestamps = false;

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => 'Academy\tasks\Models\User'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}