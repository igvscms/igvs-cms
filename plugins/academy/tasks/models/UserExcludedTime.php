<?php namespace Academy\Tasks\Models;

use Model;
use Lang;
use Academy\Tasks\Models\User;

class UserExcludedTime extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    protected $rules =[];

    public $table = 'academy_tasks_users_excluded_times';

    protected $guarded = ['*'];

    protected $fillable = [];

    public $timestamps = false;

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => 'Academy\Tasks\Models\User'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getUserOptions()
    {
        return [];//['' => 'Выберите пользователя'] + User::select('id')->selectRaw("concat(`first_name`, ' ', `last_name`, ' (', `login` , ')') as `name`")->lists('name', 'id');
    }
}