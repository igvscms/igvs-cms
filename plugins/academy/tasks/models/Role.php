<?php namespace Academy\Tasks\Models;

use Model;
use Lang;

use Academy\Tasks\Models\StatusChange;
use Academy\Tasks\Models\User;

class Role extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    public $table = 'academy_tasks_roles';

    protected $guarded = ['*'];
    public $timestamps = false;

    protected $fillable = [];

    protected $rules =[
        'name' => 'required',
    ];

    public $hasOne = [];
    public $hasMany = [
        'statuses' => ['Academy\Tasks\Models\StatusChange', 'key' => 'role_id'],
    ];
    public $belongsTo = [
        'project' => 'Academy\Tasks\Models\Project',
    ];
    public $belongsToMany = [
        'users' => [
            'Academy\Tasks\Models\User',
            'table' => 'academy_tasks_users_roles',
            'key' => 'role_id',
            'otherKey' => 'user_id',
        ],
        'rights' => [
            'Academy\Tasks\Models\Right',
            'table' => 'academy_tasks_role_rights',
            'key' => 'role_id',
            'otherKey' => 'right_id',
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $status_table = [];//статусы

    public function status_table($type_id)
    {
        $statuses = [];
        foreach(StatusChange::where('role_id', $this->id)->where('type_id', $type_id)->get() as $status)
            $statuses[$status->status_id][$status->status_new_id] = true;

        return $statuses;
    }

    public $type = null;

    public function afterSave()
    {
        $insert = [];

        foreach($this->status_table as $status_id => $statuses)
            foreach($statuses as $status_new_id)
            {
                $insert[] = [
                    'role_id' => $this->id,
                    'status_id' => $status_id == 'null' ? null : $status_id,
                    'status_new_id' => $status_new_id,
                    'type_id' => $this->type,
                ];
            }

        StatusChange::where('role_id', $this->id)->where('type_id', $this->type)->delete();
        StatusChange::insert($insert);
    }

    public function canStsatusChange($status_id)
    {
        return !!$this->statuses()->where('status_id', $status_id)->first();
    }

    public function getIdOptions($context, $selects)
    {
        if(isset($selects->user_id)) {

            $user = User::find($selects->user_id);
            return [$user->id => $user->first_name . ' ' . $user->last_name . ' (' . $user->login .')'];
        }
        
        elseif(isset($selects->scalar)) {

            $user = User::find($selects->scalar);
            return [$user->id => $user->first_name . ' ' . $user->last_name . ' (' . $user->login .')'];
        }
        return [];
    }

    public function getRolesForStatus($status_id)
    {
        if(!$this->statuses()->where('status_id', null)->where('status_new_id', $status_id)->first())//если текущая роль не может создавать задачу с таким статусом
            return;

        $roles = $this->project()->first()->roles()->lists('id');//все роли текущего проекта
        $roles = StatusChange::where('status_id', $status_id)->whereIn('role_id', $roles)->lists('role_id');//роли, которые могут изменять данный статус
        $roles = $this::whereIn('id', $roles)->lists('name');

        return implode(',',$roles);
    }

    public function beforeCreate()
    {
        if($last = $this::where('project_id', $this->project_id)->orderBy('sort_order', 'desc')->first())
            $this->sort_order = $last->sort_order + 1;
    }

    public static function getRolesByProjectIdAndUserId($project_id, $user_id)
    {
        $role = new Role();
        $roleTable = $role->getTable();
        $userRolesTable = 'academy_tasks_users_roles';
        $user = new User();
        $userTable = $user->getTable();

        return $user->join($userRolesTable, "$userRolesTable.user_id", '=', "$userTable.id")
            ->where("$userTable.project_id", $project_id)
            ->where("$userTable.user_id", $user_id)
            ->groupBy("$userRolesTable.role_id")
            ->lists("$userRolesTable.role_id");
    }
}