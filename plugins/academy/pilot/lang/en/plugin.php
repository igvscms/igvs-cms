<?php return [
    'name' => 'PILOT',
    'settings' => [
        'users' => [
            'label' => 'Users',
            'description' => 'Advanced user management with external fields'
        ]
    ]
];