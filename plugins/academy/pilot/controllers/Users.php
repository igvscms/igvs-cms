<?php namespace Academy\Pilot\Controllers;

use Backend\Classes\Controller;
use System\Classes\SettingsManager;

class Users extends Controller
{
    public $implement = [
        //'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Academy.System.Behaviors.CrudController'
    ];

    public $requiredPermissions = ['academy.pilot.users'];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        \BackendMenu::setContext('October.System', 'system', 'settings');
        SettingsManager::setContext('Academy.Pilot', 'users');
    }

    public function listExtendQuery($query, $definition = null)
    {
        $query->rightJoin('backend_users', 'backend_users.id', '=', 'igvs_courses_user_pilot.user_id')
            ->selectRaw('IF(igvs_courses_user_pilot.id,igvs_courses_user_pilot.user_id,backend_users.id)user_id');
    }
}
