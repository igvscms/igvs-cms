<?php namespace Academy\Pilot;

use Backend;
use System\Classes\PluginBase;
use Lang;
use Event;

use Academy\System\ListWidgets\CustomFields;
use Academy\System\ListWidgets\File;

/**
 * courses Plugin Information File
 */
class Plugin extends PluginBase
{
    public function registerSettings()
    {
        return [
            'users' => [
                'category'    => lang::get('academy.pilot::plugin.name'),
                'label'       => lang::get('academy.pilot::plugin.settings.users.label'),
                'description' => lang::get('academy.pilot::plugin.settings.users.description'),
                'icon'        => 'icon-plane',
                'url'         => Backend::url('academy/pilot/users'),
                'order'       => 500,
                'permissions' => ['academy.pilot.users'],
                'keywords'    => ''
            ]
        ];
    }

    public function pluginDetails()
    {
        return [
            'name'        => 'Pilot',
            'description' => 'Pilot',
            'author'      => 'academy',
            'icon'        => 'icon-connectdevelop'
        ];
    }

    public function registerPermissions()
    {
        return [
            'academy.pilot.users' => [
                'tab' => 'Pilot',
                'label' => 'Pilot users control',
            ],
        ];
    }
}