<?php namespace Academy\Pilot\Models;

class User extends \Model
{
    public $table = 'igvs_courses_user_pilot';
    protected $guarded = ['*'];
    // protected $fillable = [
    //     'user_id',
    //     'pilot_user_id',
    //     'instance_id',
    //     'cluster_id',
    //     'poo_id',
    //     'email',
    // ];

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => 'Backend\Models\User',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}