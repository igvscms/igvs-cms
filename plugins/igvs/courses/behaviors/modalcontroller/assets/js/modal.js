/**
 * Created by alvaro on 26/10/15.
 */
+function ($) {
    "use strict";

    var ManageModal = function () {

        this.update = function (params) {
            var newPopup = $('<a />');

            newPopup.popup({
                handler: 'onUpdateForm',
                extraData: params
            })
        };

        this.preview = function (params) {
            var newPopup = $('<a />');

            newPopup.popup({
                handler: 'onPreview',
                extraData: params
            })
        };

        this.create = function (params) {
            var newPopup = $('<a />');
            newPopup.popup({
                handler: 'onCreateForm',
                extraData: params
            });

            return false;
        }

    };

    $.manageModal = new ManageModal;

}(window.jQuery);

