<?php namespace Igvs\courses\Behaviors;

    use Backend\Classes\ControllerBehavior;

    class ModalController extends ControllerBehavior
    {

        /**
         * Behavior ModalController constructor.
         *
         * @param \Backend\Classes\Controller $controller
         */
        public function __construct($controller)
        {
            parent::__construct($controller);

            if(method_exists($this->controller, 'overrideAddAssets')){
                $this->controller->overrideAddAssets();
            } else {
                $this->addAssets();
            }
        }

        /**
         * Add js modal asset
         */
        public function addAssets()
        {
            $this->addJs('js/modal.js');
        }

        public function index_onPreview($options = [])
        {
            $trans = isset($options['trans']) ? $options['trans'] : 'preview_title';
            $this->vars['title'] = trans($this->getTransString($trans));

            $this->controller->asExtension('FormController')
                ->preview(post('record_id'), isset($options['context']) ? $options['context'] : null);

            return $this->makePartial('preview_form');
        }

        /**
         * Make the modal form for create record
         *
         * @return mixed
         * @throws \SystemException
         */
        public function index_onCreateForm($options = [])
        {
            $trans = isset($options['trans']) ? $options['trans'] : 'create_title';

            $this->controller->asExtension('FormController')->create(isset($options['context']) ? $options['context'] : null);
            $this->vars['title'] = trans($this->getTransString($trans));

            if(method_exists($this->controller, 'overrideOnCreateFormPartial')){
                return $this->controller->overrideOnCreateFormPartial();
            }

            return $this->makePartial('create_form');
        }


        /**
         * After create record, refresh the index list
         *
         * @return mixed
         */
        public function index_onCreate()
        {
            $this->controller->asExtension('FormController')->create_onSave();

            return $this->controller->listRefresh();
        }

        /**
         * Make the modal form for update record
         *
         * @return mixed
         * @throws \SystemException
         */
        public function index_onUpdateForm($options = [])
        {
            $trans = isset($options['trans']) ? $options['trans'] : 'update_title';

            $this->controller->asExtension('FormController')->update(post('record_id'), isset($options['context']) ? $options['context'] : null);
            $this->vars['recordId'] = post('record_id');
            $this->vars['title'] = trans($this->getTransString($trans));

            if(method_exists($this->controller, 'overrideOnUpdateFormPartial')){
                return $this->controller->overrideOnUpdateFormPartial();
            }

            return $this->makePartial('update_form');
        }

        /**
         * After update record, refresh the index list
         *
         * @return mixed
         */
        public function index_onUpdate()
        {
            $this->controller->asExtension('FormController')->update_onSave(post('record_id'));

            return $this->controller->listRefresh();
        }

        /**
         * @param null|string $trans
         * @return string
         */
        protected function getTransString( $trans = null ) {
            if(method_exists($this->controller, 'overrideGetTransString')){
                return $this->controller->overrideGetTransString($trans);
            }

            list($author, $plugin, $page, $controller) = explode('\\', get_class($this->controller) );
            $string = strtolower(join('.', [$author, $plugin]) . '::lang.' . strtolower( str_singular($controller) ));

            if(is_string($trans)) {
                $string .= '.' . trim(strtolower($trans));
            }

            return $string;
        }
    }