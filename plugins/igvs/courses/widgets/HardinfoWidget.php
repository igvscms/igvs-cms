<?php namespace Igvs\Courses\Widgets;

use Backend\Classes\ReportWidgetBase;
use Config;
use Storage;

class HardinfoWidget extends ReportWidgetBase
{
    public $defaultAlias = 'HardinfoWidget';

    public function widgetDetail()
    {
        return [
            'name' => 'Hardinfo Widget',
            'description' => ''
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function init()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('widget');
    }

    /**
     * Prepares the list data
     */
    public function prepareVars()
    {
        $chart = [
            'Free' => disk_free_space(Config::get('igvs.courses::modulesContent.path')),
            'Courses' => $this->dirSize(Config::get('igvs.courses::modulesContent.path')),
            'SCORM' => $this->dirSize(Config::get('igvs.courses::scorm.arhive') . '/..')
        ];

        $size = 0;
        foreach ($chart as $k => $v) {
            $chart[$k] = $this->bytesToGb($v);
            $size += $v;
        }

        $this->vars['chart'] = [
            'centerText' => $this->bytesToGb($size),
            'data' => $chart
        ];
    }

    function dirSize($directory) {
        $size = 0;

        $output = exec('du -sb ' . $directory . ' | cut -f1');
        return intval($output);

        /*
        foreach(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory)) as $file){
            $size+=$file->getSize();
        }

        return $size;
        */
    }

    function bytesToGb($bytes, $precision = 2)
    {
        return round ($bytes / pow(2, 30), $precision);
    }

    /**
     * {@inheritDoc}
     */
    protected function loadAssets()
    {
    }

    public function makePartialNode($id)
    {
    }

}