/*
 * This file has been compiled from: /modules/system/lang/en/client.php
 */
if ($.oc === undefined) $.oc = {}
if ($.oc.langMessages === undefined) $.oc.langMessages = {}

var widgetCategoryDefaultLang = document.documentElement.getAttribute('lang');

$.oc.langMessages[widgetCategoryDefaultLang] = $.extend(
    $.oc.langMessages[widgetCategoryDefaultLang] || {},
    {
        'topic':{
            'message':{
                'delete':'Do you really want to delete this topic?',
            },
            'command':{
                'copy': 'Copy topic',
                'pasteDown': 'Add a topic after the current',
                'pasteUp': 'Add a topic before the current',
            },
        },
        'act':{
            'message':{
                'delete':'Do you really want to delete this i-Act?',
            },
            'command':{
                'add':'Add i-Act',
            },
        },
        'module':{
            'message':{
                'delete':'Do you really want to delete this module?',
                'change_template': {
                    'ct0': 'module template is modified!',
                    'ct1': 'You just change the module template.',
                    'ct2': 'Be careful, it can drastically affect the designer and visualizer.',
                },
                'change_json': {
                    'cj0': 'Done editing',
                    'cj1': 'More than 5 minutes, you have not changed the content',
                    'cj2': 'As long as you stay on this page, other users do not have access to it to complete the editing of',
                    'cJ3': 'Continue editing',
                    'cj4': 'This content is already editing',
                },
            },
            'command':{
                'add':'Add Module',
            },
        },
        'default':{
            'command':{
                'paste':'Paste',
                'preview':'Preview',
                'delete':'Delete',
                'copy':'Copy',
                'view_images_content':'Images content',
                'copy_module_code': 'Copy module code',
                'get_parent_history': 'Open Parent Module History'
            },
        },
        'message': {
            'module_code_copied': 'Module code copied'
        }
    }
);