

FocEl = 0;
function clickByModule(id) {
    if (FocEl != 0) {

        $('#'+FocEl)
            .parent()
            .parent()
            .removeClass('active');
    }
    FocEl = id;
    if(countClick > 19) {
        window.location.href = '#'+FocEl;
        location.reload();
    }
    else {
        countClick++;
    }
    $('#'+ id).parent().parent().addClass('active');
}
countClick = 0;
$(document).ready(function() {

    renderPreview();

    // edit topic && act
    var selector = '\
        li[data-depth=0]>div.record a.colontitle,\
        li[data-depth=1]>div.record a.colontitle';

    $('#tree-list').on('click', selector, function() {
        var data = $(this).closest('li').data();
        $.manageModal.update({record_id: data.recordId});
    });

    // 
    $("body").on("click", "[data-control='chb-build-module']", function() {

        var $this = $(this);
        var dataModule = $this.closest('li').data();
        var dataBuild = $this.data();

        $('i', $this).toggleClass('icon-square-o', dataBuild.value);
        $('i', $this).toggleClass('icon-check-square', dataBuild.value);

        var value = dataBuild.value ? 0 : 1;
        $this.data('value', value);

        $.request('categoriesWidget::onAddModulesToBuild', {
            data: {
                courseId: dataModule.courseId,
                modules: [dataModule.recordId],
                value: dataBuild.value
            }
         });

    });

    // добавить модули категории в билд
    $("body").on("click", "[data-control='chb-build-category']", function() {

        var $this = $(this);
        var $li = $this.closest('li')

        var dataCtg = $li.data();
        var dataBuild = $this.data();

        $('i', $this).toggleClass('icon-square-o', dataBuild.value);
        $('i', $this).toggleClass('icon-check-square', dataBuild.value);

        var value = dataBuild.value ? 0 : 1;
        $this.data('value', value);

        var modules = [];
        $li.find("[data-control='chb-build-module']").each(function () {
            modules[modules.length] = $(this).closest('li').data().recordId;
        });

        $.request('categoriesWidget::onAddModulesToBuild', {
            data: {
                courseId: dataCtg.courseId,
                modules: modules,
                value: value
            }
        });
    });

    $("#current-build-id").change(function() {
        var $this = $(this);

        $.request('categoriesWidget::onChangeCurrentBuild', {
            data: {
                currentBuildId: $this.val(),
                courseId: $this.data().courseId
            }
        });

        renderPreview();
    });

    $("#current-category-id").change(function() {
        var $this = $(this);

        $.request('categoriesWidget::onChangeCurrentCategory', {
            data: {
                currentCategoryId: $this.val(),
                courseId: $this.data().courseId
            }
        });

        renderPreview();
    });

    function renderPreview() {
        var val         = $('#current-build-id').val(),
            course_id   = $('#toolbar-course-id').val();

        if(typeof(val) == 'undefined') return;
        if (!val.length) {
            $('#preview').attr('href', '/preview/' + course_id);
        } else {
            $('#preview').attr('href', '/preview/' + val + '/' + course_id);
        }
    }

    $(document).on('click', 'li[data-depth=2]>div.record a.colontitle', function() {
        clickByModule($(this).attr('id'));
    });
    if(req = document.getElementById(window.location.hash.toString().substring(1)))
        req.click();
});


// context-menus
(function() {

    // copy/paste
    var module, act;

    // handler
    var handler = function (key, options) {

        var data = $(this).parent('li').data();

        if (operations[key])
            operations[key](data);
    };

    // operations
    var operations = {

        // topics
        deleteTopic: function(data) {
            $.request('categoriesWidget::onDeleteCategory', {
                data: {
                    categoryId: data.recordId,
                    courseId: data.courseId
                },
                confirm: $.oc.lang.get('topic.message.delete')
            });
        },

        // acts
        addAct: function(data) {
            $.manageModal.create({
                course_id: data.courseId,
                parent_id: data.recordId
            });
        },

        copyAct: function(data) {
            localStorage.setItem('copyActId', data.recordId);
        },

        pasteAct: function(data) {

            var actId = localStorage.getItem('copyActId');

            if (!actId)
                return;

            $.request('categoriesWidget::onPasteAct', {
                data: {
                    courseId: data.courseId,
                    categoryId: data.recordId,
                    actId: actId
                }
            });
        },

        pasteTopicUp: function(data) {

            var topicId = localStorage.getItem('copyTopicId');

            if (!topicId)
                return;

            $.request('categoriesWidget::onPasteTopicUp', {
                data: {
                    categoryId: data.recordId,
                    topicId: topicId
                }
            });
        },

        pasteTopicDown: function(data) {

            var topicId = localStorage.getItem('copyTopicId');

            if (!topicId)
                return;

            $.request('categoriesWidget::onPasteTopicDown', {
                data: {
                    categoryId: data.recordId,
                    topicId: topicId
                }
            });
        },

        copyTopic: function(data) {
            localStorage.setItem('copyTopicId', data.recordId);
        },

        deleteAct: function(data) {
            $.request('categoriesWidget::onDeleteCategory', {
                data: {
                    categoryId: data.recordId,
                    courseId: data.courseId
                },
                confirm: $.oc.lang.get('act.message.delete')
            });
        },

        // modules
        addModule: function(data) {
            var config_author_code = $('#config_author_code').val();
            if (typeof(config_author_code) === 'undefined') {
                config_author_code = 'igvs';
            }

            $.crud(config_author_code + '/courses/contents/crudcreate',{
                success:function(data){
                    $.request('onRefreshListTree',{
                        data: {
                            courseId: data.model.course_id
                        }
                    })
                },
                dataModel: {
                    course_id: data.courseId,
                    category_id: data.recordId
                },
                size: data.size
            })

        },

        copyModule: function(data) {
            localStorage.setItem('copyModuleId', data.recordId);
        },

        pasteModule: function(data) {

            var moduleId = localStorage.getItem('copyModuleId');

            if (!moduleId)
                return;

            $.request('categoriesWidget::onPasteModule', {
                data: {
                    courseId: data.courseId,
                    actId: data.recordId,
                    moduleId: moduleId
                }
            });
        },

        deleteModule: function(data) {
            $.request('categoriesWidget::onDeleteModule', {
                data: {
                    moduleId: data.recordId,
                    courseId: data.courseId
                },
                confirm: $.oc.lang.get('module.message.delete')
            });
        },

        previewTopic: function(data) {
            var build_id = $('#current-build-id').val();
            var url = '/preview/topic/' + data.courseId + '/' + data.recordId;

            if (build_id.length) {
                url = '/preview/topic/' + build_id + '/' + data.courseId + '/' + data.recordId;
            }

            window.open(url);
        },

        previewModule:  function(data) {
            var url = '/preview/' + data.courseId + '#' + data.recordId;
            window.open(url);
        },

        imagesContent:  function(data) {
            var url = '/storage/app/igvs/htmlContentList/' + data.recordId + '/' + data.recordId + '_topic.html';
            window.open(url, '_blank');
        },

        copyModuleCode: function (data) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";

            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }

            var text = typeof(data.moduleCode) != 'undefined' ? data.moduleCode : '';
            target.textContent = text;

            // select the content
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }

            // clear temporary content
            target.textContent = "";

            $.oc.flashMsg({'text': $.oc.lang.get('default.message.module_code_copied') + ': ' + text});

            return succeed;
        }
    };

    $.contextMenu({
        selector: '#tree-list li[data-depth=0]>div.record', 
        build: function($trigger, e) {
            return {
                callback: handler,
                items: {
                    "addAct": {name: $.oc.lang.get('act.command.add'), icon: "add", disabled: disabledCategory},
                    "pasteAct" : {name: $.oc.lang.get('default.command.paste'), icon: "paste", disabled: disabledCategory},
                    "previewTopic" : {name: $.oc.lang.get('default.command.preview'), icon: "eye"},
                    "sep1": "---------",
                    "deleteTopic": {name: $.oc.lang.get('default.command.delete'), icon: "delete", disabled: disabledDelCategory},
                    "sep2": "---------",
                    "imagesContent": {name: $.oc.lang.get('default.command.view_images_content'), icon: "picture", disabled: isShortVersion},
                    "sep3": "---------",
                    "copyTopic" : {name: $.oc.lang.get('topic.command.copy'), icon: "copy"},
                    "pasteTopicUp": {name: $.oc.lang.get('topic.command.pasteUp'), icon: "paste", disabled: disabledPasteTopic},
                    "pasteTopicDown": {name: $.oc.lang.get('topic.command.pasteDown'), icon: "paste", disabled: disabledPasteTopic},
                }
            };
        }
    });

    $.contextMenu({
        selector: '#tree-list li[data-depth=1] > div.record', 
        build: function($trigger, e) {
            return {
                callback: handler,
                items: {
                    "addModule": {name: $.oc.lang.get('module.command.add'), icon: "add", disabled: disabledAddModule},
                    "copyAct" : {name: $.oc.lang.get('default.command.copy'), icon: "copy"},
                    "pasteModule" : {name: $.oc.lang.get('default.command.paste'), icon: "paste", disabled: disabledAddModule},
                    "sep1": "---------",
                    "deleteAct": {name: $.oc.lang.get('default.command.delete'), icon: "delete", disabled: disabledDelCategory}
                }
            };
        }
    });

    $.contextMenu({
        selector: '#tree-list li[data-depth=2] > div.record', 
        build: function($trigger, e) {
            return {
                callback: handler,
                items: {
                    "copyModule" : {name: $.oc.lang.get('default.command.copy'), icon: "copy"},
                    "previewModule" : {name: $.oc.lang.get('default.command.preview'), icon: "eye"},
                    "sep1": "---------",
                    "deleteModule": {name: $.oc.lang.get('default.command.delete'), icon: "delete", disabled: disabledDelModule},
                    "copyModuleCode": {name: $.oc.lang.get('default.command.copy_module_code'), icon: "fa-link"}
                }
            };
        }
    });

    function disabledAddModule(key, opt)
    {
        var data = $("#reorder-records").data();
        var el = this.parent('li').closest('li.category');

        // console.log('disabledAddModule.readonly - ' + readonly.call(this, key, opt));
        // console.log('disabledAddModule.disabledAddModule - ' + data.disabledAddModule);

        return readonly.call(this, key, opt)
            || disabledCategory.call(el, key, opt)
            || data.disabledAddModule;
    }

    function disabledDelModule(key, opt)
    {
        var data = $("#reorder-records").data();
        var el = this.parent('li').closest('li.category');

        // console.log('disabledDelModule.readonly - ' + readonly.call(this, key, opt));
        // console.log('disabledDelModule.disabledCategory - ' + disabledCategory.call(el, key, opt));
        // console.log('disabledDelModule.disabledDelModule - ' + data.disabledDelModule);

        return readonly.call(this, key, opt)
            || disabledCategory.call(el, key, opt)
            || data.disabledDelModule;
    }

    function disabledDelCategory(key, opt)
    {
        return disabledDelModule.call(this, key, opt)
            || disabledCategory.call(this, key, opt);
    }

    function readonly(key, opt) {
        if (typeof(this.closest('li').data('readonly_without_basecourse')) === 'boolean') {

            return this.closest('li').data('readonly') === true
                && this.closest('li').data('readonly_without_basecourse') === true;

        } else {

            return this.closest('li').data('readonly') === true;
            
        }
    }

    function disabledCategory(key, opt) {
        var data = this.closest('li').data();
        return (data.readonly || !data.editable);
    }

    function disabledPasteTopic() {
        return !localStorage.getItem('copyTopicId');
    }

    function isShortVersion(key, opt) {
        return this.closest('li').data('isshort') === true;
    }

})();