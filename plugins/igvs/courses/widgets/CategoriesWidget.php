<?php namespace Igvs\Courses\Widgets;

use Academy\Tasks\Models\Task;

use Backend\Classes\WidgetBase;

use Igvs\Courses\Models\Build;
use Igvs\Courses\Models\BuildModuleContent;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\RubricatorHistory;

use App;
use BackendAuth;
use Config;
use Exception;
use Session;

class CategoriesWidget extends WidgetBase
{
    use \Igvs\Courses\Traits\CheckAccess;

    public $defaultAlias = 'categoriesWidget';
    public $canCreateAct = false;
    public $canCreateModule = false;

    public function widgetDetail()
    {
        return [
            'name' => 'Categories Widget',
            'description' => ''
        ];
    }

    public function init()
    {
        if (isset($_COOKIE['tree_spoiler'])) {
            if (is_array($_COOKIE['tree_spoiler'])) {
                foreach ($_COOKIE['tree_spoiler'] as $id => $value) {
                    setcookie("tree_spoiler[$id]", $value, time() - 3600, '/');
                    setcookie("tree_spoiler%5B$id%5D", $value, time() - 3600, '/');
                }
            }
            setcookie("tree_spoiler", "", time() - 3600, '/');
        }
    }

    public function prepareVars()
    {
//        ModuleContent::first()->read_comments()->count();
        $lang = App::getLocale();
        $lang = !in_array($lang, ['ru', 'en']) ? 'en' : $lang;

        $this->addJs('js/lang/lang.'. $lang .'.js');

        $course = $this->controller->vars['course'];
        if (!Session::get('igvs.courses.closethemewarning.' . $course->id)) {
            $this->vars['no_theme_modules'] = ModuleContent::getModulesClearThemes($course->id, $course->theme);
        }
        $this->vars['coursePermissions'] = [
            'AddModuleContent' => $this->checkAccessOperation($course->id, 'AddModuleContent'),
            'DelModuleContent' => $this->checkAccessOperation($course->id, 'DelModuleContent'),
            'SortModuleContent' => $this->checkAccessOperation($course->id, 'SortModuleContent'),
        ];

        $this->vars['course_id'] = $course->id;

        $this->vars['records'] = $this->getFirstBuilds($course);

    }

    public function filterCategory($categories, $category_id)
    {
        foreach ($categories as $v) {

            if ($v->item->id == $category_id
            || $this->filterCategory($v->children, $category_id))
                return true;
        }

        return false;
    }

    public function buildTree($categories, $parent = 0)
    {
        $result = [];
        foreach ($categories as $category) {
            if ($category->parent_id == $parent) {

                $result[] = (object) [
                    'item' => $category,
                    'children' => $this->buildTree($categories, $category->id)
                ];
            }
        }
        return $result;
    }

    protected function loadAssets()
    {
        $this->addJs('js/igvs-treelist.js');
        $this->addJs('jquery-contextMenu/dist/jquery.contextMenu.js');
        $this->addCss('jquery-contextMenu/dist/jquery.contextMenu.css');
        $this->addJs('js/main.js?v=4');
        $this->addCss('css/main.css');
    }

    public function getFirstBuilds($course)
    {
        $this->vars['course'] = $course;
        $this->vars['currentBuildId'] = Session::get('categoriesWidget.currentBuildId.' . $course->id);
        $this->vars['currentCategoryId'] = Session::get('categoriesWidget.currentCategoryId.' . $course->id);
        $this->vars['filterRubricator'] = Session::get('categoriesWidget.filterRubricator');//.' . $course->id);

        $this->vars['course_id'] = $course->id;

        $this->vars['coursePermissions'] = [
            'AddModuleContent' => $this->checkAccessOperation($course->id, 'AddModuleContent'),
            'DelModuleContent' => $this->checkAccessOperation($course->id, 'DelModuleContent'),
            'SortModuleContent' => $this->checkAccessOperation($course->id, 'SortModuleContent'),
        ];

        switch($this->vars['filterRubricator']) {

            case 'tasks':
                $user = null;
                break;
            case 'my_tasks':
                if($user = BackendAuth::getUser())
                    break;
            default:
                return $this
                    ->buildTree(Category::with('contents')
                    ->where('course_id', $course->id)
                    ->orderBy('sort')
                    ->get());
        }

        $testUser = function($task) use($user)
        {
            if(isset($user))
                $task->where('user_id', $user->id)
                    ->orWhere('responsible_id', $user->id);
        };

        $needs = ['topic_id', 'act_id', 'module_id'];
        foreach ($needs as $need) {
            $$need = Task::where('course_id', $course->id)
                ->whereNotNull($need)
                ->where($testUser)
                ->groupBy($need)
                ->lists($need);
        }

        $categories = Category::with([
            'contents' => function($contents) use($module_id)
            {
                $contents->whereIn('id', $module_id);
            }
        ])
        ->whereIn('id', array_merge($topic_id, $act_id))
        ->orderBy('sort')
        ->get();

        return $this->buildTree($categories);
    }

    public function makePartialTree($course_id)
    {
        if (!$this->checkAccessCourse($course_id)) {
            throw new Exception('Access denied');
        }
        return [
            '#tree-list' => $this->makePartial('tree', [
                'records' => $this->getFirstBuilds(Course::find($course_id)),
            ])
        ];
    }

    public function onRefreshListTree()
    {
        if (!$this->checkAccessCourse(post('courseId')))
            throw new Exception('Access denied');

        //return $this->makePartialTree(post('courseId'));
        $this->controller->onUpdateRubricator(post('courseId'));
    }

    public function onMoveCategory()
    {
        $sourceNode = Category::find(post('sourceNode'));

        if ($sourceNode && $sourceNode->course_id != post('courseId'))
            throw new Exception('You can not change the nesting');

        $targetNode = post('targetNode') ? Category::find(post('targetNode')) : null;

        // check access
        $access = $this->checkAccessOperation($sourceNode->course_id, 'AllowEditCategories')
            && $this->checkAccessOperation($targetNode->course_id, 'AllowEditCategories');

        if (!$access)
            throw new Exception('Access denied');

        // check read-only
        if ($sourceNode->isReadonly() || $targetNode->isReadonly())
            throw new Exception('Category is read-only');

        if ($sourceNode == $targetNode)
            return;

        // сохранять id следующего элемента если он был последним. сохранять 0
        $next = Category::where('parent_id', $sourceNode)
            ->select('id')
            ->where('sort', '>', $sourceNode->sort)
            ->orderBy('sort')
            ->first();

        $value = $next ? $next->id : 0;
        $value .= ' '. intval($sourceNode->parent_id);

        switch (post('position')) {
            case 'before':
                $sourceNode->moveBefore($targetNode);
                break;
            case 'after':
                $sourceNode->moveAfter($targetNode);
                break;
            case 'child':
                $sourceNode->makeChildOf($targetNode);
                break;
            default:
                $sourceNode->makeRoot();
                break;
        }

        // type = 1 - перемещение
        RubricatorHistory::create([
            'value' => $value,
            'type' => 1,
            'category_id' => $sourceNode->id
        ]);

        //return $this->makePartialTree(post('courseId'));
        $this->controller->onUpdateRubricator(post('courseId'));
    }

    public function onPasteAct()
    {
        $source = Category::find(post('actId'));
        $target = Category::find(post('categoryId'));

        // check access
        $access = $this->checkAccessCourse($source->course_id)
            && $this->checkAccessOperation($target->course_id, 'AllowEditCategories');

        if (!$access)
            throw new Exception('Access denied');

        // check readonly
        if ($target->isReadonly())
            throw new Exception('Category is read-only');

        $target->copyCategoryTo($source);
        //return $this->makePartialTree(post('courseId'));
        $this->controller->onUpdateRubricator(post('courseId'));
    }

    public function onPasteTopicDown()
    {
        $source = Category::find(post('topicId'));//что вставляем
        $target = Category::find(post('categoryId'));//рядом с чем вставляем

        // check access
        $access = $this->checkAccessCourse($source->course_id)
            && $this->checkAccessOperation($target->course_id, 'AllowEditCategories');

        if (!$access) {
            throw new Exception('Access denied');
        }
        $target->course->addCategory($source, 'new', $target, null, null, false);
        $this->controller->onUpdateRubricator($target->course_id);
    }

    public function onPasteTopicUp()
    {
        $source = Category::find(post('topicId'));//что вставляем
        $target = Category::find(post('categoryId'));//рядом с чем вставляем

        // check access
        $access = $this->checkAccessCourse($source->course_id)
            && $this->checkAccessOperation($target->course_id, 'AllowEditCategories');

        if (!$access) {
            throw new Exception('Access denied');
        }
        $target->course->addCategory($source, 'new', $target);
        $this->controller->onUpdateRubricator($target->course_id);
    }

    public function onDeleteCategory()
    {
        if ($model = Category::find(post('categoryId'))) {
            RubricatorHistory::create(['value'=>$model->code.': '.$model->name,'type'=>3,'category_id'=>$model->id]);//type = 3 удаление
            if (!$this->checkAccessOperation($model->course_id, 'AllowEditCategories')
                || !$this->checkAccessOperation($model->course_id, 'DelModuleContent')) {
                throw new Exception('Access denied');
            }
            if ($model->isReadonly()) {
                throw new Exception('Module is read-only');
            }
            $model->delete();
        }
        //return $this->makePartialTree(post('courseId'));
        $this->controller->onUpdateRubricator(post('courseId'));
    }

    public function onMoveModule()
    {
        $model = ModuleContent::find(post('sourceNode'));

        if ($model && $model->course_id != post('courseId')) {
            throw new Exception('You can not change the nesting');
        }

        // check access
        if (!$this->checkAccessOperation($model->course_id, 'SortModuleContent')) {
            $this->controller->onUpdateRubricator(post('courseId'));
            //return $this->makePartialTree(post('courseId'));
            return;
        }

        switch (post('position')) {
            case 'before':
                $target = ModuleContent::find(post('targetNode'));
                $model->moveBefore($target);
                break;
            case 'after':
                $target = ModuleContent::find(post('targetNode'));
                $model->moveAfter($target);
                break;
            case 'child':
                $model->category_id = post('targetNode');
                $model->sort = 1;
                $model->save();
                break;
        }

        //return $this->makePartialTree(post('courseId'));
        $this->controller->onUpdateRubricator(post('courseId'));
    }

    public function onDeleteModule()
    {
        if (!$model = ModuleContent::find(post('moduleId'))) {
            return $this->makePartialTree(post('courseId'));
        }
        if (!$this->checkAccessOperation($model->course_id, 'DelModuleContent')
            || !$this->checkAccessOperation($model->course_id, 'AllowEditCategories')) {
            throw new Exception('Access denied');
        }

        if ($model->parent_id
            && $model->isReadonly()
            && $model->isReadonly(['disable_check_base_course' => true])) {

                throw new Exception ('Module is read-only');

        } elseif (!$model->parent_id
            && $model->isReadonly()) {

            throw new Exception ('Module is read-only');
        }

        $model->delete();
        $this->controller->onUpdateRubricator(post('courseId'));
    }

    public function onPasteModule()
    {
        $target = Category::find(post('actId'));
        $source = ModuleContent::find(post('moduleId'));

        // check access
        $access = $this->checkAccessOperation($target->course_id, 'AddModuleContent')
            && $this->checkAccessCourse($source->course_id);

        if (!$access) {
            throw new Exception ('Access denied');
        }

        // check is read-only
        if ($target->isReadonly()) {
            throw new Exception('Category is read-only');
        }

        $target->copyModuleTo($source);

        //return $this->makePartialTree(post('courseId'));
        $this->controller->onUpdateRubricator(post('courseId'));
    }

    public function onAddModulesToBuild()
    {
        if (!$this->checkAccessCourse(post('courseId')))
            throw new Exception('Access denied');

        if(!is_array($modules = post('modules'))) {
            //$this->controller->onUpdateRubricator(post('courseId'));
            return $this->makePartialTree(post('courseId'));
            return;
        }

        $build = Build::findOrFail(\Session::get('categoriesWidget.currentBuildId.' . (string) post('courseId')));

        if (post('value')) {
            $insert = [];
            foreach($modules as $v) {
                $insert[] = [
                    'module_content_id' => $v,
                    'build_id' => $build->id
                ];
            }
            BuildModuleContent::insert($insert);
        } else {
            BuildModuleContent::whereIn('module_content_id', $modules)
                ->where('build_id', $build->id)
                ->delete();
        }

        //$this->controller->onUpdateRubricator(post('courseId'));
        return $this->makePartialTree(post('courseId'));
    }

    public function onChangeCurrentBuild()
    {
        Session::put('categoriesWidget.currentBuildId.' . (string)post('courseId'), intval(post('currentBuildId')));
        //$this->controller->onUpdateRubricator(post('courseId'));
        return $this->makePartialTree(post('courseId'));
    }

    public function onChangeCurrentCategory()
    {
        Session::put('categoriesWidget.currentCategoryId.' . (string)post('courseId'), intval(post('currentCategoryId')));
        //$this->controller->onUpdateRubricator(post('courseId'));
        return $this->makePartialTree(post('courseId'));
    }

    public function onFilterRubricator()
    {
        Session::put('categoriesWidget.filterRubricator'/* . (int)post('courseId')*/, post('filterRubricator'));
        //$this->controller->onUpdateRubricator(post('courseId'));
        return $this->makePartialTree(post('courseId'));
    }

    public function onChangeTreeSpoiler()
    {
        Session::put('tree_spoiler.' . post('id'), post('status') !== 'false');
    }

    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('workspace');
    }
}
