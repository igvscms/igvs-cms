<?php namespace Igvs\Courses\Widgets;

use Db;
use Backend\Widgets\Filter;

/**
 * Filter Widget
 * Renders a container used for filtering things.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class LocalFilter extends Filter
{
    public function onFilterGetOptions()
    {
        $this->defineFilterScopes();

        $searchQuery = post('search');
        if (!$scopeName = post('scopeName')) {
            return;
        }

        $scope = $this->getScope($scopeName);
        $activeKeys = $scope->value ? array_keys($scope->value) : [];
        $available = $this->getAvailableOptions($scope, $searchQuery);

        if(is_array($available)) {
            $available_keys = [];
            foreach($available as $key => $value) {
                if(!isset($available_keys[$value]))
                    $available_keys[$value] = $key;
                else
                    $available_keys[$value].= ','.$key;
            }
            $available = array_flip($available_keys);
        }

        $active = $searchQuery ? [] : $this->filterActiveOptions($activeKeys, $available);

        return [
            'scopeName' => $scopeName,
            'options' => [
                'available' => $this->optionsToAjax($available),
                'active'    => $this->optionsToAjax($active),
            ]
        ];
    }

    public function applyScopeToQuery($scope, $query)
    {
        if (is_string($scope)) {
            $scope = $this->getScope($scope);
        }

        if (!$scope->value) {
            return;
        }

        $value = is_array($scope->value) ? array_keys($scope->value) : $scope->value;

        foreach($value as $key => $val) {
            $val = explode(',',$val);
            $value[$key] = $val[0];
            for($i=1;$i<count($val);$i++) {
                $value[] = $val[$i];
            }
        }

        /*
         * Condition
         */
        if ($scopeConditions = $scope->conditions) {
            if (is_array($value)) {
                $filtered = implode(',', array_build($value, function ($key, $_value) {
                    return [$key, Db::getPdo()->quote($_value)];
                }));
            }
            else {
                $filtered = Db::getPdo()->quote($value);
            }

            $query->whereRaw(strtr($scopeConditions, [':filtered' => $filtered]));
        }

        /*
         * Scope
         */
        if ($scopeMethod = $scope->scope) {
            $query->$scopeMethod($value);
        }

        return $query;
    }
}
