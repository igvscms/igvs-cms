<?php namespace Igvs\Courses\Widgets;

use Backend\Classes\WidgetBase;
use Igvs\Courses\Models\Member;
use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\MemberRole;
use Igvs\Courses\Models\UserPilot;
use Igvs\Courses\Models\User;

class DeveloperFilter extends WidgetBase
{
    public $scope;
    public $model;
    public $list;
    protected $defaultAlias = 'filerScopeText';

    public function init()
    {
        $this->fillFromConfig([
            'scope',
            'model',
            'list',
        ]);
        $this->scope->widgetAlias = $this->alias;
        $this->bindToController();
    }

    public function getColumnValue($value, $column, $record)
    {
        if (is_array($value) && count($value) == count($value, COUNT_RECURSIVE)) {
            $value = implode(', ', $value);
        }
        return htmlentities($value, ENT_QUOTES, 'UTF-8', false);
    }

    public function getValue()
    {
        return $this->getSession('value', []);
    }

    public function setValue($value)
    {
        $this->putSession('value', $value);
    }

    public function getType()
    {
        return $this->defaultAlias;
    }

    public function render()
    {
        $this->addCss('/plugins/academy/system/widgets/filterscopetext/css/academy.scope.css', 'core');
        $this->addJs('/plugins/academy/system/widgets/filterscopetext/js/academy.scope.js', 'core');
    }

    private static function intArray($array)
    {
        $result = [];
        foreach ($array as $value) {
            $result[] = (int) $value;
        }
        return $result;
    }

    public function apply($query)
    {
        if (!count($value = $this->getValue())) {
            return;
        }
        $course = new Course();
        $courses = $course->getTable();
        $member = new Member();
        $members = $member->getTable();
        $memberRole = new MemberRole();
        $memberRoles = $memberRole->getTable();
        $developer = self::DEVELOPER;

        $query->join(\DB::raw("(
            SELECT
                `course_id`
            FROM $members
            JOIN $memberRoles
                ON $memberRoles.member_id = $members.id
                AND $memberRoles.role_id = $developer
            WHERE $members.`user_id` IN (" . implode(',', self::intArray($value)) . ")
        )`members`"), 'members.course_id', '=', "$courses.id");
        return $query;
    }

    const DEVELOPER = 10;

    private function getAvailableOptions($scope, $searchQuery = null, $offset = 0, $limit = null, $valueFromIgnore = false)
    {
        $is_pilot_user = \BackendAuth::getUser()->hasPermission('igvs.courses.pilot_user');
        $member = new Member();
        $members = $member->getTable();
        $memberRole = new MemberRole();
        $memberRoles = $memberRole->getTable();
        $user = new User();
        $users = $user->getTable();

        $query = User::join($members, "$members.user_id", '=', "$users.id")
            ->join($memberRoles, "$memberRoles.member_id", '=', "$members.id")
            ->where("$memberRoles.role_id", self::DEVELOPER);

        if ($is_pilot_user) {
            $userPilot = new UserPilot();
            $userPilots = $userPilot->getTable();
            $developerName = "CONCAT(first_name, ' ', last_name, ' (', IF($userPilots.user_id, $userPilots.email, $users.email), ')')";
            $query->leftJoin($userPilots, "$userPilots.user_id", '=', "$users.id");
        }
        else {
            $developerName = "CONCAT(first_name, ' ', last_name, ' (', login, ')')";
        }
        $query->selectRaw("$developerName developer_name")
            ->addSelect("$users.id")
            ->distinct()
            ->searchWhere($searchQuery, \DB::raw($developerName));

        if (is_array($limit)) {
            $query->whereIn("$users.id", $limit);
        }
        else {
            $query->offset($offset)->limit($limit);
        }
        $query->lists('developer.developer_name', 'developer.id');

        return $query->lists('developer_name', "$users.id");
    }

    public function onGetOptions()
    {
        return ['options' => $this->getAvailableOptions($this->scope, input('search'), input('offset', 0), input('limit'))];
    }

    public function onGetDefaultOptions()
    {
        return ['options' => $this->getAvailableOptions($this->scope, null, 0, input('keys', []))];
    }

    public function onSetValue()
    {
        $this->setValue((array) input('value'));
    }
}