<?php namespace Igvs\Courses\FormWidgets;

use App;
use File;

use Backend\FormWidgets\RichEditor as RichEditorBase;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class RichEditor extends RichEditorBase
{

    protected $defaultAlias = 'richeditor';

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('$/../modules/backend/formwidgets/richeditor/partials/_richeditor.htm');
    }

    /**
     * @inheritDoc
     */
    protected function loadAssets()
    {
        $this->addCss('/modules/backend/formwidgets/richeditor/assets/css/richeditor.css', 'core');
        $this->addJs('/modules/backend/formwidgets/richeditor/assets/js/build-min.js', 'core');
        $this->addJs('/modules/backend/formwidgets/codeeditor/assets/js/build-min.js', 'core');

        if ($lang = $this->getValidEditorLang()) {
            $this->addJs('/plugins/igvs/courses/formwidgets/righeditor/lang/'.$lang.'.js', 'core');
        }
    }

    /**
     * Returns a valid language code for Redactor.
     * @return string|mixed
     */
    protected function getValidEditorLang()
    {
        $locale = App::getLocale();

        // English is baked in
        if ($locale == 'en') {
            return null;
        }

        $locale = str_replace('-', '_', strtolower($locale));
        $path = base_path('modules/backend/formwidgets/richeditor/assets/vendor/froala/js/languages/'.$locale.'.js');

        return File::exists($path) ? $locale : false;
    }
}
