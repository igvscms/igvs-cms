<?php
    return [
        'sitePath' => dirname(__FILE__) . '/../../../..',
        'resourcesPath' => dirname(__FILE__) . '/../resources',
        'modulesContent' => [
            'path' => dirname(__FILE__) . '/../../../../storage/app/igvs/module-content',
            'baseUrl' => '/storage/app/igvs/module-content'
        ],
        'modulesRelease' => [
            'path' => dirname(__FILE__) . '/../resources/modules',
            'baseUrl' => '/plugins/igvs/courses/resources/modules'
        ],
        'tmpModulesContent' => [
            'path' => dirname(__FILE__) . '/../../../../storage/app/igvs/htmlContentList',
            'logo' => dirname(__FILE__) . '/../../../../plugins/igvs/courses/assets/images/igvs-logo.png'
        ],
        'shell' => [
            'path' => dirname(__FILE__) . '/../resources/modules/shell',
            'baseUrl' => '/plugins/igvs/courses/resources/modules/shell'
        ],
        'html-module' => [
            'path' => dirname(__FILE__) . '/../resources/html-module',
            'baseUrl' => '/plugins/igvs/courses/resources/html-module'
        ],
        'scorm' => [
            'scormfiles' => dirname(__FILE__) . '/../resources/scormfiles',
            'tmp' => dirname(__FILE__) . '/../../../../storage/app/igvs/scorm/tmp',
            'arhive' => dirname(__FILE__) . '/../../../../storage/app/igvs/scorm/arhive',
            'url' => '/storage/app/igvs/scorm/arhive'
        ],
        'prepareForFtp' => [
            'basePath' => dirname(__FILE__) . '/../../../../storage/app/igvs/export-igvs',
            'modulesDir' => '__modules',
        ],
        'prepareForFtpBeta' => [
            'basePath' => dirname(__FILE__) . '/../../../../storage/app/igvs/export-beta',
            'betaPath' => dirname(__FILE__) . '/../../../../../beta.i-gvs.com/www/b/content',
            'betaUrl' => 'http://beta.i-gvs.com/b/',
        ],
        'prepareReleaseCloud' => [
            'releaseDir' => '/mnt/release_win',
            'tmp' => dirname(__FILE__) . '/../../../../storage/app/igvs/release-cloud-tmp',
            'displayReleaseDir' => '//cvs/gvs-release/',
        ],
        'export_pilot' => [
            'releaseDir' => '/mnt/release_win/pilot_work/prod',
            'tmp' => dirname(__FILE__) . '/../../../../storage/app/igvs/export-pilot-tmp',
            'displayReleaseDir' => '',
            'response_url' => 'http://pilot.i-gvs.com/oauth/',
        ],
        'zip' => [
            'releaseDir' => '/storage/app/media',
            'tmp' => dirname(__FILE__) . '/../../../../storage/app/igvs/release-cloud-tmp',
            'displayReleaseDir' => '\\004-1\gvs\release\test',
        ],
        'maker' => [
            'releaseDir' => '/mnt/release_win',
            'tmp' => dirname(__FILE__) . '/../../../../storage/app/igvs/release-cloud-tmp',
            'displayReleaseDir' => '//cvs/gvs-release/',
        ],
    ];