<?php

use Igvs\Courses\Models\ModuleContent;
use Academy\Api\Middleware\BasicHttpAuth;
use Igvs\Courses\Classes\CheckAuthMiddleware;
use Igvs\Courses\Classes\IgvsRouterHelper;


App::before(function ($request) {

    $backend_uri_prefix = IgvsRouterHelper::igvsGetBackendUriPrefix();

    Route::group(['prefix' => Backend::baseUrl($backend_uri_prefix . '/courses'), 'middleware' => CheckAuthMiddleware::class], function() {

        # CRUD cources
        Route::any('', 'Igvs\Courses\Controllers\Courses@index');

        Route::any('create', 'Igvs\Courses\Controllers\Courses@create');

        Route::any('courses/update/{id}', 'Igvs\Courses\Controllers\Courses@update')
            ->where('id', '[0-9]+');

        Route::any('courses/materials_update/{id}', 'Igvs\Courses\Controllers\Courses@materials_update')
            ->where('id', '[0-9]+');

        Route::any('{id}/properties', 'Igvs\Courses\Controllers\Courses@update')
            ->where('id', '[0-9]+');

        # CRUD categories
        Route::any('{id}/categories', 'Igvs\Courses\Controllers\Categories@index')
            ->where('id', '[0-9]+');

        // contents
        Route::any('contents/updateField/{id}', 'Igvs\Courses\Controllers\Contents@updateField')
            ->where('id', '[0-9]+');

        Route::any('contents/updateField', 'Igvs\Courses\Controllers\Contents@updateField');

        Route::any('contents/update/{id}/{context}', 'Igvs\Courses\Controllers\Contents@update')
            ->where('id', '[0-9]+');

        Route::any('contents/crudcreate', 'Igvs\Courses\Controllers\Contents@crudcreate')
            ->where('id', '[0-9]+');

        Route::any('contents/versionKeeper/{id}', 'Igvs\Courses\Controllers\Contents@versionkeeper')
            ->where('id', '[0-9]+');

        Route::any('rubricatorhistorys/view/{id}', 'Igvs\Courses\Controllers\RubricatorHistorys@view')
            ->where('id', '[0-9]+');

        Route::any('logfiles/view/{id}', 'Igvs\Courses\Controllers\LogFiles@view')
            ->where('id', '[0-9]+');
    });


    Route::group(['prefix' => Backend::baseUrl('igvs/courses'), 'middleware' => CheckAuthMiddleware::class], function() {
        # CRUD cources
        Route::any('', 'Igvs\Courses\Controllers\Courses@index');

        Route::any('create', 'Igvs\Courses\Controllers\Courses@create');

        Route::any('csv', 'Igvs\Courses\Controllers\Courses@csv');

        Route::any('{id}/properties', 'Igvs\Courses\Controllers\Courses@update')
            ->where('id', '[0-9]+');

        # CRUD categories
        Route::any('{id}/categories', 'Igvs\Courses\Controllers\Categories@index')
            ->where('id', '[0-9]+');

        // contents
        Route::any('contents/updateField/{id}', 'Igvs\Courses\Controllers\Contents@updateField')
            ->where('id', '[0-9]+');

        // exportdict
        Route::any('{id}/exportdict', 'Igvs\Courses\Controllers\CourseDict@exportDict')
            ->where('id', '[0-9]+');

        // parse dict
        Route::any('{id}/dictparse', 'Igvs\Courses\Controllers\CourseDict@dictparse')
            ->where('id', '[0-9]+');

        // import dict
        Route::any('{id}/dictimport', 'Igvs\Courses\Controllers\CourseDict@dictimport')
            ->where('id', '[0-9]+');

        // import dict
        Route::any('{id}/importdictexample', 'Igvs\Courses\Controllers\CourseDict@importDictExample')
            ->where('id', '[0-9]+');
    });


    Route::get('oauth/pilot/new', 'Igvs\Courses\Controllers\OAuth@pilotOAuth');
    Route::get('oauth/pilot/expertise', 'Igvs\Courses\Controllers\OAuth@pilotOAuthExpertise');
    Route::get('oauth/pilot/new/finish', 'Igvs\Courses\Controllers\OAuth@pilotOAuthFinish');

    Route::get('oauth/pilot/inclusive', 'Igvs\Courses\Controllers\OAuth@pilotOAuthInclusive');
    Route::get('oauth/pilot/inclusive/finish', 'Igvs\Courses\Controllers\OAuth@pilotOauthInclusiveFinish');

    Route::get('oauth/pilot/new/licencies', 'Igvs\Courses\Controllers\OAuth@pilotOAuthLicencies');
    Route::get('oauth/pilot/new/uselicence', 'Igvs\Courses\Controllers\OAuth@pilotOauthUseLicence');
    Route::get('oauth/pilot/new/endpoint', 'Igvs\Courses\Controllers\OAuth@pilotOauthEndPoint');

    Route::get('oauth/test_client', 'Igvs\Courses\Controllers\OAuth@test_client');

    Route::get('oauth/logout', 'Igvs\Courses\Controllers\OAuth@logout');

    Route::get(Backend::baseUrl('backend/auth/signin'), 'Igvs\Courses\Controllers\Auth@signin');
    Route::get(Backend::baseUrl('backend/auth/back_uri_forget'), 'Igvs\Courses\Controllers\Auth@back_uri_forget');
//    Route::get('oauth/pilot/test', 'Igvs\Courses\Controllers\OAuth@pilotOauthTest');
//    Route::get('oauth/test/set', 'Igvs\Courses\Controllers\OAuth@test_set');
//    Route::get('oauth/test/get', 'Igvs\Courses\Controllers\OAuth@test_get');

    Route::any('api/2/test', 'Igvs\Courses\Controllers\Soap@test')
        ->middleware(BasicHttpAuth::class);

    Route::any('api/2/wsdl.wsdl', 'Igvs\Courses\Controllers\Soap@wsdl')
        ->middleware(BasicHttpAuth::class);

    Route::any('api/2', 'Igvs\Courses\Controllers\Soap@api')
        ->middleware(BasicHttpAuth::class);

    Route::any('api/getTemplatesVersions', 'Igvs\Courses\Controllers\Api2@getTemplatesVersions')
        ->middleware(BasicHttpAuth::class);

    Route::any('api/user-code-test/python', 'Igvs\Courses\Controllers\RunTests@python');

});

Route::get('init-event', function () {

    $data = [
        'topic_id' => 'onNewData',
        'data' => 'someData: ' . rand(1, 100)
    ];

    \Igvs\Courses\Classes\Socket\Pusher::sentDataToServer($data);

    dd($data);

});
