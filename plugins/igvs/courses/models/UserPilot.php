<?php namespace Igvs\Courses\Models;

use Model;

/**
 * UserPilot Model
 */
class UserPilot extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_user_pilot';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'user_id',
        'pilot_user_id',
        'instance_id',
        'cluster_id',
        'poo_id',
        'email',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    private static $cachedMembers = [];
    private static $cachedUsersIdByInstanceClusterPoo = [];

    public static function getUserPilotByUserId($user_id)
    {
        if (isset(self::$cachedMembers[$user_id]))
            return self::$cachedMembers[$user_id];

        self::$cachedMembers[$user_id] = self::where('user_id', $user_id)->select('instance_id', 'cluster_id', 'poo_id')->first();

        return self::$cachedMembers[$user_id];
    }

    public static function getUserPilotByInstanceClusterPoo($instance_id, $cluster_id, $poo_id, $poo_filter_enable)
    {
        $cache_key = $instance_id
                    . '-' . $cluster_id
                    . '-' . $poo_id
                    . '-' . (int)$poo_filter_enable;

        if (isset(self::$cachedUsersIdByInstanceClusterPoo[$cache_key]))
            return self::$cachedUsersIdByInstanceClusterPoo[$cache_key];

        $users_id = self::where('instance_id', $instance_id)
            ->where('cluster_id', $cluster_id);

        if ($poo_filter_enable) {
            $users_id = $users_id->where('poo_id', $poo_id);
        }

        self::$cachedUsersIdByInstanceClusterPoo[$cache_key] = $users_id->lists('user_id');

        return self::$cachedUsersIdByInstanceClusterPoo[$cache_key];
    }
}
