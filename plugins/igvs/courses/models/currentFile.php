<?php namespace Igvs\Courses\Models;

use Input;
use System\Models\File as FileBase;

class currentFile extends FileBase
{
    private function createDirs($path,$curr='')
    {
        $path = explode('/',$path);
        for($i=0;$i<count($path);$i++)
        {
            $curr.=$path[$i].'/';
            if(file_exists($curr))
            {
                if(!is_dir($curr))
                    return false;
            }
            else
                mkdir($curr);
        }
    }

    public function getPath ()
    {
        $uploadedFile = Input::file('file_data');

        if($uploadedFile && !empty($this->attributes['attachment_id'])) {
            $this->createDirs('storage/app/igvs/module-content/'.$this->attributes['attachment_id'].'/__system');
            copy($uploadedFile->getPathname(),'storage/app/igvs/module-content/'.$this->attributes['attachment_id'].'/__system/'.$this->attributes['field'].'.'.$uploadedFile->getClientOriginalExtension());
        }

        return $this->getPublicPath() . $this->getPartitionDirectory() . $this->disk_name;
    }
}