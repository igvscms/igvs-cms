<?php namespace Igvs\Courses\Models;

use Model;

/**
 * MemberRoles Model
 */
class MemberRole extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_members_roles';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['member_id', 'role_id'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    static private $member_role = [];
    static private $member_few_role = [];

    static public function getMemberRoles($member_id, $roles = null)
    {
        if (!isset(self::$member_role[$member_id])) {
            self::$member_role[$member_id] = self::where('member_id', $member_id)->lists('role_id');
        }

        if (!is_null($roles)) {
            return array_intersect(self::$member_role[$member_id], $roles);
        }

        return self::$member_role[$member_id];
    }

    static public function getMemberFewRoles($members_id, $roles = null)
    {
        $members_id_cache_key = implode(', ', $members_id);

        if (!isset(self::$member_few_role[$members_id_cache_key])) {
            self::$member_few_role[$members_id_cache_key] = self::whereIn('member_id', $members_id)->lists('role_id');
        }

        if (!is_null($roles)) {
            return array_intersect(self::$member_few_role[$members_id_cache_key], $roles);
        }

        return self::$member_few_role[$members_id_cache_key];
    }
}
