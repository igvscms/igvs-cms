<?php namespace Igvs\Courses\Models;

use Model;

/**
 * StorageStats Model
 */
class StorageStats extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_storage_stats';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'date', 'hash', 'inode', 'size', 'is_storage', 'count_links'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'details' => ['Igvs\Courses\Models\StorageStatsDetail']
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
