<?php namespace Igvs\Courses\Models;

use Model;
use Config;
use App;

/**
 * Module Model
 */
class Module extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_modules';

    /**
     * @var array Dates fields
     */
    protected $dates = ['deleted_at', 'updated_dt', 'created_at'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'release_date',
        'release_notes',
        'developer',
        'developer_comment',
        'behaviors',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];

    public $hasMany = [
        'versions' => [
            'Igvs\Courses\Models\ModuleVersion',
            'primaryKey' => 'module_id',
            'order' => 'sort DESC'
        ],
        'valid_versions' => [
            'Igvs\Courses\Models\ModuleVersion',
            'primaryKey' => 'module_id',
            'conditions' => 'status != "broken"',
            'order' => 'sort DESC'
        ],
        'constructor_versions' => [
            'Igvs\Courses\Models\ConstructorVersion',
            'primaryKey' => 'module_id',
            'order' => 'sort DESC'
        ]
    ];

    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'specification' => ['System\Models\File', 'public' => true],
    ];
    public $attachMany = [];

    public function getLatestVersionAttribute()
    {
        $model = ModuleVersion::where('module_id', $this->attributes['id'])
            ->orderBy('sort', 'desc')
            ->first();

        return $model ? "{$model->version} ({$model->status})" : '';
    }

    public function getLastVersion()
    {
        $model = ModuleVersion::where('module_id', $this->id)
            ->select('version')
            ->orderBy('sort', 'desc')
            ->first();
        if ($model) {
            return $model->version;
        }
    }

    public function getIsSpecial($version = null)
    {
        if (is_null($version))
            $version = $this->getLastVersion();

        $model = ModuleVersion::where('module_id', $this->id)
            ->where('version', $version)
            ->select('special')
            ->first();

        if ($model) {
            return $model->special;
        }

        return false;
    }

    public function getUrlByVersion($version, $file = '')
    {
        return Config::get('igvs.courses::modulesRelease.baseUrl') . "/modules/{$this->path}/{$version}/{$file}";
    }

    public function getPathByVersion($version, $file = '')
    {
        return Config::get('igvs.courses::modulesRelease.path') . "/modules/{$this->path}/{$version}/{$file}";
    }

    public function getLatestPath($file = '')
    {
        $this->getPathByVersion($this->getLastVersion(), $file);
    }

    public function getLocaleName($lang = null)
    {
        $file = $this->getLatestPath('metadata.json');

        if (!file_exists($file) || !$metadata = json_decode(file_get_contents($file))) {
            $attr = 'name' . (is_null($lang) ? '' : "_{$lang}");
            return isset($this->attributes[$attr]) ? $this->attributes[$attr] : ''; 
        }
        if (is_null($lang)) {
            $lang = Config::get('app.locale');
        }
        if ($lang == 'rus') {
            $lang = 'ru';
        }

        if (is_object($metadata->name)) {
            if (isset($metadata->name->$lang)) {
                return $metadata->name->$lang;
            }
            return $metadata->name->en;
        }
        elseif (isset($metadata->{$full_lang = "name_{$lang}"})) {
            return $metadata->$full_lang;
        }
        return $metadata->name;
    }

    public function getNameAttribute()
    {
        return $this->getLocaleName();
    }

    public function getNameRuAttribute()
    {
        return $this->getLocaleName('ru');
    }

    public function getLastReleaseAttribute()
    {
        $model = ModuleVersion::where('module_id', $this->attributes['id'])
            ->orderBy('sort', 'desc')
            ->first();

        return $model ? $model->release_date:0;
    }

    public function getLastUpdateAttribute()
    {
        $model = ModuleVersion::where('module_id', $this->attributes['id'])
            ->orderBy('sort', 'desc')
            ->first();

        return $model ? $model->updated_at->format('Y-m-d H:i:s'): "";
    }

    public function getReleaseNotesAttribute()
    {
        $model = ModuleVersion::where('module_id', $this->attributes['id'])
            ->orderBy('sort', 'desc')
            ->first();

        return $model ? $model->release_notes:'';
    }

    /**
     * Возвращает последнюю, доступную версию
     */
    public function getLatestAvailable($number = null, $date = null)
    {
        $lower = (int)explode('.', $number, 2)[0];
        static $version;
        if (!isset($version[$this->id][$lower])) {
            $versions_db = $this->valid_versions()->get();
            $upper = strval($lower + 1);
            $maxDate = is_null($date) ? time() : strtotime("$date +1 days");

            $versions = [];
            foreach ($versions_db as $ver) {
                $versions[] = $ver;
            }

            $version[$this->id][$lower] = array_reduce($versions, function($cur, $v) use($maxDate, $lower, $upper, $number) {
                $date = strtotime($v->release_date);
                if (!is_null($number)
                    && $date < $maxDate
                    && $date > $cur->date
                    && version_compare($v->version, $lower, '>=')
                    && version_compare($v->version, $upper, '<')
                ) {
                    $cur->date = $date;
                    $cur->version = $v->version;
                } elseif (is_null($number)
                    && $date < $maxDate
                    && $date > $cur->date
                ) {
                    $cur->date = $date;
                    $cur->version = $v->version;
                }
                return $cur;
            }, (object) [
                'date' => 0,
                'version' => $number
            ])->version;
        }
        return $version[$this->id][$lower];
    }


    public function getLatestVersion($lower = '1')
    {
        $versions = ModuleVersion::where('module_id', $this->id)
            ->get()
            ->lists('version');

        usort($versions, 'version_compare');

        $versions = array_filter($versions, function($v) use ($lower) {
            return version_compare($lower, $v, '<');
        });

        return end($versions);
    }

    public function getActualType($version){
        $type = ModuleVersion::where('module_id', $this->id)
            ->where('version', $version)
            ->get()
            ->lists('behaviors');

        if (!$type) return "none";

        return $type[0];
    }

    public function getActualStatus($version){
        $status = ModuleVersion::where('module_id', $this->id)
            ->where('version', $version)
            ->first();

        if (!$status) return "none";

        return $status->status;
    }

    public function getActualConstructor(){
        $constructors = ConstructorVersion::where('module_id', $this->attributes['id'])
            ->get();

        if ($constructors == null) return null;

        $highestVersionValue = "0.0.0";
        $actual_constructor = null;
        foreach ($constructors as $constructor) {
            if ($constructor->status != "broken"){
                $ver = $constructor->version;

                if (version_compare($ver, $highestVersionValue, '>')){
                    $highestVersionValue = $ver;
                    $actual_constructor = $constructor;
                }
            }
        }

        return $actual_constructor;
    }

    public function getStatusName()
    {
        $status = [
            'alpha' => 'Alpha',
            'beta' => 'Beta',
            'stable' => 'Production',
            'unstable' => 'Bad'
        ];
    }
}