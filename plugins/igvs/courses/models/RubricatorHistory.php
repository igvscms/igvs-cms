<?php namespace Igvs\Courses\Models;

use Model;
use Config;
use Input;
use Lang;
use Igvs\Courses\Models\Category;
use BackendAuth;

/**
 * ModuleContentHistory Model
 */
class RubricatorHistory extends Model
{

    use \October\Rain\Database\Traits\SoftDeleting;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_rubricator_history';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'id',
        'value',
        'category_id',
        'type',
        'user_id',
    ];


    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];

    public $belongsTo = [
        'category' => ['Igvs\Courses\Models\Category', 'foreignKey' => 'category_id'],
        'user' => ['Backend\Models\User', 'foreignKey' => 'user_id'],
    ];

    public $belongsToMany = [
    ];

    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getMessageAttribute()
    {
        switch($this->type)
        {
            case '0':
                $value = isset($this->value) ? $this->value : '';

                return Lang::get('igvs.courses::lang.category.message.created_as').'"'.$value.'"';
            case '1':
                $value = isset($this->value) ? $this->value : '';

                $ids = explode(' ',$value);
                if($ids[0] == 0)//находились в конце
                    $return = Lang::get('igvs.courses::lang.category.message.moved_from_last');
                else
                {
                    $after = Category::where('id',$ids[0])->select('code','name')->first();
                    $after_code = isset($after->code) ? $after->code : '';
                    $after_name = isset($after->name) ? $after->name : '';

                    $return = Lang::get('igvs.courses::lang.category.message.moved_from_position').'"'.$after_code.': '.$after_name.'"';
                }
                if(isset($ids[1]) && $ids[1] != 0)//родитель был
                {
                    $parent = Category::where('id',$ids[1])->select('code','name')->first();
                    $parent_code = isset($parent->code) ? $parent->code : '';
                    $parent_name = isset($parent->name) ? $parent->name : '';

                    $return.= Lang::get('igvs.courses::lang.category.message.in_topic').'"'.$parent_code.': '.$parent_name.'"';
                }
                return $return;
            case '2':
                $source = Category::where('id',$this->value)->select('code','name')->first();
                $source_code = isset($source->code) ? $source->code : '';
                $source_name = isset($source->name) ? $source->name : '';

                return Lang::get('igvs.courses::lang.category.message.copied_from').'"'.$source_code.': '.$source_name.'"';
            case '3':
                return Lang::get('igvs.courses::lang.category.message.removed');
            case '4':
                $value = isset($this->value) ? $this->value : '';
                return Lang::get('igvs.courses::lang.category.message.changed_name').'"'.$value.'"';
            case '5':
                $value = isset($this->value) ? $this->value : '';
                return Lang::get('igvs.courses::lang.category.message.changed_code').'"'.$value.'"';
        }
        return '';
    }

    public function beforeSave()
    {
        $user = BackendAuth::getUser();
        if($user)
            $this->user_id = $user->id;
        else
            $this->user_id = 0;
    }
}