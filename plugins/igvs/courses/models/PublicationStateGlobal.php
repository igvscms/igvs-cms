<?php namespace Igvs\Courses\Models;

use Model;

/**
 * publication_state Model
 */
class PublicationStateGlobal extends PublicationState
{
    public $connection = 'igvs_courses';
}
