<?php namespace Igvs\Courses\Models;

use Model;

/**
 * build_user Model
 */
class BuildUser extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_builds_users';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}