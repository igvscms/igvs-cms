<?php namespace Igvs\Courses\Models;

use Model;

class StatusChange extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    protected $rules =[];
    protected $guarded = ['*'];
    protected $fillable = [
        'role_id',
        'status_id',
        'status_new_id',
    ];

    public $table = 'igvs_courses_roles_statuses';
    public $timestamps = false;

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'status' => ['Igvs\Courses\Models\Status', 'key' => 'status_id'],
        'status_new' => ['Igvs\Courses\Models\Status', 'key' => 'status_new_id'],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
    public $belongsToMany = [];

    private static $status_change_cache = [];
    private static $status_change_cache_count = [];

    public static function getStatusChange($roles, $status_id)
    {
        $roles_cache_key = implode(',', $roles) . '|' . $status_id;

        if (isset(self::$status_change_cache[$roles_cache_key]))
            return self::$status_change_cache[$roles_cache_key];

        $model = new StatusChange();
        $model_table = $model->getTable();

        $query = StatusChange::whereIn('role_id', $roles)
            ->where("$model_table.status_id", $status_id);

        if (is_null($status_id)) {
            $query->whereHas('status_new', function($status) {
                $status->where('created', true);
            });
        }
        $statuses = $query->lists('status_new_id');

        if (strlen($status_id)) $statuses[] = $status_id;

        self::$status_change_cache[$roles_cache_key] = $statuses;

        return self::$status_change_cache[$roles_cache_key];
    }

    public static function getStatusChangeCount($roles, $status_new_id)
    {
        $roles_cache_key = implode(',', $roles) . '|' . $status_new_id;

        if (isset(self::$status_change_cache_count[$roles_cache_key]))
            return self::$status_change_cache_count[$roles_cache_key];

        self::$status_change_cache_count[$roles_cache_key] = StatusChange::whereIn('role_id', $roles)
            ->whereNull('status_id')
            ->where('status_new_id', $status_new_id)
            ->count();

        return self::$status_change_cache_count[$roles_cache_key];
    }
}
