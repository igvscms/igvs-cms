<?php namespace Igvs\Courses\Models;

use Model;
use Config;
use Input;
use Igvs\courses\Models\logging;


/**
 * ModuleContentHistory Model
 */
class LogFile extends Model
{
    public $table = 'igvs_files';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'];
    protected $guarded = ['*'];
    protected $fillable = [
        'id',
        'name',
        'status',
        'content_id',
    ];
    public $hasOne = [];
    public $hasMany = [];

    public $belongsTo = [
        'content' => ['Igvs\Courses\Models\ModuleContent', 'foreignKey' => 'content_id'],
    ];

    public $belongsToMany = [
    ];

    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
    
    public static function addFileToLog($path,$content_id,$status=0,$oldName='')
    {
        $path = preg_replace('|([/]+)|s', '/', $path);
        if($status==2)
            $file = self::whereIn('status', [1, 3])//0 - создан 1- удален 2-восстановлен 3 - заменен 4 - переименован
            ->where('name', $path)
            ->where('content_id',$content_id)
            ->first();
        elseif($status==4)
            $file = self::whereIn('status', [0, 2, 4])//0 - создан 1- удален 2-восстановлен 3 - заменен 4 - переименован
            ->where('name', $oldName)
            ->where('content_id',$content_id)
            ->first();
        elseif($status!=0)
            $file = self::whereIn('status', [0, 2, 4])//0 - создан 1- удален 2-восстановлен 3 - заменен 4 - переименован
            ->where('name',$path)
            ->where('content_id',$content_id)
            ->first();
        if(!isset($file) || !$file)
        {
            $file = new LogFile();
            $file->content_id = $content_id;
        }
        $file->name = $path;// для stat = 4 это должно быть тут
        $file->status = $status;
        $file->save();

        $user = \BackendAuth::getUser();

        $loginarray = [
            'file_id' => $file->id,
            'status' => $status,
            'user_id' => (isset($user->id) ? $user->id : 0),
            'name' => $path,
        ];
        logging::create($loginarray);
        return $file->id;
    }
    
    public static function refactorFile($globalPath,$path,$content_id,$status=1,$oldName='')
    {
        if(($status==0 || $status==2 || $status==4) && file_exists($globalPath.$path))
        {
            self::refactorFile($globalPath, $path, $content_id, 3);
            unlink($globalPath . $path);
        }

        $file_id = self::addFileToLog($path,$content_id,$status,$oldName);

        if($status==1 || $status==3)
        {
            $cartPath = $globalPath . '/../../../card/';
            //$cartPathNew = $globalPath . '/../../../card-new/';

            if(!file_exists($cartPath))
                mkdir($cartPath);

            /*if (!file_exists($cartPathNew))
                mkdir($cartPathNew);*/

            link($globalPath . $path, $cartPath . $file_id); 
            // link($globalPath . $path, $cartPathNew . $file_id);
        }
    }

    public static function refactorDirectory($globalPath,$path,$content_id,$status=1,$oldName='')
    {
        if (!is_dir($globalPath.$path)) {
            return;
        }
        foreach (scandir($globalPath.$path) as $file) {
            if (in_array($file, ['.', '..'])) {
                continue;
            }
            if (is_dir($globalPath.$path.$file)) {
                self::refactorDirectory($globalPath,$path.$file.'/',$content_id,$status,$oldName.$file.'/');
            }
            else {
                self::refactorFile($globalPath,$path.$file,$content_id,$status,$oldName.$file);
            }
        }
    }

    public static function restoreFile($globalPath,$file_id)
    {
        if(file_exists($globalPath.'../card/'.$file_id))
        {
            $file = self::where('id', $file_id)->whereIn('status',[1,3])->first();
            if($file)
            {
                $globalPath.= $file->content->course_id . '/' . $file->content->id;
                $oldName = '/../../../card/' . $file_id;
                self::refactorFile($globalPath, $file->name, $file->content_id, 2, $oldName);
                return rename($globalPath . $oldName, $globalPath . $file->name);
            }
        }
        return false;
    }

    public static function removeFile($globalPath,$file_id)
    {
        $file = self::where('id',$file_id)->whereIn('status',[0,2,4])->first();
        if($file)
        {
            $globalPath.= $file->content->course_id . '/' . $file->content->id;
            if(file_exists($globalPath.$file->name))
            {
                self::refactorFile($globalPath, $file->name, $file->content_id, 1);
                return unlink($globalPath . $file->name);
            }
        }
        return false;
    }

    public function getStatusNameAttribute()
    {
        $statuses = ['created', 'deleted', 'restored', 'replaced', 'renamed'];
        $status = &$statuses[$this->status];
        return trans('igvs.courses::logfile.status.' . ($status ?: 'undefined'));
    }

    public function scopeGetDeveloper($query)
    {
        $is_pilot_user = \BackendAuth::getUser()->hasPermission('igvs.courses.pilot_user');
        $user = new User();
        $users = $user->getTable();

        $logging = $this->getTable();

        $userPilot = new UserPilot();
        $userPilots = $userPilot->getTable();

        $query->join(\DB::raw("(
                SELECT
                    file_id,
                    CONCAT(first_name, ' ', last_name, ' (', " . ($is_pilot_user ? "IF($userPilots.user_id, $userPilots.email, $users.email)" : "login") . ", ')')user_name
                FROM (
                    SELECT
                        file_id, user_id
                    FROM (
                        SELECT
                            max(id)id
                        FROM `igvs_loggings`
                        GROUP BY file_id
                    )max_log
                    JOIN `igvs_loggings`
                        ON `igvs_loggings`.id = max_log.id
                )logging
                JOIN $users
                    ON $users.id = logging.user_id" . ($is_pilot_user ? "
                LEFT JOIN $userPilots
                    ON $userPilots.user_id = logging.user_id" : "") . "
            )user"), "user.file_id", '=', "$logging.id")
            ->addSelect('user_name');
    }
}
