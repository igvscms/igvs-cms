<?php namespace Igvs\Courses\Models;

use Lang;
use Academy\Tasks\Models\Task;
use Academy\Tasks\Models\Role as TaskRole;
use Academy\Tasks\Models\StatusChange as TaskStatusChange;

/**
 * ModuleContent Model
 */
class ModuleContent extends \Model
{
    use \October\Rain\Database\Traits\SoftDeleting;
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;
    use \October\Rain\Database\Traits\Purgeable;
    use \Igvs\Courses\Traits\CheckAccess;
    use \Igvs\Courses\Traits\CourseProperty;

    use \Academy\System\Traits\Sorting {
         moveBefore as public moveBeforeSorting;
         moveAfter as public moveAfterSorting;
    }

    public $do_not_touch_sort;

    public $configSorting  = [
        'fldSort' => 'sort',
        // 'fldParent' => 'category_id',
        'conditions' => ['course_id', 'category_id']
    ];

    public $table = 'igvs_courses_modules_contents';
    public $force_history_save = false;

    public $position;
    protected $purgeable = ['group'];

    protected $rules =[
        'name' => ['required', 'between:3,255'],
//        'module_id' => 'required',
        'code' => 'max:50',
//        'steps' => 'required'
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'tasks' => ['Academy\Tasks\Models\Task', 'key' => 'module_id']
    ];

    public $belongsTo = [
        'category' => ['Igvs\Courses\Models\Category', 'foreignetKey' => 'category_id'],
        'module' => ['Igvs\Courses\Models\Module', 'foreignKey' => 'module_id'],
        'status' => 'Igvs\Courses\Models\Status',
        'course' => 'Igvs\Courses\Models\Course',
        'user' => 'Backend\Models\User',
        'link_to_theory_module_form' => 'Igvs\Courses\Models\ModuleContent',
    ];

    public $belongsToMany = [
        'builds' => [
            'Igvs\Courses\Models\Build',
            'table' => 'igvs_courses_builds_modules_contents',
            'key' => 'module_content_id',
            'otherKey' => 'build_id'
        ],
    ];

    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [
        'comments' => ['Academy\Cms\Models\Comment', 'name' => 'attachment']
    ];
    public $attachOne = [];
    public $attachMany = [
        'read_comments' => 'Academy\Cms\Models\BeenRead'
    ];

    private static $status_options_cached = [];

    public function getParentIdAttribute()
    {
        if (isset($this->attributes['parent_id'])) {
            return $this->attributes['parent_id'];
        }
        return $this->copy_original_id;
    }

    public function getPathContent($file = '')
    {
        $storage = \Config::get('igvs.courses::modulesContent.path');
        return "{$storage}/{$this->course_id}/{$this->id}/{$file}";
    }

    private function createCommonFolder()
    {
        if (!$this->course_id || is_null($this->course_id)) {
            return;
        }
        $storage = \Config::get('igvs.courses::modulesContent.path');
        if (!is_dir($dir = "{$storage}/{$this->course_id}/{$this->id}")) {
            mkdir($dir, 0777, true);
        }
        if (file_exists($link = "{$dir}/common") && is_link($link)) {
            try {
                unlink($link);
            }
            catch (\Exception $e) {}
        }

        $module_html_mark_up_id = \Cache::get('igvs.module.html_markup_id', null);
        if ($this->module_id == $module_html_mark_up_id
                && $this->id
                    && $this->course_id
        ) {
            Course::createCommonFolder($this->course_id);
            try {
                symlink("../commonHtmlMatkup", $link);//отработает на сервере, но не локально
            }
            catch (\Exception $e) {}
        }
    }

    public function afterCreate()
    {
        $this->createCommonFolder();
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function($model) {
            $model->code .= '_del_' . uniqid();
            $model->forcesave();
        });
    }

    public function beforeCreate()
    {
        if (empty($this->course_id) && isset($this->category_id)) {
            $this->course_id = $this->category()->first()->course_id;
        }
        $this->status_id = $this->course->status_id;

        if($this->version == '') {
            $versions = $this->getVersionOptions();
            reset($versions);
            $this->version = key($versions);
        }
        unset($this->group);

        if (empty($this->code)) {
            $this->code = uniqid();
        }
        if ($this->init_by_tpl) {
            // формирование пути до модуля
            $m = Module::where('id', $this->module_id)->select('path')->first();

            $path = \Config::get('igvs.courses::modulesRelease.path') . "/modules/{$m->path}/{$this->version}/content/moduleData.js";

            if (file_exists($path)) {
                $jspath = dirname(__FILE__) . '/../updates/nodejs/json-decode.js';
                $json = shell_exec("nodejs {$jspath} {$path}");
                $this->data = str_replace(['"content/', '\'content/'], ['"', '\''], $json);
            }
            if ($this->data == '') {
                $this->data = '{}';
            }
        }
    }

    public function moveBefore($model)
    {
        if ($this->course_id == $model->course_id) {
            $this->category_id = $model->category_id;
            $this->moveBeforeSorting($model);
        }
    }

    public function moveAfter($model)
    {
        if ($this->course_id == $model->course_id) {
            $this->category_id = $model->category_id;
            $this->moveAfterSorting($model);
        }
    }

    static function is_assoc($array)
    {
        $i = 0;
        foreach ($array as $key => $val) {
            if ($i !== $key) {
                return false;
            }
            $i++;
        }
        return true;
    }


    static function print_array2($arr,$step='  ')
    {
        if (is_array($arr)) {
            end($arr);
            $end_key = key($arr);
            if (self::is_assoc($arr)) {
                $return = '[';
                foreach ($arr as $key => $val) {
                    $return.= '
'.$step.self::print_array2($val,$step.'  ').($end_key==$key?'':',');
                }
                $return.= '
'.$step.']';
            }
            else {
                $return = '{';
                foreach ($arr as $key => $val) {
                    $return.= '
'.$step.'"'.htmlspecialchars($key).'": '.self::print_array2($val,$step.'  ').($end_key==$key?'':',');
                }
                $return.= '
'.$step.'}';
            }
            return $return;
        }
        if (is_int($arr)) {
            return $arr;
        }
        if (is_bool($arr)) {
            return $arr ? 'true' : 'false';
        }
        return '"'.str_replace(['\\','"'], ['\\\\','\"'], $arr).'"';
    }

    /**
     * Удаляет из строки символы переноса строк
     * @param $text
     * @return string
     */
    public static function removeWrongSymbols($text) {
        $text = str_replace("\n", ' ',$text);
        return $text;
    }

    static function get_diff_string($curr,$prev)
    {
        $return = [];
        for($i=0;$i < strlen($curr) && $i < strlen($prev);$i++)
        {
            if($curr[$i]!=$prev[$i])
            {
                $return['s'] = $i;
                break;
            }
        }
        if(!isset($return['s']))
        {//r - replacement s - start l - length
            $return['s'] = min(strlen($curr),strlen($prev));
            $return['l'] = max(0,strlen($curr)-strlen($prev));
            $return['r'] = substr($prev,$return['s']);
            if($return['r']===false)
                $return['r'] = '';
            return $return;
        }

        $pl = strlen($prev)-strlen($curr);
        for($i=strlen($curr)-1;$i>=0;$i--)
        {
            if($curr[$i]!=$prev[$i+$pl] || $i<=0 || $i+$pl<=0 || $i+1==$return['s'] || $i+$pl+1==$return['s'])
            {
                $return['l'] = $i-$return['s']+1;
                $return['r'] = substr($prev,$return['s'],$return['l']+$pl);
                break;
            }
        }
        if(!isset($return['r']) || !isset($return['l']))
        {//c<p s=0 p>0
            $return['l'] = max(0,-$pl)-$return['s'];
            $return['r'] = substr($prev,$return['s'],$return['l']+$pl);
        }
        return $return;
    }

    static function array_difference($curr,$prev)
    {
        if(!is_array($curr) || !is_array($prev))
        {
            if(is_array($curr) == is_array($prev))
            {
                if($curr == $prev)
                    return true;
                else
                {
                    if(is_bool($curr) || is_bool($prev))
                        return ['s' => 0, 'l' => strlen(strval($curr)), 'r' => $prev];
                    else
                        return self::get_diff_string(strval($curr), strval($prev));
                }
            }
            else
                return ['r'=>$prev,'s'=>'l'];//r - replacement
        }

        $return = [];

        //получает массив, в котором ключи являются позицией ключа исходного массива, а значение - ключем исходного массива
        $c_keys = array_keys($curr);
        $p_keys = array_keys($prev);

        //получаем оригинальные массивы, но с числовыми ключами
        $c_vals = array_values($curr);
        $p_vals = array_values($prev);

        //получает массив, в котором ключи являются ключами исходного массива, а значение - массивом позиций ключа исходного массива
        $c_keys_pos = array_flip($c_keys);
        $p_keys_pos = array_flip($p_keys);

        //пробегаемся по всем ключам текущей версии
        foreach($c_keys_pos as $key => $pos)
        {
            //если раньше не было таких ключей
            if(!isset($p_keys_pos[$key]))
            {
                $return[$key] = ['s'=>'d'];//s - status d-deleted
            }
            else//определяем куда переместились ключи
            {
                $ad = self::array_difference($c_vals[$pos],$p_vals[$p_keys_pos[$key]]);

                if($ad === true)
                {
                    if($p_keys_pos[$key]!=$pos)
                        $return[$key] = ['s'=>'m','p'=>$p_keys_pos[$key]];//s - status m - moved p - pos
                }
                else {
                    $return[$key] = ['s' => 'e', 'd' => $ad];//s - status e - edit d - diff
                    if($p_keys_pos[$key]!=$pos)
                        $return[$key]['p'] =  $p_keys_pos[$key];

                }
            }
        }
        foreach($p_keys_pos as $key => $pos)
        {
            if(!isset($c_keys_pos[$key]))
            {
                $return[$key] = ['s'=>'n','p'=>$pos,'d'=>$prev[$key]];//s - status n - new p - pos
            }
        }
        if(count($return) == 0)
            return true;
        else
            return ['r'=>$return,'l'=>'s'];//r - replacement
    }

    public function beforeSave()
    {
        $user = \BackendAuth::getUser();

        if ($user) {
            $this->user_id = $user->id;
            if (!isset($this->type) || $this->type < 10) {
                $this->type = 0;
            }
            else {
                $this->type = $this->type/10;
            }
        }
        else {
            $this->user_id = 0;
            $this->type = 2; // пользователя нет, а значит это командная строка
        }

        $this->ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR']
            : 'Script';

        if ($this->module && $this->module->path == 'html-markup') {
            $obj = json_decode($this->data);
            $this->content = (isset($obj->head) ? $obj->head : '') . (isset($obj->html) ? $obj->html : '');
        }

        //В содержание модуля могут вставить текст, содержащий символы переноса строк, что ломает сохранение
        if ($this->module && $this->module->path == 'intro') {
            $obj = json_decode($this->data);
            if (isset($obj->questions[0]->container->content->body)) {
                $obj->questions[0]->container->content->body = ModuleContent::removeWrongSymbols($obj->questions[0]->container->content->body);
                $this->data = json_encode($obj);
            }
        }

        if (!strlen($this->code))
            $this->code = $this->getUniqueCode();
    }

    private function getUniqueCode()
    {
        $codes = ModuleContent::select('code')
            ->where('course_id', $this->course_id)
            ->lists('code');

        $code = uniqid();

        while (in_array($code, $codes)) {
            $code = uniqid();
        }

        return $code;
    }

    public function beforeUpdate()/*тут происходит сохранение контента*/
    {
        $obj = json_decode($this->data,true);
        if (is_array($obj)) {
            if (isset($obj['colontitle'])) {
                unset($obj['colontitle']);
            }
            if (isset($obj['practice'])) {
                unset($obj['practice']);
            }
        }

        $data = Self::where('id', $this->id)->first();

        if (isset($data->data)) {
            $json = json_decode($data->data, true);

            if ($this->name != $data->name
                || $this->code != $data->code
                || $this->version_auto_disable != $data->version_auto_disable
                || $this->version != $data->version
                || $this->behavior != $data->behavior
                || $this->module_id != $data->module_id
                || $this->sort != $data->sort
                || $this->startscreen_show != $data->startscreen_show
                || $this->practice_title_manage != $data->practice_title_manage
                || $this->learning_outcome != $data->learning_outcome
                || $this->practice_index_title != $data->practice_index_title
                || $this->practice_assessment_title != $data->practice_assessment_title
                || $this->switch_practice_mode != $data->switch_practice_mode
                || $this->type != $data->type
                || $this->steps != $data->steps
                || $this->status_id != $data->status_id
                || $json != $obj
            ) {
                if (!$this->force_history_save) {
                    $count = ModuleContentHistory::where('module_content_id', $this->id)->count();
                    $diff_group = floor($count / 10);
                    $count = $count % 10;
                } else {
                    $diff_group = 0;
                    $count = 10 - 1;
                }

                if ($count == 10 - 1) {
                    $data->data = json_encode($json);
                }
                elseif ($json == $obj) {
                    $data->data = '{}';
                }
                else {//находим изменения и сохраняем
                    $data->data = json_encode($this->array_difference($obj, $json)['r']);
                }

                ModuleContentHistory::create([
                    'module_content_id' => $data->id,
                    'data' => bzcompress($data->data, 4),
                    'module_content_code' => $data->code,
                    'module_id' => $data->module_id,
                    'module_version' => $data->version,
                    'diff' => ($count != 10 - 1),//если =1, значит сохранена разница/. если =0, сохранен весь
                    'diff_group' => $diff_group,//id группы изменений
                    'name' => $data->name,
                    'behavior' => $data->behavior,
                    'user_id' => $data->user_id,
                    'ip' => $data->ip,
                    'type' => $data->type,
                    'created_at' => $data->updated_at,
                    'updated_at' => $data->updated_at,
                ]);
            }
            else {
                return false;
            }
        }
        $this->data = json_encode($obj);
    }

    public function afterFetch()
    {
        $data = json_decode($this->data,true);

        if (is_array($data)) {
            if (isset($data['colontitle'])) {
                unset($data['colontitle']);
            }
            if (isset($data['practice'])) {
                unset($data['practice']);
            }
        }

        $module_html_mark_up_id = \Cache::get('igvs.module.html_markup_id', null);

        if ($this->module_id === $module_html_mark_up_id) {
            if (!isset($data['questions'])) {
                $data['questions'] = [["typeTest" => "html-markup"]];
            }
            if (!isset($data['newBook'])) {
                $data['newBook'] = false;//isset($data['html']);
            }
            if (!isset($data['html'])) {
                $data['html'] = '<body></body>';
            }
            if (!isset($data['head'])) {
                $data['head'] =
                '<head>
                    <title>Новый ebook модуль</title>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <meta name="viewport" content="width=device-width, user-scalable=no">
                    <script src="../../interface/js/jquery.js"></script>
                    <script src="../../interface/js/tooltipster.js"></script>
                    <script src="../../interface/js/scrollbar.js"></script>
                    <script src="../../interface/js/content.js"></script>
                    <script src="../../interface/js/fittext.js"></script>
                </head>';
            }
        }
        $this->createCommonFolder();
        $this->data = $this->print_array2($data);
    }

    public $ignore_validation = false;

    public function beforeValidate()
    {
        if (empty($this->course_id) && isset($this->category_id)) {
            $this->course_id = $this->category()->first()->course_id;
        }
        if ($this->isReadonly()) {
            return false;//отменяем сохранение
        }
        if ($this->ignore_validation) {
            return;
        }
        if (is_array($ModuleContent = post('ModuleContent'))) {
            foreach($ModuleContent as $key => $value) {
                if (isset($this->$key) && $this->$key != $value) {
                    $this->$key = $value;
                }
                elseif (isset($this->belongsTo[$key])) {
                    if (isset($this->belongsTo[$key]['foreignKey'])) {
                        $belkey = $this->belongsTo[$key]['foreignKey'];
                    }
                    else {
                        $belkey = $key.'_id';
                    }
                    if (isset($this->$belkey) && $this->$belkey!=$value) {
                        $this->$belkey = $value;
                    }
                }
            }
        }

        $this->rules = array_merge($this->rules, [
            'code' => ['unique:igvs_courses_modules_contents,code,NULL,id,course_id,' . $this->course_id],
        ]);

        if (in_array($this->data, ['', '""'])) {
            $this->data = '[]';
        }
        if (!is_array(json_decode($this->data, true))) {
            $this->rules = array_merge($this->rules, [
                'data' => 'regex:/^error$/',
            ]);
        }
    }

    public function getPathModule()
    {
        $version = $this->getModuleVersion();

        $v = ModuleVersion::where('version', $version)
            ->where('module_id', $this->module->id)
            ->first();

        return \Config::get('igvs.courses::modulesRelease.path') . "/modules/{$this->module->path}/{$v->version}";
    }

    public function isReadonly($params = [])
    {
        $disable_check_base_course = isset($params['disable_check_base_course']) ? $params['disable_check_base_course'] : false;

        if (!($user = \BackendAuth::getUser())
            || (!$disable_check_base_course && $this->isBaseCourse())
        ) {
            return true;
        }
        if ($this->isCourseAdmin($this->course_id)) {
            return false;
        }
        if ((($this->category->readonly || ($this->category->parent_id && $this->category->parent->readonly))
            && !$this->checkAccessOperation($this->course_id, 'UncheckReadonlyCategory')
            && !($this->checkAccessOperation($this->course_id, 'EditContentLockedModuleTask') && $this->course->isUseProject()))
            || !$this->checkAccessOperation($this->course_id, 'AllowEditCategories')
        ) {
            return true;
        }

        if ($this->course->isUseProject()) {
            $roles = TaskRole::getRolesByProjectIdAndUserId($this->course->project_id, $user->id);

            $task = new Task();
            $taskTable = $task->getTable();

            $role = new TaskRole();
            $roleTable = $role->getTable();

            $statusChange = new TaskStatusChange();
            $statusChangeTable = $statusChange->getTable();

            $canChangeStatuse = Task::getBy('module', $this->id)
                ->join($statusChangeTable,
                    "$statusChangeTable.status_id", '=', \DB::raw("`$taskTable`.`status_id` and `$statusChangeTable`.`type_id` = `$taskTable`.`type_id`"))
                ->where("$taskTable.responsible_id", $user->id)
                ->whereIn("$statusChangeTable.role_id", $roles)
                ->count();

            if ($canChangeStatuse) {
                return false;
            }
            return $this->category->isReadonly(true, true);
        }
        elseif ($this->course->iCanSeeExaminationSystem()) {
            return (int)!is_null($this->status_id) >= count($this->getStatusOptions());
        }

        if ($user->hasPermission('igvs.courses.pilot_user')) {
            return !($this->checkPilotAccess($this->course_id));
        }
    }

    public function isBaseCourse()
    {
        if (!$user = \BackendAuth::getUser()) {
            return true;
        }

        if ($user->hasPermission('igvs.courses.can_edit_base_module')) {
            return false;
        }

        if (!$this->parent_id) {
            return false;
        }

        $result = \Cache::get('is_base_course.module.' . $this->id);

        if (!is_null($result))
            return (int)$result;

        $module = $this;
        $module_ids = [];

        while($module && $module->parent_id) {
            $result = \Cache::get('is_base_course.module.' . $module->id);

            if (!is_null($result))
                break;

            $module_ids[] = $module->id;

            $module = ModuleContent::where('id', $module->parent_id)->first();
        }

        if (is_null($result) && $module) {
            $result = !is_null(Course::getCourse($module->course_id)->is_base_course) ? Course::getCourse($module->course_id)->is_base_course : 0;
        }
        elseif (is_null($result)) {
            $result = !is_null(Course::getCourse($this->course_id)->is_base_course) ? Course::getCourse($this->course_id)->is_base_course : 0;
        }

        $module_ids = array_unique($module_ids);

        \Cache::forever('is_base_course.module.childs.' . $this->id, $module_ids);

        if (count($module_ids)) {
            foreach ($module_ids as $id) {
                \Cache::forever('is_base_course.module.' . $id, $result);
            }
        }

        return (int)$result;
    }

    public function getDesignerUrl()
    {
        if (!$this->module)
            return null;

        $constructor_path_base = dirname(__FILE__) . "/../resources/modules/constructor/{$this->module->path}";
        $url_base = "/plugins/igvs/courses/resources/modules/constructor/{$this->module->path}";

        $module_version = $this->getModuleVersion();

        $firstNumber = explode('.', $module_version)[0];

        $version = ConstructorVersion::where('module_id', $this->module->id)
            ->where('version', 'like', "{$firstNumber}.%")
            ->where('status', 'stable')
            ->orderBy('sort', 'DESC')
            ->first();

        if (!$version)
            return null;

        if (!is_dir(dirname(__FILE__) . "/../resources/modules/constructor/{$this->module->path}"))
            return null;

        $dir = scandir(dirname(__FILE__) . "/../resources/modules/constructor/{$this->module->path}");

        if (empty($dir))
            return null;

        if (is_dir("$constructor_path_base/{$version->version}"))
            return "{$url_base}/{$version->version}/index.html";
    }


    public function getBehaviorOptions()
    {
        $fields = \Input::get('fields', []);

        $module = $this->getModuleByModuleId();

        if (!$module) {
            return [];
        }

        $return = [];
        $version_val = $this->getModuleVersion();

        // если в форме меняется шаблон, то достаём крайнюю версию шаблона сами
        if (in_array('version', $fields) || !$version_val) {

            $version_val = $this->getVersionOptions();
            if (count($version_val)) {
                $version_val = array_flip($version_val);
                $version_val = array_shift($version_val);
            } else {
                $version_val = 0;
            }
        }

        $version = $module->versions()
            ->where('version', $version_val)
            ->first();

        if (!$version) {
            return [];
        }

        // пытаемся достать привидения из версии шаблона
        try {
            $behaviors = json_decode($version->behaviors);
        } catch (\Exception $e) {
            $behaviors = [];
        }

        $user = \BackendAuth::getUser();

        $bag_method = $user
            && $user->hasPermission('igvs.courses.pilot_user')
            && $user->hasPermission('igvs.courses.bag_method');

        // коряга по названиям поведений
        $lang_namespace = $bag_method ? 'behavior_bag_method' : 'behavior';

        $behaviors_default = [
            'check'     => trans('igvs.courses::lang.default.' . $lang_namespace . '.check'),
            'test'      => trans('igvs.courses::lang.default.' . $lang_namespace . '.test'),
            'ask'       => trans('igvs.courses::lang.default.' . $lang_namespace . '.ask'),
            'practice'  => trans('igvs.courses::lang.default.' . $lang_namespace . '.practice'),
        ];

        // оставляем поведения для текущей версии
        foreach ($behaviors_default as $k => $v) {
            if (in_array($k, $behaviors)) {
                $return[$k] = $behaviors_default[$k];
            }
        }

        // убираем поведение practice для bag_method
        if ($bag_method) {
            unset($return['practice']);
        }

        return $return;
    }

    public function isPractice() {

        $practice = [
            'check',
            'practice'
        ];

        return in_array($this->behavior, $practice);
    }

    private function getModuleByModuleId() {
        if ($this->module_id) {
            $module = Module::find($this->module_id);
        } else {
            $module = Module::first();
        }

        return $module;
    }

    public function getClassName() {
        $classes = [
            'check' => 'module practice',
            'test'  => 'module control',
            'ask'   => 'module theory'
        ];

        return isset($classes[$this->behavior]) ? $classes[$this->behavior] : '';
    }

    public function getVersionOptions()
    {
        $module = $this->getModuleByModuleId();

        $versions = [];
        foreach ($module->valid_versions as $v) {
            $versions[$v->version] = "{$v->version} ({$v->status} : ".strstr($v->release_date,' ',true).')';
        }

        return $versions;
    }

    public function getModuleVersion()
    {
        if ($this->version_auto_disable == 1) {
            $version = $this->version;
        } else {
            $version = $this->module && $this->id ? $this->module->getLatestAvailable() : $this->version;
        }

        return $version;
    }

    public function getCategoryIdAttribute()
    {
        return isset($this->attributes['category_id']) ? $this->attributes['category_id'] :
            \Input::get('category_id');
    }

    public function getGroupAttribute()
    {
        if (isset($this->attributes['group'])) {
            return $this->attributes['group'];
        }
        if ($module = Module::find($this->module_id)) {
            return $module->group_id;
        }
    }

    public function getCourseIdAttribute()
    {
        return isset($this->attributes['course_id']) ? $this->attributes['course_id'] :
            \Input::get('course_id');
    }

    public function getThemeAttribute()
    {
        $user = \BackendAuth::getUser();

        $bag_method = $user
            && $user->hasPermission('igvs.courses.pilot_user')
            && $user->hasPermission('igvs.courses.bag_method');

        $interface_theme = \Session::get('igvs.courses.interface_theme', 'default');

        if ($bag_method && $interface_theme != 'default')
            return 'grey';

        if ($bag_method) {
            return 'green';
        }

        if ($this->course) {
            return $this->course->theme;
        }
    }

    public function getPathContents()
    {
        return \Config::get('igvs.courses::modulesContent.path') . "/{$this->course_id}/{$this->id}";
    }

    public static function getModulesThemesOptions($course_id, $theme_name = '')
    {
        if (in_array($theme_name, ['_default', ''])) {
            return null;
        }
        $course_modules = ModuleContent::where('course_id', $course_id)
            ->select('module_id', 'version')
            ->get();

        $query_variables_modules_cloud = [];
        foreach($course_modules as $k => $v) {
            $query_variables_modules_cloud[$theme_name][$v->module_id][$v->getModuleVersion()] = $v->getModuleVersion();
        }
        $query = ModuleTheme::select('module_id');

        foreach ($query_variables_modules_cloud as $theme_name => $modules) {
            $query->orWhere(function($query) use ($theme_name, $modules) {
                $query->where('name', $theme_name)
                    ->where(function($query) use($modules) {
                        foreach ($modules as $module_id => $versions) {
                            $query->orWhere(function($query) use($module_id, $versions) {
                                $query->where('module_id', $module_id)
                                    ->whereIn('version', $versions);
                            });
                        }
                    });
            });
        }
        if (!count($query_variables_modules_cloud)) {
            $query->whereRaw(0);
        }
        return (object) [
            'course_modules' => $course_modules->lists('module_id'),
            'query' => $query,
        ];
    }

    public static function getModulesClearThemes($course_id, $theme_name = '')
    {
        if (!$option = Self::getModulesThemesOptions($course_id, $theme_name)) {
            return [];
        }
        $modules_themes = $option->query
            ->whereNotIn('module_id', $option->course_modules)
            ->lists('module_id');

        $field_name_postfix = \Config::get('app.locale') == 'ru' ? '_ru' : '';
        return Module::whereIn('id', $option->course_modules)
            ->whereIn('id', $modules_themes)
            ->select(\DB::raw('CONCAT(name' . $field_name_postfix . ', " (", path, ")") AS name'),'id')
            ->lists('name', 'id');
    }

    public static function getModulesThemesData($course_id, $theme_name = '')
    {
        if (!$option = Self::getModulesThemesOptions($course_id, $theme_name)) {
            return [
                'course_modules' => [],
                'modules_themes' => [],
                'modules' => [],
            ];
        }
        $modules_themes = $option->query
            ->lists('module_id');

        $field_name_postfix = \Config::get('app.locale') == 'ru' ? '_ru' : '';
        $modules = Module::whereIn('id', $option->course_modules)
            ->select(\DB::raw('CONCAT(name' . $field_name_postfix . ', " (", path, ")") AS name'),'id')
            ->lists('name', 'id');

        return [
            'course_modules' => $option->course_modules,
            'modules_themes' => $modules_themes,
            'modules' => $modules,
        ];
    }

    function getGroupOptions()
    {
        return TemplatesGroup::lists('name','id');
    }

    function getPositionOptions()
    {
        $stmt = self::where('category_id', $this->category_id)
            ->select('id', 'sort', 'code', 'name')
            ->orderBy('sort');

        if ($this->id) {
            $stmt->where('id', '<>', $this->id);
        }

        $res = $stmt->get();

        $dropdown = [];
        foreach ($res as $v) {
            $dropdown[$v->id] = trans('igvs.courses::lang.default.sort.after')
                . " <b>{$v->code}</b>: {$v->name}";
        }

        if (count($dropdown)) {
            $dropdown = [
                'last' => '-- ' . trans('igvs.courses::lang.default.sort.last'),
                'first' => '-- ' . trans('igvs.courses::lang.default.sort.first')
            ] + $dropdown;
        }
        else {
            $dropdown = ['-- ' . trans('igvs.courses::lang.default.sort.first')];
        }

        return $dropdown;
    }

    public static function saveInStorage($filepath, $link = false)
    {
        static $storage = null;

        if (!$storage) {
             $storage = realpath(\Config::get('igvs.courses::modulesContent.path') . '-storage');
        }

        $sha = sha1_file($filepath);

        $file = '/' . substr($sha, 0, 2)
            . '/' . substr($sha, 2, 2)
            . '/' . substr($sha, 4);

        if (!is_dir(dirname($storage.$file))) {
            mkdir(dirname($storage.$file), 0777, true);
        }

        $file_exists = is_file($storage.$file);

        if (!$file_exists && $link) {
            link($filepath, $storage.$file);
        }
        else if (!$file_exists && !$link) {
            copy($filepath, $storage.$file);
        }

        return $storage.$file;
    }

    public function getStatusOptions()
    {
        if (!$user = \BackendAuth::getUser()) {
            return [];
        }

        return self::getStatusOptionsCached($this->course_id, $this->status_id);
    }

    private static function getStatusOptionsCached($course_id, $status_id)
    {
        if (!$user = \BackendAuth::getUser()) {
            return [];
        }

        if (isset(self::$status_options_cached[$course_id]))
            self::$status_options_cached[$course_id] = [];

        if (isset(self::$status_options_cached[$course_id][$status_id]))
            return self::$status_options_cached[$course_id][$status_id];

        $members = Member::getMemberFew($course_id, $user->id);
        $roles = MemberRole::getMemberFewRoles($members);

        $statuses = StatusChange::getStatusChange($roles, $status_id);

        if (!count($statuses))
            return [];

        $return = Status::getStatusesLists($statuses);
        asort($return);

        self::$status_options_cached[$course_id][$status_id] = $return;

        return self::$status_options_cached[$course_id][$status_id];
    }

    public function filterFields($fields, $condition = null)
    {
        if (isset($fields->status)) {
            if ($this->course && !$this->course->iCanSeeExaminationSystem()) {
                $fields->status->hidden = true;
            }
            elseif (count($this->getStatusOptions()) < 2) {
                $fields->status->cssClass = 'hide';
            }
        }
        if (!$this->checkAccessOperation($this->course_id, 'ModuleTabEdit_custom_css')
            && isset($fields->custom_css)
        ) {
            $fields->custom_css->hidden = true;
        }

        if (($user = \BackendAuth::getUser())
            && $user->hasPermission('igvs.courses.bag_method')
        ) {
            if (isset($fields->behavior) && isset($fields->behavior->config->span)) {
                $fields->behavior->config->span = 'full';
            }
            if (isset($fields->version_auto_disable) && isset($fields->version_auto_disable->config->span)) {
                $fields->version_auto_disable->config->span = 'full';
            }
            if (isset($fields->version_auto_disable)) {
                $fields->version_auto_disable->hidden = true;
            }
            if (isset($fields->startscreen_show)) {
                $fields->startscreen_show->hidden = true;
            }
            if (isset($fields->practice_title_manage)) {
                $fields->practice_title_manage->hidden = true;
            }
            if (isset($fields->practice_index_title)) {
                $fields->practice_index_title->hidden = true;
            }
            if (isset($fields->practice_assessment_title)) {
                $fields->practice_assessment_title->hidden = true;
            }
            if (isset($fields->switch_practice_mode)) {
                $fields->switch_practice_mode->hidden = true;
            }
            if (isset($fields->module)) {
                $fields->module->hidden = true;
            }
            if (isset($fields->behavior)) {
                $fields->behavior->hidden = true;
            }
            if (isset($fields->steps)) {
                $fields->steps->hidden = true;
            }
            if (isset($fields->code)) {
                $fields->code->hidden = true;
            }
        }
        else {
            if (isset($fields->module_id)) {
                $fields->module_id->hidden = true;
            }
            if (isset($fields->group)) {
                $fields->group->hidden = true;
            }

            if (isset($fields->behavior_doubled_field)) {
                $fields->behavior_doubled_field->hidden = true;
            }
        }

        if ($this->version_auto_disable != 1 && isset($fields->version))
            $fields->version->hidden = true;
    }

    public function getModuleOptions()
    {
        $is_inclusive = null;
        $course_id = post('dataModel[course_id]');

        if ($this->course) {
            $is_inclusive = $this->course->is_inclusive;
        } elseif ($course_id) {
            if ($course_id && $course = Course::find($course_id)) {
                $is_inclusive = $course->is_inclusive;
            }
        }

        return Module::select('id')
            ->selectRaw("concat(`name`,' (',`path`,')') as name_path")
            ->where('is_inclusive', $is_inclusive)
            ->orWhere('path', 'player')
            //->has('constructor_versions')
            ->lists('name_path', 'id');
    }

    public static function explode($string, $count = null, $size = null) {
        if ((int) $count < 1 && (int) $size < 1) {
            return $string;
        }
        if ((int) $size < 1) {
            $size = round(mb_strlen($string) / $count);
        }
        elseif ((int) $count < 1) {
            $count = ceil(mb_strlen($string) / $size);
        }
        $result = [];
        for ($i = 0; $i < $count - 1; $i++) {
            $result[] = mb_substr($string, $i * $size, $size);
        }
        $result[] = mb_substr($string, ($count - 1) * $size);
        return $result;
    }

    public static function getPathStorage($filepath)
    {
        if (!file_exists($filepath)) {
            return null;
        }
        $sha = sha1_file($filepath);

        return '/' . implode('/', self::explode($sha, 3, 2));
    }

    public function getStartScreenData($name_field)
    {
        $act = $this->category;
        $topic = $this->category->parent;

        if ($name_field == 'unit_title') {
            return (!is_null($topic->unit_title) && strlen($topic->unit_title)) ? $topic->unit_title : $topic->name;
        }
        if ($name_field == 'session_title') {
            return (!is_null($act->session_title) && strlen($topic->session_title)) ? $act->session_title : $act->name;
        }
        if ($name_field == 'learning_outcome') {
            if (!is_null($this->learning_outcome) && strlen($this->learning_outcome))
                return $this->learning_outcome;

            if (!is_null($act->learning_outcome) && strlen($act->learning_outcome))
                return $act->learning_outcome;

            if (!is_null($topic->learning_outcome) && strlen($topic->learning_outcome))
                return $topic->learning_outcome;

            return '';
        }

        if (!is_null($act->{$name_field}) && strlen($act->{$name_field}))
            return $act->{$name_field};

        return (!is_null($topic->{$name_field}) && strlen($topic->{$name_field})) ? $topic->{$name_field} : '';
    }

    public function getUserRolesAttribute()
    {
        $stmt = Member::where('user_id', $this->return_comment->user_id)
            ->where('course_id', $this->course_id)->first();
        if ($stmt) {
            $names = Lang::get('igvs.courses::lang.role.names');
            $return = $stmt->roles()->lists('name');

            foreach ($return as $k => $v) {
                $return[$k] = isset($names[$v]) ? $names[$v] : $v;
            }
            return $return;
        }
    }

    public function getFullNameLoginAttribute()
    {
        $this->course->return_comment = $this->return_comment;
        return $this->course->full_name_login;
    }

    public function getBehaviorDoubledFieldAttribute()
    {
        return $this->behavior;
    }

    public function setBehaviorAttribute($v = '')
    {
        $this->setModuleContentBehavior($v);
    }

    public function setBehaviorDoubledFieldAttribute($v)
    {
        $this->setModuleContentBehavior($v);
    }

    protected function setModuleContentBehavior($v = '')
    {
        $module_content = \Input::get('ModuleContent');
        if (isset($module_content['behavior_doubled_field'])) {
            $behavior = substr($module_content['behavior_doubled_field'], 0, 10);
            $this->attributes['behavior'] = $behavior;
        } elseif (isset($module_content['behavior'])) {
            $behavior = substr($module_content['behavior'], 0, 10);
            $this->attributes['behavior'] = $behavior;
        } elseif (strlen($v)) {
            $this->attributes['behavior'] = $v;
        } else {
            $this->attributes['behavior'] = '';
        }
    }

    public function getCustomCssAttribute()
    {
        if (file_exists($custom = $this->getPathContent('common/custom.css'))) {
            return file_get_contents($custom);
        }
        return '/*Данный стиль будет использоваться для всех модулей html-markup*/';
    }

    public function setCustomCssAttribute($value)
    {
        if (file_exists($custom = $this->getPathContent('common'))) {
            unlink("{$custom}/custom.css");
            file_put_contents("{$custom}/custom.css", $value);
        }
    }
}
