<?php namespace Igvs\Courses\Models;

use Model;

/**
 * expertise Model
 */
class Expertise extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_expertises';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
