<?php namespace Igvs\Courses\Models;

use Model;

/**
 * Operation Model
 */
class Operation extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_operations';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    protected $rules =[
        'name' => ['required', 'unique'],
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    static private $cached_operations = [];

    static public function getOperationsIdsByCode($codes)
    {
        $cache_key = implode('-', $codes);

        if (!isset(self::$cached_operations[$cache_key])) {
            self::$cached_operations[$cache_key] = self::whereIn('code', $codes)->lists('id');
        }

        return self::$cached_operations[$cache_key];
    }
}
