<?php namespace Igvs\Courses\Models;

use Model;
use Igvs\Courses\Models\BackgroundTaskLog;

/**
 * background_tasks Model
 */
class BackgroundTask extends Model
{

    use \October\Rain\Database\Traits\SoftDeleting;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_background_tasks';

    /**
     * @var array Dates fields
     */
    protected $dates = ['deleted_at', 'updated_dt', 'created_at'];


    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'act',
        'options',
        'job_id',
        'progress',
        'progress_full',
        'finished',
        'result',
    ];

//    protected $jsonable = ['options'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'logs' => ['Igvs\Courses\Models\BackgroundTaskLog', 'key' => 'db_id']
    ];
    public $belongsTo = [
        'responsible' => ['Backend\Models\User'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    static public function job_plus($job, $job_percent, $job_percent_all, $db_id) {
        if ($job_percent<$job_percent_all) {
            if (!is_null($db_id)) {
                BackgroundTask::find($db_id)
                    ->update([
                        'progress'      => $job_percent,
                        'progress_full' => $job_percent_all,
                    ]);
            }
        }
    }

    static public function log_error($name, $act, $options, $message, $db_id) {
        BackgroundTaskLog::create([
                'name' => $name,
                'act' => $act,
                'options' => $options,
                'message' => $message,
                'db_id' => $db_id,
            ]);
    }

    public function scopeFilterResponsible($q, $filtered)
    {
        if (is_null($filtered) || !is_array($filtered) || !count($filtered))
            return $q;

        return $q->whereIn('responsible_id', $filtered);
    }

    public function scopeFilterCourseTitle($q, $filtered)
    {
        if (is_null($filtered) || !is_array($filtered) || !count($filtered))
            return $q;

        $like = [];

        foreach ($filtered as $item) {
            $like[] = "options like \"%\\\"courseId\\\":\\\"{$item}\\\"%\"";
        }

        return $q->whereRaw(implode(' or ', $like));
    }

    public function scopeFilterCourseCode($q, $filtered)
    {
        if (is_null($filtered) || !is_array($filtered) || !count($filtered))
            return $q;

        $like = [];

        foreach ($filtered as $item) {
            $like[] = "options like \"%\\\"courseId\\\":\\\"{$item}\\\"%\"";
        }

        return $q->whereRaw(implode(' or ', $like));
    }

}