<?php namespace Igvs\Courses\Models;

use Model;
use Config;
use Input;
use Backend\Models\User as BackendUser;

/**
 * ModuleContentHistory Model
 */
class logging extends Model
{

    use \October\Rain\Database\Traits\SoftDeleting;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_loggings';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'id',
        'file_id',
        'status',
        'user_id',
        'name',
    ];


    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];

    public $belongsTo = [
        'user' => 'Backend\Models\User',
        'file' => ['Igvs\Courses\Models\LogFile', 'foreignKey' => 'file_id'],
    ];

    public $belongsToMany = [
    ];

    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getStatusTextAttribute()
    {
        $statuses = ['created', 'deleted', 'restored', 'replaced', 'renamed'];
        $status = &$statuses[$this->status];
        return trans('igvs.courses::logfile.status.' . ($status ?: 'undefined'));
    }

    public function scopeGetDeveloper($query)
    {
        $is_pilot_user = \BackendAuth::getUser()->hasPermission('igvs.courses.pilot_user');
        $user = new BackendUser();
        $users = $user->getTable();

        $logging = $this->getTable();

        $userPilot = new UserPilot();
        $userPilots = $userPilot->getTable();

        $query->join(\DB::raw("(
                SELECT
                    $users.id, 
                    CONCAT(first_name, ' ', last_name, ' (', " . ($is_pilot_user ? "IF($userPilots.user_id, $userPilots.email, $users.email)" : "login") . ", ')')user_name
                FROM $users" . ($is_pilot_user ? "
                LEFT JOIN $userPilots
                    ON $userPilots.user_id = $users.id" : '') . "
            )user"), "user.id", '=', "$logging.user_id")
            ->addSelect('user_name');
    }
}