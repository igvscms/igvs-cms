<?php namespace Igvs\Courses\Models;

use Model;
use lang;
use Igvs\Courses\Scopes\PilotRoles;

/**
 * Role Model
 */
class Role extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    /**
     * @var array Validation rules
     */
    protected $rules =[
        'name' => 'required|between:3,255|unique:igvs_courses_roles',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_roles';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'statuses' => ['Igvs\Courses\Models\StatusChange', 'key' => 'role_id'],
    ];
    public $belongsTo = [];

    public $belongsToMany = [
        'operations' => [
            'Igvs\Courses\Models\Operation',
            'table' => 'igvs_courses_role_operations',
            'key' => 'role_id',
            'otherKey' => 'operation_id'
        ]
    ];

    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $status_table = [];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new PilotRoles);
    }

    public function status_table()
    {
        $statuses = [];
        foreach(StatusChange::where('role_id', $this->id)->get() as $status)
            $statuses[$status->status_id][$status->status_new_id] = true;

        return $statuses;
    }

    public function afterSave()
    {
        $insert = [];

        foreach($this->status_table as $status_id => $statuses)
            foreach($statuses as $status_new_id)
            {
                $insert[] = [
                    'role_id' => $this->id,
                    'status_id' => $status_id == 'null' ? null : $status_id,
                    'status_new_id' => $status_new_id,
                ];
            }

        StatusChange::where('role_id', $this->id)->delete();
        StatusChange::insert($insert);
    }

    public function getOperationsOptions()
    {
        $keys = operation::select('id','code')->orderBy('id')->get();
        $operations = Lang::get('igvs.courses::lang.role.operations');
        $return = [];
        foreach($keys as $key)
        {
            $return[$key['id']] = isset($operations[$key['code']]) ? $operations[$key['code']] : $key['code'];
        }
        return $return;
    }

    public function getNameLangAttribute()
    {
        $names = Lang::get('igvs.courses::lang.role.names');

        return isset($names[$this->name]) ? $names[$this->name] : $this->name;
    }

}