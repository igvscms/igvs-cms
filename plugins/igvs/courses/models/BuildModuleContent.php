<?php namespace Igvs\Courses\Models;

use Model;

/**
 * BuildModuleContent Model
 */
class BuildModuleContent extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_builds_modules_contents';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}