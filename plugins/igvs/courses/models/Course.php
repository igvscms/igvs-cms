<?php namespace Igvs\Courses\Models;

use Igvs\Courses\Classes\MemberEmailNotice;
use Igvs\Courses\Helpers\Crypt;
use Igvs\Courses\Models\Member;
use Igvs\Courses\Models\Role;
use Igvs\Courses\Models\User;
use Igvs\Courses\Models\MemberRole;
use Igvs\Courses\Models\Type;
use Igvs\Courses\Models\UserPilot;
use Igvs\Courses\Models\ShellVersion;

use Model;
use Lang;
use BackendAuth;

use Academy\Tasks\Models\Project;
use Academy\Tasks\Models\Role as ProjectRole;
use Academy\Tasks\Models\Task;
use Academy\Tasks\Models\StatusChange as TaskStatusChange;

use Illuminate\Database\Eloquent\Builder;

/**
 * Course Model
 */
class Course extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;
    use \October\Rain\Database\Traits\SoftDeleting;
    use \Igvs\Courses\Traits\FileHelper;
    use \Igvs\Courses\Traits\CheckAccess;

    /**
     * @var array Validation rules
     */
    protected $rules =[
        'title' => 'required|between:3,255',
        'code' => 'required|between:2,30|unique:igvs_courses_courses',
    ];

    public $with = [
        'logo',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_courses';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'type_id', 'copy_original_id', 'title', 'modification_name', 'code', 'short_version'
    ];

    /**
     * @var array Dates fields
     */
    protected $dates = [
        'deleted_at', 'updated_dt', 'created_at', 'launch_date', 'public_last_datetime'
    ];

    protected $casts = [
        'enable_shell_comments'       => 'boolean',
        'enable_shell_favorites'      => 'boolean',
        'enable_shell_screen_capture' => 'boolean',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'members' => [
            'Igvs\Courses\Models\Member',
            'primaryKey' => 'course_id',
        ],
        'vocabulary' => [
            'Igvs\Courses\Models\Vocabulary',
            'primaryKey' => 'course_id',
        ],
        'tasks' => [
            'Academy\Tasks\Models\Task',
            'key' => 'course_id',
        ],
        'modules' => 'Igvs\Courses\Models\ModuleContent',
    ];

    public $belongsTo = [
        'shell_version' => [
            'Igvs\Courses\Models\ShellVersion',
            'order' => 'sort desc'
        ],
        'type' => ['Igvs\Courses\Models\Type', 'foreignKey'=>'type_id'],
        'responsible' => ['Igvs\Courses\Models\User', 'foreignKey' => 'responsible_id'],
        'public_last_user' => ['Igvs\Courses\Models\User', 'foreignKey' => 'public_last_user_id'],
        'parent' => ['Igvs\Courses\Models\Course', 'foreignKey'=>'parent_id'],
        'project' => 'Academy\Tasks\Models\Project',
    ];

    public $belongsToMany = [
        'expertise' => [
            'Igvs\Courses\Models\Expertise',
            'table' => 'igvs_courses_coourses_expertises'
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [
        'comments' => [
            'Academy\Cms\Models\Comment',
            'name' => 'attachment',
            //'order' => 'updated_at desc',
            'conditions' => "(relation='' or relation='comments')"
        ],
        'working_programm_comments' => [
            'Academy\Cms\Models\Comment',
            'name' => 'attachment',
            //'order' => 'updated_at desc',
            'conditions' => "(relation='working_programm_comments')"
        ]
    ];

    public $attachOne = [
        'image'           => 'System\Models\File',
        'logo'            => '\Igvs\Courses\Models\currentFile',
        'bg_hat'          => '\Igvs\Courses\Models\currentFile',
        'colontitle_logo' => '\Igvs\Courses\Models\currentFile',
        'materials'       => '\Igvs\Courses\Models\currentFile',
        'cover'           => '\Igvs\Courses\Models\currentFile',
    ];
    public $attachMany = [
        'read_comments' => 'Academy\Cms\Models\BeenRead',
    ];

    public $task = null;
    private $server_name = null;

    const ADMIN       = 1;
    const COORDINATOR = 9;
    const DEVELOPER   = 10;
    const OPERATOR    = 11;
    const EXPERT      = 12;

    static private $cached_courses = [];

    static public function getCourse($course_id)
    {
        if (!isset(self::$cached_courses[$course_id])) self::$cached_courses[$course_id] = self::find($course_id);

        return self::$cached_courses[$course_id];
    }

    /**
     * Set relation to Job (Copy tasks)
     * @return \October\Rain\Database\Relations\HasMany
     */
    public function jobs()
    {
        return $this->hasMany(Job::class, 'model_id');
    }

    public function iCanSeeExaminationSystem()
    {
        if (!($user = BackendAuth::getUser()) || !$this->short_version) {
            return false;
        }
        if (!$user->hasPermission('igvs.courses.pilot_user')) {
            return true;
        }
        if (UserPilot::where('user_id', $user->id)->where('instance_id', 1)->limit(1)->count()) {
            return false;
        }
        return $this->checkAccessRights($this->id, 'show_in_pilot') || $this->checkAccessRights($this->id, 'show_in_elearning');
    }

    public function getPreviewId()
    {
        $user = BackendAuth::getUser();

        if ($user
            && $user->hasPermission('igvs.courses.pilot_user')
            && $user->hasPermission('igvs.courses.enable_crypt_preview_id')) {
            return Crypt::encode(
                $this->id,
                'course_id'
            );
        }

        return $this->id;
    }

    public function getPreviewIdAttribute()
    {
        return $this->getPreviewId();
    }

    public function isUseProject()
    {
        if (!($user = BackendAuth::getUser()))
            return false;

        return !$user->hasPermission('igvs.courses.pilot_user') && !$this->short_version && $this->project_id;
    }

    public function canCreateTopic()
    {
        if (!$user = BackendAuth::getUser()) {
            return false;
        }
        if ($user->is_superuser || $this->isCourseAdmin($this->id)) {
            return true;
        }
        if (!$this->checkAccessOperation($this->id, 'AllowEditCategories')) {
            return false;
        }
        if ($this->isUseProject()) {
            $roles = ProjectRole::getRolesByProjectIdAndUserId($this->project_id, $user->id);

            $task = new Task();
            $taskTable = $task->getTable();

            $role = new ProjectRole();
            $roleTable = $role->getTable();

            $statusChange = new TaskStatusChange();
            $statusChangeTable = $statusChange->getTable();

            $canChangeStatuse = Task::getBy('course', $this->id)
                ->join($statusChangeTable,
                    "$statusChangeTable.status_id", '=', \DB::raw("`$taskTable`.`status_id` and `$statusChangeTable`.`type_id` = `$taskTable`.`type_id`"))
                ->where("$taskTable.responsible_id", $user->id)
                ->whereIn("$statusChangeTable.role_id", $roles)
                ->count();

            return $canChangeStatuse;
        }
        if ($this->iCanSeeExaminationSystem()) {
            $members = Member::where('user_id', $user->id)->where('course_id', $this->id)->lists('id');
            $roles = MemberRole::whereIn('member_id', $members)->lists('role_id');
            return !!StatusChange::whereIn('role_id', $roles)
                ->whereNull('status_id')
                ->where('status_new_id', $this->status_id)
                ->count();
        }
        return true;
    }

    public function isReadOnly($childInterest = false, $editable = false)
    {
        if (!$user = \BackendAuth::getUser()) {
            return true;
        }
        if ($user->is_superuser || $this->isCourseAdmin($this->id)) {
            return false;
        }

        if ($this->isUseProject()) {
            $roles = ProjectRole::getRolesByProjectIdAndUserId($this->project_id, $user->id);

            $task = new Task();
            $taskTable = $task->getTable();

            $role = new ProjectRole();
            $roleTable = $role->getTable();

            $statusChange = new TaskStatusChange();
            $statusChangeTable = $statusChange->getTable();

            $canChangeStatuse = Task::getBy('course', $this->id, null, $editable)
                ->join($statusChangeTable,
                    "$statusChangeTable.status_id", '=', \DB::raw("`$taskTable`.`status_id` and `$statusChangeTable`.`type_id` = `$taskTable`.`type_id`"))
                ->where("$taskTable.responsible_id", $user->id)
                ->whereIn("$statusChangeTable.role_id", $roles)
                ->count();

            if (!$canChangeStatuse) {
                return true;
            }
            if ($childInterest) {
                return false;
            }
        }
        return !$this->checkAccessOperation($this->id, 'CourseProperty');
    }

    public function beforeCreate()
    {
        $currentUser = BackendAuth::getUser();
        if (is_null($this->for_whom) && $currentUser) {
            $this->for_whom = $currentUser->id;
        }
        if (is_null($this->material_covered)) {
            $this->material_covered = '';
        }
        if (is_null($this->support_materials)) {
            $this->support_materials = '';
        }
        if (is_null($this->shell_version)) {
            $shell_version = ShellVersion::orderBy('id', 'desc')->first();
            $this->shell_version = $shell_version ? $shell_version->id : null;
        }
        if ($this->task) {
            $task = $this->task;

            $user_id = $task && $task->user_id ? $task->user_id : 0;
            $user = \BackendAuth::findUserById($user_id);

            $short_version = $user
                && $user->hasPermission('igvs.courses.pilot_user')
                && $user->hasPermission('igvs.courses.bag_method');
                //&& $this->responsible_id != $task->user_id;

            if ($short_version) {
                $this->short_version = 1;
            }
        }
        if (!isset($this->short_version)) {
            $this->short_version = isset($currentUser)
                && $currentUser->hasPermission('igvs.courses.pilot_user')
                && $currentUser->hasPermission('igvs.courses.bag_method');
        }
        if (empty($this->sort)) {
            $this->sort = 1;
        }
    }

    public function beforeDelete()
    {
        $categories = Category::where('course_id', $this->id)
            ->where('parent_id', null)
            ->get();

        foreach ($categories as $category)
            $category->delete();
    }

    private $prev_status = null;

    public function getPrevStatus()
    {
        return $this->prev_status;
    }

    public function beforeSave()
    {
        if (!$this->parent) {
            $this->parent_id = 0;
        }

        if (!$this->responsible) {
            $this->responsible_id = 0;
        }

        if (!$this->type_id) {
            $this->type_id = 0;
        }

        if (!$this->status) {
            $this->status = 0;
        }

        if (!$this->public_status) {
            $this->public_status = 0;
        }

        if (!$this->theme) {
            $this->theme = '';
        }

        if (!$this->country) {
            $this->country = '';
        }

        $this->prev_status = &$this->original['status_id'];
        if(!$this->responsible) {

            $user = \BackendAuth::getUser();

            if ($user)
                $this->responsible = $user->id;
        }
    }

    public function afterSave()
    {
        $this->modules()->where('status_id', $this->prev_status)->update(['status_id' => $this->status_id]);

        foreach ($this->modules()->get() as $module)
        {
            $module_cache = \Cache::get('is_base_course.module.childs.' . $module->id);

            if (!is_null($module_cache)) {
                foreach ($module_cache as $module_cache_item) {
                    \Cache::forever('is_base_course.module.' . $module_cache_item, $this->is_base_course);
                }
            }
        }
    }

    private function getPotencialRoles() {
        if (!empty($this->task)) {
            $user = \BackendAuth::findUserById($this->task->user_id);
        }
        elseif (!$user = \BackendAuth::getUser()) {
            return Role::whereRaw(0);
        }
        if ($user->hasPermission('igvs.courses.pilot_user') && $this->copy_original_id != 0) {
            return Role::where('id', 6);
        }
        return Role::where('assign_creator', true);
    }

    static public function createCommonFolder($course_id)
    {
        $storage = \Config::get('igvs.courses::modulesContent.path');
        if (!is_dir($dir = "{$storage}/{$course_id}/commonHtmlMatkup")) {
            mkdir($dir, 0777, true);
        }
    }

    public function getPathContents()
    {
        return \Config::get('igvs.courses::modulesContent.path') . "/{$this->id}";
    }

    public function afterCreate()
    {
        self::createCommonFolder($this->id);
        if (!empty($this->task)) {
            $user = \BackendAuth::findUserById($this->task->user_id);
        }
        else {
            $user = \BackendAuth::getUser();
        }

        if ($user) {
            $member = new Member([
                'user_id' => $user->id,
                'course_id' => $this->id
            ]);
            $member->server_name = $this->server_name;
            $member->afterFetch();
            $member->save();

            if ($user->hasPermission('igvs.courses.pilot_user') && $this->copy_original_id != 0) {
                MemberRole::create([
                    'member_id' => $member->id,
                    'course_id' => $this->id,
                    'role_id' => 6
                ]);
            } else {
                $roles = Role::where('assign_creator', true)
                    ->lists('id');

                foreach ($roles as $role_id) {
                    MemberRole::create([
                        'member_id' => $member->id,
                        'course_id' => $this->id,
                        'role_id' => $role_id
                    ]);
                }
            }
        }
    }

    public function getProjectOptions()
    {
        return ['' => 'Select Project'] + Project::getAvailable()->lists('name', 'id');
    }

    public function getLanguageOptions()
    {
        $langs = [
            'en' => 'igvs.courses::lang.default.lang.en',
            'ru' => 'igvs.courses::lang.default.lang.ru',
        ];

        $is_bag_method = 0;

        if ($user = \BackendAuth::getUser()) {
            $is_bag_method = $user->hasPermission('igvs.courses.bag_method');
        }

        if ($is_bag_method) {
            arsort($langs);
        }

        return $langs;
    }

    public function getTypeAttribute()
    {
        $type = Type::getType($this->type_id);
        if($type)
            return $type->name;
    }

    public function getPublicLastStatusAttribute()
    {
        $lang_part = !((int)$this->public_last_user_id) ? 'unpublished' : 'published';
        $title = Lang::get('igvs.courses::lang.course.status.' . $lang_part);
        $icon_part = !((int)$this->public_last_user_id) ? 'unpublished' : "<div style='background-color: #2ecc71;' title='{$title}' class='btn-sm empty suuuuup oc-icon-check'></div>";

        return $icon_part . $title;
    }

    public function getParentIdOptions($keyValue = null, $fieldName = null)
    {
        if($this->id)
            $select = $this->where('id', '<>', $this->id)
                ->where('parent_id','<>',$this->id)
                ->select('id', 'modification_name', 'title','code')
                ->get();
        else
            $select = $this->select('id', 'modification_name', 'title','code')
                ->get();

        $return = [Lang::get('igvs.courses::lang.course.status.no_parent')];
        foreach ($select as $option) {
            $return[$option['id']] =  $option['code'].': '.$option['title'].' ('.$option['modification_name'].')';
        }
        return $return;
    }
    public function getTypeIdOptions($keyValue = null, $fieldName = null)
    {
        return [0=>'-- '.Lang::get('igvs.courses::lang.course.status.no_type')] + Type::all()->lists('name', 'id');
    }

    public function getRoles(array $roles = null)
    {
        if (!$user = \BackendAuth::getUser()) {
            return [];
        }

        $user_id = $user->id;

        $member_id = Member::getMember($this->id, $user_id);

        return MemberRole::getMemberRoles($member_id, $roles);
    }

    public function getLevelQualificationOptions()
    {
        $result = [];
        for ($i = 1; $i <= 6; $i++) {
            $result[$i] = Lang::get('igvs.courses::lang.default.location.level') . " $i";
        }
        return $result;
    }

    /*public function getLogoAttribute()
    {

        $src = '/plugins/igvs/courses/assets/images/img-none.jpg';
        /*if ($record->{$column->columnName}) {
            $src = $record->{$column->columnName}
            ->getThumb(50, 50, ['mode' => 'crop']);
        }
        return '<img src="'.$src.'" width="50" height="50">';
    }*/

    public function getPublicStatusOptions()
    {
        return [
            'unpublished' => Lang::get('igvs.courses::lang.course.status.unpublished'),
            'soon' => Lang::get('igvs.courses::lang.course.status.soon'),
            'published' => Lang::get('igvs.courses::lang.course.status.published'),
        ];
    }

    public function getThemeOptions()
    {
        return [
            '_default' => '_default',
            'blue' => 'blue',
            'green' => 'green',
            'red' => 'red',
        ];
    }



    // public function duplication($modification_name, $title, $code, $copy_type, $user_id, $task_id)
    public function duplication($course_attr, &$task)
    {
        if ($task) {
            $this->task = $task;
        }

        // replicate course
        $newCourse = $this->replicate();

        if ($task) {
            $newCourse->task = $task;
        }

        if ($course_attr['type'] != 'mod') {
            $newCourse->copy_original_id = 0;
        }
        elseif ($this->copy_original_id == 0) {
            $newCourse->copy_original_id = $this->id;
        }
        else {
            $newCourse->copy_original_id = $this->copy_original_id;
        }

        // $newCourse->fill($course_attr);
        $newCourse->parent_id =  $this->id;
        $newCourse->modification_name = $course_attr['modification_name'];
        $newCourse->title = $course_attr['title'];
        $newCourse->type = $course_attr['type'];
        $newCourse->code = $course_attr['code'];
        $newCourse->status_id = $course_attr['status_id'];
        $newCourse->shell_version_id = $this->shell_version_id;
        $newCourse->public_last_user_id = null;
        $newCourse->public_last_datetime = null;

        if ($task && $task->user_id) {
            $newCourse->responsible_id = $task->user_id;
        }


        if ($this->task) {
            $task = $this->task;

            $user_id = $task && $task->user_id ? $task->user_id : 0;
            $user = \BackendAuth::findUserById($user_id);

            $short_version = $user
                && $user->hasPermission('igvs.courses.pilot_user')
                && $user->hasPermission('igvs.courses.bag_method')
                && $this->responsible_id != $task->user_id;

            if ($short_version) {
                $newCourse->short_version = 1;
            }
        }

//        $user_id = $task && $task->user_id ? $task->user_id : 0;
//        $user = \BackendAuth::findUserById($user_id);
//        if ($task && $task->user_id && $this->responsible_id != $task->user_id && $user && $user->hasPermission('igvs.courses.pilot_user')) {
//            $this->short_version = 1;
//        }



        $newCourse->save();

        $this->recourceCopy($this->getPathContents() . '/commonHtmlMatkup', $newCourse->getPathContents() . '/commonHtmlMatkup');

        $task->target_id = $newCourse->id;
        $task->save();

        // copy images
        foreach (['logo', 'bg_hat', 'image'] as $v) {
            if ($this->{$v} && file_exists($p = $this->{$v}->getLocalPath())) {
                $this->attachFile($newCourse, $v, $p);
            }
        }
        // replicate categories
        $categories = Category::where('course_id', $this->id)
            ->where('parent_id', null)
            ->orderBy('sort', 'asc')
            ->get();

        // replicate subcategoies & modules
        foreach ($categories as $category) {
            $newCourse->addCategory($category, $course_attr['type'], null, $task);
        }

        // replicate vocabulary
        $vocabulary = Vocabulary::where('course_id', $this->id)
            ->get();

        foreach ($vocabulary as $term) {
            $term->replicateToCourse($newCourse->id);
        }
    }

    public function addCategory($category, $type = 'new', $move = null, $task = null, $parent_id = null, $before = true) {
        $newCategory = $category->replicate();
        $newCategory->course_id = $this->id;
        $newCategory->parent_id = $parent_id;
        $code_exists = is_null($parent_id) && Category::where('course_id', $this->id)
            ->select('id')
            ->where('parent_id', null)
            ->where('code', $category->code)
            ->first();
        if ($code_exists) {
            $newCategory->code = uniqid();
        }
        $newCategory->readonly = 0;

        if ($type != 'mod') {
            $newCategory->copy_original_id = 0;
        }
        elseif ($category->copy_original_id) {
            $newCategory->copy_original_id = $category->copy_original_id;
        }
        else {
            $newCategory->copy_original_id = $category->id;
        }
        $newCategory->isCopyPaste = true;
        $newCategory->push();
        $newCategory->isCopyPaste = false;
        RubricatorHistory::create([
            'value' => $category->id,
            'type' => 2,//копирование
            'category_id' => $newCategory->id
        ]);

        if (!$parent_id) {
            $subcategories = Category::where('parent_id', $category->id)
                ->orderBy('sort', 'asc')
                ->get();

            foreach ($subcategories as $subcategory) {
                $this->addCategory($subcategory, $type, null, $task, $newCategory->id);
            }
        }
        else {
            $modules = $category->getModules();
            foreach ($modules as $module) {
                $newCategory->copyModuleTo($module, false, $type, $task);
            }
        }
        if ($move === true) {
            $move = self::where('parent_id', $parent_id)
                ->where('id', '!=', $newCategory->id)
                ->orderBy('sort', $before ? 'asc' : 'desc')
                ->first();
        }
        if ($move) {
            $method = 'move' . ($before ? 'Before' : 'After');
            $newCategory->$method($move);
        }
    }

    public function getChildren()
    {
        $stmt = self::where('parent_id', $this->id)
            ->orderBy('sort', 'asc');

        $this->accessFilter($stmt);
        return $stmt->get();
    }

    public function getCourseCodeClean()
    {
        $count = 0;
        return preg_replace("/[^[:alnum:]-_\.]/", '', $this->code, -1, $count);
    }

    public function getParentId()
    {
        if (!$this->parent_id) return 0;
        $stmt = self::where('parent_id', $this->id)
            ->orderBy('sort', 'asc');

        $this->accessFilter($stmt);
        return $stmt->get();
    }

    public function getChildCount()
    {
        $stmt = self::where('parent_id', $this->id)
            ->orderBy('sort', 'asc');

        $this->accessFilter($stmt);
        return $stmt->count();
    }

    public function getAllRoot()
    {
        $stmt = self::where('parent_id', 0)
            ->orderBy('sort', 'asc');

        $this->accessFilter($stmt);
        return $stmt->get();
    }

    public function accessFilter($stmt)
    {
        $user = \BackendAuth::getUser();

        $pilot_user = Session::get('igvs.courses.pilot_user.' . $user->id);

        if (!$user->is_superuser && !$pilot_user) {

            $courses = Member::where('user_id', $user->id)
                ->get()->lists('course_id');

            $stmt->whereIn('id', $courses);
        }

        if ($pilot_user) {
            $stmt->whereIn('id', Session::get('igvs.courses.pilot_user.courses.' . $user->id));
        }

        exit();
    }

    private static function fieldUse($fields, $names, $key, $value = true, $in = true)
    {
        $names = (array) $names;
        $key = (array) $key;

        $keys = [];
        foreach ($key as $k => $v) {
            if (is_int($k)) {
                $keys[$v] = $value;
            }
            else {
                $keys[$k] = $v;
            }
        }
        foreach ($fields as $name => $options) {
            if (in_array($name, $names) == $in) {
                foreach ($keys as $key => $value) {
                    $options->$key = $value;
                }
            }
        }
    }

    public function canStatusIdField($pilot, $pilot_bag_method)
    {
        if (!$this->short_version && !($pilot && $pilot_bag_method)) {
            return false;
        }
        $id = $this->id;
        $this->id = null;
        $status_id = $this->status_id;
        $this->status_id = null;
        $statuses = $this->getStatusOptions();
        $this->id = $id;
        $this->status_id = $status_id;
        return count($statuses) >= 2;
    }

    public function filterFields($fields, $condition = null)
    {
        $user = \BackendAuth::getUser();
        switch ($condition) {
            case 'copy':
                $pilot = $user && $user->hasPermission('igvs.courses.pilot_user');
                $pilot_bag_method = $user && $user->hasPermission('igvs.courses.bag_method');
                $create_course = $user && $user->hasAccess('igvs.courses.create');
                if (!$pilot || $pilot_bag_method || !$create_course) {
                    self::fieldUse($fields, 'type', 'hidden');
                }
                if (!$this->canStatusIdField($pilot, $pilot_bag_method)) {
                    self::fieldUse($fields, 'status_id@copy', 'hidden');
                }
                return;
        }
        if (!$user->hasPermission('igvs.courses.pilot_user')) {
            self::fieldUse($fields, 'modification_name', 'hidden');
        }
        if (!$this->iCanSeeExaminationSystem()) {
            self::fieldUse($fields, ['comments', 'materials', 'materials_update', 'working_programm_comments', 'working_programm_frame'], 'hidden');
        }
        if ($user->hasPermission('igvs.courses.pilot_user') && !$user->is_superuser) {
            self::fieldUse($fields, ['short_version'], 'hidden');
        }
        if (!$this->isCourseAdmin($this->id) && !$this->checkAccessOperation($this->id, 'allow_change_rights')) {
            self::fieldUse($fields, ['members', 'update_members'], 'hidden');
        }
        if ($user->hasPermission('igvs.courses.pilot_user')
            && !$user->hasPermission('igvs.courses.pilot_export')
            && $user->hasPermission('igvs.courses.bag_method')
            && $this->checkAccessRights($this->id, 'show_in_elearning')
            && in_array(1, $this->getRoles())
        ) {
            self::fieldUse($fields, ['members', 'update_members'], 'hidden');
        }

        $allowUpdateCourseVersions = $this->id
            && $this->checkAccessOperation($this->id, 'AllowUpdateCourseModules')
            && $user->hasAccess('igvs.courses.show_tab_update_versions');

        if (!$allowUpdateCourseVersions) {
            self::fieldUse($fields, 'update_versions', 'hidden');
        }
        if (!$user->hasPermission('igvs.courses.bag_method')) {
            self::fieldUse($fields, 'expertise', 'hidden');
        }
        if (!$user->is_superuser) {
            self::fieldUse($fields, 'is_base_course', 'hidden');
        }
        if (\Session::get('igvs.courses.is_expertise_system', 0)) {
            self::fieldUse($fields, 'short_version', 'hidden');
        }
        if (!$this->checkAccessOperation($this->id, 'dictionaryAccess')
            && !$this->checkAccessOperation($this->id, 'dictionaryAccess')
        ) {
            self::fieldUse($fields, ['update_vocabulary', 'vocabulary', 'vocabulary_frame'], 'hidden');
        }
        if (!$user->hasAccess('igvs.courses.show_tab_rubricator_history')) {
            self::fieldUse($fields, 'rubricator_history', 'hidden');
        }
        if ($this->short_version || !$this->project_id) {
            self::fieldUse($fields, 'project_comments', 'hidden');
        }
        if ($user->hasPermission('igvs.courses.pilot_user') && $user->hasPermission('igvs.courses.bag_method')) {
            self::fieldUse($fields, [
                'colontitle_logo', 'colontitle_slogan', 'parent_id', 'type_id',
                'status', 'public_status', 'theme',
                'shell_version', 'include_all_themes',
                'show_module_start_screen', 'sort', 'project',
                'modification_name', 'country', 'parent',
                'enable_html_markup_number_page',
                'enable_shell_comments',
                'enable_shell_favorites',
                'enable_shell_screen_capture',
            ], 'hidden');
            self::fieldUse($fields, 'short_version', 'value');
        }
        if (isset($fields->status_id)
            && !$this->id
            && count($this->getStatusOptions()) < 2
        ) {
            self::fieldUse($fields, 'status_id', 'cssClass', 'hidden');
        }
        if (!$user->is_superuser) {
            self::fieldUse($fields, ['constructor_all_privileges'], 'hidden');
        }
        if ($condition != 'materials' && $this->isReadOnly() && $this->checkAccessOperation($this->id, 'AllowEditCategories')) {
            self::fieldUse($fields, ['materials', 'working_programm_comments', 'working_programm_frame'], 'hidden');
        }
        else {
            self::fieldUse($fields, 'materials_update', 'hidden');
        }
        if (!$user->is_superuser && !$user->hasPermission('igvs.courses.is_inclusive')) {
            self::fieldUse($fields, ['is_inclusive'], 'hidden');
        }
        if (!$user->is_superuser
            && !$user->hasPermission('igvs.courses.amounts_report')) {
            self::fieldUse($fields, 'update_amount', 'hidden');
        }
    }

    public function getStatusOptions()
    {
        if (!$this->id) {
            $roles = $this->getPotencialRoles()->lists('id');
        }
        else {
            if (!$user = BackendAuth::getUser()) {
                return [];
            }
            $members = Member::where('user_id', $user->id)->where('course_id', $this->id)->lists('id');
            $roles = MemberRole::whereIn('member_id', $members)->lists('role_id');
        }

        $query = StatusChange::whereIn('role_id', $roles)
            ->where('status_id', $this->status_id);

        if (is_null($this->status_id)) {
            $query->whereHas('status_new', function($status) {
                $status->where('created', true);
            });
        }
        $statuses = $query->lists('status_new_id');
        $statuses[] = $this->status_id;

        return Status::whereIn('id', $statuses)->orderBy('name')->lists('name', 'id');
    }

    public function getUserRolesAttribute()
    {
        if ($this->return_comment) {
            $return = [];
            $result = [];

            $stmt = Member::where('user_id', $this->return_comment->user_id)
                ->where('course_id', $this->id)->first();
            if ($stmt) {
                $result = $stmt->roles()->get();
            }

            foreach ($result as $item) {
                $return[] = $item->name_lang;
            }

            $names = Lang::get('igvs.courses::lang.role.names');
            foreach ($return as $k => $v) {
                $return[$k] = isset($names[$v]) ? $names[$v] : $v;
            }

            return $return;
        }
    }

    public function getFullNameLoginAttribute()
    {
        if ($this->return_comment) {
            $backend_user = \BackendAuth::getUser();
            $user = \BackendAuth::findUserById($this->return_comment->user_id);

            if ($backend_user->hasPermission('igvs.courses.pilot_user')) {
                $user_pilot = UserPilot::where('user_id', $this->return_comment->user_id)->first();
            }
            if (empty($user_pilot)) {
                $login = $user->attributes['login'];
            }
            elseif ($user_pilot->email) {
                $login = $user_pilot->email;
            }
            else {
                $login = $user->email;
            }
            return $user->attributes['first_name'] . ' ' . $user->attributes['last_name'] . ' (' . $login . ')';
        }
    }

    public function scopeDeveloper($query, $users)
    {
        $query->whereHas('members', function($members) {
            $members->whereHas('roles', function($roles) {
                $roles->where('role_id', self::DEVELOPER);
            });
        });
    }

    public function getRelationResponsibleOptions($count, $page)
    {
        $post = post('value', '');
        $post = str_replace('  ', ' ', trim($post));
        $post = strlen($post) ? explode(' ', $post) : [];

        $users_count_query = User::leftjoin('igvs_courses_user_pilot', 'igvs_courses_user_pilot.user_id', '=', 'backend_users.id');
        $users_count_query->where(function($users_count) use ($post) {
            foreach ($post as $item) {
                $users_count = $users_count->orWhere(function($q) use($item) {
                    $q->whereRaw("first_name like \"%{$item}%\"")
                        ->orWhereRaw("last_name like \"%{$item}%\"")
                        ->orWhereRaw("backend_users.email like \"%{$item}%\"")
                        ->orWhereRaw("igvs_courses_user_pilot.email like \"%{$item}%\"");
                });
            }
        });
        $users_count = $users_count_query->count();

        $users = User::limit($count)
            ->offset(($page - 1) * $count);
        $users->leftjoin('igvs_courses_user_pilot', 'igvs_courses_user_pilot.user_id', '=', 'backend_users.id');
        $users->where(function($users) use ($post) {
            foreach ($post as $item) {
                $users = $users->orWhere(function($q) use($item) {
                    $q->whereRaw("first_name like \"%{$item}%\"")
                        ->orWhereRaw("last_name like \"%{$item}%\"")
                        ->orWhereRaw("backend_users.email like \"%{$item}%\"")
                        ->orWhereRaw("igvs_courses_user_pilot.email like \"%{$item}%\"");
                });
            }
        });

        $users = $users->get([
            'backend_users.first_name',
            'backend_users.last_name',
            'backend_users.login',
            'backend_users.id',
            'backend_users.email',
            'igvs_courses_user_pilot.email as pilot_email',
        ]);

        $items = [];

        foreach ($users as $user) {
            if (is_null($user->pilot_email)) {
                $login_email = $user->login;
            }
            elseif ($user->pilot_email && strlen($user->pilot_email)) {
                $login_email = $user->pilot_email;
            }
            else {
                $login_email = $user->email;
            }

            $text = "{$user->first_name} {$user->last_name} ({$login_email})";

            $items[] = [
                'id' => $user->id,
                'text' => $text,
            ];
        }
        return [
            'total_count' => $users_count,
            'items' => $items
        ];
    }

    public function scopeGetDeveloper($query)
    {
        $is_pilot_user = \BackendAuth::getUser()->hasPermission('igvs.courses.pilot_user');
        $user_instance_id = \Session::get('igvs.courser.user_instance_id', null);

        $member = new Member();
        $members = $member->getTable();
        $memberRole = new MemberRole();
        $memberRoles = $memberRole->getTable();
        $user = new User();
        $users = $user->getTable();
        $courses = $this->getTable();
        $developer = self::DEVELOPER;
        $userPilot = new UserPilot();
        $userPilots = $userPilot->getTable();

        $query->addSelect(\DB::raw("
            (
                SELECT
                    GROUP_CONCAT(CONCAT(first_name, ' ', last_name, ' (', " . ($is_pilot_user ? "IF($userPilots.user_id, $userPilots.email, $users.email)" : "login") . ", ')') SEPARATOR ', ') developer_name
                FROM $memberRoles
                LEFT JOIN $members
                    ON $members.id = $memberRoles.member_id
                LEFT JOIN $users
                    ON $users.id = $members.user_id" . ($is_pilot_user ? "
                LEFT JOIN $userPilots
                    ON $userPilots.user_id = $members.user_id" : '') . "
                WHERE
                    $memberRoles.role_id = $developer
                    AND $members.course_id = $courses.id" . ($is_pilot_user ? "
                    AND $userPilots.instance_id = $user_instance_id" : '') . "
            ) developer_name"));
    }

    public function scopeGetResponsible($query)
    {
        $user = new \Igvs\Courses\Models\User();
        $users = $user->getTable();
        $courses = $this->getTable();

        $query->leftJoin($users, "$users.id", '=', "$courses.responsible_id")
            ->addSelect([
                "$users.login as responsible_login",
                "$users.first_name as responsible_first_name",
                "$users.last_name as responsible_last_name",
                "$users.email as responsible_email",
            ]);
    }

    public static function findFirst($course_id)
    {
        $course = Course::select('copy_original_id')
            ->where('id', $course_id)
            ->first();

        return $course ? $course->copy_original_id : 0;

        //лучше бы конечно так:
        if (!$course) {
            return null;//курс не найден
        }
        if (is_null($course->copy_original_id)) {
            return $course_id;//а ещё можно по парентам пройтись
        }
        return $course->copy_original_id;
    }

    public static function isFirst($course_id)
    {
        return self::findFirst($course_id) == 0 ? true : false;

        //лучше бы конечно так:
        $isFirst = self::findFirst($course_id);
        if (is_null($isFirst)) {
            return null;//куср не найден
        }
        return $isFirst == $course_id;
    }

    public function scopeGetNested($query, $parent_id = 0)
    {
        $query->where('parent_id', $parent_id);
    }

    public function getCurrentThemeCode()
    {
        if ($this->theme && !$this->is_inclusive) {
            return $this->theme;
        } elseif ($this->theme && $this->is_inclusive) {
            return 'inclusive';
        } else {
            return '';
        }
    }
}
