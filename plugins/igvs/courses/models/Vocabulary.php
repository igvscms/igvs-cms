<?php namespace Igvs\Courses\Models;

use Model;
use Igvs\Courses\Scopes\VocabularyListOrder;

/**
 * Vocabulary Model
 */
class Vocabulary extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_vocabularies';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'course_id',
        'user_id',
        'term',
        'term_clean',
        'description',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => 'System\Models\File',
    ];
    public $attachMany = [];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new VocabularyListOrder);
    }

    public function beforeCreate()
    {
        $this->term_clean = strip_tags($this->term);
    }

    public function beforeSave()
    {
        $this->term_clean = strip_tags($this->term);

        $user = \BackendAuth::getUser();

        $this->user_id = $user ? $user->id : null;
    }

    public function getTermCleanAttribute()
    {
        return strip_tags($this->term);
    }

    public function getDescriptionCleanAttribute()
    {
        return strip_tags($this->description);
    }

    public function replicateToCourse($course_id)
    {
        $new_term = $this->replicate();
        $new_term->course_id = $course_id;
        $new_term->image = $this->image ? $this->image->getLocalPath() : null;
        $new_term->push();

        if ($this->image) {
            $file = new \System\Models\File;
            $file->data = $this->image->getLocalPath();
            $file->is_public = true;
            $file->save();

            $new_term->image()->add($file);
        }
    }
}
