<?php namespace Igvs\Courses\Models;

use Academy\Api\Models\Client;
/**
 * Administrator user model
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class BackendUser extends \Backend\Models\User
{
    public $belongsToMany = [
        'groups' => [
            'Backend\Models\UserGroup',
            'table' => 'backend_users_groups',
            'key' => 'user_id',
            'other_key' => 'user_id',
        ]
    ];

    /**
     * Returns an array of merged permissions for each group the user is in.
     * @return array
     */
    public function getMergedPermissions()
    {
        if (!$this->mergedPermissions) {
            $permissions = [];
            $client_instance = null;
            $virtual_groups = [];
            $virtual_group_ids = [];

            //если к нам зашли через oauth, то при авторизации мы добавляем виртуальные группы,
            //установленные в настройках
            $user_instance_id = \Session::get('igvs.courser.user_instance_id', null);
            $user_instance_type = \Session::get('igvs.user_instance_type', null);

            if ($user_instance_id) {
                $user_instance = Client::where('id', $user_instance_id)->first();

                switch ($user_instance_type) {
                    case 'pilot':
                        $group_setting_name = 'virtual_group';
                        break;
                    case 'inclusive':
                        $group_setting_name = 'virtual_group_inclusive';
                        break;
                    default:
                        $group_setting_name = 'virtual_group';
                }

                if ($user_instance)
                    $virtual_group_ids = $user_instance->{$group_setting_name};
            }

            if (is_array($virtual_group_ids) && count($virtual_group_ids))
                $virtual_groups = \Backend\Models\UserGroup::whereIn('id', $virtual_group_ids)
                    ->get();

            //добавляем права из виртуальных группы в общую кучу
            foreach ($virtual_groups as $group) {
                if (!is_array($group->permissions)) {
                    continue;
                }

                $permissions = array_merge($permissions, $group->permissions);
            }

            //всё, что ниже так и было
            foreach ($this->getGroups() as $group) {
                if (!is_array($group->permissions)) {
                    continue;
                }

                $permissions = array_merge($permissions, $group->permissions);
            }

            if (is_array($this->permissions)) {
                $permissions = array_merge($permissions, $this->permissions);
            }

            $this->mergedPermissions = $permissions;
        }

        return $this->mergedPermissions;
    }
}
