<?php namespace Igvs\Courses\Models;

use Model;

/**
 * StorageStatsDetail Model
 */
class StorageStatsDetail extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_storage_stats_details';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'storage_stat_id', 'path', 'size', 'inode'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'stat' => ['Igvs\Courses\Models\StorageStats']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
