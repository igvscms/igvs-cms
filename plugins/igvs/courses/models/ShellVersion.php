<?php namespace Igvs\Courses\Models;

use Model;
use Lang;

/**
 * shellVersions Model
 */
class ShellVersion extends Model
{
    use \October\Rain\Database\Traits\SoftDeleting;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_shell_versions';

    /**
     * @var array Dates fields
     */
    protected $dates = ['deleted_at', 'updated_dt', 'created_at', 'release_date'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public function getStatusOptions()
    {
        return [
            'alpha' => Lang::get('igvs.courses::lang.module.status.alpha'),
            'beta' => Lang::get('igvs.courses::lang.module.status.beta'),
            'stable' => Lang::get('igvs.courses::lang.module.status.stable'),
            'unstable' => Lang::get('igvs.courses::lang.module.status.unstable'),
        ];
    }

    public function getStatusAttribute()
    {
        $statuses = [
            'alpha' => Lang::get('igvs.courses::lang.module.status.alpha'),
            'beta' => Lang::get('igvs.courses::lang.module.status.beta'),
            'stable' => Lang::get('igvs.courses::lang.module.status.stable'),
            'unstable' => Lang::get('igvs.courses::lang.module.status.unstable'),
        ];

        $status = isset($this->attributes['status']) ? $this->attributes['status'] :
            null;

        return isset($statuses[$status]) ? $statuses[$status] : '';
    }

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}