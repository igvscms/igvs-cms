<?php namespace Igvs\Courses\Models;

use Model;
use Config;

/**
 * Module Model
 */
class Template extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_content_templates';

    /**
     * @var array Dates fields
     */
    protected $dates = ['updated_dt', 'created_at'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'group' => ['Igvs\Courses\Models\TemplatesGroup', 'foreignKey' => 'group_id'],
        'module' => ['Igvs\Courses\Models\Module', 'foreignKey' => 'module_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


}