<?php namespace Igvs\Courses\Models;

use Model;
use Lang;

/**
 * ModuleVersion Model
 */
class ModuleVersion extends Model
{
    use \October\Rain\Database\Traits\SoftDeleting;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_modules_versions';

    /**
     * @var array Dates fields
     */
    protected $dates = ['deleted_at', 'updated_dt', 'created_at', 'release_date'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'module_id',
        'version',
        'status',
        'release_date',
        'release_notes',
        'developer',
        'developer_comment',
        'behaviors',
        'special',
    ];

    public function getStatusOptions()
    {
        return [
            'alpha' => Lang::get('igvs.courses::lang.module.status.alpha'),
            'develop' => Lang::get('igvs.courses::lang.module.status.develop'),
            'stable' => Lang::get('igvs.courses::lang.module.status.stable'),
            'broken' => Lang::get('igvs.courses::lang.module.status.broken'),
        ];
    }

    public function getStatusAttribute()
    {
        $statuses = [
            'alpha' => Lang::get('igvs.courses::lang.module.status.alpha'),
            'develop' => Lang::get('igvs.courses::lang.module.status.develop'),
            'stable' => Lang::get('igvs.courses::lang.module.status.stable'),
            'broken' => Lang::get('igvs.courses::lang.module.status.broken'),
        ];

        $status = isset($this->attributes['status']) ? $this->attributes['status'] :
            null;

        return isset($statuses[$status]) ? $statuses[$status] : '';
    }

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'module' => ['Igvs\Courses\Models\Module', 'foreignetKey' => 'module_id'],];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getFullVersionAttribute()
    {
        return "$this->version ($this->status : " . strstr($this->created_at, ' ', true) . ")";
    }
}