<?php namespace Igvs\Courses\Models;

use Model;

/**
 * publication_state Model
 */
class PublicationState extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_publication_states';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'hash',

        'row_type',
        'course_id',
        'topic_id',
        'act_id',
        'module_template_id',
        'module_content_id',
        'course_code',
        'topic_code',
        'module_content_code',
        'topic_is_separator',
        'course_name',
        'topic_name',
        'act_name',
        'module_template_name',
        'module_content_name',
        'behavior',
        'order',
        'is_active_publication',

        'created_at',
        'updated_at',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
