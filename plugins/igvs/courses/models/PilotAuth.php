<?php namespace Igvs\Courses\Models;

use Config;

class PilotAuth {

    private $secret_key = 'sjvrg8mp9ent96cs5u3b';
    private $client_id = 'ayo991w405e6kaog4now';

    private $url = '';

    public $params;


    public function __construct(array $params = [])
    {
        $this->setParams($params);
        $this->url = Config::get('igvs.courses::export_pilot.response_url');
    }

    public function getUserInfo()
    {
        if (!$this->getParam('user_id')) return false;

        $options = [
            'user_id' => $this->getParam('user_id'),
        ];

        $req = $this->request($options, 'user_info');

        try {
            /* dummy */
//            $req['content'] = '{"first_name":"Maria","last_name":"Malinina","email":"malinina.mm@academia-moscow.ru","courses":{"13":2,"1":2,"6":2,"2":2,"0":2}}';
            $user = json_decode($req['content']);

            if (!isset($user->first_name)
                || !isset($user->last_name)
                || !isset($user->email)
                || !isset($user->courses)) {
                return false;
            }
        } catch(\Exception $e) {
            return false;
        }

        return $user;
    }

    public function getFreeLicence()
    {
        if (!$this->getParam('user_id')) return false;

        $options = [
            'user_id' => $this->getParam('user_id')
        ];

        $req = $this->request($options, 'licencies');

        /* dummy */
//        $req['content'] = '{"1":{"pilot_course_id":1, "name":"FPC1", "free":3}, "2":{"pilot_course_id":2, "name":"FAB", "free":5}}';

        if (!$req || !isset($req['content']))
            return false;

        try {
            $req = json_decode($req['content']);
        } catch (\Exception $e) {
            return false;
        }

        return $req;
    }

    public function useLicence($course_id, $pilot_course_id)
    {
        $options = [
            'user_id' => $this->getParam('user_id'),
            'course_id' => $course_id,
            'pilot_course_id' => $pilot_course_id,
        ];

        $req = $this->request($options, 'use_licence');

        /* dummy */
        $req['content'] = 'true';

        return $req['content'];
    }

    public function endpointExport($course_id, $pilot_course_id, $parent_course_id, $export_path)
    {
        $options = [
            'user_id' => $this->getParam('user_id'),
            'course_id' => $course_id,
//            'licence_id' => $licence_id,
            'pilot_course_id' => $pilot_course_id,
            'parent_course_id' => $parent_course_id ? $parent_course_id : 'new',
            'export_path' => $export_path
        ];

        $req = $this->request($options, 'endpoint_export');

        /* dummy */
        $req['content'] = 'true';

        return $req['content'];
    }

//    public function

    public function request(array $options = [], $method)
    {
        if (!is_array($options))
            new \Exception ('Options is not array');

        if (!is_string($method))
            new \Exception ('Method is not string');

        $options = array_merge($options, [
            'client_id' => $this->client_id,
        ]);
        $options = [
            'signed_data' => json_encode($options),
            'signature' => md5(
                    urldecode(http_build_query($options)) . $this->secret_key
            ),
            'method' => $method
        ];

        $uagent = "Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.14";

        $ch = curl_init( $this->url );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   // возвращает веб-страницу
        curl_setopt($ch, CURLOPT_HEADER, 0);           //выводим заголовки
        curl_setopt($ch, CURLOPT_POST, 1);             //передача данных методом POST
        curl_setopt($ch, CURLOPT_ENCODING, "");        // обрабатывает все кодировки
        curl_setopt($ch, CURLOPT_USERAGENT, $uagent);  // useragent
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // таймаут соединения
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);        // таймаут ответа
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);       // останавливаться после 10-ого редиректа
        curl_setopt($ch, CURLOPT_POSTFIELDS,           //тут переменные которые будут переданы методом POS
                $options
            );

        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;

        return $header;
    }

    private function setParams(array $params = [])
    {
        $this->params  = $params;
    }

    public function checkTrust()
    {
        $params = $this->params;

        if (!is_array($params))
            return false;

        if (!isset($params['signature']) || !isset($params['signed_data'])) {
            return false;
        }

        $str = '';
        $params_count = count($params['signed_data']) - 1;
        $params_i = 0;

        foreach($params['signed_data'] as $k => $v) {
            $params_i++;
            $str .= "{$k}={$v}";
            if ($params_i < $params_count) {
                $str .= '&';
            }
        }

        $uri = $str . $this->secret_key;

        if ($params['signature'] != md5($uri)) {
            echo 'error auth<br>';
            return false;
        }

        return true;
    }

    public function getParam($param)
    {
        if (!isset($this->params['signed_data'])) return null;
        return isset($this->params['signed_data'][$param]) ? $this->params['signed_data'][$param] : null;
    }

}