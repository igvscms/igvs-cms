<?php namespace Igvs\Courses\Models;

use Model;
use Igvs\Courses\Models\BuildModuleContent;

/**
 * build Model
 */
class Build extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_builds';

    /**
     * @var array Validation rules
     */
    protected $rules =[
        'name' => ['required', 'between:2,255'],
        'code' => ['required', 'between:2,50', 'unique:igvs_courses_builds'],
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];

    public $belongsToMany = [
        'users' => [
            'backend\Models\User',
            'table' => 'igvs_courses_builds_users',
            'key' => 'build_id',
            'otherKey' => 'user_id'
        ]
    ];

    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function getBuildModulesByCourseId($course_id, $build_id)
    {
        return BuildModuleContent::whereIn('module_content_id', ModuleContent::where('course_id', $course_id)->lists('id'))
            ->where('build_id', $build_id)
            ->lists('module_content_id');
    }
}
