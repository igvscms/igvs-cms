<?php namespace Igvs\Courses\Models;

use Model;
use Lang;

/**
 * ConstructorVersion Model
 */
class ConstructorVersion extends Model
{
    use \October\Rain\Database\Traits\SoftDeleting;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_constructor_versions';

    /**
     * @var array Dates fields
     */
    protected $dates = ['deleted_at', 'updated_dt', 'created_at', 'release_date'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'constructor' => ['Igvs\Courses\Models\Constructor', 'foreignetKey' => 'constructor_id'],];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getStatusOptions()
    {
        return [
            'alpha' => Lang::get('igvs.courses::lang.constructor.status.alpha'),
            'beta' => Lang::get('igvs.courses::lang.constructor.status.beta'),
            'stable' => Lang::get('igvs.courses::lang.constructor.status.stable'),
            'unstable' => Lang::get('igvs.courses::lang.constructor.status.unstable'),
        ];
    }

}