<?php namespace Igvs\Courses\Models;

use Model;
use Igvs\Courses\Models\Course;

/**
 * Role Model
 */
class Type extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    /**
     * @var array Validation rules
     */
    protected $rules =[
        'name' => 'required|between:3,255|unique:igvs_courses_types',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_types';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeDelete()
    {
        Course::where('type_id',$this->id)->update(['type_id2'=>0]);
    }

    static private $types = [];


    static public function getType($id)
    {
        self::fillTypesCache();

        if (isset(self::$types[$id]))
            return self::$types[$id];

        return null;
    }

    static private function fillTypesCache()
    {
        if (!count(self::$types)) {
            $types = self::all();

            foreach ($types as $type) {
                self::$types[$type->id] = $type;
            }
        }
    }
}
