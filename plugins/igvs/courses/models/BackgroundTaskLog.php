<?php namespace Igvs\Courses\Models;

use Model;

/**
 * BackgroundTaskLog Model
 */
class BackgroundTaskLog extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_background_tasks_logs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'act',
        'options',
        'message',
        'db_id',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}