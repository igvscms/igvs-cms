<?php namespace Igvs\Courses\Models;

//use Model;
use Input;
use BackendAuth;

use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\RubricatorHistory;
use Igvs\Courses\Models\Course;

use Academy\System\Classes\Socket\Pusher;

use Academy\Tasks\Models\Role as ProjectRole;
use Academy\Tasks\Models\Task;
use Academy\Tasks\Models\StatusChange as TaskStatusChange;

/**
 * Category Model
 */
class Category extends \Model
{
    // use \October\Rain\Database\Traits\NestedTree;
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    use \October\Rain\Database\Traits\SoftDelete;

    use \Igvs\Courses\Traits\FileHelper;
    use \Igvs\Courses\Traits\CheckAccess;
    use \Igvs\Courses\Traits\CourseProperty;
    use \Academy\System\Traits\Sorting;

    private static $cacheReadonly = null;

    public static $copied = [];

    private static $cacheCategories = [];

    public $configSorting  = [
        'fldSort' => 'sort',
        'fldParent' => 'parent_id',
        'conditions' => ['course_id']
    ];

    public static function boot()
    {
        parent::boot();
        self::deleting(function($model) {
            $model->code .= '_del_' . uniqid();
            $model->forcesave();
        });
    }

    public static function getCategoryCache($id)
    {
        if (!isset(self::$cacheCategories[$id])) {
            self::$cacheCategories[$id] = self::find($id);
        }

        return self::$cacheCategories[$id];
    }

    public static function eventCopied($model, &$task)
    {
        $curCourse = Course::find($model->course_id);
        $srcCourse = Course::find($curCourse->parent_id);

        if (isset(self::$copied[$model->course_id])) {
            self::$copied[$model->course_id]['cnt']++;
        }
        else {

            $cntAll = ModuleContent::where('course_id', $srcCourse->id)
                ->count();

            self::$copied[$model->course_id]['all'] = $cntAll;
            self::$copied[$model->course_id]['cnt'] = 1;
        }

        $cnt = self::$copied[$model->course_id]['cnt'];
        $all = self::$copied[$model->course_id]['all'];

        $percent = $cnt / $all;

        // пишем состояние
        $task->fill([
            'comment' => "Copied {$cnt} of {$all}",
            'progress' => $percent,
        ]);

        $task->save();

        // транслируем
        Pusher::sentDataToServer([
            'topic_id' => 'user_' . $task->user_id,
            'data' => $task->toArray()
        ]);

        // sleep(2);
    }

    /**
     * @var array Validation rules
     */
    protected $rules =[
        'name' => ['max:255'],
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_categories';

    /**
     * @var array Dates fields
     */
    protected $dates = ['deleted_at', 'updated_dt', 'created_at'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['sort'];

    /**
     * @var array Relations
     */
    public $hasOne = [];

    public $hasMany = [
        'contents' => [
            'Igvs\Courses\Models\ModuleContent',
            'primaryKey' => 'category_id',
            'order' => 'sort ASC'
        ],
        'act_tasks' => [
            'Academy\Tasks\Models\Task',
            'key' => 'act_id',
        ],
        'topic_tasks' => [
            'Academy\Tasks\Models\Task',
            'key' => 'topic_id',
        ]
    ];

    public $belongsTo = [
        'parent' => [
            'Igvs\Courses\Models\Category',
        ],
        'course' =>'Igvs\Courses\Models\Course',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public $isCopyPaste = false;

    public function beforeDelete()
    {
        # delete modules
        $modules = $this->getModules();
        foreach ($modules as $module) {
            $module->delete();
        }

        # delete category
        $childrens = $this->getChildren();
        foreach ($childrens as $category) {
            $category->delete();
        }
    }

    public function beforeValidate()
    {
        // deleted_at,NULL
        if (!$this->parent_id) {
            $this->rules = array_merge($this->rules, [
                'code' => ['max:255', 'unique:igvs_courses_categories,code,NULL,id,course_id,' . $this->course_id],
            ]);
        }
    }

    public function beforeCreate()
    {
        if(empty($this->course_id) && isset($this->parent_id))
            $this->course_id = $this::find($this->parent_id)->course_id;

        if (empty($this->code)) {
            $this->code = uniqid();
        }

        // если сортировка явно не указана, добавляем в конец
        if (!$this->sort) {
            $this->sort = self::where('course_id', $this->course_id)
                ->where('parent_id', $this->parent_id)
                ->max('sort') + 1;
        }
    }

    public function afterCreate()
    {
        $user = \BackendAuth::getUser();

        if(!$this->isCopyPaste) {
            RubricatorHistory::create([
                'value' => $this->code . ': ' . $this->name,
                'type' => 0,//type = 0 создание
                'category_id' => $this->id
            ]);
        }

        if ($user) {
            if ($this->parent_id) {
                //clear cache get_act_options
                \Cache::forget("task.get_act_options.{$user->id}_{$this->parent_id}");
            } else {
                //clear cache get_topic_options
                \Cache::forget("task.get_topic_options.{$user->id}_{$this->course_id}");
            }
        }
    }

    public function beforeUpdate()
    {
        $prev = self::where('id',$this->id)->select('name','code')->first();
        $updates = [];
        if ($prev->name != $this->name) {
            $updates[] = [
                'value' => $prev->name,
                'type' => 4,
                'category_id' => $this->id
            ];//type = 4 - переименовывание
        }
        if ($prev->code != $this->code) {
            $updates[] = [
                'value' => $prev->code,
                'type' => 5,
                'category_id' => $this->id
            ];//type = 5 - изменение кода
        }
        if (count($updates)) {
            foreach ($updates as $array) {
                RubricatorHistory::create($array);
            }
        }
    }

    public function copyCategoryTo($model, $type = 'new', $move = true, $task = null)
    {
        $this->course->addCategory($model, $type, $move, $task, $this->id);
    }

    public function copyModuleTo($model, $doFirst = true, $copy_type = 'new', $task = null)
    {
        $count = ModuleContent::withTrashed()
            ->whereRaw('code = ? and course_id = ?', [$model->code, $this->course_id])
            ->count();

        // replicate
        $new = $model->replicate();
        $new->category_id = $this->id;
        $new->course = $this->course;
        $new->course_id = $this->course_id;
        $new->code = $count ? uniqid() : $new->code;
        $new->init_by_tpl = 0;
        $new->status_id = $new->course->status_id;
        $new->parent_id = $model->id;

        $new->copy_original_id = $copy_type == 'mod' ? $model->id : 0;

        $new->do_not_touch_sort = false;
        $new->data = $model->original['data'];
        $new->save();

        $this->recourceLn($model->getPathContents(), $new->getPathContents());

        // move to before && reorder
        if ($doFirst) {
            $first = ModuleContent::where('category_id', $this->id)
                ->where('id', '!=', $new->id)
                ->orderBy('sort', 'asc')
                ->first();

            if ($first)
                $new->moveBefore($first);
        }

        if($task)
            self::eventCopied($new, $task);

        return $new;
    }

    public function setParentIdAttribute($value)
    {
        $this->attributes['parent_id'] = $value ? $value : null;
    }

    public function getModules()
    {
        return $this->contents;
    }

    public function getParentIdAttribute()
    {
        return isset($this->attributes['parent_id']) ? $this->attributes['parent_id'] :
            Input::get('parent_id');
    }

    public function getStatusOptions()
    {
        return [
            null => '&nbsp;',
            'alpha' => 'Alpha',
            'pre-beta' => 'Pre-Beta',
            'beta' => 'Beta',
            'production' => 'Production'
        ];
    }

    public function getCourseIdAttribute()
    {
        return isset($this->attributes['course_id']) ? $this->attributes['course_id'] :
            Input::get('course_id');
    }

    public function canCreateAct()
    {
        return !$this->parent_id && !$this->isReadonly();
    }

    public function canCreateModule()
    {
        return $this->parent_id && !$this->isReadonly();
    }

    public function isReadonly($childInterest = false, $editable = false)
    {
        if (!$user = \BackendAuth::getUser()) {
            return true;
        }
        if ($childInterest) {
            if ($user->is_superuser || $this->isCourseAdmin($this->course_id)) {
                return false;
            }
            if ((($this->readonly || ($this->parent_id && self::getCategoryCache($this->parent_id)->readonly))
                && !$this->checkAccessOperation($this->course_id, 'UncheckReadonlyCategory')
                && !($this->checkAccessOperation($this->course_id, 'EditContentLockedModuleTask') && $this->coursePropertyIsUseProject()))
                    ||!$this->checkAccessOperation($this->course_id, 'AllowEditCategories')
            ) {
                return true;
            }
        }

        if (!isset($this->course_id) && isset($this->parent_id)) {
            $this->course = self::getCategoryCache($this->parent_id)->course;
        }

        /* @see \Igvs\Courses\Models\Course::isUseProject() */
        if ($this->coursePropertyIsUseProject()) {
            $roles = ProjectRole::getRolesByProjectIdAndUserId(Course::getCourse($this->course_id)->project_id, $user->id);

            $task = new Task();
            $taskTable = $task->getTable();

            $role = new ProjectRole();
            $roleTable = $role->getTable();

            $statusChange = new TaskStatusChange();
            $statusChangeTable = $statusChange->getTable();

            $canChangeStatuse = Task::getBy($this->parent_id ? 'act' : 'topic', $this->id, null, $editable)
                ->join($statusChangeTable,
                    "$statusChangeTable.status_id", '=', \DB::raw("`$taskTable`.`status_id` and `$statusChangeTable`.`type_id` = `$taskTable`.`type_id`"))
                ->where("$taskTable.responsible_id", $user->id)
                ->whereIn("$statusChangeTable.role_id", $roles)
                ->count();

            if ($canChangeStatuse) {
                return false;
            }
            return $this->parent_id ? self::getCategoryCache($this->parent_id)->isReadonly(true, $editable) : Course::getCourse($this->course_id)->isReadonly(true, $editable);
        }

        /* @see \Igvs\Courses\Models\Course::iCanSeeExaminationSystem() */
        if ($this->coursePropertyICanSeeExaminationSystem()) {
            $members = Member::getMemberFew($this->course_id, $user->id);
            $roles = MemberRole::getMemberFewRoles($members);
            return !StatusChange::getStatusChangeCount($roles, Course::getCourse($this->course_id)->status_id);
        }
        if ($user->hasPermission('igvs.courses.pilot_user')) {
            return !$this->checkPilotAccess($this->course_id);
        }
        return !$childInterest && ($this->readonly || !$this->checkAccessOperation($this->course_id, 'AllowEditCategories'));
    }

    public function getRedLockVisabilityForCoursesAdmin()
    {
        if (!$this->isCourseAdmin($this->course_id))
            return false;

        if ($this->parent_id) {
            $parent = self::getCategoryCache($this->parent_id);
            return $parent->readonly;
        }

        return $this->readonly;
    }

    private static function fieldUse($fields, $names, $key, $value = true)
    {
        foreach ((array)$names as $name) {
            if (isset($fields->$name)) {
                $fields->$name->$key = $value;
            }
        }
    }

    public function filterFields($fields, $condition = null)
    {
        $is_bag_method = 0;
        $hide_fields = [];

        if ($user = \BackendAuth::getUser()) {
            $is_bag_method = $user->hasPermission('igvs.courses.bag_method');
        }

        if ($this->parent_id
        || ($this->readonly && !$this->checkAccessOperation($this->course_id, 'UncheckReadonlyCategory'))
        || (!$this->readonly && !$this->checkAccessOperation($this->course_id, 'CheckReadonlyCategory'))
        ) {
            $hide_fields[] = 'readonly';
        }
        if ($this->parent_id ) {
            $hide_fields[] = 'code';
            $hide_fields[] = 'is_separate';
        }
        if ($is_bag_method) {
            if (!in_array('is_separate', $hide_fields)) {
                $hide_fields[] = 'is_separate';
            }
            if (!in_array('readonly', $hide_fields)) {
                $hide_fields[] = 'readonly';
            }
            if (!in_array('code', $hide_fields)) {
                $hide_fields[] = 'code';
            }
        }
        if ($this->isReadonly() && in_array($condition, ['update', 'create'])) {
            $disabled = array_keys((array) $fields);
            unset($disabled[array_search('readonly', $disabled)]);
            $this->fieldUse($fields, $disabled, 'disabled');
        }
        $this->fieldUse($fields, $hide_fields, 'hidden');
    }
}
