<?php namespace Igvs\Courses\Models;

use Model;

class Status extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \Academy\System\Traits\TransAttributes;

    protected $rules =[
        'name' => 'required|between:3,255|unique:igvs_courses_role_statuses',
        'color' => 'required',
        'icon' => 'required',
        'command' => 'required',
    ];

    public $table = 'igvs_courses_role_statuses';
    public $timestamps = false;

    protected $guarded = ['*'];
    protected $fillable = [];

    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    static private $statuses = [];

    static public function getStatus($id)
    {
        self::fillStatusesCache();

        if (isset(self::$statuses[$id]))
            return self::$statuses[$id];

        return null;
    }

    static public function getStatuses($statuses_id = [])
    {
        self::fillStatusesCache();

        if (count($statuses_id)) {
            return array_intersect_key(self::$statuses, array_flip($statuses_id));
        }

        return self::$statuses;
    }

    static public function getStatusesLists($statuses_id = [])
    {
        self::fillStatusesCache();

        $filtred_statuses = $return = [];
        if (count($statuses_id)) {
            $filtred_statuses = array_intersect_key(self::$statuses, array_flip($statuses_id));
        }

        foreach ($filtred_statuses as $status) {
            $return[$status->id] = $status->name;
        }

        return $return;
    }

    static private function fillStatusesCache()
    {
        if (!count(self::$statuses)) {
            $statuses = self::all();

            foreach ($statuses as $status) {
                self::$statuses[$status->id] = $status;
            }
        }
    }
}
