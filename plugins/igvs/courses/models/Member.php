<?php namespace Igvs\Courses\Models;

use Academy\Tasks\Models\User as TUser;
use Igvs\Courses\Classes\MemberEmailNotice;
use Igvs\Courses\Models\User as MUser;
use Igvs\Courses\Scopes\RegisterMembers;

class Member extends \Model
{
    public $table = 'igvs_courses_members';

    protected $guarded = ['*'];
    protected $fillable = ['user_id', 'course_id'];

    static $notice_sended = false;
    public $server_name = null;

    public $belongsTo = [
        'user' => ['Igvs\Courses\Models\User'],
        'course' => ['Igvs\Courses\Models\Course']
    ];

    public $belongsToMany = [
        'roles' => [
            'Igvs\Courses\Models\Role',
            'table' => 'igvs_courses_members_roles',
            'key' => 'member_id',
            'otherKey' => 'role_id'
        ]
    ];

    public $course_project_roles = [];

    static private $members = [];
    static private $members_few = [];

    static private $members_has_roles = [];
    static private $members_has_roles_operation = [];

    static public function getMember($course_id, $user_id)
    {
        if (!isset(self::$members[$course_id])) self::$members[$course_id] = [];

        if (!isset(self::$members[$course_id][$user_id])) {
            $member = self::where('user_id', $user_id)
                ->where('course_id', $course_id)
                ->first();

            $member_id = null;
            if ($member) $member_id = $member['id'];

            self::$members[$course_id][$user_id] = $member_id;
        }

        return self::$members[$course_id][$user_id];
    }

    static public function getMemberFew($course_id, $user_id)
    {
        if (!isset(self::$members_few[$course_id])) self::$members_few[$course_id] = [];

        if (!isset(self::$members_few[$course_id][$user_id])) {
            $member_ids = self::where('user_id', $user_id)
                ->where('course_id', $course_id)
                ->lists('id');

            self::$members_few[$course_id][$user_id] = $member_ids;
        }

        return self::$members_few[$course_id][$user_id];
    }

    static public function getMembersIdHasRolesByRight($user_id, $course_id, $right)
    {
        $cache_key = $user_id . '-' . $course_id . '-' . $right;

        if (!isset(self::$members_has_roles[$cache_key])) {
            self::$members_has_roles[$cache_key] = self::where('user_id', $user_id)
                ->where('course_id', $course_id)
                ->whereHas('roles', function ($q) use($right) {
                    $q->where($right, true);
                })
                ->lists('id');
        }

        return self::$members_has_roles[$cache_key];
    }

    static public function getMembersIdHasRolesByOperations($user_id, $course_id, $operations)
    {
        $cache_key = $user_id . '-' . $course_id . '-' . implode('_', $operations);

        if (!isset(self::$members_has_roles_operation[$cache_key])) {
            self::$members_has_roles_operation[$cache_key] = self::where('user_id', $user_id)
                ->where('course_id', $course_id)
                ->where(function($query) use ($operations) {
                    $query->whereHas('roles', function($q) {
                        $q->where('is_superuser', true);
                    })
                    ->orWhereHas('roles.operations', function($q) use ($operations) {
                        $q->whereIn('code', $operations);
                    });
                })
                ->lists('id');
        }

        return self::$members_has_roles_operation[$cache_key];
    }

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RegisterMembers);
    }

    private function saveCourseProjectRoles()
    {
        if (!$this->course
            || !($project = $this->course->project)
            || $this->course->short_version
        ) {
            return false;
        }
        if (!$project_user = $project->users()->where('user_id', $this->user_id)->select('id')->first()) {
            $project_user = new TUser();
            $project_user->user_id = $this->user_id;
            $project_user->project_id = $project->id;
            $project_user->save();
        }
        else {
            $project_user->roles()->detach();
        }
        if (is_array($this->course_project_roles) && count($this->course_project_roles)) {
            $project_user->roles()->attach($this->course_project_roles);
        }
        return true;
    }

    public function getUserIdAttributes()
    {
        if (isset($this->attributes['user_id'])) {
            return $this->attributes['user_id'];
        }
        if (!count($users = $this->getUserOptions())) {
            return;
        }
        reset($users);
        return key($users);
    }

    private function loadCourseProjectRoles()
    {
        $user_id = $this->getUserIdAttributes();
        if (!$this->course
         || !($project = $this->course->project)
         || !($project_user = $project->users()->where('user_id', $user_id)->select('id')->first())
         || $this->course->short_version
        ) {
            $this->course_project_roles = [];
            return false;
        }
        $this->course_project_roles = $project_user->roles()->lists('id');
        return true;
    }

    public function beforeSave()
    {
        unset($this->attributes['poo_filter']);
    }

    public function afterFetch()
    {
        $this->loadCourseProjectRoles();
    }

    public function afterSave()
    {
        $this->saveCourseProjectRoles();

        // send notice to this member about change his role
        if (!self::$notice_sended) {
            self::$notice_sended = true;
            MemberEmailNotice::sendEmailNoticeChangeRole($this);
        }
    }

    public function getCourseProjectRolesOptions()
    {
        $this->loadCourseProjectRoles();
        //$this->course_project_roles = [];
        //$this->course_project_roles[] = 41;
        if (!$project = $this->course->project) {
            return [];
        }
        return $project->roles()->lists('name', 'id');
    }

    public function getUserOptions()
    {
        $return = [];

        $user = \BackendAuth::getUser();
        $is_pilot_user = $user ? $user->hasPermission('igvs.courses.pilot_user') : false;

        $users = $this->course->members()->groupBy('user_id')->lists('user_id');

        $select = "concat(last_name, ' ', first_name, ' (', email, ')') as `name`";

        $return_users = MUser::select('id', 'last_name',  'first_name')
            ->selectRaw($select)
            ->whereNotIn('id', $users)
            ->orWhere('id', $this->user_id)
            ->get();

        foreach($return_users as $v)
        {
            $return[$v->id] = $v->name;
        }

        if ($is_pilot_user) {
            foreach ($return_users as $return_user) {
                $user_pilot = \Igvs\Courses\Models\UserPilot::where('user_id', $return_user->id)->first();

                if (count($user_pilot) && strlen($user_pilot->email)) {
                    $return[$return_user->id] = "{$return_user->last_name} {$return_user->first_name} ({$user_pilot->email})" ;
                }
            }
        }

        return $return;
    }

    public function filterFields($fields, $condition = null)
    {
        if ($this->course->short_version
        || $this->course->project_id == null
        || !count($this->getCourseProjectRolesOptions())
        ) {
            $fields->course_project_roles->hidden = true;
        }
        else {
            $fields->roles->span = 'left';
        }


        $user = \BackendAuth::getUser();
        $is_expertise = \Session::get('igvs.courses.is_expertise_system', 0);
        $is_bag_method = $user->hasPermission('igvs.courses.bag_method');
        if (!($is_expertise || $is_bag_method))
            $fields->poo_filter->hidden = true;

    }

    public function getMemberRealEmailAttribute() {
        if ($pilot = UserPilot::where('user_id', $this->user_id)->first()) {
            return $pilot->email;
        }
        if ($user = \BackendAuth::findUserById($this->user_id)) {
            return $user->email;
        }
    }

    public static function getByCourse($course_id, $user_id = null) {
        if (is_null($user_id) && $user = \BackendAuth::getUser()) {
            $user_id = $user->id;
        }
        if (is_null($user_id)) {
            return null;
        }
        return self::where('user_id', $user_id)->where('course_id', $course_id)->first();
    }
}
