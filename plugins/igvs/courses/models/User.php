<?php namespace Igvs\Courses\Models;

use Backend\Models\User as BaseUser;
use Igvs\Courses\Scopes\RegisterMembers;

class User extends BaseUser {

    protected $appends = ['full_name_login'];

    public $belongsTo = [
        'pilot' => [
            'Igvs\Courses\Models\UserPilot',
            'key' => 'id',
            'otherKey' => 'user_id'
        ]
    ];
    public $belongsToMany = [
        'groups' => ['Backend\Models\UserGroup', 'table' => 'backend_users_groups'],
        'info' => [
            'Igvs\Courses\Models\Info',
            'table' => 'igvs_courses_users_info',
            'key' => 'user_id',
            'otherKey' => 'info_id'
        ]
    ];

    public static function boot()
    {
        parent::boot();
        // static::addGlobalScope(new PollUserScope);
        static::addGlobalScope(new RegisterMembers);
    }

    public function __get($field)
    {
        $user = \BackendAuth::getUser();
        $is_pilot_user = $user ? $user->hasPermission('igvs.courses.pilot_user') : false;

        if($field == 'concat(first_name,\' \',last_name,\' (\',login,\')\')') {
            if ($is_pilot_user) {
                $user_pilot = $this->pilot()->get();

                if (count($user_pilot) && strlen($user_pilot[0]->email)) {
                    return $user_pilot[0]->email;
                } else {
                    return $user->email;
                }
            }

            return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'] . ' (' . $this->attributes['login'] . ')';
        }
        return parent::__get($field);
    }

    public function getFullNameLoginAttribute()
    {
        $user = \BackendAuth::getUser();
        $is_pilot_user = $user ? $user->hasPermission('igvs.courses.pilot_user') : false;

        if ($is_pilot_user) {
            $user_pilot = \Igvs\Courses\Models\UserPilot::where('user_id', $this->id)->first();

            if ($user_pilot && strlen($user_pilot->email)) {
                $login = $user_pilot->email;
            } else {
                $login = $this->email;
            }
        }
        else {
            $login = $this->attributes['login'];
        }
        return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'] . ' (' . $login . ')';
    }

    public function getOriginalEmail()
    {
        $is_pilot_user = $this->hasPermission('igvs.courses.pilot_user');

        if ($is_pilot_user) {
            $user_pilot = \Igvs\Courses\Models\UserPilot::where('user_id', $this->id)->first();

            if ($user_pilot && strlen($user_pilot->email)) {
                $return = $user_pilot->email;
            } else {
                $return = $this->email;
            }
        } else {
            $return = $this->email;
        }

        return $return;
    }

    public static function getUser($id = null) {
        if (!is_null($id)) {
            return self::find($id);
        }
        if ($user = \BackendAuth::getUser()) {
            return self::find($user->id);
        }
    }

    public function getUserListFullNameLogin()
    {
        $return = [];

        foreach ($this->all() as $user) {
            $return[$user->id] = $user->full_name_login;
        }

        return $return;
    }
}
