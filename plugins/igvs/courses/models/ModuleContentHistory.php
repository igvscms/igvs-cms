<?php namespace Igvs\Courses\Models;

use Model;
use Config;
use Input;

/**
 * ModuleContentHistory Model
 */
class ModuleContentHistory extends Model
{

    use \October\Rain\Database\Traits\SoftDeleting;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_modules_contents_history';

    public $timestamp = false;

    protected $dates = [
        //'created_at',
        //'updated_at',
        'deleted_at'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'module_content_id',
        'module_content_data',
        'data',
        'module_content_code',
        'diff',
        'diff_group',
        'name',
        'behavior',
        'module_id',
        'module_version',
        'user_id',
        'ip',
        'created_at',
        'updated_at',
    ];

    public function beforeCreate()
    {
        if (is_null($this->module_content_data)) {
            $this->module_content_data = '';
        }

        if (is_null($this->type)) {
            $this->type = 0;
        }
    }


    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];

    public $belongsTo = [
        'module' => ['Igvs\Courses\Models\Module', 'foreignKey' => 'module_id']
    ];

    public $belongsToMany = [
    ];

    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function versionkeeper()
    {
        $return = '<div>';

    }

    public function afterFetch()
    {


        if(isset($this->diff) && $this->diff == 1)
        {



        }
        //$this->module_content_id;
    }

    static function ApplyDiff($array,$diff)
    {
        //print_r($array);
        //print_r($diff);
        //exit;
        $return = [];
        $move = [];

        if(!is_array($array))
            return [];
        $keys = array_keys($array);
        $keys_pos = array_flip($keys);

        if(is_array($diff))
            foreach($diff as $key => $vals)
            {
                if(isset($vals['p']))
                    $move[$vals['p']] = $key;
                elseif(!isset($vals['s']) || $vals['s'] != 'd')
                {
                    if(!isset($keys_pos[$key]))
                    {
                        $keys_pos[$key] = count($keys_pos);
                        $keys[] = $key;
                        $array[$key] = '';
                    }
                    $move[$keys_pos[$key]] = $key;

                }
            }
        $pos = 0;
        for($i=0;$i<count($diff)+count($array);)
        {
            if(isset($move[$i]))
            {
                //print_r($diff[$move[$i]]);
                if(isset($diff[$move[$i]]['s']))
                    switch($diff[$move[$i]]['s'])
                    {
                        case 'm':
                            if(isset($array[$move[$i]]))
                                $return[$move[$i]] = $array[$move[$i]];
                            break;
                        case 'n':
                            $return[$move[$i]] = $diff[$move[$i]]['d'];
                            break;
                        case 'e':
                            //print_r($diff[$move[$i]]['d']);
                            if ((!isset($array[$move[$i]]) || !is_array($array[$move[$i]]))
                                && !isset($diff[$move[$i]]['d']['s'])
                            ) {
                                if(isset($diff[$move[$i]]['d']['l']))
                                    $array[$move[$i]] = 0;
                                else
                                    $array[$move[$i]] = $diff[$move[$i]]['d']['r'];
                            }
                            if(isset($diff[$move[$i]]['d']['s']) && isset($diff[$move[$i]]['d']['l']))
                            {
                                if(is_bool($diff[$move[$i]]['d']['r']))
                                    $return[$move[$i]] = $diff[$move[$i]]['d']['r'];
                                else
                                    $return[$move[$i]] = substr_replace($array[$move[$i]], $diff[$move[$i]]['d']['r'], $diff[$move[$i]]['d']['s'], $diff[$move[$i]]['d']['l']);
                            }
                            elseif(!isset($diff[$move[$i]]['d']['s']))
                                $return[$move[$i]] = self::ApplyDiff($array[$move[$i]],$diff[$move[$i]]['d']['r']);
                            elseif(!isset($diff[$move[$i]]['d']['l']))
                                if(isset($diff[$move[$i]]['d']['r']))
                                    $return[$move[$i]] = $diff[$move[$i]]['d']['r'];
                            break;

                    }
                $i++;
            }
            elseif($pos>=count($array))
                $i++;
            else
                for(;$pos<count($array);$pos++)
                {
                    if(isset($keys[$pos]) && !isset($diff[$keys[$pos]]))
                        $return[$keys[$pos]] = $array[$keys[$pos]];
                    $i++;
                    if(isset($move[$i]))
                        break;
                }
        }
        //print_r($array);
        //print_r($return);
        return $return;
    }

    public function getPrevVersion($id = 0)
    {
        if ($id == 0) {
            $last = $this;
        }
        else {
            $last = $this::where('id',$id)->first();
        }
        if ($last->diff != 0) {
            $Historys = $this::where('module_content_id', $last->module_content_id)
                ->where('diff_group', $last->diff_group)
                ->where('created_at','>', $last->created_at)
                ->select('module_content_data','diff','data')
                ->orderBy('created_at','desc')
                ->get();

            if (count($Historys) == 0 || $Historys[0]->diff != 0) {
                $return = ModuleContent::withTrashed()->select('data')->find($last->module_content_id);
                $return = json_decode($return->data,true);
            }

            foreach ($Historys as $History) {
                if ($History['data']!='') {
                    $data = bzdecompress($History['data']);
                }
                else {
                    $data = $History['module_content_data'];
                }
                $data = json_decode($data,true);
                if (is_array($data)) {
                    if (!isset($return)) {
                        $return = $data;
                    }
                    else {
                        $return = $this->ApplyDiff($return, $data);
                    }
                }
            }
            if ($last['data']!='') {
                $data = bzdecompress($last['data']);
            }
            else {
                $data = $last['module_content_data'];
            }
            return $this->ApplyDiff($return, json_decode($data,true));
        }
        else {
            if ($last['data']!='') {
                $data = bzdecompress($last['data']);
            }
            else {
                $data = $last['module_content_data'];
            }
            return json_decode($data, true);
        }
    }
}
