<?php namespace Igvs\Courses\Models;

use Model;
use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Build;
use Igvs\Courses\Models\Category;

/**
 * TempLink Model
 */
class TempLink extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_temp_links';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'user_id',
        'course_id',
        'build_id',
        'topic_id',
        'date_expired',
        'hash',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'course' => ['Igvs\Courses\Models\Course', 'foreignetKey' => 'course_id'],
        'topic' => ['Igvs\Courses\Models\Category', 'foreignetKey' => 'topic_id'],
        'build' => ['Igvs\Courses\Models\Build', 'foreignetKey' => 'build_id'],
        'user' => ['Igvs\Courses\Models\User', 'foreignKey' => 'responsible_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function beforeCreate()
    {
        $user = null;

        if (!\App::runningInConsole() && !($user = \BackendAuth::getUser())) {
            return false;
        }

        $user_id = rand(1, 9999);

        if ($user)
            $user_id = $user->id;

        $this->hash = uniqid(rand(10,99) . $user_id . '-');
    }

    public function getTopicOptions()
    {
        $course = $this->getCourseByCourseId();
        $topics = Category::where('course_id', $course->id)
            ->whereNull('parent_id')
            ->get();

        $return_topics = [ 0 => 'Не выбрано'];
        foreach ($topics as $v) {
            $return_topics[$v->id] = $v->name;
        }

        return $return_topics;
    }

    public function getBuildOptions()
    {
        $builds = Build::get();

        $return_builds = [ 0 => 'Не выбрано'];
        foreach ($builds as $v) {
            $return_builds[$v->id] = $v->name;
        }

        return $return_builds;
    }

    public function getLinkAttribute()
    {
        $protocol = isset($_SERVER['HTTPS']) && strlen($_SERVER['HTTPS']) ? 'https' : 'http';
        $domain = $_SERVER['SERVER_NAME'];

        return $protocol . '://' . $domain . '/preview/tlink/' . $this->hash;
    }

    private function getCourseByCourseId() {
        if ($this->course_id) {
            $course = Course::find($this->course_id);
        } else {
            $course = Course::first();
        }

        return $course;
    }
}
