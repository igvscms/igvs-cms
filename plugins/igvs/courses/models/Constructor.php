<?php namespace Igvs\Courses\Models;

use Model;

/**
 * Constroctor Model
 */
class Constructor extends Model
{
    use \October\Rain\Database\Traits\SoftDeleting;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'igvs_courses_constructors';

    /**
     * @var array Dates fields
     */
    protected $dates = ['deleted_at', 'updated_dt', 'created_at'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'versions' => [
            'Igvs\Courses\Models\ConstructorVersion',
            'primaryKey' => 'constructor_id',
            'order' => 'sort DESC'
        ]
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'specification' => ['System\Models\File', 'public' => true],
    ];
    public $attachMany = [];

    public function getActualVersion(){
        $versions = ConstructorVersion::where('constructor_id', $this->id)
            ->get()
            ->lists('version');

        $highestVersionValue = 0;
        $highestVersion = '';
        foreach ($versions as $ver) {
            $versionArr = preg_split("/\./", $ver);
            $version = 0;
            for ($i = count($versionArr)-1; $i >= 0; $i--) {
                $version+= $versionArr[count($versionArr)-1-$i] * pow(10,$i);
            }

            if ($version > $highestVersionValue){
                $highestVersionValue = $version;
                $highestVersion = $ver;
            }
        }

        return $highestVersion;
    }

    public function getActualStatus($version){
        $status = ConstructorVersion::where('constructor_id', $this->id)
            ->where('version', $version)
            ->get()
            ->lists('status');

        return $status[0];
    }

}