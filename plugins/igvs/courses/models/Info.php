<?php namespace Igvs\Courses\Models;

use Backend\Models\User;

class Info extends \Model {
    public $table = 'igvs_courses_user_info';
    protected $guarded = ['*'];
    protected $fillable = ['code', 'text'];
    public $timestamps = false;
    public $belongsToMany = [
        'user' => [
            'Backend\Models\User',
            'table' => 'igvs_courses_users_info',
            'key' => 'info_id',
            'otherKey' => 'user_id'
        ]
    ];

    private static function getUserInfo($code, &$user = null, &$info = null) {
        if ($user) {
            $user = User::find($user);
        }
        else {
            $user = \backendAuth::getUser();
        }
        if (!$user || !$info = self::where('code', $code)->first()) {
            return false;
        }
        return $info->user()->where('id', $user->id)->limit(1)->count();
    }

    public static function exists($code, $user_id = null) {
        return self::getUserInfo($code, $user_id);
    }

    public static function check($code, $user_id = null) {
        if (!self::getUserInfo($code, $user_id, $info) && $info) {
            $info->user()->add($user_id);
        }
        return $info;
    }
}