<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Igvs\Courses\Classes\Socket\ChatSocket;

class ChatServer extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'igvs.chatserver';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $this->info("Start server");

        $server = IoServer::factory(
            new HttpServer(
                new WsServer (
                    new ChatSocket()
                )
            ),
            8080
        );

        $server->run();

        // $this->output->writeln('Hello world!');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
