<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\Constructor;
use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Category;
use App;
use GD;

class CreateContentListPdf extends Command
{
    use \Igvs\Courses\Traits\FileHelper;

    protected $name = 'igvs:createcontentlistpdf';

    public function fire()
    {
        $this->CreatePdfFromCourses();
    }

    public function CreatePdfFromCourses(){
        ini_set('gd.jpeg_ignore_warning', 1);
        $path = Config::get('igvs.courses::modulesContent.path');
        // $dir_courses = $this->scandir($path);

        $arguments = preg_split("/,/",$this->argument('courses'));

        if (count($arguments) == 1 && $arguments[0] ==''){
            $this->info("Selected courses: all. You can choose courses by write arguments separates by ','");
            $dir_courses = Course::all()->lists('id');
        }
        else{
            $this->info("Selected courses: ".join(",",$arguments));
            $dir_courses = $arguments;
        }

        if (!$this->confirm('Do you wish to continue? [yes|no]')) return;

        foreach ($dir_courses as $dir_course) {
            $courseName = Course::find($dir_course);
            if (!$courseName){
                $this->info("Course ".$dir_course." doesn't exist");
                continue;
            }

            $actListByCat = ModuleContent::where("course_id",$dir_course)->get()->lists('category_id');
            $actListByCat = array_unique($actListByCat);
            $topicList = [];

            foreach ($actListByCat as $actIdByCat) {
                $actObjByCat = Category::find($actIdByCat);
                if (!$actObjByCat) continue;
                array_push($topicList, Category::find($actObjByCat->parent_id));
            }

            $topicList = array_unique($topicList);

            foreach ($topicList as $topic) {
                if (!$topic)
                    continue;
                $html = "<!DOCTYPE html><html>";
                $html .= '<head>
                <meta charset="utf-8">
                <title>Image Catalogue</title>
                <link rel="stylesheet" type="text/css" href="/themes/igvs/assets/css/imgs_content.css">
                <style>
                body { font-family: DejaVu Sans, sans-serif; }
                </style>
                </head>';
                $html .= '<body>';


                $actListByTopic = Category::where("parent_id",$topic->id)->get()->lists('id');
                $actListByTopic = array_unique($actListByTopic);
                $dir_modules = [];
                foreach ($actListByTopic as $actByTopic) {
                    $modules = ModuleContent::where("category_id",$actByTopic)->get()->lists('id');
                    foreach ($modules as $module) {
                        array_push($dir_modules, $module);
                    }
                }
                $dir_modules = array_unique($dir_modules);

                // $dir_modules = $this->scandir("{$path}/{$dir_course}");

                foreach ($dir_modules as $dir_module) {
                    file_put_contents(Config::get('igvs.courses::tmpModulesContent.path')."/errorImageCreate.log", $dir_module.' memory use: '.(memory_get_usage()/1024/1024).' \n', FILE_APPEND);
                    $this->info('memory usage: '.memory_get_usage()/1024/1024);
                    $this->count_div = 0;
                    if (!file_exists("{$path}/{$dir_course}/{$dir_module}")) {
                        $contents = [];
                    }
                    else{
                        $contents = $this->scandir("{$path}/{$dir_course}/{$dir_module}");
                    }

                    //find module
                    $module = ModuleContent::find($dir_module);
                    if (!$module)
                        continue;
                    //ишещь акт
                    $act = Category::find($module->category_id);
                    if (!$act)
                        continue;
                    // //ишещь топик
                    // $topic = Category::find($act->parent_id);
                    // if (!$topic)
                    //     continue;
    /* old logo path
                    preg_replace("/\/plugins\/igvs\/courses\/config\/\.\.\/\.\.\/\.\.\/\.\./","",Config::get('igvs.courses::tmpModulesContent.logo'))."\"/>
    */ 
                    $html .= "<div class=\"container\">";

                    $html .= '<header class="main-responsive-header">
                                <div class="header-wrapper">
                                    <div class="title">
                                        <div class="main-logo"></div>
                                        <h1 class="title-head">GVS Image Collection</h1>
                                    </div>
                                    <div class="information">
                                        <table class="information-list">
                                            <tr>
                                                <td>Course:</td>
                                                <td>'.$courseName->title.'</td>
                                            </tr>
                                            <tr>
                                                <td>Topic:</td>
                                                <td>'.$topic->name.'</td>
                                            </tr>
                                            <tr>
                                                <td>i-Act:</td>
                                                <td>'.$act->name.'</td>
                                            </tr>
                                            <tr>
                                                <td>Module:</td>
                                                <td>'.$module->code.' : '.$module->name.'</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </header>';


                    $html .= "<div class=\"catalogue\">";
                    if (count($contents) == 0){
                        $html .= "<div style=\"width:720px;\">NO IMAGES</div>";
                    }
                    else foreach ($contents as $content) {
                        $html .= $this->ScanForImgs("{$path}/{$dir_course}/{$dir_module}/{$content}", $topic->id."/".$dir_module);
                    }
                    $html .= "</div>";


                    $html .= '<footer class="footer"><p>ATTENTION: This material is copyrighted</p></footer>';
                    // $html .= '<div style="page-break-after: always;"></div>';

                    $html .= "</div>";
                }


                $html .= "<script type=\"text/javascript\" src=\"/themes/igvs/assets/javascript/imgs_content.js\"></script>";
                $html .= "</body>";
                $html .= "</html>";
                //$this->CreatePdf($html,$dir_course." - ".$courseName->title);
                //Save HTML
                if (!file_exists(Config::get('igvs.courses::tmpModulesContent.path')."/".$topic->id)) {
                    mkdir(Config::get('igvs.courses::tmpModulesContent.path')."/".$topic->id, 0777, true);
                }
                file_put_contents(Config::get('igvs.courses::tmpModulesContent.path')."/".$topic->id."/".$topic->id."_topic.html", $html);
                $this->info('Topic '.$topic->id.' created');
            }

            $this->info('Course '.$dir_course.' done');
        }

        $this->info('COMPLETE!');

        //$this->deleteDir(Config::get('igvs.courses::tmpModulesContent.path'));
    }

    private function ScanForImgs($dir, $saveDir){

        $storage_path = storage_path('app/igvs/module-content/');

        $verstka = "";

        if (is_dir($dir)){
            $files = $this->scandir($dir);
            foreach ($files as $file) {
                $verstka .= $this->ScanForImgs("{$dir}/{$file}", $saveDir);
            }
        }
        else{
            try{
                if (is_array(getimagesize($dir))){
                    $pathTemp = Config::get('igvs.courses::tmpModulesContent.path');
                    //$verstka .= "<div>".CompressImg($dir, $pathTemp, 80)."</div>";
                    preg_match("/\/[^\/]+$/", $dir, $fileName);

                    $dir2 = str_replace($storage_path, '', realpath($dir));

                    $verstka .= "<div class=\"catalogue-block\">
                    <div class=\"image-links\" >
                        <a href=\"javascript:;\" class=\"link disabled\" data-btn-save=\"".$dir2."\">Add to Project</a>
                    </div>
                    <div class=\"image-thumb\">
                    <img src=\"".preg_replace("/\/plugins\/igvs\/courses\/config\/\.\.\/\.\.\/\.\.\/\.\./","",$this->CompressImg($dir, $pathTemp, 100, $saveDir))."\"/>
                    </div>
                    <div class=\"image-info\">
                    <p>".substr($fileName[0],1)."</p>
                    </div>";
                    $verstka .= "</div>";

                    $this->count_div+=1;
                    if ($this->count_div%3 == 0){
                        // $verstka .= '<div style="clear: both; height:15px;"></div>';
                        if ($this->count_div%9 == 0){
                            $verstka .= '</div>';
                            $verstka .= '<footer class="footer"><p>ATTENTION: This material is copyrighted</p></footer>';
                            // $verstka .= '<div style="page-break-after: always;"></div>';
                            $verstka .= '</div>';
                            $verstka .= '<div class="container">';
                            $verstka .= '<div class="catalogue">';
                        }
                    }
                    // $verstka .= "<div style=\"float:left;width:160px;\"><img width=\"120px\" src=\"".preg_replace("/\/plugins\/igvs\/courses\/config\/\.\.\/\.\.\/\.\.\/\.\./","",$dir)."\"/></div>";
                }
            }
            catch(\Exception $e){
                file_put_contents(Config::get('igvs.courses::tmpModulesContent.path')."/errors.log", $dir. " * " .$e->getLine()." - ".$e->getMessage(), FILE_APPEND);
                $this->error($dir. " * " .$e->getLine()." - ".$e->getMessage());
            }
        }

        return $verstka;
    }

    private function CreatePdf($string,$name){
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($string);
        if (!file_exists(Config::get('igvs.courses::tmpModulesContent.pdf'))) {
            mkdir(Config::get('igvs.courses::tmpModulesContent.pdf'), 0777, true);
        }
        $pdf->save(Config::get('igvs.courses::tmpModulesContent.pdf')."/".$name.".pdf");
        file_put_contents(Config::get('igvs.courses::tmpModulesContent.pdf')."/".$name.".html", $string);
    }

    private function CompressImg($source_url, $destination_url, $quality, $topicModulePath){
        // file_put_contents(Config::get('igvs.courses::tmpModulesContent.path')."/errorImageCreate.log", $source_url.' memory use: '.(memory_get_usage()/1024/1024).' \n', FILE_APPEND);
        
        $source_url = preg_replace("/\/plugins\/igvs\/courses\/config\/\.\.\/\.\.\/\.\.\/\.\./","",$source_url);
        $info = getimagesize($source_url);
     
        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
        elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
        else return 'svg not supported';
     
        //resize
        $aspect = 0;
        $newWidth = 0;
        $newHeight = 0;
        if ($info[0] > $info[1]){
            //ширина больше высоты
            $aspect = $info[0] / $info[1];
            $newWidth = 200;
            $newHeight = 200/$aspect;
        }
        else{
            $aspect = $info[1] / $info[0];
            $newHeight = 200;
            $newWidth = 200/$aspect;
        }

        $resized_img = imagecreatetruecolor($newWidth, $newHeight);
        $white = imagecolorallocate($resized_img, 255,255,255);
        imagefilledrectangle($resized_img, 0, 0, $newWidth, $newHeight, $white);
        imagecopyresampled($resized_img, $image, 0, 0, 0, 0, $newWidth, $newHeight, $info[0], $info[1]);

        //save image
        $tempFolder = preg_replace("/\/plugins\/igvs\/courses\/config\/\.\.\/\.\.\/\.\.\/\.\./","",$destination_url);
        $tempVar = explode("/", $source_url);
        $imgPath = $tempFolder.'/'.$topicModulePath.'/'.$tempVar[count($tempVar)-1];
        // $imgPath = $tempFolder.explode("/module-content", $source_url)[1];
        $imgFolder = preg_split("/\/[^\/]+$/", $imgPath);

        if (!file_exists($imgFolder[0])) {
            mkdir($imgFolder[0], 0777, true);
        }

        imagejpeg($resized_img, $imgPath, $quality);

        //return destination file
        return preg_split("/\/htmlContentList\/(.[^\/]*)\//", $imgPath)[1];
        // return preg_split("/\/module-content\/(.[^\/]*)\//", $imgPath)[1];
        //return $imgPath;
    }

    public static function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    protected function getArguments()
    {
        return [
            ['courses', InputArgument::OPTIONAL, 'Courses ID.'],
        ];
    }

}