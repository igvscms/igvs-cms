<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use FilesystemIterator;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Course;

class RenameRusNameFiles extends Command
{
    public $translit_table = [
        "й" => "y", "Й" => "Y",
        "ц" => "ts", "Ц" => "Ts",
        "у" => "u", "У" => "U",
        "к" => "k", "К" => "K",
        "е" => "e", "Е" => "E",
        "н" => "n", "Н" => "N",
//        "н" => "h", "Н" => "H",
        "г" => "g", "Г" => "G",
        "ш" => "sh", "Ш" => "Sh",
        "щ" => "sh", "Щ" => "Sh",
        "з" => "z", "З" => "Z",
        "х" => "h", "Х" => "H",
//        "х" => "x", "Х" => "X",
        "ъ" => "", "Ъ" => "",
        "ф" => "f", "Ф" => "F",
        "ы" => "e", "Ы" => "E",
        "в" => "v", "В" => "V",
        "а" => "a", "А" => "A",
        "п" => "p", "П" => "P",
//        "р" => "r", "Р" => "R",
        "р" => "p", "Р" => "P",
        "о" => "o", "О" => "O",
        "л" => "l", "Л" => "L",
        "д" => "d", "Д" => "D",
        "ж" => "j", "Ж" => "J",
        "э" => "e", "Э" => "E",
        "я" => "ya", "Я" => "Ya",
        "ч" => "ch", "Ч" => "Ch",
//        "с" => "s", "С" => "S",
        "с" => "c", "С" => "C",
        "м" => "m", "М" => "M",
        "и" => "i", "И" => "I",
        "т" => "t", "Т" => "T",
        "ь" => "", "Ь" => "",
        "б" => "b", "Б" => "B",
        "ю" => "yu", "Ю" => "Yu",
        " " => "_", ":" => "",
    ];

    public $counter = 0;

    public $iteration_stamp = '';

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs.renamerusnamefiles';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $this->iteration_stamp = time();

        $this->info("I" . $this->iteration_stamp);
        $this->info("Welcome to Ebook Rename Russian Files\r\n--------------------------");

        $course_id = $this->ask('Enter please Course ID: ');
        $this->log("\r\n--------------------");
        $this->log('Chosen course:' . $course_id);

        $template_markup = Module::where('path', 'html-markup')->first();

        if (!$template_markup) {
            $this->error('Template HTML-Markup not found!');
            $this->log('Template HTML-Markup not found!');
            exit();
        }

        $template_markup_id = $template_markup->id;

        $count_module_content = ModuleContent::where('module_id', $template_markup_id)
            ->where('course_id', $course_id)
            ->count();

        $count_all_module_content = ModuleContent::where('course_id', $course_id)
            ->count();

        if (!$count_module_content) {
            $this->error('Modules with Template HTML-Markup not found in course ' . $course_id);
            $this->log('Modules with Template HTML-Markup not found in course ' . $course_id);
            exit();
        }

        $this->info("Found modules: " . $count_module_content . " of {$count_all_module_content} all course modules");
        $this->log("Found modules: " . $count_module_content . " of {$count_all_module_content} all course modules");

        $counter = 0;

        ModuleContent::where('module_id', $template_markup_id)
            ->where('course_id', $course_id)
            ->orderBy('course_id', 'asc')
            ->chunk(10, function ($part) use ($counter, $count_module_content) {

            foreach ($part as $module) {
                $this->counter++;

                echo "{$this->counter}/{$count_module_content} C-{$module->course_id} M-{$module->id} ";
                $this->log("{$this->counter}/{$count_module_content} C-{$module->course_id} M-{$module->id} ");

                try {

                    $path = $module->getPathContents();
                    $renamed_files_list = $this->scanAndRename($path);

                    $this->log('path: ' . $path);

                    if (!count($renamed_files_list)) {
                        echo " no data to change. skip. ";
                        $this->log("no data to change. ");
                        $this->warn('skip');
                        continue;
                    }

                    $this->saveContent($module);

                    try {
                        $data = json_decode($module->data);

                        $content = $data->html;
                        $content = str_replace(array_keys($renamed_files_list), array_values($renamed_files_list), $content);

                        $data->html = $content;
                    } catch (\Exception $e) {
                        echo "error json_decode \r\n";
                        $this->log("error json_decode");

                        $content = $module->content;

                        $data = (object)[
                            'html' => $content,
                            'questions' => [["typeTest" => "html-markup"]]
                        ];
                    }

                    $module->data = json_encode($data);
                    $module->content = $content;

                    $this->saveContent($module, true);
                    $module->save();

                    echo "renamed files " . (count($renamed_files_list) ? count($renamed_files_list) : '-') . " ";
                    $this->log("renamed files " . (count($renamed_files_list) ? count($renamed_files_list) : '-'));

                    $this->info('success');
                    $this->log('success');

                } catch (\Exception $e) {

                    $this->error('error' . $e->getMessage());
                    $this->log('error');

                }
            }

        });
    }

    public function scanAndRename($path, $filename_prefix = '')
    {
        $this->log('scanAndRename: ' . $path);

        $renamed_files_list = [];

        $files_list = scandir($path);

        if (!is_array($files_list) || !count($files_list))
            return false;

        $this->log('file-list: ' . print_r($files_list, true));

        foreach ($files_list as $file) {
            if (in_array($file, ['.', '..']))
                continue;

            $filename_utf8 =  mb_convert_encoding($file, 'utf-8', 'cp1251');

            $matches = null;
            preg_match_all("/[а-яё\s]/iu", $filename_utf8, $matches);

            if (!$matches || !is_array($matches) || !count($matches) || (isset($matches[0]) && !count($matches[0]))) {
                if (is_dir("{$path}/{$file}")) {
                    $dir_renamed_files_list = $this->scanAndRename("{$path}/{$file}", $filename_prefix . $file . '/');

                    if ($dir_renamed_files_list && is_array($dir_renamed_files_list) && count($dir_renamed_files_list)) {
                        $renamed_files_list = array_merge($renamed_files_list, $dir_renamed_files_list);
                    }
                }

                continue;
            }

            $new_filename = str_replace(array_keys($this->translit_table), array_values($this->translit_table), $filename_utf8);

            if (is_file("{$path}/{$new_filename}")) {
                echo "Переименование не совершено, т.к. файл с таким имененм уже существует (\"{$path}/{$file}\" ({$filename_utf8}) в \"{$path}/{$new_filename}\")\n\r";
                continue;
            }

            $cmd = "mv \"{$path}/{$file}\" \"{$path}/{$new_filename}\"";
            $this->log($cmd);
            shell_exec($cmd);

            if (is_dir("{$path}/{$new_filename}")) {
                $dir_renamed_files_list = $this->scanAndRename("{$path}/{$new_filename}", $new_filename . '/');

                if ($dir_renamed_files_list && is_array($dir_renamed_files_list) && count($dir_renamed_files_list)) {
                    $renamed_files_list = array_merge($renamed_files_list, $dir_renamed_files_list);
                }
            }

            $renamed_files_list[$filename_prefix . $filename_utf8] = $filename_prefix . $new_filename;
        }

        return $renamed_files_list;
    }

    private function log($text)
    {
        $log_dir_path = __DIR__ . '/../../../../storage/app/igvs/logs/rename-rusname-files';

        if (!is_dir($log_dir_path))
            mkdir($log_dir_path);

        $log_name = $log_dir_path . "/{$this->iteration_stamp}.log";

        $text = '[' . $this->iteration_stamp . '][' . date('Y-m-d H:i:s') .  ']' . ' ' . $text . "\r\n";

        file_put_contents($log_name, $text, FILE_APPEND);
    }

    private function saveContent($module_content, $after = false)
    {
        $log_dir_path = __DIR__ . '/../../../../storage/app/igvs/logs/rename-rusname-files';

        if (!$module_content || !is_object($module_content)) {
            $this->log('save log failed!!');
            return;
        }

        if (!is_dir($log_dir_path . "/{$this->iteration_stamp}"))
            mkdir($log_dir_path . "/{$this->iteration_stamp}");

        $postfix = $after ? '-new' : '';
        file_put_contents($log_dir_path . "/{$this->iteration_stamp}/{$module_content->course_id}-{$module_content->id}{$postfix}.txt", print_r($module_content, true));

    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}