<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use League\Flysystem\Exception;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Category;

class Resort extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:resort';

    /**
     * @var string The console command description.
     */
    protected $description = 'Пересортировка';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $courses = Course::all();

        $start = microtime(true); 

        $step = 1;

        foreach ($courses as $course) {

            $categories = Category::select('id')
                ->where('course_id', $course->id)
                ->where('parent_id', null)
                ->orderBy('old_nest_left', 'asc')
                ->get();

            $this->info("Resorting: " . $course->code);

            $i = $step;
            foreach ($categories as $category) {

                Category::where('id', $category->id)
                    ->update(['sort' => $i]);

                $i += $step;

                $parents = Category::select('id')
                    ->where('parent_id', $category->id)
                    ->orderBy('old_nest_left', 'asc')
                    ->get();

                $j = $step;
                foreach ($parents as $parent) {

                    Category::where('id', $parent->id)
                        ->update(['sort' => $j]);

                    $j += $step;

                    $modules = ModuleContent::select('id')
                        ->where('category_id', $parent->id)
                        ->orderBy('old_sort', 'asc')
                        ->get();

                    $k = $step;
                    foreach ($modules as $module) {

                        ModuleContent::where('id', $module->id)
                            ->update(['sort' => $k]);

                        $k += $step;
                    }
                }
            }
        }

        $this->info('Время выполнения скрипта: '.(microtime(true) - $start).' сек.');

    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}