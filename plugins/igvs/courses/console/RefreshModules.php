<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Updates\SeedAllTables;

class RefreshModules extends Command
{
    use \Igvs\Courses\Traits\FileHelper;

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:refreshmodules';

    /**
     * @var string The console command description.
     */
    protected $description = 'Update module templates';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        try {
        Eloquent::unguard();
        $seeder = new SeedAllTables();
        $seeder-> UpdateConstructorsTable();
        $output = $seeder-> seedModulesTemplates();
        $this->output->writeln($output);
        } catch(\Exception $e) {
            dd([
                $e->getMessage(),
                $e->getFile(),
                $e->getLine()
            ]);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}