<?php namespace Igvs\Courses\Console;

use Igvs\Courses\Models\TempLink;
use Illuminate\Console\Command;
use League\Flysystem\Exception;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Models\Course;

class CreateTempLink extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:createtemplink';

    /**
     * @var string The console command description.
     */
    protected $description = 'Создание временных ссылок';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        try {
            $this->info("Welcome to create temp link master!\r\nOptions: --date, --courses (one or few separated by comma):\r\n");

            $arg_user_id = $this->option('user');
            $arg_date = $this->option('date');
            $arg_courses = preg_split("/,/",$this->option('courses'));

            $courses_ids = [];

            if (!strlen($arg_user_id)) {
                $this->error('You should write `user` param, ex. --user=12');
                exit();
            }

            if (count($arg_courses) == 1 && $arg_courses[0] =='') {
                $this->info("Selected courses: no one. You can choose courses by write arguments (courses) separates by ',', ex/--courses=23,87,35");
                exit();
            } else {
                $this->info("Selected courses: ".join(",",$arg_courses));

                foreach ($arg_courses as $course) {
                    $course = explode(':', $course);

                    $courses_ids[$course[0]] = isset($course[1]) ? $course[1] : null;
                }

                $courses = Course::whereIn('id', array_keys($courses_ids))->orderBy('id', 'desc')->get();
            }

            if (!$this->confirm('Do you want to continue? [yes|no]')) return;

            $errors = [];

            $count_courses = count($courses);

            echo "\n\n";
            echo date('d-m-Y H:i:s') . " Count of all selected courses: " . $count_courses;
            echo "\n\n";

            $date = strlen($arg_date) ? $arg_date : date('Y-m-d', strtotime('+1 day'));
            $time = '23:59:59';

            $date_time = $date . ' ' . $time;

            foreach ($courses as $key => $course) {
                $course_id = $course->id;

                try {

                    $temp_link = TempLink::create([
                        'user_id'       => $arg_user_id,
                        'course_id'     => $course_id,
                        'build_id'      => 0,
                        'topic_id'      => 0,
                        'date_expired'  => $date_time,
                    ]);

                    $this->info("success expired: {$temp_link->date_expired}, id: {$course_id}, link: /preview/tlink/{$temp_link->hash}");

                } catch (Exception $e) {

                    $errors[] = $course_id;
                    $this->error('fail: ' . $e->getMessage());

                }
            }

            echo date('d-m-Y H:i:s') . " Finish.  Errors: " . (count($errors) ? implode(', ', $errors) : 'no') . " \n\n";

        } catch(\Exception $e) {
            var_dump([
                $e->getMessage(),
                $e->getFile(),
                $e->getLine()
            ]);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        /*
         ['courses', InputArgument::OPTIONAL, 'Courses ID. You can choose courses by write arguments separates by \',\''],
        ['modules', InputArgument::OPTIONAL, 'Modules ID. You can choose template modules by write arguments separates by \',\''],
         */

        return [
            ['user', null, InputOption::VALUE_OPTIONAL, 'User ID', ''],
            ['courses', null, InputOption::VALUE_OPTIONAL, 'Courses ID. You can choose courses by write arguments separates by \',\'', ''],
            ['date', null, InputOption::VALUE_OPTIONAL, 'Date version. Script search version before this date. Format YYYY-mm-dd'],
        ];
    }
}
