<?php namespace Igvs\Courses\Console;

use Db;
use Config;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Helpers\Translit;
use Exception;

class FixFiles extends Command
{

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:fixfiles';

    /**
     * @var string The console command description.
     */
    protected $description = 'fix files';

    private $doFix;

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        if ($this->confirm("Do fix files and data? [y|N]", false)) {
            $this->doFix = true;
        }

        $course_id = $this->option('course');

        $courses = !$course_id ? Course::all() :
            Course::where('id', $course_id)->get();

        foreach ($courses as $course) {

            $modules = ModuleContent::where('course_id', $course->id)
                ->get();

            foreach ($modules as $module) {
                $count = $this->fixModule($module, $course);
            }

            $this->output->writeln("\n");
        }
    }

    private function fixModule($module, $course)
    {
        $dir = $module->getPathContents();
        $counter = 0;
        $files = [];

        $this->progressDir($dir, $files, $counter);

        // info
        if (!empty($files)) {
            $this->comment("COURSE: {$course->code}, MODULE: {$module->code}");

            $max = 0;
            foreach ($files as $k => $v) {
                if (strlen($k) > $max) {
                    $max = strlen($k);
                }
            }

            foreach ($files as $old => $new) {
                $len = strlen($old);
                $space = str_repeat(' ', $max - $len);
                $this->info("  {$old}{$space}  !=  {$new}");
            }

            $this->output->writeln("");
        }

        if ($this->doFix && !empty($files)) {
            $module->data = str_replace(array_keys($files), array_values($files), $module->data);
            $module->save();
        }

        return $counter;
    }

    public function progressDir($dir, &$files, &$counter)
    {
        if (! ($dh = opendir($dir)) ) {
            $this->ouput->writeln("Error to open dir: $dir");
            return;
        }

        while ($file = readdir($dh)) {

            if (in_array($file, ['.', '..', 'target-zones.css'])) {
                continue;
            }

            if (substr($file, 0, 1) == '.') {
                continue;
            }

            $path = $dir . DIRECTORY_SEPARATOR . $file;

            // fix filename
            $new_file = $this->fixFilename($file);

            if ($new_file != $file) {
                $counter++;

                $files[$file] = $new_file;

                if ($this->doFix) {
                    if (!rename($path, $dir . DIRECTORY_SEPARATOR . $new_file)) {
                        die ("Error to rename, abort");
                    }
                }
            }

            if (is_dir($path))
                $this->progressDir($path, $files, $counter);
        }

        closedir($dh);
    }

    private function fixFilename($str)
    {
        $replace = [
            "й" => "y", "Й" => "Y",
            "ц" => "ts", "Ц" => "Ts",
            "у" => "u", "У" => "U",
            "к" => "k", "К" => "K",
            "е" => "e", "Е" => "E",
            // "н" => "n", "Н" => "N",
            "н" => "h", "Н" => "H",
            "г" => "g", "Г" => "G",
            "ш" => "sh", "Ш" => "Sh",
            "щ" => "sh", "Щ" => "Sh",
            "з" => "z", "З" => "Z",
            // "х" => "h", "Х" => "H",
            "х" => "x", "Х" => "X",
            "ъ" => "", "Ъ" => "",
            "ф" => "f", "Ф" => "F",
            "ы" => "e", "Ы" => "E",
            "в" => "v", "В" => "V",
            "а" => "a", "А" => "A",
            "п" => "p", "П" => "P",
            // "р" => "r", "Р" => "R",
            "р" => "p", "Р" => "P",
            "о" => "o", "О" => "O",
            "л" => "l", "Л" => "L",
            "д" => "d", "Д" => "D",
            "ж" => "j", "Ж" => "J",
            "э" => "e", "Э" => "E",
            "я" => "ya", "Я" => "Ya",
            "ч" => "ch", "Ч" => "Ch",
            // "с" => "s", "С" => "S",
            "с" => "c", "С" => "C",
            "м" => "m", "М" => "M",
            "и" => "i", "И" => "I",
            "т" => "t", "Т" => "T",
            "ь" => "", "Ь" => "",
            "б" => "b", "Б" => "B",
            "ю" => "yu", "Ю" => "Yu",
            ":" => "",
        ];

        $separator = '-';
        $str = strtr($str, $replace);
        return preg_replace('![^' . preg_quote($separator) . '\s\(\)&\.\_+0-9A-Za-z\'\s]+!u', '', $str);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['course', null, InputOption::VALUE_REQUIRED, 'Course ID', null],
        ];
    }
}