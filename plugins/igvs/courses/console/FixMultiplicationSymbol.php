<?php namespace Igvs\Courses\Console;

use Db;
use Config;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Helpers\Translit;
use Exception;

class FixMultiplicationSymbol extends Command
{

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:fixmultiplicationsymbol';

    /**
     * @var string The console command description.
     */
    protected $description = 'fix multiplication symbol';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $course_id = $this->option('course');

        $this->info("Fix Multiplication in EBooks");
        $this->info("Replacing <span class=\"greek\">&times;</span> to <span class=\"greek\">&8729;</span>");

        if (!$this->confirm('Do this in ' . ($course_id ?: 'all') . ' course(s) ? [y|N]', false)) {
            $this->info("Goodbye!");
            exit();
        }

        $count = ModuleContent::withTrashed()
            ->where(function($q) use ($course_id) {
                if ($course_id) {
                    $q->where('course_id', $course_id);
                }
                $q->where('data', 'like', '%<span class=\\\\"greek\\\\">&times;%');
            })
            ->count();

        $this->info("All filtered modules count is: {$count}");

        $counter = 0;

        ModuleContent::withTrashed()
            ->where(function($q) use ($course_id) {
                if ($course_id) {
                    $q->where('course_id', $course_id);
                }
                $q->where('data', 'like', '%<span class=\\\\"greek\\\\">&times;%');
            })
            ->chunk(100, function ($modules) use (&$counter, $count) {

                foreach ($modules as $module) {

                    $counter++;

                    $this->info("{$counter} of {$count}");

                    try {
                        $str_pos = strpos($module->data, '<span class=\"greek\">&times;<\/span>');
                        $str_pos2 = strpos($module->data, '<span class=\"greek\">&times;</span>');

                        $this->info("{$module->id} {$module->name} Founded in pos: {$str_pos} | {$str_pos2}");

                        $module->data = str_replace(
                            [
                                '<span class=\"greek\">&times;<\/span>',
                                '<span class=\"greek\">&times;</span>',
                            ],
                            [
                                '<span class=\"greek\">&#8729;<\/span>',
                                '<span class=\"greek\">&#8729;</span>',
                            ],
                            $module->data);

                        $module->save();

                    } catch (\Exception $e) {

                        echo $e->getMessage() . "\r\n";
                        continue;
                    }

                    return true;
                }
            });

        $this->output->writeln("\n");
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['course', null, InputOption::VALUE_REQUIRED, 'Course ID', null],
        ];
    }
}
