<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use League\Flysystem\Exception;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\ModuleContent;

class ExportCourses extends Command
{
    use \Igvs\Courses\Traits\Scorm;
    use \Igvs\Courses\Traits\PrepareForFtp;
    use \Igvs\Courses\Traits\ExportToReleaseCloud;
    use \Igvs\Courses\Traits\ExportToPilot;
    use \Igvs\Courses\Traits\QueueCommand;
    use \Igvs\Courses\Traits\FileHelper;

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:exportcourses';

    /**
     * @var string The console command description.
     */
    protected $description = 'Экспорт курсов';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        try {
            $this->info("Welcome to export course master!\r\nSelect Export Type (or few separated by comma):\r\n");

            $arg_types = preg_split("/,/",$this->option('types'));
            $arg_courses = preg_split("/,/",$this->option('courses'));

            $this->info("Export types: 1.to pilot, 2.to maker, 3.to zip local");

            if (count($arg_types) == 1 && $arg_types[0] ==''){
                $this->info("Selected Export Types: no one. You can choose by write arguments separates by ',', ex. --types=2,3");
                exit();
            } else {
                $this->info("Selected Export Types: ".join(",",$arg_types));
                array_walk($arg_types, function(&$v, $k) {
                    $v = (int)$v;
                });
            }

            $courses_ids = [];

            if (count($arg_courses) == 1 && $arg_courses[0] =='') {
                $this->info("Selected courses: no one. You can choose courses by write arguments (courses) separates by ',', ex/--courses=23,87,35");
                exit();
            } else {
                $this->info("Selected courses: ".join(",",$arg_courses));

                foreach ($arg_courses as $course) {
                    $course = explode(':', $course);

                    $courses_ids[$course[0]] = isset($course[1]) ? $course[1] : null;
                }

                $courses = Course::whereIn('id', array_keys($courses_ids))->orderBy('id', 'desc')->get();
            }

            if (!$this->confirm('Вы хотите продолжить? [yes|no]')) return;

            $errors = [];

            $count_courses = count($courses);

            echo "\n\n";
            echo date('d-m-Y H:i:s') . " Всего курсов для выгрузки: " . $count_courses;
            echo "\n\n";

            foreach ($courses as $key => $course) {
                $course_id = $course->id;
                $build_id = isset($courses_ids[$course->id]) ? $courses_ids[$course->id] : null;

                echo date('d-m-Y H:i:s') . " " . ($key + 1) . "/" . $count_courses . " Ставим в очередь с id " . $course_id . " build_id=" . (!is_null($build_id)? $build_id : 'none') . ".. ";

                try {

                    $options = [
                        'courseId'      => $course_id,
                        'buildId'       => $build_id,
                        'topicId'       => null,
                        'theme'         => $course->include_all_themes ? 'all' : $course->theme,
                        'language'      => $course->language,
                        'date'          => date('Y-m-d'),
                        'postfix'       => '_' . rand(100, 999),
                        'beta_folder'   => null,
                        'to_igvs'       => 'false',
                        'to_scorm'      => 'false',
                        'to_cose'       => 'false',
                        'to_beta'       => 'false',
                        'to_pilot'      => in_array(1, $arg_types) ? 'true' : "false",
                        'to_zip'        => 'false',
                        'to_maker'      => in_array(2, $arg_types) ? 'true' : "false",
                        'to_zip_local'  => in_array(3, $arg_types) ? 'true' : "false",
                        'show_module_start_screen' => $course->show_module_start_screen ? 'true' : "false",
                        'set_topic_lock'  => 'false',
                        'set_transfer_user_to_review'  => 'false',
                        'replace_browsed' => 'true',
                        'export_without_module_content' => 'false',
                        'create_maker_config' => 'true',
                        'queue_name' => 'ExportToMaker',
                    ];

                    $db_id = $this->pushTask('Export to Release Cloud', 'exportToReleaseCloud', $options, 0);

                    $this->info('success db-id:' . $db_id);

                } catch (Exception $e) {

                    $errors[] = $course_id;
                    $this->error('fail');

                }
            }

            echo date('d-m-Y H:i:s') . " Конец. Все выбранные курсы поставили в очередь. Ошибки в курсах: " . (count($errors) ? implode(', ', $errors) : 'нет') . " \n\n";

        } catch(\Exception $e) {
            var_dump([
                $e->getMessage(),
                $e->getFile(),
                $e->getLine()
            ]);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        /*
         ['courses', InputArgument::OPTIONAL, 'Courses ID. You can choose courses by write arguments separates by \',\''],
        ['modules', InputArgument::OPTIONAL, 'Modules ID. You can choose template modules by write arguments separates by \',\''],
         */

        return [
            ['courses', null, InputOption::VALUE_OPTIONAL, 'Courses ID with Build. You can choose courses by write arguments separates by \',\', and write build after `:`, for example: 342:1,439:2,435,565', ''],
            ['types', null, InputOption::VALUE_OPTIONAL, 'Types of Export by write arguments separates by \',\'', ''],
            ['modules', null, InputOption::VALUE_OPTIONAL, 'Modules ID. You can choose template modules by write arguments separates by \',\'', ''],
            ['date', null, InputOption::VALUE_OPTIONAL, 'Date version. Script search version before this date. Format YYYY-mm-dd'],
        ];
    }
}
