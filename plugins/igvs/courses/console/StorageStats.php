<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use FilesystemIterator;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\StorageStats as Stats;
use Igvs\Courses\Models\StorageStatsDetail as StatsDetail;

class StorageStats extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'igvs.storagestats';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $this->output->writeLn('I collect statistics...');

        $content = realpath(Config::get('igvs.courses::modulesContent.path'));
        $storage = realpath(Config::get('igvs.courses::modulesContent.path') . '-storage');

        $courses = Course::withTrashed()
            ->orderBy('id', 'asc')
            ->get();

        $timestamp = time();


        $counter = [];

        foreach ($courses as $course) {

            $deleted = $course->deleted_at ? ', deleted_at: '.$course->deleted_at : '';
            $this->info("{$course->code} (id: {$course->id}{$deleted})");

            if (!is_dir("{$content}/{$course->id}")) {
                $this->output->writeLn('Пропущено, папка отсутствует');
                continue;
            }

            $start = microtime(true);

            foreach(new \RecursiveIteratorIterator (new RecursiveDirectoryIterator ("{$content}/{$course->id}")) as $file) {
                
                try {

                    if ($file->isDir()) {
                        continue;
                    }

                    // StorageStatsDetail

                    $hash = ModuleContent::getPathStorage($file->getRealPath());

                    $stats = Stats::firstOrNew([
                        'date' => $timestamp,
                        'hash' => $hash
                    ]);

                    // storage
                    if (!$stats->inode && file_exists("{$storage}/{$hash}")) {

                        $sf = new \SplFileInfo("{$storage}/{$hash}");

                        $cnt = shell_exec("ls -l --tabsize=1 {$sf->getRealPath()} | cut -f 2 --delimiter=' '");

                        $stats->fill([
                            'inode' => $sf->getInode(),
                            'size' => $sf->getSize(),
                            'is_storage' => true,
                            'count_links' => $cnt
                        ]);
                    }

                    $stats->save();

                    // link
                    $detail = StatsDetail::create([
                        'path' => $file->getRealPath(),
                        'size' => $file->getSize(),
                        'inode' => $file->getInode(),
                        'storage_stat_id' => $stats->id
                    ]);

                    // $detail->stat()->attach($stats->id);

                }
                catch (\Exception $e) {

                    dd([
                        'message' => $e->getMessage(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                        'src' => $file->getRealPath(),
                        // 'dist' => str_replace($backup, $content, $file->getRealPath())
                    ]);

                }
            }


            $time = microtime(true) - $start;
            $this->output->writeLn(sprintf('Время обработки %.4F сек.', $time));

        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}