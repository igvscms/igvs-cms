<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use FilesystemIterator;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Course;

class ClearCourses extends Command
{
    use \Igvs\Courses\Traits\FileHelper;
    /**
     * @var string The console command name.
     */
    protected $name = 'igvs.clearcourses';

    /**
     * @var string The console command description.
     */
    protected $description = 'Удаляет курсы';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $this->output->writeLn('relink content...');

        $content = realpath(Config::get('igvs.courses::modulesContent.path'));
        $storage = realpath(Config::get('igvs.courses::modulesContent.path') . '-storage');

        $courses = Course::onlyTrashed()
            ->orderBy('deleted_at', 'desc')
            ->get();

        $counter = [];

        foreach ($courses as $course) {

            $this->info("{$course->code} (id: {$course->id}, deleted_at: {$course->deleted_at})");

            // удаляем папку с контентом
            if (is_dir("{$content}/{$course->id}")) {
                $this->output->writeLn('Удаление папки с курсом...');
                $this->removeDir("{$content}/{$course->id}");
            }
            else {
                // $this->output->writeLn('Пропущено, папка отсутствует');
            }

            // Удаляем курс из бд
            // $this->output->writeLn('Удалиние из бд...');

            /*$start = microtime(true);
            $time = microtime(true) - $start;
            $this->output->writeLn(sprintf('Время обработки %.4F сек.', $time));*/

        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}