<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

use Igvs\Courses\Classes\Socket\Pusher;
use React\EventLoop\Factory as ReactLoop;
use React\ZMQ\Context as ReactContext;
use React\Socket\Server as ReactServer;
use React\Socket\SecureServer;
use Ratchet\Wamp\WampServer;

class PushServer extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'igvs.pushserver';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $loop = ReactLoop::create();

        $pusher = new Pusher;

        $context = new ReactContext($loop);

        $pull = $context->getSocket(\ZMQ::SOCKET_PULL);
        $pull->bind('tcp://127.0.0.1:5555');
        $pull->on('message', [$pusher, 'broadcast']);

        $port = $this->option('port');

        $webSoc = new ReactServer($loop);

        if ($cert = $this->option('cert')) {
            $webSoc = new SecureServer(
                $webSoc,
                $loop,
                [
                    'local_cert' => realpath($cert),
                    'allow_self_signed' => true,
                    'verify_peer' => false
                ]
            );
        }

        $webSoc->listen($port, '0.0.0.0');

        $webServer = new IoServer(
            new HttpServer(
                new WsServer(
                    new WampServer(
                        $pusher
                    )
                )
            ),
            $webSoc
        );

        $this->info('Run PusherServer - ' . $cert);

        $loop->run();
        // $this->output->writeln('Hello world!');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['cert', null, InputOption::VALUE_OPTIONAL, 'local certificate address \',\'', ''],
            ['port', null, InputOption::VALUE_OPTIONAL, 'service port \',\'', 8080],
        ];
    }
}
