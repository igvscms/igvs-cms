<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use League\Flysystem\Exception;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Models\ModuleContent;

class SearchModuleTypeDiff extends Command
{

    //https://habr.com/ru/post/45041/

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:searchmoduletypediff';

    private $log_path = '';
    private $chunk_count = 2000;

    // module template id => type test
    private $templates_codes_relation = [
        //1 => '', // Interactive Picture	Interactive Picture (deprecated)
        2 => ['cards'], // cards	Cards
        3 => ['cardspopup'], // cardspopup	Interactive Scheme
        4 => ['clothing'], // clothing	Clothing
        5 => ['dragdrop'], // dragDrop	Mapping 3 Way
        6 => ['game'], // game	Interactive Game
        7 => ['block'], // interactive-picture-popup	Interactive Picture
        8 => ['intro_aims', 'intro_objectives', 'intro_keywords'], // intro	Intro
        9 => ['mapping', 'mapping-pic'], // mapping	Mapping Text to Text
        10 => ['mapping_picture'], // mapping-picture	Mapping Picture to Text
        11 => ['menu'], // menu	Drop-down menu
        12 => ['picture-text'], // pictureTextDragDrop	Drag and Drop: Picture(s) into Text
        13 => ['animation'], // player	Animation
        14 => ['radiobutton_text', 'radiobutton_image', 'checkbox_text', 'checkbox_image'], // radiobutton-checkbox	Test Multiple Answers/Test Single Answer
        15 => ['sequence'], // sequence	Sequencing Text
        16 => ['sequence-picture'], // sequence-picture	Sequencing Pictures
        17 => ['slideshow'], // slideshow	Slideshow
        18 => ['sorting_image', 'sorting'], // sorting	Sorting
        19 => ['stepbystep'], // step-fab	i-Practice/i-Practice: Dialogue
        20 => ['stepbystep'], // stepbystep	Step By Step
        21 => ['sticker_dd', 'sticker_ti', 'sticker_ts'], // sticker	Drag and drop – into schemes and pictures/Drop down menu – into schemes and pictures/Text In – into schemes and pictures
        22 => ['sticker'], // sticker_old	Drag and Drop
        23 => ['tablein'], // table-input	Table Input
        24 => ['tabs'], // tabs	Interactive Tabs
        25 => ['textin'], // textin	Text Input
        26 => ['TextInDragDrop'], // textinDragDrop	Drag and Drop: Text Tabs into Text
        27 => ['crossword'], // crossword	Crossword
        28 => ['tree'], // tree	Tree
//        29 => '', // tree-new	Tree
        30 => ['work-action'], // work-action	Work Action
        31 => ['work-portfolio'], // work-portfolio	Work Portfolio
//        32 => '', // tabsundefined	tabsundefined
        33 => ['stepbystep'], // step-levelUp	i-Practice/i-Practice: Level Up
        34 => ['graph'], // graph	graph
        35 => ['html-markup'], // html-markup	HTML Markup
//        36 => '', // slideshow_new	Slideshow_new
//        37 => '', // videoPlayer	video player !!!
//        38 => '', // presentation	Presentation
        39 => ['mapping', 'mapping-pic'], // mapping_s	Mapping Text to Text (adapted for inclusive)
        40 => ['checkbox_text'], // radiobutton-checkbox_s	Test Multiple Answers/Test Single Answer (adapted for inclusive)
        41 => ['sequence'], // sequence_s	Sequencing Text (adapted for inclusive)
        42 => ['slideshow'], // slideshow_s	Slideshow (adapted for inclusive)
        43 => ['tabs'], // tabs_s	Interactive Tabs (adapted for inclusive)
        44 => ['TextInDragDrop'], // textinDragDrop_s	Drag and Drop Text into Text  (adapted for inclusive)
    ];

    /**
     * @var string The console command description.
     */
    protected $description = 'Выявление различия типа модуля в настройках и в контенте';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $date_time = date('Y_m_d-H_i_s');

        $this->log_path = __DIR__ . "/../../../../storage/logs/search_module_type_diff_{$date_time}.log";

        $start = microtime(true);

        $step = 0;
        $chunk_iterator = 0;

        $modules_count = ModuleContent::whereNull('deleted_at')->count();

        echo "Всего модулей: " . $modules_count . "\r\n\r\n";

        $this->writeFile(date("Y-m-d H:i:s") . " Старт", true);
        $this->writeFile("Всего модулей: " . $modules_count, true);

        ModuleContent::whereNull('deleted_at')
            ->whereNotIn('module_id', [1, 29, 32, 36, 37, 38,])
            ->orderBy('id')
            ->chunk($this->chunk_count, function($modules) use (&$step, &$chunk_iterator, $modules_count) {

                $chunk_iterator++;

                foreach ($modules as $module) {
                    $step++;

                    $this->writeConsole("{$chunk_iterator}*{$this->chunk_count}=" . ($chunk_iterator * $this->chunk_count) .
                        "\r\nModule {$step}/{$modules_count}  Module Id: {$module->id}", $step > 1 ? 1 : null);

                    try {
                        $json = json_decode($module->data);

                        if (!isset($json->questions)
                            || !isset($json->questions[0])
                            || !isset($json->questions[0]->typeTest))
                            continue;

                        $module_content_template_id = $module->module_id;

                        if (!isset($this->templates_codes_relation[$module_content_template_id]))
                            continue;

                        $template_type_test = $this->templates_codes_relation[$module_content_template_id];
                        $module_content_type_test = $json->questions[0]->typeTest;

                        if (in_array($template_type_test, $module_content_type_test))
                            file_put_contents($this->log_path, "{$module->id},", FILE_APPEND);
                            $this->writeFile("{$module->id},");

                    } catch (\Exception $e) {
                        continue;
                    }
                }

//                if ($chunk_iterator)
//                    sleep(1);
        });

        $this->info('Время выполнения скрипта: '.(microtime(true) - $start).' сек.');

    }

    private function writeConsole($text = null, $erase_row_count = null, $empty_row_count_after = null)
    {
        if (!is_null($erase_row_count)) {
            for ($i=0; $i < $erase_row_count; $i++) {
                echo "\033[1A\r"; # курсор вверх
                echo "\033[0K"; # очистить строку
            }
        }

        if (!is_null($text)) {
            echo $text;
        }
        if (!is_null($empty_row_count_after)) {
            echo str_repeat("\r\n", $empty_row_count_after);
        }
    }

    private function writeFile($text, $new_line_after = false)
    {
        file_put_contents($this->log_path, "{$text}", FILE_APPEND);

        if ($new_line_after)
            file_put_contents($this->log_path, "\r\n", FILE_APPEND);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
