<?php namespace Igvs\Courses\Console;

use Db;
use Config;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\ModuleContent;

class FixModuleCode extends Command
{
    const OS_WIN = 1;
    const OS_LINUX = 2;
    const OS_OTHER = 3;

    private $path;
    private $storage;

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:fixmodulecode';

    /**
     * @var string The console command description.
     */
    protected $description = 'Import course';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        Db::beginTransaction();

        try {

            Eloquent::unguard();


            $this->info('========================');
            $this->log('========================');
            $this->info('Собираем ID курсов');

            $course_count = 0;
            $module_count = 0;

            $course_ids = Course::lists('id');

            $this->info("Найдено " . count($course_ids) . " курсов");
            $this->log("Найдено " . count($course_ids) . " курсов");

            foreach ($course_ids as $course_id) {
                $this->info("Course ID: {$course_id}");
                $this->log("Course ID: {$course_id}");

                $modules = ModuleContent::where('course_id', $course_id)
                    ->where('code', '')
                    ->get();

                if (count($modules)) {
                    $course_count++;
                    $this->info(" -= Найдено " . count($modules) . " модулей");
                    $this->log(" -= Найдено " . count($modules) . " модулей");
                }

                foreach ($modules as $module) {
                    $module_count++;

                    $code_new = $this->getUniqueCode($course_id);

                    $this->log("Module id:{$module->id} Current code: '{$module->code}' New code: '{$code_new}'");

                    $module->code = $code_new;
                    $module->save();
                }
            }

            $this->info("Добавлены коды в {$module_count} модулей в {$course_count} курсах");
            $this->log("Добавлены коды в {$module_count} модулей в {$course_count} курсах");

            Db::commit();

        } catch (\Exception $e) {

            Db::rollBack();

            throw $e;
        }
    }

    /**
     * @param string $message
     */
    private function log($message)
    {
        $filename = __DIR__ . '/../../../../storage/app/igvs/fix-module-code.log';
        $date_time = date('Y-m-d H:i:s');

        file_put_contents($filename, $date_time . ' ' . $message . "\r\n", FILE_APPEND);
    }

    private function getUniqueCode($course_id)
    {
        $codes = ModuleContent::select('code')
            ->where('course_id', $course_id)
            ->lists('code');

        $code = uniqid();

        while (in_array($code, $codes)) {
            $code = uniqid();
        }

        return $code;
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

    function serverOS()
    {
        $sys = strtoupper(PHP_OS);
     
        if(substr($sys,0,3) == "WIN")
            return self::OS_WIN;

        if($sys == "LINUX")
            return self::OS_LINUX;
        
        return self::OS_OTHER;
    }
}