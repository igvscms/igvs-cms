<?php namespace Igvs\Courses\Console;

use Db;
use Config;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Database\Eloquent\Model as Eloquent;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Build;
use Igvs\Courses\Models\BuildModuleContent;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\ModuleVersion;
use Igvs\Courses\Models\Vocabulary;
use Igvs\Courses\Models\LogFile;


class ImportEbookOnly extends Command
{
    use \Igvs\Courses\Traits\FileHelper;

    const OS_WIN = 1;
    const OS_LINUX = 2;
    const OS_OTHER = 3;
    
    private $storage;

    /**
     * @var string temp DB name
     */
    private $db_name;

    /**
     * @var string structure
     */
    private $structure = [
        'topics' => [],
        'acts' => [],
        'modules' => [],
    ];


    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:importebookonly';

    /**
     * @var string The console command description.
     */
    protected $description = 'Import text book';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        Db::beginTransaction();

        try {

            $this->info("Welcome to Import Ebook Only (Specific Import :) )\r\n--------------------------\r\n");

            echo "Если ты видишь этот текст - значит тебя попросили загрузить модули типа Ebook в систему.\n\rПоложи несжатые файлы в доступный для скрипта каталог.\r\n";

            Eloquent::unguard();

            $this->storage = Config::get('igvs.courses::modulesContent.path');
            $dir_course = $this->option('path'); // path to one course

            if (substr($dir_course, -1, 1) == '/') {
                $dir_course = substr($dir_course, 0, strlen($dir_course)-1);
            }

            // парсим структуру
            $structure_file_path = $dir_course . '/structure.csv';
            if (!is_file($structure_file_path)) {
                $this->error('Файл структуры structure.csv не найден!');
                exit();
            }

            $this->parseStructure($structure_file_path);

            $this->importRubricator($dir_course);

            Db::commit();

        } catch (\Exception $e) {

            Db::rollBack();

            echo 'Error>> Line ' . $e->getLine() . ': ' . $e->getMessage() . "\r\n" . $e->getCode() . "\r\n" . $e->getTraceAsString();

            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['path', null, InputOption::VALUE_REQUIRED, 'Path to one course', null],
            ['act', null, InputOption::VALUE_OPTIONAL, 'ID Act', null],
            ['course_id', null, InputOption::VALUE_OPTIONAL, 'Course ID', null],
        ];
    }

    private function parseStructure($structure_file_path)
    {
        $i = 0;

        echo "Парсинг файла {$structure_file_path} ";

        $handle = fopen($structure_file_path, "r");
        while (!feof($handle)) {
            echo '.';

            $i++;

            if ($i == 1)
                continue;

            $buffer = fgets($handle, 4096);
            $buffer = mb_convert_encoding($buffer, 'utf-8', 'cp1251');

            $row = str_getcsv($buffer, ";");

            if (count($row) < 8) {
                continue;
            }
            $row['parent_id'] = (int)$row[5];

            //switch class
            switch ($row[8]) {
                case 'separator':
                    $this->structure['topics'][(int)$row[0]] = $row;
                    break;
                case 'group':
                    $this->structure['acts'][(int)$row[0]] = $row;
                    break;
                case 'item':
                    if ($row[4] == '6') { //type == 6  это ebook
                        $this->structure['modules'][(int)$row[0]] = $row;
                    }
                    break;
            }


        }
        fclose($handle);

        echo "\r\n";
    }

    private function importRubricator($path)
    {
        $course = $this->findCreateCourse($path);

        $topic = null;
        $topic_import_id_now = 0;

        echo "Крепим топики\r\n";

        $counter = 0;
        $count = count($this->structure['topics']);

        if ($count) {
            $this->info("Count categories:" . $count);

            foreach ($this->structure['topics'] as $structure_topic_id => $structure_topic) {
                $counter++;
                $topic_id = (int)$structure_topic[1];

                $this->info("Step:" . $counter . '/' . $count);

                if ($topic_id > 0 && $topic_id != $topic_import_id_now) {
                    if (!$topic = Category::find($topic_id)) {
                        echo "Топик не найден, создаём\r\n";
                        $topic = $this->createTopic($course);
                        $topic_id = $topic->id;

                        foreach ($this->structure['acts'] as $act_id => $act) {
                            if ($act['parent_id'] == $structure_topic_id) {
                                $act['parent_id'] = $topic_id;
                                $this->structure['acts'][$act_id] = $act;
                            }
                        }
                    }

                    $topic_import_id_now = $topic_id;
                }

                $this->attachActs2($topic, $topic_import_id_now, $path);
            }
        }
    }

    private function createTopic($course, $title = null)
    {
        if (is_null($title)) {
            $title = $course->title;
        }

        $topic = Category::create([
            'course_id' => $course->id,
            'code' => uniqid(),
            'name' => $title,
        ]);

        return $topic;
    }

    private function findCreateCourse($path)
    {
        try {
            $course_config = json_decode(file_get_contents($path . '/config.json'));
            $course_title = $course_config->title;
            $course_preview = $course_config->preview;
        } catch(\Exception $e) {
            $this->warn('Error read course config. Error text: ' . $e->getMessage());
            $arr = explode('/', $path);
            $course_title = array_pop($arr);
            $course_preview = '';
        }
        //разбираемся с курсом: либо его указал пользователь и курс существует, либо создаём сами
        if (!$course_id = $this->option('course_id')) {
            $course_id = $this->ask('Enter please Course ID: ');
        }
        if ($course_id && $course = Course::find($course_id)) {
            $this->info('Course founded.');
        }
        else {
            $this->warn('You don\'t write course ID or Course not found. We will be create new Course.');

            $course_data = [
                'title'         => $course_title,
                'status'        => 'alpha',
                'language'      => 'ru',
                'country'       => 'uk',
                'public_status' => 'published',
                'code' => uniqid()
            ];
            $course = Course::create($course_data);
            $course->save();
        }

        $this->info('Course ID:' . $course->id);
        $this->info('Course Title:' . $course->title);
        $this->info('Course Code:' . $course->code);
        $this->info('Course Preview:' . $path . '/' . $course_preview . ' It exist: ' . (file_exists($path . '/' . $course_preview) ? "Y" : "N"));

        return $course;
    }

    private function attachActs2($topic, $topic_id, $path)
    {
        echo "Крепим акты\r\n";

        foreach ($this->structure['acts'] as $act_id => $act) {
            if ($act['parent_id'] == $topic_id) {
                $modules = [];

                if (!$act_model = Category::find($act[1])) {
                    echo "Акт не найден, создаём\r\n";
                    $act_model = Category::create([
                        'course_id' => $topic->course_id,
                        'parent_id' => $topic->id,
                        'name' => $act[6]
                    ]);
                    foreach ($this->structure['modules'] as $module_id => $module) {
                        if ($module['parent_id'] == $act_id) {
                            $module['parent_id'] = $act_model->id;
                            $this->structure['modules'][$module_id] = $module;
                        }
                    }

                    $act_id = $act_model->id;
                }

                foreach ($this->structure['modules'] as $module_id => $module) {
                    if ($module['parent_id'] == $act_id) {
                        $modules[] = $module;
                    }
                }

                // atach modules
                $this->attachModules2($act_model, $modules, $path);
            }
        }

    }

    private function attachModules2($act, $modules, $path)
    {
        $sort = 1;
        $html_markup_template = Module::where('path', 'html-markup')->first();
        $html_markup_id = $html_markup_template ? $html_markup_template->id : -1;

        $module_template_version = $html_markup_template ? $html_markup_template->getLatestVersion() : '';

        $this->info('       Count modules ' . count($modules));

        foreach ($modules as $v) {
            $name = $v[6];

            $code = explode('/', $v[7]);
            $course_code = $code[0];
            $code = $code[count($code)-1];

            $resource_index = $code . '.html';


            $npath = "{$path}/{$resource_index}";

            $this->info('       Attach Module ' . $name . ' ' . $code);

            if (!file_exists($npath)) {
                $this->warn("index \"{$npath}\" not found!");
                continue;
            }

            $info = $this->getModuleInfo($npath, $name, $course_code);

            $m = ModuleContent::create([
                'course_id'         => $act->course_id,
                'category_id'       => $act->id,
                'name'              => $name,
                'module_id'         => $html_markup_id,
                'behavior'          => 'ask',
                'version'           => $module_template_version,
                'data'              => $info['data'],
                'content'           => $info['content'],
                'code'              => $code,
                'sort'              => ($sort++)
            ]);

            $storage = $m->getPathContents();


            $right_files_list = $this->getFileList($info['content']);


            if (file_exists("{$path}/resources")) {
                $this->copyFilesToStorage("{$path}/resources", $storage . '/resources', $m, $right_files_list);
            } else {
                $this->warn("resources \"{$path}/resources\" not found!");
            }

            if (file_exists("{$path}/design")) {
                $this->copyFilesToStorage("{$path}/design", $storage . '/design', $m);
            }
        }
    }

    private function copyFilesToStorage($path, $storage, $module, $right_files_list = null)
    {

        foreach(new \RecursiveIteratorIterator (new RecursiveDirectoryIterator ($path)) as $file)  {

            $file_name = $file->getFilename();

            // создаем папки
            $dir = str_replace($path, $storage, $file->getPath());
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }

            if ($file->isDir()) {
                continue;
            }

            if (!is_null($right_files_list) && !$this->is_right_file($file->getRealPath(), $right_files_list))
                continue;

            LogFile::refactorFile($storage, $file_name, $module->id, 0);

            // вычисление имени файла по has-1
            $storage_file = ModuleContent::saveInStorage($file->getRealPath());

            // создаем линк на файл в module-content
            if (!is_dir($storage)) {
                mkdir($storage, 0777, true);
            }

            if (!is_file($dir.'/'.$file_name)) {
//                link($storage_file, $dir.'/'.$file_name);
            }

        }
    }

    private function getModuleInfo($path, $name, $course_code)
    {
        $return = [];
        $content = file_get_contents($path);
        $content = str_replace('/shellserver/content/' . $course_code . '/book/', '', $content);
        $content = preg_replace('/\<base href=(.*)\>/', '', $content);

        preg_match('/\<title\>(.*)\<\/title\>/i', $content, $matches);

        $colontitle = count($matches) ? $matches[1] : $name;

        $return['colontitle'] = $colontitle;
        $return['content'   ] = $content;
        $return['data'      ] = json_encode((object) [
            'html' => $content,
            'questions' => [["typeTest" => "html-markup"]]
        ]);
        return $return;
    }

    function serverOS()
    {
        $sys = strtoupper(PHP_OS);

        if(substr($sys,0,3) == "WIN")
            return self::OS_WIN;

        if($sys == "LINUX")
            return self::OS_LINUX;

        return self::OS_OTHER;
    }
}