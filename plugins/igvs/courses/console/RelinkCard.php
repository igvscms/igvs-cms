<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use FilesystemIterator;
use Igvs\Courses\Models\ModuleContent;


class RelinkCard extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'igvs.relinkcard';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $this->output->writeLn('relink card...');

        $backup = realpath(Config::get('igvs.courses::modulesContent.path') . '/../card');
        $storage = realpath(Config::get('igvs.courses::modulesContent.path') . '-storage');
        $content = realpath(Config::get('igvs.courses::modulesContent.path') . '/../card' . '-new');

        foreach(new \RecursiveIteratorIterator (new RecursiveDirectoryIterator ($backup)) as $file) {

            try {
                // создаем папки
                $dir = str_replace($backup, $content, $file->getPath());
                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                }

                if ($file->isDir()) {
                    continue;
                }

                $src = ModuleContent::saveInStorage($file->getRealPath(), true);

                if (!is_file(str_replace($backup, $content, $file->getRealPath()))) {
                    link($src, str_replace($backup, $content, $file->getRealPath()));
                }
                else {
                    $this->output->writeLn('Файл пропущен ' . $file->getRealPath());
                }
            }
            catch (\Exception $e) {
                dd([
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'src' => $file->getRealPath(),
                    'dist' => str_replace($backup, $content, $file->getRealPath())
                ]);
            }
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
