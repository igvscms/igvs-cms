<?php namespace Igvs\Courses\Console;

use Db;
use Config;
use Illuminate\Console\Command;
use League\Flysystem\Exception;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Module;

class ImportCose extends Command
{
    use \Igvs\Courses\Traits\FileHelper;

    const OS_WIN = 1;
    const OS_LINUX = 2;
    const OS_OTHER = 3;

    private $storage;
    private $module_release;
    private $path;
    private $tmp;
    private $tmp_time;

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:importcose';

    /**
     * @var string The console command description.
     */
    protected $description = 'Import cose';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        Db::beginTransaction();

        try {

            Eloquent::unguard();

            $this->storage          = Config::get('igvs.courses::modulesContent.path');
            $this->module_release   = Config::get('igvs.courses::modulesRelease.path') . '/modules';
            $this->tmp              = Config::get('igvs.courses::cose.tmp');
            $this->tmp_time         = time();

            $path     = $this->option('path'); // path to one course
            $act_id         = $this->option('act'); // act id

            $this->path = $path;

            $input_param_errors = 0;

            echo "=== Start import === \n";
            echo "act id: " . $act_id . "\n";
            echo "path: " . $path . "\n";
            echo "-------------------- \n";

            if (is_null($act_id) || !strlen($act_id)) {
                echo "error. no act id. \n";
                $input_param_errors++;
            }

            if (is_null($path) || !strlen($path)) {
                echo "error. no dir path. \n";
                $input_param_errors++;
            }

            if (!is_dir($path)) {
                echo "error. dir is not exist. \n";
                $input_param_errors++;
            }

            if ($input_param_errors == 0) {
                // check exist act
                $act = Category::whereRaw('id = ?', [(int)$act_id])->first();
                if (!$act) {
                    echo "Act with id " . $act_id . " is not exist. \n";
                    return;
                }
                echo "Act is exist. \nImport.. \n";

                $scorm_list = $this->dirList($path);
                $this->unzipScormPacks($scorm_list);

                $unziped_list = $this->dirList($this->tmp . '/' . $this->tmp_time);
                $this->attachModulesFromFiles($act, $this->tmp . '/' . $this->tmp_time, $unziped_list);

                Db::commit();
                return;
            } else {
                echo "-------------------- \n";
                echo "Command has error(s). Stop. \n";
            }

            echo "==================== \n";

        } catch (\Exception $e) {

            Db::rollBack();

            throw $e;
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['path', null, InputOption::VALUE_REQUIRED, 'Path to one course', null],
            ['act', null, InputOption::VALUE_REQUIRED, 'Id of act', null],
        ];
    }

    private function dirList($path)
    {
        if (!is_dir($path)) {
            return;
        }
        $list = scandir($path);
        return array_diff($list, ['.', '..']);
    }

    private function unzipScormPacks($packslist)
    {
        if (!count($packslist)) {
            echo "No packs found.\n";
            return;
        }

        echo count($packslist) . " items founded. \n";

        foreach ($packslist as $v) {
            if (is_dir($this->path . '/' . $v)) {
                echo "\"" . $v . "\" is folder. Skip.\n";
                continue;
            }

            $file_info = pathinfo($this->path . '/' . $v);


            if ($file_info['extension'] != 'zip') {
                echo "File \"" . $v . "\" is not zip! Skip.";
            }

            $zip = new \ZipArchive();
            if (!$zip->open($this->path . '/' . $v)) {
                echo "\"" . $v . "\" is broken zip-archive. Skip.";
                continue;
            }

            echo "\"" . $v . "\" Archive extracting.. ";

            echo $zip->extractTo($this->tmp . '/' . $this->tmp_time . '/' . $file_info['filename']) ? "Done" : "Error!";
            echo "\n";

            $zip->close();
        }
    }

    private function attachModulesFromFiles($act, $path, $modules_list)
    {
        if (!count($modules_list)) {
            echo "No modules found.\n";
            return;
        }

        $sort = ModuleContent::where('course_id', $act->course_id)
            ->where('category_id', $act->id)
            ->max('sort');

        if (!$sort)
            $sort = 1;

        function search_name_folder($path, $iterator)
        {
            if (is_dir($path . '_' . $iterator)) {
                return search_name_folder($path, ++$iterator);
            } else {
                return $iterator;
            }
        }

        foreach ($modules_list as $v) {

            $resource = $v;
            $npath = "{$path}/{$resource}";

            if (!is_dir($npath) || !file_exists($npath.'/content/moduleData.js'))
                return;

            echo "Read data from " . $npath . "\n";

            $info = $this->getModuleInfo($npath);
            $storage = "{$this->storage}/{$act->course_id}/{$resource}";

            if (is_dir($storage)) {
                $folder_iterator = search_name_folder($storage, 0);
                $storage = $storage . '_' . $folder_iterator;
                $resource = $resource . '_' . $folder_iterator;
            }

            echo "Insert in to base module_id=" . $info['module_id'] . " behavior=" . $info['behavior'] . " version=" . $info['version'] . ".. ";

            $lesson = ModuleContent::create([
                'course_id'         => $act->course_id,
                'category_id'       => $act->id,
                'name'              => trim($v),
                'module_id'         => $info['module_id'],
                'behavior'          => $info['behavior'],
                'version'           => $info['version'],
                'data'              => $info['data'],
                'code'              => $resource,
                'sort'              => ($sort++)
            ]);

            echo $lesson ? "Done" : "Error!";
            echo  "\n";

            echo "Copy to {$storage}\n";

            echo "-- \n";
            $this->recourceCopy("{$npath}/content", $storage);
            unlink("{$storage}/moduleData.js");
            if (file_exists("{$storage}/template.js"))
                unlink("{$storage}/template.js");
        }
    }

    private function getModuleInfo($path)
    {
        $npath = "{$path}/content/moduleData.js";
        $version = '1';
        $module_template = '';
        $dir_list = is_dir($path) ? scandir($path) : [];

        if (count($dir_list)) {
            foreach ($dir_list as $dir_value) {

                if ($dir_value === 'metadata.json') {
                    $metadata = file_get_contents("{$path}/metadata.json");
                    $metadata = json_decode($metadata);
                    $module_template = $metadata->folder_name;
                    break;
                }

                // aliases modules
                $aliases = [
                    'intro.md' => ['md' => 'intro.md', 'version' => '1'],
                    'intro-new.md' => ['md' => 'intro.md', 'version' => '2'],
                    'intPicturePopup.md' => ['md' => 'interactive-picture-popup.md', 'version' => '1']
                ];

                if (isset($aliases[$dir_value])) {
                    $version = $aliases[$dir_value]['version'];
                    $dir_value = $aliases[$dir_value]['md'];
                }

                $module_template = stripos($dir_value, '.md');
                if ($module_template) {
                    $module_template = substr($dir_value, 0, $module_template);
                    break;
                }
            }
        }

        if (!file_exists($npath))
            throw new \Exception ("Not found file ({$npath})");

        $cmd = $this->serverOs() === self::OS_WIN ? 'node' : 'nodejs';
        $jspath = dirname(__FILE__).'/../updates/nodejs/json-decode.js';
        $json = shell_exec("{$cmd} {$jspath} {$npath}");
        $data = json_decode($json);

        if (!$data)
            throw new \Exception("Invalid JSON ({$dir_value})");

        $module = Module::where('path', $module_template)
            ->first();

        if (!$module)
            throw new \Exception("Not found module (module: {$module_template})");

        $practice = isset($data->practice) ? (boolean)$data->practice : false;
        $behavior = $this->getModuleBehavior($module, $practice);
        if (!$behavior)
            throw new \Exception("Not found behavior (module: {$module_template})");

        $json = str_replace(['"content/', '\'content/'], ['"', '\''], $json);

        return [
            'behavior'  => $behavior,
            'module_id' => $module->id,
            'version'   => $module->getLatestVersion($version),
            'data'      => $json
        ];
    }

    private function getModuleBehavior($module, $practice)
    {
        $module_path = $this->module_release . '/' . $module->path . '/' . $module->getLatestVersion();

        if (!file_exists($module_path))
            throw new \Exception("Not found module template (module: {$module->name}): " . $module_path);

        $metadata_filename = $module_path . '/metadata.json';
        $metadata = file_get_contents($metadata_filename);

        try {
            $metadata_json = json_decode($metadata);
            $metadata_behaviors = $metadata_json->behaviors_types;
        } catch (Exception $e) {
            throw new \Exception("Parse metadata json error (module: {$module->name})(" . print_r($e) . ")");
        }

        if (!count($metadata_behaviors)) {
            throw new \Exception("Module has not supporting behavior types (module: {$module->name})");
        }

        if (count($metadata_behaviors) != 2
            && in_array('test', $metadata_behaviors)
            && in_array('check', $metadata_behaviors)) {
            throw new \Exception("Module has too many supporting behavior types (module: {$module->name})");
        }

        if (count($metadata_behaviors) == 1) {
            return $metadata_behaviors[0];
        }

        return $practice ? 'check' : 'test';
    }


    function serverOS()
    {
        $sys = strtoupper(PHP_OS);

        if(substr($sys,0,3) == "WIN")
            return self::OS_WIN;

        if($sys == "LINUX")
            return self::OS_LINUX;

        return self::OS_OTHER;
    }
}