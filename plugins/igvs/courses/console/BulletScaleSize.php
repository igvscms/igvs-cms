<?php namespace Igvs\Courses\Console;

use Db;
use Config;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Build;
use Igvs\Courses\Models\BuildModuleContent;
use Igvs\Courses\Models\Module;

class BulletScaleSize extends Command
{
    use \Igvs\Courses\Traits\FileHelper;

    const OS_WIN = 1;
    const OS_LINUX = 2;
    const OS_OTHER = 3;

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:bulletscalesize';

    /**
     * @var string The console command description.
     */
    protected $description = 'BulletScaleSize';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        echo "---\r";

        $this->info("Welcome to Bullet Scale Size\r\n--------------------------\r\n");

        while (1) {
            $course_id = (int)$this->ask('Enter please Course ID: ');
            if ($course_id)
                break;
        }

        while (1) {
            $new_width = (int)$this->ask('Enter new WIDTH: ');
            if ($new_width)
                break;
        }
        while (1) {
            $new_height = (int)$this->ask('Enter old HEIGHT: ');
            if ($new_height)
                break;
        }


        $dir = __DIR__ . '/../../../../' . 'storage/app/igvs/module-content/' . $course_id;

        $query = "find {$dir} -iname 'bullet.svg'";

        exec($query, $res_ar);

        echo "Files count: " . count($res_ar) . " \r\n";

        if (!$this->confirm('Do you wish to continue? [yes|no]')) return;

        if (count($res_ar)) {
            foreach ($res_ar as $row) {
                if (!file_exists($row)) {
                    $this->error("File not found: {$row}");
                    continue;
                }

                $time = time();
                $dest_bu = preg_replace('/bullet\.(.*)/isU', "bul_bu_{$time}.$1", $row);

                //backup file
                copy($row, $dest_bu);

                $content = file_get_contents($row);

                $content = preg_replace('/width="(.*)([A-z]{0,2})"/isU', "width=\"{$new_width}$2\"", $content);
                $content = preg_replace('/height="(.*)([A-z]{0,2})"/isU', "height=\"{$new_height}$2\"", $content);
                $content = preg_replace('/viewBox="([\d\.]{1,})\s([\d\.]{1,})\s([\d\.]{1,})\s([\d\.]{1,})"/isU', "viewBox=\"$1 $2 {$new_width} {$new_height}\"", $content);
                $content = preg_replace('/enable-background="(.*)([\d\.]{1,})\s([\d\.]{1,})\s([\d\.]{1,})\s([\d\.]{1,})"/isU', "enable-background=\"$1$2 $3 {$new_width} {$new_height}\"", $content);

                file_put_contents($row, $content);

                echo '.';
            }

            echo "\n";
        }
        

        exit();

    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
//            ['path', null, InputOption::VALUE_OPTIONAL, 'Path to one course', null],
//            ['act', null, InputOption::VALUE_OPTIONAL, 'ID Act', null],
//            ['course_id', null, InputOption::VALUE_REQUIRED, 'Course ID', null],
        ];
    }

    function serverOS()
    {
        $sys = strtoupper(PHP_OS);
     
        if(substr($sys,0,3) == "WIN")
            return self::OS_WIN;

        if($sys == "LINUX")
            return self::OS_LINUX;
        
        return self::OS_OTHER;
    }
}