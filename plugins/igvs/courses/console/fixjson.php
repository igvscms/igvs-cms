<?php namespace Igvs\Courses\Console;

use Db;
use Config;
use Illuminate\Console\Command;

use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Module;

class Fixjson extends Command
{

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:fixjson';

    /**
     * @var string The console command description.
     */
    protected $description = 'fix json';

    private $doFix;

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $mid = Module::where('path','radiobutton-checkbox')->first();
        $contents = ModuleContent::where('module_id',$mid->id)->get();
        foreach($contents as $content)
        {
            $keys = [];
            $data = json_decode($content['data'],true);
            if(isset($data['questions']))
                foreach($data['questions'] as $key => $question)
                {
                    $res = false;
                    if(isset($question['answers']))
                        foreach($question['answers'] as $answer)
                        {
                            if(isset($answer['correct']))
                                if($answer['correct'] == 'true')
                                {
                                    $res = true;
                                    break;
                                }
                        }
                    if(!$res)
                        $keys[$key] = 1;
                }
            if(count($keys)>0)
            {
                echo $content->course_id.' - '.$content->code.': '.$content->name."\r\n";
            }
        }
    }
}