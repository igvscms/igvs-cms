<?php namespace Igvs\Courses\Console;

use Igvs\Courses\Classes\PublicationState;
use Igvs\Courses\Traits\ExportHelper;
use Illuminate\Console\Command;
use League\Flysystem\Exception;
use Symfony\Component\Console\Input\InputOption;

use Igvs\Courses\Models\Course;

class PublicState extends Command
{

    use ExportHelper;
    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:publicstate';

    /**
     * @var string The console command description.
     */
    protected $description = 'Public State of Course';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        try {
            $this->info("Welcome to export course master!\r\n");

            $courses_id_build = $this->getCoursesFromCommaString($this->option('courses'));

            if (!count($courses_id_build)) {
                $this->info("Selected courses: no one. You can choose courses by write arguments (courses) separates by ',', ex/--courses=23,87,35");
                exit();
            } else {

                $courses_id = array_keys($courses_id_build);
                $this->info("Selected courses: ".join(",",$courses_id));

                $courses = Course::whereIn('id', $courses_id)->orderBy('id', 'desc')->get();
            }

            if (!$this->confirm('Do you want to continue? [yes|no]')) return;

            $errors = [];

            $count_courses = count($courses);

            echo "\n\n";
            $this->echoMessage("Count of all prepared for publication courses: {$count_courses}");

            foreach ($courses as $key => $course) {
                $course_id = $course->id;
                $build_id = isset($courses_ids[$course->id]) ? $courses_ids[$course->id] : null;

                $this->echoMessage(($key + 1) . "/{$count_courses} In progress ID {$course_id} build_id=" . (!is_null($build_id)? $build_id : 'none') . ".. ");

                try {

                    $publication_state = new PublicationState([
                        \Igvs\Courses\Models\PublicationState::class,
                        \Igvs\Courses\Models\PublicationStateGlobal::class,
                    ]);
                    $publication_state->create($course_id, $build_id);

                    $this->info('success');

                } catch (Exception $e) {

                    $errors[] = $course_id;
                    $this->error('fail');

                }
            }

            $this->echoMessage("Finish. All selected courses were published. Errors: " . (count($errors) ? implode(', ', $errors) : 'нет'));

        } catch(\Exception $e) {
            var_dump([
                $e->getMessage(),
                $e->getFile(),
                $e->getLine()
            ]);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    private function echoMessage($msg)
    {
        echo date('d-m-Y H:i:s') . " ${msg}\r\n";
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['courses', null, InputOption::VALUE_OPTIONAL, 'Courses ID with Build. You can choose courses by write arguments separates by \',\', and write build after `:`, for example: 342:1,439:2,435,565', ''],
        ];
    }
}
