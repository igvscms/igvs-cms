<?php namespace Igvs\Courses\Console;

use Db;
use Config;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Helpers\Translit;
use Exception;

class FixCommon extends Command
{

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:fixcommon';

    /**
     * @var string The console command description.
     */
    protected $description = 'fix common';

    private $doFix;

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        if ($this->confirm("Do fix files and data? [y|N]", false)) {
            $this->doFix = true;
        }

        $course_id = $this->option('course');

        $courses = !$course_id ? Course::all() :
            Course::where('id', $course_id)->get();

        foreach ($courses as $course) {

            $modules = ModuleContent::withTrashed()
                ->where('course_id', $course->id)
                ->get();

            foreach ($modules as $module) {

                try {

                    $old_path = $new_path = Config::get('igvs.courses::modulesContent.path') . "/{$module->course_id}/{$module->code}";
                    $new_path = Config::get('igvs.courses::modulesContent.path') . "/{$module->course_id}/{$module->id}";

                    if ($this->doFix) {
                        rename($old_path, $new_path);
                    }
                    else {
                        if (!file_exists($old_path))
                            $this->info("Not found. Course: {$module->course_id}, Module: {$module->code}");
                    }


                } catch (\Exception $e) {

                    echo $e->getMessage() . "\r\n";
                    continue;
                }

            }

            $this->output->writeln("\n");
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['course', null, InputOption::VALUE_REQUIRED, 'Course ID', null],
        ];
    }
}