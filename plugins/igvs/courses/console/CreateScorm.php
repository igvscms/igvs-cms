<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\ModuleContent;

class CreateScorm extends Command
{
    use \Igvs\Courses\Traits\FileHelper;
    use \Igvs\Courses\Traits\Scorm;

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:createscorm';

    /**
     * @var string The console command description.
     */
    protected $description = 'Create SCORM';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $courseId = $this->option('course');
        $buildId = $this->option('build');
        $theme = $this->option('theme');

        $this->createScorm($courseId, $buildId, $theme);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            // ['course', InputArgument::REQUIRED, 'Course ID.'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['course', null, InputOption::VALUE_REQUIRED, 'Course ID', null],
            ['build', null, InputOption::VALUE_OPTIONAL, 'Build ID', null],
            ['theme', null, InputOption::VALUE_OPTIONAL, 'Color Theme', '_default'],
        ];
    }
}