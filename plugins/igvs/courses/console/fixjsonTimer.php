<?php namespace Igvs\Courses\Console;

use Db;
use Config;
use Illuminate\Console\Command;

use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\ModuleContentHistory;

class FixjsonTimer extends Command
{

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:fixjsontimer';

    /**
     * @var string The console command description.
     */
    protected $description = 'fix json timer';

    private $doFix;

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        //$mid = Module::where('path','radiobutton-checkbox')->first();
        $contents = ModuleContent::where('module_id','!=',0)->orderBy('course_id')->orderBy('category_id')->get();
        foreach($contents as $content)
        {
            $data = json_decode($content['data'],true);
            if(isset($data['timer']) && $data['timer'] === false)
            {
                $skip = 0;
                while(true)
                {
                    $history = ModuleContentHistory::where('module_content_id',$content->id)->orderBy('created_at')->skip($skip++)->take(1)->get();
                    if(!isset($history[0]))
                        break;
                    try
                    {
                        $data = $history[0]->getPrevVersion();
                    }
                    catch(\Exception $e)
                    {
                        echo 'Ошибка Json: Курс: '.$content->course_id.' Категория: '.$content->category_id.' '.$content->code.': '.$content->name.' id = '.$history[0]->id."\r\n";
                        break;
                    }

                    if(isset($data['timer']) && $data['timer'] !== false)
                    {
                        echo 'false timer: Курс: '.$content->course_id.' Категория: '.$content->category_id.' '.$content->code.': '.$content->name.' timer = '.$data['timer']."\r\n";
                        break;
                    }
                }
            }
        }
    }
}