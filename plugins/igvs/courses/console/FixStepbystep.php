<?php namespace Igvs\Courses\Console;

use Db;
use Config;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Build;
use Igvs\Courses\Models\BuildModuleContent;
use Igvs\Courses\Models\Module;
use Exception;

class FixStepbystep extends Command
{

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:fixstepbystep';

    /**
     * @var string The console command description.
     */
    protected $description = 'fix lmv';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $course_id = $this->option('course');

        Db::beginTransaction();

        try {

            Eloquent::unguard();

            $tpl = Module::where('path', 'stepbystep')
                ->first();

            $modules = ModuleContent::where('course_id', $course_id)
                ->where('module_id', $tpl->id)
                // ->where('behavior', 'check')
                ->get();

            foreach ($modules as $v) {

                if ($v->behavior == 'test') {
                    $v->delete();
                    continue;
                }

                $v->behavior = 'practice';
                $v->data = '{
                    "practice_data": ' . $v->data . ',
                    "assessment_data": ' . $this->getJsonAssessment($v) . '
                }';

                $v->save();

                $this->output->writeln($v->code);
            }

            Db::commit();

        } catch (Exception $e) {

            Db::rollBack();

            dd([ 
                'message' => $e->getMessage(),
                'line' => $e->getLine()
            ]);

             throw $e;
        }
    }

    public function getJsonAssessment($module)
    {
        // convert js object to json
        $path = $module->getPathContents();
        $path = "$path/moduleDataControl.js";

        if (!file_exists($path))
            throw new Exception("Not found file {$path}");

        $jspath = dirname(__FILE__).'/../updates/nodejs/json-decode.js';
        $json = shell_exec("nodejs {$jspath} {$path}");

        if (empty($json))
            throw new Exception("ERROR! Not valid JSON!");

        return str_replace(['"content/', '\'content/'], ['"', '\''], $json);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['course', null, InputOption::VALUE_REQUIRED, 'Course ID', null],
        ];
    }
}