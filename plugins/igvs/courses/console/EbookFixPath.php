<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Course;

class EbookFixPath extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'igvs.ebookfixpath';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $start = microtime(true);

        $counter = 0;

        $this->info("Welcome to Ebook Fix Path\r\n--------------------------\r\n");

        $course_id = $this->ask('Enter please Course ID: ');

        ModuleContent::where('module_id', 35)
            ->where('course_id', $course_id)
            ->chunk(10, function ($part) use ($counter) {

            $this->output->writeLn('part ' . (++$counter));

            foreach ($part as $module) {
                $path = $module->getPathContents();

                if (!is_dir($path.'/resources')) {
                    // mkdir($path.'/resources');
                    $this->output->writeLn('mkdir '.$path.'/resources');
                }

                if (is_dir($path.'/images')) {
                    // rename($path.'/images', $path.'/resources/images');
                    $this->output->writeLn('mv ' . $path.'/images' . ' ' . $path.'/resources/images');
                }

                if (is_dir($path.'/image')) {
                    // rename($path.'/image', $path.'/resources/image');
                    $this->output->writeLn('mv ' . $path.'/image' . ' ' . $path.'/resources/image');
                }

            }
        });

        $time = microtime(true) - $start;
        $this->output->writeLn(sprintf('Время обработки %.4F сек.', $time));
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}