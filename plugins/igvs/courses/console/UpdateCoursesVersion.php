<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use League\Flysystem\Exception;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\ModuleContent;

class UpdateCoursesVersion extends Command
{
    use \Igvs\Courses\Traits\FileHelper;

    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:updateversion';

    /**
     * @var string The console command description.
     */
    protected $description = 'Обновление версий модулей курсов';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        try {
            $arg_courses = preg_split("/,/",$this->option('courses'));
            $arg_modules = preg_split("/,/",$this->option('modules'));
            $arg_date = $this->option('date');

            if (count($arg_courses) == 1 && $arg_courses[0] ==''){
                $this->info("Selected courses: all. You can choose courses by write arguments (courses) separates by ','");
                $courses = Course::orderBy('id', 'desc')->get()->lists('id');
            } else {
                $this->info("Selected courses: ".join(",",$arg_courses));
                $courses = $arg_courses;
            }

            if (count($arg_modules) == 1 && $arg_modules[0] ==''){
                $this->info("Selected module templates: all. You can choose modules by write arguments (modules) separates by ','");
                $arg_modules = [];
            } else {
                $this->info("Selected module templates: ".join(",",$arg_modules));
            }

            if (!strlen($arg_date) && $arg_courses[0] =='') {
                $arg_date = null;
            }

            try {
                strtotime($arg_date);
            } catch (\Exception $e) {
                $arg_date = null;
            }

            if (!$this->confirm('Do you wish to continue? [yes|no]')) return;

            $errors = [];

            $count_courses = count($courses);

            echo "\n\n";
            echo date('d-m-Y H:i:s') . " Всего курсов для обновления: " . $count_courses;
            echo "\n\n";
            
            foreach ($courses as $key => $course_id) {
                echo date('d-m-Y H:i:s') . " " . ($key + 1) . "/" . $count_courses . " Обновляем курс с id " . $course_id . ".. ";

                try {
                    $count_content_modules = ModuleContent::where('course_id', trim($course_id));
                    if (is_array($arg_modules) && count($arg_modules) > 0){
                        $count_content_modules = $count_content_modules->whereIn('module_id', $arg_modules);
                    }
                    $count_content_modules = $count_content_modules->count();

                    echo "найдено модулей: {$count_content_modules}. Обновление.. ";

                    $callback = function ($contents) use($arg_date) {
                        foreach ($contents as $content) {
                            $content->force_history_save = true;
                            $content->version = $content->module->getLatestAvailable($content->getModuleVersion(), $arg_date);
                            $content->save();
                        }
                    };

                    $module_content = ModuleContent::where('course_id', trim($course_id));
                    if (is_array($arg_modules) && count($arg_modules) > 0){
                        $module_content = $module_content->whereIn('module_id', $arg_modules);
                    }
                    $module_content->chunk(100, $callback);

                    $this->info('success');

                } catch (Exception $e) {

                    $errors[] = $course_id;
                    $this->error('fail');

                }
            }

            echo date('d-m-Y H:i:s') . " Конец обновления. Ошибки в курсах: " . (count($errors) ? implode(', ', $errors) : 'нет') . " \n\n";

        } catch(\Exception $e) {
            var_dump([
                $e->getMessage(),
                $e->getFile(),
                $e->getLine()
            ]);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        /*
         ['courses', InputArgument::OPTIONAL, 'Courses ID. You can choose courses by write arguments separates by \',\''],
        ['modules', InputArgument::OPTIONAL, 'Modules ID. You can choose template modules by write arguments separates by \',\''],
         */

        return [
            ['courses', null, InputOption::VALUE_OPTIONAL, 'Courses ID. You can choose courses by write arguments separates by \',\'', ''],
            ['modules', null, InputOption::VALUE_OPTIONAL, 'Modules ID. You can choose template modules by write arguments separates by \',\'', ''],
            ['date', null, InputOption::VALUE_OPTIONAL, 'Date version. Script search version before this date. Format YYYY-mm-dd'],
        ];
    }
}