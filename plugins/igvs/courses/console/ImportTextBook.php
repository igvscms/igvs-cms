<?php namespace Igvs\Courses\Console;

use Db;
use Config;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Database\Eloquent\Model as Eloquent;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Build;
use Igvs\Courses\Models\BuildModuleContent;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\ModuleVersion;
use Igvs\Courses\Models\Vocabulary;
use Igvs\Courses\Models\LogFile;


class ImportTextBook extends Command
{
    use \Igvs\Courses\Traits\FileHelper;

    const OS_WIN = 1;
    const OS_LINUX = 2;
    const OS_OTHER = 3;

    private $path;
    private $storage;

    /**
     * @var string temp DB name
     */
    private $db_name;


    /**
     * @var string The console command name.
     */
    protected $name = 'igvs:importtextbook';

    /**
     * @var string The console command description.
     */
    protected $description = 'Import text book';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        Db::beginTransaction();

        try {

            $this->info("Welcome to Import TextBook\r\n--------------------------\r\n");


            Eloquent::unguard();

            $this->storage = Config::get('igvs.courses::modulesContent.path');
            $dir_course = $this->option('path'); // path to one course

            // добавляем модули в существующий акт
            $act_id = $this->option('act');


            // создаём временные настройки соединения с БД
            $db_name = uniqid();
            $this->db_name = $db_name;

            $db_path = $dir_course . '/db/shelldb_static.db';

            if (!file_exists($db_path)) {
                $this->error('Not found file db: ' . $db_path);
                exit();
            }

            Config::set('database.connections.'.$db_name, array(
                'driver' => 'sqlite',
                'database' => $db_path,
            ));

            $this->importRubricator($dir_course);
            Db::commit();

        } catch (\Exception $e) {

            Db::rollBack();

            echo $e->getMessage();

            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['path', null, InputOption::VALUE_REQUIRED, 'Path to one course', null],
            ['act', null, InputOption::VALUE_OPTIONAL, 'ID Act', null],
            ['course_id', null, InputOption::VALUE_OPTIONAL, 'Course ID', null],
        ];
    }

    private function importRubricator($path)
    {
        $course = $this->findCreateCourse($path);

        $import_topics = [];

        // находим топики
        $query = "select * from navigation where type = 'separator' order by id";
        $result = DB::connection($this->db_name)->select($query);
        if (count($result)) {
            foreach ($result as $row) {
                $import_topics[$row->id] = (strlen($row->desc) ? $row->desc . '||' : '') . $row->text;
            }
        }

        // находим акты
        $query = "select * from navigation where parent = 0 and type <> 'separator' order by id";
        $result = DB::connection($this->db_name)->select($query);

        $topic = null;
        $topic_import_id_now = 0;

        $counter = 0;
        $count = $result && count($result) ? count($result) : 0;

        if ($result && count($result)) {
            $this->info("Count categories:" . count($result));

            foreach ($result as $db_topic) {
                $counter++;
                $topic_id = $this->getTopicId($import_topics, $db_topic->id);

                if ($topic_id > 0 && $topic_id != $topic_import_id_now) {
                    $topic_import_id_now = $topic_id;
                    $topic = $this->createTopic($course, $import_topics[$topic_id]);
                } elseif($topic_id == 0 && is_null($topic)) {
                    $topic = $this->createTopic($course);
                }

                $this->info("Step:" . $counter . '/' . $count);

                $this->attachActs($topic, $db_topic, $path);
            }
        }


        $this->importVocabulary($course);

        $this->info('Course ID: ' . $course->id);
    }

    private function getTopicId($import_topics, $act_id)
    {
        if (!count($import_topics)) {
            return 0;
        }

        $return_id = 0;

        foreach ($import_topics as $it_k => $it_v) {
            if ($it_k < $act_id && $it_k > $return_id) {
                $return_id = $it_k;
            }
        }

        return $return_id;
    }

    private function createTopic($course, $title = null)
    {
        if (is_null($title)) {
            $title = $course->title;
        }

        $topic = Category::create([
            'course_id' => $course->id,
            'code' => uniqid(),
            'name' => $title,
        ]);

        return $topic;
    }

    private function findCreateCourse($path)
    {
        try {
            $course_config = json_decode(file_get_contents($path . '/config.json'));
            $course_title = $course_config->title;
            $course_preview = $course_config->preview;
        } catch(\Exception $e) {
            $this->warn('Error read course config. Error text: ' . $e->getMessage());
            $arr = explode('/', $path);
            $course_title = array_pop($arr);
            $course_preview = '';
        }
        //разбираемся с курсом: либо его указал пользователь и курс существует, либо создаём сами
        $course_id = $this->option('course_id');
            
        if ($course_id && $course = Course::find($course_id)) {

            if (!$this->confirm("You select course with ID {$course_id}.\r\nIf the Course ID: ALL DATA IN THIS COURSE WILL BE DELETED!!\r\nDo you wish to continue? [yes|no]")) return;

            $modules = ModuleContent::where('course_id', $course_id)
                ->get();

            foreach ($modules as $module) {
                $this->removeDir($module->getPathContents());
                $module->delete();
            }

            $categories = Category::where('course_id', $course_id)
                ->get();

            foreach ($categories as $category) {
                $category->delete();
            }

            $replace_title_logo_ask = $this->ask('Course founded. Title and logo will be replace. You sure?');

            if ($replace_title_logo_ask) {
                try {
                    $course->title = $course_title;
                    $course->logo = $path . '/' . $course_preview;
                    $course->language = 'ru';
                    $course->save();
                } catch (\Exception $e) {
                    $this->warn('Error update course propertes. Error text: ' . $e->getMessage());
                }
            }
        }
        else {
            $this->warn('You don\'t write course ID or Course not found. We will be create new Course.');

            $course_preview_path = (strlen($course_preview) && is_file($path . '/' . $course_preview)) ? $path . '/' . $course_preview : '';

            $course_data = [
                'title'         => $course_title,
                'status'        => 'alpha',
                'language'      => 'ru',
                'country'       => 'uk',
                'public_status' => 'published',
                'logo' => $course_preview_path,
                'code' => uniqid()
            ];
            $course = Course::create($course_data);
            $course->save();
        }

        $this->info('Course ID:' . $course->id);
        $this->info('Course Title:' . $course->title);
        $this->info('Course Code:' . $course->code);
        $this->info('Course Preview:' . $path . '/' . $course_preview . ' It exist: ' . (file_exists($path . '/' . $course_preview) ? "Y" : "N"));

        return $course;
    }

    private function attachActs($topic, $db_act, $path)
    {
        $this->info('   Attach Act ' . $db_act->text . ' ' . $db_act->resource);

        $name = (strlen($db_act->desc) ? $db_act->desc . '||' : '') . $db_act->text;

        $act = Category::create([
            'course_id' => $topic->course_id,
            'parent_id' => $topic->id,
            'name' => $name
        ]);

        // atach modules
        $query = "select * from navigation where parent = " . $db_act->id . " order by number, id";
        $db_modules = DB::connection($this->db_name)->select($query);

        $this->attachModules($act, $path, $db_modules);

    }

    private function attachModules($act, $path, $modules)
    {
        $sort = 1;
        $html_markup_id = Module::where('path', 'html-markup')->first();
        $html_markup_id = $html_markup_id ? $html_markup_id->id : -1;

        $this->info('       Count modules ' . count($modules));

        foreach ($modules as $v) {
            $name = (strlen($v->desc) ? $v->desc . '||' : '') . $v->text;
            $resource_index = $v->resource . '.html';
            $resource = str_replace('/index', '', $v->resource);

            $npath = "{$path}/{$resource_index}";

            $this->info('       Attach Module ' . $name . ' ' . $resource);

            if (!file_exists($npath)) {
                $this->warn("index \"{$npath}\" not found!");
                continue;
            }

            $module_template = $this->getModuleTemplate("{$path}/{$resource}");
            $module_template_version = $module_template ? $module_template->getLatestVersion() : '';
            $info = $this->getModuleInfo($npath, $name);
            $data = $module_template ? $this->getModuleData("{$path}/{$resource}") : $info['data'];
            $content = $module_template ? '' : $info['content'];

            $m = ModuleContent::create([
                'course_id'         => $act->course_id,
                'category_id'       => $act->id,
//                'name'              => $info['colontitle'],
                'name'              => $name,
                'module_id'         => $module_template ? $module_template->id : $html_markup_id,
                'behavior'          => $v->class,
                'version'           => $module_template_version,
                'data'              => $data,
                'content'           => $content,
                'code'              => $resource,
                'sort'              => ($sort++)
            ]);

            $storage = $m->getPathContents();

            $right_files_list = $this->getFileList($content);

            if ($v->class == 'page') {
                if (file_exists("{$path}/resources")) {
                    $this->copyFilesToStorage("{$path}/resources", $storage . '/resources', $m, $right_files_list);
                } else {
                    $this->warn("resources \"{$path}/resources\" not found!");
                }

                if (file_exists("{$path}/design")) {
                    $this->copyFilesToStorage("{$path}/design", $storage . '/design', $m);
                }
            }
            if ($v->class == 'module') {
                if (file_exists("{$path}/{$resource}")) {
                    $this->copyFilesToStorage("{$path}/{$resource}", $storage, $m);
                } else {
                    $this->warn("resources \"{$path}/{$resource}\" not found!");
                }

                if (file_exists("{$path}/design")) {
                    $this->copyFilesToStorage("{$path}/design", $storage . '/design', $m);
                }
            }
        }
    }

    private function copyFilesToStorage($path, $storage, $module, $right_files_list = null)
    {

        foreach(new \RecursiveIteratorIterator (new RecursiveDirectoryIterator ($path)) as $file)  {

            $file_name = $file->getFilename();

            // создаем папки
            $dir = str_replace($path, $storage, $file->getPath());
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }

            if ($file->isDir()) {
                continue;
            }

            if (!is_null($right_files_list) && !$this->is_right_file($file->getRealPath(), $right_files_list))
                continue;

            LogFile::refactorFile($storage, $file_name, $module->id, 0);

            // вычисление имени файла по has-1
            $storage_file = ModuleContent::saveInStorage($file->getRealPath());

            // создаем линк на файл в module-content
            if (!is_dir($storage)) {
                mkdir($storage, 0777, true);
            }

            if (!is_file($dir.'/'.$file_name)) {
                link($storage_file, $dir.'/'.$file_name);
            }

        }
    }

    private function getModuleInfo($path, $name)
    {
        $return = [];
        $content = file_get_contents($path);

        preg_match('/\<title\>(.*)\<\/title\>/i', $content, $matches);

        $colontitle = count($matches) ? $matches[1] : $name;

        $return['colontitle'] = $colontitle;
        $return['content'   ] = $content;
        $return['data'      ] = json_encode((object) [
            'html' => $content,
            'questions' => [["typeTest" => "html-markup"]]
        ]);
        return $return;
    }

    private function getModuleData($path)
    {
        $npath = "{$path}/content/moduleData.js";

        if (!file_exists($npath)) {
            $this->warn("{$npath} not found!");
            return '';
        }

        $cmd = $this->serverOs() === self::OS_WIN ? 'node' : 'nodejs';
        $jspath = dirname(__FILE__).'/../updates/nodejs/json-decode.js';
        $json = shell_exec("{$cmd} \"{$jspath}\" \"{$npath}\"");
        $data = json_decode($json);

        if (!$data) {
            $this->warn("{$npath} Invalid JSON");
            return '';
        }

        return $json;
    }

    private function getModuleTemplate($path)
    {
        $matches = null;

        if(!file_exists($path))
            return false;

        $files_list = scandir($path);

        if (!is_array($files_list) || !count($files_list))
            return false;

        foreach ($files_list as $file) {
            $matches = null;

            if (in_array($file, ['.', '..']))
                continue;

            preg_match("/(.*)\.md/", $file, $matches);

            if (is_array($matches) && isset($matches[1]))
                break;
        }

        if (!is_array($matches) || !isset($matches[1]))
            return false;

        $module = Module::where('path', $matches[1])->first();

        if (!$module) {
            $this->warn(".md module \"{$matches[1]}\" not found!");
            return false;
        }


        return $module;
    }

    private function importVocabulary($course)
    {
        $query = "select * from vocabulary order by id";
        $result = DB::connection($this->db_name)->select($query);

        if ($result && count($result)) {
            $this->info("Count vocabulary:" . count($result));

            foreach ($result as $voc) {
                $this->createVocabulary($course->id, $voc->term, $voc->description);
            }
        }
    }

    private function createVocabulary($course_id, $term, $description)
    {
        $term = str_replace(chr( 194 ) . chr( 160 ), ' ', $term);
        $description = str_replace(chr( 194 ) . chr( 160 ), ' ', $description);

        $term = trim($term);
        $description = trim($description);

        $vocabulary = Vocabulary::create([
            'course_id' => $course_id,
            'term' => $term,
            'description' => $description,
        ]);

        return $vocabulary;
    }

    function serverOS()
    {
        $sys = strtoupper(PHP_OS);

        if(substr($sys,0,3) == "WIN")
            return self::OS_WIN;

        if($sys == "LINUX")
            return self::OS_LINUX;

        return self::OS_OTHER;
    }
}