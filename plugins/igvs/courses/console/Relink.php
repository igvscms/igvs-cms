<?php namespace Igvs\Courses\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use FilesystemIterator;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Course;

class Relink extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'igvs.relink';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    public $iteration_stamp = '';

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        $this->iteration_stamp = time();

        $this->info("I" . $this->iteration_stamp);
        $this->output->writeLn('relink content...');

        $this->log("\r\n--------------------");

        $backup = realpath(Config::get('igvs.courses::modulesContent.path'));
        $storage = realpath(Config::get('igvs.courses::modulesContent.path') . '-storage');
        $content = realpath(Config::get('igvs.courses::modulesContent.path') . '-new');

        $courses = Course::withTrashed()
            ->orderBy('id', 'asc')
            ->get();

        $counter_courses = 0;
        $count_courses = count($courses);

        $this->info('Count courses:' . count($courses));
        $this->log('Count courses:' . count($courses));

        foreach ($courses as $course) {
            $counter_courses++;
            $counter_files_added = 0;
            $counter_files_skipped = 0;


            $this->info("{$counter_courses}/{$count_courses} Course ID " . $course->id);
            $this->log("{$counter_courses}/{$count_courses} Course ID " . $course->id);

            $deleted = $course->deleted_at ? ', deleted_at: '.$course->deleted_at : '';
            $this->info("{$course->code} (id: {$course->id}{$deleted})");
            $this->log("{$course->code} (id: {$course->id}{$deleted})");

            if (!is_dir("{$backup}/{$course->id}")) {
                $this->output->writeLn('Пропущено, папка отсутствует');
                $this->log('Пропущено, папка отсутствует');
                continue;
            }

            $start = microtime(true);

            foreach(new \RecursiveIteratorIterator (new RecursiveDirectoryIterator ("{$backup}/{$course->id}")) as $file)  {

                try {

                    // создаем папки
                    $dir = str_replace($backup, $content, $file->getPath());
                    if (!is_dir($dir)) {
                        mkdir($dir, 0777, true);
                    }

                    if ($file->isDir()) {
                        continue;
                    }

                    $src = ModuleContent::saveInStorage($file->getRealPath(), true);

                    if (!is_file(str_replace($backup, $content, $file->getRealPath()))) {
                        link($src, str_replace($backup, $content, $file->getRealPath()));
                        $counter_files_added++;
                    }
                    else {
                        $counter_files_skipped++;
//                        $this->output->writeLn('Файл пропущен ' . $file->getRealPath());
                    }

                }
                catch (\Exception $e) {

                    dd([
                        'message' => $e->getMessage(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                        'src' => $file->getRealPath(),
                        'dist' => str_replace($backup, $content, $file->getRealPath())
                    ]);

                    $this->log(print_r([
                        'message' => $e->getMessage(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                        'src' => $file->getRealPath(),
                        'dist' => str_replace($backup, $content, $file->getRealPath())
                    ], true));

                }
            }


            $time = microtime(true) - $start;
            $this->output->writeLn("Файлов: добавлено {$counter_files_added} пропущено {$counter_files_skipped}");
            $this->output->writeLn(sprintf('Время обработки %.4F сек.', $time));

            $this->log("Файлов: добавлено {$counter_files_added} пропущено {$counter_files_skipped}");
            $this->log(sprintf('Время обработки %.4F сек.', $time));

        }
    }

    private function log($text)
    {
        $log_dir_path = __DIR__ . '/../../../../storage/app/igvs/logs/relink';

        if (!is_dir($log_dir_path))
            mkdir($log_dir_path, 777, true);

        $log_name = $log_dir_path . "/{$this->iteration_stamp}.log";

        $text = '[' . $this->iteration_stamp . '][' . date('Y-m-d H:i:s') .  ']' . ' ' . $text . "\r\n";

        file_put_contents($log_name, $text, FILE_APPEND);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}