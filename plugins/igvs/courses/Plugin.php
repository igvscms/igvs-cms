<?php namespace Igvs\Courses;

use Backend;
use BackendAuth;
use Igvs\Courses\Models\Course;
use System\Classes\PluginBase;
use Backend\Classes\Controller;
use Lang;
use Event;
use Route;
use Cookie;

/**
 * courses Plugin Information File
 */
class Plugin extends PluginBase
{

    public function boot()
    {
        // Add constraining to Course's relation 'jobs'
        Course::extend(function ($model) {
            $model->with['jobs'] = function ($query) {
                $user = BackendAuth::getUser();

                if ($user) {
                    $query->where('user_id', $user->id)
                        ->whereIn('status', ['progress', 'waiting']);
                } else {
                    $query->where('user_id', -1);
                }
            };
        });

        Event::listen('auth.login', function() {
            \Session::forget('igvs.courser.user_instance_id');
            \Session::forget('igvs.user_instance_type');
            \Session::forget('igvs.user_instance_version');
        });

        Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
            \Session::forget('locale');
            \Session::forget('igvs.courses.is_expertise_system');

            Backend\Models\Preference::setAppLocale();

            $controller->addCss('/plugins/igvs/courses/assets/css/plugin.css?a=1');
            $controller->addJs('/plugins/igvs/courses/assets/js/courses.js');

            $user = BackendAuth::getUser();

            $backendUriExpertise = \Config::get('cms.backendUriExpertise', 'backend');
            $cmsExpertiseThemeEnable = \Config::get('cms.expertiseThemeEnable', false);
            $cmsExpertiseTheme = \Config::get('cms.expertiseTheme', 'default');

            $is_expertise = strpos(\Request::getRequestUri(), $backendUriExpertise) !== false;
            $is_bag_method = $user && $user->hasPermission('igvs.courses.bag_method');
            $is_pilot_subsystem = 0;

            $interface_theme = \Session::get('igvs.courses.interface_theme', 'default');
            if ($interface_theme == '') {
                \Session::put('igvs.courses.interface_theme', 'default');
                $interface_theme = 'default';
            }

            if ($cmsExpertiseThemeEnable)
                $interface_theme = $cmsExpertiseTheme;

            if ($is_bag_method || $is_expertise || $cmsExpertiseThemeEnable) {
                $controller->addCss('/plugins/igvs/courses/assets/css/bug-tracker.css');
                $controller->addCss("/plugins/igvs/courses/assets/css/expertise-common.css");
                $controller->addCss("/plugins/igvs/courses/assets/css/expertise-{$interface_theme}.css");
                $is_pilot_subsystem = 1;
            }

            \Session::put('igvs.courses.is_pilot_subsystem', $is_pilot_subsystem);

            if ($is_expertise) {
                \Session::put('igvs.courses.is_expertise_system', 1);
            }

            if ($cmsExpertiseThemeEnable) {
                \Session::put('igvs.courser.full_name', $user ? $user->getFullNameAttribute() : '');
                \Session::put('igvs.courser.work_company', \Config::get('cms.expertiseThemePoo', ''));
                \Session::put('igvs.courser.work_company2', \Config::get('cms.expertiseThemePoo2', ''));
                \Session::put('igvs.courser.logo_name', \Config::get('cms.expertiseThemeLogoName', ''));
            }

            if (in_array(get_class($controller), [
                    'Igvs\Courses\Controllers\Courses',
                    'Igvs\Courses\Controllers\Categories',
                    'Backend\Controllers\Preferences'
                ])
                && !in_array($action, ['update'])) {

                $controller->layout = 'backend_custom_layout';
            }

            $is_pilot_user = $user ? BackendAuth::getUser()->hasPermission('igvs.courses.pilot_user') : false;
            if (((get_class($controller) == 'Backend\Controllers\Users'
                && $action == 'myaccount')
                || (get_class($controller) == 'System\Controllers\Settings'))
                && $is_pilot_user) {
                header('Location: ' . \Backend::url('/'));
                exit();
            }

            if (!$is_pilot_user) {
                \Cookie::queue('pilot_domain', '', 0);
            }

            // save module html-markup ID
            $module_html_mark_up_id = \Igvs\Courses\Models\Module::where('path', 'html-markup')->first();
            \Cache::forever('igvs.module.html_markup_id', $module_html_mark_up_id ? $module_html_mark_up_id->id : null);

            if ($user) {
                //clear cache task query
                \Cache::forget('task.get_project_options.projects.' . $user->id);
                \Cache::forget('task.get_project_options.roles.' . $user->id);
                \Cache::forget('task.get_project_options.need_project.' . $user->id);
                \Cache::forget('task.get_project_options.result.' . $user->id);

                //clear cache get_project_options
                $task_projects = \Cache::get('task.get_status_options.projects.' . $user->id, []);
                foreach ($task_projects as $task_project_id) {
                    \Cache::forget("task.get_project_options.project.{$user->id}_{$task_project_id}");
                }

                //clear cache get_status_options
                $statuses_cache_code = \Cache::get('task.get_status_options.statuses_cache_code.' . $user->id, []);
                foreach ($statuses_cache_code as $status_cache_code) {
                    \Cache::forget("task.get_status_options.statuses.{$user->id}_{$status_cache_code}");
                }

                //clear cache get_topic_options
                $task_projects = \Cache::get('task.get_topic_options.courses.' . $user->id, []);
                foreach ($task_projects as $task_course_id) {
                    \Cache::forget("task.get_topic_options.{$user->id}_{$task_course_id}");
                }

                //clear cache get_act_options
                $task_projects = \Cache::get('task.get_act_options.topics.' . $user->id, []);
                foreach ($task_projects as $task_topic_id) {
                    \Cache::forget("task.get_act_options.{$user->id}_{$task_topic_id}");
                }
            }
        });
    }

    public function register()
    {
        \App::singleton('backend.auth', function () {
            return \Igvs\Courses\Classes\AuthManager::instance();
        });

        $commands = [
            'igvs.createcontentlistpdf' => 'Igvs\Courses\Console\CreateContentListPdf',
            'igvs.refreshmodules' => 'Igvs\Courses\Console\RefreshModules',
            'igvs.createscorm' => 'Igvs\Courses\Console\CreateScorm',
            'igvs.importtextbook' => 'Igvs\Courses\Console\ImportTextBook',
            'igvs.importebookonly' => 'Igvs\Courses\Console\ImportEbookOnly',
            'igvs.bulletscalesize' => 'Igvs\Courses\Console\BulletScaleSize',
            'igvs.importcourse' => 'Igvs\Courses\Console\ImportCourse',
            'igvs.importcoursefromtar' => 'Igvs\Courses\Console\ImportCourseFromTar',
            'igvs.importcose' => 'Igvs\Courses\Console\ImportCose',
            'igvs.fixstepbystep' => 'Igvs\Courses\Console\FixStepbystep',
            'igvs.fixfiles' => 'Igvs\Courses\Console\FixFiles',
            'igvs.fixjson' => 'Igvs\Courses\Console\fixjson',
            'igvs.fixjsontimer' => 'Igvs\Courses\Console\fixjsonTimer',
            'igvs.fixcommon' => 'Igvs\Courses\Console\FixCommon',
            'igvs.fixmodulecode' => 'Igvs\Courses\Console\FixModuleCode',
            'igvs.updateversion' => 'Igvs\Courses\Console\UpdateCoursesVersion',
            'igvs.exportcourses' => 'Igvs\Courses\Console\ExportCourses',
            'igvs.publicstate' => 'Igvs\Courses\Console\PublicState',
            'igvs.resort' => 'Igvs\Courses\Console\Resort',
            'igvs.chatserver' => 'Igvs\Courses\Console\ChatServer',
            'igvs.pushserver' => 'Igvs\Courses\Console\PushServer',
            'igvs.relink' => 'Igvs\Courses\Console\Relink',
            'igvs.relinkcard' => 'Igvs\Courses\Console\RelinkCard',
            'igvs.storagestats' => 'Igvs\Courses\Console\StorageStats',
            'igvs.clearcourses' => 'Igvs\Courses\Console\ClearCourses',
            'igvs.ebookfixpath' => 'Igvs\Courses\Console\EbookFixPath',
            'igvs.renamerusnamefiles' => 'Igvs\Courses\Console\RenameRusNameFiles',
            'igvs.searchmoduletypediff' => 'Igvs\Courses\Console\SearchModuleTypeDiff',
            'igvs.createtemplink' => 'Igvs\Courses\Console\CreateTempLink',
            'igvs.fixmultiplicationsymbol' => 'Igvs\Courses\Console\FixMultiplicationSymbol',
        ];
        array_walk($commands, function($class, $key) {
            $this->registerConsoleCommand($key, $class);
        });
    }

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'i-GVS',
            'description' => 'i-GVS CMS',
            'author'      => 'igvs',
            'icon'        => 'icon-connectdevelop'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Igvs\Courses\Components\Course' => 'Course',
            'Igvs\Courses\Components\Catalogue' => 'Catalogue',
            'Igvs\Courses\Components\PilotAuth' => 'PilotAuth',
//            'Igvs\Courses\Components\ApiModules' => 'ApiModules'
        ];
    }

    public function registerReportWidgets()
    {
        return [
            'Igvs\Courses\Widgets\HardinfoWidget'=>[
                'label'   => 'Hardinfo Widget',
                'context' => 'dashboard'
            ],
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        $tabs = [
            'Courses' => [
                'courses', 'templates',  'builds', 'background_tasks',
                'export', 'import_rubricator','export_scorm','create',
                'duplicate', 'pilot_user','roles','types','expertise',
                'bag_method', 'pilot_export','show_tab_update_versions',
                'show_tab_rubricator_history', 'show_tab_vocabulary',
                'expertise_dict', 'temp_link', 'is_base_course',
                'clear_import_from_intro', 'can_edit_base_module',
                'exportlist', 'build', 'publicationstates_controll',
                //'link_to_theory_module',
                'is_inclusive', 'show_btn_application_for_publication',
                'show_btn_preview', 'show_btn_temp_link', 'enable_crypt_preview_id',
                'publication_state',
            ],
            'Courses (Modules)' => [
                'content_data', 'content_json_history', 'content_log',
                'content_builds', 'amounts_report',
            ]
        ];

        $result = [];
        foreach($tabs as $tab => $premissions) {
            $tab = trans('igvs.courses::plugin.tabs.' . $tab);
            foreach($premissions as $premission) {
                $result['igvs.courses.' . $premission] = [
                    'tab' => $tab,
                    'label' => trans('igvs.courses::plugin.permission.' . $premission),
                ];
            }
        }
        return $result;
    }

    public function getPluginLabel()
    {
        $backendUriExpertise = \Config::get('cms.backendUriExpertise', 'backend');

        $user = \BackendAuth::getUser();

        if ($user
            && $user->hasPermission('igvs.courses.pilot_user')
            && $user->hasPermission('igvs.courses.bag_method')) {

            if (strpos(\Request::getRequestUri(), $backendUriExpertise) !== false) {
                return trans('igvs.courses::plugin.plugin_label_expertise');
            }

            return trans('igvs.courses::plugin.plugin_label_bug_method');
        }

        return trans('igvs.courses::plugin.plugin_label');
    }

    public function showHelpMenu()
    {
        $user = \BackendAuth::getUser();

        return $user
            && $user->hasPermission('igvs.courses.pilot_user')
            && $user->hasPermission('igvs.courses.bag_method');
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        $plugin_label = $this->getPluginLabel();

        $return = [
            'courses' => [
                'label' => $plugin_label,
                'url' => Backend::url('igvs/courses'),
                'icon' => 'icon-connectdevelop',
                'permissions' => ['igvs.courses.*'],
                'order' => 500,
                'sideMenu' => [
                    'courses' => [
                        'label' => Lang::get('igvs.courses::lang.course.txt_field.courses'),
                        'icon' => 'icon-files-o',
                        'url' => Backend::url('igvs/courses'),
                        'permissions' => ['igvs.courses.courses']
                    ],
                    'templates' => [
                        'label' => Lang::get('igvs.courses::lang.module.modules_templates'),
                        'icon' => 'icon-cubes',
                        'url' => Backend::url('igvs/courses/templates'),
                        'permissions' => ['igvs.courses.templates']
                    ],
                    'builds' => [
                        'label' => Lang::get('igvs.courses::lang.build.builds'),
                        'icon' => 'icon-tags',
                        'url' => Backend::url('igvs/courses/builds'),
                        'permissions' => ['igvs.courses.builds']
                    ],
                    'publicationstates' => [
                        'label' => Lang::get('igvs.courses::lang.publicationstates'),
                        'icon' => 'icon-cloud-upload',
                        'url' => Backend::url('igvs/courses/publicationstates'),
                        'permissions' => ['igvs.courses.publicationstates']
                    ],
                    'roles' => [
                        'label' => Lang::get('igvs.courses::lang.role.roles'),
                        'icon' => 'icon-users',
                        'url' => Backend::url('igvs/courses/roles'),
                        'permissions' => ['igvs.courses.roles']
                    ],
                    'statuses' => [
                        'label' => Lang::get('igvs.courses::status.label'),
                        'icon' => 'icon-leaf',
                        'url' => Backend::url('igvs/courses/statuses'),
                        'permissions' => ['igvs.courses.statuses']
                    ],
                    'expertise_dict' => [
                        'label' => Lang::get('igvs.courses::lang.expertise.label'),
                        'icon' => 'icon-leaf',
                        'url' => Backend::url('igvs/courses/expertise'),
                        'permissions' => ['igvs.courses.statuses']
                    ],
                    'course_types' => [
                        'label' => Lang::get('igvs.courses::lang.course.course_types'),
                        'icon' => 'icon-list-alt',
                        'url' => Backend::url('igvs/courses/types'),
                        'permissions' => ['igvs.courses.types']
                    ],
                    'background_tasks' => [
                        'label' => Lang::get('igvs.courses::lang.backgroundtasks.self'),
                        'icon' => 'icon-tasks',
                        'url' => Backend::url('igvs/courses/backgroundtasks'),
                        'permissions' => ['igvs.courses.background_tasks']
                    ],
                    'contents' => [
                        'label' => trans('igvs.courses::content.name'),
                        'icon' => 'icon-trash-o',
                        'url' => Backend::url('igvs/courses/contents'),
                        'permissions' => ['igvs.courses.contents']
                    ],
                    'temp_link' => [
                        'label' => trans('igvs.courses::lang.temp_link.self'),
                        'icon' => 'oc-icon-link',
                        'url' => Backend::url('igvs/courses/templink'),
                        'permissions' => ['igvs.courses.temp_link']
                    ],
                ]
            ],
        ];

        $help_menu = [
            'help' => [
                'label' => Lang::get('igvs.courses::lang.help'),
                'url' => '#',
                'icon' => 'icon-connectdevelop',
                'order' => 600,
            ]
        ];

        if ($this->showHelpMenu()) {
            $return = array_merge($return, $help_menu);
        }

        return $return;
    }

    public function registerFormWidgets()
    {
        return ['Igvs\Courses\FormWidgets\RichEditor' => 'richeditor'];
    }
}
