<?php namespace Igvs\Courses\Scopes;

use Illuminate\Database\Eloquent\ScopeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Igvs\Courses\Models\UserPilot;
use Igvs\Courses\Models\Member;
use Backend\Models\User;
use Backend\Facades\BackendAuth;

class VocabularyListOrder implements ScopeInterface
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     *
     *
     */

    public function apply(Builder $builder, Model $model)
    {
        if (!\App::runningInBackend())
            return;

        $builder->orderBy('term_clean');
    }

    public function remove(Builder $builder, Model $model)
    {
    }
}