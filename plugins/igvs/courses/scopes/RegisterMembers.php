<?php namespace Igvs\Courses\Scopes;

use Illuminate\Database\Eloquent\ScopeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Igvs\Courses\Models\UserPilot;
use Igvs\Courses\Models\Member;
use Backend\Models\User;
use Backend\Facades\BackendAuth;

class RegisterMembers implements ScopeInterface
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     *
     *
     */

    public function apply(Builder $builder, Model $model)
    {
        if (!\App::runningInBackend())
            return;

        $user = BackendAuth::getUser();

        if (!$user || !$user->hasPermission('igvs.courses.pilot_user'))
            return;

        //ищем пользователей того же экземпляра приложения
        $id = $model->getTable() . '.' . ($model instanceOf Member ? 'user_id' : 'id');

        //проверяем, хочет ли пользователь увидеть только пользователей из своего ПОО (галочка в форме выбора участников курса)
        $poo_filter_enable = is_array(post('Member')) && isset(post('Member')['poo_filter']) ? (int)post('Member')['poo_filter'] : 0;

        $up = UserPilot::getUserPilotByUserId($user->id);
        $allow_users_id = UserPilot::getUserPilotByInstanceClusterPoo($up->instance_id, $up->cluster_id, $up->poo_id, $poo_filter_enable);

        // ограничение, на отображение пользователей в группах
        $builder->where(function ($query) use ($allow_users_id, $id) {
            $query->whereIn($id, $allow_users_id);
        });
    }

    public function remove(Builder $builder, Model $model)
    {
    }
}
