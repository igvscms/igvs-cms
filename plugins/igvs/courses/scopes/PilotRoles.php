<?php namespace Igvs\Courses\Scopes;

use Illuminate\Database\Eloquent\ScopeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Backend\Facades\BackendAuth;

class PilotRoles implements ScopeInterface
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     *
     *
     */

    public function apply(Builder $builder, Model $model)
    {
        if (!\App::runningInBackend())
            return;

        $user = BackendAuth::getUser();

        if (!$user || !$user->hasPermission('igvs.courses.pilot_user'))
            return;

        $access_name = $user->hasPermission('igvs.courses.bag_method') ? 'show_in_elearning' : 'show_in_pilot';

        // ограничение, на отображение ролей
        $builder->where(function ($query) use($access_name) {
            $query->where($access_name, 1);
        });
    }

    public function remove(Builder $builder, Model $model)
    {
    }
}