var course_name = window.location.pathname.split('/')[2];

var loadURL = function(url) {
    var oRequest = new XMLHttpRequest();
    oRequest.open('GET', url+'&nocache='+Math.random(), false);
    oRequest.send(null);

    return oRequest.responseText;
};

var menu_path = window.location.protocol + '//' + window.location.host + window.location.pathname + '?act=list_menu';

var menu_file = JSON.parse(loadURL(menu_path));

var logo_container = document.getElementsByClassName('logo-block-logotype')[0];

var menu_data_def = menu_file.nav.eapp;
var menu_data = [];

for (var i_t = 0; i_t < menu_data_def.length; i_t++) {
    var t = menu_data_def[i_t];
    if (t.type == 'topic') {
        var child_t = [];
        for (var i = 0; i < menu_data_def.length; i++) {
            var i_o = menu_data_def[i];
            if (i_o.type == 'group' && i_o.parent == t.id) {
                var child = [];
                for (var ii = 0; ii < menu_data_def.length; ii++) {
                    var ii_o = menu_data_def[ii];
                    if (i_o.id == ii_o.parent) {
                        child.push(ii_o);
                    }
                }

                i_o.child = child;
                child_t.push(i_o);
            }
        }
        t.child = child_t;
        menu_data.push(t)
    }
}

function $G(name) {
    return document.getElementById(name);
}

//var container = document.getElementsByClassName('dropdown-group')[0];
//container.innerHTML = '';


var container = document.createElement('div');
container.className = 'dropdown-group';

document.getElementById('nav').appendChild(container);


while (typeof(document.getElementsByClassName('dropdown-group')[1]) != 'undefined') {
    var elem = document.getElementsByClassName('dropdown-group')[1];
    elem.parentNode.removeChild(elem);
}
if (typeof(document.getElementsByClassName('dropdwon-group')[0]) != 'undefined') {
    var elem = document.getElementsByClassName('dropdwon-group')[0];
    elem.parentNode.removeChild(elem);
}

var h_id = 0;
var path = window.location.protocol + '//' + window.location.host + window.location.pathname + '/' + window.location.search;
for (var i = 0; i < menu_data.length; i++) {
    var topic_child_menu_text = '';
    var el_t = menu_data[i];

    for (var el_t_i=0; el_t_i < el_t.child.length; el_t_i++) {
        var child_menu_text = '';
        var el = el_t.child[el_t_i];

        for (var el_c_i=0; el_c_i < el.child.length; el_c_i++) {
            el_c = el.child[el_c_i];

            var img = '',
                module_name_prefix = '';
            switch (el_c.class) {
                case 'module practice':
                    img = 'check';
                    break;
                case 'module control':
                    img = 'test';
                    break;
                case 'module theory':
                    img = 'ask';
                    break;
            }


            var item_name = el_c.textTitle,
                item_name_practice = el_c.textTitle;

            if (el_c.behavior == 'practice' && el_c.practice_title_manage === '1') {
                item_name += ' ' + el_c.practice_index_title;
                item_name_practice += ' ' + el_c.practice_assessment_title;
            } else if (el_c.behavior == 'practice') {
                item_name_practice += ' (Assessment)';
            }

            if (el_c.behavior != 'practice' || (el_c.behavior == 'practice' && el_c.switch_practice_mode != '2')) {
                child_menu_text += '<div data-navigation-item data-navigation-href="' + path + '?lesson=' + el_c.id + '&act=index" data-hash-nav="' + el_c.id + '" class="dropdown-block__item dropdown-block__item-ask" data-navigation-id="' + el_c.code + '" data-behavior="' + el_c.behavior + '" data-assessment="0">' +
                    '<svg width=25 height=25 class="dropdown-block__svg">' +
                    '<use xlink:href="' + window.location.href.replace(window.location.hash, "") + '#' + img + '">' +
                    '</svg>' +
                    '<p>' + item_name + '</p>' +
                    '</div>';
            }

            if (el_c.behavior == 'practice' && el_c.switch_practice_mode != '1') {
                child_menu_text += '<div data-navigation-item data-navigation-href="' + path + '?lesson=' + el_c.id + '&act=assessment" data-hash-nav="' + el_c.id + 'a" class="dropdown-block__item dropdown-block__item-ask" data-behavior="' + el_c.behavior + '" data-assessment="1">' +
                    '<svg width=25 height=25 class="dropdown-block__svg">' +
                    '<use xlink:href="' + window.location.href.replace(window.location.hash, "") + '#test">' +
                    '</svg>' +
                    '<p>' + item_name_practice + '</p>' +
                    '</div>';
            }
            h_id++;
        }

        var group_c = '<div data-navigation-group class="dropdown-block" data-group-id="' + el_t_i + '">' +
            '<p class="dropdown-block__name">' + el.textTitle + '</p>' +
            '<svg width=16 height=16 class="dropdown-block__arrow">' +
            '<use xlink:href="' + window.location.href.replace(window.location.hash, "") + '#arrow">' +
            '</svg>' +
            '<div data-navigation-groupset class="dropdown-block__content">' +
            child_menu_text +
            '</div></div>';

        topic_child_menu_text  += group_c;

    }

    topic_child_menu_text  = '<div class="dropdown-group__separator">' + el_t.textTitle + '</div>' + topic_child_menu_text;
    container.innerHTML += topic_child_menu_text;
}