$(document).on("click", "#btn-export-to-scorm", function() {

    $("#export-form select").prop('disabled', true)

    $(this).hide();
    $("#btn-preparation-scorm").css('display', 'inline-block');

    var $form = $("#export-form");

    $.request('onCreateScorm', {
        data: {
            course_id: $("[name=course_id]", $form).val(),
            build_id: $("[name=build_id]", $form).val(),
            theme: $("[name=theme]", $form).val()
        },
        success: function(data) {
            $("#btn-download-scorm").attr('href', data.result);
            $("#btn-download-scorm").show();
            $("#btn-preparation-scorm").css('display', 'none');
        }
    });

});

$(document).on("click", "#btn-export-to-pilot", function() {

    $("#export-form select").prop('disabled', true);
    $("#to-gvs-modal-content-cancel-popup").prop('disabled', true);
    $("#success").hide();
    $("#error").hide();
    $("#error_licence").hide();


    $("#btn-preparation-gvs").css('display', 'inline-block');
    $("#btn-export-to-pilot").hide();
    var $form = $("#export-form");

    $('#current_topic_code').val($("[name=folder_name]", $form).val());


    var licence_pci = $("[name=licence]", $form).val();
    if (licence_pci.indexOf('/') == -1) {
        licence_pci = [0, 0, 0];
    } else {
        licence_pci = licence_pci.split('/');
    }

    if (typeof(licence_pci[2]) == 'undefined') {
        alert('неверный код лицензии. обратитесь к разработчику.')
        return;
    }

    $.request('onPilotExport', {
        data: {
            course_id: $("[name=course_id]", $form).val(),
            dont_use_licence: $("[name=dont_use_licence]", $form).val(),
            system: $("[name=system]", $form).val(),
            competence: $("[name=competence]", $form).val(),
            licence_id: licence_pci[0],
            pilot_course_id: licence_pci[1],
            external_course_id: licence_pci[2]
        },
        success : function(data) {
//                console.log(data);
            $("#to-gvs-modal-content-cancel-popup").prop('disabled', false);
            if (typeof(data.result) != 'undefined') {
                if (data.result == 'true') {
                    //корректный ответ. ошибок нет.
                    $(this).hide();
                    $("#btn-export-to-pilot").hide();
                    $("#btn-preparation-gvs").hide();
                    $("#to-gvs-modal-content-cancel-popup").hide();
                    $("#structure_diff").hide();
                    $("#export-form").hide();
                    $("#to-gvs-modal-content-close-popup").show();
                    $("#success").show();
                    return;
                } else if (data.result == 'false') {
                    //корректный ответ. есть ошибки.
                    $("#export-form select").prop('disabled', false);
                    $("#btn-export-to-pilot").show();
                    $("#btn-preparation-gvs").hide();
                    $("#error_licence").show();
                    return;
                }
            }

            //некорректный ответ.
            $("#error").show();
            $("#btn-preparation-gvs").hide();
            $("#to-gvs-modal-content-cancel-popup").hide();
            $("#to-gvs-modal-content-close-popup").show();
        }
    });
});

$(document).on("click", "#btn-public-state", function() {

    $("#export-form select").prop('disabled', true);
    $("#to-gvs-modal-content-cancel-popup").prop('disabled', true);
    $("#success").hide();
    $("#error").hide();
    $("#error_licence").hide();


    $("#btn-preparation-gvs").css('display', 'inline-block');
    $("#btn-public-state").hide();
    var $form = $("#public-state-form");


    $.request('onPublicPublicationState', {
        data: {
            course_id: $("[name=course_id]", $form).val(),
            build_id: $("[name=build_id]", $form).val(),
        },
        success : function(data) {
//                console.log(data);
            $("#to-gvs-modal-content-cancel-popup").prop('disabled', false);
            if (typeof(data.result) != 'undefined') {
                if (data.result == 'true') {
                    //корректный ответ. ошибок нет.
                    $(this).hide();
                    $("#btn-public-state").hide();
                    $("#btn-preparation-gvs").hide();
                    $("#to-gvs-modal-content-cancel-popup").hide();
                    $("#to-gvs-modal-content").hide();
                    $("#to-gvs-modal-content-close-popup").show();
                    $("#success").show();
                    return;
                } else if (data.result == 'false') {
                    //корректный ответ. есть ошибки.
                    $("#export-form select").prop('disabled', false);
                    $("#btn-public-state").show();
                    $("#btn-preparation-gvs").hide();
                    $("#error_licence").show();
                    return;
                }
            }
            $("#btn-public-state").show();
            //некорректный ответ.
            $("#error").show();
            $("#btn-preparation-gvs").hide();
            $("#to-gvs-modal-content-cancel-popup").hide();
            $("#to-gvs-modal-content-close-popup").show();
        }
    });
});

// подгрузка отличий существующей выгрузки и планируемой
$(document).on("change", "#form-build", function() {
    var $form = $("#public-state-form");

    $('#course-structure-diff-loading').show();

    $("#btn-public-state").prop('disabled', true);

    $.request('onGetCourseDiff', {
        data: {
            course_id: $("[name=course_id]", $form).val(),
            build_id: $("[name=build_id]", $form).val(),
        },
        update: {'course_structure_diff': '#course-structure-diff'},
        complete: function () {
            $("#btn-public-state").prop('disabled', false);
            // $("#course-structure-diff").hide()
            //                             .show();
            $('#course-structure-diff-loading').hide();
        },
        // success: function (result) {
        //     console.log(result);
        //     if (typeof(result.course_structure_diff) !== 'undefined') {
        //         // $("#course-structure-diff").html(result.course_structure_diff);
        //         document.getElementById('course-structure-diff').innerHTML = result.course_structure_diff;
        //         console.log('123');
        //
        //     }
        // }
    });
});

$(document).on("click", "#btn-add-operator-in-course", function() {

    $("#add-operator-in-course select").prop('disabled', true);
    $("#to-gvs-modal-content-cancel-popup").prop('disabled', true);
    $("#success").hide();
    $("#error").hide();

    $("#btn-preparation-gvs").css('display', 'inline-block');
    $("#btn-add-operator-in-course").hide();
    var $form = $("#add-operator-in-course");

    $('#current_topic_code').val($("[name=folder_name]", $form).val());

    // $.request('onElearningAddOperatorInCourse', {
    //     files: true,
    //     data: {
    //         course_id: $("[name=course_id]", $form).val(),
    //         user_id: $("[name=user_id]", $form).val()
    //     },
    //     success : function(data) {
    //         $("#to-gvs-modal-content-cancel-popup").prop('disabled', false);
    //         if (typeof(data.result) != 'undefined') {
    //             if (data.result == 'true') {
    //                 //корректный ответ. ошибок нет.
    //                 $(this).hide();
    //                 $("#btn-add-operator-in-course").hide();
    //                 $("#btn-preparation-gvs").hide();
    //                 $("#to-gvs-modal-content-cancel-popup").hide();
    //                 $("#structure_diff").hide();
    //                 $("#add-operator-in-course").hide();
    //                 $("#to-gvs-modal-content-close-popup").show();
    //                 $("#success").show();
    //                 return;
    //             } else if (data.result == 'false') {
    //                 //корректный ответ. есть ошибки.
    //                 $("#add-operator-in-course select").prop('disabled', false);
    //                 $("#btn-add-operator-in-course").show();
    //                 $("#btn-preparation-gvs").hide();
    //                 $("#error_licence").show();
    //                 return;
    //             }
    //         }
    //
    //         //некорректный ответ.
    //         $("#error").show();
    //         $("#btn-preparation-gvs").hide();
    //         $("#to-gvs-modal-content-cancel-popup").hide();
    //         $("#to-gvs-modal-content-close-popup").show();
    //     }
    // });
});

$(document).on("change", "#licence", function() {
    $('#btn-export-to-pilot').attr('disabled', ($(this).val().length ? false : true));
});

/* Temp Link popoup */
$(document).on('click', '#create-temp-link-form .control-balloon-selector li', function(){
    var value = $(this).data('value');
    $(this).parent().children('li').removeClass('active');
    $(this).addClass('active');
    $(this).closest('.control-balloon-selector').children('input').val(value);
});

$(document).on('click', '#create-temp-link-form .secondary-tabs .nav-tabs li', function(){
    var labels = $(this).closest('.secondary-tabs').children('.nav-tabs').children('li');
    var tabs = $(this).closest('.secondary-tabs').children('.tab-content').children('.tab-pane');
    var value = labels.index(this);

    tabs.removeClass('active');
    $(tabs[value]).addClass('active');
});

$(document).on("click", "#btn-create-temp-link", function() {
    var $form = $("#create-temp-link-form");

    var consider_build = parseInt($("[name=consider_build]", $form).val());
    var consider_topic = parseInt($("[name=consider_topic]", $form).val());

    $.request('onTempLinkCreate', {
        data: {
            course_id: $("#toolbar-course-id").val(),
            build_id: consider_build ? $("#current-build-id").val() : 0,
            topic_id: consider_topic ? $("#current-category-id").val() : 0,
            date: $("[name=date]", $form).val(),
            time: $("[name=time]", $form).val()
        },
        success: function (data_in) {
            var data = {'status': 'false'};

            if (typeof(data_in.result) !== 'undefined' && typeof(data_in.result) === 'string') {
                try {
                    data = JSON.parse(data_in.result);
                } catch (e) {
                    $.oc.alert('Ошибка создания временной ссылки. Код: 001');
                }
            } else {
                $.oc.alert('Ошибка создания временной ссылки. Код: 002');
                return;
            }

            var $form = $(".create-temp-link-form");
            var link = window.location.origin + '/preview/tlink/' + data.hash;

            $("[name=link]", $form).val(link)
            $("[name=link]", $form).attr('value', link)

            buildTempLinkHistory(data.links_history);
        }
    });
});

$(document).on("click", ".temp-link-delete", function() {
    var id = $(this).closest('tr').data('id');

    $.request('onTempLinkDelete', {
        data: {
            id: id,
            course_id: $("#toolbar-course-id").val()
        },
        success: function (data_in) {
            var data = {'status': 'false'};
            if (typeof(data_in.result) !== 'undefined' && typeof(data_in.result) === 'string') {
                try {
                    data = JSON.parse(data_in.result);
                } catch (e) {
                    $.oc.alert('Ошибка создания временной ссылки. Код: 001');
                }
            } else {
                $.oc.alert('Ошибка создания временной ссылки. Код: 002');
                return;
            }

            buildTempLinkHistory(data.links_history);
        }
    });
});

$(document).on("click", ".temp-link-create-first", function() {
    $(this)
        .closest('.popover-body')
        .find('.nav-tabs')
        .children('li')
        .first()
        .children('a')
        .first()
        .click();
});

$(document).on("click", ".temp-link-fast-date", function() {
    var type = $(this).data('value');
    var date = new Date();

    switch (type) {
        case 'day':
            date.setDate(date.getDate() + 1);
            break;
        case 'week':
            date.setDate(date.getDate() + 7);
            break;
        case 'month':
            date.setMonth(date.getMonth() + 1);
            break;
        case 'year':
            date.setFullYear(date.getFullYear() + 1);
            break;
    }

    var date_year = date.getFullYear(),
        date_month = date.getMonth() + 1,
        date_day = date.getDate(),
        date_hour = date.getHours(),
        date_minuts = date.getMinutes();

    date_month = date_month < 10 ? '0' + date_month : date_month;

        date_day = date_day < 10 ? '0' + date_day : date_day;
        date_hour = date_hour < 10 ? '0' + date_hour : date_hour;
        date_minuts = date_minuts < 10 ? '0' + date_minuts : date_minuts;

    var $form = $("#create-temp-link-form");

    $("[name=date]", $form).val(date_year + '-' + date_month + '-' + date_day);
    $("[name=time]", $form).val(date_hour + ':' + date_minuts + ':00');
});

function buildTempLinkHistory(items) {
    //очищаем контейнер
    $('.temp_link_history_container').html('');

    var html = '';

    $.each(items, function(i, val) {
        var text_array = [];
        text_array.push(val.topic);
        text_array.push(val.build);
        text_array.push(val.expired);

        var link = location.origin + '/preview/tlink/' + val.hash;

        var row = '<td><a href="' + link + '" target="_blank">' + text_array.join('</a></td><td><a href="' + link + '" target="_blank">') + '</a></td><td><span class="temp-link-delete">Удалить</span></td>';

        html += '<tr data-id="' + val.id + '" class="temp-link-item">' + row + '</tr>';
    });

    var lang = {
        'create_first_link' : $('#lang-temp-link-create_first_link').text(),
        'it_easy' : $('#lang-temp-link-it_easy').text(),
        'topic' : $('#lang-temp-link-topic').text(),
        'build' : $('#lang-temp-link-build').text(),
        'expired_at' : $('#lang-temp-link-expired_at').text(),
    };

    if (!items.length) {
        html = '<div style="text-align: center; line-height: 40px;"><span class="temp-link-create-first">' + lang.create_first_link + '</span><br>' + lang.it_easy + '</div>';
    } else {
        html = '<table><thead><tr><th>' + lang.topic + '</th><th>' + lang.build + '</th><th>' + lang.expired_at + '</th><th></th></tr></thead><tbody>' + html + '</tbody></table>'
    }

    $('.temp_link_history_container').html(html);
}

$(document).ready(function(){
    $('#temp-link-popup').on('showing.oc.popover', function(e, popover){
        $('.create-temp-link-form')
            .children('.tab-content')
            .children('.tab-pane')
            .removeClass('active');
        $('.create-temp-link-form .temp_link_create_container')
            .parent()
            .addClass('active');

        popover.options.content = $('#popup-layout').html();
        window.aaa = popover;

        setTimeout(function() {
            $(popover.$container[0]).attr('id','create-temp-link-form');
        }, 100);
    });

    if ($('#temp-link-popup').length) {
        $.request('onTempLinkGetHistory', {
            data: {
                course_id: $("#toolbar-course-id").val(),
            },
            success: function (data_in) {
                var data = {'status': 'false'};
                if (typeof(data_in.result) !== 'undefined' && typeof(data_in.result) === 'string') {
                    try {
                        data = JSON.parse(data_in.result);
                    } catch (e) {
                        $.oc.alert('Ошибка создания временной ссылки. Код: 001');
                    }
                } else {
                    $.oc.alert('Ошибка создания временной ссылки. Код: 002');
                    return;
                }

                buildTempLinkHistory(data.links_history);
            }
        });
    }
});
/* Temp Link popoup END */

/* Prepare for export release cloud */
$(document).on('click', '#btn_preview', function() {
    window.open('<?= $beta_url ?>' + $('#current_topic_code').val().toLowerCase());
});

$(document).on("change", "[name=to_scorm], [name=to_cose], [name=to_igvs], [name=to_pilot], [name=to_zip], [name=to_maker]", function() {
    var objs = $('[name=to_scorm], [name=to_cose], [name=to_igvs], [name=to_pilot], [name=to_zip], [name=to_maker]'),
        visibility = false;

    for(var o_i=0; o_i < objs.length; o_i++) {
        var o = objs[o_i];
        if ($(o).prop('checked')) visibility = true;
    }

    $('[data-field-name=date], [data-field-name=postfix]').css({
        'display': !visibility ? 'none' : 'block'
    });

    $('[data-field-name=date-blocked], [data-field-name=postfix-blocked]').css({
        'display': visibility ? 'none' : 'block'
    });

    $('[name=date-value-block]').text($('#Form-field-date').val());
    $('[name=postfix-value-block]').text($('#Form-field-postfix').val());
});

$(document).on("change", "[name=to_beta]", function() {
    var visibility = $(this).prop('checked');

    $('[data-field-name=beta_folder]').css({
        'display': !visibility ? 'none' : 'block'
    });
    $('[data-field-name=beta_folder-blocked]').css({
        'display': visibility ? 'none' : 'block'
    });
    $('[name=beta_folder-value-block]').text($('[name=beta_folder]').val());
});


$(document).on("click", "#btn-continue", function() {
    $(this).hide();
    var $form = $("#export-form");

    $.request('onExportReleaseCloud', {
        data: {
            build_id: $("[name=build_id]", $form).val(),
            course_id: $("[name=course_id]", $form).val(),
            topic_id: $("[name=topic_id]", $form).val(),
            theme: $("[name=theme]", $form).val(),
            language: $("[name=language]", $form).val(),
            date: $("[name=date]", $form).val(),
            postfix: $("[name=postfix]", $form).val(),
            beta_folder: $("[name=beta_folder]", $form).val(),
            courses_id: $("[name=courses_id]", $form).val(),
            to_scorm: $("[name=to_scorm]", $form).prop('checked'),
            to_cose: $("[name=to_cose]", $form).prop('checked'),
            to_igvs: $("[name=to_igvs]", $form).prop('checked'),
            to_beta: $("[name=to_beta]", $form).prop('checked'),
            to_pilot: $("[name=to_pilot]", $form).prop('checked'),
            to_zip: $("[name=to_zip]", $form).prop('checked'),
            to_maker: $("[name=to_maker]", $form).prop('checked'),
            to_zip_local: $("[name=to_zip_local]", $form).prop('checked'),
            to_mobile: $("[name=to_mobile]", $form).prop('checked'),
            show_module_start_screen: $("[name=show_module_start_screen]", $form).prop('checked'),
            set_topic_lock: $("[name=set_topic_lock]", $form).prop('checked'),
            set_transfer_user_to_review: $("[name=set_transfer_user_to_review]", $form).prop('checked'),
            replace_browsed: $("[name=replace_browsed]", $form).prop('checked'),
            export_without_module_content: $("[name=export_without_module_content]", $form).prop('checked'),
            create_maker_config: $("[name=create_maker_config]", $form).prop('checked')
        },
        success: function (data) {
            $('#to-gvs-modal-content').html($('#text_your_job_add').text() + data.result);
            $("#btn-preparation-gvs").hide();
            $("#to-gvs-modal-content-cancel-popup").css('display', 'none');
            $("#to-gvs-modal-content-close-popup").show();
            $("#btn-open-task").show();
            $("#btn-open-task").click(function () {
                location.href = $('#backend_url').text() + '/backgroundtasks/preview/' + data.result;
            });
        }
    });
});

$(document).on("click", "#btn-export-to-release-cloud", function() {

    $("#export-form select").prop('disabled', true);

    $("#btn-preparation-gvs").css('display', 'inline-block');

    var $form = $("#export-form");

    $('#current_topic_code').val($("[name=folder_name]", $form).val());

    $.request('onPrepareForReleaseCompare', {
        data: {
            build_id: $("[name=build_id]", $form).val(),
            course_id: $("[name=course_id]", $form).val(),
            topic_id: $("[name=topic_id]", $form).val()
        },
        success : function(data) {
            if (typeof(data.result) == 'undefined') {
                $('#btn-continue').click();
                $(this).hide();
            } else {
                $("#btn-preparation-gvs").hide();
                $('#btn-continue').show();
                $('#no_latest_version_notice').css({'visibility' : 'visible'})
                $('#no_latest_version_notice_count').html(data.result);
            }
        }
    });
});
/* Prepare for export release cloud END */

$(document).on("change", "#Form-field-System-name-group select", function() {
    var sys_val = $(this).val();

    if (typeof(competences[sys_val]) === 'undefined') {
        $('#Form-field-Competence-name-group select').html('');
        return;
    }

    var html = '';

    for (var id in competences[sys_val]) {
        var name = competences[sys_val][id];

        html += '<option value="' + id + '">' + name + '</option>';
    }

    $('#Form-field-Competence-name-group select').html(html);
});

$(document).on("click", "#btn-import-course", function() {

    $("#export-form select").prop('disabled', true);

    $(this).hide();
    $("#btn-preparation-gvs").css('display', 'inline-block');

    var $form = $("#import-form");

    $.request('onImportCourse', {
        data: {
            filepath: $("[name=filepath]", $form).val(),
            course_id: $("[name=course_id]", $form).val(),
            sort: $("[name=sort]", $form).val()
        },
        success: function(data) {
            $("#current-build-id").change();
            $('#to-gvs-modal-content').html($('#text_your_job_add').text() + data.result);
            $("#btn-preparation-gvs").hide();
            $("#to-gvs-modal-content-cancel-popup").css('display', 'none');
            $("#to-gvs-modal-content-close-popup").show();
            $("#btn-open-task").show();
        }
    });
});

function select_file(title, path) {
    $('#Form-field-filename').val(title);
    $('#Form-field-filepath').val(path);
}

$(document).on('click', '#btn-import-course-select-file', function() {
    new $.oc.mediaManager.popup({
        alias: 'ocmediamanager',
        cropAndInsertButton: false,
        hideInsert: true,
        bottomToolbar : true,
        onInsert: function(items) {
            if (!items.length) {
                alert($('#igvs_courses_content_mediamanager_please_select_item_to_insert').text());
                return
            }

            if (items.length > 1) {
                alert($('#igvs_courses_content_mediamanager_please_select_a_single_item').text());
                return
            }

            var path, title, documentType;
            for (var i=0, len=items.length; i<len; i++) {
                path = items[i].path.substr(1, items[i].path.length);
                title = items[i].title;
                documentType = items[i].documentType;
            }

            if (documentType != 'document' || ['.tar'].indexOf(path.substr(-4).toLowerCase()) == -1) {
                alert($('#igvs_courses_content_mediamanager_please_select_a_tar_file').text());
                return;
            }

            select_file(title, path)

            this.hide()
        }
    });
});

/* set cookie */
function setCookie(name, value, options = {}) {
    if (typeof(options['path']) == 'undefined') {
        options['path'] = '/';
    }
    if (typeof(options['expires']) == 'undefined') {
        options['expires'] = new Date(Date.now() + 86400e3);
    }
    // при необходимости добавьте другие значения по умолчанию
    // ...

    if (options.expires.toUTCString) {
        options.expires = options.expires.toUTCString();
    }

    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

    for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
            updatedCookie += "=" + optionValue;
        }
    }

    document.cookie = updatedCookie;
}

// Пример использования:
//setCookie('user', 'John', {secure: true, 'max-age': 3600});

// возвращает куки с указанным name,
// или undefined, если ничего не найдено
function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function deleteCookie(name) {
    setCookie(name, "", {
        'max-age': -1
    })
}
