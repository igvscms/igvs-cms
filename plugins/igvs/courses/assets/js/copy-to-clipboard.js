var Clipboard = {
    copy : function (value) {

        switch (this.get_method()) {
            case 'ie':
                window.clipboardData.setData('Text', value);
                break;
            case 'webkit':
                navigator.clipboard.writeText(value);
                break;
            default:
                window.prompt("Copy to clipboard: Ctrl+C, Enter", value);
                break;
        }
    },
    get_method : function () {
        if (typeof(window.clipboardData) != 'undefined' && window.clipboardData.setData)
            return 'ie';

        if (typeof(navigator.clipboard != 'undefined'))
            return 'webkit';

        return 'default';
    }
};