$(document).ready(function(){
    $("body").on("click", "#btn-copy-course", function() {

        // validation fields
        if (!$("[name=type]", $form).val().length) {

            $.oc.flashMsg({
                'text': 'The Select action field is required.',
                'class': 'error',
                'interval': 3
            });

            return;
        }
        if ($("[name=modification_name]", $form).length && !$("[name=modification_name]", $form).val().length) {

            $.oc.flashMsg({
                'text': 'The Modification name field is required.',
                'class': 'error',
                'interval': 3
            });

            return;
        }
        if (!$("[name=title]", $form).val().length) {

            $.oc.flashMsg({
                'text': 'The Title field is required.',
                'class': 'error',
                'interval': 3
            });

            return;
        }
        if (!$("[name=code]", $form).val().length) {

            $.oc.flashMsg({
                'text': 'The Code field is required.',
                'class': 'error',
                'interval': 3
            });

            return;
        }
        if ($("[name=status_id]", $form).length && !$("[name=status_id]", $form).val().length && $("[name=status_id]", $form).data('one-status') != true) {

            $.oc.flashMsg({
                'text': 'It is impossible to create a copy, because there is no status selection.',
                'class': 'error',
                'interval': 3
            });

            return;
        }

        $(this).closest('.modal').trigger('close.oc.popup');

        // request
        var $form = $("#copy-course-form");

        $.request('onCopy', {
            data: {
                course_id: $("[name=course_id]", $form).val(),
                modification_name: $("[name=modification_name]", $form).val(),
                title: $("[name=title]", $form).val(),
                code: $("[name=code]", $form).val(),
                type: $("[name=type]", $form).val(),
                status_id: $("[name=status_id]", $form).val()
            },
            loading: "#loading"
        });

    });

    $(document).on("click", "#btn-save-link-to-theory", function() {

        var $form = $("#link-to-theory-form");

        $("[name=structure_id]", $form).prop('disabled', true);

        $(this).hide();
        $("#btn-preparation-link-to-theory").css('display', 'inline-block');

        $.request('onLinkToTheoryModuleFormSave', {
            data: {
                structure_id: $("[name=structure_id]", $form).val()
            },
            success: function(data) {
                $.oc.flashMsg({'text':'Ссылка сохранена'});
                $("#btn-preparation-link-to-theory").hide();

                $("#btn-save-link-to-theory").closest('.modal').trigger('close.oc.popup');
            }
        });
    });
});