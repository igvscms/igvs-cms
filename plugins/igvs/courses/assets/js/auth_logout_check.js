function inIframe () {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

var backend_root = $('meta[name=backend-base-path]').attr('content');

if (inIframe())
    parent.location.href = backend_root + '/backend/auth/back_uri_forget';