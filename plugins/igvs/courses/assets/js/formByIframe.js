function sendComplete(id) {
    var iframe = document.getElementById(id);
    if (iframe.onSendComplete &&
        typeof(iframe.onSendComplete) == 'function') iframe.onSendComplete();
}

function createIFrame() {
    var id = 'f' + Math.floor(Math.random() * 99999);
    var div = document.createElement('div');
    div.innerHTML = "<iframe style=\"display:none\" src=\"about:blank\" id=\"" + id + "\" name=\"" + id + "\" onload=\"sendComplete('" + id + "')\"></iframe>";
    document.body.appendChild(div);
    return document.getElementById(id);
}

function getIFrameXML(iframe) {
    var doc = iframe.contentDocument;
    if (!doc && iframe.contentWindow) doc = iframe.contentWindow.document;
    if (!doc) doc = window.frames[iframe.id].document;
    if (!doc) return null;
    if (doc.location == "about:blank") return null;
    if (doc.XMLDocument) doc = doc.XMLDocument;
    return doc;
}
/* ajax start */
function ajax_iframe(form, url, callback) {
    if (!document.createElement) return;
    if (typeof(form) == "string") form = document.getElementById(form);
    var frame = createIFrame();
    var act = form.getAttribute('action');
    frame.onSendComplete = function() {
        form.setAttribute('action', act);
        form.removeAttribute('target');
        var doc = getIFrameXML(frame);
        callback(doc.body.innerHTML);
    };
    form.setAttribute('target', frame.id);
    form.setAttribute('action', url);
    form.submit();
}
