$(document).ready(function() {
    $(document).on('mouseenter mouseleave', '.status', function(e) {
        var build_id = $(this).data('build_id'),
            el = $('[data-build_id="' + build_id + '"]');

        if (e.type == 'mouseenter') {
            el.addClass('hover');
        } else {
            el.removeClass('hover');
        }

    });

    $(document).on('change', '#Form-field-Course-theme', function() {
        var val = $(this).val();

        if (val == '_default') return;

        $.ajax({
            type : 'POST',
            url : 'properties',
            data : {
                theme_name : val
            },
            headers : {
                'X-OCTOBER-REQUEST-HANDLER' : 'onChangeThemeCheck'
            },
            success : function(d) {
                if (typeof(d.result) == 'undefined') return;

                $('[data-build_id]', '#Form-field-Course-color_themes_widget-group').removeClass('active');
                $('[data-build_id="' + val + '"]', '#Form-field-Course-color_themes_widget-group').addClass('active');

                var newPopup = $('<a />');
                newPopup.popup({
                    content : d.result
                });
            }
        });
    });
});