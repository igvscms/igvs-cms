var coursesListen = {

    htmlProgress: '\
        <div id="cp-progress-wrap" class="cp-progress-wrap">\
            <div id="cp-progress-container" class="cp-progress-container">\
                <div class="cp-progress-text">\
                    <div id="cp-progress-title" class="cp-progress-title"></div>\
                    <div id="cp-progress-comment" class="cp-progress-comment"></div>\
                </div>\
            </div>\
        </div>',

    cpProgress: function (topic, success) {

        $("body").append(this.htmlProgress);

        // bar
        var bar = new ProgressBar.Circle("#cp-progress-container", {
            color: '#95b752',
            trailColor: '#eee',
            strokeWidth: 12,
            trailWidth: 12,
            easing: 'bounce',
            duration: 1400,
            from: {color: '#e5a91a', a:0},
            to: {color: '#95b752', a:1},
            text: {
                autoStyleContainer: false
            },
            step: function(state, circle) {
                circle.path.setAttribute('stroke', state.color);
                // circle.path.setAttribute('stroke-width', state.width);

                var value = Math.round(circle.value() * 100);
                if (value === 0) {
                    circle.setText('');
                } else {
                    circle.setText(value + '%');
                }
            }
        });

        bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
        bar.text.style.fontSize = '2rem';

        // socket
        var conn = new ab.connect(
            window.clientConfig.socketLocation,
            function (session) {
                session.subscribe(topic, function (topic, data) {
                    var progress = parseFloat(data.data.progress);

                    if (data.data.error) {
                        $("#cp-progress-wrap").hide();
                        $.oc.alert(data.data.comment);
                        return;
                    }

                    // console.log(progress);
                    if (progress == 1) {
                        $("#cp-progress-wrap").hide();
                        $.oc.flashMsg({
                            'text': data.data.comment,
                            'class': 'success'
                        });

                        if (success) {
                            success();
                        }

                        window.location.href = window.location.pathname + '?page=' + $("#Lists select[name=page]").val();
                    }

                    bar.set(progress);

                    $("#cp-progress-title").text(data.data.title)
                    $("#cp-progress-comment").text(data.data.comment);

                    if($("#cp-progress-wrap").css('display') == 'none' && progress != 1) {
                        $("#cp-progress-wrap").show();
                    }
                });
            },
            function (code, reason, detail) {
                console.warn('WebSocket connection closed: code=', code, '; reasone=', reason, '; detail=', detail);
            },
            {
                maxRetries: 60,
                retryDelay: 4000,
                skipSubprotocolCheck: true
            }
        );
    }
};