var igvs = (function(){
    return {
        config : {
            display_error : true,
            theme : $('#Form-field-ModuleContent-theme', parent.window.document).val(),
            base_url : '/storage/app/igvs/module-content/' +
                $('#Form-field-ModuleContent-course_id', parent.window.document).val() +
                '/' +
                $('#Form-field-ModuleContent-id', parent.window.document).val() +
                '/' 
        },
        system : {
            openPopup : function(callback) {
                var self = this;

                new $.oc.mediaManager.popup({
                    alias: 'ocmediamanager',
                    cropAndInsertButton: false,
                    hideInsert: true,
                    bottomToolbar : true,
                    onInsert: function(items) {
                        if (!items.length) {
                            alert($('#igvs_courses_content_mediamanager_please_select_item_to_insert', parent.window.parent.window.parent.window.document).text());
                            return
                        }

                        if (items.length > 1) {
                            alert($('#igvs_courses_content_mediamanager_please_select_a_single_item', parent.window.parent.window.parent.window.document).text());
                            return
                        }

                        var path, publicUrl
                        for (var i=0, len=items.length; i<len; i++) {
                            path = items[i].path.substr(1, items[i].path.length);
                            publicUrl = items[i].publicUrl
                        }

                        callback(path, publicUrl);

                        this.hide()
                    }
                });
            },
            ajax : function(act, callback, params) {
                if (typeof(act) == 'undefined') return false;
                if (typeof(params) == 'undefined') params = [];

                var url_parts = window.location.href.split('/');
                var lesson_id = url_parts[url_parts.length-1];

                $.ajax({
                    method : 'GET',
                    url : '/b/ajax/?lesson=' + lesson_id,
                    data : {
                        act : act,
                        params : params
                    },
                    success : function(data) {
                        callback(data);
                    }
                });

            },
            ajaxEditReload : function(text) {
                //var editor = parent.window.$('#CodeEditor-formData-data').codeEditor('getEditorObject');
                //editor.getSession().setValue(text);
            },
            getLessonId : function() {
                var url_parts = window.location.pathname.split('/');
                return url_parts[url_parts.length-1];
            }
        },
        storage : {
            getAudio : function(callback) {
                if (!igvs.checkCallback(callback, 'igvs.storage.getAudio', 'input callback is not function'))
                    return false;
                
                igvs.system.openPopup(callback);
            },
            getImage : function(callback) {
                if (!igvs.checkCallback(callback, 'igvs.storage.getImage', 'input callback is not function'))
                    return false;

                igvs.system.openPopup(callback);
            },
            getVideo : function(callback) {
                if (!igvs.checkCallback(callback, 'igvs.storage.getVideo', 'input callback is not function'))
                    return false;

                igvs.system.openPopup(callback);
            },
            getFile : function(callback) {
                if (!igvs.checkCallback(callback, 'igvs.storage.getFile', 'input callback is not function'))
                    return false;

                igvs.system.openPopup(callback);
            },
            getBasePath : function() {
                return igvs.config.base_url;
            },
            getLanguage : function () {
                var parseQueryString = function (strQuery) {
                    var strSearch   = strQuery.substr(1),
                        strPattern  = /([^=]+)=([^&]+)&?/ig,
                        arrMatch    = strPattern.exec(strSearch),
                        objRes      = {};
                    while (arrMatch != null) {
                        objRes[arrMatch[1]] = arrMatch[2];
                        arrMatch = strPattern.exec(strSearch);
                    }
                    return objRes;
                };
                var queryStr = parseQueryString(window.location.search);
                return queryStr['lang']==undefined ? 'en' : queryStr['lang'];
            },
        },
        moduleContent : {
            load : function(callback) {
                if (!igvs.checkCallback(callback, 'igvs.moduleContent.load', 'input callback is not function'))
                    return false;

                var lesson_id = igvs.system.getLessonId();

                parent.window.$.request('onLoadJson', {
                    data: {
                        id: lesson_id
                    },
                    success: function(data) {
                        callback(data.result);
                    },
                    error: function() {
                        callback(false);
                        igvs.exception('igvs.moduleContent.load', 'error response from server');
                    }
                });
            },
            save : function(data, callback) {
                if (!igvs.checkCallback(callback, 'igvs.moduleContent.save', 'input callback is not function'))
                    return false;
                if (typeof(data) != 'string') igvs.exception('igvs.moduleContent.save', 'input data must be string');

                var lesson_id = igvs.system.getLessonId();

                igvs.system.ajaxEditReload(data);

                if (!('contentLoaded' in parent.window)) {
                    parent.window.contentLoaded = 0;
                }
                parent.window.contentLoaded++;

                parent.window.$.request('onSaveJson', {
                    data: {
                        id: lesson_id,
                        data: data
                    },
                    success: function(data) {
                        parent.window.contentLoaded--;
                        callback(data);
                    },
                    error: function() {
                        parent.window.contentLoaded--;
                        callback(false);
                        igvs.exception('igvs.moduleContent.load', 'error response from server');
                    }
                });
            }
        },
        permissions : {
            get : function(callback, param) {
                if (!igvs.checkCallback(callback, 'igvs.permissions.get', 'input callback is not function'))
                    return false;

                console.log('permissions get');

                if (!param) param = '';
                var lesson_id = igvs.system.getLessonId();

                parent.window.$.request('onLoadPermission', {
                    data: {
                        id: lesson_id,
                        permission: param
                    },
                    success: function(data) {
                        console.log('data');
                        console.log(data);
                        try {
                            var result = JSON.parse(data.result);
                        } catch (e) {
                            callback(false);
                            igvs.exception('igvs.permissions.get', 'error response from server (invalid json)');
                            return;
                        }

                        callback(result.result);
                    },
                    error: function() {
                        callback(false);
                        igvs.exception('igvs.permissions.get', 'error response from server');
                    }
                });
            }
        },

        tools : {
            designerStateLoaded : function() {
                parent.parent.parent.parent.document.getElementById('workspace_frame').setAttribute('data-designer-state-loaded',true);
            }
        },


        checkCallback : function(callback, function_name, text) {
            if (typeof(callback) != 'function') {
                    if (this.config.display_error) {
                        this.exception(function_name, text);
                    }
                    return false;
            }
            return true;
        },
        exception : function(function_name, text) {
            if (this.config.display_error) {
                console.log('Error in igvs function \'' + function_name + '\'. ' + text);
            }
        }
    }
})();