<?php
/**
 * Created by PhpStorm.
 * User: shubin-ma
 * Date: 14.04.16
 * Time: 15:20
 */

namespace Igvs\Courses\Helpers;


class Translit
{
    static protected $translit_ru_en_table = [
        "й" => "y", "Й" => "Y",
        "ц" => "ts", "Ц" => "Ts",
        "у" => "u", "У" => "U",
        "к" => "k", "К" => "K",
        "е" => "e", "Е" => "E",
        "н" => "n", "Н" => "N",
//        "н" => "h", "Н" => "H",
        "г" => "g", "Г" => "G",
        "ш" => "sh", "Ш" => "Sh",
        "щ" => "sh", "Щ" => "Sh",
        "з" => "z", "З" => "Z",
        "х" => "h", "Х" => "H",
//        "х" => "x", "Х" => "X",
        "ъ" => "", "Ъ" => "",
        "ф" => "f", "Ф" => "F",
        "ы" => "e", "Ы" => "E",
        "в" => "v", "В" => "V",
        "а" => "a", "А" => "A",
        "п" => "p", "П" => "P",
        "р" => "r", "Р" => "R",
//        "р" => "p", "Р" => "P",
        "о" => "o", "О" => "O",
        "л" => "l", "Л" => "L",
        "д" => "d", "Д" => "D",
        "ж" => "j", "Ж" => "J",
        "э" => "e", "Э" => "E",
        "я" => "ya", "Я" => "Ya",
        "ч" => "ch", "Ч" => "Ch",
        "с" => "s", "С" => "S",
//        "с" => "c", "С" => "C",
        "м" => "m", "М" => "M",
        "и" => "i", "И" => "I",
        "т" => "t", "Т" => "T",
        "ь" => "", "Ь" => "",
        "б" => "b", "Б" => "B",
        "ю" => "yu", "Ю" => "Yu",
        ":" => "",
//        " " => "_",
    ];

    /*
    static protected $ruEn = [
        "й" => "y", "Й" => "Y",
        "ц" => "ts", "Ц" => "Ts",
        "у" => "u", "У" => "U",
        "к" => "k", "К" => "K",
        "е" => "e", "Е" => "E",
        "н" => "n", "Н" => "N",
        "г" => "g", "Г" => "G",
        "ш" => "sh", "Ш" => "Sh",
        "щ" => "sh", "Щ" => "Sh",
        "з" => "z", "З" => "Z",
        "х" => "h", "Х" => "H",
        "ъ" => "", "Ъ" => "",
        "ф" => "f", "Ф" => "F",
        "ы" => "e", "Ы" => "E",
        "в" => "v", "В" => "V",
        "а" => "a", "А" => "A",
        "п" => "p", "П" => "P",
        "р" => "r", "Р" => "R",
        "о" => "o", "О" => "O",
        "л" => "l", "Л" => "L",
        "д" => "d", "Д" => "D",
        "ж" => "j", "Ж" => "J",
        "э" => "e", "Э" => "E",
        "я" => "ya", "Я" => "Ya",
        "ч" => "ch", "Ч" => "Ch",
        "с" => "s", "С" => "S",
        "м" => "m", "М" => "M",
        "и" => "i", "И" => "I",
        "т" => "t", "Т" => "T",
        "ь" => "", "Ь" => "",
        "б" => "b", "Б" => "B",
        ":" => "",
//        " " => "_",
    ];
    */

    static public function translit($str, $options = [])
    {
        $separator = '-';
        $remove_special_symbol = false;

        if (isset($options['remove_special_symbol'])) {
            $remove_special_symbol = (boolean)$options['remove_special_symbol'];
        }

        $replace_search = array_keys(self::$translit_ru_en_table);
        $replace_replace = array_values(self::$translit_ru_en_table);

        // separator
        $replace_search[] = ' ';
        $replace_replace[] = $separator;

        $str = str_replace($replace_search, $replace_replace, $str);

        if ($remove_special_symbol) {
            // Remove all characters that are not the separator, letters, numbers or whitespaces.
            $str = preg_replace('![^\s\(\)&\.' . preg_quote($separator) . '\.\_+0-9A-Za-z\'\s]+!u', '', $str);
        }

        return $str;
    }

    /*
    public static function ruEn($str, $escape = false, $exclude = '-')
    {
        $search = array_keys(self::$ruEn);
        $replace = array_values(self::$ruEn);
        $str = str_replace($search, $replace, $str);

        $exclude = !is_array($exclude) ?: implode($exclude, '');
        $exclude = preg_quote($exclude);

        if ($escape) {
            $str = preg_replace("|[^{$exclude}+0-9A-Za-z]+|u", '', $str);
        }

        return $str;
    }

    // Фиксит строку в которой втречаются символы кирилицы
    fixRuEn($str, $escape = true, $separator = '-')
    {
    }

    escapeRu()
    {

    }
    */
}