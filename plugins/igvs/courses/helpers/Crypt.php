<?php namespace Igvs\Courses\Helpers;

class Crypt
{
    static private function strcode($str, $passw = "")
    {
        $salt = "Dn8*#2n!9j";
        $len = strlen($str);
        $gamma = '';
        $n = $len > 100 ? 8 : 2;
        while (mb_strlen($gamma) < $len) {
            $gamma .= mb_substr(pack('Z', sha1($passw . $gamma . $salt)), 0, $n);
        }
        return $str ^ $gamma;
    }

    static public function encode($str, $passw = "")
    {
        return base64_encode(self::strcode($str, $passw));
    }

    static public function decode($str, $passw = "")
    {
        return self::strcode(base64_decode($str), $passw);
    }
}
