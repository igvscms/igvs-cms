<?php return [
    'status' => [
        'created' => 'Created',
        'deleted' => 'Deleted',
        'restored' => 'Restored',
        'replaced' => 'Replaced',
        'renamed' => 'Renamed'
    ]
];