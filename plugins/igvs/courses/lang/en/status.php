<?php return [
    'label' => 'Statuses',
    'create' => 'Create status',
    'change' => 'Change status',
    'field' => [
        'created' => 'Available for creation',
        'command' => 'Command'
    ],
    'block_export' => 'Block export',
];