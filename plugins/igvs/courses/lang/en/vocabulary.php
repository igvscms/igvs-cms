<?php return [
    'relation_label' => 'term',
    'field' => [
        'term' => 'Term',
        'description' => 'Description'
    ],
    'statuses' => [
        'new' => '<span style="color: green;">New</span>',
        'rewrite' => '<span style="color: red;">Be rewrite</span>',
        'existed' => 'Exist in dict',
        'double' => '<span style="color: red;">Double</span>',
    ],
    'continue' => 'Continue'
];