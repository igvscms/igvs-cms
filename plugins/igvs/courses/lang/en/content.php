<?php return [
    'name' => 'Deleted modules',
    'field' => [
        'term' => 'Term',
        'name' => 'Name',
        'code' => 'Code',
        'user' => 'User',
        'course' => 'Course',
        'category' => 'Category',
        'deleted_at' => 'Deleted',
        'created_at' => 'Created',
    ],
    'is_not_last_version' => 'The latest version',
    'mediamanager' => [
        'please_select_item_to_insert' => 'Please select item to insert',
        'please_select_a_single_item' => 'Please select a single item.',
        'please_select_a_tar_file' => 'Please select a TAR file.',
        'please_select_a_xls_xlsx_document_file' => 'Please select a XLS/XLSX document file.',
    ],
];