<?php return [
    'relation_label' => 'Member',
    'field' => [
        'course_project_roles' => 'Roles in the project',
        'roles' => 'Roles in the course'
    ]
];