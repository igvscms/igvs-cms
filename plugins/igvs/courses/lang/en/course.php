<?php return [
    'field' => [
        'comments' => 'The event log',
        'materials' => 'Working programm',
        'developer' => 'Developer',
        'action' => [
             'go_to_the_course' => 'Go to the course',
        ],
        'member_id' => 'member ID',
        'first_name' => 'member first name',
        'last_name' => 'member last name',
        'pilot id' => 'pilot ID',
        'cluster id' => 'cluster ID',
        'instance id' => 'instance ID',
        'id' => 'course ID',
    ],
    'message' => [
        'current_role' => 'Your role in the current course',
        'successfully_deleted_records' => 'Successfully deleted the selected records.',
        'waiting' => 'Waiting for copying...',
    ],
    'action' => [
        'export' => 'Export all courses'
    ],
    'amounts' => [
        'amounts' => 'Amounts report',
        'topic_name' => 'Topic Name',
        'amount_modules' => 'Amount Modules',
        'amount_questions' => 'Amount Questions',
        'amount_modules_by_behaviors' => 'Amount modules by behaviors',
        'amount_questions_by_behaviors' => 'Amount questions by behaviors',
    ],
];
