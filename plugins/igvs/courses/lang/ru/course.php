<?php return [
    'field' => [
        'comments' => 'Журнал событий',
        'materials' => 'Рабочая программа',
        'developer' => 'Разработчик',
        'action' => [
             'go_to_the_course' => 'Перейти к курсу',
        ],
        'member_id' => 'ID участника',
        'first_name' => 'Имя участника',
        'last_name' => 'Фамилие участника',
        'pilot id' => 'ID пилота',
        'cluster id' => 'ID кластера',
        'instance id' => 'ID инстанса',
        'id' => 'ID курса',
    ],
    'message' => [
        'current_role' => 'Ваша роль в текущем курсе',
        'successfully_deleted_records' => 'Выбранные записи успешно удалены',
        'waiting' => 'Ожидает копирования...',
    ],
    'action' => [
        'export' => 'Экспорт всех курсов'
    ],
    'amounts' => [
        'amounts' => 'Сводка по количеству',
        'topic_name' => 'Topic Name',
        'amount_modules' => 'Amount Modules',
        'amount_questions' => 'Amount Questions',
        'amount_modules_by_behaviors' => 'Amount modules by behaviors',
        'amount_questions_by_behaviors' => 'Amount questions by behaviors',
/*
        'topic_name' => 'Наименование Топика',
        'amount_modules' => 'Количество Модулей',
        'amount_questions' => 'Количество Вопросов',
        'amount_modules_by_behaviors' => 'Количество Модулей по поведению',
        'amount_questions_by_behaviors' => 'Количество Вопросов по поведению',
        */
    ],
];
