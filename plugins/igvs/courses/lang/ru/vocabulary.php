<?php return [
    'relation_label' => 'термина',
    'field' => [
        'term' => 'Термин',
        'description' => 'Значение'
    ],
    'statuses' => [
        'new' => '<span style="color: green;">Новый</span>',
        'rewrite' => '<span style="color: red;">Будет заменён</span>',
        'existed' => 'Уже в словаре',
        'double' => '<span style="color: red;">Дубль</span>',
    ],
    'continue' => 'Продолжить'
];