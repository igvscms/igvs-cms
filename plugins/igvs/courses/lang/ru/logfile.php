<?php return [
    'status' => [
        'created' => 'Создан',
        'deleted' => 'Удален',
        'restored' => 'Восстановлен',
        'replaced' => 'Заменен',
        'renamed' => 'Переименован'
    ]
];