<?php return [
    'name' => 'Удаленные модули',
    'field' => [
        'term' => 'Термин',
        'name' => 'Название',
        'code' => 'Код',
        'user' => 'Пользователь',
        'course' => 'Курс',
        'category' => 'Категория',
        'deleted_at' => 'Удалено',
        'created_at' => 'Создано',
    ],
    'is_not_last_version' => 'Последняя версия',
    'mediamanager' => [
        'please_select_item_to_insert' => 'Пожалуйста, выберите что-то для вставки.',
        'please_select_a_single_item' => 'Пожалуйста, выберите одно значение.',
        'please_select_a_tar_file' => 'Пожалуйста, выберите файл формата TAR.',
        'please_select_a_xls_xlsx_document_file' => 'Пожалуйста, выберете файд формата XLS/XLSX.',
    ],
];