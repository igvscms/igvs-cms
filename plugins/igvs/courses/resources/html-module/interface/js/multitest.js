$(document).ready(function() {

	// var jsonUrl = "develop/json/" + $("body").attr('jsonUrl') + ".json";
	// $.when($.get("develop/js/templateRadiobuttonImage.html"), $.get("develop/js/templateRadiobuttonText.html"), $.getJSON(jsonUrl)).done(function(a1, a2, jsonData) {
	// 	var tempFn = {
	// 		'radiobuttonimage': doT.template(a1[0]),
	// 		'radiobuttontext': doT.template(a2[0])
	// 	}
	// 	var textJson = jsonData[0][0];
	// });
	if (typeof jsonObj !== "undefined") {
		var tempFn = {
			'radiobuttonimage': doT.template(document.getElementById('templateRadiobuttonImage').text),
			'radiobuttontext': doT.template(document.getElementById('templateRadiobuttonText').text),
			'mappingtext': doT.template(document.getElementById('templateMappingText').text),
		}
		var textJson = jsonObj[0];
		var resultText = "";
		$.each(textJson.questions, function() {
			shuffle(this.answers);
			var str = this.typeTest + this.answersType;
			str = str.toLowerCase();
			resultText += tempFn[str](this);
		});
		$(".questions").html(resultText);
		if ($("#startButton") > 0) {
			$("#startButton").on("click", function() {
				$(".startWrapper").css("display", "none");
				$(".multitestInnerWrapper").css("display", "table");
				$(".quiestion.current").css("overflow", "auto");
				multitestus();
			});
		} else {
			$(".startWrapper").css("display", "none");
			$(".multitestInnerWrapper").css("display", "table");
			$(".quiestion.current").css("overflow", "auto");
			multitestus();
		}


	} else {
		multitestus();
	}

});

function multitestus() {
	// body...
	/* MULTITEST */
	var questionTypes = {
		'radio': window.RadioButtons,
		'checkboxes': window.Checkboxes,
		'links': window.Links
	};
	var start = true;
	var startTime = new Date;
	if ($("body").attr("timer")) {
		start_countdown($("body").attr("timer"));
	} else {
		start_timer();
	}
	var questionContainer = $('.questions');
	var questions = questionContainer.find('.question');
	var questionCount = 0;
	var questionNum = questions.length;
	var scores = 0;
	var indicators = multiplyIndicators($('.indicator'));
	var resultsDiv = $('.resultWrapper');
	var results = new Results(resultsDiv);
	var currentQuestionDiv, currentQuestion, currentIndicator;
	questionContainer.addClass('active');
	var answerBtn = $('.answer');
	answerBtn.on("click", goToNextQuestion);
	answerBtn[0].addEventListener('touchend', goToNextQuestion, false)
	goToNextQuestion();


	resultsDiv.find('#replay').on("click", repeat);
	resultsDiv.find('#replay')[0].addEventListener('touchend', repeat, false)

	function multiplyIndicators(indicator) {
		var container = indicator.parent();
		for (var i = 2; i <= questionNum; i++) {
			container.append(indicator.clone().html(i));
		}
		return container.children();
	}

	function goToNextQuestion() {
		answerBtn[0].disabled = true;
		answerBtn.addClass('disabled');
		if (!start) {
			var result = currentQuestion.getResult();
			if (result) currentIndicator.addClass('high');
			else currentIndicator.addClass('low');
			scores += result;
			currentQuestionDiv.removeClass('current').css("display", "none");
			currentIndicator.removeClass('current');
			$(currentQuestion).off('complete').off('incomplete');
		}

		if (questionCount < questionNum) {
			currentQuestionDiv = questions.eq(questionCount);
			currentIndicator = indicators.eq(questionCount);
			currentQuestionDiv.addClass('current').css("display", "block").css("left", "0");
			currentIndicator.addClass('current');
			container = currentQuestionDiv.find('.collapsibles-container');
			// init_collapsibles(); // вот этот инит надо расскоментить данпан

			if (container.find(".plankImg").size() != 0) {
				imgResize(container);
				imgSlideShowInit(container);
				// checkImgHeight(currentImg);
			};

			if (container.find(".plankText").size() != 0) {
				setClassesTextBlocks(container)
				resizeTextBlocks(container)
			};
			currentQuestion = new questionTypes[currentQuestionDiv.attr('type')](currentQuestionDiv);

			answerBtn[0].disabled = true;
			answerBtn.addClass('disabled');
			$(currentQuestion).on('complete', function(e) {
				answerBtn[0].disabled = false;
				answerBtn.removeClass('disabled');
			});
			$(currentQuestion).on('incomplete', function(e) {
				answerBtn[0].disabled = true;
				answerBtn.addClass('disabled');
			});
			questionCount++;
		} else {
			showResult();
		}

		start = false;
	}

	function showResult() {
		stop_timer();
		var endTime = new Date;

		$(".multitestInnerWrapper").css('display', 'none');
		container = null;
		// init_collapsibles();
		answerBtn.css('display', 'none');
		questionContainer.removeClass('active');
		resultsDiv.addClass('active');
		results.set(scores, questionNum - scores, formatTime(Math.round((endTime.getTime() - startTime.getTime()) / 1000)));
		// $("#content").css("display", 'none');
		// $("#content").slideDown({
		// 	duration: 700,
		// 	easing: "swing"
		// });
		saveResult(startTime, endTime);

	}

	function repeat() {
		reset();
		resultsDiv.removeClass('active');
		questionContainer.addClass('active');
		answerBtn.css('display', 'inline-block');
		$(".multitestInnerWrapper").css("display", "table");
		startTime = new Date;
		// $(".plank").removeClass().addClass("plank");
		$.each($(".ABX"), function() {
			// body...
			if ($(this).find(".plank").parent().hasClass("columnMultitest")) {
				$(this).find(".plank").unwrap();
			}
			$(this).find(".plank").shuffle();
		});


		if ($("body").attr("timer")) {
			start_countdown($("body").attr("timer"));
		} else {
			start_timer();
		}
		goToNextQuestion();
	}

	function reset() {
		currentQuestionDiv.css("display", "none");
		indicators.removeClass('high').removeClass('low');
		questionCount = 0;
		scores = 0;
		start = true;
	}

	function saveResult(startTime, endTime) {
		var app = window.parent.app;

		var date = endTime.getDate() + '.' + (endTime.getMonth() + 1) + '.' + endTime.getFullYear();
		app.addJournal(JSON.stringify([app.getEappID(), date, getTime(endTime), getTime(startTime), questionNum, scores]));
	}

	function getTime(date) {
		return date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
	}

	function formatTime(seconds) {
		var hours = Math.floor(seconds / 3600);
		var minutes = Math.floor((seconds % 3600) / 60);
		return formatAs2Digits(hours) + ':' + formatAs2Digits(minutes) + ':' + formatAs2Digits(seconds % 60);
	}

	function formatAs2Digits(n) {
		return ('00' + n).substr(-2);
	}
	var timer;

	function simple_timer(sec, block, direction, stop) {
		var time = sec;
		direction = direction || false;

		// var hour = parseInt(time / 3600);
		// if (hour < 1) hour = 0;
		// time = parseInt(time - hour * 3600);
		// if (hour < 10) hour = '0' + hour;

		var minutes = parseInt(time / 60);
		if (minutes < 1) minutes = 0;
		time = parseInt(time - minutes * 60);
		if (minutes < 10) minutes = '0' + minutes;

		var seconds = time;
		if (seconds < 10) seconds = '0' + seconds;

		// block.text(hour + ':' + minutes + ':' + seconds);
		block.text(minutes + ':' + seconds);


		if (!stop) {
			if (direction) {
				sec++;

				timer = setTimeout(function() {
					simple_timer(sec, block, direction);
				}, 1000);
			} else {
				sec--;

				if (sec > 0) {
					timer = setTimeout(function() {
						simple_timer(sec, block, direction);
					}, 1000);
				} else {
					// alert('Время вышло!');
					showResult();
				}
			}
		}
	}

	function start_timer() {
		var block = $('.timer');
		simple_timer(0, block, true);
	}

	function start_countdown(duration) {
		var block = $('.timer');
		simple_timer(duration, block, false);
	}

	function stop_timer() {
		clearTimeout(timer);
	}

};

/* RESULTS */

/**
 * Created by kravchenko.as on 29.01.14.
 */
(function() {

	function Results(container) {
		this.container = container;
		//this.diagram = container.find('#donutchart');
		this.percentField = container.find('#percent');
		this.rightAnswers = container.find('#rightAnswers');
		this.wrongAnswers = container.find('#wrongAnswers');
		this.answersAmount = container.find('#answersAmount');
		this.timeSpent = container.find('#timeSpent');
		this.doneTest = container.find('.done').find("h3");
		this.round = container.find('.round');
	}

	Results.prototype.set = function(correctNum, incorrectNum, timeSpent) {

		this.doneTest.attr('style', 'color :green');
		this.round.removeClass('test_failed');
		var totalNum = correctNum + incorrectNum;
		this.rightAnswers.html(correctNum);
		this.wrongAnswers.html(incorrectNum);
		this.answersAmount.html(totalNum);
		this.timeSpent.html(timeSpent);
		var testDoneOk = "TEST COMPLETED!";
		var testNotDone = "TEST FAILED!";

		var percent = (correctNum / totalNum * 100).toFixed(0);
		if (percent > 70 || percent === 70) {
			this.doneTest.html(testDoneOk);
		};
		if (percent < 70) {
			this.doneTest.html(testNotDone).attr('style', 'color :red !important');
			this.round.addClass('test_failed');
		};
		this.percentField.html(percent + "%");
		// var percent = parseInt(correctNum / totalNum * 10000) / 100;
		// this.percentField.html(percent + '%');
		//this.diagram.attr('data-text', percent + '%');
		//this.diagram.attr('data-percent', percent);

		//this.animate();
	};

	/*Results.prototype.animate = function(options){
	 this.diagram.circliful(options);
	 };*/

	window.Results = Results;
})();

/* CHECKBOXES */

/**
 * Created by kravchenko.as on 28.01.14.
 */
(function() {
	function Checkboxes(container) {
		this.optionContainer = container.find('.CHECKBOXES');
		var that = this;

		var options = this.optionContainer.find('input');
		options.click(function() {
			$(that).trigger('complete');
			options.off('click');
		});

		this.getResult = function() {
			return getResult(that);
		};
	}

	function getResult(that) {
		var options = that.optionContainer.find('input');
		for (var i = 0; i < options.length; i++) {
			if (options[i].checked != (options[i].getAttribute('checkIt') == 'true')) {
				return 0;
			}
		}
		return 1;
	}

	Checkboxes.prototype.update = function(data) {
		var html = '';
		var options = data.options;
		for (var i = 0; i < options.length; i++) {
			html += '<li><input id="item' + i + '" type="checkbox" class="CheckBoxClass" checkIt=' + options[i]['checked'] + '><label for="item' + i + '">' + options[i]['text'] + '</label></li>';
		}
		this.optionContainer.innerHTML = html;
		document.getElementById('description').innerHTML = data.task;
	};

	window.Checkboxes = Checkboxes;
})();

/* RADIOBUTTONS */

/**
 * Created by kravchenko.as on 28.01.14.
 */
(function() {
	function RadioButtons(container) {
		this.optionContainer = container.find('.RADIOBUTTONS');
		var that = this;

		var options = this.optionContainer.find('input');
		options.click(function() {
			$(that).trigger('complete');
			options.off('click');
		});

		this.getResult = function() {
			return getResult(that);
		};
	}

	function getResult(that) {
		var options = that.optionContainer.find('input');
		for (var i = 0; i < options.length; i++) {
			if (options[i].checked != (options[i].getAttribute('checkIt') == 'true')) {
				return 0;
			}
		}
		return 1;
	}

	RadioButtons.prototype.update = function(data) {
		var html = '';
		var options = data.options;
		for (var i = 0; i < options.length; i++) {
			html += '<li><input id="item' + i + '" value="rad' + i + '" type="radio" name="radio" checkIt=' + options[i]['checked'] + '><label for="item' + i + '">' + options[i]['text'] + '</label></li>';
		}
		this.optionContainer.innerHTML = html;
		document.getElementById('description').innerHTML = data.task;
	};

	window.RadioButtons = RadioButtons;
})();

/* LINKS */

/**
 * Created with WebStorm.
 * User: Alex
 * Date: 20.11.13
 * Time: 22:25
 * To change this template use File | Settings | File Templates.
 */

(function() {
	//var rightAnswers; = [{1:true, 2:true}, {0:true}, {2:true}, {3:true}, {4:true}];
	window.Links = Links;

	function Links(container) {
		this.container = container;
		this.lnks = new LinkCreator(container.find('.LINKS'));

		var that = this;
		$(this.lnks).on('dotDrop', function() {
			if (that.lnks.testCompleted()) {
				$(that).trigger('complete');
			} else {
				$(that).trigger('incomplete');
			}
		});

		this.getResult = function() {
			return getResult(container, that.lnks.result());
		};
	}

	Links.prototype.update = function(data) {
		var leftData = data.leftFields;
		var rightData = data.rightFields;
		var leftHtml = '';
		var rightHtml = '';
		for (var i in leftData) {
			if (!leftData.hasOwnProperty(i)) continue;
			var cons = leftData[i].cons;
			leftHtml += '<div class="plank"><div class="plank-content"><div class="collapsible left"><div class="text">' + leftData[i].text + '</div></div></div><div class="plank-dotplace"><div class="plank-dotplace-IN"><div class="dotplace source" dots="' + cons.split(' ').length + '" cons="' + cons + '"></div></div></div></div>';
		}
		for (var j in rightData) {
			if (!rightData.hasOwnProperty(j)) continue;
			rightHtml += '<div class="plank"><div class="plank-dotplace"><div class="plank-dotplace-IN"><div class="dotplace target"></div></div></div><div class="plank-content"><div class="collapsible right"><div class="text">' + rightData[j] + '</div></div></div></div>';
		}
		$('.sources').html(leftHtml);
		$('.targets').html(rightHtml);

		this.lnks = new LinkCreator(this.container.find('.LINKS'));
	};

	function getResult(container, results) {
		var success = true;
		var rightAnswers = readRightAnswers(container);
		loop: for (var key in rightAnswers) {
			if (!rightAnswers.hasOwnProperty(key)) continue;
			for (var id in rightAnswers[key]) {
				if (!rightAnswers[key].hasOwnProperty(id)) continue;
				if (!results[key][id]) {
					success = false;
					break loop;
				}
			}
		}

		return Number(success);
	}

	function readRightAnswers(container) {
		var result = [];
		container.find('.source > .dotplace-IN').each(function(i, elem) {
			var numbers = $(elem).attr('cons').split(' ');
			var answers = {};
			numbers.forEach(function(item, index, array) {
				answers[parseInt(item) - 1] = true;
			});
			result.push(answers);
		});
		return result;
	}

	function LinkCreator(container) {
		var that = this;

		initTouchEvents(); // init touch only if is REALLY needed
		container.find('.dot').remove();

		var canvas = container.find('.draw');
		if (!canvas.length)
			canvas = $('<canvas class="draw"></canvas>').prependTo(container);

		var leftplace = container.find('.source > .dotplace-IN');
		var rightplace = container.find('.target > .dotplace-IN');

		var draw;

		if (canvas[0].getContext) {
			draw = canvas[0].getContext("2d");
			innerResize();
			drawAllLines();
			draw.clearRect(0, 0, canvas[0].width, canvas[0].height);
		} else {
			alert("canvas недоступен ):");
		}

		$(window).resize(function() {
			innerResize();
			drawAllLines();
		});

		initPlaces();
		var alldots = container.find('.dot');

		function initPlaces() {
			var place;
			for (var i = 0; i < leftplace.length; i++) {
				place = $(leftplace[i]);
				place.data("id", i);
				place.css("z-index", "2");
				createDots(place);
			}
			for (var i = 0; i < rightplace.length; i++) {
				place = $(rightplace[i]);
				place.data("id", 100 + i);
				place.css("z-index", "2");
			}

		}

		function createDots(place) {
			var dotAmount = parseInt(place.attr("dots"));
			var dot;
			var placepos = place.position();
			placepos.left = placepos.left + place.width() / 2;
			placepos.top = placepos.top + place.height() / 2;
			for (var j = 0; j < dotAmount; j++) {
				dot = $('<div class="dot"></div>').appendTo(place);
				dot.css("z-index", "4");
				dot.css("left", (place.width() / 2 - dot.width() / 2) + "px");

				dot.data("parent", place);
				dot.data("owner", place);
				dot.data("interval", 0);
				dot.data("id", place.data("id") + "_" + j);
				dot.data("linked", false);

				dot.css("top", (place.height() / 2 - dot.height() / 2) + "px");
				dot.on("mousedown", dotDownHandler);
			}
		}

		function drawAllLines() {
			draw.clearRect(0, 0, canvas[0].width, canvas[0].height);
			var alldots = container.find('.dot');

			$.each(alldots, function() {
				drawLine($(this));
			});

			// for (var i = 0; i < alldots.length; i++) {
			// 	cdot = $(alldots[i]);
			// 	drawLine(cdot);
			// }
		}

		function drawLine(dot) {
			var start = getCenterOf(dot.data("parent"));
			var end = getCenterOf(dot);

			draw.beginPath();
			draw.moveTo(start.left, start.top);
			draw.lineTo(end.left, end.top);
			draw.lineCap = 'round';
			draw.strokeStyle = 'rgb(47,105,142)';
			draw.lineWidth = 4;
			draw.stroke();
		}

		// ========= //
		// MOVE DOTS //
		// ========= //

		var currentDot = null;
		var dx = 0;
		var dy = 0;

		var lastX = -1;
		var lastY = -1;

		function dotDownHandler(e) {

			$(".questionHover").css('display', 'none');
			e.preventDefault();
			currentDot = null;
			currentDot = $(this);

			var offset = currentDot.position();

			lastX = e.pageX;
			lastY = e.pageY;

			dx = lastX - offset.left;
			dy = lastY - offset.top;

			$(document).on("mousemove", dotMoveHandler);
			$(document).on("mouseup", dotUpHandler);

			var intervalID = setInterval(updateDotLine, 40, $(this));
			currentDot.data("interval", intervalID);
		}

		function dotMoveHandler(e) {
			e.preventDefault();

			lastX = e.pageX;
			lastY = e.pageY;
			var x = lastX - dx;
			var y = lastY - dy + container.scrollTop();
			var parentOffset = currentDot.parent().offset();
			var containerOffset = container.offset();

			if (x + parentOffset.left - containerOffset.left < 0) {
				x = containerOffset.left - parentOffset.left;
			}
			if (y + parentOffset.top - containerOffset.top < 0) {
				y = containerOffset.top - parentOffset.top;
			}
			if ((x + parentOffset.left + currentDot.width()) > containerOffset.left + container.width()) {
				x = containerOffset.left + container.width() - parentOffset.left - currentDot.width();
			}
			if ((y + parentOffset.top + currentDot.height()) > containerOffset.top + container.height()) {
				y = containerOffset.top + container.height() - parentOffset.top - currentDot.height();
			}

			currentDot.css("left", x + "px");
			currentDot.css("top", y + "px");
		}

		function dotUpHandler(e) {
			$(".questionHover").css('display', 'block');
			e.preventDefault();
			dotMoveHandler(e);

			$(document).off("mousemove", dotMoveHandler);
			$(document).off("mouseup", dotUpHandler);

			// calc animation here //
			var newTarget = findNewParentForDot(currentDot);

			if (currentDot.data("parent").attr("practiceCorrect")) {
				if (currentDot.data("parent").attr("practiceCorrect") !== newTarget.attr("practiceCorrect")) {
					newTarget.parent().parent().addClass("redBg");

					var newPractice = newTarget;
					setTimeout(function() {
						newPractice.parent().parent().removeClass("redBg");
					}, 500);
					newTarget = currentDot.data("parent");
				}
			}

			currentDot.data("owner", newTarget);
			var pos = getCenterOf(newTarget);
			var oldParent = currentDot.parent();
			newTarget.append(currentDot);

			pos.left = newTarget.width() / 2 - Math.round(currentDot.width() / 2);
			pos.top = newTarget.height() / 2 - Math.round(currentDot.height() / 2);

			currentDot.css('left', currentDot.position().left + (oldParent.offset().left - newTarget.offset().left) + 'px');
			currentDot.css('top', currentDot.position().top + (oldParent.offset().top - newTarget.offset().top) + 'px');

			var thedot = currentDot;
			currentDot.animate({
				left: pos.left + "px",
				top: pos.top + "px"
			}, 400, function() {
				clearDotInterval(thedot);
				$(that).trigger('dotDrop');
			});

			currentDot = null;
		}

		// ===== //
		// UTILS //
		// ===== //

		function findNewParentForDot(dot) {
			var dotpos = getCenterOf(dot);

			var place;
			var placepos;
			var diff = 40;
			for (var i = 0; i < rightplace.length; i++) {
				place = $(rightplace[i]);
				placepos = getCenterOf(place);

				if ((Math.abs(placepos.top - dotpos.top) < diff) && (Math.abs(placepos.left - dotpos.left) < diff)) {
					if (testDotsConflict(dot, place)) {
						dot.data("linked", true);
						return place;
					}
				}
			}

			dot.data("linked", false);
			return dot.data("parent");
		}

		function updateDotLine(dot) {
			drawAllLines();
		}

		function testDotsConflict(dot, owner) {
			var alldots = container.find('.dot');
			var cdot;

			for (var i = 0; i < alldots.length; i++) {
				cdot = $(alldots[i]);
				if ((cdot.data("parent").data("id") == dot.data("parent").data("id")) && (cdot.data("owner").data("id") == owner.data("id") && (cdot.data("id") != dot.data("id")))) {
					return false;
				}
			}
			return true;
		}

		function clearDotInterval(dot) {
			clearInterval(dot.data("interval"));
		}

		function getCenterOf(elem) {
			var result = elem.offset();

			result.left = result.left + Math.round(elem.width() / 2) - container.offset().left;
			result.top = result.top + Math.round(elem.height() / 2) - container.offset().top;

			return result;
		}

		function innerResize() {
			canvas[0].width = container.width();
			canvas[0].height = container.height();
		}

		// ====== //
		// PUBLIC //
		// ====== //

		this.initPlaces = initPlaces;

		this.testCompleted = function() {
			var alldots = container.find('.dot');
			var cdot;

			for (var i = 0; i < alldots.length; i++) {
				cdot = $(alldots[i]);
				if (cdot.data("linked") == false) {
					return false;
				}
			}
			return true;
		};

		this.result = function() {
			var alldots = container.find('.dot');
			var cdot;

			var res = [];
			for (var i = 0; i < alldots.length; i++) {
				cdot = $(alldots[i]);
				var dotID = cdot.data('parent').data('id');
				if (!res[dotID]) res[dotID] = {};
				res[dotID][cdot.data('owner').data('id') - 100] = true;
			}

			return res;
		};
	}
})();

(function($) {
	var defaults = {
		columns: 4,
		classname: 'columnMultitest',
		min: 1
	};

	$.fn.autocolumnlist = function(params) {

		var options = $.extend({}, defaults, params);

		return this.each(function() {

			var els = $(this).find('.plank');
			var dimension = els.size();

			if (dimension > 0) {
				var elCol = Math.ceil(dimension / options.columns);
				if (elCol < options.min) {
					elCol = options.min;
				}
				var start = 0;
				var end = elCol;

				for (i = 0; i < options.columns; i++) {
					// Add "last" class for last columnMultitest
					if ((i + 1) == options.columns) {
						els.slice(start, end).wrapAll('<div class="' + options.classname + ' last" />');
					} else {
						els.slice(start, end).wrapAll('<div class="' + options.classname + '" />');
					}
					start = start + elCol;
					end = end + elCol;
				}
			}
		});
	};
})(jQuery);

(function($) {

	$.fn.shuffle = function() {

		var allElems = this.get(),
			getRandom = function(max) {
				return Math.floor(Math.random() * max);
			},
			shuffled = $.map(allElems, function() {
				var random = getRandom(allElems.length),
					randEl = $(allElems[random]).clone(true)[0];
				allElems.splice(random, 1);
				return randEl;
			});

		this.each(function(i) {
			$(this).replaceWith($(shuffled[i]));
		});

		return $(shuffled);

	};
})(jQuery);

function shuffle(o) { //v1.0
	for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
};



function imgResize(container) {
	// если меняется minsize Надо в css менять начальное значение plankimg на такое же
	var minSizeImg = 180;
	var planksImg = container.find('.plankImg');
	if (!planksImg.parent().hasClass("ABX")) {
		container.find(".ABX").data('owlCarousel').destroy();
		container.find(".ABX").css({
			"height": "100%",
			"width": "100%"
		})
	};
	var fontSize = container.closest('body').css('font-size').slice(0, -2);
	var padding = container.find('.plankImg').css('padding').slice(0, -2);
	var widthImg = planksImg.width();
	// var heightImg = planksImg.height();
	var plankLast = planksImg.last();
	var plankLastMargin = parseInt(plankLast.css('margin-bottom').slice(0, -2))
	container.bottom = container.offset().top + container.outerHeight(true);
	plankLastBottom = plankLast.offset().top + plankLast.outerHeight() + plankLastMargin;
	while (plankLastBottom < container.bottom) {
		// это плюсует размеры пока не упрется в максимальные
		widthImg += 10;
		heightImg = widthImg + 3 * fontSize;
		planksImg.css({
			'line-height': widthImg - padding * 2 + 'px',
			'width': widthImg + 'px',
			'height': heightImg + 'px'
		});
		plankLastBottom = plankLast.offset().top + plankLast.outerHeight() + plankLastMargin;
	}
	while (plankLastBottom > container.bottom && widthImg > minSizeImg) {
		// это минусует размеры пока не войдет в окно
		widthImg -= 10;
		heightImg = widthImg + 3 * fontSize;
		planksImg.css({
			'line-height': widthImg - padding * 2 + 'px',
			'width': widthImg + 'px',
			'height': heightImg + 'px'
		});
		plankLastBottom = plankLast.offset().top + plankLast.outerHeight() + plankLastMargin;
	}
	if (plankLastBottom > container.bottom && widthImg == minSizeImg) {
		owlSetSizes(container);
	};
}

function owlSetSizes(container) {
	// body...
	var row_middle = container.find('.row-middle');
	var row_top = container.find('.row-top');
	var contentContainer = container.find(".collapsibles-content");
	var plankContainer = container.find('.owl-carousel');
	var fontSize = container.closest('body').css('font-size').slice(0, -2);
	var widthOwlContainer = container.width() - 6 * fontSize;
	// var heightOwlContainer = container.outerHeight(true) - row_top.outerHeight(true);

	//asdasdasd fignya

	var padding = container.find('.plankImg').css('padding').slice(0, -2);
	var planksImg = container.find('.plankImg');
	var widthImg = 200;
	var heightImg = widthImg + 3 * fontSize;
	var plankLast = planksImg.last();
	// figna
	plankContainer.css({
		'width': widthOwlContainer + 'px'
			// 'height': heightOwlContainer + 'px'
	});
	planksImg.css({
		'line-height': widthImg - padding * 2 + 'px',
		'width': widthImg + 'px',
		'height': heightImg + 'px'
	});
	plankContainer.owlCarousel({
		items: 3,
		itemsCustom: false,
		itemsDesktop: false,
		itemsDesktopSmall: false,
		itemsTablet: false,
		itemsTabletSmall: false,
		itemsMobile: false,
		singleItem: false,
		itemsScaleUp: false,

		//Basic Speeds
		slideSpeed: 200,
		paginationSpeed: 800,
		rewindSpeed: 1000,

		//Autoplay
		autoPlay: false,
		stopOnHover: false,

		// Navigation
		navigation: true,
		navigationText: ["prev", "next"],
		rewindNav: true,
		scrollPerPage: false,

		//Pagination
		pagination: true,
		paginationNumbers: false,

		// Responsive 
		responsive: true,
		responsiveRefreshRate: 200,
		responsiveBaseWidth: window,

		// CSS Styles
		baseClass: "owl-carousel",
		theme: "owl-theme",

		//Lazy load
		lazyLoad: false,
		lazyFollow: true,
		lazyEffect: "fade",

		//Auto height
		autoHeight: false,

		//JSON 
		jsonPath: false,
		jsonSuccess: false,

		//Mouse Events
		dragBeforeAnimFinish: true,
		mouseDrag: true,
		touchDrag: true,

		//Transitions
		transitionStyle: false,

		// Other
		addClassActive: false,

		//Callbacks
		beforeUpdate: false,
		afterUpdate: false,
		beforeInit: false,
		afterInit: false,
		beforeMove: false,
		afterMove: false,
		afterAction: false,
		startDragging: false
	});
}


/**
 * Created by pankrashin.da on 12.11.2014.
 */
function imgSlideShowInit(container) {
	var wrapperBoard = container.find(".wrapperBoard");
	var footer = wrapperBoard.find(".footer");
	var titleImg = wrapperBoard.find(".titleImg");
	var boardTable = wrapperBoard.find('.boardTable');
	var board = wrapperBoard.find('.board');
	var imgSmall = container.find('.imgSmall');
	var imgHide = wrapperBoard.find('.imgHide');

	var nextArrowVK = wrapperBoard.find('.next');
	var prevArrowVK = wrapperBoard.find('.prev');
	//var exitVK = wrapperBoard.find('.exit');
	var currentImg = wrapperBoard.find(".imgHide").eq(0);
	var imgCurrentNumber = wrapperBoard.find(".imgCurrentNumber");
	var imgTotalNumber = wrapperBoard.find(".imgTotalNumber");
	var imgThere = wrapperBoard.find(".imgThere");
	imgTotalNumber.html("" + wrapperBoard.find('.imgHide').length);
	var imgId = "img1";
	//var currentImgTitle = "";


	imgSmall.on('click', function(event) {
		event.preventDefault();
		$.each(container.find('.imgHide'), function(index, val) {
			$(this).css('display', 'none');
		});
		imgId = $(this).attr("id");

		currentImg = wrapperBoard.find(".imgHide." + $(this).attr("id")).css('display', 'block');
		titleImg.html(currentImg.attr("titleImg"));

		imgCurrentNumber.html(imgHide.index(currentImg) + 1);
		container.find(".overlay").fadeIn('fast', function() {});
		checkImgHeight(currentImg);
	});

	wrapperBoard.find(".exit").on('click', function(event) {
		event.preventDefault();
		$(this).closest(".overlay").fadeOut('fast');
	});
	wrapperBoard.find(".footerExit").on('click', function(event) {
		event.preventDefault();
		$(this).closest(".overlay").fadeOut('fast');
	});

	nextArrowVK.on('click', function(event) {
		event.preventDefault();
		if (currentImg && currentImg.next() && currentImg.next().hasClass('imgHide')) {
			currentImg = currentImg.css('display', 'none').next().css('display', 'block');
			titleImg.html(currentImg.attr("titleImg"));
			imgCurrentNumber.html(parseInt(imgCurrentNumber.html()) + 1 + '');
		}
		checkImgHeight(currentImg);
	});

	imgThere.on('click', function(event) {
		event.preventDefault();
		if (currentImg && currentImg.next() && currentImg.next().hasClass('imgHide')) {
			currentImg = currentImg.css('display', 'none').next().css('display', 'block');
			titleImg.html(currentImg.attr("titleImg"));
			imgCurrentNumber.html(parseInt(imgCurrentNumber.html()) + 1 + '');
		}
		checkImgHeight(currentImg);
	});

	prevArrowVK.on('click', function(event) {
		event.preventDefault();
		if (currentImg && currentImg.prev() && currentImg.prev().hasClass('imgHide')) {
			currentImg = currentImg.css('display', 'none').prev().css('display', 'block');
			titleImg.html(currentImg.attr("titleImg"));
			imgCurrentNumber.html(imgCurrentNumber.html() - 1 + '');
		}
		checkImgHeight(currentImg);
	});

	function checkImgHeight(currentImg) {
		currentImg.css('width', "100%");
		if (boardTable.height() > board.height()) {
			console.log("resizeCheck");
			var maxImgHeight = board.height() - footer.height() - titleImg.height();
			var ImgNaturalHeightXWidth = currentImg[0].naturalWidth / currentImg[0].naturalHeight;
			var neededWidth = maxImgHeight * ImgNaturalHeightXWidth;
			currentImg.css('width', neededWidth);
			if (neededWidth > currentImg[0].naturalWidth) {
				currentImg.css('width', currentImg[0].naturalWidth);
			} else {
				currentImg.css('width', neededWidth);
			}
		};
	}

	// $(window).resize(function(event) {
	// 	checkImgHeight(currentImg);
	// });
}

function resizeTextBlocks(container) {
	var container = container;
	var planks = container.find(".plank");
	if (planks.parent().hasClass('columnMultitest')) {
		planks.unwrap();
	};
	container.find(".minPlank").removeClass("minPlank");
	container.find(".size100").removeClass("size100");
	container.find(".collapsed").removeClass("collapsed");

	var parentOffset = container.offset();
	var parentHeight = container.height();
	var parentBottom = parentOffset.top + parentHeight;
	var elemOffset = planks.last().offset();
	var row_middle = container.find('.row-middle');
	var row_top = container.find('.row-top');
	var contentContainer = container.find(".collapsibles-content");
	var plankContainer = container.find('.ABX');
	// var averageSize = getSizeClass(planks);
	elemOffset.bottom = elemOffset.top + planks.last().outerHeight(true);
	if (elemOffset.bottom > parentBottom && planks.size() > 3) {
		firstStepBuildColumns(container);
	}
	// в случае если после построения колонок все равно скролл

	if (elemOffset.bottom > parentBottom && planks.size() > 5) {
		secondStepManyColumns(container);
	}
	if (elemOffset.bottom > parentBottom && planks.size() > 5) {
		//init_collapsibles();
		// collapse_overpluses();
	}

	function firstStepBuildColumns(container) {
		switch (averageSizeClass) {
			case 'size0_25':
				var columnCount = 2;
				while (contentContainer.outerHeight() > container.outerHeight() && columnCount < 5) {
					if (columnCount === 4) {
						planks.addClass('size100');
					};
					buildColumns(columnCount);
					columnCount++;
				}
				break
			case 'size0_33':
				var columnCount = 2;
				while (contentContainer.outerHeight() > container.outerHeight() && columnCount < 4) {
					if (columnCount === 3) {
						planks.addClass('size100');
					};
					buildColumns(columnCount);
					columnCount++;
				}
				break
			case 'size0_5':
				buildColumns(2);
				break
			case 'size_1':
				break
			default:
				break
		}
	};

	function secondStepManyColumns(container) {
		if (planks.is(".size_1")) {
			return
		};

		var columns = container.find(".columnMultitest");
		var freeMax, bottomMax, freeColumn, targetColumn, heights, minHeight;
		// debugger
		while (contentContainer.outerHeight() > container.outerHeight()) {

			columnsFreespases = [];
			columnLastBottoms = [];
			columns.each(function(index, el) {
				var lastInColumn = $(this).children().last();
				var columnLastBottom = lastInColumn.offset().top + lastInColumn.outerHeight(true) - lastInColumn.css("margin-bottom").slice(0, -2);
				var freeSpaceColumn = parentBottom - columnLastBottom;

				$(this).data('columnLastBottom', columnLastBottom);
				$(this).data('freeSpaceColumn', freeSpaceColumn);

				columnsFreespases.push(freeSpaceColumn);
				columnLastBottoms.push(columnLastBottom);

			});
			freeMax = Math.max.apply(null, columnsFreespases);
			bottomMax = Math.max.apply(null, columnLastBottoms);

			freeColumn = columns.filter(function(index) {
				return $(this).data('freeSpaceColumn') === freeMax;
			}).eq(0);

			targetColumn = columns.filter(function(index) {
				return $(this).data('columnLastBottom') === bottomMax;
			}).eq(0);

			heights = [];
			targetColumn.children().each(function(index, el) {
				heights.push($(this).outerHeight(true));
			});

			minHeight = Math.min.apply(null, heights);

			if (freeColumn.data().freeSpaceColumn < minHeight) {
				return;
			}

			targetColumn.children().each(function(index, el) {
				if ($(this).outerHeight(true) === minHeight) $(this).addClass("minPlank");
			});

			targetColumn.find(".minPlank").first().detach().appendTo(freeColumn);

		}
	}

	function buildColumns(columnCount) {
		if (planks.parent().hasClass('columnMultitest')) {
			planks.unwrap();
		};
		plankContainer.autocolumnlist({
			columns: parseInt(columnCount),
			classname: 'columnMultitest'
		});
	}
}

function setClassesTextBlocks(container) {

	// var fontSizeForEm = $("body").css("font-size").substr(0, 2);
	// var heightOfPlank = container.find(".plank").height();
	// var shirinaOkna = container.find('.row-middle').width() - 32; // 32 это 2ем от 16px

	var planks = container.find(".plank");
	planks.removeClass("size0_25").removeClass("size0_33").removeClass("size0_5").removeClass("size_1");
	// var row_middle = container.find('.row-middle');
	// var row_top = container.find('.row-top');
	// var plankContainer = container.find('.ABX');
	var averageSize = getSizeClass(planks);
	// var averageSizeClass = "";
	// УСТАНАВЛИВАЕМ СРЕДНЕЕ ЗНАЧЕНИЕ ШИРИНЫ ПЛАШЕК ДЛЯ ПЛАШЕК
	setSizeClasses(planks, averageSize);
}


function getSizeClass(planks) {
	var fontSizeForEm = $("body").css("font-size").substr(0, 2);
	var sizesArray = [];
	var heightOfPlank = container.find(".plank").height();
	var sizesArraySumm = 0;
	$.each(planks, function(index, val) {
		/* iterate through array or object */
		if ($(this).width() < fontSizeForEm * 13.5) {
			sizesArray.push(0.25)
		} else if ($(this).width() < fontSizeForEm * 18) {
			sizesArray.push(0.33)
		} else if ($(this).width() < fontSizeForEm * 27) {
			sizesArray.push(0.5)
		} else if (($(this).width() < fontSizeForEm * 54 || $(this).width() === fontSizeForEm * 54) && $(this).height() === heightOfPlank) {
			sizesArray.push(1)
		} else {
			sizesArray.push(1)
		}
	});
	var sizesArraySumm = sizesArray.reduce(function(pv, cv) {
		return pv + cv;
	}, 0);
	var averageSize = sizesArraySumm / sizesArray.length;
	return averageSize;
}

function setSizeClasses(planks, averageSize) {
	if (averageSize < 0.33) {
		averageSizeClass = "size0_25"
	} else
	if (averageSize < 0.5) {
		averageSizeClass = "size0_33"
	} else
	if (averageSize < 1) {
		averageSizeClass = "size0_5"
	} else averageSizeClass = "size_1";

	planks.each(function(index, el) {
		$(this).addClass(averageSizeClass);
	});
}

/*
autor: Leo
*/
function initTouchEvents() {

	//$('body').css('-ms-touch-action', 'none');
	//$('body').css('touch-action', 'none');

	document.addEventListener("touchstart", touchHandler, true);
	document.addEventListener("touchmove", touchHandler, true);
	document.addEventListener("touchend", touchHandler, true);
	document.addEventListener("touchcancel", touchHandler, true);
	window.addEventListener("MSPointerDown", touchHandler, true);
	window.addEventListener("MSPointerMove", touchHandler, true);
	window.addEventListener("MSPointerUp", touchHandler, true);
	window.addEventListener("MSPointerCancel", touchHandler, true);
}

function uninitTouchEvents() {
	document.removeEventListener("touchstart", touchHandler, true);
	document.removeEventListener("touchmove", touchHandler, true);
	document.removeEventListener("touchend", touchHandler, true);
	document.removeEventListener("touchcancel", touchHandler, true);
	window.removeEventListener("MSPointerDown", touchHandler, true);
	window.removeEventListener("MSPointerMove", touchHandler, true);
	window.removeEventListener("MSPointerUp", touchHandler, true);
	window.removeEventListener("MSPointerCancel", touchHandler, true);
}

function touchHandler(event) {
	event.preventDefault();

	var touches = event.changedTouches;
	//var touches = event.touches;
	var first; // = touches[0];
	var type = "";

	switch (event.type) {
		case "touchstart":
			type = "mousedown";
			first = touches[0];
			break;
		case "touchmove":
			type = "mousemove";
			first = touches[0];
			break;
		case "touchend":
			type = "mouseup";
			first = touches[0];
			break;
		case "MSPointerDown":
			type = "mousedown";
			first = event;
			break;
		case "MSPointerMove":
			type = "mousemove";
			first = event;
			break;
		case "MSPointerUp":
			type = "mouseup";
			first = event;
			break;
		default:
			return;
	}

	// initMouseEvent(type, canBubble, cancelable, view, clickCount,
	//                screenX, screenY, clientX, clientY, ctrlKey,
	//                altKey, shiftKey, metaKey, button, relatedTarget);

	var simulatedEvent = document.createEvent("MouseEvent");
	simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0 /*left*/ , null);

	first.target.dispatchEvent(simulatedEvent);

	/*if((event.type == "touchend") || (event.type == "MSPointerUp")){
		var simulatedEvent2 = document.createEvent("MouseEvent");
		simulatedEvent2.initMouseEvent('click', true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left* /, null);
		
		first.target.dispatchEvent(simulatedEvent);
	}*/

}



$(window).resize(function() {
	// проверка для мультитеста container =~ question current;
	if ((typeof container) != 'undefined' && container != null) {
		switch (true) {
			case (container.find(".plankImg").size() != 0):
				imgResize(container);
				//checkImgHeight(currentImg);
				break
			case (container.find(".plankText").size() != 0):
				resizeTextBlocks(container);
				break
			default:
				// collapse_overpluses();
				break
		}
	}
});