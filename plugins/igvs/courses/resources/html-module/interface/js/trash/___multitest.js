$(function () {
	var questionTypes = {
		'radio': window.RadioButtons,
		'checkboxes': window.Checkboxes,
		'links': window.Links
	};
	
	var start = true;
	var startTime = new Date;

	var questionContainer = $('.questions');
	var questions = questionContainer.find('.question');

	var questionCount = 0;
	var questionNum = questions.length;
	var scores = 0;

	var indicators = multiplyIndicators($('.indicator'));
	var resultsDiv = $('.results');
	var results = new Results(resultsDiv);

	var currentQuestionDiv;
	var currentQuestion;
	var currentIndicator;

	questionContainer.addClass('active');

	var answerBtn = $('.answer');
	answerBtn.click(goToNextQuestion);
	goToNextQuestion();

	resultsDiv.find('#replay').click(repeat);

	function multiplyIndicators(indicator) {
		var container = indicator.parent();
		for (var i = 2; i <= questionNum; i++) {
			container.append(indicator.clone().html(i));
		}
		return container.children();
	}

	function goToNextQuestion() {
		if (!start) {
			var result = currentQuestion.getResult();
			if (result) currentIndicator.addClass('high');
			else currentIndicator.addClass('low');
			scores += result;

			currentQuestionDiv.removeClass('current');
			currentIndicator.removeClass('current');

			$(currentQuestion).off('complete').off('incomplete');
		}

		if (questionCount < questionNum) {
			currentQuestionDiv = questions.eq(questionCount);
			currentIndicator = indicators.eq(questionCount);
			currentQuestionDiv.addClass('current');
			currentIndicator.addClass('current');

			container = currentQuestionDiv.find('.collapsibles-container');
			init_collapsibles();
			currentQuestion = new questionTypes[currentQuestionDiv.attr('type')](currentQuestionDiv);

			answerBtn[0].disabled = true;
			answerBtn.addClass('disabled');
			$(currentQuestion).on('complete', function (e) {
				answerBtn[0].disabled = false;
				answerBtn.removeClass('disabled');
			});
			$(currentQuestion).on('incomplete', function (e) {
				answerBtn[0].disabled = true;
				answerBtn.addClass('disabled');
			});
		} else if (questionCount == questionNum) {
			showResult();
		}

		start = false;
		questionCount++;
	}

	function showResult() {
		container = null;
		init_collapsibles();
		answerBtn.css('display', 'none');
		questionContainer.removeClass('active');
		resultsDiv.addClass('active');

		var endTime = new Date;
		results.set(scores, questionNum - scores, formatTime(Math.round((endTime.getTime() - startTime.getTime()) / 1000)));

		saveResult(startTime, endTime);
	}

	function repeat() {
		reset();
		resultsDiv.removeClass('active');
		questionContainer.addClass('active');
		answerBtn.css('display', 'inline-block');
		startTime = new Date;
		goToNextQuestion();
	}

	function reset() {
		indicators.removeClass('high').removeClass('low');
		questionCount = 0;
		scores = 0;
		start = true;
	}

	function saveResult(startTime, endTime) {
		var app = window.parent.app;

		var date = endTime.getDate() + '.' + (endTime.getMonth() + 1) + '.' + endTime.getFullYear();
		app.addJournal(JSON.stringify([app.getEappID(), date, getTime(endTime), getTime(startTime), questionNum, scores]));
	}

	function getTime(date) {
		return date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
	}

	function formatTime(seconds) {
		var hours = Math.floor(seconds / 3600);
		var minutes = Math.floor((seconds % 3600) / 60);
		return formatAs2Digits(hours) + ':' + formatAs2Digits(minutes) + ':' + formatAs2Digits(seconds % 60);
	}

	function formatAs2Digits(n) {
		return ('00' + n).substr(-2);
	}
});
