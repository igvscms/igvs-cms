/**
 * Created by kravchenko.as on 28.01.14.
 */
(function () {
	function RadioButtons(container){
		this.optionContainer = container.find('.RADIOBUTTONS');
		var that = this;

		var options = this.optionContainer.find('input');
		options.click(function(){
			$(that).trigger('complete');
			options.off('click');
		});

		this.getResult = function(){
			return getResult(that);
		};
	}

	function getResult(that){
		var options = that.optionContainer.find('input');
		for (var i = 0; i < options.length; i++) {
			if (options[i].checked != (options[i].getAttribute('checkIt') == 'true')) {
				return 0;
			}
		}
		return 1;
	}

	RadioButtons.prototype.update = function (data) {
		var html = '';
		var options = data.options;
		for (var i = 0; i < options.length; i++) {
			html += '<li><input id="item' + i + '" value="rad' + i + '" type="radio" name="radio" checkIt=' + options[i]['checked'] + '><label for="item' + i + '">' + options[i]['text'] + '</label></li>';
		}
		this.optionContainer.innerHTML = html;
		document.getElementById('description').innerHTML = data.task;
	};

	window.RadioButtons = RadioButtons;
})();
