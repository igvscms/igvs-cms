/**
 * Created by kravchenko.as on 29.01.14.
 */
(function(){

	function Results(container){
		this.container = container;
		//this.diagram = container.find('#donutchart');
		this.percentField = container.find('#percent');
		this.rightAnswers = container.find('#rightAnswers');
		this.wrongAnswers = container.find('#wrongAnswers');
		this.answersAmount = container.find('#answersAmount');
		this.spentTimeField = container.find('#timeSpent');
	}

	Results.prototype.set = function(correctNum, incorrectNum, spentTime){
		var totalNum = correctNum + incorrectNum;
		this.rightAnswers.html(correctNum);
		this.wrongAnswers.html(incorrectNum);
		this.answersAmount.html(totalNum);

		var percent = parseInt(correctNum / totalNum * 10000) / 100;
		this.percentField.html(percent + '%');
		//this.diagram.attr('data-text', percent + '%');
		//this.diagram.attr('data-percent', percent);

		this.spentTimeField.html(spentTime);

		//this.animate();
	};

	/*Results.prototype.animate = function(options){
		this.diagram.circliful(options);
	};*/

	window.Results = Results;
})();