
var container;
var content;

var collapsibles;
var proportionals;

$( window ).resize( function() {
	if (( typeof container ) != 'undefined' && container != null ) { collapse_overpluses() }
});

$( function() {
	
	$( document ).ready( function() {
		
		var body = $( 'body' );
		
		$( '.link-anchor' ).click( function() {
			body.mCustomScrollbar( 'scrollTo', $( this ).attr( 'href' ) )
		});
		
		var inline_images = $( '.inline-image' );
		inline_images.each( function() {
			var e = $( this );
			e.load( function() { e.css( 'height', e.height()/10 + 'em' ) });
		});
		
		var temp = body.append( '<div style="position: fixed; top: 0; left: -200%; visibility: hidden;"/>' );
		var outline_images = $( '.outline-image' );
		outline_images.each( function() {
			var place = $( this );
			var e = place.find( 'img' );
			temp.append( e );
			e.load( function() { e.css( 'width', e.width()/10 + 'em' ); place.prepend( e ) });
		});
		
		body.mCustomScrollbar({
			autoDraggerLength: true,
			scrollInertia: 800,
			mouseWheelPixels: 300,
			advanced:{
				updateOnBrowserResize: true,
				updateOnContentResize: true
			}
		});
		
		setTimeout( function() {
			var tooltips = $( '.link-tooltip' );
			tooltips.attr( 'title', function() { return $( $( this ).attr( 'href' ) ).html() } );
			tooltips.tooltipster({
				theme: '.tooltip',
				maxWidth: 500,
				trigger: 'click'
			});
			
		}, 2500 );
		
	});
		
});

function init_collapsibles() {
	
	if (( typeof container ) != 'undefined' && container != null ) {
		
		content = $( container ).find( '.collapsibles-content' ); 

		init_proportionals();

		collapsibles = container.find('.collapsible' );
		collapsibles.find( '.loupe' ).remove();
		var loupes = $( '<a class="loupe" />' ).appendTo( collapsibles );
		
		collapse_overpluses();

		loupes.click( function() {
			var parent = $( this ).parent();
			var direction = 'to-left';
			var right = Math.floor( container.width()*7/10 );
			var left = -right;
			if ( $( this ).parent().hasClass( 'from-left' ) ) {
				direction = 'to-right';
				left = Math.floor( container.width()*7/10 );
				right = -left;
			}
			var string = '';
			var text = $( this ).parent().find( '.text' ).html();
			var image = $( this ).parent().find( '.image' ).html();
			if (( typeof text ) != 'undefined' && text != null ) string += text;
			if (( typeof image ) != 'undefined' && image != null ) string += image;
			container.attr( 'style', 'position: absolute' );
			parent.addClass( 'pressed' );
			container.animate({ left: left, right: right }, 300, function() {
				container.removeAttr( 'style' );
				container.addClass( 'minimized' ).addClass( direction );
				var zoomback = $( '<div class="zoomback"/>' ).appendTo( container );
				zoomback.css( 'opacity', 0 );
				zoomback.animate({ 'opacity': 1 }, 300, function() { zoomback.removeAttr( 'style' ) });
				var expanded = $( '<div class="expanded"/>' ).appendTo( container.parent() );
				expanded.css( 'opacity', 0 );
				expanded.html( '<div class="expanded-table"><div class="expanded-cell"><div class="substrate">' + string + '</div></div></div>' );
				expanded.mCustomScrollbar({
					autoDraggerLength: true,
					scrollInertia: 800,
					mouseWheelPixels: 300,
					advanced:{
						updateOnBrowserResize: true,
						updateOnContentResize: true
					}
				});
				expanded.animate({ 'opacity': 1 }, 300, function() { expanded.removeAttr( 'style' ) });
				zoomback.click( function() {
					var zb = $( this );
					expanded.animate({ 'opacity': 0 }, 300, function() {
						expanded.remove();
						container.animate({ right: 0, left: 0 }, 300, function() {
							container.removeAttr( 'style' );
							container.removeClass( 'minimized' ).removeClass( direction );
							parent.removeClass( 'pressed' );
							zb.remove();
						});
					});
				});
			});
		});
		
	}
	
}
function collapse_overpluses() {
	
	if (( typeof container ) != 'undefined' && container != null ) {
		
		collapse_proportionals();

		collapsibles.removeClass( 'collapsed' );
		var collapsed = 0;
		while ( ( ( container.offset().top + container.height()) < ( content.offset().top + content.height() ) ) && collapsibles.length > collapsed ) {
			max = collapsibles[0];
			collapsibles.each( function() { if ( $( this ).height() >= $( max ).height() ) max = $( this );	})
			$( max ).addClass( 'collapsed' );
			collapsed++
		}
	}

}


function init_proportionals() {
	
	/*var inline_images = $( '.inline-image' );
	inline_images.each( function() {
		var e = $( this );
		e.load( function() { e.css( 'height', e.height()/10 + 'em' ) });
	});*/
	
	proportionals = $( content ).find( '.outline-image' );
	collapse_proportionals();
	//alert( proportionals );
	/*proportionals.each( function() {
		var e = $( this ).find( 'img' );
		e.load( function() { e.css( 'width', e.width()/10 + 'em' ) });
	});*/
	
}
function collapse_proportionals() {

	proportionals.each( function() {
		var e = $( this );
		var image = e.find( 'img' );
		if ( image.width() > e.width() ) {
			e.parent().parent().addClass( 'x-collapsed' );
		} else {
			e.parent().parent().removeClass( 'x-collapsed' );
		}
	});

}

