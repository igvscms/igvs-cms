/**
 * Created with WebStorm.
 * User: Alex
 * Date: 20.11.13
 * Time: 22:25
 * To change this template use File | Settings | File Templates.
 */


(function () {
	//var rightAnswers; = [{1:true, 2:true}, {0:true}, {2:true}, {3:true}, {4:true}];
	window.Links = Links;

	function Links(container) {
		this.container = container;
		this.lnks = new LinkCreator(container.find('.LINKS'));

		var that = this;
		$(this.lnks).on('dotDrop', function(){
			if (that.lnks.testCompleted()) {
				$(that).trigger('complete');
			} else {
				$(that).trigger('incomplete');
			}
		});

		this.getResult = function(){
			return getResult(container, that.lnks.result());
		};
	}

	Links.prototype.update = function (data) {
		var leftData = data.leftFields;
		var rightData = data.rightFields;
		var leftHtml = '';
		var rightHtml = '';
		for (var i in  leftData) {
			if (!leftData.hasOwnProperty(i)) continue;
			var cons = leftData[i].cons;
			leftHtml += '<div class="plank"><div class="plank-content"><div class="collapsible left"><div class="text">' + leftData[i].text + '</div></div></div><div class="plank-dotplace"><div class="plank-dotplace-IN"><div class="dotplace source" dots="' + cons.split(' ').length + '" cons="' + cons + '"></div></div></div></div>';
		}
		for (var j in rightData) {
			if (!rightData.hasOwnProperty(j)) continue;
			rightHtml += '<div class="plank"><div class="plank-dotplace"><div class="plank-dotplace-IN"><div class="dotplace target"></div></div></div><div class="plank-content"><div class="collapsible right"><div class="text">' + rightData[j] + '</div></div></div></div>';
		}
		$('.sources').html(leftHtml);
		$('.targets').html(rightHtml);

		this.lnks = new LinkCreator(this.container.find('.LINKS'));
	};

	function getResult(container, results) {
		var success = true;
		var rightAnswers = readRightAnswers(container);
		loop:
			for (var key in rightAnswers) {
				if (!rightAnswers.hasOwnProperty(key)) continue;
				for (var id in rightAnswers[key]) {
					if (!rightAnswers[key].hasOwnProperty(id)) continue;
					if (!results[key][id]) {
						success = false;
						break loop;
					}
				}
			}

		return Number(success);
	}

	function readRightAnswers(container) {
		var result = [];
		container.find('.source > .dotplace-IN').each(function (i, elem) {
			var numbers = $(elem).attr('cons').split(' ');
			var answers = {};
			numbers.forEach(function (item, index, array) {
				answers[parseInt(item) - 1] = true;
			});
			result.push(answers);
		});
		return result;
	}

	function LinkCreator(container) {
		var that = this;

		initTouchEvents();// init touch only if is REALLY needed
		container.find('.dot').remove();

		var canvas = container.find('.draw');
		if (!canvas.length)
			canvas = $('<canvas class="draw"></canvas>').prependTo(container);

		var leftplace = container.find('.source > .dotplace-IN');
		var rightplace = container.find('.target > .dotplace-IN');

		var draw;

		if (canvas[0].getContext) {
			draw = canvas[0].getContext("2d");
			innerResize();
			drawAllLines();
			draw.clearRect(0, 0, canvas[0].width, canvas[0].height);
		} else {
			alert("canvas недоступен ):");
		}

		$(window).resize(function () {
			innerResize();
			drawAllLines();
		});

		initPlaces();

		function initPlaces() {
			var place;
			for (var i = 0; i < leftplace.length; i++) {
				place = $(leftplace[i]);
				place.data("id", i);
				place.css("z-index", "2");
				createDots(place);
			}
			for (var i = 0; i < rightplace.length; i++) {
				place = $(rightplace[i]);
				place.data("id", 100 + i);
				place.css("z-index", "2");
			}
		}

		function createDots(place) {
			var dotAmount = parseInt(place.attr("dots"));
			var dot;
			var placepos = place.position();
			placepos.left = placepos.left + place.width() / 2;
			placepos.top = placepos.top + place.height() / 2;
			for (var j = 0; j < dotAmount; j++) {
				dot = $('<div class="dot"></div>').appendTo(place);
				dot.css("z-index", "4");
				dot.css("left", (place.width() / 2 - dot.width() / 2) + "px");

				dot.data("parent", place);
				dot.data("owner", place);
				dot.data("interval", 0);
				dot.data("id", place.data("id") + "_" + j);
				dot.data("linked", false);

				dot.css("top", (place.height() / 2 - dot.height() / 2) + "px");
				dot.on("mousedown", dotDownHandler);
			}
		}

		function drawAllLines() {
			draw.clearRect(0, 0, canvas[0].width, canvas[0].height);
			var alldots = container.find('.dot');
			var cdot;

			for (var i = 0; i < alldots.length; i++) {
				cdot = $(alldots[i]);
				drawLine(cdot);
			}
		}

		function drawLine(dot) {
			var start = getCenterOf(dot.data("parent"));
			var end = getCenterOf(dot);

			draw.beginPath();
			draw.moveTo(start.left, start.top);
			draw.lineTo(end.left, end.top);
			draw.lineCap = 'round';
			draw.strokeStyle = 'rgb(0,100,200)';
			draw.lineWidth = 4;
			draw.stroke();
		}

		// ========= //
		// MOVE DOTS //
		// ========= //

		var currentDot = null;
		var dx = 0;
		var dy = 0;

		var lastX = -1;
		var lastY = -1;

		function dotDownHandler(e) {
			e.preventDefault();
			currentDot = null;
			currentDot = $(this);

			var offset = currentDot.position();

			lastX = e.pageX;
			lastY = e.pageY;

			dx = lastX - offset.left;
			dy = lastY - offset.top;

			$(document).on("mousemove", dotMoveHandler);
			$(document).on("mouseup", dotUpHandler);

			var intervalID = setInterval(updateDotLine, 20, $(this));
			currentDot.data("interval", intervalID);
		}

		function dotMoveHandler(e) {
			e.preventDefault();

			lastX = e.pageX;
			lastY = e.pageY;
			var x = lastX - dx;
			var y = lastY - dy + container.scrollTop();
			var parentOffset = currentDot.parent().offset();
			var containerOffset = container.offset();

			if (x + parentOffset.left - containerOffset.left < 0) {
				x = containerOffset.left - parentOffset.left;
			}
			if (y + parentOffset.top - containerOffset.top < 0) {
				y = containerOffset.top - parentOffset.top;
			}
			if ((x + parentOffset.left + currentDot.width()) > containerOffset.left + container.width()) {
				x = containerOffset.left + container.width() - parentOffset.left - currentDot.width();
			}
			if ((y + parentOffset.top + currentDot.height()) > containerOffset.top + container.height()) {
				y = containerOffset.top + container.height() - parentOffset.top - currentDot.height();
			}

			currentDot.css("left", x + "px");
			currentDot.css("top", y + "px");
		}

		function dotUpHandler(e) {
			e.preventDefault();
			dotMoveHandler(e);

			$(document).off("mousemove", dotMoveHandler);
			$(document).off("mouseup", dotUpHandler);

			// calc animation here //
			var newTarget = findNewParentForDot(currentDot);

			currentDot.data("owner", newTarget);
			var pos = getCenterOf(newTarget);
			var oldParent = currentDot.parent();
			newTarget.append(currentDot);

			pos.left = newTarget.width() / 2 - Math.round(currentDot.width() / 2);
			pos.top = newTarget.height() / 2 - Math.round(currentDot.height() / 2);

			currentDot.css('left', currentDot.position().left + (oldParent.offset().left - newTarget.offset().left) + 'px');
			currentDot.css('top', currentDot.position().top + (oldParent.offset().top - newTarget.offset().top) + 'px');

			var thedot = currentDot;
			currentDot.animate({left: pos.left + "px", top: pos.top + "px"}, 400, function () {
				clearDotInterval(thedot);
				$(that).trigger('dotDrop');
			});

			currentDot = null;
		}

		// ===== //
		// UTILS //
		// ===== //

		function findNewParentForDot(dot) {
			var dotpos = getCenterOf(dot);

			var place;
			var placepos;
			var diff = 40;
			for (var i = 0; i < rightplace.length; i++) {
				place = $(rightplace[i]);
				placepos = getCenterOf(place);

				if ((Math.abs(placepos.top - dotpos.top) < diff) && (Math.abs(placepos.left - dotpos.left) < diff)) {
					if (testDotsConflict(dot, place)) {
						dot.data("linked", true);
						return place;
					}
				}
			}

			dot.data("linked", false);
			return dot.data("parent");
		}

		function updateDotLine(dot) {
			drawAllLines();
		}

		function testDotsConflict(dot, owner) {
			var alldots = container.find('.dot');
			var cdot;

			for (var i = 0; i < alldots.length; i++) {
				cdot = $(alldots[i]);
				if ((cdot.data("parent").data("id") == dot.data("parent").data("id")) && (cdot.data("owner").data("id") == owner.data("id") && (cdot.data("id") != dot.data("id")))) {
					return false;
				}
			}
			return true;
		}

		function clearDotInterval(dot) {
			clearInterval(dot.data("interval"));
		}

		function getCenterOf(elem) {
			var result = elem.offset();

			result.left = result.left + Math.round(elem.width() / 2) - container.offset().left;
			result.top = result.top + Math.round(elem.height() / 2) - container.offset().top;

			return result;
		}

		function innerResize() {
			canvas[0].width = container.width();
			canvas[0].height = container.height();
		}

		// ====== //
		// PUBLIC //
		// ====== //

		this.initPlaces = initPlaces;

		this.testCompleted = function () {
			var alldots = container.find('.dot');
			var cdot;

			for (var i = 0; i < alldots.length; i++) {
				cdot = $(alldots[i]);
				if (cdot.data("linked") == false) {
					return false;
				}
			}
			return true;
		};

		this.result = function () {
			var alldots = container.find('.dot');
			var cdot;

			var res = [];
			for (var i = 0; i < alldots.length; i++) {
				cdot = $(alldots[i]);
				var dotID = cdot.data('parent').data('id');
				if (!res[dotID]) res[dotID] = {};
				res[dotID][cdot.data('owner').data('id') - 100] = true;
			}

			return res;
		};
	}
})();