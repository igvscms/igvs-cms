$(document).ready(function() {
	multitestus();
})

function multitestus() {
	var questionTypes = {
		'links': window.Links
	};
	var start = true;
	var startTime = new Date;
	start_timer();


	var questionContainer = $('.questions');

	var questions = questionContainer.find('.question');

	var questionCount = 0;
	var questionNum = questions.length;
	var scores = 0;

	var indicators = multiplyIndicators($('.indicator'));
	var resultsDiv = $('.resultWrapper');
	var results = new Results(resultsDiv);

	var currentQuestionDiv, currentQuestion, currentIndicator;

	questionContainer.addClass('active');

	var answerBtn = $('.answer');
	answerBtn.on("click", practicaGoToNextQuestion);
	answerBtn[0].addEventListener('touchend', practicaGoToNextQuestion, false)
	function practicaGoToNextQuestion () {
		// debugger
		var dotsNew = $(".dot");
		var temporaryVariable = true;
		dotsNew.each(function  () {
			var current_dot = $(this);
			if(current_dot.data('parent').attr('answerRight') != current_dot.parent().attr('answerRight')) {
				temporaryVariable = false;

				answerBtn[0].disabled = true;
				answerBtn.addClass('disabled');
				// $(this).parent().parent().parent().parent().css('display', "none");
				current_dot.parent().parent().parent().next().addClass("wrongPractica");
				current_dot.data('parent').parent().parent().prev().addClass("wrongPractica");
				function  repeatLinks() {
					// debugger
					current_dot.parent().parent().parent().next().removeClass("wrongPractica");
					current_dot.data('parent').parent().parent().prev().removeClass("wrongPractica");
					current_dot.appendTo(current_dot.data("parent"));
					currentQuestion.updateLines(current_dot);

				}
				
				setTimeout(repeatLinks, 1000);

				// debugger
			}

		});
		if (temporaryVariable == true) {goToNextQuestion();};
	}

	goToNextQuestion();


	resultsDiv.find('#replay').on("click", repeat);
	resultsDiv.find('#replay')[0].addEventListener('touchend', repeat, false)

	function multiplyIndicators(indicator) {
		var container = indicator.parent();
		for (var i = 2; i <= questionNum; i++) {
			container.append(indicator.clone().html(i));
		}
		return container.children();
	}

	function goToNextQuestion() {
		answerBtn[0].disabled = true;
		answerBtn.addClass('disabled');
		if (!start) {
			var result = currentQuestion.getResult();
			if (result) currentIndicator.addClass('high');
			else currentIndicator.addClass('low');
			scores += result;
				currentQuestionDiv.removeClass('current').css("display", "none");
				currentIndicator.removeClass('current');

				$(currentQuestion).off('complete').off('incomplete');
				if (questionCount < questionNum) {
					currentQuestionDiv = questions.eq(questionCount);
					currentIndicator = indicators.eq(questionCount);
					currentQuestionDiv.addClass('current').css("display", "block").css("left", "0");
					currentIndicator.addClass('current');

					container = currentQuestionDiv.find('.collapsibles-container');
					init_collapsibles();

					currentQuestion = new questionTypes[currentQuestionDiv.attr('type')](currentQuestionDiv);

					answerBtn[0].disabled = true;
					answerBtn.addClass('disabled');
					$(currentQuestion).on('complete', function(e) {
						answerBtn[0].disabled = false;
						answerBtn.removeClass('disabled');
					});
					$(currentQuestion).on('incomplete', function(e) {
						answerBtn[0].disabled = true;
						answerBtn.addClass('disabled');
					});
					questionCount++;
				} else if (questionCount == questionNum) {
					showResult();
				}
		}

		if (questionCount < questionNum && questionCount == 0) {
			currentQuestionDiv = questions.eq(questionCount);
			currentIndicator = indicators.eq(questionCount);
			currentQuestionDiv.addClass('current').css("display", "block").css("left", "0");
			currentIndicator.addClass('current');

			container = currentQuestionDiv.find('.collapsibles-container');
			init_collapsibles();

			currentQuestion = new questionTypes[currentQuestionDiv.attr('type')](currentQuestionDiv);

			answerBtn[0].disabled = true;
			answerBtn.addClass('disabled');
			$(currentQuestion).on('complete', function(e) {
				answerBtn[0].disabled = false;
				answerBtn.removeClass('disabled');
			});
			$(currentQuestion).on('incomplete', function(e) {
				answerBtn[0].disabled = true;
				answerBtn.addClass('disabled');
			});
			questionCount++;
		} else if (questionCount == questionNum && questionCount == 0) {
			showResult();
		}

		start = false;
	}

	function showResult() {
		stop_timer();
		var endTime = new Date;
		$(".multitestInnerWrapper").css('display', 'none');
		container = null;
		answerBtn.css('display', 'none');
		questionContainer.removeClass('active');
		resultsDiv.addClass('active');
		results.set(scores, questionNum - scores, formatTime(Math.round((endTime.getTime() - startTime.getTime()) / 1000)));
		// saveResult(startTime, endTime);
	}

	function repeat() {
		reset();
		resultsDiv.removeClass('active');
		questionContainer.addClass('active');
		answerBtn.css('display', 'inline-block');
		$(".multitestInnerWrapper").css("display", "table");
		startTime = new Date;
		$.each($(".ABX"), function() {
			if ($(this).find(".plank").parent().hasClass("column")) {
				$(this).find(".plank").unwrap();
			}
			$(this).find(".plank").shuffle();
		});
		start_timer();
		goToNextQuestion();
	}

	function reset() {
		currentQuestionDiv.css("display", "none");
		indicators.removeClass('high').removeClass('low');
		questionCount = 0;
		scores = 0;
		start = true;
	}

	function saveResult(startTime, endTime) {
		var app = window.parent.app;
		var date = endTime.getDate() + '.' + (endTime.getMonth() + 1) + '.' + endTime.getFullYear();
		app.addJournal(JSON.stringify([app.getEappID(), date, getTime(endTime), getTime(startTime), questionNum, scores]));
	}

	function getTime(date) {
		return date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
	}

	function formatTime(seconds) {
		var hours = Math.floor(seconds / 3600);
		var minutes = Math.floor((seconds % 3600) / 60);
		// return formatAs2Digits(hours) + ':' + formatAs2Digits(minutes) + ':' + formatAs2Digits(seconds % 60);
		return formatAs2Digits(minutes) + ':' + formatAs2Digits(seconds % 60);
	}

	function formatAs2Digits(n) {
		return ('00' + n).substr(-2);
	}
	var timer;

	function simple_timer(sec, block, direction, stop) {
		var time = sec;
		direction = direction || false;
		var minutes = parseInt(time / 60);
		if (minutes < 1) minutes = 0;
		time = parseInt(time - minutes * 60);
		if (minutes < 10) minutes = '0' + minutes;

		var seconds = time;
		if (seconds < 10) seconds = '0' + seconds;
		block.text(minutes + ':' + seconds);
		if (!stop) {
			if (direction) {
				sec++;
				timer = setTimeout(function() {
					simple_timer(sec, block, direction);
				}, 1000);
			} else {
				sec--;
				if (sec > 0) {
					setTimeout(function() {
						simple_timer(sec, block, direction);
					}, 1000);
				} else {
					alert('Время вышло!');
				}
			}
		}
	}

	function start_timer() {
		var block = $('.timer');
		simple_timer(0, block, true);
	}

	function stop_timer() {
		clearTimeout(timer);
	}

};

/* RESULTS */

/**
 * Created by kravchenko.as on 29.01.14.
 */
(function() {

	function Results(container) {
		this.container = container;
		//this.diagram = container.find('#donutchart');
		this.percentField = container.find('#percent');
		this.rightAnswers = container.find('#rightAnswers');
		this.wrongAnswers = container.find('#wrongAnswers');
		this.answersAmount = container.find('#answersAmount');
		this.timeSpent = container.find('#timeSpent');
		this.doneTest = container.find('.done').find("h3");
	}

	Results.prototype.set = function(correctNum, incorrectNum, timeSpent) {
		var totalNum = correctNum + incorrectNum;
		this.rightAnswers.html(correctNum);
		this.wrongAnswers.html(incorrectNum);
		this.answersAmount.html(totalNum);
		this.timeSpent.html(timeSpent);
		var testDoneOk = "TASK COMPLETED!";
		var testNotDone = "TASK FAILED!";

		var percent = (correctNum / totalNum * 100).toFixed(0) ;
		if (percent > 70 || percent === 70) {this.doneTest.html(testDoneOk);};
		if (percent < 70 ) {this.doneTest.html(testNotDone);};
		this.percentField.html(percent+ "%");
		// var percent = parseInt(correctNum / totalNum * 10000) / 100;
		// this.percentField.html(percent + '%');
		//this.diagram.attr('data-text', percent + '%');
		//this.diagram.attr('data-percent', percent);

		//this.animate();
	};

	/*Results.prototype.animate = function(options){
	 this.diagram.circliful(options);
	 };*/

	window.Results = Results;
})();



/* LINKS */

/**
 * Created with WebStorm.
 * User: Alex
 * Date: 20.11.13
 * Time: 22:25
 * To change this template use File | Settings | File Templates.
 */

(function() {
	window.Links = Links;

	function Links(container) {
		this.container = container;
		this.lnks = new LinkCreator(container.find('.LINKS'));

		var that = this;
		$(this.lnks).on('dotDrop', function() {
			if (that.lnks.testCompleted()) {
				$(that).trigger('complete');
			} else {
				$(that).trigger('incomplete');
			}
		});

		this.getResult = function() {
			return getResult(container, that.lnks.result());
		};
	}

	Links.prototype.update = function(data) {
		var leftData = data.leftFields;
		var rightData = data.rightFields;
		var leftHtml = '';
		var rightHtml = '';
		for (var i in leftData) {
			if (!leftData.hasOwnProperty(i)) continue;
			var cons = leftData[i].cons;
			leftHtml += '<div class="plank"><div class="plank-content"><div class="collapsible left"><div class="text">' + leftData[i].text + '</div></div></div><div class="plank-dotplace"><div class="plank-dotplace-IN"><div class="dotplace source" dots="' + cons.split(' ').length + '" cons="' + cons + '"></div></div></div></div>';
		}
		for (var j in rightData) {
			if (!rightData.hasOwnProperty(j)) continue;
			rightHtml += '<div class="plank"><div class="plank-dotplace"><div class="plank-dotplace-IN"><div class="dotplace target"></div></div></div><div class="plank-content"><div class="collapsible right"><div class="text">' + rightData[j] + '</div></div></div></div>';
		}
		$('.sources').html(leftHtml);
		$('.targets').html(rightHtml);

		this.lnks = new LinkCreator(this.container.find('.LINKS'));
	};

	Links.prototype.updateLines = function (current_dot) {
		this.lnks.updateLines(current_dot);
	};

	function getResult(container, results) {
		var success = true;
		var rightAnswers = readRightAnswers(container);
		loop: for (var key in rightAnswers) {
			if (!rightAnswers.hasOwnProperty(key)) continue;
			for (var id in rightAnswers[key]) {
				if (!rightAnswers[key].hasOwnProperty(id)) continue;
				if (!results[key][id]) {
					success = false;
					break loop;
				}
			}
		}

		return Number(success);
	}

	function readRightAnswers(container) {
		var result = [];
		container.find('.source > .dotplace-IN').each(function(i, elem) {
			var numbers = $(elem).attr('cons').split(' ');
			var answers = {};
			numbers.forEach(function(item, index, array) {
				answers[parseInt(item) - 1] = true;
			});
			result.push(answers);
		});
		return result;
	}

	function LinkCreator(container) {
		var that = this;

		initTouchEvents(); // init touch only if is REALLY needed
		container.find('.dot').remove();

		var canvas = container.find('.draw');
		if (!canvas.length)
			canvas = $('<canvas class="draw"></canvas>').prependTo(container);

		var leftplace = container.find('.source > .dotplace-IN');
		var rightplace = container.find('.target > .dotplace-IN');

		var draw;

		if (canvas[0].getContext) {
			draw = canvas[0].getContext("2d");
			innerResize();
			drawAllLines();
			draw.clearRect(0, 0, canvas[0].width, canvas[0].height);
		} else {
			alert("canvas недоступен ):");
		}

		$(window).resize(function() {
			innerResize();
			drawAllLines();
		});

		initPlaces();

		function initPlaces() {
			var place;
			for (var i = 0; i < leftplace.length; i++) {
				place = $(leftplace[i]);
				place.data("id", i);
				place.css("z-index", "2");
				createDots(place);
			}
			for (var i = 0; i < rightplace.length; i++) {
				place = $(rightplace[i]);
				place.data("id", 100 + i);
				place.css("z-index", "2");
			}
		}

		function createDots(place) {
			var dotAmount = parseInt(place.attr("dots"));
			var dot;
			var placepos = place.position();
			placepos.left = placepos.left + place.width() / 2;
			placepos.top = placepos.top + place.height() / 2;
			for (var j = 0; j < dotAmount; j++) {
				dot = $('<div class="dot"></div>').appendTo(place);
				dot.css("z-index", "4");
				dot.css("left", (place.width() / 2 - dot.width() / 2) + "px");

				dot.data("parent", place);
				dot.data("owner", place);
				dot.data("interval", 0);
				dot.data("id", place.data("id") + "_" + j);
				dot.data("linked", false);

				dot.css("top", (place.height() / 2 - dot.height() / 2) + "px");
				dot.on("mousedown", dotDownHandler);
			}
		}

		function drawAllLines() {
			draw.clearRect(0, 0, canvas[0].width, canvas[0].height);
			var alldots = container.find('.dot');
			var cdot;

			for (var i = 0; i < alldots.length; i++) {
				cdot = $(alldots[i]);
				drawLine(cdot);
			}
		}

		this.updateLines = function(current_dot){
			drawAllLines();
			// current_dot()
			current_dot.data("linked", false);
			// var alldots = container.find('.dot');
			// var cdot;

			// for (var i = 0; i < alldots.length; i++) {
			// 	cdot = $(alldots[i]);
			// 	if (cdot.data("linked") == false) {
			// 		return false;
			// 	}
			// }

		};


		function drawLine(dot) {
			var start = getCenterOf(dot.data("parent"));
			var end = getCenterOf(dot);

			draw.beginPath();
			draw.moveTo(start.left, start.top);
			draw.lineTo(end.left, end.top);
			draw.lineCap = 'round';
			draw.strokeStyle = 'rgb(47,105,142)';
			draw.lineWidth = 4;
			draw.stroke();
		}

		// ========= //
		// MOVE DOTS //
		// ========= //

		var currentDot = null;
		var dx = 0;
		var dy = 0;

		var lastX = -1;
		var lastY = -1;

		function dotDownHandler(e) {
			e.preventDefault();
			currentDot = null;
			currentDot = $(this);

			var offset = currentDot.position();

			lastX = e.pageX;
			lastY = e.pageY;

			dx = lastX - offset.left;
			dy = lastY - offset.top;

			$(document).on("mousemove", dotMoveHandler);
			$(document).on("mouseup", dotUpHandler);

			var intervalID = setInterval(updateDotLine, 20, $(this));
			currentDot.data("interval", intervalID);
		}

		function dotMoveHandler(e) {
			e.preventDefault();

			lastX = e.pageX;
			lastY = e.pageY;
			var x = lastX - dx;
			var y = lastY - dy + container.scrollTop();
			var parentOffset = currentDot.parent().offset();
			var containerOffset = container.offset();

			if (x + parentOffset.left - containerOffset.left < 0) {
				x = containerOffset.left - parentOffset.left;
			}
			if (y + parentOffset.top - containerOffset.top < 0) {
				y = containerOffset.top - parentOffset.top;
			}
			if ((x + parentOffset.left + currentDot.width()) > containerOffset.left + container.width()) {
				x = containerOffset.left + container.width() - parentOffset.left - currentDot.width();
			}
			if ((y + parentOffset.top + currentDot.height()) > containerOffset.top + container.height()) {
				y = containerOffset.top + container.height() - parentOffset.top - currentDot.height();
			}

			currentDot.css("left", x + "px");
			currentDot.css("top", y + "px");
		}

		function dotUpHandler(e) {
			e.preventDefault();
			dotMoveHandler(e);

			$(document).off("mousemove", dotMoveHandler);
			$(document).off("mouseup", dotUpHandler);

			// calc animation here //
			var newTarget = findNewParentForDot(currentDot);

			currentDot.data("owner", newTarget);
			var pos = getCenterOf(newTarget);
			var oldParent = currentDot.parent();
			newTarget.append(currentDot);

			pos.left = newTarget.width() / 2 - Math.round(currentDot.width() / 2);
			pos.top = newTarget.height() / 2 - Math.round(currentDot.height() / 2);

			currentDot.css('left', currentDot.position().left + (oldParent.offset().left - newTarget.offset().left) + 'px');
			currentDot.css('top', currentDot.position().top + (oldParent.offset().top - newTarget.offset().top) + 'px');

			var thedot = currentDot;
			currentDot.animate({
				left: pos.left + "px",
				top: pos.top + "px"
			}, 400, function() {
				clearDotInterval(thedot);
				$(that).trigger('dotDrop');
			});

			currentDot = null;
		}

		// ===== //
		// UTILS //
		// ===== //

		function findNewParentForDot(dot) {
			var dotpos = getCenterOf(dot);

			var place;
			var placepos;
			var diff = 40;
			for (var i = 0; i < rightplace.length; i++) {
				place = $(rightplace[i]);
				placepos = getCenterOf(place);

				if ((Math.abs(placepos.top - dotpos.top) < diff) && (Math.abs(placepos.left - dotpos.left) < diff)) {
					if (testDotsConflict(dot, place)) {
						dot.data("linked", true);
						return place;
					}
				}
			}

			dot.data("linked", false);
			return dot.data("parent");
		}

		function updateDotLine(dot) {
			drawAllLines();
		}

		function testDotsConflict(dot, owner) {
			var alldots = container.find('.dot');
			var cdot;

			for (var i = 0; i < alldots.length; i++) {
				cdot = $(alldots[i]);
				if ((cdot.data("parent").data("id") == dot.data("parent").data("id")) && (cdot.data("owner").data("id") == owner.data("id") && (cdot.data("id") != dot.data("id")))) {
					return false;
				}
			}
			return true;
		}

		function clearDotInterval(dot) {
			clearInterval(dot.data("interval"));
		}

		function getCenterOf(elem) {
			var result = elem.offset();

			result.left = result.left + Math.round(elem.width() / 2) - container.offset().left;
			result.top = result.top + Math.round(elem.height() / 2) - container.offset().top;

			return result;
		}

		function innerResize() {
			canvas[0].width = container.width();
			canvas[0].height = container.height();
		}

		// ====== //
		// PUBLIC //
		// ====== //

		this.initPlaces = initPlaces;

		this.testCompleted = function() {
			var alldots = container.find('.dot');
			var cdot;

			for (var i = 0; i < alldots.length; i++) {
				cdot = $(alldots[i]);
				if (cdot.data("linked") == false) {
					return false;
				}
			}
			return true;
		};

		this.result = function() {
			var alldots = container.find('.dot');
			var cdot;

			var res = [];
			for (var i = 0; i < alldots.length; i++) {
				cdot = $(alldots[i]);
				var dotID = cdot.data('parent').data('id');
				if (!res[dotID]) res[dotID] = {};
				res[dotID][cdot.data('owner').data('id') - 100] = true;
			}

			return res;
		};
	}
})();


/*
autor: Leo
*/
function initTouchEvents()
{
	
	//$('body').css('-ms-touch-action', 'none');
	//$('body').css('touch-action', 'none');
	
	document.addEventListener("touchstart", touchHandler, true);
	document.addEventListener("touchmove", touchHandler, true);
	document.addEventListener("touchend", touchHandler, true);
	document.addEventListener("touchcancel", touchHandler, true);
	window.addEventListener("MSPointerDown", touchHandler, true);
	window.addEventListener("MSPointerMove", touchHandler, true);
	window.addEventListener("MSPointerUp", touchHandler, true);
	window.addEventListener("MSPointerCancel", touchHandler, true);
}

function uninitTouchEvents()
{
	document.removeEventListener("touchstart", touchHandler, true);
	document.removeEventListener("touchmove", touchHandler, true);
	document.removeEventListener("touchend", touchHandler, true);
	document.removeEventListener("touchcancel", touchHandler, true);
	window.removeEventListener("MSPointerDown", touchHandler, true);
	window.removeEventListener("MSPointerMove", touchHandler, true);
	window.removeEventListener("MSPointerUp", touchHandler, true);
	window.removeEventListener("MSPointerCancel", touchHandler, true);
}

function touchHandler(event)
{	
	event.preventDefault();

	var touches = event.changedTouches;
	//var touches = event.touches;
	var first;// = touches[0];
	var type = "";

		switch(event.type)
	{
		case "touchstart": type = "mousedown"; first = touches[0]; break;
		case "touchmove":  type="mousemove"; first = touches[0]; break;        
		case "touchend":   type="mouseup"; first = touches[0]; break;
		case "MSPointerDown": type = "mousedown"; first = event; break;
		case "MSPointerMove":  type="mousemove"; first = event; break;        
		case "MSPointerUp":   type="mouseup"; first = event; break;
		default: return;
	}

	// initMouseEvent(type, canBubble, cancelable, view, clickCount,
	//                screenX, screenY, clientX, clientY, ctrlKey,
	//                altKey, shiftKey, metaKey, button, relatedTarget);

	var simulatedEvent = document.createEvent("MouseEvent");
	simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
	
	first.target.dispatchEvent(simulatedEvent);
	
	/*if((event.type == "touchend") || (event.type == "MSPointerUp")){
		var simulatedEvent2 = document.createEvent("MouseEvent");
		simulatedEvent2.initMouseEvent('click', true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left* /, null);
		
		first.target.dispatchEvent(simulatedEvent);
	}*/

}