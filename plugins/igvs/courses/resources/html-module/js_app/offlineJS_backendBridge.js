/**
 * Created with PyCharm.
 * User: Alex
 * Date: 22.12.2014
 * Time: 20:05
 * To change this template use File | Settings | File Templates.
 */

(function () {
	var srcData = {};

	window.backendBridge = {
		init: function (ent) {
			var dir = window.location.search.replace("?dir=", "") || "tmp";
			var jsonDir = "content/" + dir + "/db/js_db.json";
			$.getJSON(jsonDir, function(json){
				srcData = json;
				srcData.config.dir = dir;
				ent(window.backendBridge.getConfig());
			});
		},

		getConfig: function () {
			return srcData.config;
		},

		login: function (login, password, completeCallback) {

		},

		regMe: function (login, password, fio, completeCallback) {

		},

		openWorkPage: function (contentDir) {

		},

		getLanguagePack: function (lang, completeCallback) {
			completeCallback(srcData.languagePack);
		},

		getNavigation: function (completeCallback) {
			completeCallback(srcData.nav);
		},

		setLastViewedPage: function (type, parent, id, completeCallback) {

		},

		getLastViewedPage: function (completeCallback) {
			completeCallback(srcData.lastViewedPage);
		},

		saveUISettings: function (theme, interfaceFontSize, contentFontSize, font, completeCallback) {

		},

		getUserSettings: function (completeCallback) {
			completeCallback(null);
		},

		getWorkmenu: function (completeCallback) {
			completeCallback(srcData.workMenu);
		},

		clearWorkmenu: function (completeCallback) {
			completeCallback && completeCallback();
		},

		inBookmarks: function (pageID, completeCallback) {
			completeCallback(false);
		},

		appendBookmark: function (pageID, completeCallback) {
			completeCallback && completeCallback();
		},

		removeBookmark: function (pageID, completeCallback) {
			completeCallback && completeCallback();
		},

		addNote: function (pageID, text, completeCallback) {
			completeCallback && completeCallback();
		},

		getNote: function (pageID, completeCallback) {
			completeCallback('');
		},

		delNote: function (pageID, completeCallback) {
			completeCallback && completeCallback();
		},

		saveSel: function (pageID, htmlText, completeCallback) {
			completeCallback && completeCallback();
		},

		recSel: function (pageID, classNum, completeCallback) {
			completeCallback && completeCallback();
		},

		delRecSel: function (pageID, classNum, completeCallback) {
			completeCallback && completeCallback();
		},

		delSelections: function (pageID, completeCallback) {
			completeCallback && completeCallback();
		},

		getSelection: function (pageID, completeCallback) {
			completeCallback();
		},

		delHighlightComment: function (pageID, num, completeCallback) {
			completeCallback && completeCallback();
		},

		saveHighlightComment: function (pageID, classNum, comment, completeCallback) {
			completeCallback && completeCallback();
		},

		getHighlightComment: function (pageID, num, completeCallback) {
			completeCallback();
		},

		getJournal: function (completeCallback) {
			completeCallback([]);
		},

		addJournalRec: function (rec, completeCallback) {
			completeCallback();
		},

		clearJournal: function (completeCallback) {
			completeCallback();
		},

		getHrefs: function (completeCallback) {
			completeCallback(srcData.hrefs);
		},

		getResources: function (completeCallback) {
			completeCallback();
		},

		getVocabulary: function (completeCallback) {
			completeCallback(srcData.vocabulary);
		},

		openUrl: function (url) {

		},

		printWorkContents: function () {

		},

		printContentPage: function (pageID) {

		},

		html2printer: function () {

		},

		initSearch: function (fileData, completeCallback) {
			completeCallback && completeCallback();
		},

		find: function (phrase, completeCallback) {
			completeCallback();
		},

		saveExcel: function () {

		},

		checkForUpdate: function () {

		}
	};
})();