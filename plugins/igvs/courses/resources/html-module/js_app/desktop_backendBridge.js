/**
 * Created with PyCharm.
 * User: Alex
 * Date: 07.10.2014
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 */

(function () {
	var shellObj = window.shellObj;
	
	window.backendBridge = {
		login: function(login, password, completeCallback){
			completeCallback(shellObj.login(login, password));
		},
		
		regMe: function(login, password, fio, completeCallback){
			completeCallback(shellObj.regMe(login, password, fio));
		},

		openWorkPage: function(contentDir){
			shellObj.openWorkPage(contentDir);
		},

		getLanguagePack: function (lang, completeCallback) {
			completeCallback(JSON.parse(shellObj.getLanguagePack(lang)));
		},

		getNavigation: function (completeCallback) {
			completeCallback(JSON.parse(shellObj.getNavi()));
		},

		setLastViewedPage: function(type, parent, id, completeCallback){
			shellObj.setLastViewedPage(type, parent, id);
			completeCallback && completeCallback();
		},

		getLastViewedPage: function(completeCallback){
			completeCallback(JSON.parse(shellObj.getLastViewedPage()));
		},

		saveUISettings: function(theme, interfaceFontSize, contentFontSize, font, completeCallback){
			shellObj.saveUISettings(theme, interfaceFontSize, contentFontSize, font);
			completeCallback && completeCallback();
		},

		getUserSettings: function(completeCallback){
			completeCallback(JSON.parse(shellObj.getUserSettings()));
		},

		getWorkmenu: function(completeCallback){
			completeCallback(JSON.parse(shellObj.getWorkmenu()));
		},

		clearWorkmenu: function(completeCallback){
			shellObj.clearWorkmenu();
			completeCallback && completeCallback();
		},

		inBookmarks: function(pageID, completeCallback){
			completeCallback(JSON.parse(shellObj.inBookmarks(pageID)));
		},

		appendBookmark: function(pageID, completeCallback){
			shellObj.appendBookmark(pageID);
			completeCallback && completeCallback();
		},

		removeBookmark: function(pageID, completeCallback){
			shellObj.removeBookmark(pageID);
			completeCallback && completeCallback();
		},

		addNote: function(pageID, text, completeCallback){
			shellObj.addNote(pageID, text);
			completeCallback && completeCallback();
		},

		getNote: function(pageID, completeCallback){
			completeCallback(shellObj.getNote(pageID));
		},

		delNote: function(pageID, completeCallback){
			shellObj.delNote(pageID);
			completeCallback && completeCallback();
		},

		saveSel: function(pageID, htmlText, completeCallback){
			shellObj.saveSel(pageID, htmlText);
			completeCallback && completeCallback();
		},

		recSel: function(pageID, classNum, selection, completeCallback){
			shellObj.recSel(pageID, classNum, selection);
			completeCallback && completeCallback();
		},

		delRecSel: function(pageID, classNum, completeCallback){
			shellObj.delRecSel(pageID, classNum);
			completeCallback && completeCallback();
		},

		delSelections: function(pageID, completeCallback){
			shellObj.delSelections(pageID);
			completeCallback && completeCallback();
		},

		getSelection: function(pageID, completeCallback){
			completeCallback(shellObj.getSelection(pageID));
		},

		delHighlightComment: function(pageID, num, completeCallback){
			shellObj.delHighlightComment(pageID, num);
			completeCallback && completeCallback();
		},

		saveHighlightComment: function(pageID, classNum, comment, selection, completeCallback){
			shellObj.saveHighlightComment(pageID, classNum, comment, selection);
			completeCallback && completeCallback();
		},

		getHighlightComment: function(pageID, num, completeCallback){
			completeCallback(shellObj.getHighlightComment(pageID, num));
		},

		getJournal: function(completeCallback){
			completeCallback(JSON.parse(shellObj.getJournal()));
		},

		addJournalRec: function(rec, completeCallback){
			shellObj.addJournal(rec);
			completeCallback();
		},

		clearJournal: function(completeCallback){
			shellObj.delJournal();
			completeCallback();
		},

		getHrefs: function(completeCallback){
			completeCallback(JSON.parse(shellObj.getHrefs()));
		},

		getResources: function(completeCallback){
			completeCallback(JSON.parse(shellObj.getResources()));
		},

		getVocabulary: function(completeCallback){
			completeCallback(JSON.parse(shellObj.getVocabulary()));
		},

		openUrl: function(url){
			shellObj.openUrl(url);
		},

		printWorkContents: function(){
			shellObj.print_work_contents();
		},

		printContentPage: function(pageID){
			shellObj.printContentPage(pageID);
		},

		html2printer: function(){
			shellObj.html2printer();
		},

		initSearch: function(fileData, completeCallback){
			shellObj.initSearch(fileData);
			completeCallback && completeCallback();
		},

		find: function(phrase, completeCallback){
			completeCallback(JSON.parse(shellObj.find(phrase)));
		},

		saveExcel: function(){
			shellObj.saveExcel();
		},

		checkForUpdate: function(){
			shellObj.checkForUpdate();
		},

		doUpdate: function(){
			shellObj.doUpdate();
		},

		openTutorial: function(tutorial){
			shellObj.openFile('html/' + path + '/tutor/' + tutorial + '.pdf');
		}
	};
})();