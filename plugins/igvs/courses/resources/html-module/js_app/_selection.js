function bindReady(handler) {
	var called = false;

	function ready() { // (1)
		if (called) return;
		called = true;
		handler()
	}

	if (document.addEventListener) { // (2)
		document.addEventListener("DOMContentLoaded", function () {
			ready()
		}, false)
	} else if (document.attachEvent) {  // (3)
		// (3.1)
		if (document.documentElement.doScroll && window == window.top) {
			function tryScroll() {
				if (called) return;
				if (!document.body) return;
				try {
					document.documentElement.doScroll("left");
					ready()
				} catch (e) {
					setTimeout(tryScroll, 0)
				}
			}

			tryScroll()
		}
		// (3.2)
		document.attachEvent("onreadystatechange", function () {
			if (document.readyState === "complete") {
				ready()
			}
		})
	}
	// (4)
	if (window.addEventListener)
		window.addEventListener('load', ready, false);
	else if (window.attachEvent)
		window.attachEvent('onload', ready);
	/*  else  // (4.1)
	 window.onload=ready
	 */
}

readyList = [];

function onReady(handler) {
	if (!readyList.length) {
		bindReady(function () {
			for (var i = 0; i < readyList.length; i++) {
				readyList[i]()
			}
		})
	}
	readyList.push(handler)
}

$(function () {
	rangy.init();
	selOn("#content");
});

function addClass(o, c) {
	var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g");
	if (re.test(o.className)) return;
	o.className = (o.className + " " + c).replace(/\s+/g, " ").replace(/(^ | $)/g, "")
}

function removeClass(o, c) {
	var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g");
	o.className = o.className.replace(re, "$1").replace(/\s+/g, " ").replace(/(^ | $)/g, "")
}

function selOn(selector) {
	var container = (selector[0] == "#") ? document.getElementById(selector.substring(1, selector.length)) : document.getElementsByClassName(selector.substring(1, selector.length))[0] || "Invalid Selector";
	
	$(window).resize(function(){
		if(!window['contextmenu']) return;
		if(parseInt(contextmenu.style.left) + contextmenu.clientWidth > container.clientWidth) contextmenu.style.left = (container.clientWidth - contextmenu.clientWidth) + 'px';
	});

	window.addConListener = function (e) {
		contMenu = document.getElementById("contextmenu");
		if (contMenu) contMenu.remove();
		if (window.stopFlag == false) {
			contextmenu = document.createElement("div");
			contextmenu.target = e.currentTarget || e.toElement;
			contextmenu.setAttribute("id","contextmenu");
			contextmenu.innerHTML = "<ul><li id=\"copyBut\">Копировать</li><li id=\"comBut\">Комментировать</li><li id=\"delBut\">Удалить</li></ul>";
			contextmenu.style.zIndex = "99999";
			contextmenu.style.position = "absolute";
			contextmenu.style.fontWeight = "normal";
			contextmenu.style.fontSize = "18px";
			contextmenu.style.fontStyle = "normal";
			contextmenu.style.marginLeft = "0px";
			contextmenu.style.marginTop = "0px";
			container.appendChild(contextmenu);
			var contextMenuWidth = contextmenu.clientWidth;
			container.removeChild(contextmenu);

			var x = e.pageX;
			if(x + contextMenuWidth > container.clientWidth) x = container.clientWidth - contextMenuWidth;
			var y = Math.max(e.pageY, lastMousePos.y) + 30;

			contextmenu.style.left = x + "px";
			contextmenu.style.top = y + "px";
			container.appendChild(contextmenu)
		}
	};
	sels = document.getElementsByClassName("selection");
	for (i = 0; i < sels.length; i++) {
		if (sels[i].onclick == null) {
			sels[i].addEventListener('click', function (e) {
				rDoc = window.parent.parent.document;
				// if (rDoc.getElementById("highlight-note")) {rDoc.getElementById("highlight-note").remove()}
				addConListener(e);
				e.preventDefault();
			}, false);
		}
	}

	document.onmousedown = function (e) {
		window.stopFlag = e.toElement.parentNode.parentNode.id == "contextmenu";

		if (e.toElement.id == "saveBut") {
			conMenu = document.getElementById("contextmenu");
			if (conMenu) conMenu.remove();

			maxNum = -1;
			var sels = document.getElementsByClassName("selection");
			for (i = 0; i < sels.length; i++) {
				if (!(sels[i].classList.contains("notSaved"))) {
					curCl = sels[i].classList[1];
					if (parseInt(curCl.substring(1, curCl.length)) > maxNum) maxNum = parseInt(curCl.substring(1, curCl.length));
				}
			}
			newNum = maxNum + 1;
			if (!newNum) newNum++;
			var highlighter = rangy.createHighlighter();
			highlighter.addClassApplier(rangy.createCssClassApplier("selection n" + newNum.toString()));
			highlighter.highlightSelection("selection n" + newNum.toString());
			rangy.getSelection().removeAllRanges();

			sels = document.getElementsByClassName("selection");
			for (i = 0; i < sels.length; i++) {
				if (sels[i].onclick == null) {
					sels[i].addEventListener('click', function (e) {
						// rDoc = window.parent.parent.document
						// if (rDoc.getElementById("highlight-note")) {rDoc.getElementById("highlight-note").remove()}
						addConListener(e);
						e.preventDefault();
					}, false);
				}
			}

			window.parent.parent.app.recSel("n" + newNum.toString());
			window.parent.parent.app.saveSel(document.getElementById("content").innerHTML)

		} else if (e.toElement.id == "delSel") {
			window.getSelection().removeAllRanges();
			conMenu = document.getElementById("contextmenu");
			if (conMenu) conMenu.remove()
		} else if (e.toElement.id == "copyBut") {
			var selected = contextmenu.target;
			conMenu = document.getElementById("contextmenu");
			if (conMenu) conMenu.remove();
			if (window.getSelection().toString().length) {
				window.parent.parent.app.copyToClipboard();
				rangy.getSelection().removeAllRanges();
			} else {
				var nodes = document.getElementsByClassName(selected.classList[1]);
				var copyNode = document.createElement("div");
				copyNode.id = "copyNode";
				for (var p = 0; p < nodes.length; p++) {
					copyNode.appendChild(nodes[p].cloneNode(true));
				}
				document.getElementsByTagName("body")[0].appendChild(copyNode);
				var sel = rangy.getSelection();
				var range = rangy.createRange();
				range.selectNode(copyNode);
				sel.setSingleRange(range);
				window.parent.parent.app.copyToClipboard();
				copyNode.remove();
				sel.removeAllRanges();
			}
		} else if (e.toElement.id == "delBut") {
			var selected = contextmenu.target;
			var selector = selected.classList[1];
			var contMenu = document.getElementById("contextmenu");
			if (contMenu) contMenu.remove();
			rangy.getSelection().removeAllRanges();
			while (document.getElementsByClassName(selector).length > 0) {
				var tags = document.getElementsByClassName(selector);
				for (var i = 0; i < tags.length; i++) {
					tags[i].outerHTML = tags[i].innerHTML
				}
			}

			window.parent.parent.app.delRecSel(selector);
			window.parent.parent.app.saveSel(document.getElementById("content").innerHTML)

		} else if (e.toElement.id == "comBut") {

			var sel = rangy.getSelection();
			if (window.getSelection().toString().length == 0) {
				selected = contextmenu.target;
				classNum = selected.classList[1]
			} else {
				maxNum = -1;
				var sels = document.getElementsByClassName("selection");
				for (i = 0; i < sels.length; i++) {
					if (!(sels[i].classList.contains("notSaved"))) {
						curCl = sels[i].classList[1];
						if (parseInt(curCl.substring(1, curCl.length)) > maxNum) maxNum = parseInt(curCl.substring(1, curCl.length))
					}
				}
				newNum = maxNum + 1;
				if (!newNum) newNum++;
				var highlighter = rangy.createHighlighter();
				highlighter.addClassApplier(rangy.createCssClassApplier("notSaved selection n" + newNum.toString()));
				highlighter.highlightSelection("notSaved selection n" + newNum.toString());
				window.getSelection().removeAllRanges();
				classNum = "n" + newNum.toString()
			}
			conMenu = document.getElementById("contextmenu");
			window.parent.parent.app.addHighlightComment(classNum, contextmenu.style.left);
			if (conMenu) conMenu.remove()

		} else if (e.toElement.id != "comBut" && e.toElement.id != "saveBut" && e.toElement.id != "delBut") {
			window.getSelection().removeAllRanges();
			conMenu = document.getElementById("contextmenu");
			if (conMenu) conMenu.remove();
			rDoc = window.parent.parent.document;
			if (rDoc.getElementById("highlight-note")) {
				rDoc.getElementById("highlight-note").remove()
			}

			tags = document.getElementsByClassName("notSaved");
			for (i = 0; i < tags.length; i++) {
				tags[i].outerHTML = tags[i].innerHTML
			}
		}

	};
	
	var lastMousePos = null;
	container.onmousedown = function (e) {
		window.blockSel = !!e.toElement.classList.contains("selection");
		lastMousePos = {
			x: e.pageX,
			y: e.pageY
		};
	};
	
	container.onmouseup = function (e) {
		if (e.button == 0 && window.blockSel == false && window.stopFlag == false) {
			var selection = window.getSelection();

			var ourRange;
			if (selection.rangeCount > 0) {
				ourRange = selection.getRangeAt(selection.rangeCount - 1);
			} else {
				return;
			}
			
			// if (ourRange.endContainer.parentElement.classList[0] == "selection" || ourRange.endContainer.parentElement.parentElement.classList[0] == "selection") {noIns = false} else {noIns = true}
			if (ourRange.toString().length > 0) {
				rangy.getSelection().expand("word", {
					trim: true,
					wordOptions: {
						wordRegex: /[\n\s\.,;:a-z0-9]+(['\-][a-z0-9\s]+)*/gi
					}
				});
				// var selections = document.getElementsByClassName("selection")
				var contMenu = document.getElementById("contextmenu");
				if (contMenu) contMenu.remove();

				if (window.stopFlag == false) {
					var contextmenu = document.createElement("div");
					contextmenu.setAttribute("id", "contextmenu");
					contextmenu.innerHTML = "<ul><li id=\"copyBut\">Копировать</li><li id=\"saveBut\">Выделить</li><li id=\"comBut\">Комментировать</li></ul>";
					contextmenu.style.zIndex = "99999";
					contextmenu.style.position = "absolute";
					contextmenu.style.fontWeight = "normal";
					contextmenu.style.fontSize = "18px";
					contextmenu.style.fontStyle = "normal";
					contextmenu.style.marginLeft = "0px";
					contextmenu.style.marginTop = "0px";
					container.appendChild(contextmenu);
					var contextMenuWidth = contextmenu.clientWidth;
					container.removeChild(contextmenu);
					
					var x = e.pageX;
					if(x + contextMenuWidth > container.clientWidth) x = container.clientWidth - contextMenuWidth;
					var y = Math.max(e.pageY, lastMousePos.y) + 30;
					
					contextmenu.style.left = x + "px";
					contextmenu.style.top = y + "px";
					container.appendChild(contextmenu);
				}
			}
		} else if (window.stopFlag == false) {
			rangy.getSelection().removeAllRanges()
		}
	}
}