<?php namespace Igvs\Courses\Updates;

use Schema;
use DB;
use Illuminate\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CoursesLauncheDateNullable extends Migration
{
    public function up()
    {
        DB::statement('ALTER TABLE igvs_courses_courses MODIFY launch_date timestamp null');
    }

    public function down()
    {
    }
}