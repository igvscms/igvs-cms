<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCategories_1_0_36 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_categories', function($table) {
            \Db::statement('ALTER TABLE igvs_courses_categories CHANGE nest_left old_nest_left INT DEFAULT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories CHANGE nest_right old_nest_right INT DEFAULT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories CHANGE nest_depth old_nest_depth INT DEFAULT NULL');
            $table->integer('sort')->unsigned()->default(0);
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->dropColumn('sort');
        });
    }
}