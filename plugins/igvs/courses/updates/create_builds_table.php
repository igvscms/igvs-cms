<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateBuildsTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_courses_builds', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->text('description');
            $table->boolean('show_all');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_builds');
    }

}
