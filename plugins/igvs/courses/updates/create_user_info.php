<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class CreateUserInfo extends Migration {
    public function up() {
        \Schema::create('igvs_courses_user_info', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('code')->unique();
            $table->binary('text');
        });
    }
    public function down() {
        \Schema::dropIfExists('igvs_courses_user_info');
    }
}