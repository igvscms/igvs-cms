<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePublicationStatesTable_1_1_1 extends Migration
{
    public function up()
    {
        Schema::create('igvs_courses_publication_states', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('row_type', 50);

            $table->integer('course_id')->index();
            $table->integer('topic_id')->index()->nullable()->default(null);
            $table->integer('act_id')->index()->nullable()->default(null);
            $table->integer('module_template_id')->index()->nullable()->default(null);
            $table->string('module_content_id')->index()->nullable()->default(null); //because practice can be id+'a' (assessment)

            $table->string('course_code', 30);
            $table->string('topic_code', 50)->nullable()->default(null);
            $table->string('module_content_code', 50)->nullable()->default(null);

            $table->boolean('topic_is_separator')->nullable()->default(null);

            $table->string('course_name');
            $table->string('topic_name')->nullable()->default(null);
            $table->string('act_name')->nullable()->default(null);
            $table->string('module_template_name')->nullable()->default(null);
            $table->string('module_content_name')->nullable()->default(null);
            $table->string('behavior')->nullable()->default(null);

            $table->integer('is_active_publication')->default(1);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_publication_states');
    }
}
