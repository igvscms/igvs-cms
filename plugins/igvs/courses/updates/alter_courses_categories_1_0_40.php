<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCategories_1_0_40 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_categories', function($table) {
            $table->text('unit_title')
                ->after('code')
                ->default('');
            $table->text('qcf_ref')
                ->after('code')
                ->default('');
            $table->text('nos_ref')
                ->after('code')
                ->default('');
            $table->text('awarding_body_code')
                ->after('code')
                ->default('');
            $table->text('level')
                ->after('code')
                ->default('');
            $table->text('credits_qualification_title')
                ->after('code')
                ->default('');
            $table->text('learning_outcome')
                ->after('code')
                ->default('');
            $table->text('functional_skills')
                ->after('code')
                ->default('');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_categories', function($table) {
             $table->dropColumn('unit_title');
             $table->dropColumn('qcf_ref');
             $table->dropColumn('nos_ref');
             $table->dropColumn('awarding_body_code');
             $table->dropColumn('level');
             $table->dropColumn('credits_qualification_title');
             $table->dropColumn('learning_outcome');
             $table->dropColumn('functional_skills');
        });
    }
}