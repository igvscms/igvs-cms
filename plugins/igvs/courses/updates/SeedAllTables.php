<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Constructor;
use Igvs\Courses\Models\ConstructorVersion;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\ModuleVersion;
use Igvs\Courses\Models\ModuleTheme;

class SeedAllTables extends Seeder
{
    use \Igvs\Courses\Traits\FileHelper;

    public function run()
    {
        $this->seedModulesTemplates();
    }

    public function seedModulesTemplates()
    {
        $path = Config::get('igvs.courses::modulesRelease.path') . '/modules';
        $dir_modules = $this->scandir($path, ['.gitignore', '.git']);

        $reorder = $output = [];
        foreach ($dir_modules as $dir_module) {

            $attr = ['path' => $dir_module];
            $module = Module::where($attr)->withTrashed()->first() ?:
                new Module($attr);
            $module->save();

            $dir_versions = $this->scandir("{$path}/{$dir_module}");

            $lastVersion = 0;
            $module->name = $module->name ?: $module->path;
            $module->name_ru = isset($module->name_rus) ? $module->name_rus : $module->path;
            foreach ($dir_versions as $dir_version) {

                try {

                    $file = "{$path}/{$dir_module}/{$dir_version}/metadata.json";

                    if (!file_exists($file))
                        continue;

                    if ( ! ($metadata = json_decode(file_get_contents($file))) )
                        continue;

                    $require_metadata_params = [
                        'version',
                        'status',
                        'release_notes',
                        'developer',
                        'developer_comment',
                        'behaviors_types',
                    ];

                    $require_metadata_params_errors = [];

                    foreach ($require_metadata_params as $require_metadata_param) {
                        if (!isset($metadata->{$require_metadata_param})) {
                            $require_metadata_params_errors[] = $require_metadata_param;
                        }
                    }

                    //todo раскоментить это, когда js-ки поправят developmant_comment в новом модуле
                    /*if (count($require_metadata_params_errors)) {
                        throw new \Exception('Module template haven\'t require(s) field(s): ' . implode(',', $require_metadata_params_errors)
                            . "\r\n Metadata Dump: " . print_r($metadata, true));
                    }*/

                    $version = ModuleVersion::withTrashed()
                        ->where('module_id', $module->id)
                        ->where('version', $metadata->version)
                        ->first();

                    if(version_compare($metadata->version, $lastVersion)==1) {
                        $lastVersion = $metadata->version;

                        $module->is_inclusive = isset($metadata->special) ? $metadata->special : 0;

                        if(is_object($metadata->name))
                            $module->name = $metadata->name->en;
                        else
                            $module->name = $metadata->name;

                        if(is_object($metadata->name))
                            $module->name_ru = $metadata->name->ru;
                        else {
                            if (isset($metadata->name_rus)) {
                                $module->name_ru = $metadata->name_rus;
                            } else {
                                $module->name_ru = $metadata->name;
                            }
                        }
                    }
                    $release_data = date('Y-m-d', strtotime($metadata->release_date));

                    if (!$version) {

                        if (!in_array($module->id, $reorder))
                            $reorder[] = $module->id;

                        $version = ModuleVersion::create([
                            'module_id'         => $module->id,
                            'version'           => $metadata->version,
                            'status'            => $metadata->status,
                            'release_date'      => $release_data,
                            'release_notes'     => $metadata->release_notes,
                            'developer'         => $metadata->developer,
                            'developer_comment' => isset($metadata->developer_comment) ? $metadata->developer_comment : '', //todo убрать эту проверку
                            'behaviors'         => json_encode($metadata->behaviors_types),
                            'special'           => (int)(isset($metadata->special) && $metadata->special),
                        ]);

                        if (isset($metadata->themes) && count($metadata->themes)) {
                            foreach ($metadata->themes as $v) {
                                ModuleTheme::create([
                                    'module_id' => $module->id,
                                    'version' => $metadata->version,
                                    'name' => $v
                                ]);
                            }
                        }

                        $output[]= "{$module->path} => {$version->version}";
                    } else {
                        if ($version->sort == 0 && !in_array($module->id, $reorder)) {
                            $reorder[] = $module->id;
                        }
                        $version->release_date = $release_data;
                        $version->release_notes = $metadata->release_notes;
                        $version->developer = $metadata->developer;
                        $version->developer_comment = isset($metadata->developer_comment) ? $metadata->developer_comment : ''; //todo убрать эту проверку;
                        $version->behaviors = json_encode($metadata->behaviors_types);
                        $version->special = (int)(isset($metadata->special) && $metadata->special);
                        $version->save();

                        ModuleTheme::where('module_id', $module->id)
                            ->where('version', $metadata->version)
                            ->delete();

                        if (isset($metadata->themes) && count($metadata->themes)) {
                            foreach ($metadata->themes as $v) {
                                ModuleTheme::create([
                                    'module_id' => $module->id,
                                    'version' => $metadata->version,
                                    'name' => $v
                                ]);
                            }
                        }
                    }
                    
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage() . " in {$path}/{$dir_module}/{$dir_version}");
                }
            }
            $module->save();
        }

        // resorting
        foreach ($reorder as $id) {

            $versions = ModuleVersion::where('module_id', $id)
                ->get()->all();

            usort($versions, function ($a, $b) {
                return version_compare($a->version, $b->version);
            });

            $i = 1;
            foreach ($versions as $version) {
                $version->sort = $i++;
                $version->save();
            }
        }

        return implode("\r\n", $output);
    }

    public function UpdateConstructorsTable(){
        $path = Config::get('igvs.courses::modulesRelease.path') . '/constructor';
        $dir_constructors = $this->scandir($path, ['.gitignore', '.git']);
        $output = [];

        $reorder = [];

        foreach ($dir_constructors as $dir_constructor) {

            $module = Module::where('path', $dir_constructor)->first();

            // если модуль с данным кодом не существует - переходим к следующему конструктору
            if (!$module)
                continue;

            $dir_versions = $this->scandir("{$path}/{$dir_constructor}");

            $lastVersion = 0;

            foreach ($dir_versions as $dir_version) {

                $file = "{$path}/{$dir_constructor}/{$dir_version}/metadata.json";

                if (!file_exists($file))
                    continue;

                if ( ! ($metadata = json_decode(file_get_contents($file))) )
                    continue;

                $version = ConstructorVersion::withTrashed()
                    ->where('module_id', $module->id)
                    ->where('version', $metadata->version)
                    ->first();

                if(version_compare($metadata->version, $lastVersion)==1)
                    $lastVersion = $metadata->version;

                $release_data = date('Y-m-d', strtotime($metadata->release_date));

                if (!$version) {

                    if (!in_array($module->id, $reorder))
                        $reorder[] = $module->id;


                    $version = ConstructorVersion::create([
                        'module_id'         => $module->id,
                        'version'           => $metadata->version,
                        'status'            => $metadata->status,
                        'release_date'      => $release_data,
                        'release_notes'     => isset($metadata->release_notes)?$metadata->release_notes:'',
                        'developer'         => $metadata->developer,
                        'developer_comment' => isset($metadata->developer_comment)?$metadata->developer_comment:'',
                    ]);

                    $output[]= "{$module->path} => {$version->version}";
                }
            }
        }

        // resorting
        foreach ($reorder as $id) {

            $versions = ConstructorVersion::where('module_id', $id)
                ->get()->all();

            usort($versions, function ($a, $b) {
                return version_compare($a->version, $b->version);
            });

            $i = 1;
            foreach ($versions as $version) {
                $version->sort = $i++;
                $version->save();
            }
        }

        return implode("\r\n", $output);
    }
}