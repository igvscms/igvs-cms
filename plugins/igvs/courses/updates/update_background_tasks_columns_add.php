<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class updateBackgroundTasksColumnsAdd1 extends Migration
{

    public function up()
    {
        Schema::table('igvs_background_tasks', function ($table) {
            $table->dropColumn('finished');
            $table->timestamp('finished_at')
                ->nullable()
                ->after('result');
            $table->timestamp('started_at')
                ->nullable()
                ->after('result');
            $table->integer('responsible_id')
                ->after('result');
        });
    }

    public function down()
    {
        Schema::table('igvs_background_tasks', function ($table) {
//            $table->boolean('finished');
//            $table->dropColumn('responsible_id');
//            $table->dropColumn('finished_at');
            $table->dropColumn('started_at');
        });
    }

}