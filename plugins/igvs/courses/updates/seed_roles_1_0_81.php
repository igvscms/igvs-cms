<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Role;

class SeedRolesTables_1_0_81 extends Seeder
{
    public function run()
    {
        Role::where('id', '*')->update(['show_in_elearning' => false]);
        Role::whereIn('name', ['Admin', 'Координатор', 'Разработчик', 'Оператор', 'Эксперт'])
            ->update(['show_in_elearning' => true]);

        Role::where('id', '*')->update(['show_in_pilot' => false]);
        Role::whereIn('name', ['Admin', 'Pilot Content Manager With Create Course', 'Pilot Content Manager'])
            ->update(['show_in_pilot' => true]);
    }
}