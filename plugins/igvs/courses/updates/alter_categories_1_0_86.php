<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCategories_1_0_86 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_categories', function ($table) {
            $table->boolean('is_separate')
                ->after('parent_id')
                ->default(0);
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_categories', function($table) {
            $table->dropColumn('is_separate');
        });
    }
}