<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterLog_1_0_60 extends Migration
{
    public function up()
    {
        Schema::table('igvs_files', function($table) {
            $table->index('content_id', 'index_igvs_files_content_id');
        });
        
        Schema::table('igvs_loggings', function($table) {
            $table->index('user_id', 'index_igvs_loggings_user_id');
            $table->index('file_id', 'index_igvs_loggings_file_id');
        });
    }

    public function down()
    {
        Schema::table('igvs_files', function($table) {
            $table->dropIndex('index_igvs_files_content_id');
        });

        Schema::table('igvs_loggings', function($table) {
            $table->dropIndex('index_igvs_loggings_user_id');
            $table->dropIndex('index_igvs_loggings_file_id');
        });
    }
}