<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCoursesUserPilot_1_0_50 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_user_pilot', function($table) {
                $table->string('email')
                ->after('instance_id');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_user_pilot', function($table) {
             $table->dropColumn('email');
        });
    }
}