<?php namespace Igvs\Courses\Updates;

use Igvs\Courses\Models\Info;
use October\Rain\Database\Updates\Seeder;

class SeedUserInfo_1_0_56 extends Seeder {
    public function run() {
        Info::where('code', 'createTheme')->update(['text' => 'Для создания темы в составе раздела, нажмите правой кнопкой мыши на название раздела, выберите "Добавить тему" заполните поля формы и нажмите кнопку "Сохранить"']);
        Info::where('code', 'createAct')->update(['text' => 'Для создания раздела, нажмите на кнопку "Добавить раздел"']);
        Info::where('code', 'createModule')->update(['text' => 'Для создания модуля в составе темы, нажмите правой кнопкой мыши на название темы, выберите "Создать модуль" заполните поля формы и нажмите кнопку "Сохранить"']);
    }
}