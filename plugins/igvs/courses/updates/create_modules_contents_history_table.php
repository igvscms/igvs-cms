<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Config;

class CreateModulesContentsHistoryTable extends Migration
{
    public function up()
    {
        Schema::create('igvs_courses_modules_contents_history', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('module_content_id');
            $table->mediumText('module_content_data');
            $table->string('module_content_code', 30);
            $table->integer('module_id');
            $table->string('module_version', 10);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_modules_contents_history');
    }

}
