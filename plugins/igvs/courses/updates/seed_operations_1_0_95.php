<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Operation;

class SeedOperations_1_0_95 extends Seeder
{
    public function run()
    {
        Operation::insert([
            [
                'name_en' => '',
                'name_ru' => '',
                'code' => 'EditContentLockedModuleTask',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ]
        ]);
    }
}