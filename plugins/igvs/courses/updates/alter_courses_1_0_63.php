<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCourses_1_0_63 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->integer('public_last_user_id')
                ->after('public_status')
                ->nullable();
            $table->dateTime('public_last_datetime')
                ->after('public_status')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table) {
             $table->dropColumn('public_last_user_id');
             $table->dropColumn('public_last_datetime');
        });
    }
}