<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class AlterVocabularies_1_0_90 extends Migration
{
    public function up()
    {
        \Schema::table('igvs_courses_vocabularies', function($table) {
            $table->integer('user_id')
                ->after('course_id')
                ->unsigned()
                ->nullable();
        });
    }

    public function down()
    {
        \Schema::table('igvs_courses_vocabularies', function($table) {
             $table->dropColumn('user_id');
        });
    }
}