<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\ShellVersion;

class SeedShellVersionTable extends Seeder
{
    use \Igvs\Courses\Traits\FileHelper;

    public function run()
    {
        $this->seedShellVersions();
    }

    public function seedShellVersions()
    {
        $path = Config::get('igvs.courses::modulesRelease.path') . '/shell';
        $dir_modules = $this->scandir($path, ['.gitignore', '.git']);

        $reorder = $output = [];

        $dir_versions = $this->scandir("{$path}");

        $lastVersion = 0;
        foreach ($dir_versions as $dir_version) {

            $file = "{$path}/{$dir_version}/metadata.json";

            if (!file_exists($file))
                continue;

            if ( ! ($metadata = json_decode(file_get_contents($file))) )
                continue;

            $version = ShellVersion::withTrashed()
                ->where('version', $metadata->version)
                ->first();

            if(version_compare($metadata->version, $lastVersion)==1) {
                $lastVersion = $metadata->version;
            }
            $release_data = date('Y-m-d', strtotime($metadata->release_date));

            if (!$version) {
                $version = ShellVersion::create([
                    'version'           => $metadata->version,
                    'status'            => 'alpha', // $metadata->status
                    'release_date'      => $release_data,
                    'release_notes'     => $metadata->release_notes,
                    'developer'         => $metadata->developer,
                ]);

                $output[]= "{shell} => {$version->version}";
            }
        }

        // resorting
        $versions = ShellVersion::get()->all();

        if (count($versions) > 1) {
            usort($versions, function ($a, $b) {
                return version_compare($a->version, $b->version);
            });
        }

        $i = 1;
        foreach ($versions as $version) {
            $version->sort = $i++;
            $version->save();
        }

        return implode("\r\n", $output);
    }
}