<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterModulesContents_1_0_67 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->boolean('startscreen_disabled')
                ->after('publisher_note')
                ->default(false);
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->dropColumn('startscreen_disabled');
        });
    }
}