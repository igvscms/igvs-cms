<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCourses_1_0_82 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->boolean('constructor_all_privileges')
                ->after('colontitle_slogan')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table) {
                 $table->dropColumn('constructor_all_privileges');
        });
    }
}