<?php namespace Igvs\Courses\Updates;

use Schema;
use DB;
use October\Rain\Database\Updates\Migration;
use Igvs\Courses\Models\Constructor;
use Igvs\Courses\Models\ConstructorVersion;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\ModuleVersion;
use Config;

class UpdateModulesContents_1_0_70 extends Migration
{
    public function up()
    {
        DB::statement("UPDATE igvs_courses_modules_contents SET `startscreen_show` = -1 WHERE `startscreen_show` = 1");
    }

    public function down()
    {
        DB::statement("UPDATE igvs_courses_modules_contents SET `startscreen_show` = 1 WHERE `startscreen_show` = -1");
    }
}
