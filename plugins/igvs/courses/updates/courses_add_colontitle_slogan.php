<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CoursesAddColontitleSlogan extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->string('colontitle_slogan')
                ->after('material_covered');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->dropColumn('colontitle_slogan');
        });
    }
}