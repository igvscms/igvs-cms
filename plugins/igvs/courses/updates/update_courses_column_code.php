<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Db;

class updateCoursesColumnCode extends Migration
{

    public function up()
    {
        Schema::table('igvs_courses_courses', function ($table) {
            $table->string('code',30);
        });

        DB::update('update `igvs_courses_courses` set `code` = `id`',[]);

        Schema::table('igvs_courses_courses', function ($table) {
            $table->unique('code');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function ($table) {
            $table->dropColumn('code');
        });
    }

}