<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterModulesContents_1_0_46 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->integer('status_id')->unsigned()->nullable();

            $table->foreign('status_id','f_statusId_igvsCoursesCourses')
                ->references('id')
                ->on('igvs_courses_role_statuses')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {

            $table->dropForeign('f_statusId_igvsCoursesCourses');
            $table->dropColumn('status_id');
        });
    }
}