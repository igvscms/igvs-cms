<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class AlterModulesContents_1_0_99 extends Migration
{
    public function up()
    {
        \Schema::table('igvs_courses_modules_contents', function($table) {
            $table->boolean('version_auto_disable')
                ->after('code')
                ->default(0);
        });
    }

    public function down()
    {
        \Schema::table('igvs_courses_modules_contents', function($table) {
             $table->dropColumn('version_auto_disable');
        });
    }
}