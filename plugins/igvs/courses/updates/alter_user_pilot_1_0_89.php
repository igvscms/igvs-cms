<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class AlterUserPilot_1_0_89 extends Migration
{
    public function up()
    {
        \Schema::table('igvs_courses_user_pilot', function($table) {
            $table->integer('poo_id')
                ->after('cluster_id')
                ->unsigned()
                ->nullable()
                ->default(0);
        });
    }

    public function down()
    {
        \Schema::table('igvs_courses_user_pilot', function($table) {
             $table->dropColumn('poo_id');
        });
    }
}