<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCategories_1_0_85 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_categories', function ($table) {
            $table->text('etracker')
                ->after('qcf_ref')
                ->default('')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_categories', function($table) {
            $table->dropColumn('etracker');
        });
    }
}