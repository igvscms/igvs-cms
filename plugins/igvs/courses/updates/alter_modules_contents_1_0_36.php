<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterModulesContents_1_0_36 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->renameColumn('sort', 'old_sort');
        });
        
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->integer('old_sort')->nullable()->change();
            $table->integer('sort')->unsigned()->default(0);
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
            // $table->dropColumn('sort');
            // $table->$table->renameColumn('old_sort', 'sort');
        });
    }
}