<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Operation;
use Igvs\Courses\Models\Role;

class SeedOperations_1_0_92 extends Seeder
{
    public function run()
    {
        $operation = Operation::where('code', 'dictionaryAccess')->first();
        $role = Role::where('name', 'Эксперт')->first();

        if ($role)
            $role->operations()->add($operation);
    }
}