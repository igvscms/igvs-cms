<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Igvs\Courses\Models\Operation;
use Igvs\Courses\Models\Role;

class SeedOperations_1_0_98 extends Seeder
{
    public function run()
    {
        $roles = [
            'Content Manager' => [
                'AllowEditCategories',

                'ChangeModuleImages',
                'ChangeModuleAudio',
                'ChangeModuleVideo',
                'ChangeModuleText',
                'ChangeModuleStructure',

                'AddModuleContent',
                'DelModuleContent',
                'SortModuleContent',

                'dictionaryAccess',
                'dictionaryAccessEdit',

                'ModuleTabEdit_json_history',
                'ModuleTabEdit_custom_css',
                'ModuleTabEdit_designer',
                'ModuleTabEdit_json',
                'ModuleTabEdit_properties',
                'ModuleTabEdit_comments',
                'ModuleTabEdit_builds',
                'ModuleTabEdit_files_history',
            ],
            'Designer' => [
                'AllowEditCategories',
                'ChangeModuleImages',
                'ModuleTabEdit_designer',
            ],
            'Review' => [
                'ModuleTabEdit_designer',
            ],
            'Pilot Content Manager With Create Course' => [
                'ModuleTabEdit_designer',
                'ModuleTabEdit_json',
            ],
            'Pilot Content Manager' => [
                'AllowEditCategories',
                'AllowUpdateCourseModules',

                'ChangeModuleImages',
                'ChangeModuleAudio',
                'ChangeModuleVideo',
                'ChangeModuleText',
                'ChangeModuleStructure',

                'AddModuleContent',
                'DelModuleContent',
                'SortModuleContent',

                'ModuleTabEdit_json_history',
                'ModuleTabEdit_designer',
                'ModuleTabEdit_json',

            ],
            'Editor' => [
                'ModuleTabEdit_designer',
            ],
            'Content Manager without topic lock' => [
                'CheckReadonlyCategory',
                'UncheckReadonlyCategory',

                'AllowEditCategories',

                'ChangeModuleImages',
                'ChangeModuleAudio',
                'ChangeModuleVideo',
                'ChangeModuleText',
                'ChangeModuleStructure',

                'AddModuleContent',
                'DelModuleContent',
                'SortModuleContent',

                'ModuleTabEdit_json_history',
                'ModuleTabEdit_custom_css',
                'ModuleTabEdit_designer',
                'ModuleTabEdit_json',
                'ModuleTabEdit_properties',
                'ModuleTabEdit_comments',
                'ModuleTabEdit_builds',
                'ModuleTabEdit_files_history',
            ],
            'Координатор' => [],
            'Разработчик курса' => [
                'AllowEditCategories',

                'ChangeModuleImages',
                'ChangeModuleAudio',
                'ChangeModuleVideo',
                'ChangeModuleText',
                'ChangeModuleStructure',

                'AddModuleContent',
                'DelModuleContent',
                'SortModuleContent',

                'dictionaryAccess',
                'dictionaryAccessEdit',

                'ModuleTabEdit_designer',
                'ModuleTabEdit_json',
                'ModuleTabEdit_properties',
                'ModuleTabEdit_comments',
            ],
            'Оператор курса' => [
                'ChangeModuleImages',
                'ChangeModuleAudio',
                'ChangeModuleVideo',
                'ChangeModuleText',
                'ChangeModuleStructure',

                'allow_change_rights',
                'CourseProperty',

                'AddModuleContent',
                'DelModuleContent',

                'ModuleTabEdit_designer',
                'ModuleTabEdit_json',
                'ModuleTabEdit_properties',
                'ModuleTabEdit_comments',
            ],
            'Эксперт курса' => [
                'dictionaryAccess',

                'ModuleTabEdit_designer',
                'ModuleTabEdit_comments',
            ],
            'Content Manager для редактирования Электронных учебников' => [
                'AllowEditCategories',

                'ChangeModuleImages',
                'ChangeModuleAudio',
                'ChangeModuleVideo',
                'ChangeModuleText',
                'ChangeModuleStructure',

                'dictionaryAccess',

                'ModuleTabEdit_custom_css',
                'EditContentLockedModuleTask',

                'ModuleTabEdit_designer',
                'ModuleTabEdit_json',
                'ModuleTabEdit_properties',
            ],
        ];
        foreach ($roles as $role => $rights) {
            if ($role = Role::where('name', $role)->first()) {
                $role->operations = Operation::whereIn('code', $rights)->get();
                $role->save();
            }
        }
    }
}