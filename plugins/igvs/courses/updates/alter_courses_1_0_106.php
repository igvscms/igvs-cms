<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCourses_1_0_106 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->boolean('enable_html_markup_number_page')
                ->nullable()
                ->default(0)
                ->after('status_id');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table) {
                 $table->dropColumn('enable_html_markup_number_page');
        });
    }
}
