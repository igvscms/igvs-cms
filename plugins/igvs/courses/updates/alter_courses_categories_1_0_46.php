<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCategories_1_0_46 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_categories', function($table) {
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `unit_title` TEXT DEFAULT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `qcf_ref` TEXT DEFAULT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `nos_ref` TEXT DEFAULT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `awarding_body_code` TEXT DEFAULT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `level` TEXT DEFAULT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `credits` TEXT DEFAULT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `qualification_title` TEXT DEFAULT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `learning_outcome` TEXT DEFAULT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `functional_skills` TEXT DEFAULT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `session_title` TEXT DEFAULT NULL');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_categories', function($table) {
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `unit_title` TEXT DEFAULT NOT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `qcf_ref` TEXT DEFAULT NOT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `nos_ref` TEXT DEFAULT NOT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `awarding_body_code` TEXT DEFAULT NOT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `level` TEXT DEFAULT NOT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `credits` TEXT DEFAULT NOT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `qualification_title` TEXT DEFAULT NOT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `learning_outcome` TEXT DEFAULT NOT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `functional_skills` TEXT DEFAULT NOT NULL');
            \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `session_title` TEXT DEFAULT NOT NULL');
        });
    }
}