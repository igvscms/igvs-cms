<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateStorageStatsTable extends Migration
{
    public function up()
    {
        Schema::create('igvs_courses_storage_stats', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id');

            $table->integer('date');
            $table->string('hash')->index();
            $table->integer('inode')->unsigned()->nullable();
            $table->integer('size')->unsigned()->nullabel();
            $table->boolean('is_storage')->default(false);
            $table->integer('count_links')->unsigned()->default(0);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_storage_stats');
    }
}
