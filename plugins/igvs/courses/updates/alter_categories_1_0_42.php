<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCategories_1_0_42 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function ($table) {
            $table->dropColumn('view_all_themes');
            $table->dropColumn('show_start_screen');
            $table->text('include_all_themes')
                ->after('code')
                ->default('');
            $table->text('show_module_start_screen')
                ->after('code')
                ->default('');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->dropColumn('include_all_themes');
            $table->dropColumn('show_module_start_screen');
            $table->text('view_all_themes')
                ->after('code')
                ->default('');
            $table->text('show_start_screen')
                ->after('code')
                ->default('');
        });
    }
}