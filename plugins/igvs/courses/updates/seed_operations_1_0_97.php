<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Igvs\Courses\Models\Operation;

class SeedOperations_1_0_97 extends Seeder
{
    public function run()
    {
        $changeOperations = [
            'CourseAdd' => 'allow_change_rights',
            'CourseCopy' => null
        ];
        foreach ($changeOperations as $oldCode => $newCode) {
            if ($operaation = Operation::where('code', $oldCode)->first()) {
                if (!is_null($newCode)) {
                    $operaation->code = $newCode;
                    $operaation->save();
                }
                else {
                    $operaation->delete();
                }
            }
        }
    }
}