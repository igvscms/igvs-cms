<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class AlterModulesContents_1_0_76 extends Migration
{
    public function up()
    {
        \Schema::table('igvs_courses_modules_contents', function($table) {
            $table->integer('parent_id')
                ->unsigned()
                ->nullable();

            $table->foreign('parent_id','f_parentId_igvsCoursesModulesContents')
                ->references('id')
                ->on('igvs_courses_modules_contents')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        \Schema::table('igvs_courses_modules_contents', function($table) {
             $table->dropColumn('parent_id');
             $table->dropForeign('f_parentId_igvsCoursesModulesContents');
        });
    }
}