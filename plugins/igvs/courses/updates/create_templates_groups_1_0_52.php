<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Igvs\Courses\Models\TemplatesGroup;
use Igvs\Courses\Models\Module;

class CreateTemplatesGroups_1_0_52 extends Migration
{

    public function up()
    {
        // добавляем колонки причастности модуля к группе
        Schema::table('igvs_courses_modules', function ($table)
        {
            $table->integer('group_id')
                ->nullable()
                ->default(0)
                ->after('path');
        });

        Schema::create('igvs_courses_templates_groups', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules', function ($table)
        {
            $table->dropColumn('group_id');
        });

        Schema::dropIfExists('igvs_content_templates_groups');
    }

}
