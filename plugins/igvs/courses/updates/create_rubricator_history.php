<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateRubricatorHistory extends Migration
{

    public function up()
    {
        Schema::create('igvs_rubricator_history', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('value');//значение до изменения
            $table->integer('category_id');//что изменилось
            $table->tinyInteger('type');//как изменилось
            $table->integer('user_id');//кем изменилось
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_rubricator_history');
    }

}