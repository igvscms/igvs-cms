<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class AlterVocabularies_1_0_80 extends Migration
{
    public function up()
    {
        \Schema::table('igvs_courses_vocabularies', function($table) {
            $table->string('term_clean', 255)
            ->after('term');
        });
    }

    public function down()
    {
        \Schema::table('igvs_courses_vocabularies', function($table) {
             $table->dropColumn('term_clean');
        });
    }
}