<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateConstructorVersionsTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_courses_constructor_versions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('constructor_id');
            $table->string('version');
            $table->enum('status', ['alpha', 'beta', 'stable', 'unstable']);
            $table->timestamp('release_date');
            $table->string('release_notes');
            $table->string('developer');
            $table->string('developer_comment');
            $table->string("behaviors");
            $table->integer("sort");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_constructor_versions');
    }

}
