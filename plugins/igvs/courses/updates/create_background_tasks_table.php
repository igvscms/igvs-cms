<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateBackgroundTasksTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_background_tasks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('act', 100);
            $table->string('options', 1000);
            $table->integer('job_id');
            $table->integer('progress');
            $table->integer('progress_full');
            $table->boolean('finished');
            $table->text('result');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_background_tasks');
    }

}
