<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Role;

class SeedBackendUserGroups_1_0_104 extends Seeder
{
    public function run()
    {
        $user_group = \Backend\Models\UserGroup::firstOrNew([
            'name' => 'Inclusive'
        ]);

        if ($user_group->exists)
            return;

        $user_group->permissions = ['igvs.courses.is_inclusive' => 1];
        $user_group->save();
    }
}