<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddedFewInTheContentHistoryTable extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents_history', function($table) {
            $table->string('ip',20);
            $table->tinyInteger('type');
        });
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->string('ip',20);
            $table->tinyInteger('type');
            $table->Integer('user_id');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents_history', function($table) {
            $table->dropColumn('ip');
            $table->dropColumn('type');
        });
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->dropColumn('ip');
            $table->dropColumn('type');
            $table->dropColumn('user_id');
        });
    }
}