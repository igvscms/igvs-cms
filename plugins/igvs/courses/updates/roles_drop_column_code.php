<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class RolesDropColumnCode extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_roles', function($table) {
            $table->dropColumn('code');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_roles', function($table) {
            $table->string('code')->index();
        });
    }
}