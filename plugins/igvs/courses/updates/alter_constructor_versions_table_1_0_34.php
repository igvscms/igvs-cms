<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterConstructorVersionsTable_1_0_34 extends Migration
{

    public function up()
    {
        Schema::table('igvs_courses_constructor_versions', function ($table) {
            $table->dropColumn('status');
        });

        Schema::table('igvs_courses_constructor_versions', function ($table) {
            $table->renameColumn('constructor_id', 'module_id');
            $table->dropColumn('behaviors');

            $table->enum('status', ['alpha', 'beta', 'stable', 'unstable'])
                ->after('version');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_constructor_versions', function ($table) {
            $table->dropColumn('status');
        });

        Schema::table('igvs_courses_constructor_versions', function ($table) {
            $table->renameColumn('module_id', 'constructor_id');

            $table->string('behaviors')
                ->after('developer_comment');

            $table->enum('status', ['alpha', 'beta', 'stable', 'unstable'])
                ->after('version');
        });
    }

}
