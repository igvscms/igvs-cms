<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterRoleStatuses_1_0_51 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_role_statuses', function($table)
        {
            $table->string('command');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_role_statuses', function($table)
        {
            $table->dropColumn('command');
        });
    }
}