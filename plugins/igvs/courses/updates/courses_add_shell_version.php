<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CoursesAddShellVersion extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->string('shell_version_id')
                ->after('country');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->dropColumn('shell_version_id');
        });
    }
}