<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ChangeModulesContentsCode50 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents', function ($table) {
            $table->string('code', 50)->change();
        });

        Schema::table('igvs_courses_modules_contents_history', function ($table) {
            $table->string('module_content_code', 50)->change();
        });
    }

    public function down()
    {
    }
}