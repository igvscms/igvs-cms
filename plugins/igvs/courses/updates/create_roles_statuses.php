<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateRolesStatuses extends Migration
{
    public function up()
    {
        Schema::create('igvs_courses_roles_statuses', function($table)
        {
            $table->integer('role_id')->unsigned();
            $table->integer('status_id')->unsigned()->nullable();
            $table->integer('status_new_id')->unsigned();

            $table->foreign('role_id','f_roleId_igvsCoursesStatusesChanges')
                ->references('id')
                ->on('igvs_courses_roles')
                ->onDelete('cascade');

            $table->foreign('status_id','f_statusId_igvsCoursesStatusesChanges')
                ->references('id')
                ->on('igvs_courses_role_statuses')
                ->onDelete('cascade');

            $table->foreign('status_new_id','f_statusNewId_igvsCoursesStatusesChanges')
                ->references('id')
                ->on('igvs_courses_role_statuses')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_roles_statuses');
    }
}