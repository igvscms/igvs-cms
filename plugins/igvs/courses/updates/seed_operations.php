<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Operation;

class SeedOperationsTables extends Seeder
{
    public function run()
    {
        Operation::insert([
            [
                'name_en' => 'Allow Сheck Readonly Category',
                'name_ru' => 'Разрешить устанавливать галочку "Только для чтения"',
                'code' => 'CheckReadonlyCategory',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
            [
                'name_en' => 'Allow Uncheck Readonly Category',
                'name_ru' => 'Разрешить снимать галочку "Только для чтения"',
                'code' => 'UncheckReadonlyCategory',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
            [
                'name_en' => 'Allow Edit Categories',
                'name_ru' => 'Разрешить редактировать рубрикатор',
                'code' => 'AllowEditCategories',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
            /*[
                'name_en' => 'Show Designer',
                'name_ru' => 'Показывать дизайнер',
                'code' => 'showDesigner',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],*/
        ]);
    }
}