<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Igvs\Courses\Models\Course;

class CreateCoursesTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_courses_courses', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('parent_id')->default(0);
            $table->string('title');
            $table->string('modification_name');
            $table->text('description');
            $table->integer('responsible_id');
            $table->enum('status', ['alpha', 'pre-beta', 'beta', 'production']);
            $table->string('language');
            $table->string('country');
            $table->string('theme');
            $table->integer('sort');

            $table->enum('public_status', ['unpublished', 'published', 'soon']);
            $table->integer('level_qualification');
            $table->string('learning_hours');
            $table->timestamp('launch_date');
            $table->string('version');
            $table->text('for_whom');
            $table->text('support_materials');
            $table->text('material_covered');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Course::withTrashed()->get()->each(function($model) {
            
            if ($model->logo)
                $model->logo()->delete();

            if ($model->bg_hat)
                $model->bg_hat()->delete();

            if ($model->image)
                $model->image()->delete();
        });

        Schema::dropIfExists('igvs_courses_courses');
    }

}
