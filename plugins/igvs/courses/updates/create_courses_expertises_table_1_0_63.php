<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCoursesExpertisesTable_1_0_63 extends Migration
{
    public function up()
    {
        Schema::create('igvs_courses_coourses_expertises', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('course_id');
            $table->string('expertise_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_coourses_expertises');
    }
}
