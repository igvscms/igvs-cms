<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateBuildModuleContentsTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_courses_builds_modules_contents', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('build_id');
            $table->integer('module_content_id');
            $table->primary(['build_id', 'module_content_id'], 'builds_modules_contents');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_builds_modules_contents');
    }

}