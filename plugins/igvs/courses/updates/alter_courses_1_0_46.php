<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCourses_1_0_46 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table)
        {
            $table->integer('project_id')->unsigned()->nullable();
            $table->boolean('short_version');

            $table->foreign('project_id','f_projectId_igvsCoursesCourses')
                ->references('id')
                ->on('academy_tasks_projects')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table)
        {
            $table->dropForeign('f_projectId_igvsCoursesCourses');
            $table->dropColumn('project_id');
            $table->dropColumn('short_version');
        });
    }
}