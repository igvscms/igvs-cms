<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Operation;

class SeedOperations_1_0_96 extends Seeder
{
    private function newOperationByCode($code)
    {
        return [
            'name_en' => '',
            'name_ru' => '',
            'code' => $code,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ];
    }

    public function run()
    {
        $changeOperations = [
            'ModuleContentHistory' => 'ModuleTabEdit_json_history',
            'editingCommonStyle' => 'ModuleTabEdit_custom_css'
        ];
        foreach ($changeOperations as $oldCode => $newCode) {
            if ($operaation = Operation::where('code', $oldCode)->first()) {
                $operaation->code = $newCode;
                $operaation->save();
            }
        }
        $newOperations = ['designer', 'json', 'properties', 'comments', 'builds', 'files_history'];
        $insertOperation = [];
        foreach ($newOperations as $code) {
            $insertOperation[] = $this->newOperationByCode('ModuleTabEdit_' . $code);
        }
        Operation::insert($insertOperation);
    }
}