<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCoursesUserPilot_1_0_38 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_user_pilot', function($table) {
            $table->integer('instance_id')
                ->after('pilot_user_id');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_user_pilot', function($table) {
             $table->dropColumn('instance_id');
        });
    }
}