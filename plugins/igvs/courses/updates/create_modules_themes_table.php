<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateModulesThemesTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_courses_modules_themes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('module_id');
            $table->string('version');
            $table->string('name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_modules_themes');
    }

}
