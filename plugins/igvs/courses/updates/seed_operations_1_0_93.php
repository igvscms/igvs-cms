<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Operation;

class SeedOperations_1_0_93 extends Seeder
{
    public function run()
    {
        Operation::insert([
            [
                'name_en' => 'Access to the edit dictionary',
                'name_ru' => 'Доступ к изменению словаря',
                'code' => 'dictionaryAccessEdit',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ]
        ]);
    }
}