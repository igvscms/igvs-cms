<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterModulesContentsTable_1_0_45 extends Migration
{

    public function up()
    {
        Schema::table('igvs_courses_modules_contents', function ($table) {
            $table->index('course_id');
            $table->index('category_id');
            $table->index('code');
            $table->index('deleted_at');
        });
    }

    public function down()
    {
    }
}