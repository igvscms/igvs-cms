<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ModulesContentsAddInitByTpl extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->boolean('init_by_tpl')
                ->default(false)
                ->after('data');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->dropColumn('init_by_tpl');
        });
    }
}
