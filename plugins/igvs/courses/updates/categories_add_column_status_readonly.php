<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CategoriesAddStatusRadonly extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_categories', function($table) {

            $table->enum('status', ['alpha', 'pre-beta', 'beta', 'production'])
                ->nullable()
                ->after('code');

            $table->boolean('readonly')
                ->default(false);
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_categories', function($table) {
            $table->dropColumn('status');
            $table->dropColumn('readonly');
        });
    }
}
