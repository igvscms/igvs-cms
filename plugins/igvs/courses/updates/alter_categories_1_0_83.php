<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class AlterCategories_1_0_83 extends Migration
{
    public function up()
    {
        \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `code` VARCHAR(50)');
    }

    public function down()
    {
        \Db::statement('ALTER TABLE igvs_courses_categories MODIFY `code` VARCHAR(20)');
    }
}