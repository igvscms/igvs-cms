<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class AlterModulesContents_1_0_101 extends Migration
{
    public function up()
    {
        \Schema::table('igvs_courses_modules_versions', function($table) {
            $table->boolean('special')
                ->after('sort')
                ->default(0);
        });
    }

    public function down()
    {
        \Schema::table('igvs_courses_modules_versions', function($table) {
             $table->dropColumn('special');
        });
    }
}