<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCategories_1_0_44 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_categories', function ($table) {
            $table->text('session_title')
                ->after('code')
                ->default('');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_categories', function($table) {
            $table->dropColumn('session_title');
        });
    }
}