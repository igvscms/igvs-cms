<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Igvs\Courses\Models\Module;

class CreateModulesTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_courses_modules', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('path');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_modules');
    }

}
