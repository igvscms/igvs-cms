<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterRoles_1_0_81 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_roles', function($table) {
            $table->boolean('show_in_elearning')
                ->after('show_in_pilot')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_roles', function($table) {
            $table->dropColumn('show_in_pilot');
        });
    }
}