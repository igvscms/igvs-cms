<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class CreateUsersInfo extends Migration {
    public function up() {
        \Schema::create('igvs_courses_users_info', function($table) {

            $table->integer('user_id')
                ->unsigned();

            $table->integer('info_id')
                ->unsigned();

            $table->foreign('user_id', 'fk_ui_user_id')
                ->references('id')
                ->on('backend_users')
                ->onDelete('cascade');

            $table->foreign('info_id', 'fk_ui_info_id')
                ->references('id')
                ->on('igvs_courses_user_info')
                ->onDelete('cascade');

            $table->primary(['user_id', 'info_id'], 'pk_users_info_user_info');
        });
    }
    public function down() {
        Schema::dropIfExists('igvs_courses_users_info');
    }
}