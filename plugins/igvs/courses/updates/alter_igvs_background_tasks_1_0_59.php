<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterBackgroundTasks_1_0_59 extends Migration
{
    public function up()
    {
        Schema::table('igvs_background_tasks', function($table)
        {
            \Db::statement('ALTER TABLE igvs_background_tasks MODIFY `options` TEXT DEFAULT NULL');
        });
    }

    public function down()
    {
        Schema::table('igvs_background_tasks', function($table)
        {
            \Db::statement('ALTER TABLE igvs_background_tasks MODIFY `options` VARCHAR(1000)');
        });
    }
}