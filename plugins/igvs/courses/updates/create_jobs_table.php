<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Igvs\Courses\Models\Course;

class CreateJobsTable extends Migration
{
    public function up()
    {
        Schema::create('igvs_courses_jobs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->string('comment');
            $table->string('error');
            $table->string('status'); // 'new', 'waiting', 'processing', 'successful', 'fail'
            $table->timestamp('viewed');
            $table->double('progress');
            $table->text('params');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_courses');
    }
}
