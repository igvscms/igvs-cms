<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;

use Igvs\Courses\Models\Operation;

class SeedOperations_1_0_77 extends Seeder
{
    public function run()
    {
        Operation::insert([
            ['code' => 'editingCommonStyle']
        ]);
    }
}