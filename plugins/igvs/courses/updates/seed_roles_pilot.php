<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Role;

class SeedPilotRolesTables extends Seeder
{
    public function run()
    {
        Role::insert([
            ['name' => 'Pilot Content Manager With Create Course',  'assign_creator' => false, 'allow_change_rights' => false, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Pilot Content Manager', 'assign_creator' => false, 'allow_change_rights' => false, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
        ]);
    }
}