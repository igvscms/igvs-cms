<?php namespace Academy\Tasks\Updates;

use October\Rain\Database\Updates\Migration;

class AlterRoleStatuses_1_0_84 extends Migration
{
    public function up()
    {
        \Schema::table('igvs_courses_role_statuses', function($table) {
            $table->boolean('created');
        });
    }
    public function down()
    {
        \Schema::table('igvs_courses_role_statuses', function($table) {
            $table->dropColumn('created');
        });
    }
}