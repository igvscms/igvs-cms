<?php namespace Igvs\Courses\Updates;

use Igvs\Courses\Models\Expertise;
use October\Rain\Database\Updates\Seeder;
use Config;

class SeedExpertise_1_0_63 extends Seeder
{
    public function run()
    {
        Expertise::insert([
            [
                'name' => 'Сухое строительство и штукатурные работы',
                'code' => 'PQ_17_001',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ], [
                'name' => 'Малярные и декоративные работы ',
                'code' => 'PQ_17_002',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ], [
                'name' => 'Облицовка плиткой',
                'code' => 'PQ_17_003',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ], [
                'name' => 'Столярное дело',
                'code' => 'PQ_17_004',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ], [
                'name' => 'Плотницкое дело',
                'code' => 'PQ_17_005',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ], [
                'name' => 'Сантехника и отопление',
                'code' => 'PQ_17_006',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ], [
                'name' => 'Кирпичная кладка',
                'code' => 'PQ_17_007',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ], [
                'name' => 'Электромонтажник',
                'code' => 'PQ_17_008',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ], [
                'name' => 'Специалист по холодильно-вентиляционной технике',
                'code' => 'PQ_17_009',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ], [
                'name' => 'Ландшафтный дизайн',
                'code' => 'PQ_17_010',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
        ]);
    }
}