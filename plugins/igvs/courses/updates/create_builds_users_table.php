<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateBuildsUsersTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_courses_builds_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('build_id');
            $table->integer('user_id');
            $table->primary(['build_id', 'user_id'], 'builds_users');
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_builds_users');
    }

}
