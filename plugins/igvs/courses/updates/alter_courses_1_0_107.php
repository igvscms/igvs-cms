<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCourses_1_0_107 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->boolean('enable_shell_comments')
                ->nullable()
                ->default(0)
                ->after('enable_html_markup_number_page');
            $table->boolean('enable_shell_favorites')
                ->nullable()
                ->default(0)
                ->after('enable_html_markup_number_page');
            $table->boolean('enable_shell_screen_capture')
                ->nullable()
                ->default(0)
                ->after('enable_html_markup_number_page');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table) {
             $table->dropColumn('enable_shell_comments');
             $table->dropColumn('enable_shell_favorites');
             $table->dropColumn('enable_shell_screen_capture');
        });
    }
}
