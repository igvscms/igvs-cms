<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateBackgroundTasksLogTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_background_tasks_logs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('act', 100);
            $table->string('options', 1000);
            $table->string('message', 2000);
            $table->integer('db_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_background_tasks_logs');
    }

}