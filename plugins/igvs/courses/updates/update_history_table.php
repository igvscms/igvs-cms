<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Db;

class updateHistoryTable extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents_history', function ($table) {
            $table->string('name');
            $table->string('behavior');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents_history', function ($table) {
            $table->dropColumn('name');
            $table->dropColumn('behavior');
        });
    }
}