<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class AlterUserPilot_1_0_79 extends Migration
{
    public function up()
    {
        \Schema::table('igvs_courses_user_pilot', function($table) {
            $table->integer('cluster_id')
                ->after('instance_id')
                ->unsigned()
                ->nullable()
                ->default(0);
        });
    }

    public function down()
    {
        \Schema::table('igvs_courses_user_pilot', function($table) {
             $table->dropColumn('cluster_id');
        });
    }
}