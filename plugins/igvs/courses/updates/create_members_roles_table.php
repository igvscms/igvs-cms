<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateMembersRolesTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_courses_members_roles', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('member_id')->index();
            $table->integer('role_id')->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_members_roles');
    }

}
