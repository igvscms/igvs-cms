<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Operation;

class SeedAddOperationUpdateVersions extends Seeder
{
    public function run()
    {
        Operation::insert([
            [
                'name_en' => 'Updating Course Modules',
                'name_ru' => 'Обновление модулей курса',
                'code' => 'AllowUpdateCourseModules',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
        ]);
    }
}