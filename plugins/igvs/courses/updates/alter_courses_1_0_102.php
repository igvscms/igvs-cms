<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCourses_1_0_102 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->boolean('is_inclusive')
                ->nullable()
                ->default(0)
                ->after('is_base_course');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table) {
                 $table->dropColumn('is_inclusive');
        });
    }
}