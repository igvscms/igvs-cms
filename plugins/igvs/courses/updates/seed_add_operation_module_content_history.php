<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Operation;

class SeedAddOperationModuleContentHistory extends Seeder
{
    public function run()
    {
        Operation::insert([
            [
                'name_en' => 'Module Content History',
                'name_ru' => 'История контента модулей',
                'code' => 'ModuleContentHistory',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
        ]);
    }
}