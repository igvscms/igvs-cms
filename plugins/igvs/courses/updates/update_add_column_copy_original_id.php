<?php namespace Igvs\Courses\Updates;

use Schema;
use DB;
use October\Rain\Database\Updates\Migration;

class addColumnCopyOriginalId extends Migration
{
    public function up()
    {
        DB::statement('ALTER TABLE igvs_courses_courses change copy_parent_id copy_original_id integer(11)');

        Schema::table('igvs_courses_categories', function($table) {

            $table->integer('copy_original_id')
                ->default(0)
                ->after('id');
        });

        Schema::table('igvs_courses_modules_contents', function($table) {

            $table->integer('copy_original_id')
                ->default(0)
                ->after('id');
        });
    }

    public function down()
    {
        DB::statement('ALTER TABLE igvs_courses_courses change copy_original_id copy_parent_id integer(11)');

        Schema::table('igvs_courses_categories', function($table) {
            $table->dropColumn('copy_original_id');
        });

        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->dropColumn('copy_original_id');
        });
    }
}
