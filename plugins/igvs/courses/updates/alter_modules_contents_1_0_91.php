<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class AlterModulesContents_1_0_91 extends Migration
{
    public function up()
    {
        \Schema::table('igvs_courses_modules_contents', function($table) {
            $table->smallInteger('switch_practice_mode')
                ->after('practice_assessment_title')
                ->default(0);
        });
    }

    public function down()
    {
        \Schema::table('igvs_courses_modules_contents', function($table) {
             $table->dropColumn('switch_practice_mode');
        });
    }
}