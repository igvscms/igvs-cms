<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Role;
use Igvs\Courses\Models\Status;
use Igvs\Courses\Models\StatusChange;

class SeedStatusChange_1_0_94 extends Seeder
{
    public function run()
    {
        $roles = Role::whereIn('name', [
            'Admin',
            'Pilot Content Manager With Create Course',
            'Pilot Content Manager',
            'Разработчик курса',
            'Оператор курса',
        ])->get();
        $status = Status::where('name', 'В доработке')->first();

        if (!$status)
            return;

        foreach ($roles as $role) {
            StatusChange::firstOrCreate([
                'role_id' => $role->id,
                'status_id' => null,
                'status_new_id' => $status->id,
            ]);
        }
    }
}