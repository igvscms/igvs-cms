<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCourses_1_0_87 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->boolean('is_base_course')
                ->after('constructor_all_privileges')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table) {
                 $table->dropColumn('is_base_course');
        });
    }
}