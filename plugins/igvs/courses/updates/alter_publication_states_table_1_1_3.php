<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterPublicationStatesTable_1_1_3 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_publication_states', function($table) {
            $table->integer('order')
                ->after('behavior');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_publication_states', function($table) {
            $table->dropColumn('order');
        });
    }
}
