<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCategories_1_0_43 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_categories', function ($table) {
            $table->dropColumn('credits_qualification_title');

            $table->text('credits')
                ->after('code')
                ->default('');
            $table->text('qualification_title')
                ->after('code')
                ->default('');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_categories', function($table) {
            $table->dropColumn('credits_qualification_title');
            $table->dropColumn('qualification_title');

            $table->text('credits_qualification_title')
                ->after('code')
                ->default('');
        });
    }
}