<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class RolesAddIsSuperuser extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_roles', function($table) {

            $table->boolean('is_superuser')
                ->default(false)
                ->after('code');

        });
    }

    public function down()
    {
        Schema::table('igvs_courses_roles', function($table) {
            $table->dropColumn('is_superuser');
        });
    }
}
