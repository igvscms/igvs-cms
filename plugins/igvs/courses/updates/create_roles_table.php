<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateRolesTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_courses_roles', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('code')->index();
            $table->boolean('assign_creator');
            $table->boolean('allow_change_rights');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_roles');
    }
}
