<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterModulesContents_1_0_71 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->dropColumn('publisher_note');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->text('publisher_note')
                ->after('init_by_tpl')
                ->default('');
        });
    }
}