<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterRoles_1_0_49 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_roles', function($table) {
            $table->integer('show_in_pilot')
                ->after('allow_change_rights')
                ->nullable();
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_roles', function($table) {
            $table->dropColumn('show_in_pilot');
        });
    }
}