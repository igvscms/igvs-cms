<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTempLinksTable_1_0_78 extends Migration
{
    public function up()
    {
        Schema::create('igvs_courses_temp_links', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('course_id');
            $table->integer('build_id');
            $table->integer('topic_id');
            $table->dateTime('date_expired');
            $table->string('hash');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_temp_links');
    }
}
