<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CoursesAddCopyType extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->integer('copy_parent_id')
                ->after('parent_id');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->dropColumn('copy_parent_id');
        });
    }
}