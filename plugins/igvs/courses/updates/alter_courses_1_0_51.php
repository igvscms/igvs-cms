<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCourses_1_0_51 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table)
        {
            $table->integer('status_id')->unsigned()->nullable();

            $table->foreign('status_id','f_statusId_igvsCourses')
                ->references('id')
                ->on('igvs_courses_role_statuses')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table)
        {
            $table->dropForeign('f_statusId_igvsCourses');
            $table->dropColumn('status_id');
        });
    }
}