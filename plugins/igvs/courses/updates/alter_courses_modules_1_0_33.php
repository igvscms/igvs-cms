<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class alterCoursesModules_1_0_33 extends Migration
{

    public function up()
    {
        Schema::table('igvs_courses_modules', function ($table) {
            $table->string('is_inclusive')
                ->nullable()
                ->default(0)
                ->after('path');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules', function ($table) {
            $table->dropColumn('is_inclusive');
        });
    }

}