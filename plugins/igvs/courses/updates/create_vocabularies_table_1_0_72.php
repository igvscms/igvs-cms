<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateVocabulariesTable_1_0_72 extends Migration
{
    public function up()
    {
        Schema::create('igvs_courses_vocabularies', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('course_id');
            $table->string('term', 255);
            $table->text('description');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_vocabularies');
    }
}
