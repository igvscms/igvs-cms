<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Operation;

class SeedAddOperationAddCopyCourse extends Seeder
{
    public function run()
    {
        Operation::insert([
            [
                'name_en' => 'Create new course',
                'name_ru' => 'Создание нового курса',
                'code' => 'CourseAdd',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
            [
                'name_en' => 'Copy course',
                'name_ru' => 'Копирование курсов',
                'code' => 'CourseCopy',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
            [
                'name_en' => 'Delete courses which do not owner',
                'name_ru' => 'Удаление не своих курсов',
                'code' => 'DeleteCoursesWhichDoNotOwner',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
            [
                'name_en' => 'Course Property',
                'name_ru' => 'Редактирование свойств курса',
                'code' => 'CourseProperty',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
        ]);
    }
}