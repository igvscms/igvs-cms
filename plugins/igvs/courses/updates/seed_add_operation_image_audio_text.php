<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Operation;

class SeedAddOperationImageAudioText extends Seeder
{
    public function run()
    {
        Operation::insert([
            [
                'name_en' => 'Allow Change Images',
                'name_ru' => 'Разрешить изменять изображения',
                'code' => 'ChangeModuleImages',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
            [
                'name_en' => 'Allow Change Audio',
                'name_ru' => 'Разрешить изменять аудио',
                'code' => 'ChangeModuleAudio',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
            [
                'name_en' => 'Allow Change Video',
                'name_ru' => 'Разрешить изменять видео',
                'code' => 'ChangeModuleVideo',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
            [
                'name_en' => 'Allow Change Text',
                'name_ru' => 'Разрешить изменять текст',
                'code' => 'ChangeModuleText',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
            [
                'name_en' => 'Allow Change Structure',
                'name_ru' => 'Разрешить изменять структуру',
                'code' => 'ChangeModuleStructure',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ],
        ]);
    }
}