<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Igvs\Courses\Models\TemplatesGroup;
use Igvs\Courses\Models\Module;

class SeedTemplatesGroups_1_0_54 extends Migration
{

    public $group_templates = [
        'I-ASK (Теоретические модули)' => 'Теоретические модули',
        'I-CHECK, I-TEST (Практические и контрольные модули)' => 'Практические и контрольные модули',
        ];

    public function up()
    {
        foreach($this->group_templates as $group_name_old => $group_name_new) {

            $template_group = TemplatesGroup::where('name', $group_name_old)->first();

            if ($template_group) {
                $template_group->name = $group_name_new;
                $template_group->save();
            }

        }

    }

    public function down()
    {
        $this->group_templates = array_flip($this->group_templates);

        foreach($this->group_templates as $group_name_old => $group_name_new) {

            $template_group = TemplatesGroup::where('name', $group_name_old)->first();

            if ($template_group) {
                $template_group->name = $group_name_new;
                $template_group->save();
            }

        }
    }

}
