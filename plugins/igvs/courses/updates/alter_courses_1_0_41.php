<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCourses_1_0_41 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->boolean('show_start_screen')
                ->after('theme')
                ->default(0);
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table) {
             $table->dropColumn('show_start_screen');
        });
    }
}