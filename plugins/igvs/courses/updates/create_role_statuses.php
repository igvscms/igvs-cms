<?php namespace Academy\Tasks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateRoleStatuses extends Migration
{
    public function up()
    {
        Schema::create('igvs_courses_role_statuses', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('color',16);
            $table->string('icon',32);
            $table->boolean('is_blocks_an_object');
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_role_statuses');
    }
}