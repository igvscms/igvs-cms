<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Db;


class updateUserPilotColumnPilotUserId extends Migration
{

    public function up()
    {
        Schema::table('igvs_courses_user_pilot', function ($table) {
            $table->string('pilot_user_id')->change();
        });

    }

    public function down()
    {
        Schema::table('igvs_courses_user_pilot', function ($table) {
            $table->integer('pilot_user_id')->change();
        });
    }

}