<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateLogTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_files', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('content_id');
            $table->tinyInteger('status');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('igvs_loggings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->tinyInteger('status');
            $table->string('name');
            $table->integer('file_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_loggings');
        Schema::dropIfExists('igvs_files');
    }

}