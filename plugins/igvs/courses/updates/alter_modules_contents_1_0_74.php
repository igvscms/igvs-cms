<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterModulesContents_1_0_74 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->string('practice_assessment_title')
                ->after('learning_outcome')
                ->default('');

            $table->string('practice_index_title')
                ->after('learning_outcome')
                ->default('');

            $table->boolean('practice_title_manage')
                ->after('learning_outcome')
                ->default(0);
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
             $table->dropColumn('practice_title_manage');
             $table->dropColumn('practice_index_title');
             $table->dropColumn('practice_assessment_title');
        });
    }
}