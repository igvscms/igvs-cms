<?php namespace Igvs\Courses\Updates;

use Schema;
use DB;
use October\Rain\Database\Updates\Migration;
use Igvs\Courses\Models\Constructor;
use Igvs\Courses\Models\ConstructorVersion;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\ModuleVersion;
use Config;

class enumStatus extends Migration
{
    use \Igvs\Courses\Traits\FileHelper;

    public function up()
    {
        DB::statement("ALTER TABLE igvs_courses_constructor_versions MODIFY COLUMN status ENUM('alpha', 'stable', 'broken', 'develop')");
        DB::statement("ALTER TABLE igvs_courses_modules_versions MODIFY COLUMN status ENUM('alpha', 'stable', 'broken', 'develop')");

        $path = Config::get('igvs.courses::modulesRelease.path') . '/constructor';
        $dir_constructors = $this->scandir($path, ['.gitignore', '.git']);

        foreach ($dir_constructors as $dir_constructor) {
            $dir_versions = $this->scandir("{$path}/{$dir_constructor}");

            foreach ($dir_versions as $dir_version) {

                $attr = ['path' => $dir_constructor];
                $constructor = Constructor::where($attr)->withTrashed()->first();

                if (!$constructor)
                    continue;

                $file = "{$path}/{$dir_constructor}/{$dir_version}/metadata.json";

                if (!file_exists($file))
                    continue;

                if (!($metadata = json_decode(file_get_contents($file))))
                    continue;

                $version = ConstructorVersion::withTrashed()
                    ->where('constructor_id', $constructor->id)
                    ->where('version', $metadata->version)
                    ->first();

                if (!$version)
                    continue;

                $version->status = $metadata->status;

                $version->save();
            }
        }

        $path = Config::get('igvs.courses::modulesRelease.path') . '/modules';
        $dir_modules = $this->scandir($path, ['.gitignore', '.git']);

        foreach ($dir_modules as $dir_module) {
            $dir_versions = $this->scandir("{$path}/{$dir_module}");

            foreach ($dir_versions as $dir_version) {

                $attr = ['path' => $dir_module];
                $module = Module::where($attr)->withTrashed()->first();

                $file = "{$path}/{$dir_module}/{$dir_version}/metadata.json";

                if (!file_exists($file))
                    continue;

                if (!($metadata = json_decode(file_get_contents($file))))
                    continue;

                $version = ModuleVersion::withTrashed()
                    ->where('module_id', $module->id)
                    ->where('version', $metadata->version)
                    ->first();

                if (!$version)
                    continue;

                $version->status = $metadata->status;

                $version->save();
            }
        }
    }

    public function down()
    {
        DB::statement("ALTER TABLE igvs_courses_constructor_versions MODIFY COLUMN status ENUM('alpha', 'beta', 'stable', 'unstable')");
        DB::statement("ALTER TABLE igvs_courses_modules_versions MODIFY COLUMN status ENUM('alpha', 'beta', 'stable', 'unstable')");
    }
}
