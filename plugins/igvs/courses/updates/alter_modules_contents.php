<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterModulesContents_1_0_35 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents', function($table)
        {
            $table->tinyInteger('steps')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents', function($table)
        {
            $table->dropColumn('steps');
        });
    }
}