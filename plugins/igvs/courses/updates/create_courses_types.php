<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCoursesTypes extends Migration
{

    public function up()
    {
        Schema::create('igvs_courses_types', function($table)
        {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });
        Schema::table('igvs_courses_courses', function($table)
        {
            $table->integer('type_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_types');
        Schema::table('igvs_courses_courses', function($table)
        {
            $table->dropColumn('type_id');
        });
    }

}