<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ChangeDataTypeInContentHistory extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents_history', function($table)
        {
            $table->binary('data');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents_history', function($table) {
            $table->dropColumn('data');
        });
    }
}