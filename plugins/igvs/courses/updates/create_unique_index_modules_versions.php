<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateUniqueIndexModulesVersions extends Migration
{

    public function up()
    {
        Schema::table('igvs_courses_modules_versions', function($table) {
            $table->unique(['module_id','version'],'module_id_version');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_versions', function($table) {
            $table->dropUnique('module_id_version');
        });
    }

}