<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterModulesContents_1_0_73 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
            $table->text('learning_outcome')
                ->after('startscreen_show')
                ->default('');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents', function($table) {
             $table->dropColumn('learning_outcome');
        });
    }
}