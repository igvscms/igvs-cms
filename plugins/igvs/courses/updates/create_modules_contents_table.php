<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Config;

class CreateModulesContentsTable extends Migration
{
    use \Igvs\Courses\Traits\FileHelper;

    public function up()
    {
        Schema::create('igvs_courses_modules_contents', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('course_id');
            $table->integer('category_id');
            $table->integer('module_id');
            $table->string('behavior');
            $table->string('name');
            $table->string('code', 30);
            $table->string('version', 10);
            $table->integer('sort');
            $table->mediumText('data');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        // drop table
        Schema::dropIfExists('igvs_courses_modules_contents');

        // delete files
        $dir = Config::get('igvs.courses::modulesContent.path');
        if (is_dir($dir))
            $this->removeDir($dir);
    }

}
