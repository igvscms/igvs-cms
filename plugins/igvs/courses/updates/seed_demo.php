<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use System\Models\File;
use Config;
use DB;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Build;
use Igvs\Courses\Models\BuildModuleContent;
use Igvs\Courses\Models\Module;

class SeedDemo extends Seeder
{
    use \Igvs\Courses\Traits\FileHelper;

    const OS_WIN = 1;
    const OS_LINUX = 2;
    const OS_OTHER = 3;

    private $path;
    private $storage;

    public function run()
    {
        /*
        DB::beginTransaction();

        try {
        */

            $this->seedBuilds();
            $this->path = dirname(__FILE__) . '/demo';
            $this->storage = Config::get('igvs.courses::modulesContent.path');

            $dir_courses = $this->scandir($this->path, ['info.json', '.gitignore']);

            foreach ($dir_courses as $dir_course) {

                // 
                $info = file_get_contents("$this->path/$dir_course/info.json");
                $info = str_replace(["\\\n", "\\\r\n"], '\n', $info);
                $info = json_decode($info);

                $course_data = [
                    'status'        => 'alpha',
                    'language'      => 'en',
                    'country'       => 'uk',
                    'public_status' => 'published'
                ];

                if ($info)
                    $course_data = array_merge($course_data, (array) $info);

                // create course
                $course = Course::create($course_data);

                // atach images
                $this->attachFile($course, 'logo', "$this->path/$dir_course/img/logo.png");
                $this->attachFile($course, 'bg_hat', "$this->path/$dir_course/img/background.jpg");
                $this->attachFile($course, 'image', "$this->path/$dir_course/img/preview.jpg");

                // atach topics
                $this->attachTopics($course, "{$this->path}/{$dir_course}");
            }

        /*
            DB::commit();

        } catch (Exception $e) {
             DB::rollbackTransaction();
             throw $e;
        }
        */
        
        // $this->seedContentModulesBuild();
    }

    private function attachTopics($course, $path)
    {
        $dir_topics = $this->scandir($path, ['img', 'info.json']);
        usort($dir_topics, 'version_compare');
        foreach ($dir_topics as $dir_topic) {

            $json = file_get_contents("{$path}/{$dir_topic}/db/js_db.json");
            $obj = json_decode($json);

            $topic = Category::create([
                'course_id' => $course->id,
                'code' => strtoupper($dir_topic),
                'name' => $obj->config->title,
            ]);

            // atach acts
            $this->attachActs($topic, "{$path}/{$dir_topic}");
        }
    }

    private function attachActs($topic, $path)
    {
        // atach acts
        foreach ($this->getNav($path) as $v) {

            $act = Category::create([
                'course_id' => $topic->course_id,
                'parent_id' => $topic->id,
                'name' => $v['item']->textTitle
            ]);

            // attach modules
            $this->attachModules($act, $path, $v['child']);
        }
    }

    private function attachModules($act, $path, $lessons)
    {
        $sort = 1;
        foreach ($lessons as $v) {

            if (preg_match("/.*assessment$/", $v->resource)) {
                continue;
            }

            $resource = str_replace('/index', '', $v->resource);
            $npath = "{$path}/{$resource}";

            if (!is_dir($npath) || !file_exists($npath.'/content/moduleData.js'))
                return;

            $info = $this->getModuleInfo($npath);
            $storage = "{$this->storage}/{$act->course_id}/{$resource}";
            $colontitle = str_replace(['i-Ask:', 'i-Check:', 'i-Test:', 'i-Practice:'], '' , $v->textTitle);

            $lesson = ModuleContent::create([
                'course_id'         => $act->course_id,
                'category_id'       => $act->id,
                'name'              => trim($colontitle),
                'module_id'         => $info['module_id'],
                'behavior'          => $info['behavior'],
                'version'           => $info['version'],
                'data'              => $info['data'],
                'code'              => $resource,
                'sort'              => ($sort++)
            ]);

            $this->recourceCopy("{$npath}/content", $storage);
            unlink("{$storage}/moduleData.js");
            if (file_exists("{$storage}/template.js"))
                unlink("{$storage}/template.js");
        }
    }

    private function getModuleInfo($path)
    {
        $npath = "{$path}/content/moduleData.js";
        $version = '1';
        $module_template = '';
        $dir_list = is_dir($path) ? scandir($path) : [];

        if (count($dir_list)) {
            foreach ($dir_list as $dir_value) {

                if ($dir_value === 'metadata.json') {
                    $metadata = file_get_contents("{$path}/metadata.json");
                    $metadata = json_decode($metadata);
                    $module_template = $metadata->folder_name;
                    break;
                }

                // aliases modules
                $aliases = [
                    'intro.md' => ['md' => 'intro.md', 'version' => '1'],
                    'intro-new.md' => ['md' => 'intro.md', 'version' => '2'],
                    'intPicturePopup.md' => ['md' => 'interactive-picture-popup.md', 'version' => '1']
                ];

                if (isset($aliases[$dir_value])) {
                    $version = $aliases[$dir_value]['version'];
                    $dir_value = $aliases[$dir_value]['md'];
                }

                $module_template = stripos($dir_value, '.md');
                if ($module_template) {
                    $module_template = substr($dir_value, 0, $module_template);
                    break;
                }
            }
        }

        if (!file_exists($npath))
            throw new \Exception ("Not found file ({$npath})");

        $cmd = $this->serverOs() === self::OS_WIN ? 'node' : 'nodejs';
        $jspath = dirname(__FILE__).'/nodejs/json-decode.js';
        $json = shell_exec("{$cmd} {$jspath} {$npath}");
        $data = json_decode($json);

        if (!$data)
            throw new \Exception("Invalid JSON ({$dir_value})");

        $module = Module::where('path', $module_template)
            ->first();

        if (!$module)
            throw new \Exception("Not found module (module: {$module_template})");

        // 
        $json = str_replace(['"content/', '\'content/'], ['"', '\''], $json);

        return [
            'behavior'  => $this->getModuleBehavior($data->colontitle, $module),
            'module_id' => $module->id,
            'version'   => $module->getLatestVersion($version),
            'data'      => $json
        ];
    }

    private function getModuleBehavior($colontitle, $module)
    {
        $colontitle = trim($colontitle, chr(0xC2) . chr(0xA0)); // трим неразрывных пробелов
        $behaviors = ['i-check', 'i-test', 'i-ask', 'i-practice'];
        $behavior = null;

        foreach ($behaviors as $b) {
            if (strripos(strtolower(trim($colontitle)), $b) === 0) {
                $behavior = str_replace('i-', '', $b);
            }
        }

        // hardcode 
        if (in_array($module->path, ['intro', 'player']))
            $behavior = 'ask';

        /*if (!$behavior && !($behavior = $module->getDefaultBehavior()) )
            throw new \Exception("Not identify module behavior: {$colontitle}");*/

        if (!$behavior) {
            // dd([$behavior, $colontitle]);
            throw new \Exception("Not identify module behavior: {$colontitle} {$module->path}");
        }

        return $behavior;
    }

    private function getNav($topic)
    {
        $json = file_get_contents("{$topic}/db/js_db.json");
        $nav = json_decode($json)->nav->eapp;

        $res = [];
        foreach ($nav as $item) {
            if ($item->parent == 0)
                $res[$item->id]['item'] = $item;
            else
                $res[$item->parent]['child'][] = $item;
        }

        return $res;
    }

    private function seedBuilds()
    {
        Build::insert([
            ['name' => 'Alpha', 'code' => 'alpha', 'show_all' => false, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Beta', 'code' => 'beta', 'show_all' => false, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
            ['name' => 'Demo', 'code' => 'demo', 'show_all' => false, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')],
        ]);
    }

    private function seedContentModulesBuild(){

        $module_content = ModuleContent::all();
        $build          = Build::first();

        if ($build) {
            foreach ($module_content as $value) {
                BuildModuleContent::create([
                    'build_id'          => $build->id,
                    'module_content_id' => $value->id,
                ]);
            }
        }
    }

    function serverOS()
    {
        $sys = strtoupper(PHP_OS);
     
        if(substr($sys,0,3) == "WIN")
            return self::OS_WIN;

        if($sys == "LINUX")
            return self::OS_LINUX;
        
        return self::OS_OTHER;
    }
}