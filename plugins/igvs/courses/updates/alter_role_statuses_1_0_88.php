<?php namespace Academy\Tasks\Updates;

use October\Rain\Database\Updates\Migration;

class AlterRoleStatuses_1_0_88 extends Migration
{
    public function up()
    {
        \Schema::table('igvs_courses_role_statuses', function($table) {
            $table->boolean('block_export')
                ->after('created')
                ->nullable();
        });
    }
    public function down()
    {
        \Schema::table('igvs_courses_role_statuses', function($table) {
            $table->dropColumn('block_export');
        });
    }
}