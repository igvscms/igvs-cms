<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Migration;

class AlterModulesContents_1_0_100 extends Migration
{
    public function up()
    {
        \Schema::table('igvs_courses_modules_contents', function($table) {
            $table->integer('link_to_theory_module_form_id')
                ->after('code')
                ->nullable()
                ->default(null);
        });
    }

    public function down()
    {
        \Schema::table('igvs_courses_modules_contents', function($table) {
             $table->dropColumn('link_to_theory_module_form_id');
        });
    }
}