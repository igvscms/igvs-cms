<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateStorageStatsDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('igvs_courses_storage_stats_details', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('storage_stat_id')->unsigned();
            $table->string('path', 1000);
            $table->integer('size')->unsigned();
            $table->integer('inode')->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_storage_stats_details');
    }
}





