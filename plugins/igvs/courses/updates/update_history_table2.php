<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Db;

class updateHistoryTable2 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_modules_contents_history', function ($table) {
            $table->boolean('diff');
            $table->integer('diff_group');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_modules_contents_history', function ($table) {
            $table->dropColumn('diff');
            $table->dropColumn('diff_group');
        });
    }
}