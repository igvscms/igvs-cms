<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateMembersTable extends Migration
{

    public function up()
    {
        Schema::create('igvs_courses_members', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->integer('course_id')->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('igvs_courses_members');
    }

}
