<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterJobsTable_1_0_45 extends Migration
{

    public function up()
    {
        Schema::table('igvs_courses_jobs', function ($table) {
            $table->string('type')->index();
            $table->string('model_name');
            $table->integer('model_id')->unsigned()->nullable()->index();
            $table->integer('target_id')->unsigned()->nullable()->index();

        });
    }

    public function down()
    {
        Schema::table('igvs_courses_jobs', function ($table) {
            $table->dropColumn('type');
            $table->dropColumn('model_name');
            $table->dropColumn('model_id');
            $table->dropColumn('target_id');
        });
    }
}