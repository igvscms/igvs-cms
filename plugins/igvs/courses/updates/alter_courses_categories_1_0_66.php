<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCategories_1_0_66 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_categories', function($table) {
            $table->dropColumn('publisher_note');
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_categories', function($table) {
            $table->text('publisher_note')
                ->after('functional_skills')
                ->default('');
        });
    }
}