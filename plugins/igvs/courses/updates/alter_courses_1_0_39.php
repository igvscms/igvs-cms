<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AlterCourses_1_0_39 extends Migration
{
    public function up()
    {
        Schema::table('igvs_courses_courses', function($table) {
            $table->boolean('view_all_themes')
                ->after('theme')
                ->default(0);
        });
    }

    public function down()
    {
        Schema::table('igvs_courses_courses', function($table) {
             $table->dropColumn('view_all_themes');
        });
    }
}