<?php namespace Igvs\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Igvs\Courses\Models\Module;

class DeleteConstructorsTable_1_0_34 extends Migration
{

    public function up()
    {
        Module::withTrashed()->get()->each(function($model) {

            if ($model->specification)
                $model->specification()->delete();

        });

        Schema::dropIfExists('igvs_courses_constructors');
    }

    public function down()
    {
        Schema::create('igvs_courses_constructors', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('path');
            $table->timestamps();
            $table->softDeletes();
        });
    }

}
