<?php namespace Igvs\Courses\Updates;

use October\Rain\Database\Updates\Seeder;
use Config;

use Igvs\Courses\Models\Role;

class SeedBackendUserGroups_1_0_105 extends Seeder
{
    public function run()
    {
        $user_groups = \Backend\Models\UserGroup::whereIn('name', [
            'IGVS Content managers',
            'IGVS Courses read-only',
            'IGVS Contents managers (Advanced permissions)',
        ])->get();

        foreach ($user_groups as $user_group) {
            $user_group->permissions = array_merge($user_group->permissions, [
                'igvs.courses.show_btn_preview' => 1,
                'igvs.courses.show_btn_temp_link' => 1,
            ]);
            $user_group->save();
        }
    }
}
