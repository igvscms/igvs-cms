<?php namespace Igvs\Courses\Controllers;

use View;
use Response;
use BackendAuth;
use BackendMenu;
use Lang;
use Backend\Classes\Controller;
use Exception;
use Config;
use Igvs\Courses\Models\Logging;
//use Igvs\Courses\Models\File;
//use Igvs\Courses\Models\ModuleContentHistory;

/**
 * Contents Back-end Controller
 */
class Loggings extends Controller
{
    use \Igvs\Courses\Traits\CheckAccess;

    public $implement = [
        'Backend.Behaviors.ListController',
    ];

    public $listConfig = 'config_list.yaml';

    public $file_id = 0;

    public function view($file_id=0)
    {
        $this->layoutPath = ['plugins/igvs/courses/layouts'];
        $this->file_id = $file_id;
        return $this->asExtension('ListController')->index();
    }

    public function listExtendQuery($query)
    {
        $query->where('file_id',$this->file_id);
        $query->getDeveloper();
    }
}