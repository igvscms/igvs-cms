<?php namespace Igvs\Courses\Controllers;

use BeSimple\SoapServer\SoapServer;
use Illuminate\Routing\Controller as ControllerBase;
use Igvs\Courses\Classes\PHPClass2WSDL;
//use PHP2WSDL\PHPClass2WSDL;
use Symfony\Component\HttpFoundation\Request;


class Soap extends ControllerBase
{
    public $publicActions = ['pilotOauth', 'pilotOauthFinish'];

    private $wsdl_class;
    private $service_uri = 'http';
    private $wsdl_uri = 'http';

    public function test()
    {

        // проверяем наличие класса SoapClient
        if (class_exists('SoapClient')){

            // отключаем кэширование
            ini_set("soap.wsdl_cache_enabled", "0" );

            // подключаемся к серверу
            $client = new \SoapClient(
                "https://igvs-cms-dev.academia-moscow.ru/api/2/wsdl.wsdl",
//                "https://igvs-new/api/2/wsdl.wsdl",
//                "https://i-gvs-cms-dev-1/api/2/wsdl.wsdl",
                array(
                    'trace' => true,
                    "login" => "pilot-test-login", // логин
                    "password" => "123456", // пароль
                )
            );

            // обращаемся к функции, передаем параметры

            try {
                $result = $client->updateUsers([
                    'USERS' => ['1', '2', '3'],
                    'INSTANCE_ID' => '123'
                ]);
                echo 'result:<br>';
                var_dump($result);
            } catch (\Exception $e) {
                echo 'error<br>';
                $er = $client->__getLastResponse();
                var_dump($er);
                echo '<br>';
                var_dump($client->__getLastResponseHeaders());
                echo '<br>';
                var_dump($client->__getFunctions());
            }

            exit();

        } else echo "Включите поддержку SOAP в PHP!";

    }

    public function wsdl()
    {
        try {
            $this->wsdl_class = 'Igvs\Courses\Classes\Wsdl';
            $this->service_uri = "https://{$_SERVER['SERVER_NAME']}/api/2";

            $wsdlGenerator = new PHPClass2WSDL($this->wsdl_class, $this->service_uri);
            $wsdlGenerator->generateWSDL(true);

            $wsdl_content = $wsdlGenerator->dump();

            $wsdl_local_path = __DIR__ . '/../../../../storage/app/igvs/soap_wsdl.wsdl';
            file_put_contents($wsdl_local_path, $wsdl_content);

            return response($wsdl_content, 200)
                ->header('Content-Type', 'text/xml; charset=UTF-8');
        } catch (\Exception $e) {
            file_put_contents(__DIR__ . '/../../../../soap_wsdl.log', date('Y-m-d H:i:s') . " error: " . print_r($e, true) . "\r\n", FILE_APPEND);
        }

    }

    public function api()
    {
        try {

            $this->wsdl_uri = \Config::get('app.url') . '/api/2/wsdl.wsdl';
            $wsdl_local_path = __DIR__ . '/../../../../storage/app/igvs/soap_wsdl.wsdl';

            if (!file_exists($wsdl_local_path)) {
                return new \Exception('Error 101');
            }

            $soap_server = new SoapServer($wsdl_local_path);
            $soap_server->setClass('Igvs\Courses\Classes\Wsdl');

            $soap_server->handle();

        } catch (\Exception $e) {
            file_put_contents(__DIR__ . '/../../../../soap_wsdl_api.log', date('Y-m-d H:i:s') . " error: " . print_r($e, true) . "\r\n===========\r\n", FILE_APPEND);
        }
    }

}

?>
