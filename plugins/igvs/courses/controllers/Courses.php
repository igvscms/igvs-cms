<?php namespace Igvs\Courses\Controllers;

use Flash;
use Igvs\Courses\Models\Vocabulary;
use Redirect;
use Backend;
use BackendMenu;
use Backend\Classes\Controller;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\ModuleTheme;
use Igvs\Courses\Models\Member;
use Igvs\Courses\Models\MemberRole;
use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Status;
use Illuminate\Support\Facades\Session;
use Db;
use Input;
use Response;
use Request;
use View;
use Exception;
use BackendAuth;
use Queue;
use Academy\Cms\Models\Comment;
use Igvs\Courses\Models\UserPilot;

/**
 * Courses Back-end Controller
 */
class Courses extends Controller
{
    use \Backend\Traits\FormModelSaver;

    use \Igvs\Courses\Traits\CustomRoutes;
    use \Igvs\Courses\Traits\CheckAccess;
    use \Igvs\Courses\Traits\AntiCache;

    use \Academy\System\Traits\SocketSender;

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.RelationController',
        'Academy.System.Behaviors.CrudController',
        'list' => 'Academy.System.Behaviors.ListController',
        'notifier' => 'Academy.System.Behaviors.Notifier',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = ['igvs.courses.courses'];

    private function userFromElerning()
    {
        if (!($user = \BackendAuth::getUser()) || !$user->hasPermission('igvs.courses.pilot_user')) {
            return false;
        }
        return !!UserPilot::where('user_id', $user->id)->limit(1)->count();//->where('instance_id', 1)
    }

    public function __construct()
    {
        if (get('old')) {
            $this->implement['list'] = 'Academy.Cms.Behaviors.ListController';
        }
        if ($this->userFromElerning()) {
            unset($this->implement['notifier']);
        }
        $user = BackendAuth::getUser();

        //$this->implement[] = 'Academy.System.Behaviors.ListController';

        if ($user && $user->hasPermission('igvs.courses.pilot_user') && $user->hasPermission('igvs.courses.bag_method')) {
            $this->listConfig = 'config_list_pilot.yaml';
        }

        parent::__construct();

        BackendMenu::setContext('Igvs.Courses', 'courses', 'courses');

        $this->addCss('/plugins/igvs/courses/assets/css/course-progress.css');
        $this->addJs('/plugins/igvs/courses/assets/js/course-progress.js');
        $this->addJs('/plugins/igvs/courses/assets/js/formByIframe.js');
    }

    public function index()
    {
        $this->addCss('/plugins/igvs/courses/assets/css/course-list.css');

        $this->addJs('/plugins/academy/system/assets/vendors/autobahnjs/autobahn.min.js');
        $this->addJs('/plugins/academy/system/assets/vendors/kimmobrunfeldt/progressbar.min.js');

        $this->asExtension('ListController')->index();

        $this->vars['user'] = BackendAuth::getUser();
    }

    public function create() {
        $backendUriExpertise = \Config::get('cms.backendUriExpertise', 'backend');
        $is_expertise = strpos(\Request::getRequestUri(), $backendUriExpertise) !== false;

        if ($is_expertise) {
            $url = \Backend::url($backendUriExpertise . '/courses');
            header('Location: ' . $url);
            exit();
        }

        return $this->asExtension('FormController')->create();
    }

    public function beforeCreate()
    {
        if (!$this->user->hasAccess('igvs.courses.create'))
            return false;
    }

    public function update($recordId = null, $context = null)
    {
        // check assess
        if (!$this->checkAccessCourse($recordId)) {
            return Response::make(View::make('backend::access_denied'), 403);
        }

        $this->layoutPath = ['plugins/igvs/courses/controllers/contents'];
        $this->addJs('/plugins/igvs/courses/assets/js/course-form.js');
        $this->addCss('/plugins/igvs/courses/assets/css/course-form.css');

        $course = Course::find($recordId);

        if ($this->user->hasPermission('igvs.courses.pilot_user')
            && $this->user->hasPermission('igvs.courses.bag_method')
            && !count($course->getRoles([1, 5, 6, 9, 10, 11, 12]))
        ) {
            return Response::make(View::make('backend::access_denied'), 403);
        }
        if (!Session::get('igvs.courses.closethemewarning.' . $recordId)) {
            $this->vars['no_theme_modules'] = ModuleContent::getModulesClearThemes($recordId, $course->theme);
        }
        $this->vars['course'] = $course;
        return $this->asExtension('FormController')->update($recordId, $context);
    }

    public function update_onSave($recordId, $context = null)
    {
        if (!$this->checkAccessOperation($recordId, 'CourseProperty')) {
            throw new Exception('Access denied');
        }

        return $this->asExtension('FormController')
            ->update_onSave($recordId, $context);
    }

    public function update_onChangeThemeCheck($id)
    {
        if (Input::get('theme_name') == '_default'
            || !count($no_theme_modules = ModuleContent::getModulesClearThemes($id, Input::get('theme_name')))
        ) {
            return '';
        }
        $this->vars['course'] = Course::find($id);

        $return = $this->makePartial('change_theme_check', [
            'no_theme_modules' => $no_theme_modules,
        ]);
        if (strlen($return)) {
            Session::put("igvs.courses.closethemewarning.$id", false);
        }
        return $return;
    }

    public function copy($id)
    {
        if (!$this->checkAccessCourse($id)) {
            throw new Exception ('Access denied');
        }
        $this->asExtension('CrudController')->runButton = \Lang::get('igvs.courses::lang.default.command.create');
        $this->pageTitle = \Lang::get('igvs.courses::lang.course.command.copy');
        return $this->asExtension('CrudController')->crudupdate($id, 'copy');
    }

    protected function getStatusId($course, $pilot, $pilot_bag_method, $status_id = null)
    {
        if (!$course->short_version && !($pilot && $pilot_bag_method)) {
            return;
        }
        $course->status_id = null;
        $statuses = $course->getStatusOptions();
        if (!count($statuses)) {
            return;
        }
        if (isset($statuses[$status_id])) {
            return $status_id;
        }
        reset($statuses);
        return key($statuses);
    }

    protected function getType($course, $pilot, $pilot_bag_method, $create_course, $type = null)
    {
        if (!$pilot || $pilot_bag_method) {
            return 'new';
        }
        if (!$create_course) {
            return 'mod';
        }
        if (in_array($type, ['new', 'mod'])) {
            return $type;
        }
    }

    public function copy_onSave($course_id)
    {
        $course_id = (int) $course_id;
        if (!$this->checkAccessCourse($course_id)) {
            throw new Exception('Access denied');
        }
        $course = Course::findOrFail($course_id);
        $course_code = $course->code;
        $formController = $this->asExtension('FormController');
        $formController->initForm($course, 'copy');
        $data = (object) $formController
            ->formGetWidget()
            ->getSaveData();

        $course->id = null;
        $course->title = $data->title;
        $course->code = $data->code;
        $course->validate();

        $pilot = $this->user && $this->user->hasPermission('igvs.courses.pilot_user');
        $pilot_bag_method = $this->user && $this->user->hasPermission('igvs.courses.bag_method');
        $create_course = $this->user && $this->user->hasAccess('igvs.courses.create');

        $data->title = trim($data->title);
        $data->code = trim($data->code);
        $data->type = $this->getType($course, $pilot, $pilot_bag_method, $create_course, isset($data->type) ? $data->type : null);
        $data->status_id = $this->getStatusId($course, $pilot, $pilot_bag_method, isset($data->status_id) ? $data->status_id : null);
        $data->server_name = $_SERVER['SERVER_NAME'];

        if (is_null($data->type)) {
            Flash::error(trans('validation.required', ['attribute' => 'type']));
            return;
        }

        $task = \Igvs\Courses\Models\Job::create([
            'user_id' => $this->user->id,
            'type' => 'copying-course',
            'model_name' => Course::class,
            'model_id' => $course_id,
            'title' => 'Copying course "'.$course_code.'".',
            'comment' => 'igvs.courses::course.message.waiting',
            'status' => 'waiting',
            'progress' => 0,
        ]);
        $task_id = $task->id;
        $data = (array) $data;

        Queue::pushOn('copying-courses', function ($job) use ($data, $course_id, $task_id) {

            // begining ...
            $task = \Igvs\Courses\Models\Job::find($task_id);

            $task->fill([
                'comment' => 'Start copying',
                'status' => 'processing',
                'viewed' => null
            ]);

            $task->save();

            // push event
            $send = function ($task) {
                \Academy\System\Classes\Socket\Pusher::sentDataToServer([
                    'topic_id' => 'user_' . $task->user_id,
                    'data' => $task->toArray()
                ]);
            };

            // copying ...
            try {

                // push event
                $send($task);

                $course = Course::find($course_id);
                $course->duplication([
                    'modification_name' => '',
                    'title' => $data['title'],
                    'code' => $data['code'],
                    'type' => $data['type'],
                    'status_id' => $data['status_id'],
                    'server_name' => $data['server_name'],
                ], $task);

                // successful
                $task->fresh();
                $task->fill([
                    'comment' => 'Copying completed successfully',
                    'status' => 'successful',
                    'viewed' => null
                ]);
                $task->save();
                $send($task);
                $job->delete();
            }
            catch (\Exception $e) {

                $job->delete();

                $err = print_r([
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage()
                ], true);

                $task->fill([
                    'comment' => 'Error. ' . $e->getMessage(),
                    'error' => $err,
                    'status' => 'fail',
                    'viewed' => null
                ]);

                $task->save();
                $send($task);

                throw $e;
            }
        });
        return trans('igvs.courses::course.message.waiting');
    }

    public function onDelete()
    {
        if (!$this->checkAccessCourse(post('recordId'))) {
            Flash::error('Access denied');
            return Redirect::refresh();
        }

        Flash::success(trans('igvs.courses::course.message.successfully_deleted_records'));
        $course = Course::find(post('recordId'));
        $course->delete();
        return Redirect::refresh();
    }

    public function update_onCloseThemeWarning($id) {
        Session::put('igvs.courses.closethemewarning.' . $id, true);
    }

    public function onRefreshVersionList()
    {
        $modules = ModuleContent::with('module', 'module.versions')
            ->where('course_id', post('course_id'))
            ->get();

        $params = [
            'modules' => $modules,
            'date' => post('date')
        ];

        return [
            '#version-list' => $this->makePartial('version_list', $params)
        ];
    }

    public function update_version($course_id)
    {
        $this->layout = 'plugins/academy/system/layouts/clear';
        return $this->makePartial('update_version', [
            'formModel' => Course::find($course_id)
        ]);
    }

    public function onUpdateVersion()
    {
        $versions = post('versions');

        foreach ($versions as $v) {
            $module = ModuleContent::findOrFail($v['module_id']);
            $module->version = $v['version'];
            $module->save();
        }

        return $this->onRefreshVersionList();
    }

    public function onRefreshVersions()
    {
        set_time_limit(60 * 30);
        $user = BackendAuth::getUser();
        if (!$user->is_superuser)
            throw new Exception('Access denied');

        $courses = post('checked');

        try {

            foreach ($courses as $course_id) {

                $callback = function ($contents) {
                    foreach ($contents as $content) {
                        $content->force_history_save = true;
                        $content->version = $content->module->getLatestAvailable($content->getModuleVersion());
                        $content->save();
                    }
                };

                ModuleContent::where('course_id', $course_id)
                    ->chunk(100, $callback);
            }

            Flash::success('Successfully updated');

        } catch (Exception $e) {
            Flash::error('Error updated' /*. $e->getMessage()*/);
        }

        return Redirect::refresh();
    }

    public function onImportDict($course_id)
    {
        $post = post('dict_file');
        $request = $_REQUEST;

        $course = Course::find($course_id);

        if (!$this->checkAccessCourse($course_id))
            throw new Exception ('Access denied');

        return $this->makePartial('import_dict', [
            'course' => $course,
        ]);
    }

    public function onClearImportFromIntro($course_id)
    {
        if (!$this->checkAccessCourse($course_id))
            throw new Exception ('Access denied');

//        return [
//            '#Courses-update-RelationController-vocabulary-view' =>  $this->makePartial('field_vocabulary', [
//            '#Courses-update-RelationController-vocabulary-view' =>  $this->relationRender('vocabulary')
//        ];
        return $this->makePartial('import_dict_from_intro', [
            'course_id' => $course_id,
        ]);
    }

    public function onClearImportFromIntroSuccess()
    {
        $course_id = post('course_id');

        if (!$this->checkAccessCourse($course_id))
            throw new Exception ('Access denied');

        $course = Course::find($course_id);

        if (!$course)
            return [
                "#updateContainer" => $this->makePartial('output', [
                    'text' => empty($ouput) ? 'Курс не найден!' : '',
                    'output' => ''
                ])
            ];

        //чистим словарь
        $vocabularies = $course->vocabulary()->get();

        foreach ($vocabularies as $vocabulary) {
            $vocabulary->delete();
        }

        //ищем модули intro
        $intro_template_id = Module::where('path', 'intro')->first();

        if (!$intro_template_id)
            return [
                "#updateContainer" => $this->makePartial('output', [
                    'text' => empty($ouput) ? 'Шаблон Intro не найден!' : '',
                    'output' => ''
                ])
            ];

        $intro_template_id = $intro_template_id->id;

        $modules_content = ModuleContent::where('course_id', $course_id)
            ->where('module_id', $intro_template_id)
            ->get();

        //добавляем термины в словарь
        foreach ($modules_content as $module) {
            try {

                $json = json_decode($module->data);
                foreach ($json->questions as $question) {
                    if ($question->typeTest == 'intro_keywords') {
                        foreach ($question->container->table as $words) {
                            foreach ($words as $word) {
                            try {

                                Vocabulary::create([
                                    'course_id' => $course_id,
                                    'term' => $word->text,
                                    'description' => $word->popup->text,
                                ]);

                            } catch(\Exception $e) {}
                            }
                        }
                    }
                }

            } catch(\Exception $e) {}
        }

        return [
            "#updateContainer" => $this->makePartial('output', [
                'text' => empty($ouput) ? 'Импорт успешно завершен' : '',
                'output' => ''
            ])
        ];
    }

    function listExtendColumns($list)
    {
        if (!$this->user->is_superuser)
            $list->showCheckboxes = false;

        if(!$this->user->hasPermission('igvs.courses.expertise'))
            $list->removeColumn('tasks');

        if($this->user->hasPermission('igvs.courses.pilot_user')
            && $this->user->hasPermission('igvs.courses.bag_method')) {
            $list->removeColumn('type');
            // $list->removeColumn('parent_code');
            $list->removeColumn('status');
            $list->removeColumn('public_status');
            // $list->removeColumn('language');
            // $list->removeColumn('country');
            // $list->removeColumn('user');
            // $list->removeColumn('level_qualification');
            // $list->removeColumn('launch_date');
            // $list->removeColumn('updated_at');
        }

        if(!$this->user->hasPermission('igvs.courses.bag_method')) {
            $list->removeColumn('status_id');
            $list->removeColumn('developer_name');
        }

        $backendUriExpertise = \Config::get('cms.backendUriExpertise', 'backend');
        $is_expertise = strpos(\Request::getRequestUri(), $backendUriExpertise) !== false;
        $plugin_author_name = $is_expertise ? $backendUriExpertise : 'igvs';

        $list->recordUrl = $plugin_author_name . '/courses/:id/categories';
    }

    public function listExtendQuery($query)
    {
        $user_id = $this->user->id;

        $copied = \Igvs\Courses\Models\Job::where('type', 'copying-course')
            ->whereIn('status', ['waiting', 'processing'])
            ->whereNotNull('target_id')
            ->lists('target_id');

        $query->getDeveloper();
        $query->getResponsible();

        // не показываем курсы которые копируются
        $query->whereNotIn('id', $copied);

        $pilot_user = Session::get('igvs.courses.pilot_user.' . $user_id);

        if (!$this->user->is_superuser) {

            $query->where(function ($query) use ($pilot_user, $user_id) {

                $query->whereHas('members', function ($query) use ($user_id) {
                    $query->where('user_id', $user_id);
                    $query->has('roles');
                });

                if ($pilot_user) {
                    $query->OrWhereIn('id', Session::get('igvs.courses.pilot_user.courses.' . $user_id));
                }

            });
        }

    }

    public function minicreate()
    {
        return $this->asExtension('CrudController')->crudcreate('minicreate');
    }

    public function minicreate_onSave()
    {
        return $this->asExtension('CrudController')->crudcreate_onSave();
    }

    public function initRelation($model, $field = null)
    {
        $result = parent::initRelation($model, $field);

        switch ($field ?: post('_relation_field')) {
            case 'members':
                $this->relationModel->course_id = $model->id;
                break;
            case 'comments':
                $this->relationModel->data = $model->status_id;
                break;
            case 'working_programm_comments':
                $this->relationModel->relation = 'working_programm_comments';
                break;
        }
        return $result;
    }

    public function onUpdateNewComment($id)
    {
        if (!$course = Course::find($id)) {
            return;
        }
        $comment = new Comment();
        $comment->data = $course->status_id;
        $course->comments()->add($comment);
        $_POST['_relation_field'] = 'comments';
        $_POST['_relation_extra_config'] = 'W10=';
        $_POST['manage_id'] = $comment->id;
        return parent::onRelationButtonUpdate();
    }

    public function getHeadValueForExportCsv($column, $result, $relation)
    {
        if ($relation != 'comments' || $column->columnName != 'data') {
            return $result;
        }
        return trans('igvs.courses::lang.default.status.status');
    }

    public function getColumnValueForExportCsv($column, $record, $result, $relation)
    {
        if ($relation != 'comments' || $column->columnName != 'data') {
            return $result;
        }
        if ($status = Status::find($record->data)) {
            return $status->name;
        }
    }

    public static function quotes() {
        $arr = func_get_args();
        if (count($arr) == 1 && is_array($arr[0])) {
            $arr = $arr[0];
        }
        foreach ($arr as &$elem) {
            $elem = str_replace('"', '""', mb_convert_encoding($elem, 'Windows-1251'));
            //какие переносы может учитывать spss ??
        }
        return '"' . implode('";"', $arr) . '"' . "\r\n";
    }

    public static function flush_quotes()
    {
        echo call_user_func_array('self::quotes', func_get_args());
        flush();
    }

    private static function getUserString($user, $id)
    {
        if (!$user) {
            return [
                $id,
                '',
                '',
                '',
                '',
                '',
                '',
                '',
            ];
        }
        $result = [
            $user->id,
            $user->first_name,
            $user->last_name,
            $user->login,
            $user->email,
        ];
        if ($user->pilot) {
            $result[] = $user->pilot->id;
            $result[] = $user->pilot->cluster_id;
            $result[] = $user->pilot->instance_id;
        }
        else {
            $result[] = '';
            $result[] = '';
            $result[] = '';
        }
        return $result;
    }

    public function csv()
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="allCoursesList.csv"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        self::flush_quotes([
            'ID',
            trans('igvs.courses::lang.default.txt_field.name'),
            trans('igvs.courses::lang.default.txt_field.code'),
            trans('igvs.courses::lang.course.txt_field.parent_code'),
            trans('igvs.courses::lang.default.txt_field.running_title'),
            trans('igvs.courses::lang.default.status.group'),
            trans('igvs.courses::lang.default.status.status'),
            trans('igvs.courses::lang.course.txt_field.modification_name'),
            trans('igvs.courses::lang.default.status.public'),
            trans('igvs.courses::lang.course.txt_field.color_theme'),
            trans('igvs.courses::lang.default.txt_field.shell_version'),
            trans('igvs.courses::lang.course.txt_field.include_all_themes'),
            trans('igvs.courses::lang.course.txt_field.show_module_start_screen'),
            trans('igvs.courses::lang.course.txt_field.constructor_all_privileges'),
            trans('igvs.courses::lang.course.txt_field.is_base_course'),
            trans('igvs.courses::lang.default.location.language'),
            trans('igvs.courses::lang.default.location.country'),
            trans('igvs.courses::lang.course.txt_field.qcfl'),
            trans('igvs.courses::course.field.member_id'),
            trans('igvs.courses::course.field.first_name'),
            trans('igvs.courses::course.field.last_name'),
            trans('backend::lang.user.login'),
            trans('backend::lang.user.email'),
            trans('igvs.courses::course.field.pilot id'),
            trans('igvs.courses::course.field.cluster id'),
            trans('igvs.courses::course.field.instance id'),
            trans('igvs.courses::member.field.roles'),
        ]);

        $limit = 100;
        $offset = 0;
        while (count($courses = Course::limit($limit)->offset($offset)->get())) {
            $offset += $limit;
            foreach ($courses as $course) {
                $courseFileds = [
                    $course->id,
                    $course->title,
                    $course->code,
                    $course->parent ? $course->parent->code : '',
                    $course->colontitle_slogan,
                    $course->type,
                    $course->status,
                    $course->modification_name,
                    $course->public_status,
                    $course->theme,
                    $course->shell_version ? $course->shell_version->version : '',
                    (int)$course->include_all_themes,
                    (int)$course->show_module_start_screen,
                    (int)$course->constructor_all_privileges,
                    (int)$course->is_base_course,
                    $course->language,
                    trans("igvs.courses::lang.default.location.$course->country"),
                    $course->level_qualification,
                ];
                if (!count($course->members)) {
                    self::flush_quotes($courseFileds);
                }
                foreach ($course->members as $member) {
                    $roles = [];
                    foreach ($member->roles as $role) {
                        $roles[] = $role->name;
                    }
                    $userFileds = self::getUserString($member->user, $member->user_id);
                    //dd(array_merge($courseFileds, [implode(', ', $roles)], $userFileds));
                    self::flush_quotes(array_merge($courseFileds, $userFileds, [implode(', ', $roles)]));
                }
            }
        }
        die;
    }

    public function formExtendFieldsBefore($form)
    {
        if (\Session::get('locale') === 'ru') {
            foreach ($form->fields as &$field) {
                if (isset($field['type']) && $field['type'] === 'datepicker') {
                    $field['firstDay'] = 1;
                }
            }
            foreach ($form->tabs['fields'] as &$field) {
                if (isset($field['type']) && $field['type'] === 'datepicker') {
                    $field['firstDay'] = 1;
                }
            }
            unset($field);
        }
        if ($active_tab = get('q')) {
            switch ($active_tab) {
                case 'comments': $active_tab = trans('igvs.courses::course.field.' . $active_tab);
                    break;
            }
            $form->tabs['defaultTab'] = $active_tab;
        }
    }

    public function onDropDownAjax()
    {
        $relation = Request::input('relation');
        $count = Request::input('count', 20);
        $page = Request::input('page', 1);
        $relationModel = new Course();

        if (method_exists($relationModel, "getRelation{$relation}Options")) {
            return json_encode(call_user_func_array([$relationModel, "getRelation{$relation}Options"], [$count, $page]));
        }
        $relationModel = $relationModel->makeRelation($relation);
        if (!$relationModel) {
            return 'error relation';
        }
        $keyName = $relationModel->getKeyName();
        $name_from = Request::input('name_from', 'name');

        if ($selection = Request::input('selection')) {
            $area_prof_activity = $relationModel->select($keyName, Db::raw($selection . ' AS ' . $name_from));
        }
        else {
            $area_prof_activity = $relationModel->select($keyName, $name_from);
        }

        $area_prof_activity = $area_prof_activity->skip($count * ($page - 1));

        if ($value = Request::input('value')) {
            $area_prof_activity = $area_prof_activity->whereRaw("{$selection} like '%$value%'");
            $area_prof_activity_count = $relationModel->whereRaw("{$selection} like '%$value%'");
        }
        else {
            $area_prof_activity_count = $relationModel;
        }
        if ($relation === 'parent') {
            $this->listExtendQuery($area_prof_activity);
        }
        $result_array = [];
        foreach ($area_prof_activity->take($count)->get() as $value) {
            $result_array[] = ['id'=> $value[$keyName], 'text' => $value[$name_from]];
        }
        return json_encode([
            'total_count' => $area_prof_activity_count->count(),
            'items' => $result_array
        ]);
    }

    public function materials_update($recordId = null, $context = null)
    {
        $this->layout = 'plugins/academy/system/layouts/clear';

        // check assess
        if (!$this->checkAccessCourse($recordId))
            return Response::make(View::make('backend::access_denied'), 403);

        $this->addJs('/plugins/igvs/courses/assets/js/course-form.js');
        $this->addCss('/plugins/igvs/courses/assets/css/course-form.css');

        $course = Course::find($recordId);

        if ((!in_array(1, $course->getRoles())
            && $this->user->hasPermission('igvs.courses.pilot_user'))
            && (($this->user->hasPermission('igvs.courses.pilot_user')
                && $this->user->hasPermission('igvs.courses.bag_method'))
                && !count(array_intersect([9, 10, 11, 12,], $course->getRoles()))
            )
        ) {
            return Response::make(View::make('backend::access_denied'), 403);
        }
        return $this->asExtension('FormController')->update($recordId, 'materials');
    }

    public function materials_update_onSave($recordId, $context = null)
    {
        return $this->asExtension('FormController')
            ->update_onSave($recordId, 'materials');
    }

    public function countsTable($recordId)
    {
        $this->layout = 'plugins/academy/system/layouts/clear';
        $this->addCss('/plugins/igvs/courses/assets/css/counts_table.css');

        // check assess
        if (!$this->checkAccessCourse($recordId))
            return Response::make(View::make('backend::access_denied'), 403);

        $report = new \Igvs\Courses\Classes\Reports\CourseQuestionAmount($recordId);
        $report->getReport();

        return $this->makePartial('amount_questions', [
            'topics' => $report->topics,
            'count_modules_course' => $report->count_modules_course,
            'count_modules_topic' => $report->count_modules_topic,
            'count_modules_behavior_topic' => $report->count_modules_behavior_topic,
            'count_modules_behavior_course' => $report->count_modules_behavior_course,
            'count_questions_topics' => $report->count_questions_topics,
            'count_questions_behavior_topic' => $report->count_questions_behavior_topic,
            'count_questions_behavior_course' => $report->count_questions_behavior_course,
            'count_questions_course' => $report->count_questions_course,
            'behavior_types' => $report->behavior_types,
        ]);
    }

    public function countsTableXls($recordId)
    {
        // check assess
        if (!$this->checkAccessCourse($recordId))
            return Response::make(View::make('backend::access_denied'), 403);

        $report = new \Igvs\Courses\Classes\Reports\CourseQuestionAmount($recordId);
        $report->getReport();

        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=amounts_{$recordId}.xls");


        echo $this->makePartial('amount_questions', [
            'topics' => $report->topics,
            'count_modules_course' => $report->count_modules_course,
            'count_modules_topic' => $report->count_modules_topic,
            'count_modules_behavior_topic' => $report->count_modules_behavior_topic,
            'count_modules_behavior_course' => $report->count_modules_behavior_course,
            'count_questions_topics' => $report->count_questions_topics,
            'count_questions_behavior_topic' => $report->count_questions_behavior_topic,
            'count_questions_behavior_course' => $report->count_questions_behavior_course,
            'count_questions_course' => $report->count_questions_course,
            'behavior_types' => $report->behavior_types,
        ]);

        die;
    }
}
