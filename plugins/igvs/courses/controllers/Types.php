<?php namespace Igvs\Courses\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Roles Back-end Controller
 */
class Types extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['igvs.courses.types'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Igvs.Courses', 'courses', 'types');
    }

    public function myaction()
    {
        dd(['hello, world']);
    }
}