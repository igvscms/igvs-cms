<?php namespace Igvs\Courses\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Temp Link Back-end Controller
 */
class TempLink extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        $this->implement[] = 'Academy.Cms.Behaviors.ListController';

        parent::__construct();

        BackendMenu::setContext('Igvs.Courses', 'courses', 'templink');
    }
}
