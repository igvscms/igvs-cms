<?php namespace Igvs\Courses\Controllers;

use View;
use Response;
use BackendAuth;
use BackendMenu;
use Lang;
use Backend\Classes\Controller;
use Exception;
use Config;
use Igvs\Courses\Models\RubricatorHistory;
use Igvs\Courses\Models\Category;
//use Igvs\Courses\Models\File;
//use Igvs\Courses\Models\ModuleContentHistory;

/**
 * Contents Back-end Controller
 */
class RubricatorHistorys extends Controller
{
    use \Igvs\Courses\Traits\CheckAccess;

    public $implement = [
        'Backend.Behaviors.ListController',
    ];

    public $listConfig = 'config_list.yaml';

    public $course_id = 0;

    public function view($course_id=0)
    {
        $this->layoutPath = ['plugins/igvs/courses/layouts'];
        $this->course_id = $course_id;
        return $this->asExtension('ListController')->index();
    }

    public function listExtendQuery($query)
    {
        $categories = Category::where('course_id', $this->course_id)->select('id')->lists('id');
        $query->whereIn('category_id',$categories);
    }

    public static function getAfterFilters()
    {
        return [];
    }

    public static function getBeforeFilters()
    {
        return [];
    }

    public static function getMiddleware()
    {
        return [];
    }

    public function callAction($method, $parameters = false)
    {
        return $this->run($method, $parameters);
    }
}
