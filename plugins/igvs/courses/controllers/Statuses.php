<?php namespace Igvs\Courses\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

class Statuses extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['igvs.courses.statuses'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Igvs.Courses', 'courses', 'statuses');
    }
}