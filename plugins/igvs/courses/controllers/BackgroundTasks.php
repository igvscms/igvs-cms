<?php namespace Igvs\Courses\Controllers;

use Flash;
use Redirect;
use BackendMenu;
use Backend\Classes\Controller;
use Igvs\Courses\Models\BackgroundTask;

/**
 * Background Tasks Back-end Controller
 */
class BackgroundTasks extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Igvs.Courses', 'courses', 'background_tasks');
    }

    public function onUpdateList()
    {
//        $this->asExtension('ModalController')->index_onUpdate();

        return $this->asExtension('ListController')->index();
    }

    public function listExtendQuery($query)
    {
        $query->withTrashed();
    }

    public function onCancel()
    {
        Flash::success('Successfully canceled the task.');

        $task_id = post('id');
        if ($task = BackgroundTask::find($task_id)) {
            $task->delete();
        }
        return Redirect::refresh();
    }
}