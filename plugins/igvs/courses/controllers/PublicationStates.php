<?php namespace Igvs\Courses\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

/**
 * Builds Back-end Controller
 */
class PublicationStates extends Controller
{
    public $implement = [
        'Backend.Behaviors.ListController'
    ];

    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['igvs.courses.builds'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Igvs.Courses', 'courses', 'publicationstates');
    }
}
