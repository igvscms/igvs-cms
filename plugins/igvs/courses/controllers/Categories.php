<?php namespace Igvs\Courses\Controllers;

use Backend\Facades\Backend;
use Igvs\Courses\Classes\ExportHelper;
use Igvs\Courses\Classes\ExportStructureDiffHelper;
use Igvs\Courses\Classes\OperatorsActions;
use Igvs\Courses\Classes\PublicationState;
use System\Models\Parameter;
use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Database\Eloquent\Builder;
use Form;
use Input;
use Config;
use Session;
use Response;
use View;
use Exception;
use Lang;

use PHPExcel_IOFactory;

use Igvs\Courses\Widgets\CategoriesWidget;
use Igvs\Courses\Controllers\Contents;
use Igvs\Courses\Models\Build;
use Igvs\Courses\Models\User;
use Igvs\Courses\Models\Info;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\PilotAuth;
use Igvs\Courses\Models\UserPilot;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\TempLink;

use Academy\Cms\Models\Comment;

/**
 * Categories Back-end Controller
 */
class Categories extends Controller
{
    use \Igvs\Courses\Traits\CustomRoutes;
    use \Igvs\Courses\Traits\FileHelper;
    use \Igvs\Courses\Traits\Scorm;
    use \Igvs\Courses\Traits\PrepareForFtp;
    use \Igvs\Courses\Traits\ExportToReleaseCloud;
    use \Igvs\Courses\Traits\ExportToPilot;
    use \Igvs\Courses\Traits\CheckAccess;
    use \Igvs\Courses\Traits\QueueCommand;
    use \Igvs\Courses\Traits\ImportRubricator;
    use \Igvs\Courses\Traits\ImportCourse;
    use \Igvs\Courses\Traits\AntiCache;
    use \Igvs\Courses\Traits\CourseHelper;

    use \Academy\System\Traits\SocketSender;

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Igvs.Courses.Behaviors.ModalController',
        'Academy.System.Behaviors.CrudController',
        'notifier' => 'Academy.System.Behaviors.Notifier',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    //public static $notifierConfig = 'config_notifier.yaml';

    private function userFromElerning()
    {
        if (!($user = \BackendAuth::getUser()) || !$user->hasPermission('igvs.courses.pilot_user')) {
            return false;
        }
        return !!UserPilot::where('user_id', $user->id)->limit(1)->count();//->where('instance_id', 1)
    }

    public function __construct()
    {
        if ($this->userFromElerning()) {
            unset($this->implement['notifier']);
        }
        parent::__construct();

        $categories_widget = new CategoriesWidget($this);
        $categories_widget->bindToController();
    }

    //при каких условиях и что отправлять
    public static function rulesForSendingSocketsMessages() { return [
        //'categoriesWidget::onAddModulesToBuild',
        //'categoriesWidget::onChangeCurrentBuild',
        //'categoriesWidget::onDeleteCategory',
        //'categoriesWidget::onDeleteModule',
        //'categoriesWidget::onMoveCategory',
        //'categoriesWidget::onMoveModule',
        //'categoriesWidget::onPasteModule',
        //'categoriesWidget::onPasteAct',
        //'onCreateModule',
        //'onRefreshListTree',
        //'onCreate',
        'onUpdateRubricator',
        //'onSendNotifierNewCourse' => function() { return true;}
    ];}

    //преобразование результата
    public static function socketMessageResultHandler() { return [
        'onUpdateRubricator' => function($data, $result)
        {
            $cw = new CategoriesWidget(null);
            return ['original' => $cw->makePartialTree($data['params']['id'])];
        }
    ];}


    function onUpdateRubricator($course_id = null)
    {
        $options = ['result' => 1];
        if (!is_null($course_id)) {
            $options['params']['id'] = $course_id;
        }
        $this->socketNewSend('onUpdateRubricator', $options);
    }
    /**
     * Displays the menu items in a tree list view so they can be reordered
     */

    public function strip_tags($list)
    {
        foreach($list as $key => $value) {
            $list[$key] = $string = preg_replace("(<[^<>]+>)", ' ', $value);
        }
        return $list;
    }

    public function index($course_id)
    {
        // check assess
        if (!$this->checkAccessCourse($course_id))
            return Response::make(View::make('backend::access_denied'), 403);

        $course = Course::find($course_id);

        if (!$course) {
            header('Location: /error');
            exit();
        }

        $this->pageTitle = preg_replace("/(<br\s*?\\*?\/*?>|&nbsp;)/i", " ", $course->code.': '.$course->title.($course->modification_name != '' ? ' ('.$course->modification_name.')' : '' ));

        $this->vars['commit_date'] = $this->getCommitDate();
        $this->vars['course'] = $course;

        $this->addJs('/plugins/igvs/courses/assets/js/categories.js?a=1');
        $this->addJs('/plugins/igvs/courses/assets/js/formByIframe.js');
        $this->addCss('/plugins/igvs/courses/assets/css/categories.css');

        $toolbarConfig = $this->makeConfig();
        $toolbarConfig->buttons = '$/igvs/courses/controllers/categories/_list_toolbar.htm';

        $this->vars['builds'] = Build::orderBy('name', 'asc')
            ->get()
            ->lists('name', 'id');

        $categories = Category::where('course_id', $course_id)
            ->get();

        $list = [];
        foreach ($categories as $v) {

            $tab = $v->parent_id ? 8 : 4;
            $separator = str_repeat('&nbsp;', $tab);

            $list[$v->id] = !$v->code ? $v->name :
                $v->code . ': ' . $v->name;

            $list[$v->id] = $separator . $list[$v->id];
        }

        $this->vars['topics'] = $this->strip_tags($list);
        $this->vars['toolbar'] = $this->makeWidget('Backend\Widgets\Toolbar', $toolbarConfig);
    }

    # extends Controller/Contents
    public function index_onCreateFormModule($course_id)
    {

        if (!$this->checkAccessOperation($course_id, 'AddModuleContent')) {
            throw new Exception('Access denied');
        }

        $c = new Contents;
        return $c->onCreateFormModule();
    }

    public function index_onCreateModule($course_id)
    {
        if (!$this->checkAccessOperation($course_id, 'AddModuleContent')) {
            throw new Exception('Access denied');
        }

        $c = new Contents;
        $c->asExtension('FormController')
            ->create_onSave();

        # Определение и вставка элемента с учетом сортировки
        $pos = post('ModuleContent.position');
        $new = $c->formGetModel();

        $stmt = ModuleContent::where('category_id', $new->category_id)
            ->where('course_id', $new->course_id);

        // Вставка после указанного элемента
        if (!in_array($pos, ['last', 'first', '0'])) {

            $model = $stmt->where('id', $pos)
                ->first();

            if ($model) {
                $new->moveAfter($model);
            }
        }
        // Вставка в начало
        else if ($pos == 'first') {
            $new->resort();
        }
        // Вставка в конец
        else {
            $sort = (int) $stmt->max('sort');
            ModuleContent::where('id', $new->id)
                ->update(['sort' => ++$sort]);
        }
        //$this->sendNotifierMessage('onSendNotifierNewCourse', 'Добавлен новый модуль "' . $new->name . '"');
        $this->onUpdateRubricator();
        /*return $this->widget->categoriesWidget
            ->makePartialTree(post('ModuleContent')['course_id']);*/
    }

    public function index_onLoadVersions()
    {
        $c = new Contents;
        return $c->onLoadVersions();
    }

    public function index_onPreview()
    {
        $model = Category::find(post('record_id'));

        return $this->asExtension('ModalController')->index_onPreview([
            'trans' => $model->parent_id ? 'previewact_title' : 'previewtopic_title'
        ]);
    }

    # ModalController extends

    /**
     * After create record, refresh the index list
     * @return mixed
     */
    public function changeFlashMessage($message, $type = 'success')
    {
        if ($result = \Flash::get($type)) {
            \Flash::forget($type);
        }
        \Flash::$type($message);
        return $result;
    }

    public function index_onCreate()
    {
        if (!$this->checkAccessCourse(post('Category[course_id]'))) {
            throw new Exception('Access denied');
        }
        $this->asExtension('ModalController')->index_onCreate();
        if (\Flash::success()
            && $this->user
            && $this->user->hasPermission('igvs.courses.bag_method')
            && post('Category[parent_id]')
        ) {
            $this->changeFlashMessage('Тема была успешно создана');
        }
        $course_id = $this->vars['formModel']->course_id;

        if (post('Category[parent_id]')) {
            Info::check('createAct');
        }
        else {
            Info::check('createTheme');
        }
        $this->onUpdateRubricator(post('Category[course_id]'));
        //return $this->widget->categoriesWidget->makePartialTree($course_id);
    }

    /**
     * Make the modal form for create record
     * @return mixed
     * @throws \SystemException
     */
    public function index_onCreateForm()
    {
        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception ('Access denied');

        if (!$this->user->hasPermission('igvs.courses.pilot_user')
            || !$this->user->hasPermission('igvs.courses.bag_method')) {
            $context = null;
        }
        else {
            $context = 'short';
        }
        return $this->asExtension('ModalController')->index_onCreateForm([
            'trans' => post('parent_id') ? 'createact_title' : 'createtopic_title',
            'context' => $context
        ]);
    }

    /**
     * After update record, refresh the index list
     * @return mixed
     */
    public function index_onUpdate()
    {
        $category = Category::find(post('record_id'));

        if (!$this->checkAccessCourse($category->course_id))
            throw new Exception('Assess denied');

        $this->asExtension('ModalController')->index_onUpdate();
        $course_id = $this->vars['formModel']->course_id;

        $this->onUpdateRubricator();

        //return $this->widget->categoriesWidget->makePartialTree($course_id);
    }

    /**
     * Make the modal form for update record
     * @return mixed
     * @throws \SystemException
     */
    public function index_onUpdateForm()
    {
        $model = Category::find(post('record_id'));

        if (!$this->checkAccessCourse($model->course_id))
            throw new Exception('Assess denied');

        if (!$this->user->hasPermission('igvs.courses.pilot_user')
            || !$this->user->hasPermission('igvs.courses.bag_method')) {
            $context = 'update';
        }
        else {
            $context = 'short';
        }
        if (!$model->isReadonly()
            || ($model->readonly && $model->checkAccessOperation($model->course_id, 'UncheckReadonlyCategory'))
            || (!$model->readonly && $model->checkAccessOperation($model->course_id, 'CheckReadonlyCategory'))
        ) {
            $index_function = 'index_onUpdateForm';
        }
        else {
            $index_function = 'index_onPreview';
        }
        return $this->asExtension('ModalController')->$index_function([
            'trans' => $model->parent_id ? 'updateact_title' : 'updatetopic_title',
            'context' => $context
        ]);
    }

    // import rubricator form
    public function index_onImportCourseForm()
    {
        $arhive = Config::get('igvs.courses::scorm.arhive');

        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception('Assess denied');

        if (!$this->user->hasAccess('igvs.courses.import_course'))
            throw new Exception('Assess denied');

        return $this->makePartial('import_course', ['course_id' => post('course_id')]);
    }

    // import rubricator form
    public function index_onImportCourse()
    {
        $course_id = post('course_id');
        $storage_path = storage_path('app/media/');
        $inputFileName = $storage_path . post('filepath');
        if (!is_file($inputFileName)) {
            new \Exception("File not found");
        }
        if (!Course::find($course_id)) {
            new \Exception("Course with {$course_id} not found");
        }

        $options = [
            'courseId' => post('course_id'),
            'inputFileName' => $inputFileName,
        ];

        $this->pushTask('Import Course', 'ImportCourse', $options, $this->user->id);

        return 'Импорт курса поставлен в очередь операций';
    }

    // import rubricator form
    public function index_onImportRubricatorForm()
    {
        $course_id = post('course_id');
        $storage_path = storage_path('app/media/');
        $inputFileName = $storage_path . post('filepath');
        if (!is_file($inputFileName)) {
            new \Exception("File not found");
        }
        if (!Course::find($course_id)) {
            new \Exception("Course with {$course_id} not found");
        }

        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception('Assess denied');

        if (!$this->user->hasAccess('igvs.courses.import_rubricator'))
            throw new Exception('Assess denied');

        return $this->makePartial('import_rubricator', ['course_id' => post('course_id')]);
    }

    // import rubricator
    public function index_onImportRubricator()
    {
        $course_id = post('course_id');
        $storage_path = storage_path('app/media/');
        $inputFileName = $storage_path . post('filepath');
        if (!is_file($inputFileName)) {
            new \Exception("File not found");
        }
        if (!Course::find($course_id)) {
            new \Exception("Course with {$course_id} not found");
        }

        if (!$this->user->hasAccess('igvs.courses.import_rubricator'))
            throw new Exception('Assess denied');

        $return = $this->importRubricator($inputFileName, $course_id);

        return $return;
    }

    // import course
//    public function index_onImportCourse()
//    {
//        $course_id = post('course_id');
//        $storage_path = storage_path('app/media/');
//        $inputFileName = $storage_path . post('filepath');
//        if (!is_file($inputFileName)) {
//            new \Exception("File not found");
//        }
//        if (!Course::find($course_id)) {
//            new \Exception("Course with {$course_id} not found");
//        }
//
//        if (!$this->user->hasAccess('igvs.courses.import_rubricator'))
//            throw new Exception('Assess denied');
//
//        $return = $this->importCourse($inputFileName, $course_id);
//
//        return $return;
//    }


    // prepare for Release
    public function index_onPrepareForRelease()
    {
        $themes['all'] = 'All';

        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception('Assess denied');

        $topics = Category::where('course_id', post('course_id'))
            ->whereNull('parent_id')
            ->orderBy('sort')
            ->get();

        $folders = [];

        foreach ($topics as $k => $v) {
            $v->name = preg_replace("(<[^<>]+>)", ' ',$v->name . ' (' . $v->code . ')');
            $topics [$k] = $v;
            $folders[$v->id] = $v->code;
        }

        /*$this->addJs('/modules/backend/formwidgets/datepicker/assets/js/build-min.js');
        $this->addCss('/modules/backend/formwidgets/datepicker/assets/vendor/pikaday/css/pikaday.css');*/
        $course = Course::find(post('course_id'));

        return $this->makePartial('prepare_for_release_cloud', [
            'course' => $course,
            'builds' => Build::orderBy('name', 'asc')->get()->lists('name', 'id'),
            'topics' => $topics->lists('name', 'id'),
            'folders' => $folders,
            'themes' => $course->getThemeOptions(),
            'beta_url' => Config::get('igvs.courses::prepareForFtpBeta.betaUrl'),
            'backend_url' => Backend::url('igvs/courses')
        ]);
    }

    // prepare for PilotExport
    public function index_onPreparePublicationState()
    {
        $course_id = post('course_id');
        $build_id  = post('build_id');

        if (!$this->checkAccessCourse($course_id))
            throw new Exception('Assess denied');

        $licences = ExportStructureDiffHelper::getPreviousStructure($course_id);
        $structure = ExportStructureDiffHelper::getStructure($licences, $course_id, $build_id);

        return $this->makePartial('prepare_for_publication_state', [
            'course'  => Course::find($course_id),

            'structure' => $structure,
            'builds' => self::getBuildsList(),
        ]);
    }

    public function index_onGetCourseDiff()
    {
        $course_id = post('course_id');
        $build_id  = post('build_id');

        if (!$this->checkAccessCourse($course_id))
            throw new Exception('Assess denied');

        $licences = ExportStructureDiffHelper::getPreviousStructure($course_id);
        $structure = ExportStructureDiffHelper::getStructure($licences, $course_id, $build_id);

        $this->vars['structure'] = $structure;
    }

    public function index_onPublicPublicationState()
    {
        $publication_state = new PublicationState([
            \Igvs\Courses\Models\PublicationState::class,
            \Igvs\Courses\Models\PublicationStateGlobal::class,
        ]);
        $publication_state->create(post('course_id'), post('build_id'));

        return ['result' => 'true'];
    }

    // prepare for PilotExport
    public function index_onPreparePilotExport()
    {

        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception('Assess denied');

        //ищем пользователя pilot
        $user_pilot = UserPilot::where('user_id', $this->user->id)->first();
        if (!$user_pilot)
            return false;

        $course = Course::find(post('course_id'));
        $parent_course_id = $this->getCourseParentId(post('course_id'));

        //запрашиваем список лицензий и структуру курса
        $course_id_for_licencies = $course && $course->copy_original_id != 0 ? $parent_course_id : post('course_id');
        $licences = \Socialite::with('pilotoauth')->licencies($course_id_for_licencies);

        $licences['licencies'] = $licences;

        //если обрабатывается модификация курса
        //ищем среди лицензий, лицензию на курс
        //если такая есть, то выводим сообщение об успешном запуске экспорта
        $modifed_course_export = false;
        $modifed_course_export_success = false;

        if ($licences && $course && $course->copy_original_id != 0) {

            $modifed_course_export = true;
            $course_id = strval($course_id_for_licencies);

            if (isset($licences['licencies'][$course_id])) {
                $modifed_course_export_success = true;

                $user_instance_type = \Session::get('igvs.user_instance_type', null);
                $user_instance_version = \Session::get('igvs.user_instance_version', null);
                if (!is_null($user_instance_type) && $user_instance_type == 'inclusive') {
                    $user_instance_type = 'pilotoauthinclusive';
                } else {
                    $user_instance_type = 'pilotoauth';
                }

                $oauth_user = unserialize(\Session::get('oauth_user'));
                $oauth_user_uid = (isset($oauth_user->extra) && isset($oauth_user->extra['uid'])) ? $oauth_user->extra['uid'] : null;

                $options = [
                    'courseId'      => post('course_id'),
                    'user_id'       => $user_pilot->pilot_user_id,
                    'pilot_course_id' => $licences['licencies'][$course_id]['pilot_course_id'],
                    'parent_course_id' => $this->getCourseParentId(post('course_id')),
                    'oauth_token' => \Session::get('oauth_token'),
                    'oauth_user_uid' => $oauth_user_uid,
                    'oauth_driver' => $user_instance_type,
                    'oauth_version' => $user_instance_version,
                    'remote_server_domain' => \Session::get('igvs.courser.pilot_host'),
                ];

                $this->pushTask('Export to Pilot', 'exportToPilot', $options, $this->user->id);
            }
        }

        $licences_list = [];

        if (count($licences) && isset($licences['licencies']) && count($licences['licencies'])) {
            $licences_list[''] = Lang::get('igvs.courses::lang.course.txt_field.select_licence');
        } else {
            $licences_list[''] = Lang::get('igvs.courses::lang.course.status.licenses_have_not');
        }

        if (count($licences) && isset($licences['licencies']) && count($licences['licencies'])) {
            foreach ($licences['licencies'] as $key_lic => $lic) {
                $licences_list[$key_lic . '/' . $lic['pilot_course_id'] . '/' . $lic['id']] = $lic['name'] . " (" . Lang::get('igvs.courses::lang.course.free_licenses') . ": {$lic['free']}) ";
            }
        }

        $structure = ExportStructureDiffHelper::getStructure($licences, post('course_id'));

        return $this->makePartial('prepare_for_pilot_export', [
            'course'  => Course::find(post('course_id')),
            'parent_course_id' => $parent_course_id,
            'licences' => $licences_list,
            'lic' => $licences,
            'modifed_course_export' => $modifed_course_export,
            'modifed_course_export_success' => $modifed_course_export_success,
            'structure' => $structure,
        ]);
    }

    public function index_onPilotExport()
    {
        set_time_limit(60*30);

        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception('Assess denied');

        $user_pilot = UserPilot::where('user_id', $this->user->id)->first();
        if (!$user_pilot)
            return false;

        $course = Course::find(post('course_id'));
        if (!$course)
            return false;

        $course->public_last_datetime = date('Y-m-d H:i:s');
        $course->public_last_user_id = $this->user->id;
        $course->save();

        $dont_use_licence = (int)post('dont_use_licence', 0);
        // должен вернуть "true"
        if (!$dont_use_licence) {
            $use_licence = \Socialite::with('pilotoauth')->useLicence(post('course_id'), post('pilot_course_id'), post('external_course_id'));
        } else {
            $use_licence = "true";
        }

        if ($use_licence == 'true') {
            $user_instance_type = \Session::get('igvs.user_instance_type', null);
            $user_instance_version = \Session::get('igvs.user_instance_version', null);
            if (!is_null($user_instance_type) && $user_instance_type == 'inclusive') {
                $user_instance_type = 'pilotoauthinclusive';
            } else {
                $user_instance_type = 'pilotoauth';
            }

            $oauth_user = unserialize(\Session::get('oauth_user'));
            $oauth_user_uid = (isset($oauth_user->extra) && isset($oauth_user->extra['uid'])) ? $oauth_user->extra['uid'] : null;

            /* dummy */
            $options = [
                'courseId' => post('course_id'),
                'user_id' => $user_pilot->pilot_user_id,
                'poo_id' => $user_pilot->poo_id,
                'licence_id' => post('licence_id'),
                'system' => post('system'),
                'competence' => post('competence'),
                'pilot_course_id' => post('pilot_course_id'),
                'parent_course_id' => $this->getCourseParentId(post('course_id')),
                'oauth_token' => \Session::get('oauth_token'),
                'oauth_user_uid' => $oauth_user_uid,
                'oauth_driver' => $user_instance_type,
                'oauth_version' => $user_instance_version,
                'remote_server_domain' => \Session::get('igvs.courser.pilot_host'),
                'remote_server_domain_without_protocol' => \Session::get('igvs.courser.pilot_domain'),
                'dont_use_licence' => $dont_use_licence
            ];

            $this->pushTask('Export to Pilot', 'exportToPilot', $options, $this->user->id);
        }

        return $use_licence;
    }

    public function index_onElearningExport()
    {
        set_time_limit(60*30);

        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception('Assess denied');

        $user_pilot = UserPilot::where('user_id', $this->user->id)->first();
        if (!$user_pilot)
            return false;

        $course = Course::find(post('course_id'));
        if (!$course)
            return false;

        $count_modules = ModuleContent::where('course_id', $course->id)->count();

        $syscomp =
        $structure =
        $competences =
        $systems =
        $first_system_key = [];

        if ($count_modules) {

            //запрашиваем список компетенций и структуру курса
            $syscomp = ExportHelper::getSystemsAndComps();
            $structure = ExportHelper::getStructureCourse(post('course_id'));
            $competences = ExportHelper::getCompetences($syscomp);
            $systems = ExportHelper::getSystems($syscomp);

            $first_system_key = count($systems) ? array_keys($systems)[0] : 0;

            //находим разницу в структуре курса
            $structure_modules = ExportHelper::getStructureModules($structure);
            $structure = ExportStructureDiffHelper::getStructure($structure_modules, post('course_id'));
        }

        $modules_not_exists = $count_modules === 0;
        $modules_not_exists_text = $modules_not_exists ? 'Чтобы опубликовать курс, заполните хотя бы один модуль.' : '';

        return $this->makePartial('elearning_export', [
            'course'  => $course,
            'structure' => $structure,
            'structure_modules_error' => $modules_not_exists,
            'structure_modules_error_text' => $modules_not_exists_text,
            'syscomp' => $syscomp,
            'systems' => $systems,
            'competences' => $competences,
            'first_system_key' => $first_system_key,
        ]);
    }

    public function index_onElearningAddOperatorInCoursePrepare()
    {
        set_time_limit(60*30);

        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception('Assess denied');

        $user_pilot = UserPilot::where('user_id', $this->user->id)->first();
        if (!$user_pilot)
            return false;

        $course = Course::find(post('course_id'));
        if (!$course)
            return false;

        $operators = OperatorsActions::getListByInstanceCluster($user_pilot->instance_id, $user_pilot->cluster_id);
        $operators = OperatorsActions::filterUsedOperators($course->id, $operators);
        $operators = OperatorsActions::sortByPooId($user_pilot->poo_id, $operators);

        return $this->makePartial('elearning_add_operator_in_course', [
            'course'  => $course,
            'operators' => $operators,
            'poo_id' => (int)$user_pilot->poo_id,
        ]);
    }


    public function elearningAddOperatorInCourse()
    {
        $this->layout = 'plugins/academy/system/layouts/clear';

        set_time_limit(60*30);

        //check permissions
        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception('Assess denied');

        $user_pilot = UserPilot::where('user_id', $this->user->id)->first();
        if (!$user_pilot)
            return 'error 1';

        $course = Course::find(post('course_id'));
        if (!$course)
            return 'error 2';

        $operator_id = post('user_id');
        if (!$operator_id)
            return 'error 3';

        $check_id_in_list = OperatorsActions::checkIdInList($operator_id, $user_pilot->instance_id, $user_pilot->cluster_id);
        if (!$check_id_in_list)
            return 'error 4';
        //END check permissions

        $add_operator_result = OperatorsActions::addOperatorToCourse($course->id, $operator_id);
        $add_comment_result = false;

        if ($add_operator_result) {
            $files = \Request::file('files');

            $add_comment_result = \Academy\Cms\Models\Comment::create([
                'user_id' => $this->user->id,
                'description' => "Запрос на публикацию курса от пользователя {$this->user->first_name} {$this->user->last_name} ({$user_pilot->email})",
                'attachment_id' => $course->id,
                'attachment_type' => 'Igvs\Courses\Models\Course',
                'files' => $files,
            ]);
        }

        return $add_comment_result ? 'true' : 'false';
    }

    public function index_onPrepareForReleaseCompare()
    {
        set_time_limit(60*30);

        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception('Assess denied');

        if (!$this->user->hasAccess('igvs.courses.export'))
            throw new Exception('Assess denied');

        return $this->compareContentModuleVersions($this->getModuleContentIds(post('course_id'), post('build_id'), post('topic_id')));
    }

    public function index_onExportReleaseCloud()
    {
        set_time_limit(60*30);

        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception('Assess denied');

        if (!$this->user->hasAccess('igvs.courses.export'))
            throw new Exception('Assess denied');

        $courses_id = trim(post('courses_id'));

        $options = [
            'courseId'    => post('course_id'),
            'buildId'     => post('build_id'),
            'topicId'     => post('topic_id'),
            'theme'       => post('theme'),
            'language'    => post('language'),
            'date'        => post('date'),
            'postfix'     => post('postfix'),
            'beta_folder' => post('beta_folder'),
            'to_igvs'     => post('to_igvs'),
            'to_scorm'    => post('to_scorm'),
            'to_cose'     => post('to_cose'),
            'to_beta'     => post('to_beta'),
            'to_pilot'    => post('to_pilot'),
            'to_zip'      => post('to_zip'),
            'to_maker'    => post('to_maker'),
            'to_mobile'    => post('to_mobile'),
            'to_zip_local'    => post('to_zip_local'),
            'show_module_start_screen' => post('show_module_start_screen'),
            'set_topic_lock'  => post('set_topic_lock'),
            'set_transfer_user_to_review'  => post('set_transfer_user_to_review'),
            'replace_browsed' => post('replace_browsed'),
            'export_without_module_content' => post('export_without_module_content'),
            'create_maker_config' => post('create_maker_config'),
        ];

        $replace_browsed = isset($options['replace_browsed']) ? $options['replace_browsed'] : false;
        $export_without_module_content = isset($options['export_without_module_content']) ? $options['export_without_module_content'] : false;

        $export_igvs = isset($options['to_igvs']) ? $options['to_igvs'] : false;
        $export_scorm = isset($options['to_scorm']) ? $options['to_scorm'] : false;
        $export_cose = isset($options['to_cose']) ? $options['to_cose'] : false;
        $export_beta = isset($options['to_beta']) ? $options['to_beta'] : false;
        $export_pilot = isset($options['to_pilot']) ? $options['to_pilot'] : false;
        $export_zip = isset($options['to_zip']) ? $options['to_zip'] : false;
        $export_maker = isset($options['to_maker']) ? $options['to_maker'] : false;
        $export_mobile = isset($options['to_mobile']) ? $options['to_mobile'] : false;
        $export_zip_local = isset($options['to_zip_local']) ? $options['to_zip_local'] : false;

        // для создания отдельного потока, кроме того, что можно прописать другое имя
        // НУЖНО в supervisor добавить ещё один воркер
        if ($export_maker == 'true') {
            $options['queue_name'] = 'ExportToMaker';
        }

        //сохраняем значение настройки
        Session::put('igvs::courses.categories.replace_browsed', (int)($replace_browsed == 'true'));
        Session::put('igvs::courses.categories.export_without_module_content', (int)($export_without_module_content == 'true'));

        Session::put('igvs::courses.categories.export_igvs', (int)($export_igvs == 'true'));
        Session::put('igvs::courses.categories.export_scorm', (int)($export_scorm == 'true'));
        Session::put('igvs::courses.categories.export_cose', (int)($export_cose == 'true'));
        Session::put('igvs::courses.categories.export_beta', (int)($export_beta == 'true'));
        Session::put('igvs::courses.categories.export_pilot', (int)($export_pilot == 'true'));
        Session::put('igvs::courses.categories.export_zip', (int)($export_zip == 'true'));
        Session::put('igvs::courses.categories.export_maker', (int)($export_maker == 'true'));
        Session::put('igvs::courses.categories.export_mobile', (int)($export_mobile == 'true'));
        Session::put('igvs::courses.categories.export_zip_local', (int)($export_zip_local == 'true'));


        $db_id = $this->pushTask('Export to Release Cloud', 'exportToReleaseCloud', $options, $this->user->id);

        if (!$courses_id
            || !strlen($courses_id)) {
            return $db_id;
        }

        //если указаны ID других курсов через запятую
        //убираем пробелы
        $courses_id = str_replace(' ', '', $courses_id);
        $courses_id = explode(',', $courses_id);
        //убираем дубликаты
        $courses_id = array_unique($courses_id);

        //убираем текущий курс из списка, если есть
        $this_id_key = array_search(post('course_id'), $courses_id);
        if ($this_id_key) unset($courses_id[$this_id_key]);

        $options['buildId'] = '';
        $options['topicId'] = '';

        //ставим в очередь каждый курс
        foreach ($courses_id as $course_id) {
            $options['courseId'] = $course_id;

            $db_id = $this->pushTask('Export to Release Cloud', 'exportToReleaseCloud', $options, $this->user->id);
        }

        return $db_id;
    }

    // export to Scorm
    public function index_onExportForm()
    {
        $arhive = Config::get('igvs.courses::scorm.arhive');

        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception('Assess denied');

        if (!$this->user->hasAccess('igvs.courses.export_scorm'))
            throw new Exception('Assess denied');

        $course = Course::find(post('course_id'));

        return $this->makePartial('export_form', [
            'course' => $course,
            'builds' => self::getBuildsList(),
            'themes' => $course->getThemeOptions(),
        ]);
    }

    public function index_onCreateScorm()
    {
        set_time_limit(60*5);

        if (!$this->checkAccessCourse(post('course_id')))
            throw new Exception('Assess denied');

        if (!$this->user->hasAccess('igvs.courses.export_scorm'))
            throw new Exception('Assess denied');

        return $this->createScorm(post('course_id'), post('build_id'), null, post('theme'));
    }

    public function index_onCloseThemeWarning($id) {
        Session::put('igvs.courses.closethemewarning.' . $id, true);
    }

    public function minicreate()
    {
        if (get('dataModel[parent_id]')) {
            $this->pageTitle = trans('igvs.courses::lang.category.createact_title');
        }
        else {
            $this->pageTitle = trans('igvs.courses::lang.category.createtopic_title');
        }
        return $this->asExtension('CrudController')->crudcreate('short');
    }

    public function minicreate_onSave()
    {
        return $this->asExtension('CrudController')->crudcreate_onSave();
    }

    public function onStatusChange()
    {
        if(!$module = ModuleContent::find(post('module_id')))
            return;

        $module->status_id = post('status_id');

        if($module->save())
            return [
                '#module_status_' . $module->id => $this->makePartial('$/igvs/courses/controllers/statuses/_statuslist.htm', [
                    'status_id' => $module->status_id,
                    'statuses' => $module->getStatusOptions(),
                    'module_id' => $module->id
                ])
            ];
    }

    public function onCourseStatusChange11()
    {
        if (!($course = Course::find(post('course_id')))
        || !($status_id = post('status_id'))
        || !isset($course->getStatusOptions()[$status_id])
        ) {
            return;
        }
        return $this->makePartial('change_status', [
            'status_id' => $status_id,
            'course_id' => $course->id,
            'old_status_id' => $course->status_id,
            'count' => $course->modules()->where('status_id', $course->status_id)->count()
        ]);
    }

    public function onCourseStatusChange()
    {
        if (!($course = Course::find(post('course_id')))
        || !($status_id = post('status_id'))
        || !isset($course->getStatusOptions()[$status_id])
        ) {
            return;
        }
        $modules = $course->modules()->where('status_id', $course->status_id)->lists('id');
        $course->status_id = $status_id;

        if ($course->save()) {
            $result = [
                '#course_status' => $this->makePartial('$/igvs/courses/controllers/statuses/_statuslist_course.htm', [
                    'status_id' => $course->status_id,
                    'statuses' => $course->getStatusOptions(),
                    'course_id' => $course->id
                ])
            ];
            if (isset($modules[0])) {

                $module = ModuleContent::find(reset($modules));
                $result['#module_status_class_' . implode(', #module_status_class_', $modules)] = $this->makePartial('$/igvs/courses/controllers/statuses/_statuslist_class.htm', [
                    'status_id' => $module->status_id,
                    'statuses' => $module->getStatusOptions(),
                    'module_id' => $module->id
                ]);

                $modules = $course
                    ->modules()
                    ->where('status_id', $course->status_id)
                    ->select('id')
                    ->get();

                foreach ($modules as $module) {
                    $comment = new Comment();
                    $comment->data = $status_id;
                    $module->comments()->add($comment);
                }
            }
            return $result;
        }
    }

    public function onInfoCheck() {
        $info = Info::check(post('info_code'), $this->user->id);
        return ['#info_' . $info->id => ''];
    }

    public function onTempLinkCreate()
    {
        if (!($user = \BackendAuth::getUser())) {
            return false;
        }

        // создание ссылки
        $course_id = post('course_id', 0);
        $date = post('date', date('Y-m-d', strtotime('+1 month')));
        $time = post('time', date('H:i'));

        if (!strlen($date)) {
            $date = date('Y-m-d', strtotime('+1 month'));
        }

        if (!strlen($time)) {
            $time = date('H:i');
        }

        $time .= ':00';

        $temp_link = TempLink::create([
            'user_id'       => $user->id,
            'course_id'     => $course_id,
            'build_id'      => post('build_id', 0),
            'topic_id'      => post('topic_id', 0),
            'date_expired'  => $date . ' ' . $time,
        ]);

        // получаем список всех ссылок данного пользователя для данного курса
        $return_links = $this->getTempLinkExisted();

        return json_encode([
            'status' => 'ok',
            'hash' => $temp_link->hash,
            'links_history' => $return_links,
        ]);
    }

    public function onTempLinkGetHistory()
    {
        if (!($user = \BackendAuth::getUser())) {
            return false;
        }

        // получаем список всех ссылок данного пользователя для данного курса
        $return_links = $this->getTempLinkExisted();

        return json_encode([
            'status' => 'ok',
            'links_history' => $return_links,
        ]);
    }

    public function onTempLinkDelete()
    {
        if (!($user = \BackendAuth::getUser())) {
            return false;
        }

        $id = post('id', 0);

        TempLink::where('id', $id)
            ->where('user_id', $user->id)
            ->delete();

        // получаем список всех ссылок данного пользователя для данного курса
        $return_links = $this->getTempLinkExisted();

        return json_encode([
            'status' => 'ok',
            'links_history' => $return_links,
        ]);
    }

    private function getTempLinkExisted()
    {
        if (!($user = \BackendAuth::getUser())) {
            return false;
        }

        $course_id = post('course_id', 0);

        $return_links = [];

        $temp_link_existed = TempLink::where('course_id', $course_id)
            ->where('user_id', $user->id)
            ->where('date_expired', '>', date('Y-m-d H:i:s'))
            ->get();

        foreach ($temp_link_existed as $item) {
            $return_links[] = [
                'id' => $item->id,
                'build' => $item->build ? $item->build->name : '',
                'topic' => $item->topic ? $item->topic->name : '',
                'expired' => $item->date_expired,
                'hash' => $item->hash,
            ];
        }

        return $return_links;
    }
}
