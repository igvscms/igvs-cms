<?php namespace Igvs\Courses\Controllers;

use Backend\Classes\Controller;

use Igvs\Courses\Classes\MemberEmailNotice;
use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Status;
use Igvs\Courses\Models\ModuleContent;
use Academy\Cms\Models\Comment;

class CourseComments extends Controller {
    public $implement = ['Backend.Behaviors.FormController'];
    public $formConfig = 'config_form.yaml';
    public $requiredPermissions = ['igvs.courses.courses'];

    public function __construct() {
        parent::__construct();
        \BackendMenu::setContext('Igvs.Courses', 'courses', 'courses');
    }

    public function declension($quantity, $word, $endings = ['','а','ов']) {
        $q100 = $quantity % 100;
        $q10 = $q100 % 10;

        if (($q100 > 4 && $q100 < 21) || ($q10 > 4) || ($q10 == 0)) {
            return $word . $endings[2];
        }
        if ($q10 == 1) {
            return $word . $endings[0];
        }
        return $word . $endings[1];
    }

    public function mb_lcfirst($text) {
        return mb_strtolower(mb_substr($text, 0, 1)) . mb_substr($text, 1);
    }

    public function onModalCreate() {
        $this->asExtension('FormController')->create($this->action);
        return $this->makePartial('$/academy/system/partials/_modal.htm', ['body' => $this->makePartial($this->action)]);
    }

    public function create_content() {
        return $this->create();
    }

    public function create_onSave() {
        if ($course = Course::find(post('course_id'))) {
            $modules = $course->modules()->where('status_id', $course->status_id)->lists('id');
        }
        $result = parent::create_onSave();
        if (!$course = Course::find(post('course_id'))) {
            return $result;
        }

        MemberEmailNotice::sendEmailNoticeChangeCourseStatus($course, $this->vars['formModel']);

        $result['#course_status'] = $this->makePartial('$/igvs/courses/controllers/statuses/_statuslist_course.htm', [
            'status_id' => $course->status_id,
            'statuses' => $course->getStatusOptions(),
            'course_id' => $course->id
        ]);

        if (isset($modules[0])) {

            $module = ModuleContent::find(reset($modules));
            $result['#module_status_class_' . implode(', #module_status_class_', $modules)] = $this->makePartial('$/igvs/courses/controllers/statuses/_statuslist_class.htm', [
                'status_id' => $module->status_id,
                'statuses' => $module->getStatusOptions(),
            ]);
        }
        return $result;
    }

    public function create_content_onSave() {
        $result = parent::create_onSave();
        if (!$module = ModuleContent::find(post('module_id'))) {
            return $result;
        }

        $result['#module_status_class_' . (int)post('module_id')] = $this->makePartial('$/igvs/courses/controllers/statuses/_statuslist_class.htm', [
            'status_id' => $module->status_id,
            'statuses' => $module->getStatusOptions(),
        ]);
        return $result;
    }

    public function formExtendModel($model) {
        if ($this->action == 'create' && $this->getAjaxHandler() == 'onSave') {
            if (!($course = Course::find(post('course_id')))
            || !($status_id = post('status_id'))
            || !isset($course->getStatusOptions()[$status_id])
            ) {
                return;
            }
            $course->status_id = $status_id;

            if ($course->save()) {
                $modules = $course
                    ->modules()
                    ->where('status_id', $course->status_id)
                    ->select('id')
                    ->get();

                foreach ($modules as $module) {
                    $comment = new Comment();
                    $comment->data = $status_id;
                    $module->comments()->add($comment);
                }

                $model->data = post('status_id');
                $course->comments()->add($model);
            }
        }
        elseif ($this->action == 'create_content' && $this->getAjaxHandler() == 'onSave') {
            if (!($module = ModuleContent::find(post('module_id')))
            || !($status_id = post('status_id'))
            || !isset($module->getStatusOptions()[$status_id])
            ) {
                return;
            }
            $module->status_id = $status_id;

            if ($module->save()) {
                $model->data = post('status_id');
                $module->comments()->add($model);
            }
        }
    }
}
