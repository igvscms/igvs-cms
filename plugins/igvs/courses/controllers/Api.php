<?php namespace Igvs\Courses\Controllers;

use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\ModuleContent;
use October\Rain\Database\Updates;
use Igvs\Courses\Updates\SeedAllTables;
use Illuminate\Database\Eloquent\Model as Eloquent;

use Response;
use BackendMenu;
use Config;
use App;
use Backend\Classes\Controller;
use Input;

/**
 * Api Back-end Controller
 */
class Api extends Controller
{
    use \Igvs\Courses\Traits\FileHelper;
    use \Igvs\Courses\Traits\CheckAccess;

    /*public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';*/

    protected $count_div = 0;

    public function __construct()
    {
        parent::__construct();

        $this->count_div = 0;

        BackendMenu::setContext('Igvs.Courses', 'courses', 'api');
    }

    public function RefreshModulesAndConstructors(){
        try {
            Eloquent::unguard();
            $seeder = new SeedAllTables();
            $seeder-> UpdateConstructorsTable();
            $seeder-> seedModulesTemplates();
            return Response::make('{sucsess:true,error_msg:""}');
        }
        catch(\Exception $e) {
            return Response::make('{sucsess:false,error_msg:'.$e->getMessage().'}');
        }
    }

    public function getModules () {
        $modules = Module::all();

        $object = [];

        foreach ($modules as $module) {
            $version = $module->getLatestVersion();
            $constructor = $module->getActualConstructor();
            $object[$module->id] = array(
                'name' => $module->attributes['name'],
                'release_date' => $module->getLastUpdateAttribute(),
                'version' => $version,
                'status'=> $module->getActualStatus($version),
                'type' => $module->getActualType($version),
                'path' => $module->path
            );

            if (isset($constructor)){
                $object[$module->id]['constructor'] = array(
                    'version' => $constructor->version,
                    'release_date' => $constructor->attributes['release_date'],
                    'release_notes' => $constructor->release_notes,
                    'status' => $constructor->attributes['status']
                );
            }
            else
                $object[$module->id]['constructor'] = null;
        }
        return Response::make(json_encode($object, JSON_UNESCAPED_UNICODE));
    }

    public function copyFile()
    {
        $path = Input::get('path');
        $module = ModuleContent::findOrFail(Input::get('module_id'));
        $storage = realpath(Config::get('igvs.courses::modulesContent.path'));
        $filename = basename($path);

        // проверка на попытку скопировать файл находящийся выше storage/igvs/module-content
        $realpath = realpath("{$storage}/{$path}");
        if ($realpath !== "{$storage}/{$path}") {
            exit;
        }

        // preg_match('/^(\d+)\/(\d+)\/.*$/', $path, $matches)
        if ($this->checkAccessCourse($module->course_id)) {

            $source = "{$storage}/{$path}";
            $dest = "{$storage}/{$module->course_id}/{$module->id}/copy_";

            $dest = !file_exists($dest . $filename) ? $dest . $filename :
                $dest . uniqid() . "_{$filename}";

            if (is_file($source) && link($source, $dest)) {
                exit('1');
            }
        }
    }

}