<?php namespace Igvs\Courses\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Roles Back-end Controller
 */
class Roles extends Controller
{
    use \Igvs\Courses\Traits\AntiCache;

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['igvs.courses.roles'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Igvs.Courses', 'courses', 'roles');
        $this->addCss('/plugins/igvs/courses/assets/css/course-form.css');//.form-group.checkbox-field
    }
}