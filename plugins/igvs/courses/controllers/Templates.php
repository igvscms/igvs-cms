<?php namespace Igvs\Courses\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Config;
// use SSH;

use Igvs\Courses\Models\ModuleVersion;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Updates\SeedAllTables;
use Igvs\Courses\Updates\SeedShellVersionTable;

/**
 * Templates Back-end Controller
 */
class Templates extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        //'Backend.Behaviors.ListController',
        'Igvs.Courses.Behaviors.LocalListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = ['igvs.courses.templates'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Igvs.Courses', 'courses', 'templates');
    }

    public function onLoadUpdates()
    {
        return $this->makePartial('update_form');
    }

    public function onCheckForUpdatesModules()
    {
        /*
        $commands = [
            'cd ' . Config::get('igvs.courses::modulesRelease.path'),
            'git diff --stat --color origin'
        ];

        $output = '';
        SSH::run($commands, function($line) use (&$output) {
            $output .= $line . PHP_EOL;
        });
        */

//        $dir = Config::get('igvs.courses::modulesRelease.path');
//        $cmd = "cd \"{$dir}\" && git fetch && git diff --stat --color origin";
//        $output = shell_exec($cmd);

        $output = 'Нажмите обновить';

        return [
            '#updateContainer' => $this->makePartial('output', [
                'text' => $output ? '' : e(trans('system::lang.updates.none.help')),
                'output' => $output,
                'handler' => 'onApplyUpdatesModules'
            ])
        ];
    }

    public function onApplyUpdatesModules()
    {
        \Cache::forget('igvs.module.html_markup_id');
        \Cache::forget('igvs.modules.all');

        Eloquent::unguard();

        $path = Config::get('igvs.courses::modulesRelease.path');
        $cmd = "git -C \"$path\" clean -d -f";
        $output = shell_exec($cmd);

        $cmd = "git -C \"$path\" reset --hard";
        $output .= shell_exec($cmd);

        $cmd = "git -C \"$path\" pull";
        $output .= shell_exec($cmd);
        $output .= (new SeedAllTables)->seedModulesTemplates();
        $output .= (new SeedShellVersionTable)->seedShellVersions();
        $output .= (new SeedAllTables)->UpdateConstructorsTable();

        return [
            "#updateContainer" => $this->makePartial('output', [
                'text' => empty($ouput) ? 'Update succeeded!' : '',
                'output' => $output
            ])
        ];
    }

    public function listExtendQuery($query)
    {
        $versions = ModuleVersion::orderBy('sort','asc')->lists('id','module_id');
        $query->whereIn('id',$versions);
    }
}
