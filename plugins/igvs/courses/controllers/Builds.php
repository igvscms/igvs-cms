<?php namespace Igvs\Courses\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Builds Back-end Controller
 */
class Builds extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['igvs.courses.builds'];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Igvs.Courses', 'courses', 'builds');
    }
}
