<?php namespace Igvs\Courses\Controllers;

use Backend\Facades\Backend;
use Response;
use BackendMenu;
use Config;
use App;
use Input;

use Socialite;
use Illuminate\Routing\Controller as ControllerBase;
use Request;
use Cookie;
use League\OAuth1\Client\Credentials\TemporaryCredentials;
use Session;

use Igvs\Courses\Models\PilotAuth as PilotAuthModel;
use Igvs\Courses\Classes\UserSync;
use Academy\Api\Models\Client;
use Igvs\Courses\Models\UserPilot;
use Igvs\Courses\Models\Member;
use Backend\Models\User as BackendUser;
use Backend\Models\UserGroup;
use Backend\Models\AccessLog;
use Faker\Factory;
use BackendAuth;

/**
 * Api Back-end Controller
 */
class OAuth extends ControllerBase
{

    public $publicActions = ['pilotOauth', 'pilotOauthFinish', 'logout'];

    public function logout()
    {
        $pilot_host = Session::get('igvs.courser.pilot_host');

        BackendAuth::logout();
        echo '<script>window.location.href="' . $pilot_host  . '/?logout=yes";</script>';
    }

    public function pilotOauth()
    {
        $this->setPilotHost();

        try {
            $redirect = Socialite::driver('pilotoauth')->redirect();
            $target_url = $redirect->getTargetUrl();

            echo '<script>window.location.href="' . $target_url . '";</script>';
        } catch (\Exception $e) {

            UserSync::log(print_r($e->getMessage(), true), $this,'errors');

            echo $e->getMessage();
            echo '<br>';
            echo '<pre>';
            var_dump($e->getTraceAsString());
            echo '</pre>';
        }
    }

    public function pilotOAuthExpertise()
    {
        $this->setPilotHost();

        $target_url = '';

        try {
            $redirect = Socialite::driver('pilotoauthexpertise')->redirect();
            $target_url = $redirect->getTargetUrl();

            echo '<script>window.location.href="' . $target_url . '";</script>';
        } catch (\Exception $e) {

            UserSync::log(print_r($e->getMessage(), true), $this,'errors');

            echo $e->getMessage();
            echo '<br>';
            echo '<pre>';
            var_dump($e->getTraceAsString());
            echo '</pre>';
        }
    }

    public function pilotOAuthInclusive()
    {
        $this->setPilotHost();

        $target_url = '';

        try {
            $redirect = Socialite::driver('pilotoauthinclusive')->redirect();
            $target_url = $redirect->getTargetUrl();

            echo '<script>window.location.href="' . $target_url . '";</script>';
        } catch (\Exception $e) {

            UserSync::log(print_r($e->getMessage(), true), $this,'errors');

            echo $e->getMessage();
            echo '<br>';
            echo '<pre>';
            var_dump($e->getTraceAsString());
            echo '</pre>';
        }
    }

    private function setPilotHost()
    {
        $referer = Request::header('Referer');

        $pilot_host = '';
        $pilot_domain = '';
        if ($referer && strlen($referer)) {
            preg_match("/^(https?:\/\/)?([^\/]+)/i",
                $referer, $matches);

            $pilot_host = $matches[0]; // например https://pilot.i-gvs.com
            $pilot_domain = isset($matches[2]) ? $matches[2] : ''; // например pilot.i-gvs.com
        }

        Session::put('igvs.courser.pilot_host', $pilot_host);
        Session::put('igvs.courser.pilot_domain', $pilot_domain);
    }

    public function pilotOauthFinish()
    {
        $user_info = Socialite::with('pilotoauth')->user();

        $pilot_domain = Session::get('igvs.courser.pilot_domain', '');

        // выясняем инстанс
        try {
            $instance_id = $user_info->user['instance_id'];
            $client_instance = Client::where('app_id', $instance_id)
                ->where('domain', $pilot_domain)
                ->first();

            if (!$client_instance) {
                throw new \Exception('Client not found. Please, contact with IGVS developer. code 1.');
            }
        }catch (\Exception $e) {

            UserSync::log('Client not found. Please, contact with IGVS developer. code 2. Error: '
                . $e->getMessage()
                . "\r\n"
                . print_r([
                    '$user_info' => $user_info,
                    '$domain' => $pilot_domain
                ], true), $this, 'errors');

            throw new \Exception('Client not found. Please, contact with IGVS developer. code 2. Error: ' . $e->getMessage());
        }

        // добавляем недостающий параметр
        $user_info->user['ID'] = $user_info->user['uid'];
        $user_info->user['NAME'] = isset($user_info->user['first_name']) ? $user_info->user['first_name'] : $user_info->user['last_name'];
        // преобразовываем ключи массива в верхний регистр
        $user_info->user = UserSync::changeArrayKeysCase($user_info->user, $this);

        $user_sync = new UserSync();

        // подменяем группу, если не основной пилот
        $user_sync->updateOperatorGroup($client_instance->id);

        // создаём/обновляем пользователя
        $user_sync->saveUpdateUser($user_info->user, $client_instance->id, $this);

        //сохраняем, что лежит в сессии до авторизации
        $oauth_token = Session::get('oauth_token', '');
        $oauth_user = Session::get('oauth_user', '');
        $igvs_courser_pilot_locale = Session::get('igvs.courser.pilot_locale', '');
        $igvs_courser_pilot_host = Session::get('igvs.courser.pilot_host', '');
        $igvs_courser_pilot_domain = Session::get('igvs.courser.pilot_domain', '');

        // Authenticate user
        $this->authUser($user_info->user, $client_instance->id);

        Session::start();

        //записываем в сессию, что сохранили до авторизации
        Session::put('oauth_token', $oauth_token);
        Session::put('oauth_user', $oauth_user);
        Session::put('igvs.courser.pilot_locale', $igvs_courser_pilot_locale);
        Session::put('igvs.courser.pilot_host', $igvs_courser_pilot_host);
        Session::put('igvs.courser.pilot_domain', $igvs_courser_pilot_domain);
        Session::put('igvs.user_instance_type', 'pilot');
        Session::put('igvs.user_instance_version', $client_instance->version);

        Cookie::queue('pilot_domain', $igvs_courser_pilot_domain, 60*24*365);

        $user_full_name = implode(' ', [
            isset($user_info->user['LAST_NAME']) ? $user_info->user['LAST_NAME'] : '',
            isset($user_info->user['FIRST_NAME']) ? $user_info->user['FIRST_NAME'] : '',
            isset($user_info->user['SECOND_NAME']) ? $user_info->user['SECOND_NAME'] : '',
        ]);

        $user_work_company = isset($user_info->user['WORK_COMPANY']) ? $user_info->user['WORK_COMPANY'] : '';
        $interface_theme = isset($user_info->user['CUSTOM_DESIGN']) ? $user_info->user['CUSTOM_DESIGN'] : '';
        $interface_logo = isset($user_info->user['DESIGN_PICTURE']) ? $user_info->user['DESIGN_PICTURE'] : '';
        $interface_cluster_name = isset($user_info->user['DESIGN_CLUSTER_NAME']) ? $user_info->user['DESIGN_CLUSTER_NAME'] : '';

        $installation_phone = isset($user_info->user['INSTALLATION_PHONE']) ? $user_info->user['INSTALLATION_PHONE'] : '';
        $installation_skype = isset($user_info->user['INSTALLATION_SKYPE']) ? $user_info->user['INSTALLATION_SKYPE'] : '';
        $installation_email = isset($user_info->user['INSTALLATION_EMAIL']) ? $user_info->user['INSTALLATION_EMAIL'] : '';
        $installation_feedback_link = isset($user_info->user['INSTALLATION_FEEDBACK_LINK']) ? $user_info->user['INSTALLATION_FEEDBACK_LINK'] : '';
        $installation_footer_text_dbconn = isset($user_info->user['INSTALLATION_FOOTER_TEXT_DBCONN']) ? $user_info->user['INSTALLATION_FOOTER_TEXT_DBCONN'] : '';
        $installation_footer_link_dbconn = isset($user_info->user['INSTALLATION_FOOTER_LINK_DBCONN']) ? $user_info->user['INSTALLATION_FOOTER_LINK_DBCONN'] : '';
        $installation_license_link = isset($user_info->user['INSTALLATION_LICENSE_LINK']) ? $user_info->user['INSTALLATION_LICENSE_LINK'] : '';

        Session::put('igvs.courser.user_instance_id', $client_instance->id);
        Session::put('igvs.courser.full_name', $user_full_name);
        Session::put('igvs.courser.work_company', $user_work_company);
        Session::put('igvs.courses.interface_theme', $interface_theme);
        Session::put('igvs.courses.interface_logo', $interface_logo);
        Session::put('igvs.courses.interface_cluster_name', $interface_cluster_name);

        Session::put('igvs.courses.installation_phone', $installation_phone);
        Session::put('igvs.courses.installation_skype', $installation_skype);
        Session::put('igvs.courses.installation_email', $installation_email);
        Session::put('igvs.courses.installation_feedback_link', $installation_feedback_link);
        Session::put('igvs.courses.installation_footer_text_dbconn', $installation_footer_text_dbconn);
        Session::put('igvs.courses.installation_footer_link_dbconn', $installation_footer_link_dbconn);
        Session::put('igvs.courses.installation_license_link', $installation_license_link);

        $backendUriExpertise = \Config::get('cms.backendUriExpertise', 'backend');
        $is_expertise = Request::get('expertise', 0);
        $plugin_author_name = $is_expertise ? $backendUriExpertise : 'igvs';

        $url = Backend::url($plugin_author_name . '/courses');
        echo '<script>window.location.href="' . $url . '";</script>';
    }

    public function pilotOauthInclusiveFinish()
    {
        $user_info = Socialite::with('pilotoauthinclusive')->user();

        $pilot_domain = Session::get('igvs.courser.pilot_domain', '');

        // выясняем инстанс
        try {
            $instance_id = $user_info->user['instance_id'];
            $client_instance = Client::where('app_id', $instance_id)
                ->where('domain', $pilot_domain)
                ->first();

            if (!$client_instance) {
                throw new \Exception('Client not found. Please, contact with IGVS developer. code 1.');
            }
        }catch (\Exception $e) {

            UserSync::log('Client not found. Please, contact with IGVS developer. code 2. Error: '
                . $e->getMessage()
                . "\r\n"
                . print_r([
                    '$user_info' => $user_info,
                    '$domain' => $pilot_domain
                ], true), $this, 'errors');

            throw new \Exception('Client not found. Please, contact with IGVS developer. code 2. Error: ' . $e->getMessage());
        }

        // добавляем недостающий параметр
        $user_info->user['ID'] = $user_info->user['uid'];
        $user_info->user['NAME'] = isset($user_info->user['first_name']) ? $user_info->user['first_name'] : $user_info->user['last_name'];
        // преобразовываем ключи массива в верхний регистр
        $user_info->user = UserSync::changeArrayKeysCase($user_info->user, $this);

        $user_sync = new UserSync();

        // подменяем группу, если не основной пилот
        $user_sync->updateOperatorGroup($client_instance->id);

        // создаём/обновляем пользователя
        $user_sync->saveUpdateUser($user_info->user, $client_instance->id, $this);

        //сохраняем, что лежит в сессии до авторизации
        $oauth_token = Session::get('oauth_token', '');
        $oauth_user = Session::get('oauth_user', '');
        $igvs_courser_pilot_locale = Session::get('igvs.courser.pilot_locale', '');
        $igvs_courser_pilot_host = Session::get('igvs.courser.pilot_host', '');
        $igvs_courser_pilot_domain = Session::get('igvs.courser.pilot_domain', '');

        // Authenticate user
        $this->authUser($user_info->user, $client_instance->id);

        Session::start();

        //записываем в сессию, что сохранили до авторизации
        Session::put('oauth_token', $oauth_token);
        Session::put('oauth_user', $oauth_user);
        Session::put('igvs.courser.pilot_locale', $igvs_courser_pilot_locale);
        Session::put('igvs.courser.pilot_host', $igvs_courser_pilot_host);
        Session::put('igvs.courser.pilot_domain', $igvs_courser_pilot_domain);
        Session::put('igvs.user_instance_type', 'inclusive');
        Session::put('igvs.user_instance_version', $client_instance->version_inclusive);

        Cookie::queue('pilot_domain', $igvs_courser_pilot_domain, 60*24*365);

        $user_full_name = implode(' ', [
            isset($user_info->user['LAST_NAME']) ? $user_info->user['LAST_NAME'] : '',
            isset($user_info->user['FIRST_NAME']) ? $user_info->user['FIRST_NAME'] : '',
            isset($user_info->user['SECOND_NAME']) ? $user_info->user['SECOND_NAME'] : '',
        ]);

        $user_work_company = isset($user_info->user['WORK_COMPANY']) ? $user_info->user['WORK_COMPANY'] : '';
        $interface_theme = isset($user_info->user['CUSTOM_DESIGN']) ? $user_info->user['CUSTOM_DESIGN'] : '';
        $interface_logo = isset($user_info->user['DESIGN_PICTURE']) ? $user_info->user['DESIGN_PICTURE'] : '';
        $interface_cluster_name = isset($user_info->user['DESIGN_CLUSTER_NAME']) ? $user_info->user['DESIGN_CLUSTER_NAME'] : '';

        $installation_phone = isset($user_info->user['INSTALLATION_PHONE']) ? $user_info->user['INSTALLATION_PHONE'] : '';
        $installation_skype = isset($user_info->user['INSTALLATION_SKYPE']) ? $user_info->user['INSTALLATION_SKYPE'] : '';
        $installation_email = isset($user_info->user['INSTALLATION_EMAIL']) ? $user_info->user['INSTALLATION_EMAIL'] : '';
        $installation_feedback_link = isset($user_info->user['INSTALLATION_FEEDBACK_LINK']) ? $user_info->user['INSTALLATION_FEEDBACK_LINK'] : '';
        $installation_footer_text_dbconn = isset($user_info->user['INSTALLATION_FOOTER_TEXT_DBCONN']) ? $user_info->user['INSTALLATION_FOOTER_TEXT_DBCONN'] : '';
        $installation_footer_link_dbconn = isset($user_info->user['INSTALLATION_FOOTER_LINK_DBCONN']) ? $user_info->user['INSTALLATION_FOOTER_LINK_DBCONN'] : '';
        $installation_license_link = isset($user_info->user['INSTALLATION_LICENSE_LINK']) ? $user_info->user['INSTALLATION_LICENSE_LINK'] : '';

        Session::put('igvs.courser.user_instance_id', $client_instance->id);
        Session::put('igvs.courser.full_name', $user_full_name);
        Session::put('igvs.courser.work_company', $user_work_company);
        Session::put('igvs.courses.interface_theme', $interface_theme);
        Session::put('igvs.courses.interface_logo', $interface_logo);
        Session::put('igvs.courses.interface_cluster_name', $interface_cluster_name);

        Session::put('igvs.courses.installation_phone', $installation_phone);
        Session::put('igvs.courses.installation_skype', $installation_skype);
        Session::put('igvs.courses.installation_email', $installation_email);
        Session::put('igvs.courses.installation_feedback_link', $installation_feedback_link);
        Session::put('igvs.courses.installation_footer_text_dbconn', $installation_footer_text_dbconn);
        Session::put('igvs.courses.installation_footer_link_dbconn', $installation_footer_link_dbconn);
        Session::put('igvs.courses.installation_license_link', $installation_license_link);

        $backendUriExpertise = \Config::get('cms.backendUriExpertise', 'backend');
        $is_expertise = Request::get('expertise', 0);
        $plugin_author_name = $is_expertise ? $backendUriExpertise : 'igvs';

        $url = Backend::url($plugin_author_name . '/courses');
        echo '<script>window.location.href="' . $url . '";</script>';
    }

    private function authUser($user_data, $client_instance) {

        $pilot_user_id = $user_data['ID'];

        $user_pilot = UserPilot::where('pilot_user_id', $pilot_user_id)
            ->where('instance_id', $client_instance)
            ->first();

        if (!$user_pilot) {
            return false;
        }

        $backend_user = BackendAuth::findUserById($user_pilot->user_id);

        if (!$backend_user) {
            return false;
        }

        BackendAuth::login($backend_user);

        // Log the sign in event
        AccessLog::add($backend_user);
    }
}