<?php namespace Igvs\Courses\Controllers;

use Illuminate\Routing\Controller as ControllerBase;

use Response;
use Igvs\Courses\Models\Module;

class API2 extends ControllerBase
{
    public $publicActions = ['getTemplatesVersions'];


    public function getTemplatesVersions()
    {
        $modules = Module::all();

        $object = [];

        foreach ($modules as $module) {
            $version = $module->getLatestAvailable();

            $object[$module->id] = array(
                'id' => $module->id,
                'name' => $module->attributes['name'],
                'version' => $version,
                'path' => $module->path
            );
        }
        return Response::make(json_encode($object, JSON_UNESCAPED_UNICODE));
    }

}

?>