<?php namespace Igvs\Courses\Controllers;

use Input;
use Illuminate\Routing\Controller as ControllerBase;

/**
 * Api Back-end Controller
 */
class RunTests extends ControllerBase
{

    public $publicActions = [
        'python',
    ];

    private $path_tmp_data = '';
    private $user_script_file_name = '';
    private $path_user_script_file_name = '';

    private function prepareContext()
    {
        $this->path_tmp_data = __DIR__ . '/../../../../storage/app/igvs/users-code/' . date('Y-m-d_H-i-s') . rand(1000, 9999);

        if (!is_dir($this->path_tmp_data))
            mkdir($this->path_tmp_data, 0777, true);

        $this->createUserScriptFile();
        $this->copyRunScriptToUserDir();
    }

    private function createUserScriptFile()
    {
        $result = false;

        do {
            $file_name = 'python_' . date('Y_m_d-H_i_s') . '_' . rand(1000, 9999) . '.py';

            if (is_file($this->path_tmp_data . '/' . $file_name))
                continue;

            file_put_contents($this->path_tmp_data . '/' . $file_name, '');

            $result = true;
        } while (!$result);

        $this->user_script_file_name = $file_name;
        $this->path_user_script_file_name = $this->path_tmp_data . '/' . $file_name;
    }

    private function copyRunScriptToUserDir()
    {
        $path_from = __DIR__ . '/../resources/docker-python-tests/run-test.sh';
        copy($path_from,$this->path_tmp_data . '/run-test.sh');
    }

    public function python()
    {
        $data = input('data', null);

        // create temp folder and file
        $this->prepareContext();

        $logs_all = [];
        $results = [];

        $code = $tests = null;

        try {
            if (is_array($data)) {
                $code = isset($data['code']) ? $data['code'] : null;
                $tests = isset($data['tests']) ? $data['tests'] : null;
            }

            if ($code === null) {
                throw new \Exception("Not set code param");
            }

            if ($tests === null) {
                throw new \Exception("Not set tests param");
            }

            $test_iteration = 0;
            foreach ($tests as $test) {
                $test_iteration++;

                $skip = false;
                // set stdin
                $stdin = '';

                if (isset($test['inputs'])
                    && is_array($test['inputs'])
                    && count($test['inputs'])) {

                    $stdin = implode(' ', $test['inputs']);

                    $logs_all[] = "Test {$test_iteration}/" . count($tests);
                    $logs_all[] = 'Inputs: ' . $stdin;
                } elseif (isset($test['inputs'])
                    && is_array($test['inputs'])
                    && !count($test['inputs'])) {

                    $logs_all[] = 'Inputs: no inputs';
                } elseif (isset($test['inputs'])
                    && !is_array($test['inputs'])) {

                    $logs_all[] = 'Inputs: system error: inputs is not array';
                } elseif (!isset($test['inputs'])) {

                    $logs_all[] = 'Inputs: system error: inputs is not set';
                }

                $stdin = preg_replace("/[^a-zA-ZА-Яа-я0-9\s\-.]/", '', $stdin);
                $stdin = escapeshellarg($stdin);

                // check output
                if (!isset($test['output'])) {
                    $logs_all[] = 'Reference output: is not set';
                    $skip = true;
                } elseif (isset($test['output'])
                        && !is_string($test['output'])
                        && !is_numeric($test['output'])) {
                    $logs_all[] = 'Reference output: set, but not string or numeric';
                    $skip = true;
                }

                if ($skip) {
                    $logs_all[] = 'Skipped';
                    $logs_all[] = '';

                    continue;
                }

                // save user code to temp file
                file_put_contents($this->path_user_script_file_name, $code);

                // start docker container with user code and current iteration inputs
                $docker_random_name = 'python_test_' . date('Y-m-d_H-i-s') . rand(1000, 9999);
                $cmd = "docker run -v {$this->path_tmp_data}:/tmp/data --rm --name {$docker_random_name} python-app bash /tmp/data/run-test.sh '{$this->user_script_file_name}' {$stdin} 2>&1";
                exec($cmd, $result_exec);

                $result_exec = implode("\n", array_map(function($el){
                    return htmlspecialchars($el);
                }, $result_exec));
                $logs_all[] = 'Reference Output: ' . trim($test['output']);
                $logs_all[] = 'Output: ' . $result_exec;
                $logs_all[] = '';

                $results[] = trim($result_exec) == trim($test['output']);
            }

            $return = [
                'code' => $code,
                'tests' => $tests,
                'logs' => implode("\n", $logs_all),
                'results' => $results,
            ];

            echo json_encode($return);

        } catch (\Exception $e) {
            $return = [
                'data' => $data,
                'code' => $code,
                'tests' => $tests,
                'logs' => $e->getMessage(),
                'results' => [],
            ];

            echo json_encode($return);
        }
    }


}
