<?php namespace Igvs\Courses\Controllers;

use Illuminate\Routing\Controller as ControllerBase;

class Auth extends ControllerBase
{
    public function signin()
    {
        $c = new \Backend\Controllers\Auth();
        $c->addJs('/plugins/igvs/courses/assets/js/auth_logout_check.js');

        return $c->run('signin');
    }

    public function back_uri_forget()
    {
        \Session::forget('url.intended');

        return \Redirect::to(\Backend::baseUrl('backend/auth'));
    }
}
