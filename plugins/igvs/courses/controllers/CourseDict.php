<?php namespace Igvs\Courses\Controllers;

use Illuminate\Routing\Controller as ControllerBase;
use Lang;
use Igvs\Courses\Models\Vocabulary;
use Igvs\Courses\Models\Course as CourseModel;

class CourseDict extends ControllerBase
{
    use \Igvs\Courses\Traits\CheckAccess;

    function exportDict($id)
    {
        $course = CourseModel::find($id);

        if (!$this->checkAccessCourse(
            $course->id))
            throw new Exception ('Access denied');

        header('Content-Type: text/plain');
        header("Content-Disposition: attachment; filename=\"dict_export_{$id}_" . date('Y-m-d_H-i-s') . '.csv"');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $vocabulary = $course->vocabulary()->get();

        $header = [
            'id',
            'term',
            'term_clean',
            'description',
        ];

        echo '"' . implode('";"', $header) . "\"\r\n";

        if ($vocabulary) {
            foreach ($vocabulary as $row) {
                $csv_row = '"' . implode('";"', [
                    'id' => $row->id,
                    'term' => str_replace('"', '""', $row->term),
                    'term_clean' => str_replace('"', '""', $row->term_clean),
                    'description' => str_replace('"', '""', $row->description),
                ]) . '"';

                $csv_row = str_replace(["\r","\n"],["", ""], $csv_row);

                echo mb_convert_encoding($csv_row, 'cp1251', 'utf-8');
                echo "\r\n";
            }
        }

        exit();
    }

    function importDictExample($id)
    {
        $course = CourseModel::find($id);

        if (!$this->checkAccessCourse(
            $course->id))
            throw new Exception ('Access denied');

        header('Content-Type: text/plain');
        header("Content-Disposition: attachment; filename=\"dict_example_" . date('Y-m-d_H-i-s') . '.csv"');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $header = [
            'id',
            'term',
            'term_clean',
            'description',
        ];

        $vocabulary = [
            (object)[
                'id' => '',
                'term' => 'Атом',
                'term_clean' => 'Атом',
                'description' => 'Мельчайшая частица химического элемента, состоящая из ядра и электронов.',
            ],
            (object)[
                'id' => '',
                'term' => '<p>Ани<sup>оны</sup></p>',
                'term_clean' => 'Анионы',
                'description' => '<p>отрицательно заряженные ионы</p>',
            ],
            (object)[
                'id' => '',
                'term' => 'Клапан',
                'term_clean' => 'Клапан',
                'description' => 'деталь самодействующих и регулируемых устройств путем открытия или закрытия проходного отверстия, через которое расходуются жидкости, газы или пар.',
            ],
        ];

        echo '"' . implode('";"', $header) . "\"\r\n";

        if ($vocabulary) {
            foreach ($vocabulary as $row) {
                $csv_row = '"' . implode('";"', [
                    'id' => $row->id,
                    'term' => str_replace('"', '""', $row->term),
                    'term_clean' => str_replace('"', '""', $row->term_clean),
                    'description' => str_replace('"', '""', $row->description),
                ]) . '"';

                $csv_row = str_replace(["\r","\n"],["", ""], $csv_row);

                echo mb_convert_encoding($csv_row, 'cp1251', 'utf-8');
                echo "\r\n";
            }
        }

        exit();
    }

    function parseDictOld($post)
    {
        if (!count($_FILES)) {
            echo '[]';
            exit();
        }

        $return = [];

        $content = explode("\r\n", file_get_contents($_FILES['dict_file']['tmp_name']));

        foreach ($content as $content_key => $content_row) {
            if (!$content_key)
                continue;

            $array_content = explode(';', $content_row);

            if (count($array_content) < 4)
                continue;

            $array_content[1] = mb_convert_encoding($array_content[1], 'utf-8', 'cp1251');
            $array_content[2] = mb_convert_encoding($array_content[2], 'utf-8', 'cp1251');
            $array_content[3] = mb_convert_encoding($array_content[3], 'utf-8', 'cp1251');

            $return[] = $array_content;

        }

        print_r($return);

        exit();
    }

    function dictParse($course_id)
    {
        \Backend\Models\Preference::setAppLocale();

        if (!count($_FILES) || !$_FILES['dict_file']['tmp_name']) {
            echo 'Ошибка. Не удаётся прочитать импортируемый файл. CODE 319';
            exit();
        }

        $course_dict = Vocabulary::where('course_id', $course_id)
            ->lists('term', 'id');

        $course_dict_ids = array_keys($course_dict);
        $course_dict_values = array_values($course_dict);

        foreach ($course_dict_values as $key => $val) {
            $course_dict_values[$key] = strip_tags($val);
        }

        $content_handle = fopen($_FILES['dict_file']['tmp_name'], "r");

        $iterator = 1;
        $unique_dict_import = [];

        echo '<input type="hidden" name="md5_file" value="' . md5_file($_FILES['dict_file']['tmp_name']) . '">';

        echo '<table class="dict-table"><tr>
                <td>№ <br> п/п</td>
                <td>' . Lang::get('igvs.courses::vocabulary.field.term') . '</td>
                <td>' . Lang::get('igvs.courses::vocabulary.field.description') . '</td>
                <td>' . Lang::get('igvs.courses::lang.default.command.action') . '</td>
                <td>' . Lang::get('igvs.courses::lang.default.command.import') . '<br><input type="checkbox" id="check-all"></td>
                </tr>';

        while ($content_row = fgetcsv($content_handle, 0, ';', '"')) {
            if (!$iterator)
                continue;

            if (count($content_row) < 3 || !strlen($content_row[1]))
                continue;

            if (count($content_row) < 4)
                $content_row[3] = $content_row[2];

            $content_row[0] = mb_convert_encoding($content_row[0], 'utf-8', 'cp1251');
            $content_row[1] = mb_convert_encoding($content_row[1], 'utf-8', 'cp1251');
            $content_row[2] = mb_convert_encoding($content_row[2], 'utf-8', 'cp1251');

            if (count($content_row) == 4)
                $content_row[3] = mb_convert_encoding($content_row[3], 'utf-8', 'cp1251');

            $status = 'new';

            if (in_array((int)$content_row[0], $course_dict_ids)) {
                $status = 'rewrite';
            } elseif(in_array(strip_tags($content_row[1]), $course_dict_values)) {
                $status = 'existed';
            }

            if (in_array(strip_tags($content_row[1]), $unique_dict_import)) {
                $status = 'double';
            } else {
                $unique_dict_import[] = strip_tags($content_row[1]);
            }

            $checked = in_array($status, ['new', 'rewrite']) ? 'checked' : '';

            echo '<tr>';
            echo "<td>$iterator</td>";
            echo "<td>" . strip_tags($content_row[1]) . "</td>";
            echo "<td>" . strip_tags($content_row[3]) . "</td>";
            echo "<td>" . Lang::get('igvs.courses::vocabulary.statuses.' . $status) . "</td>";
            echo "<td><input class='approve' type='checkbox' name='dict[approve][" . $iterator . "]' {$checked}></td>";
            echo '</tr>';

            $iterator++;
        }

        fclose($content_handle);

        echo '</table>';

        exit();
    }

    function dictimport($course_id)
    {
        if (!count($_FILES) || !$_FILES['dict_file']['tmp_name']) {
            echo 'Ошибка. Не удаётся прочитать импортируемый файл.';
            exit();
        }

        $md5_file = md5_file($_FILES['dict_file']['tmp_name']);

        $post_md5_file = post('md5_file');
        $dict = post('dict');

        if ($md5_file != $post_md5_file) {
            echo 'Вы выбрали другой файл. Нажмите Импортировать.';
            exit();
        }

        $approve = isset($dict['approve']) ? $dict['approve'] : [];
        $approve = array_keys($approve);

        $course_dict = Vocabulary::where('course_id', $course_id)
            ->lists('term', 'id');

        $course_dict_ids = array_keys($course_dict);
        $course_dict_values = array_values($course_dict);

        $course_dict_term_clean = [];

        foreach ($course_dict_values as $key => $val) {
            $course_dict_term_clean[$key] = strip_tags($val);
        }

        $content_handle = fopen($_FILES['dict_file']['tmp_name'], "r");

        $iterator = 1;
        $saved = 0;

        while ($content_row = fgetcsv($content_handle, 0, ';', '"')) {
            if (!$iterator)
                continue;

            if (count($content_row) < 3 || !strlen($content_row[1]))
                continue;

            if (count($content_row) < 4)
                $content_row[3] = $content_row[2];

            $content_row[0] = mb_convert_encoding($content_row[0], 'utf-8', 'cp1251');
            $content_row[1] = mb_convert_encoding($content_row[1], 'utf-8', 'cp1251');
            $content_row[2] = mb_convert_encoding($content_row[2], 'utf-8', 'cp1251');

            if (count($content_row) == 4)
                $content_row[3] = mb_convert_encoding($content_row[3], 'utf-8', 'cp1251');

            if (in_array($iterator, $approve)) {
                if (isset($content_row[0]) && $content_row[0] != 0 && in_array($content_row[0], $course_dict_ids)) {
                    $dict = Vocabulary::find($content_row[0]);
                    if (!$dict)
                        $dict = new Vocabulary();
                } else {
                    $dict = new Vocabulary();
                }

                $dict->course_id = $course_id;
                $dict->term = $content_row[1];
                $dict->term_clean = strip_tags($content_row[1]);
                $dict->description = $content_row[3];
                $dict->save();

                $saved++;
            }

            $iterator++;
        }

        fclose($content_handle);

        echo "Всего импортировано: {$iterator}, сохранено: {$saved}";
    }
}