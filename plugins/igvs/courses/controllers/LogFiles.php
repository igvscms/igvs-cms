<?php namespace Igvs\Courses\Controllers;

use View;
use Response;
use BackendAuth;
use BackendMenu;
use Lang;
use Backend\Classes\Controller;
use Exception;
use Config;
use Igvs\Courses\Models\Logging;
use Igvs\Courses\Models\LogFile;
//use Igvs\Courses\Models\ModuleContentHistory;

/**
 * Contents Back-end Controller
 */
class LogFiles extends Controller
{
    use \Igvs\Courses\Traits\CheckAccess;
    use \Igvs\Courses\Traits\CustomRoutes;


    public $implement = [
        'Backend.Behaviors.ListController',
    ];

    public $listConfig = 'config_list.yaml';

    public $content_id = 0;

    public function view($content_id=0)
    {
        $count = LogFile::where('content_id',$content_id)->count();
        if($count>0) {
            $this->layoutPath = ['plugins/igvs/courses/layouts'];
            $this->content_id = $content_id;
            return $this->asExtension('ListController')->index($content_id);
        }
        else
        {
            echo '
                <!doctype>
                <html>
                <head>
                </head>
                <body>
                    <style>
                        div{
                            left:50%;
                            top:50%;
                            transform: translate(-50%,-50%);
                            position: absolute;
                            text-align: center;
                            color:#333;
                            text-transform: uppercase;
                        }
                        h1{
                            font-family: \'Open Sans\',Arial,sans-serif;
                            font-size: 25px;
                            font-weight: 100;
                        }
                        
                    </style>
                    <div>
                        <h1>'.Lang::get('igvs.courses::lang.history.message.file_clear').'</h1>
                    </div>
                </body>';
            exit;
        }
    }

    public function preview($file_id=0)
    {
        $file = LogFile::where('id',$file_id)->first();
        if(!$file)
            exit;
        switch($file->status)
        {
            case 0:
            case 2:
            case 4:
                $file_name = $file->content->getPathContents().$file->name;
            break;
            case 1:
            case 3:
                $file_name = __DIR__.'/../../../../storage/app/igvs/card/'.$file_id;
            break;
        }

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $file_name);

            header('Content-Description: File Transfer');
            header('Content-Type: '.$mime);
            //header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            //header('Content-Length: ' . filesize($file));
            readfile($file_name);
        exit();
    }

    public function listExtendQuery($query)
    {
        $query->where('content_id',$this->content_id);
        $query->getDeveloper();
    }

    public function onRestore()
    {
        return LogFile::restoreFile(__DIR__.'/../../../../storage/app/igvs/module-content/',(int)post('id'));
    }

    public function onRemove()
    {
        return LogFile::removeFile(__DIR__.'/../../../../storage/app/igvs/module-content/',(int)post('id'));
    }
}