<?php namespace Igvs\Courses\Controllers;

use Igvs\Courses\Models\Category;
use View;
use Response;
use BackendAuth;
use BackendMenu;
use Lang;
use Exception;
use Config;
use Session;

use Backend\Classes\Controller;

use Backend\Models\User as BackendUser;

use Igvs\Courses\Controllers\Categories;

use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\Info;
use Igvs\Courses\Models\Status;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\ModuleContentHistory;
use Igvs\Courses\Models\ModuleVersion;

use Academy\Cms\Models\Comment;

use Academy\Tasks\Models\Project;
use Academy\Tasks\Models\HistoryRead;
use Academy\Tasks\Models\CommentRead;
/**
 * Contents Back-end Controller
 */
class Contents extends Controller
{
    use \Igvs\Courses\Traits\CheckAccess;
    use \Igvs\Courses\Traits\AntiCache;
    use \Igvs\Courses\Traits\CustomRoutes;
    use \Igvs\Courses\Traits\FileHelper;

    use \Academy\System\Traits\SocketSender;

    public $socketActionsSendHandlers = [];

    public static function rulesForSendingSocketsMessages() { return [
        'onShortCommentsUpdate' => function($data, $user, $is_current_cocket, $default)
        {
            $module = ModuleContent::find($data['params'][0]);
            return $user['action'] == 'Igvs\Courses\Controllers\Categories::index' && isset($user['params']['id']) && $user['params']['id'] == $module->course_id;
        },
        'onUpdateRubricator' => function($data, $user, $is_current_cocket, $default) {
            return $user['action'] == 'Igvs\Courses\Controllers\Categories::index' && !count(array_diff_assoc($data['params'], $user['params']));
        },
        'otherUserChangeModule' => function($data, $user, $is_current_cocket, $default) {
            return $data['action'] == $user['action']
                && !$is_current_cocket
                && isset($data['params'][0])
                && isset($user['params'][0])
                && $data['params'][0] == $user['params'][0];
        },
    ];}

    //преобразование результата
    public static function socketMessageResultHandler() { return [
        'onShortCommentsUpdate' => function($data, $result)
        {
            if ($user = \BackendAuth::getUser()) {
                $user_id = $user->id;
            }
            else {
                $user_id = null;
            }
            $count = ModuleContent::find($data['params'][0])->read_comments()->count([],$user_id);
            return ['original' => [
                '#module_status_' . $data['params'][0] . ' > .newMessages' => (int)$count ?: ''
            ]];
        },
        'onUpdateRubricator' => Categories::socketMessageResultHandler()['onUpdateRubricator'],
    ];}

    function onUpdateRubricator($course_id = null)
    {
        $options = ['result' => 1];
        if (!is_null($course_id)) {
            $options['params']['id'] = $course_id;
        }
        $this->socketNewSend('onUpdateRubricator', $options);
    }

    public $implement = [
        'Backend.Behaviors.FormController',
        'Academy.Cms.Behaviors.EditListController',
        'Igvs.Courses.Behaviors.ModalController',
        'Academy.System.Behaviors.CrudController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';
    public $listConfig = [
        'deleted' => 'config_list.yaml',
        'courses' => 'config_list_courses.yaml'
    ];

    private $field = '';

    public function __construct()
    {
        $user = \BackendAuth::getUser();

        if ($user && $user->hasPermission('igvs.courses.bag_method')) {
            $this->formConfig = 'config_form_short_version.yaml';
        }
        parent::__construct();

        BackendMenu::setContext('Igvs.Courses', 'courses', 'contents');
    }

    public function onCreateFormModule($context = null)
    {
        if (!$this->checkAccessCourse(post('course_id')))
            throw new \Exception('Access denied');

        $this->asExtension('FormController')->create();

        $this->vars['title'] = Lang::get('igvs.courses::lang.module.command.create');

        if(method_exists($this, 'overrideOnCreateFormPartial')){
            return $this->overrideOnCreateFormPartial();
        }

        return $this->makePartial('create');
    }

    public function onLoadVersions()
    {
        $module = Module::find(post('module_id'));

        if (!$module)
            return;

        # versions
        $dropdown = '';
        foreach ($module->versions as $v)
            $dropdown .= "<option value='{$v->version}'>{$v->version} ($v->status : ".strstr($v->release_date,' ',true).")</option>";

        return [
            "form [name='ModuleContent[version]']" => $dropdown
        ];
    }

    public function designer($id)
    {
        $this->vars['model'] = ModuleContent::findOrFail($id);
        $url = $this->vars['model']->getDesignerUrl();

        $this->addJs('/modules/cms/formwidgets/mediafinder/assets/js/mediafinder.js', 'core');
        $this->addCss('/modules/cms/formwidgets/mediafinder/assets/css/mediafinder.css', 'core');

        $this->addJs('/plugins/igvs/courses/assets/js/module-content.api.js?1');
        $this->addCss('/plugins/igvs/courses/assets/css/designer.css');


        $this->addJs($url.'/../assets/lang/lang-common_'.Config::get('app.locale').'.js');
        $this->addJs($url.'/../assets/lang/lang_'.Config::get('app.locale').'.js');
        $this->layoutPath = ['plugins/igvs/courses/layouts'];
    }

    private function canShowField($field, $module, $context)
    {
        $globalAccess = [
            'files_history' => 'log',
            'json_history' => 'json_history',
            'json' => 'data',
            'builds' => 'builds'
        ];
        if ((isset($globalAccess[$context]) && !$this->user->hasAccess('igvs.courses.content_'.$globalAccess[$context]))
            || (!$module->checkAccessOperation($module->course_id, "ModuleTabEdit_$context", true))
        ) {
            return false;
        }
        switch ($field) {
            case 'custom_css':
                return $module->module->path === 'html-markup';
            case 'comments':
                return $module->course->iCanSeeExaminationSystem();
            case 'project_comments':
                return !$module->course->short_version && $module->course->project_id;
            default:
                return true;
        }
    }

    private function getFields($form, $module)
    {
        $fields = [];
        foreach($form->config->tabs['fields'] as $name => $field) {
            if ($this->canShowField($name, $module, $field['context'])) {
                $fields[$field['context']] = trans('igvs.courses::lang.content.tabs.' . $field['context']);
            }
        }
        return $fields;
    }

    public function updateField($recordId = null, $context = null)
    {
        if ($context = get('context')) {
            Session::put('active', $context);
        }
        $this->initForm((object)['exists' => true]);
        $this->vars['fields'] = $this->getFields($this->widget->form, ModuleContent::find($recordId));
        if(!isset($this->vars['fields'][Session::get('active')])) {
            Session::put('active', 'designer');
        }
        $this->vars['content_id']= $recordId;
        $this->layoutPath = ['plugins/igvs/courses/controllers/contents/update'];
    }

    /*public function socketActionsReceive($handler, $action, $params, $is_current_window, $default)
    {
        if ($handler == 'otherUserChangeModule' && $is_current_window) {
            return false;
        }
    }*/

    public function update_onSave($recordId = null, $context = null)
    {
        if(!is_null($recordId)) {
            $content = ModuleContent::where('id',$recordId)->first();
            if ($content && !$content->isReadonly()) {
                parent::update_onSave($recordId);
            }
            if ($context == 'properties') {
                $this->onUpdateRubricator($content->course_id);
            }
        }
        $this->sendMessage('otherUserChangeModule', 'Данный модуль был изменен в другом окне, пожалуйста, обновите страницу', 'warning');
    }

    public function sendMessage($handler, $text, $class)
    {
        $this->socketNewSend($handler, [
            'result' => [
                'original' => [
                    '#layout-flash-messages' => '<p class="flash-message ' . $class . '" data-control="flash-message" data-interval="5">' . htmlspecialchars($text) . '</p>',
                ],
            ]
        ]);
    }

    public function initRelation($model, $field = null)
    {
        $result = parent::initRelation($model, $field);

        if ($field == null) {
            $field = post('_relation_field');
        }
        if ($field == 'comments') {
            $this->relationModel->data = $model->status_id;
        }
        return $result;
    }

    public function formExtendQuery($query)
    {
        $query->withTrashed();
    }

    public function update($recordId = null, $context = null)
    {
        Session::put('active', $context);
        $this->field = $context;
        $this->addJs('/plugins/igvs/courses/assets/js/copy-to-clipboard.js', 'core');
        $this->addJs('/modules/cms/formwidgets/mediafinder/assets/js/mediafinder.js', 'core');
        $this->addCss('/modules/cms/formwidgets/mediafinder/assets/css/mediafinder.css', 'core');
        $this->addCss('/plugins/igvs/courses/assets/css/forms.css','core');
        $this->addJs('/plugins/igvs/courses/widgets/categorieswidget/assets/js/lang/lang.'.Config::get('app.locale').'.js');

        $this->layoutPath = ['plugins/igvs/courses/controllers/contents'];

        $module = ModuleContent::withTrashed()->find($recordId);

        // check assess
        if (!$module || !$this->checkAccessCourse($module->course_id))
            return Response::make(View::make('backend::access_denied'), 403);

        if ($context == 'comments') {
            $module->read_comments()->create();
        }

        $this->asExtension('FormController')
            ->update($recordId, $context);
    }

    static function get(&$value, $default)
    {
        if(isset($value))
            return $value;

        return $default;
    }

    static function getNameType($type)
    {
        $names = ['json', 'designer', 'update', 'rollback'];
        return Lang::get('igvs.courses::lang.history.changed_by.' . self::get($names[$type], 'unknown'));
    }

    private static function getHistoryDates($id, $maxTime = null)
    {
        $query = ModuleContentHistory::where('module_content_id', $id)
            ->orderby('created_at', 'desc');
        if (!is_null($maxTime)) {
            $query->where('created_at', '<', $maxTime);
        }
        return $query->get();
    }

    private static function getHistoryGroups($id)
    {
        $content = ModuleContent::withTrashed()->find($id);
        $result = [
            [
                (object) [
                    'id' => null,
                    'module_content_id' => $content->id,
                    'updated_at' => $content->updated_at,
                    'name' => $content->name,
                    'module_content_code' => $content->code,
                    'module_version' => $content->getModuleVersion(),
                    'behavior' => $content->behavior,
                    'module' => $content->module,
                    'getPrevVersion' => json_decode($content->data, true),
                    'type' => $content->type,
                    'ip' => $content->ip,
                    'user_id' => $content->user_id,
                    'created_at' => $content->updated_at,
                ],
            ]
        ];
        $pos = 1;
        $maxTime = null;
        do {
            if (count($history = Self::getHistoryDates($id, $maxTime))) {
                $result[$pos] = $history;
            }
            $pos++;
            $content = ModuleContent::withTrashed()->find($id);
            $maxTime = $content->created_at;
        }
        while ($id = $content->parent_id);
        return $result;
    }

    private static function findInGroup($groups, $id, $getFirst = false)
    {
        foreach ($groups as $group) {
            foreach ($group as $num => $item) {
                if (($item->id == $id || $getFirst) && empty($item->selected)) {
                    $item->selected = true;
                    return $item;
                }
            }
        }
        if (count($groups) && !$getFirst) {
            return Self::findInGroup($groups, $id, true);
        }
    }

    private static function getFullVersion($version)
    {
        $content = ModuleContent::withTrashed()->find($version->module_content_id);

        if ($course = Course::withTrashed()->find($content->course_id)) {
            $course = '<a href="' . \Backend::url('igvs/courses/' . $content->course_id . '/categories#module_' . $version->module_content_id) . '" target="_top">' . $course->code . ': ' . $course->title . '</a>';
        }
        return (object) [
            'id' => $version->id,
            'content_id' => $version->module_content_id,
            'updated_at' => $version->updated_at,
            'name' => $version->name,
            'code' => $version->module_content_code,
            'version' => $version->module_version,
            'behavior' => $version->behavior,
            'module_id' => $version->module->name,
            'course' => $course,
            'data' => isset($version->getPrevVersion) ? $version->getPrevVersion : $version->getPrevVersion(),
            'type' => self::getNameType($version->type),
            'ip' => $version->ip,
        ];
    }

    public static function str_pad($label, $count)
    {
        return $label . str_repeat(' ', $count - mb_strlen($label));
    }

    public function versionKeeper($id)
    {
        $this->vars['versions'] = $groups = Self::getHistoryGroups($id);
        $this->vars['autoselect'] = $autoselect = post('autoselect', 1);

        $version2 = Self::findInGroup($groups, post('version2'));
        if (!$version1 = Self::findInGroup($groups, post('version1'))) {
            die($this->makePartial('clear_history'));
        }

        $this->vars['version1'] = Self::getFullVersion($version1);
        $this->vars['version2'] = Self::getFullVersion($version2);
        $content = ModuleContent::withTrashed()->find($id);
        $this->vars['is_trashed'] = !is_null($content->deleted_at);
        $this->vars['read'] = $content->isReadonly();

        $this->vars['fields'] = [
            'ip' => 'IP',
            'name' => 'igvs.courses::lang.default.txt_field.name',
            'code' => 'igvs.courses::lang.default.txt_field.code',
            'module_id' => 'igvs.courses::lang.module.txt_field.template',
            'version' => 'igvs.courses::lang.default.txt_field.version',
            'behavior' => 'igvs.courses::lang.content.txt_field.behavior',
            'course' => 'igvs.courses::lang.course.self',
            'type' => 'igvs.courses::lang.history.txt_field.changed_by',
        ];
        $fieldLength = 0;
        foreach ($this->vars['fields'] as $name => $label) {
            if ($fieldLength < $length = mb_strlen($this->vars['fields'][$name] = trans($label))) {
                $fieldLength = $length;
            }
        }
        $this->vars['fieldLength'] = $fieldLength + 1;

        $this->layoutPath = ['plugins/igvs/courses/controllers/contents/version_diff'];
    }

    public function logging($content_id)
    {
        $this->layoutPath = ['plugins/igvs/courses/controllers/contents/log'];

        $this->vars['version1'] = [];
    }

    private static function appleDiff($curr,$next,$diff)
    {
        foreach($next as $key => $val)
        {
            if(isset($diff[$key]))
            {
                if(!isset($curr[$key]))
                    unset($next[$key]);
                else
                    if(is_array($diff[$key]))
                        $next[$key] = self::appleDiff($curr[$key],$next[$key],$diff[$key]);
                    else
                        $next[$key] = $curr[$key];
            }
        }
        return $next;
    }

    public function versionBack($id)
    {
        $history_id = post('version1');
        if ($history_id > 0) {
            $history = ModuleContentHistory::where('id',$history_id)->first();//то к чему откатываемся
            //if ($history->module_content_id == $id) {
            $content = ModuleContent::where('id', $id)->first();
            if ($history->name!='') {
                $content->name = $history->name;
            }
            //$content->code = $history->module_content_code;
            $content->version = $history->module_version;
            $content->module_id = $history->module_id;
            $content->behavior = $history->behavior;
            $next = $history->getPrevVersion();
            $curr = json_decode($content->data,true);

            $diff = post('diff');
            if(is_array($diff)) {
                $next = self::appleDiff($curr, $next, $diff);
            }
            $content->data = json_encode($next);
            $content->type = 30;//3 - откат истории
            $content->save();
            //$this->update($history->module_content_id);
            //}
        }
        header("Location: ../update/".$id);
        exit;
    }

    public function preview($recordId, $context = null)
    {
        $this->addCss('/plugins/igvs/courses/assets/css/module-content-form.css', 'core');

        $this->layoutPath = ['plugins/igvs/courses/layouts'];

        $this->asExtension('FormController')
            ->preview($recordId, $context);
    }

    public function update_onInsertFiles()
    {
        $document_root =  realpath(dirname(__DIR__).'/../../..');


        $lesson_id = post('id');
        $files_path = post('files_path'); //надо парсить по разделителю |

        $module = ModuleContent::find($lesson_id);

        if (!$module)
            throw new \Exception("Dest module (ID:{$lesson_id}) was not found");

        // check assess
        if (!$this->checkAccessCourse($module->course_id))
            throw new \Exception("Access to course with ID {$module->course_id} denied");

        $files_path = explode('|', $files_path);

        if (!count($files_path))
            return;

        $return = [];
        $return2 = [];

        foreach ($files_path as $file_path) {
            $source = $document_root . $file_path;
            $dest = $module->getPathContents() . preg_replace('/\/storage\/app\/igvs\/module-content\/([0-9]+)\/([0-9]+)/', '', $file_path);
            $return[] = $this->recourceLn($source, $dest, null, null, true);

            $return2[] = [
                'source' => $source,
                'dest' => $dest,
            ];

        }

        return json_encode((object)[$return,$return2]);
    }

    public function update_onLoadJson()
    {
        $module = ModuleContent::find(post('id'));

        // check assess
        if (!$this->checkAccessCourse($module->course_id))
            throw new \Exception('Access denied');

        $module_html_markup_id = Module::where('path', 'html-markup')->first();
        $module_html_markup_id = $module_html_markup_id ? $module_html_markup_id->id : -1;

        if ($module->module_id != $module_html_markup_id) {
            $obj = json_decode($module->data);

            if (is_object($obj)) {
                $obj->colontitle = $module->name;
                $obj->practice = $module->isPractice();
            }

            return json_encode($obj);
        } else {
            if ((!strlen($module->data) || $module->data == "[\n  ]") && !is_null($module->content)) {
                preg_match("/\<head\>(.*)\<\/head\>/s", $module->content, $head);
                $head = isset($head[1]) ? $head[1] : '';
                preg_match("/\<body.*\>(.*)\<\/body\>/sU", $module->content, $body);
                $body = isset($body[1]) ? $body[1] : '';

                $obj = [
                    'questions' => [[
                        'typeTest' => 'html-markup',
                    ]],
                    'head' => "<head>{$head}</head>",
                    'html' => !is_null($module->content) ? "<body><div class=\"froala-editor\">{$body}</div></body>" : '<body><div class=\"froala-editor\"></div></body>'
                ];

                return json_encode($obj, JSON_UNESCAPED_UNICODE);
            }

            return json_encode(json_decode($module->data));
        }

    }

    public function onRelationManageCreate()
    {
        $result = parent::onRelationManageCreate();
        if (post('_relation_field') == 'comments') {
            $this->socketNewSend('onShortCommentsUpdate',[
                'result' => true
            ]);
        }
        return $result;
    }

    public function update_onSaveJson()
    {
        if (!$module = ModuleContent::find(post('id'))) {
            \Flash::error('Access denied');
            return;
        }

        if ($module->isReadonly())
            return;

        // check assess
        if (!$this->checkAccessCourse($module->course_id))
            throw new \Exception('Access denied');

        $module_html_markup_id = Module::where('path', 'html-markup')->first();
        $module_html_markup_id = $module_html_markup_id ? $module_html_markup_id->id : -1;

        $obj = json_decode(post('data'));
        if ($module->module_id != $module_html_markup_id && is_object($obj)) {
            if(isset($obj->colontitle))
                unset($obj->colontitle);
            if(isset($obj->practice))
                unset($obj->practice);
        }
        $module->data = json_encode($obj);
        $module->type = 10;//сохранено из конструктора
        $module->save();
        $this->sendMessage('otherUserChangeModule', 'Данный модуль был изменен в другом окне, пожалуйста, обновите страницу', 'warning');
    }

    public function update_onLoadPermission()
    {
        $module     = ModuleContent::find(post('id'));
        $permission = post('permission');

        // check assess
        if (!$this->checkAccessCourse($module->course_id))
            throw new \Exception('Access denied');

        $readOnly = $module->isReadonly();

        $need = [
            'image' => 'ChangeModuleImages',
            'audio' => 'ChangeModuleAudio',
            'video' => 'ChangeModuleVideo',
            'text' => 'ChangeModuleText',
            'structure' => 'ChangeModuleStructure',
        ];
        $perm = $this->getPermissionsCourse($module->course_id, $need);
        $permissions = [
            'no_limits' => (boolean)$module->course->constructor_all_privileges
        ];
        foreach ($need as $key => $prm) {
            $permissions[$key] = !$readOnly && in_array($prm, $perm);
        }

        $return  = [
            'param_in' => [
                'id'            => post('id'),
                'permission'    => post('permission'),
            ]
        ];

        if ($permission && strlen($permission)) {
            $return['result'] = isset($permissions[$permission]) ? $permissions[$permission] : null;
        } else {
            $return['result'] = $permissions;
        }

        return json_encode($return);
    }

    public function formExtendFields($form, $context)
    {
        if (\Session::get('locale') === 'ru') {
            foreach ($form->fields as &$field) {
                if (isset($field['type']) && $field['type'] === 'datepicker') {
                    $field['firstDay'] = 1;
                }
            }
            foreach ($form->tabs['fields'] as &$field) {
                if (isset($field['type']) && $field['type'] === 'datepicker') {
                    $field['firstDay'] = 1;
                }
            }
            unset($field);
        }

        $globalAccess = [
            'files_history' => 'log',
            'json_history' => 'json_history',
            'json' => 'data',
            'builds' => 'builds'
        ];

        if (!empty($form->model->id)) {

            $readOnly = $form->model->isReadonly();

            foreach ($form->tabs['fields'] as $field => $value) {
                if (isset($globalAccess[$value['context']]) && !$this->user->hasAccess('igvs.courses.content_'.$globalAccess[$value['context']])) {
                    $form->removeField($field);
                    continue;
                }
                if ($field == 'comments' && !$form->model->course->iCanSeeExaminationSystem()) {
                    $form->removeField($field);
                    continue;
                }
                if ($field == 'project_comments' && ($form->model->course->short_version || !$form->model->course->project_id)) {
                    $form->removeField($field);
                    continue;
                }
                if (($field == 'module' && $form->model->module->path == 'html-markup') || $readOnly) {
                    $value['disabled'] = true;
                }
                if (Lang::get($value['context']) == $this->field) {
                    $value['context'] = $this->field;
                    $form->addTabFields([$field => $value]);
                    $added[$field] = 1;
                }
                elseif (isset($value['spec']) && $value['spec'] == true) {
                    unset($value['spec']);
                    $value['context'] = $this->field;
                    unset($value['type']);
                    $value['label'] = '';
                    $value['cssClass'] = 'hidden';
                    $form->addTabFields([$field => $value]);
                }
            }
        }
    }

    public function formExtendFieldsBefore($form)
    {
        $active_tab = !isset($_COOKIE['course_table_active']) ? null :
            $_COOKIE['course_table_active'];

        if ($active_tab)
            $form->tabs['defaultTab'] = $active_tab;
    }

    public function minicreate()
    {
        return $this->asExtension('CrudController')
            ->crudcreate('minicreate');
    }

    public function minicreate_onSave()
    {
        return $this->asExtension('CrudController')
            ->crudcreate_onSave();
    }

    public function crudcreate_onSave()
    {
        Info::check('createModule');
        $res = $this->asExtension('CrudController')
            ->crudcreate_onSave();

        # Определение и вставка элемента с учетом сортировки
        $pos = post('ModuleContent.position');
        $new = $this->formGetModel();

        $stmt = ModuleContent::where('category_id', $new->category_id)
            ->where('course_id', $new->course_id);

        // Вставка после указанного элемента
        if (!empty($pos) && !in_array($pos, ['last', 'first', '0'])) {

            $model = $stmt->where('id', $pos)
                ->first();

            if ($model) {
                $new->moveAfter($model);
            }
        }
        // Вставка в начало
        else if ($pos == 'first') {
            $new->resort();
        }
        // Вставка в конец
        else {
            $sort = (int) $stmt->max('sort');
            ModuleContent::where('id', $new->id)
                ->update(['sort' => ++$sort]);
        }

        return $res;
    }

    public function onUpdateNewComment($id)
    {
        if (!$content = ModuleContent::find($id)) {
            return;
        }
        $comment = new Comment();
        $comment->data = $content->status_id;
        $content->comments()->add($comment);
        $_POST['_relation_field'] = 'comments';
        $_POST['_relation_extra_config'] = 'W10=';
        $_POST['manage_id'] = $comment->id;
        return parent::onRelationButtonUpdate();
    }

    public function getHeadValueForExportCsv($column, $result, $relation)
    {
        if ($relation != 'comments' || $column->columnName != 'data') {
            return $result;
        }
        return trans('igvs.courses::lang.default.status.status');
    }

    public function getColumnValueForExportCsv($column, $record, $result, $relation)
    {
        if ($relation != 'comments' || $column->columnName != 'data') {
            return $result;
        }
        if ($status = Status::find($record->data)) {
            return $status->name;
        }
    }

    public function onInfoCheck() {
        $info = Info::check(post('info_code'), $this->user->id);
        return ['#info_' . $info->id => ''];
    }

    public function listExtendQuery($query, $definition = null)
    {
        if (isset($this->params[0])) {
            $version = new ModuleVersion();
            $versionTable = $version->getTable();
            $query->where('course_id', $this->params[0])
                ->addSelect(\Db::raw("`$versionTable`.`version` `last_version`"))
                ->join($versionTable, "$versionTable.module_id", '=', 'igvs_courses_modules_contents.module_id')
                ->join(\Db::raw("(SELECT max(`id`)`id` FROM `$versionTable` GROUP BY `module_id`)`v`"), 'v.id', '=', "$versionTable.id");
        }
        else {
            $query->onlyTrashed();
        }
    }

    public function history($id)
    {
        $this->vars['model'] = ModuleContent::withTrashed()->find($id);
    }

    public function index($course_id = null)
    {
        if (!is_null($course_id)) {
            $this->layout = 'plugins/academy/system/layouts/clear';
        }
        return Parent::index();
    }

    public function onUpdateVersion()
    {
        foreach ((array) post('versions') as $module_id => $version) {
            $module = ModuleContent::findOrFail($module_id);
            if ($version = $module->module->versions()->find($version)) {
                $module->version = $version->version;
                $module->save();
            }
        }
        return $res = $this->asExtension('EditListController')
            ->listRefresh('courses');
    }

    public function onLinkToTheoryModuleForm($id)
    {
        $this_module = ModuleContent::findOrFail($id);

        $structure = [
            [
                'title' => 'Не выбрано',
                'depth' => 2,
                'disabled' => false,
                'code' => '',
                'behavior' => 'ask',
            ]
        ];

        $categories = Category::where('course_id', $this_module->course_id)->orderBy('sort', 'asc')->get();
        $modules = ModuleContent::where('course_id', $this_module->course_id)->orderBy('sort', 'asc')->get();

        foreach($categories as $topic) {
            $structure[] = [
                'title' => $topic->name,
                'depth' => 0,
            ];

            foreach($categories as $act) {

                if ($act->parent_id != $topic->id)
                    continue;

                $structure[] = [
                    'id' => $act->id,
                    'title' => $act->name,
                    'depth' => 1,
                ];

                foreach($modules as $module) {
                    if ($act->id != $module->category_id)
                        continue;

                    $structure[] = [
                        'title' => "{$module->code}: {$module->name}",
                        'code' => $module->id,
                        'behavior' => $module->behavior,
                        'depth' => 2,
                    ];
                }
            }
        }

        return $this->makePartial('link_to_theory_module', [
            'structure' => $structure,
            'module_act' => $this_module->category_id,
            'link_to_theory_module_form' => $this_module->link_to_theory_module_form ? $this_module->link_to_theory_module_form_id : '',
        ]);
    }

    public function onLinkToTheoryModuleFormSave($id)
    {
        $this_module = ModuleContent::findOrFail($id);

        $this_module->link_to_theory_module_form_id = \Input::get('structure_id');

        return $this_module->save();
    }

    public function onLinkToTheoryModuleSave($id)
    {


        return $this->makePartial('link_to_theory_module', [
            'course_id' => $id
        ]);
    }
}
