<?php namespace Igvs\Courses\Components;

use Igvs\Courses\Helpers\Crypt;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\ShellVersion;
use Igvs\Courses\Traits\CourseHelper;
use Request;
use Cms\Classes\ComponentBase;
use Igvs\Courses\Models\Course as CourseItem;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\Vocabulary;
use Igvs\Courses\Models\BehaviorType;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\ModuleTheme;
use Igvs\Courses\Models\TempLink;
use Igvs\Courses\Components\Catalogue;
use Response;
use Config;
use Input;

class Course extends ComponentBase
{
    use \Igvs\Courses\Traits\AntiCache;
    use CourseHelper;

    public $storage_http_path = '/storage/app/igvs/module-content';

    public $page_type;
    public $data;

    public $build_id;
    public $course_id;
    public $topic_id;
    public $course_item;
    public $logo;
    public $bg_hat;
    public $theme;

    public $vars;

    public $file_mime_types = [
        'jpg' => 'image/jpeg',
        'png' => 'image/png',
        'gif' => 'image/gif',
        'mp3' => 'audio/mpeg',
        'js' => 'application/x-javascript',
        'css' => 'text/css',
    ];

    public function componentDetails()
    {
        return [
            'name'        => 'igvs.courses::lang.components.course.name',
            'description' => 'igvs.courses::lang.components.course.description'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function runErrorLink($is_old = false)
    {
        $protocol = isset($_SERVER['HTTPS']) && strlen($_SERVER['HTTPS']) ? 'https' : 'http';
        $domain = $_SERVER['SERVER_NAME'];
        $link = $protocol . '://' . $domain . $_SERVER['REQUEST_URI'];

        if ($is_old) {
            $code = '1';
        }
        else {
            $code = '2';
        }
        header('Location: /preview/error/?link=' . $link . '&code=' . $code);
        exit();
    }

    private function decodeCourseId($course_id)
    {
        if (!(int)$course_id && strlen($course_id) != strlen((int)$course_id)) {
            $course_id = Crypt::decode($course_id, 'course_id');
        }

        return $course_id;
    }

    public function runShell($course_id, $build = null, $topic = null)
    {
        $course_id = $this->decodeCourseId($course_id);

        if (!$this->course_item = CourseItem::find($course_id)) {
            return $this->runErrorLink();
        }
        //$this->logo = $course_item->logo ? $course_item->logo->getPath() : '';
        //$this->bg_hat = $course_item->bg_hat ? $course_item->bg_hat->getPath() : '';

        $this->theme = $this->course_item->getCurrentThemeCode();

        switch ($act = Input::get('act')) {
            case 'list_menu':
                $this->data = $this->menuList($course_id, $build, $topic);
                $this->page['vocabulary'] = $this->getVocabulary($course_id);
                $this->page_type = 'course_config_json';
                break;
            case 'vocabulary':
                $this->data = $this->menuList($course_id, $build, $topic);
                $this->page['vocabulary'] = $this->getVocabulary($course_id);
                $this->page_type = 'course_vocabulary_json';
                break;
            case 'assessment':
            case 'index':
                $this->data = $this->index($act);
                $this->page_type = 'data';
                break;
            case 'moduledata':
                $this->vars['lesson'] = $lesson = $this->getLesson();
                $this->data = $lesson->data;
                $this->page_type = 'module_data';
                break;
            default:
                $this->data = $this->shell();
                $this->page_type = 'data';
        }
    }

    public function runTlink($temp_link)
    {
        $result = TempLink::where('hash', $temp_link)
            ->where('date_expired', '>', date('Y-m-d H:i:s'))
            ->first();
        if (!$result) {
            return $this->runErrorLink(true);
        }
        return [
            $result->course_id,
            $result->build_id > 0 ? $result->build_id : null,
            $result->topic_id > 0 ? $result->topic_id : null,
        ];
    }

    public function runParams($course_id, $build_id = null, $topic_id = null, $is_javascript_format = false)
    {
        $course_id = $this->decodeCourseId($course_id);

        //получение дерева рубрикатора-----------------------------------------------

        $categories = Category::where('course_id', $course_id)
            ->orderBy('sort');

        $lessons = ModuleContent::where('course_id', $course_id)
            ->orderBy('sort');

        if (!is_null($build_id)) {
            $Catalogue = Catalogue::getCatalogue($build_id, $topic_id, $course_id);
            $categories->whereIn('id', $Catalogue['build_category_ids']);
            $lessons->whereIn('id', $Catalogue['build_module_ids']);
        }
        elseif (!is_null($topic_id)) {
            $categories->where(function($categories) use($topic_id) {
                $categories->where('parent_id', $topic_id)
                    ->orWhere(function ($q) use($topic_id) {
                        $q->whereNull('deleted_at')
                            ->orWhere('id', $topic_id);
                    });;
            });
        }
        $categories->orWhere(function($q) use ($course_id) {
            $q->where('is_separate', 1)
                ->where('course_id', $course_id)
                ->whereNull('deleted_at');
        });

        $menu_list = [];
        $menu_list[null] = (object) [
            'children' => [],
        ];
        foreach ($categories->get() as $cat_value) {
            $menu_list[$cat_value->id] = (object) [
                'name' => self::getMenuItemName($cat_value->name),
                'parent' => $cat_value->parent_id,
                'children' => [],
                'type' => $cat_value->is_separate ? 'separate' : '',
            ];
        }
        $lessons->whereIn('category_id', array_keys($menu_list));

        foreach ($menu_list as $num => $cat) {
            if ($num != null) {
                $menu_list[$cat->parent]->children[] = $cat;
                unset($cat->parent);
            }
        }

        foreach ($lessons->get() as $les_value) {
            if ($les_value->behavior != 'practice' || ($les_value->behavior == 'practice' && $les_value->switch_practice_mode != 2)) {
                $menu_list[$les_value->category_id]->children[] = (object)[
                    'name' => self::getMenuItemName($les_value->name)
                        . ($les_value->behavior == 'practice' && $les_value->practice_title_manage === '1' ? ' ' . $les_value->practice_index_title : ''),
                    'behavior' => $les_value->behavior,
                    'code' => $les_value->code,
                    'id' => $les_value->id,
                ];
            }
            if ($les_value->behavior == 'practice' && $les_value->switch_practice_mode != 1) {
                $menu_list[$les_value->category_id]->children[] = (object) [
                    'name' => self::getMenuItemName($les_value->name) . ' '
                            . ($les_value->practice_title_manage === '1' ? $les_value->practice_assessment_title : '(Assessment)'),
                    'behavior' => 'assessment',
                    'code' => $les_value->code . ' assessment',
                    'id' => $les_value->id . 'a',
                ];
            }
        }

        //получение словаря----------------------------------------------------------

        $vocabulary = Vocabulary::where('course_id', $course_id)->get();

        function sup_trim($string) {
            return trim(str_replace(chr(194) . chr(160), ' ', $string));
        }
        $glossary = [];
        foreach ($vocabulary as $k => $v) {
            $glossary[sup_trim($v->term)] = [
                'description' => sup_trim($v->description),
                'image' => $v->image ? $v->image->getPath() : '',
            ];
        }

        //получение дополнительных полей---------------------------------------------

        $logo_path = '/plugins/igvs/courses/assets/images/default_blank.png';
        $logo_bg_path = '/plugins/igvs/courses/assets/images/default_blank.png';
        $shell_version = $this->getVersion(ShellVersion::orderBy('sort', 'desc')->first());

        if ($course = CourseItem::find($course_id)) {
            $shell_version = $this->getVersion($course->shell_version, $shell_version);
            $logo_path = $course->logo ? $course->logo->getPath() : $logo_path;
            $logo_bg_path = $course->bg_hat ? $course->bg_hat->getPath() : $logo_bg_path;
        }
        $shell_base_url = Config::get('igvs.courses::modulesRelease.baseUrl') . "/shell/{$shell_version}/";

        $content = $this->renderPartial('Course::params' . ($is_javascript_format ? '_js' : ''), [
            'data' => (object) [
                'base'          => $shell_base_url,
                'lang'          => $course->language,
                'title'         => $course->title,
                'theme'         => $course->is_inclusive ? 'inclusive' : $course->theme,
                'logo'          => $logo_path,
                'background'    => $logo_bg_path,
                'contents'      => $menu_list[null]->children,
                'glossary'      => $glossary,
                'saveStats'     => false,
                'comments'      => $course->enable_shell_comments,
                'favorites'     => $course->enable_shell_favorites,
                'screenCapture' => $course->enable_shell_screen_capture,
            ],
        ]);
        return Response::make($content)->header('Content-Type', 'application/javascript');
    }

    public function onRun()
    {
        $params = $this->getRouter()->getParameters();
        $countParams = 5;
        foreach ($params as $num => $value) {
            if (false === $value) {
                $countParams = $num;
                break;
            }
        }

        switch ($params[0]) {
            case 'topic':
                switch ($countParams) {
                    case 3: return $this->runShell($params[1], null, $params[2]);//topic/:course/:topic
                    case 4:
                        if (in_array($params[3], ['params', 'params.js'])) {
                            $is_js_format = substr($params[3], -3, 3) == '.js';
                            return $this->runParams($params[1], null, $params[2], $is_js_format);//topic/:course/:topic/params
                        }
                        return $this->runShell($params[2], $params[1], $params[3]);//topic/:build/:course/:topic
                    case 5:
                        if (in_array($params[4], ['params', 'params.js'])) {
                            $is_js_format = substr($params[4], -3, 3) == '.js';
                            return $this->runParams($params[2], $params[1], $params[3], $is_js_format);//topic/:build/:course/:topic/params
                        }
                    default: return $this->runErrorLink();//error
                }
            case 'tlink':
                switch ($countParams) {
                    case 2: return call_user_func_array([$this, 'runShell'], $this->runTlink($params[1]));//tlink/:build
                    case 3:
                        if (in_array($params[2], ['params', 'params.js'])) {
                            return call_user_func_array([$this, 'runParams'], $this->runTlink($params[1]));//tlink/:build/params
                        }
                    default: return $this->runErrorLink();//error
                }
            default:
                switch ($countParams) {
                    case 1: return $this->runShell($params[0]);//:course
                    case 2:
                        if (in_array($params[1], ['params', 'params.js'])) {
                            $is_js_format = substr($params[1], -3, 3) == '.js';
                            return $this->runParams($params[0], null, null, $is_js_format);//:course/params
                        }
                        return $this->runShell($params[1], $params[0]);//:build/:course
                    case 3:
                        if (in_array($params[2], ['params', 'params.js'])) {
                            $is_js_format = substr($params[2], -3, 3) == '.js';
                            return $this->runParams($params[1], $params[0], null, $is_js_format);//:build/:course/params
                        }
                    default: return $this->runErrorLink();
                }
        }
    }

    public function getLesson()
    {
        $module = ModuleContent::find((int) Input::get('lesson'));

        if (is_object($obj = json_decode($module->data))) {
            $obj->unlock = true;
            $obj->templateLanguage = $this->course_item->language;
            $obj->selected_theme = $this->course_item->theme;
            $obj->view_all_themes = !!$this->course_item->include_all_themes;
            $obj->show_start_screen = ((int)$this->course_item->show_module_start_screen + (int)$module->startscreen_show) > 0;
            $obj->behavior = $module->behavior;
            $obj->colontitle = $module->name;
            $obj->practice = $module->isPractice();
            $obj->trainingUrl = $module->link_to_theory_module_form ? $module->link_to_theory_module_form->code : '';
            $obj->startscreen = [];
            $keys = [
                'unit_title',
                'session_title',
                'qcf_ref',
                'etracker',
                'nos_ref',
                'awarding_body_code',
                'level',
                'credits',
                'qualification_title',
                'learning_outcome',
                'functional_skills',
                'publisher_note',
            ];
            foreach ($keys as $key) {
                $obj->startscreen[$key] = $module->getStartScreenData($key);
            }
            if ($obj->practice
                && $module->practice_title_manage === '1'
            ) {
                $obj->practice_index_title = ' ' . $module->practice_index_title;
                $obj->practice_assessment_title = ' ' . $module->practice_assessment_title;
            }
            else {
                $obj->practice_index_title = '';
                $obj->practice_assessment_title = ' (Assessment)';
            }
            $module->data = json_encode($obj);
        }
        return $module;
    }

    private function getVersion($shell, $default = '1.0')
    {
        return $shell ? implode('.', array_slice(explode('.', $shell->version), 0, 2)) : $default;
    }

    public function shell()
    {
        if (substr($_SERVER['REQUEST_URI'], -1) != '/') {
            header('location: ' . $_SERVER['REQUEST_URI'] . '/');
            die();
        }
        $logo_path = '/plugins/igvs/courses/assets/images/default_blank.png';
        $logo_bg_path = '/plugins/igvs/courses/assets/images/default_blank.png';
        $shell_version = $this->getVersion(ShellVersion::orderBy('sort', 'desc')->first());

        if ($course = $this->course_item) {
            $shell_version = $this->getVersion($course->shell_version, $shell_version);
            $logo_path = $course->logo ? $course->logo->getPath() : $logo_path;
            $logo_bg_path = $course->bg_hat ? $course->bg_hat->getPath() : $logo_bg_path;
        }
        $shell_path = Config::get('igvs.courses::modulesRelease.path') . "/shell/{$shell_version}/";
        $content = file_get_contents($shell_path . '/index.html');
        $body_class_impaired = $course->is_inclusive ? ' class="impaired"' : '';

        if ((int) $shell_version[0] != 5) {
            $shell_base_url = Config::get('igvs.courses::modulesRelease.baseUrl') . "/shell/{$shell_version}/";
            $content = str_replace([
                '<head>',
                'assets/images/ava.png',
                '<div class=logo-block>',
                '<p>CARRY OUT A FULL SERVICE and something else</p>',
                '<script src=js/main.js',
                '<title>I-GVS Shell</title>',
                'debugger;',
                '<body>',
            ],[
                '<head><base href="' . $shell_base_url . '"><meta name="language" content="' . $course->language . '">',
                $logo_path,
                '<div class="logo-block" style="background-image: url(' . $logo_bg_path . ');">',
                "<p>{$course->title}</p>",
                '<script src="/plugins/igvs/courses/assets/js/shell_menu_build.js?i=2"></script><script src=js/main.js',
                "<title>" . preg_replace('/(<br\s*?\\*?\/*?>|&nbsp;|&shy;)/i', ' ', $course->title) . "</title>",
                '',
                "<body{$body_class_impaired}>",
            ], $content);
        }
        return $content;
    }

    public function index($act)
    {
        if (substr(Input::get('lesson'), -1) == 'a') {
            $act = 'assessment';
        }
        if (!$lesson = $this->getLesson()) {
            return 'Not found';
        }
        $module_name = $lesson->module ? $lesson->module->path : 'no_module';
        $module_content_base_path = Config::get('igvs.courses::modulesContent.baseUrl') . "/$lesson->course_id/$lesson->id/";

        $time = time();

        $module_file_base_path = $lesson->module->getUrlByVersion($lesson->getModuleVersion());

        $number_page_visible_css = '';
        if ($this->course_item->enable_html_markup_number_page !== '1') {
            $number_page_visible_css = '<style>.number-page { display: none; }</style>';
        }

        if ($module_name == 'html-markup') {
            return '<html>' . str_replace([
                '<head>',
                '.JPG',
                '../../interface',
                'content.js',
                '</body>',
            ], [
                "<head>\r\n<base href='{$module_content_base_path}'>",
                '.jpg',
                "{$module_file_base_path}interface",
                'content.js?anticache=' . $time,
                $number_page_visible_css . '</body>',
            ], $lesson->content) . '</html>';
        }

        $module_file_path = $lesson->module->getPathByVersion($lesson->getModuleVersion());

        if (!is_file($url_index = $module_file_path . '/' . ($act != 'assessment' ? 'index' : 'assessment') . '.html')) {
            return "Not found index file of $module_name $lesson->getModuleVersion() template in <b>$module_file_path</b>";
        }

        $additional_themes = [];
        $theme_name = '_default';
        if ($this->course_item->include_all_themes == 1) {
            $themes = ModuleTheme::select('name')
                ->where('module_id', $lesson->module_id)
                ->where('version', $lesson->getModuleVersion())
                ->where('name', '!=', '_default')
                ->distinct()
                ->select('name')
                ->lists('name');

            foreach ($themes as $themes_value) {
                if ($themes_value != '' && is_dir($module_file_path . '/themes/' . $themes_value)) {
                    $additional_themes[] = "<link rel=stylesheet href='{$module_file_base_path}themes/{$themes_value}/css/style.css?time=$time' data-theme='{$themes_value}'>";
                }
            }
        }
        elseif ($this->theme != '' && is_dir($module_file_path . '/themes/' . $this->theme)) {
            $theme_name = $this->theme;
        }

        $pattern = '/([A-Za-z0-9:\/.=-]+)/i';
        preg_match($pattern, $_SERVER['HTTP_REFERER'], $http_referer_matches);
        $http_referer = isset($http_referer_matches[0]) ? $http_referer_matches[0] : $_SERVER['HTTP_REFERER'];

        $module_data_url = $http_referer . '?act=moduledata&lesson=' . $lesson->id . '&type=' . ($act != 'assessment' ? 'index' : 'assessment');
        $colontitle_slogan = (string)$this->course_item->colontitle_slogan;
        $colontitle_logo = $this->course_item->colontitle_logo ? "<img src=\"{$this->course_item->colontitle_logo->getPath()}\"/>" : '';
        $theme_replace_new = "{$module_file_base_path}themes/{$theme_name}/css/style.css";

        $content_index = file_get_contents($url_index);
        $content_index = preg_replace(["/(img|content|js|css)\/([a-zA-Z0-9-_]+?)\.(svg|gif|png|js|css)/iUum"], $module_file_base_path . '$1/$2.$3', $content_index, -1, $count);
        $content_index = preg_replace(["/<link rel=('|\"|)stylesheet/iUum"], '<link data-theme="_default" rel=$1stylesheet', $content_index, -1, $count);
        return str_replace([
            '<head>',
            $module_file_base_path . 'content/moduleData.js',//это никогда не работало? - я ни в одной версии ни одного модуля не нашел такую строку
            $module_file_base_path . 'content/moduleDataControl.js',
            'themes/_default/' . $module_file_base_path . 'css/style.css',
            $module_file_base_path . 'content/target-zones.css',
            $module_file_base_path . 'content/parts.css',
            '{{logo}}',
            '{{slogan}}',
        ], [
            '<head class="php-replaced">' . implode($additional_themes, "\r\n") . '<base href=\''. $module_content_base_path .'/\'>',
            $module_data_url . '&time=' . $time,
            $module_data_url . '&time=' . $time,
            $theme_replace_new . '?time=' . $time,
            'target-zones.css?' . $time,
            'parts.css?' . $time,
            $colontitle_logo,
            $colontitle_slogan,
        ], $content_index);
    }

    public function getVocabulary($course_id)
    {
        $course_id = $this->decodeCourseId($course_id);

        $vocabulary = Vocabulary::where('course_id', $course_id)
            ->orderBy('term', 'ASC')
            ->get();

        function sup_trim($string) {
            return trim(str_replace(chr( 194 ) . chr( 160 ), ' ', $string));
        }
        $return = [];
        foreach ($vocabulary as $k => $v) {
            $term = sup_trim($v['term']);

            $return[$term] = [
                'term_clean' => preg_replace('/&(amp;)?([A-Za-z]+?);/','',htmlentities(strip_tags($term))),
                'description' => sup_trim($v['description']),
                'image' => $v->image ? $v->image->getPath() : '',
            ];
        }
        return $return;
    }

    public function menuList($course_id, $build_id, $topic_id)
    {
        $course_id = $this->decodeCourseId($course_id);

        $categories = Category::where('course_id', $course_id)
            ->orderBy('sort');

        $lessons = ModuleContent::where('course_id', $course_id)
            ->orderBy('sort');

        if (!is_null($build_id)) {
            $Catalogue = Catalogue::getCatalogue($build_id, $topic_id, $course_id);
            $categories->whereIn('id', $Catalogue['build_category_ids']);
            $lessons->whereIn('id', $Catalogue['build_module_ids']);
        }
        elseif (!is_null($topic_id)) {
            $categories->where('parent_id', $topic_id)
                ->orWhere(function ($q) use($topic_id) {
                    $q->whereNull('deleted_at')
                        ->orWhere('id', $topic_id);
                });

        }

        $categories->orWhere(function($q) use ($course_id) {
            $q->where('is_separate', 1)
                ->where('course_id', $course_id)
                ->whereNull('deleted_at');
        });

        $categories_ids = [];
        $menu_list = [];
        foreach ($categories->get() as $cat_value) {
            $categories_ids[] = $cat_value->id;
            $menu_list[$cat_value->id] = (array)$cat_value;
            $menu_list[$cat_value->id]['lessons'] = [];
        }
        $lessons->whereIn('category_id', $categories_ids);

        foreach ($lessons->get() as $les_value) {
            $menu_list[$les_value->category_id]['lessons'][] = (array)$les_value;
        }
        $return = [];
        //topics || groups
        function getMenuItemName($item) {
            return str_replace('||', '. ', $item['attributes']['name']);
        }

        foreach ($menu_list as $cat_key => $cat_value) {
            if (is_null($cat_value['attributes']['parent_id'])) {
                $cat_path = ((int) $course_id) . '/' . $cat_value['attributes']['id'];
            }
            else {
                $cat_path = '';
            }

            $return[] = [
                'type' => !is_null($cat_value['attributes']['parent_id']) && $cat_value['attributes']['parent_id'] != "0" ? 'group' : 'topic',
                'number' => $cat_key,
                'resource' => "",
                'id' => $cat_value['attributes']['id'],
                'class' => "",
                'textName' => "",
                'parent' => !is_null($cat_value['attributes']['parent_id']) ? $cat_value['attributes']['parent_id'] : 0,
                'textTitle' => getMenuItemName($cat_value),
                'section' => "eapp",
                'preview' => "",
            ];

            foreach($cat_value['lessons'] as $less_key => $less_value) {
                $return[] = [
                    'type' => 'item',
                    'number' => $less_key,
                    'resource' => $cat_path . '/' . $less_value['attributes']['id'] . '/index',
                    //'resource' => '',
                    'id' => $less_value['attributes']['id'],
                    'class' => $this->getClass($less_value['attributes']['behavior']),
                    'code' => $less_value['attributes']['code'],
                    'textName' => "",
                    'parent' => $cat_value['attributes']['id'],
                    'textTitle' => getMenuItemName($less_value),
                    'section' => "eapp",
                    'preview' => "",
                    'behavior' => $less_value['attributes']['behavior'],
                    'template_module_id' => $less_value['attributes']['module_id'],
                    'practice_title_manage' => $less_value['attributes']['practice_title_manage'],
                    'practice_index_title' => $less_value['attributes']['practice_index_title'],
                    'practice_assessment_title' => $less_value['attributes']['practice_assessment_title'],
                    'switch_practice_mode' => $less_value['attributes']['switch_practice_mode'],
                ];
            }
        }
        return $return;
    }

    public function getClass($type)
    {
        $classes = [
            'practice'  => 'module practice',
            'check'     => 'module practice',
            'test'      => 'module control',
            'ask'       => 'module theory',

            'page'      => 'module theory',
            'module'    => 'module control',
        ];
        return isset($classes[$type]) ? $classes[$type] : '';
    }
}
