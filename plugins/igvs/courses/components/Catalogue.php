<?php namespace Igvs\Courses\Components;

use Request;
use Cms\Classes\ComponentBase;
use Igvs\Courses\Models\Course as CorseItem;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\BehaviorType;
use Igvs\Courses\Models\Build;
use Igvs\Courses\Models\BuildCategory;
use Igvs\Courses\Models\BuildModuleContent;
use Response;

class Catalogue extends ComponentBase
{
    use \Igvs\Courses\Traits\AntiCache;

    public $course_id;
    public $page;

    public $data;

    public $builds;
    public $build_id;
    public $build_name;

    public $topic_count = 0;
    public $modules_count = 0;
    public $ipractice_count = 0;

    public function componentDetails()
    {
        return [
            'name'        => 'igvs.courses::lang.components.catalogue.name',
            'description' => 'igvs.courses::lang.components.catalogue.description'
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function onRun()
    {
        $act = Request::input('act');

        $this->course_id = (int)$this->param('course');
        $this->build_id = (int)$this->param('build');
        $this->builds = Build::all();


        if ((is_null($this->build_id) || $this->build_id == 0) && count($this->builds)) {
            $this->build_id = $this->builds[0]->id;
            $this->build_name = $this->builds[0]->name;
        }
        if (!is_null($this->build_id) && $this->build_id != 0) {
            $build = Build::find($this->build_id);
            if ($build) {
                $this->build_name = $build->name;
            }
        }

        if (!is_null($this->course_id) && $this->course_id != 0) {
            $course = $this->getCourse();

            $topics = [];
            foreach ($course[2] as $key => $value) {
                $topics[$key] = $value;
            }

            uasort($topics, function ($a, $b) {
                if ($a->sort > $b->sort) return 1;
                else if ($a->sort < $b->sort) return -1;
                else return 0;
            });


            $this->data = [
                'item' => $course[0],
                'counts' => $course[1],
                'topics' => $topics
            ];

            $this->page = 'course';
        } else {
            $this->data = $this->getCatalogue($this->build_id);
            $this->page = 'catalogue';
        }

        $this->addCss('assets/css/font2.css', 'core');
        $this->addCss('assets/css/catalogue.css', 'core');

    }

    static public function getCatalogue($build_id, $topic_id = null, $course_id = null) {
//        $courses = CorseItem::where('public_status', '<>', '')
//                            ->where('public_status', '<>', 'unpublished')
//                            ->orderBy('public_status', 'asc')
//                            ->orderBy('id', 'asc')
//                            ->get();
        if (!is_null($course_id)) {
            $build_module_ids = BuildModuleContent::where('igvs_courses_builds_modules_contents.build_id', $build_id)
                ->leftjoin('igvs_courses_modules_contents', 'igvs_courses_builds_modules_contents.module_content_id', '=', 'igvs_courses_modules_contents.id')
                ->where('igvs_courses_modules_contents.course_id', $course_id)
                ->lists('module_content_id');
        } else {
            $build_module_ids = BuildModuleContent::where('build_id', $build_id)->lists('module_content_id');
        }

        $build_courses_ids = [];
        $build_category_ids = [];
//        $build_module_ids = [];
        $topic_ids = [];
//        $courses_ids = [];
        $courses = [];

//        foreach ($build_modules as $key => $value) {
//            $build_module_ids[]=$value->module_content_id;
//        }

        //получаем категории и модули в соответствии сборке
        $modules = ModuleContent::whereIn('id', $build_module_ids)->get();

        foreach ($modules as $key => $value) {
            if ($value->course->public_status == ''
            && $value->course->public_status == 'unpublished')
                continue;

            if (!in_array($value->course->id, $build_courses_ids)) {
                $value->course->image = $value->course->image ? $value->course->image->getPath() : '';
                $courses[$value->course->id] = $value->course;
                $build_courses_ids[] = $value->course->id;
            }

            if (!in_array($value->category_id, $build_category_ids))
                $build_category_ids[] = $value->category_id;
        }

        if (!is_null($topic_id)) {
            $categories = Category::whereIn('id', $build_category_ids)
                            ->whereRaw('parent_id = ? or id = ?', [$topic_id, $topic_id])
                            ->get();
        } else {
            $categories = Category::whereIn('id', $build_category_ids)->get();
        }


        //курсы из сборки
        foreach ($categories as $key => $value) {
//            if (!in_array($value->course_id, $courses_ids)) {
//                $courses_ids[] = $value->course_id;
//            }
            if (!in_array($value->parent_id, $topic_ids)) {
                $topic_ids[] = $value->parent_id;
            }
        }

        //топики из курса
        $topics = Category::whereIn('id', $topic_ids)->get();
        foreach ($topics as $key => $value) {
            $build_category_ids[] = $value->id;
        }

        //картинка курса
        //--фильтруем курсы в сборке
//        foreach ($courses as $key => $value) {
//            if (in_array($value->id, $courses_ids)) {
//                $value->image = $value->image ? $value->image->getPath() : '';
//                $courses[$key] = $value;
//
//                $build_courses_ids[] = $value->id;
//            }
//        }

        return [
            'courses' => $courses,
            'build_category_ids' => $build_category_ids,
            'build_module_ids' => $build_module_ids,
            'build_courses_ids' => $build_courses_ids,
        ];
    }

    public function getCourse() {
        $item = CorseItem::find($this->course_id);

        if (!$item) {
            header('Location: /error');
            exit();
        }

        $topics = [];
        $iact = [];
        $topic_count = 0;
        $modules = [];
        $ipractice_count = 0;

        if ($item) {
            $item->image = $item->image ? $item->image->getPath() : '';

            $build_modules = BuildModuleContent::where('build_id', $this->build_id)->get();

            $modules = ModuleContent::where('course_id', $this->course_id)
                ->where('behavior', '<>', 'practice');
            if (!is_null($this->build_id)) {
                $modules->whereIn('id', $build_modules->lists('module_content_id'));
            }
            $modules = $modules->get();

            $iact = Category::where('course_id', $this->course_id)
                ->whereIn('id', $modules->lists('category_id'))
                ->get();

            $topics = Category::where('course_id', $this->course_id)
                ->whereNull('parent_id')
                ->whereIn('id', $iact->lists('parent_id'))
                ->get();

            $ipractice_count = ModuleContent::where('course_id', $this->course_id)
                 ->where('behavior', 'practice');
            if (!is_null($this->build_id) && count($build_modules)) {
                $ipractice_count = $ipractice_count->whereIn('id', $build_modules->lists('module_content_id'));
            }
            $ipractice_count = $ipractice_count->count();
        }

        return [$item, [
            'topic_count'       => count($topics),
            'iact_count'        => count($iact),
            'ipractice_count'   => $ipractice_count
        ], $topics];
    }

    public function getLesson()
    {
        $lesson_id = Request::input('lesson');

        $lesson = ModuleContent::where('course_id', $this->course_id)
                            ->where('id', $lesson_id)
                            ->first();
        return $lesson;
    }

    public function menuList() 
    {

        $categories_ids = [];
        $return = [];

        $categories = Category::where('course_id', $this->course_id)
                                ->orderBy('sort')
                                ->get();

        foreach ($categories as $cat_value) {
             $categories_ids[] = $cat_value->id;
             $menu_list[$cat_value->id] = (array)$cat_value;
             $menu_list[$cat_value->id]['lessons'] = [];
         }

        $lessons = ModuleContent::where('course_id', $this->course_id)
                            ->whereIn('category_id', $categories_ids)
                            ->orderBy('sort')
                            ->get();

        if (count($lessons)) {
            foreach ($lessons as $les_value) {
                $menu_list[$les_value->category_id]['lessons'][] = (array)$les_value;
            }
        }


        $cat_path = '';
        //topics || groups
        foreach ($menu_list as $cat_key => $cat_value) {
            if (is_null($cat_value['attributes']['parent_id'])) {
                $cat_path = $this->course_item->id . '/' . $cat_value['attributes']['id'];
            }

            $return[] = [
                'type' => !is_null($cat_value['attributes']['parent_id']) ? 'group' : 'topic',
                'number' => $cat_key,
                'resource' => "",
                'id' => $cat_value['attributes']['id'],
                'class' => "",
                'textName' => "",
                'parent' => !is_null($cat_value['attributes']['parent_id']) ? $cat_value['attributes']['parent_id'] : 0,
                'textTitle' => $cat_value['attributes']['name'],
                'section' => "eapp",
                'preview' => ""
            ];

            if (count($cat_value['lessons'])) {
                //modules
                foreach($cat_value['lessons'] as $less_key => $less_value) {

                    $return[] = [
                        'type' => 'item',
                        'number' => $less_key,
                        'resource' => $cat_path . '/' . $less_value['attributes']['id'] . '/index',
                        //'resource' => '',
                        'id' => $less_value['attributes']['id'],
                        'class' => $this->getClass(BehaviorType::where('id', $less_value['attributes']['behavior_type_id'])->first()['code']),
                        'textName' => "",
                        'parent' => $cat_value['attributes']['id'],
                        'textTitle' => $less_value['attributes']['name'],
                        'section' => "eapp",
                        'preview' => ""
                    ];
                }
            }


        }

        return $return;
    }

    public function getClass($type) 
    {
        $classes = [
            'i-check' => 'module practice',
            'i-test'  => 'module control',
            'i-ask'   => 'module theory'
        ];

        return isset($classes[$type]) ? $classes[$type] : '';
    }
}