<?php namespace Igvs\Courses\Components;


use Cms\Classes\ComponentBase;
use Illuminate\Validation\Validator;
use League\Flysystem\Exception;
use Request;
use View;
use App;
use Config;
use Backend\Facades\Backend;

use Igvs\Courses\Models\PilotAuth as PilotAuthModel;
use Igvs\Courses\Models\UserPilot;
use Igvs\Courses\Models\Member;
use Backend\Models\User;

use Faker\Factory;
use BackendAuth;
use Backend\Models\AccessLog;
use Session;
//use October\Rain\Auth;


class PilotAuth extends ComponentBase
{

    public $sekret_key = 'sjvrg8mp9ent96cs5u3b';

    public $data = [];

    public function componentDetails()
    {
        return [
            'name'        => 'PilotAuth',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        BackendAuth::logout();

        $pa = new PilotAuthModel(Request::all());
        if ($pa->checkTrust()) {
            $this->userAuth($pa);
        }
        return Backend::redirectIntended('igvs/courses/');
    }

    private function userAuth(PilotAuthModel $pa)
    {
        $user_info = $pa->getUserInfo();

        if (!$user_info) {
            return false;
        }

        $faker = Factory::create();
        $login = 'pilot_user.' . $pa->getParam('user_id');
        $password = $faker->password(10,20);

        $user_pilot = UserPilot::where('pilot_user_id', $pa->getParam('user_id'))->first();
        if (!$user_pilot) {

            //create user
            $backend_user = new User();

            $user_params = [
                'login'      => $login,
                'first_name' => $user_info->first_name,
                'last_name'  => $user_info->last_name,
                'email'      => "$login@pilotmail.pilot",
                'password'              => $password,
                'password_confirmation' => $password,
                'permissions' => [
                    "igvs.courses.courses" => 1,
                    "igvs.courses.create"  => (int)$user_info->permission_create_course,
                    "igvs.courses.pilot_user"  => 1,
                ]
            ];

            $backend_user->fill($user_params);
            $backend_user->save();

            $user_pilot = new UserPilot();
            $user_pilot->user_id = $backend_user->id;
            $user_pilot->pilot_user_id = $pa->getParam('user_id');
            $user_pilot->save();
        } else {
            $user_id = $user_pilot->user_id;

            $backend_user = User::find($user_id);
            $backend_user->login = $login;
            $backend_user->password = $password;
            $backend_user->password_confirmation = $password;
            $backend_user->permissions = [
                "igvs.courses.courses" => 1,
                "igvs.courses.create"  => (int)$user_info->permission_create_course,
                "igvs.courses.pilot_user"  => 1,
            ];
            $backend_user->save();
        }


        $member_list = Member::where('user_id', $backend_user->id)->get();
        foreach ($member_list as $member) {
            $member->roles()->detach(5);
        }

        foreach ($user_info->courses as $course_id => $permission) {
            if (!$permission) {
                continue;
            }

			// attache role
            $member = Member::firstOrCreate([
                'user_id' => $backend_user->id,
                'course_id' => $course_id,
            ]);
            $member->roles()->attach(5);
        }


        // Authenticate user
        $user = BackendAuth::authenticate([
            'login' => $login,
            'password' => $password
        ], true);

        // Log the sign in event
        AccessLog::add($user);

        return true;
    }
}
