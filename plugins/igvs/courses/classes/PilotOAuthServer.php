<?php

//namespace League\OAuth1\Client\Server;
namespace Igvs\Courses\Classes;

use Academy\Api\Models\OauthLog;
use League\OAuth1\Client\Credentials\TokenCredentials;
use Igvs\Courses\Classes\UserSync;
use Igvs\Courses\Models\UserPilot;
use Academy\Api\Models\Client;
use League\OAuth1\Client\Signature\SignatureInterface;
use Session;
use Config;

class PilotOAuthServer extends \League\OAuth1\Client\Server\Server
{
    protected $cachedLicenciesResponse;
    protected $client_version;
    public $api_client_id;

    public $end_point_action_name = '';
    public $end_point_action_name_default = 'endpoint_export';

    public function __construct($clientCredentials, SignatureInterface $signature = null)
    {
        $this->api_client_id = isset($clientCredentials['api_client_id']) ? $clientCredentials['api_client_id'] : '0';
        $this->client_version = isset($clientCredentials['client_version']) ? $clientCredentials['client_version'] : '1.0';
        $this->end_point_action_name = isset($clientCredentials['end_point_action_name']) ? $clientCredentials['end_point_action_name'] : $this->end_point_action_name_default;

        unset($clientCredentials['api_client_id']);
        unset($clientCredentials['client_version']);
        unset($clientCredentials['end_point_action_name']);

        parent::__construct($clientCredentials, $signature);
    }

    public function getTemporaryCredentials()
    {
        $uri = $this->urlTemporaryCredentials();

        $client = $this->createHttpClient();

        $header = $this->temporaryCredentialsProtocolHeader($uri);
        $authorizationHeader = array('Authorization' => $header);
        $headers = $this->buildHttpClientHeaders($authorizationHeader);

        $request_time_start = time();
        $request_time_finish = time();
        try {
            $response = $client->post($uri, [
                'headers' => $headers,
            ]);
            $request_time_finish = time();
            $statusCode = $response->getStatusCode();

            OauthLog::saveData([
                'income' => 0,
                'type' => 'auth',
                'action' => 'getTemporaryCredentials',
                'client' => $this->api_client_id,
                'headers' => is_array($headers) ? var_export($headers, true) : $headers,
                'data' => implode("\r\n", [
                    "Status code: {$statusCode}\r\nURL: {$uri}\r\n",
                    'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                    'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                    'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
                ]),
                'request_duration' => $request_time_finish - $request_time_start,
                'answer' => (string)$response->getBody(),
            ]);


        } catch (BadResponseException $e) {

            try {

                $response = $e->getResponse();
                $body = $response->getBody();
                $statusCode = $response->getStatusCode();

                OauthLog::saveData([
                    'income' => 0,
                    'is_error' => 1,
                    'type' => 'auth',
                    'action' => 'getTemporaryCredentials (2)',
                    'client' => $this->api_client_id,
                    'headers' => is_array($headers) ? var_export($headers, true) : $headers,
                    'data' => implode("\r\n", [
                        "Status code: {$statusCode}\r\nURL: {$uri}\r\n",
                        'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                        'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                        'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
                    ]),
                    'request_duration' => $request_time_finish - $request_time_start,
                    'answer' => (string)$body,
                ]);

            } catch (\Exception $e) {

                $error_massage = $e->getMessage();

                OauthLog::saveData([
                    'income' => 0,
                    'is_error' => 1,
                    'type' => 'auth',
                    'action' => 'getTemporaryCredentials (3)',
                    'client' => $this->api_client_id,
                    'headers' => is_array($headers) ? var_export($headers, true) : $headers,
                    'data' => implode("\r\n", [
                        "URL: {$uri}\r\n",
                        'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                        'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                        'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
                    ]),
                    'request_duration' => $request_time_finish - $request_time_start,
                    'answer' => "Error message: " . $error_massage,
                ]);

            } finally {
                return $this->handleTemporaryCredentialsBadResponse($e);
            }
        }
        catch (\Exception $e) {
            $error_massage = $e->getMessage();

            OauthLog::saveData([
                'income' => 0,
                'is_error' => 1,
                'type' => 'auth',
                'action' => 'getTemporaryCredentials (4)',
                'client' => $this->api_client_id,
                'headers' => is_array($headers) ? var_export($headers, true) : $headers,
                'data' => implode("\r\n", [
                    "URL: {$uri}\r\n",
                    'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                    'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                    'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
                ]),
                'request_duration' => $request_time_finish - $request_time_start,
                'answer' => "Error message: " . $error_massage,
            ]);
        }

        return isset($response) && $response ? $this->createTemporaryCredentials((string) $response->getBody()) : null;
    }


    public function urlTemporaryCredentials()
    {
        $version = $this->client_version;

        $pilot_url_root = Session::get('igvs.courser.pilot_host');
        $return = "{$pilot_url_root}/oauth/{$version}/request_token.php";

        return $return;
    }

    /**
     * {@inheritDoc}
     */
    public function urlAuthorization()
    {
        $version = $this->client_version;

        $pilot_url_root = Session::get('igvs.courser.pilot_host');
        return "{$pilot_url_root}/oauth/{$version}/authorize.php";
    }

    /**
     * {@inheritDoc}
     */
    public function urlTokenCredentials()
    {
        $version = $this->client_version;

        $pilot_url_root = Session::get('igvs.courser.pilot_host');
        return "{$pilot_url_root}/oauth/{$version}/access_token.php?a=1";
    }

    /**
     * {@inheritDoc}
     */
    public function urlUserDetails()
    {
        $version = $this->client_version;

        $pilot_url_root = Session::get('igvs.courser.pilot_host');
        return "{$pilot_url_root}/oauth/{$version}/user.php";
    }

    /**
     * {@inheritDoc}
     */
    public function urlLicencies()
    {
        $version = $this->client_version;

        $pilot_url_root = Session::get('igvs.courser.pilot_host');
        $return = "{$pilot_url_root}/oauth/{$version}/licencies.php";

        OauthLog::saveData([
            'income' => 0,
            'type' => 'auth',
            'action' => 'urlLicencies',
            'client' => $this->api_client_id,
            'data' => $return,
        ]);

//        file_put_contents(__DIR__ . "/../../../../storage/app/igvs/export_pilot.log", date('Y-m-d H:i:s') . "\r\n" .  print_r([
//                'urlLicencies' => Session::get('igvs.courser.pilot_host')
//            ], true) . "\r\n", FILE_APPEND);
        return $return;
    }

    /**
     * {@inheritDoc}
     */
    public function urlUseLicence()
    {
        $version = $this->client_version;

        $pilot_url_root = Session::get('igvs.courser.pilot_host');
        $return = "{$pilot_url_root}/oauth/{$version}/use_licence.php";

        OauthLog::saveData([
            'income' => 0,
            'type' => 'auth',
            'action' => 'urlUseLicence',
            'client' => $this->api_client_id,
            'data' => "Formed URL: " . $return,
        ]);

//        file_put_contents(__DIR__ . "/../../../../storage/app/igvs/export_pilot.log", date('Y-m-d H:i:s') . "\r\n" . print_r([
//                'urlUseLicence' => Session::get('igvs.courser.pilot_host')
//            ], true) . "\r\n", FILE_APPEND);

        return $return;
    }

    /**
     * {@inheritDoc}
     */
    public function urlEndPoint()
    {
        $version = $this->client_version;

        $pilot_url_root = Session::get('igvs.courser.pilot_host');
        $return = "{$pilot_url_root}/oauth/{$version}/{$this->end_point_action_name}.php";

        OauthLog::saveData([
            'income' => 0,
            'type' => 'auth',
            'action' => 'urlEndPoint',
            'client' => $this->api_client_id,
            'data' => "Formed URL: " . $return,
        ]);

//        file_put_contents(__DIR__ . "/../../../../storage/app/igvs/export_pilot.log", date('Y-m-d H:i:s') . "\r\n" .  print_r([
//                'urlEndPoint' => Session::get('igvs.courser.pilot_host')
//            ], true) . "\r\n", FILE_APPEND);

        return $return;
    }

    /**
     * {@inheritDoc}
     */
    public function urlFetchData()
    {
        $version = $this->client_version;

        $pilot_url_root = Session::get('igvs.courser.pilot_host');
        $return = "{$pilot_url_root}/oauth/{$version}/fetch_data.php";

        OauthLog::saveData([
            'income' => 0,
            'type' => 'auth',
            'action' => 'urlFetchData',
            'client' => $this->api_client_id,
            'data' => "Formed URL: " . $return,
        ]);

        return $return;
    }

    /**
     * {@inheritDoc}
     */
    public function userDetails($data, TokenCredentials $tokenCredentials)
    {
        OauthLog::saveData([
            'income' => 0,
            'type' => 'auth',
            'action' => 'userDetails source_data',
            'client' => $this->api_client_id,
            'data' => print_r($data, true),
        ]);

        $user = new \League\OAuth1\Client\Server\User();

        $user->uid = $data['email'];
        $user->first_name = $data['first_name'];
        if (isset($data['second_name'])) {
            $user->second_name = $data['second_name'];
        }
        $user->last_name = $data['last_name'];
        if (isset($data['work_company'])) {
            $user->work_company = $data['work_company'];
        }
        $user->email = $data['email'];

        if (isset($data['email'])) {
            $user->email = $data['email'];
        }

        $used = array('id', 'screen_name', 'name', 'location', 'description', 'profile_image_url');

        if (is_array($data) && count($data)) {
            foreach ($data as $key => $value) {
                if (strpos($key, 'url') !== false) {
                    if (!in_array($key, $used)) {
                        $used[] = $key;
                    }

                    $user->urls[$key] = $value;
                }
            }

            // Save all extra data
            $user->extra = array_diff_key($data, array_flip($used));
        } else {
            // Save all extra data
            $user->extra = array_flip($used);
        }

        return $user;
    }

    public function getLicencies($force = true, $course_id)
    {
        $tokenCredentials = unserialize(\Session::get('oauth_token'));

        UserSync::log("getLicencies start . \r\n" . print_r([], true) . "\r\n", $this, 'details');

        if (!$this->cachedLicenciesResponse || $force) {
            $url = $this->urlLicencies();

            $client = $this->createHttpClient();

            $headers = array_merge($this->getHeaders($tokenCredentials, 'GET', $url), [
                'Cms-course-id' => $course_id,
            ]);

            UserSync::log("getLicencies send . \r\n" . print_r(['$headers' => $headers], true) . "\r\n", $this, 'details');

            $request_time_start = time();
            $request_time_finish = time();
            try {

                $response = $client->get($url, [
                    'headers' => $headers,
                ]);
                $request_time_finish = time();
            } catch (BadResponseException $e) {
                $response = $e->getResponse();
                $body = $response->getBody();
                $statusCode = $response->getStatusCode();

                UserSync::log("getLicencies error. \r\n" . print_r([
                    'statusCode' => $statusCode,
                    'bod' => $body,
                        'Старт запроса' => date('Y-m-d H:i:s', $request_time_start),
                        'Окончание запроса' => date('Y-m-d H:i:s', $request_time_finish),
                        'Длительность запроса в секундах' => $request_time_finish - $request_time_start,
                    ], true) . "\r\n", $this, 'details');

                throw new \Exception(
                    "Received error [$body] with status code [$statusCode] when retrieving token credentials."
                );
            }
            switch ($this->responseType) {
                case 'json':
                    $this->cachedLicenciesResponse = json_decode((string) $response->getBody(), true);
                    break;

                case 'xml':
                    $this->cachedLicenciesResponse = simplexml_load_string((string) $response->getBody());
                    break;

                case 'string':
                    parse_str((string) $response->getBody(), $this->cachedLicenciesResponse);
                    break;

                default:
                    throw new \InvalidArgumentException("Invalid response type [{$this->responseType}].");
            }

            UserSync::log("getLicencies success. \r\n" . print_r([
                'response_body' => (string)$response->getBody(),
                'Старт запроса' => date('Y-m-d H:i:s', $request_time_start),
                'Окончание запроса' => date('Y-m-d H:i:s', $request_time_finish),
                'Длительность запроса в секундах' => $request_time_finish - $request_time_start,
                    ], true) . "\r\n", $this, 'details');
        }

        return $this->cachedLicenciesResponse;
    }

    public function useLicence($course_id, $pilot_course_id, $external_course_id)
    {
        $tokenCredentials = unserialize(\Session::get('oauth_token'));
        $user = unserialize(\Session::get('oauth_user'));

        $return = null;
        $response = null;

        $url = $this->urlUseLicence();
        $client = $this->createHttpClient();

        $headers = array_merge($this->getHeaders($tokenCredentials, 'GET', $url), [
            'User-id' => $user->extra['uid'],
            'Course-id' => $course_id,
            'Pilot-course-id' => $pilot_course_id,
            'External-course-id' => $external_course_id,
        ]);

        $request_time_start = time();
        $request_time_finish = time();
        try {
            $response = $client->get($url, [
                'headers' => $headers,
            ]);

            $request_time_finish = time();

            OauthLog::saveData([
                'income' => 0,
                'type' => 'auth',
                'action' => 'useLicence',
                'client' => $this->api_client_id,
                'headers' => is_array($headers) ? var_export($headers, true) : $headers,
                'data' => implode("\r\n", [
                    "Status code: {$response->getStatusCode()}\r\nURL: {$url}",
                    'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                    'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                    'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
                ]),
                'request_duration' => $request_time_finish - $request_time_start,
                'answer' => print_r((string)$response->getBody(), true),
            ]);

        } catch (BadResponseException $e) {
            $response = $e->getResponse();
            $body = $response->getBody();
            $statusCode = $response->getStatusCode();

            OauthLog::saveData([
                'income' => 0,
                'is_error' => 1,
                'type' => 'auth',
                'action' => 'useLicence (2)',
                'client' => $this->api_client_id,
                'headers' => is_array($headers) ? var_export($headers, true) : $headers,
                'data' => implode("\r\n", [
                    "Status code: {$response->getStatusCode()}\r\nURL: {$url}",
                    'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                    'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                    'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
                ]),
                'request_duration' => $request_time_finish - $request_time_start,
                'answer' => "Body: " . print_r((string)$body, true),
            ]);

            throw new \Exception(
                "Received error [$body] with status code [$statusCode] when retrieving token credentials."
            );

        } catch (\Exception $e) {

            OauthLog::saveData([
                'income' => 0,
                'is_error' => 1,
                'type' => 'auth',
                'action' => 'useLicence (3)',
                'client' => $this->api_client_id,
                'headers' => is_array($headers) ? var_export($headers, true) : $headers,
                'data' => implode("\r\n", [
                    "URL: {$url}",
                    'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                    'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                    'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
                ]),
                'request_duration' => $request_time_finish - $request_time_start,
                'answer' => "Error message: {$e->getMessage()}",
            ]);
        }

        $return = $response ? (string)$response->getBody() : '';

        return $return;
    }

    private function getClient($pilot_domain)
    {
        $result = Client::where('domain', $pilot_domain)->first();

        return $result;
    }

    public function endPoint($options)
    {
        $course_id          = isset($options['course_id']) ? $options['course_id'] : null;
        $pilot_course_id    = isset($options['pilot_course_id']) ? $options['pilot_course_id'] : null;
        $parent_course_id   = isset($options['parent_course_id']) ? $options['parent_course_id'] : null;
        $export_dir         = isset($options['export_dir']) ? $options['export_dir'] : null;
        $tokenCredentials   = isset($options['token_credentials']) ? $options['token_credentials'] : null;
        $oauth_user_uid     = isset($options['oauth_user_uid']) ? $options['oauth_user_uid'] : null;
        $oauth_version      = isset($options['oauth_version']) ? $options['oauth_version'] : null;
        $poo_id             = isset($options['poo_id']) ? $options['poo_id'] : null;
        $url                = isset($options['url']) ? $options['url'] : null;
        $file_md5           = isset($options['file_md5']) ? $options['file_md5'] : null;
        $all_options        = isset($options['options']) ? $options['options'] : null;
        $system             = isset($all_options['system']) ? $all_options['system'] : '';
        $competence         = isset($all_options['competence']) ? $all_options['competence'] : '';
        $domain             = isset($all_options['remote_server_domain_without_protocol']) ? $all_options['remote_server_domain_without_protocol'] : null;

        $end_point_action_name = $this->end_point_action_name;
        $api_client_id = $this->api_client_id;

        if (!is_null($domain)) {
            $client = $this->getClient($domain);

            if ($client) {
                $api_client_id = $client->id;

                if ($this->end_point_action_name_default == 'inclsv_export') {
                    $end_point_action_name = $client->endpoint_method_inclusive;
                } else {
                    $end_point_action_name = $client->endpoint_method;
                }
            }
        }

        $return = null;

        $version = is_null($oauth_version) ? $this->client_version : $oauth_version;

        $url = !strlen($url) ? $this->urlEndPoint() : $url . "/oauth/{$version}/{$end_point_action_name}.php";
        $client = $this->createHttpClient();

        $expertises = [];
        $course = \Igvs\Courses\Models\Course::find($course_id);
        if ($course) {
            $expertises = $course->expertise->lists('code');
        }

        $headers = array_merge($this->getHeaders($tokenCredentials, 'GET', $url), [
            'Expertises' => implode(',', $expertises),
            'System' => $system,
            'Comps' => $competence,

            'Poo-id' => $poo_id,
            'User-id' => $oauth_user_uid,
            'Course-id' => $course_id,
            'Pilot-course-id' => $pilot_course_id,
            'Parent-course-id' => $parent_course_id ? $parent_course_id : 'new',
            'Export-path' => $export_dir,
            'Check-sum' => $file_md5,
        ]);

        $request_time_start = time();
        $request_time_finish = time();
        try {
            $response = $client->get($url, [
                'headers' => $headers,
            ]);

            $request_time_finish = time();

            OauthLog::saveData([
                'income' => 0,
                'is_error' => 0,
                'type' => 'auth',
                'action' => 'endPoint',
                'client' => $api_client_id,
                'headers' => is_array($headers) ? var_export($headers, true) : $headers,
                'data' => implode("\r\n", [
                    "Status code: {$response->getStatusCode()}\r\nURL: {$url}",
                    'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                    'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                    'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
                ]),
                'request_duration' => $request_time_finish - $request_time_start,
                'answer' => print_r((string)$response->getBody(), true),
            ]);

        } catch (BadResponseException $e) {
            $response = $e->getResponse();
            $body = (string)$response->getBody();
            $statusCode = $response->getStatusCode();

            OauthLog::saveData([
                'income' => 0,
                'is_error' => 1,
                'type' => 'auth',
                'action' => 'endPoint',
                'client' => $this->api_client_id,
                'headers' => is_array($headers) ? var_export($headers, true) : $headers,
                'data' => implode("\r\n", [
                    "Status code: {$response->getStatusCode()}\r\nURL: {$url}",
                    'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                    'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                    'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
                ]),
                'request_duration' => $request_time_finish - $request_time_start,
                'answer' => print_r($body, true),
            ]);

            throw new \Exception(
                "Received error [$body] with status code [$statusCode] when retrieving token credentials."
            );
        }
        switch ($this->responseType) {
            case 'json':
                $return = json_decode((string) $response->getBody(), true);
                break;

            case 'xml':
                $return = simplexml_load_string((string) $response->getBody());
                break;

            case 'string':
                parse_str((string) $response->getBody(), $return);
                break;

            default:
                throw new \InvalidArgumentException("Invalid response type [{$this->responseType}].");
        }


        return $return;
    }

    public function systemsAndComps($url)
    {
        return $this->getFetchData($url, 'systemsandcomps');
    }

    public function structureCourse($url, $params)
    {
        return $this->getFetchData($url, 'structure', $params);
    }

    public function getFetchData($url, $method = null, $params = [])
    {
        $return = null;

        $user = \BackendAuth::getUser();
        if (!$user)
            return false;

        if (is_null($method) || !strlen($method))
            throw new \Exception('$method must be not null');

        $user_pilot = UserPilot::where('user_id', $user->id)->first();
        if (!$user_pilot)
            return false;

        $tokenCredentials = unserialize(\Session::get('oauth_token'));

        $version = $this->client_version;

        $url = !strlen($url) ? $this->urlFetchData() : $url . "/oauth/{$version}/fetch_data.php";
        $client = $this->createHttpClient();

        $headers = array_merge($this->getHeaders($tokenCredentials, 'GET', $url),
            [
                'Method'     => $method,
                'Action'     => $method, // договорились продублировать method тут
                'Poo-id'     => $user_pilot->poo_id,
                'User-id'    => $user_pilot->pilot_user_id,
                'Cluster-id' => $user_pilot->cluster_id,
            ],
            is_array($params) && count($params) ? $params : []
        );

        $request_time_start = time();
        $request_time_finish = time();
        try {
            $response = $client->get($url, [
                'headers' => $headers,
            ]);

            $request_time_finish = time();

            OauthLog::saveData([
                'income' => 0,
                'is_error' => 0,
                'type' => 'auth',
                'action' => $method,
                'client' => $this->api_client_id,
                'headers' => is_array($headers) ? var_export($headers, true) : $headers,
                'data' => implode("\r\n", [
                    "Status code: {$response->getStatusCode()}\r\nURL: {$url}",
                    'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                    'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                    'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
                ]),
                'request_duration' => $request_time_finish - $request_time_start,
                'answer' => print_r((string)$response->getBody(), true),
            ]);

        } catch (BadResponseException $e) {
            $response = $e->getResponse();
            $body = $response->getBody();
            $statusCode = $response->getStatusCode();

            $request_time_finish = time();

            OauthLog::saveData([
                'income' => 0,
                'is_error' => 1,
                'type' => 'auth',
                'action' => $method . ' (2)',
                'client' => $this->api_client_id,
                'headers' => is_array($headers) ? var_export($headers, true) : $headers,
                'data' => implode("\r\n", [
                    "Status code: {$response->getStatusCode()}\r\nURL: {$url}",
                    'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                    'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                    'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
                ]),
                'request_duration' => $request_time_finish - $request_time_start,
                'answer' => print_r((string)$body, true),
            ]);

            throw new \Exception(
                "Received error [$body] with status code [$statusCode] when retrieving token credentials."
            );
        } catch(\Exception $e) {

            $request_time_finish = time();

            OauthLog::saveData([
                'income' => 0,
                'is_error' => 1,
                'type' => 'auth',
                'action' => $method . ' (3)',
                'client' => $this->api_client_id,
                'headers' => is_array($headers) ? var_export($headers, true) : $headers,
                'data' => implode("\r\n", [
                    "URL: {$url}",
                    'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                    'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                    'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
                ]),
                'request_duration' => $request_time_finish - $request_time_start,
                'answer' => print_r((string)$e->getMessage(), true),
            ]);

            throw new \Exception(
                "Error {$e->getMessage()}"
            );
        }

        switch ($this->responseType) {
            case 'json':
                $return = json_decode((string) $response->getBody(), true);
                break;

            case 'xml':
                $return = simplexml_load_string((string) $response->getBody());
                break;

            case 'string':
                parse_str((string) $response->getBody(), $return);
                break;

            default:
                throw new \InvalidArgumentException("Invalid response type [{$this->responseType}].");
        }


        return $return;
    }

    /**
     * {@inheritDoc}
     */
    public function userUid($data, TokenCredentials $tokenCredentials)
    {
        return $data['id'];
    }

    /**
     * {@inheritDoc}
     */
    public function userEmail($data, TokenCredentials $tokenCredentials)
    {
        return;
    }

    /**
     * {@inheritDoc}
     */
    public function userScreenName($data, TokenCredentials $tokenCredentials)
    {
        return $data['name'];
    }
}
