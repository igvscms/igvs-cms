<?php namespace Igvs\Courses\Classes;


class ExportHelper
{
    //список компетенций
    static public function getSystemsAndComps()
    {
        return \Socialite::with('pilotoauth')->systemsAndComps();
    }

    //структуру курса
    static function getStructureCourse($course_id) {
        return \Socialite::with('pilotoauth')->structureCourse(null, [
            'cms_course_id' => $course_id,
            'cms-course-id' => $course_id,
        ]);
    }

    static function getCompetences($syscomp)
    {
        $competences_array = isset($syscomp['comps']) ? $syscomp['comps'] : [];
        $competences = [];

        foreach ($competences_array as $sys_key => $comps) {
            $competences[$sys_key] = [];
            foreach ($comps as $comp_item)
                $competences[$sys_key][$comp_item['ID']] = $comp_item['NAME'];
        }

        return $competences;
    }

    static function getSystems($syscomp)
    {
        return isset($syscomp['systems']) ? $syscomp['systems'] : [];
    }

    static function getStructureModules($structure)
    {
        if ($structure && is_array($structure)) {
            return $structure;
        }

        return [];
    }
}
