<?php namespace Igvs\Courses\Classes;

use Igvs\Courses\Models\BuildModuleContent;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\BackgroundTask;
use Igvs\Courses\Models\PublicationState as PublicationStateModel;
use Igvs\Courses\Traits\CourseHelper;

class ExportStructureDiffHelper
{
    use CourseHelper;

    /**
     * Возвращает совмещённые данные структуры курса (новую и старую) в ввиде массива
     * с информацией об разнице между входящей структуры и текущей
     * @param $structure_modules array Массив последней выгруженной структуры
     * @param $course_id int Индетификатор курса
     * @return array Структура курса с изменениями
     */
    static public function getStructure($licences, $course_id, $build_id = null)
    {
        $build_id = (int)$build_id;
        if (!$build_id) {
            $build_id = null;
        }

        $backend_user = \BackendAuth::getUser();

        $build_module_ids = self::getBuildModuleContentIds($course_id, $build_id);

        $course_modules = ModuleContent::select('id', 'name', 'code', 'behavior', 'switch_practice_mode', 'practice_title_manage', 'practice_index_title', 'practice_assessment_title', 'sort', 'category_id', 'status_id', 'updated_at')
            ->where(function($q) use($course_id, $build_module_ids) {
                $q->where('course_id', $course_id);

                if (!is_null($build_module_ids)) {
                    $q->whereIn('id', $build_module_ids);
                }
            })
            ->get();

        if (!count($course_modules)) {
            return [];
        }

        $course_categories = Category::select('id', 'name', 'code', 'sort', 'parent_id', 'updated_at')
            ->where('course_id', $course_id)
            ->get();

        $background_task_last_time = BackgroundTask::select('created_at')
            ->where('act', 'exportToPilot')
            ->whereRaw("options like \"%\\\"courseId\\\":\\\"{$course_id}\\\"%\"")
            ->where('responsible_id', $backend_user->id)
            ->orderBy('created_at', 'desc')
            ->first();

        $background_task_last_time = $background_task_last_time ? strtotime($background_task_last_time->created_at) : time();

        // прошлая структура
        $structure_modules = self::getStructureModules($licences);

        // текущая структура
        $structure = self::setDatabaseData($course_categories, $course_modules, $background_task_last_time);

        // сравнение с прошлой структурой
        $structure = self::setStructureData($structure, $structure_modules);

        return self::structureSortInit($structure);
    }

    static private function getStructureModules($licences)
    {
        $structure_modules = [];

        if (isset($licences['structure']) && is_array($licences['structure'])) {
            $structure_modules = $licences['structure'];
        }

        return $structure_modules;
    }

    static public function  getBuildModuleContentIds($course_id, $build_id)
    {
        $module_content_ids = ModuleContent::where('course_id', $course_id)->lists('id');

        if (is_null($build_id)) {
            return $module_content_ids;
        }

        return BuildModuleContent::where('build_id', $build_id)
            ->whereIn('module_content_id', $module_content_ids)
            ->lists('module_content_id');
    }

    /**
     * Перебирает текущую структуру
     * @param $course_categories array Массив категорий
     * @param $course_modules array Массив модулей контента
     * @param $background_task_last_time int Время последней выгрузки данного курса текущим пользователем
     * @return array Массив, содержащий структуру курса
     */
    static function setDatabaseData($course_categories, $course_modules, $background_task_last_time)
    {
        $structure = [];

        foreach ($course_categories as $topic) {
            if ($topic->parent_id)
                continue;

            if (!isset($structure[$topic->id]))
                $structure[$topic->id] = [
                    'name' => $topic->name,
                    'order' => (int)$topic->sort,
                    'code' => $topic->code,
                    'updated_at' => $topic->updated_at,
                    'depth' => 0,
                    'childs' => [],
                    'action' => 'new',
                    'name_old' => '',
                    'code_old' => '',
                    'order_old' => '',
                ];

            foreach ($course_categories as $act) {
                if (!$act->parent_id || $act->parent_id != $topic->id)
                    continue;

                if (!isset($structure[$topic->id]['childs'][$act->id]))
                    $structure[$topic->id]['childs'][$act->id] = [
                        'name' => $act->name,
                        'order' => (int)$act->sort,
                        'code' => $act->code,
                        'updated_at' => $act->updated_at,
                        'depth' => 1,
                        'childs' => [],
                        'action' => 'new',
                        'name_old' => '',
                        'code_old' => '',
                        'order_old' => '',
                    ];

                foreach ($course_modules as $module) {
                    if ($module->status && $module->status->block_export)
                        continue;

                    if ($module->category_id == $act->id) {
                        self::addModuleInStructure($structure[$topic->id]['childs'][$act->id]['childs'], $module, $background_task_last_time);
                    }
                }
            }
        }

        return self::cleanUpEmptyTrees($structure);
    }

    /**
     * @param $structure array структура на уровне 'childs' ($structure[$topic->id]['childs'][$act->id]['childs'])
     * @param $module object Объект Модуля контента
     * @param $background_task_last_time string Время последней публикации
     */
    private static function addModuleInStructure(&$structure, $module, $background_task_last_time)
    {
        $usual_attributes = [
            'name' => self::getMenuItemName($module->name),
            'order' => $module->sort,
            'code' => $module->code,
            'updated_at' => strtotime($module->updated_at),
            'background_task_last_time' => $background_task_last_time,
            'depth' => 2,
            'action' => 'new',
            'name_old' => '',
            'code_old' => '',
            'order_old' => '',
            'module' => (array)$module,
        ];

        $practice_index_attributes =
            $practice_assessment_attributes =
                $usual_attributes;

        $practice_index_attributes['name'] = self::getPracticeIndexName($module);
        $practice_assessment_attributes['name'] = self::getPracticeAssessmentName($module);
        $practice_assessment_attributes['order'] += 0.5;

        if ($module->behavior != 'practice') {
            $structure[$module->id] = $usual_attributes;
            return;
        }

        $id_assessment = self::modifyAssessmentCode($module->id);

        switch ($module->switch_practice_mode) {
            case 1:
                $structure[$module->id] = $practice_index_attributes;
                break;
            case 2:
                $structure[$id_assessment] = $practice_assessment_attributes;
                break;
            default:
                $structure[$module->id] = $practice_index_attributes;
                $structure[$id_assessment] = $practice_assessment_attributes;
        }
    }

    private static function cleanUpEmptyTrees($structure)
    {
        // удаляем акты, если в них нет модулей
        foreach ($structure as &$topic) {
            foreach($topic['childs'] as $act_id => $act) {
                if(isset($act['childs']) && count($act['childs'])) {
                    continue;
                }

                unset($topic['childs'][$act_id]);
            }
        }

        // удаляем топики, если в них нет актов
        foreach ($structure as $topic_id => &$topic) {
            if(isset($topic['childs']) && count($topic['childs'])) {
                continue;
            }

            unset($structure[$topic_id]);
        }

        return $structure;
    }

    /**
     * Перебирает структуру, полученную из Elearning
     * @param $structure array Текущая страктура
     * @param $structure_modules array Структура из Elearning
     * @return array Массив, содержащий структуру курса
     */
    static function setStructureData($structure, $structure_modules)
    {
        if (is_array($structure_modules)) {
            foreach ($structure_modules as $structure_module) {
                $structure_module = (array)$structure_module;

                $id = isset($structure_module['external_id']) ? (string)$structure_module['external_id'] : 0;
                $parent_id = isset($structure_modules[$structure_module['parent']]) ? (int)($structure_modules[$structure_module['parent']]['external_id']) : 0;

                switch ($structure_module['class']) {
                    case 'separator':
                        if (!isset($structure[$id])) {
                            $structure[$id] = [
                                'name' => '',
                                'order' => '',
                                'code' => '',
                                'updated_at' => '',
                                'depth' => 0,
                                'childs' => [],
                                'action' => 'delete',
                            ];
                        }

                        $structure[$id]['name_old'] = $structure_module['name'];
                        $structure[$id]['code_old'] = $structure_module['code'];
                        $structure[$id]['order_old'] = (int)$structure_module['order'];

                        self::structureCalcAction($structure[$id]);
                        break;
                    case 'group':
                        if (!isset($structure_modules[(string)$structure_module['parent']]))
                            continue;

                        if (!isset($structure[$parent_id]['childs'][$id])) {
                            $structure[$parent_id]['childs'][$id] = [
                                'name' => '',
                                'order' => '',
                                'code' => '',
                                'updated_at' => '',
                                'depth' => 1,
                                'childs' => [],
                                'action' => 'delete',
                            ];
                        }

                        $structure[$parent_id]['childs'][$id]['name_old'] = $structure_module['name'];
                        $structure[$parent_id]['childs'][$id]['code_old'] = $structure_module['code'];
                        $structure[$parent_id]['childs'][$id]['order_old'] = (int)$structure_module['order'];

                        self::structureCalcAction($structure[$parent_id]['childs'][$id]);
                        break;
                    case 'item':
                        if (!isset($structure_modules[(string)$structure_module['parent']])
                            || !isset($structure_modules[(string)$structure_module['parent']]['parent']))
                            continue;

                        $structure_topic_id = (int)$structure_modules[(string)$structure_modules[(string)$structure_module['parent']]['parent']]['external_id'];

                        if (!isset($structure[$structure_topic_id]['childs'][$parent_id]['childs'][$id])) {
                            $structure[$structure_topic_id]['childs'][$parent_id]['childs'][$id] = [
                                'name' => '',
                                'order' => '',
                                'code' => '',
                                'updated_at' => '',
                                'depth' => 2,
                                'action' => 'delete',
                            ];
                        }

                        $code_old = $structure_module['code'];
                        preg_match("/^.+\/([^\/]+)$/", $code_old, $matches);

                        if (is_array($matches) && count($matches)) {
                            $code_old = $matches[1];
                        }

                        $structure[$structure_topic_id]['childs'][$parent_id]['childs'][$id]['name_old'] = $structure_module['name'];
                        $structure[$structure_topic_id]['childs'][$parent_id]['childs'][$id]['code_old'] = $code_old;
                        $structure[$structure_topic_id]['childs'][$parent_id]['childs'][$id]['order_old'] = (int)$structure_module['order'];

                        self::structureCalcAction($structure[$structure_topic_id]['childs'][$parent_id]['childs'][$id]);
                        break;
                }
            }
        }

        return $structure;
    }

    /**
     * Сортирует структуру по полю order, в том числе дочерние элементы
     * @param $structure array Структура
     * @return array Отсортированная структура
     */
    static function structureSortInit($structure)
    {
        $return = [];

        self::structureSort($structure);

        foreach ($structure as &$item_topic) {
            $item = $item_topic;
            unset($item['childs']);
            $return[] = $item;

            self::structureSort($item_topic['childs']);

            foreach ($item_topic['childs'] as &$item_act) {
                $item = $item_act;
                unset($item['childs']);
                $return[] = $item;

                self::structureSort($item_act['childs']);

                foreach ($item_act['childs'] as &$item_module) {
                    $return[] = $item_module;
                }
            }
        }

        return $return;
    }

    /**
     * Сортирует массив по полю order
     * @param $array array Массив
     */
    static function structureSort(&$array)
    {
        uasort($array, function($a, $b){
            $a_sort = (float)$a['order'];
            $b_sort = (float)$b['order'];

            if ($a_sort > $b_sort)
                return 1;
            else if ($a_sort < $b_sort)
                return -1;
            else
                return 0;
        });
    }

    /**
     * Анализатор изменений элементов структуры. Выставляет флаги изменений в поле action.
     * @param $item array Элемент структуры
     */
    static function structureCalcAction(&$item)
    {
        if ($item['action'] == 'delete')
            return;

        $actions = [];

        // проверка имени
        if (strlen($item['name_old']) && $item['name'] != $item['name_old']) {
            $actions[] = 'changed_name';
        } else if (strlen($item['name_old']) && $item['name'] == $item['name_old']) {
            $actions[] = 'no_change_name';
        }

        // проверка кода
        if (strlen($item['code_old']) && $item['code'] != $item['code_old']) {
            $actions[] = 'changed_code';
        } elseif (strlen($item['code_old']) && $item['code'] == $item['code_old']) {
            $actions[] = 'no_change_code';
        }

        // проверка порядка (сортировки)
        if (strlen($item['order_old']) && (int)$item['order'] != (int)$item['order_old']) {
            $actions[] = 'changed_order';
        } elseif (strlen($item['order_old']) && (int)$item['order'] == (int)$item['order_old']) {
            $actions[] = 'no_change_order';
        }

        // проверка изменения контента
        if (isset($item['background_task_last_time'])
            && strlen($item['updated_at'])
            && $item['updated_at'] > $item['background_task_last_time']) {
            $actions[] = 'changed_content';
        } else {
            $actions[] = 'no_change_content';
        }

        $item['action'] = implode('|', $actions);
    }

    static public function getPreviousStructure($course_id)
    {
        $course = PublicationStateModel::select('hash')
                                        ->where('course_id', $course_id)
                                        ->where('row_type', 'course')
                                        ->orderBy('created_at', 'desc')
                                        ->first();

        if (!$course) {
            return [];
        }

        $all_rows = PublicationStateModel::where('hash', $course->hash)
                                            ->where('row_type', '!=', 'course')
                                            ->get();

        $return = [];

        foreach ($all_rows as $row) {
            if (self::isSkipPreviousItem($row)) {
                continue;
            }

            $return[self::getPreviousItemExternalId($row)] = [
                'class'       => self::getPreviousItemClass($row),
                'external_id' => self::getPreviousItemExternalId($row),
                'parent'      => self::getPreviousItemParentId($row),
                'name'        => self::getPreviousItemName($row),
                'code'        => self::getPreviousItemCode($row),
                'order'       => $row->order,
            ];
        }

        return ['structure' => $return];
    }

    static private function isSkipPreviousItem($row)
    {
        return !in_array($row->row_type, ['topic', 'act', 'module_content']);
    }

    static private function getPreviousItemClass($row)
    {
        switch ($row->row_type) {
            case 'topic':
                return 'separator';
            case 'act':
                return 'group';
            case 'module_content':
                return 'item';
        }

        return null;
    }

    static private function getPreviousItemExternalId($row)
    {
        switch ($row->row_type) {
            case 'topic':
                return (string)$row->topic_id;
            case 'act':
                return (string)$row->act_id;
            case 'module_content':
                return (string)$row->module_content_id;
        }

        return null;
    }

    static private function getPreviousItemParentId($row)
    {
        switch ($row->row_type) {
            case 'topic':
                return null;
            case 'act':
                return (string)$row->topic_id;
            case 'module_content':
                return (string)$row->act_id;
        }

        return null;
    }

    static private function getPreviousItemName($row)
    {
        switch ($row->row_type) {
            case 'topic':
                return $row->topic_name;
            case 'act':
                return $row->act_name;
            case 'module_content':
                return $row->module_content_name;
        }

        return null;
    }

    static private function getPreviousItemCode($row)
    {
        switch ($row->row_type) {
            case 'topic':
                return $row->topic_code;
            case 'module_content':
                return $row->module_content_code;
        }

        return null;
    }
}
