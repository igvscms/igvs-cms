<?php namespace Igvs\Courses\Classes;

use Illuminate\Routing\Controller as ControllerBase;
use Faker\Factory;
use Igvs\Courses\Models\UserPilot;
use Backend\Models\UserGroup;
use Backend\Models\User;
use Academy\Api\Models\Client;
use Igvs\Courses\Classes\UserSync;

class Wsdl extends ControllerBase {

    /**
     * @soap
     *
     * @param array $data Массив пользователей, instance id и server name
     * @return array количество созданных и обновлённых пользователей
     */
    public function updateUsers($data)
    {
        try {
            $is_compare_domain_as_auth = \Config::get('cms.soapEnableCompareDomainNameVsAuth', 1);

            // логируем входящие данные
            UserSync::log("data\r\n" . print_r($data, true) . "\r\n", $this,'all');
            UserSync::log("php://input\r\n" . print_r(implode(" ", file("php://input")), true) . "\r\n", $this,'all');

            // определяем client_id и домен
            $client = \Session::get('academy.api.client_id', 0);
//            $pilot_domain = \Session::get('academy.api.client_id', 'undefined');

            UserSync::log("\r\nauth api.client is : " . print_r($client, true) . "\r\n", $this,'all');

            // если client_id не определён: логируем и возвращаем ошибку
            if (!$client) {
                UserSync::log("\r\n session academy.api.client is false|null: " . print_r($client, true) . "\r\n", $this,'all');

                return [
                    'errors' => [
                        [
                            'text' => 'Error client ID. Contact please with IGVS developers.',
                            'code' => 10001
                        ]
                    ]
                ];
            }

            // проверяем, что пришедшие данные - массив, если нет - логируем и возвращаем ошибку
            if (!is_array($data)) {
                UserSync::log("Finish. Error \r\n" . 'You must give array with USERS and INSTANCE_ID and SERVER_NAME!' . "\r\n", $this,'all');
                return $this->generateErrorArray('You must give array with USERS and INSTANCE_ID and SERVER_NAME!', 10003);
            }

            // проверяем, что в пришедших данных есть USERS, INSTANCE_ID и SERVER_NAME, если нет - логируем и возвращаем ошибку
            if (!isset($data['USERS']) || !isset($data['INSTANCE_ID']) || !isset($data['SERVER_NAME'])) {
                UserSync::log("Finish. Error \r\n" . 'Your data missing USERS array or INSTANCE_ID or SERVER_NAME string!' . "\r\n", $this,'all');
                return $this->generateErrorArray('Your data missing USERS array or INSTANCE_ID or SERVER_NAME string!', 10002);
            }

            // находим-с клиент в списке клиентов, пришедшый (его мы узнали на этапе авторизации по логину и паролю)
            $client_instance_from_basic_auth = Client::where('id', $client)
                ->first();

            // находим-с клиент в списке клиентов по наименованию сервера, пришедшего в данных
            $client_instance = Client::where('domain', $data['SERVER_NAME'])
                ->where('app_id', $data['INSTANCE_ID'])
                ->first();

            // если не нашли один из клиентов - логируем и возвращаем ошибку
            if (!$client_instance || !$client_instance_from_basic_auth) {
                UserSync::log("\r\n" . 'Error client ID. Contact please with IGVS developers. 10006' . "\r\n", $this,'all');

                return [
                    'errors' => [
                        [
                            'text' => 'Error client ID. Contact please with IGVS developers.',
                            'code' => 10006
                        ]
                    ]
                ];
            }

            // если авторизовались под учётными данными другого интанса - возвращаем ошибку
            if ($client_instance != $client_instance_from_basic_auth
                && $is_compare_domain_as_auth) {
                UserSync::log("\r\n" . 'Error client ID. Contact please with IGVS developers. 10007' . "\r\n", $this,'all');

                return [
                    'errors' => [
                        [
                            'text' => 'Error client ID. Contact please with IGVS developers.',
                            'code' => 10007
                        ]
                    ]
                ];
            }

            // берём id
            $client_instance_id = $client_instance->id;

            //todo убрать эту карягу отсюда!!
            //$client_instance_id = 4;

            $user_update = new UserSync();

            // удаляет из обязательных параметров "courses", "lang"
            $user_update->initSoapUpdate();
            // подменяем группу, если не основной пилот
            $user_update->updateOperatorGroup($client_instance_id);


            // создаём/обновляем пользователей
            if (count($data['USERS'])) {
                foreach ($data['USERS'] as $user_data) {
                    UserSync::log("check user.", $this,'all');
                    // проверяем, что в данных пользователя есть все заявленные поля
                    if ($user_update->checkUserData($user_data, $this)) {
                        UserSync::log("check user. ok", $this,'all');
                        $user_update->saveUpdateUser($user_data, $client_instance_id, $this, true);
                    } else {
                        UserSync::log("check user. false", $this,'all');
                    }
                }
            }

            // данные для ответа
            $return = [
                'created' => $user_update->getCountCreated(),
                'updated' => $user_update->getCountUpdated(),
                'all_changed' => $user_update->getCountCreated() + $user_update->getCountUpdated(),
                'input_users_count' => count($data['USERS']),
                'errors' => $user_update->getCountErrors()
            ];

            // логируем данные, которые собираемся вернуть
            UserSync::log("Finish. \r\n" . print_r($return, true) . "\r\n", $this,'all');

            return $return;

        } catch (\Exception $e) {

            // если что-то не так - логируем
            UserSync::log("unexpected error 10007 \r\n" . print_r($e->getMessage(), true) . "\r\n", $this,'errors');

            return $this->generateErrorArray('unexpected error!', 10007);

        }
    }

    private function generateErrorArray($error_text, $code = 0)
    {
        return [
            'error' => ['text' => $error_text, 'code' => $code],
            'input_data' => file_get_contents('php://input'),
        ];
    }

}
