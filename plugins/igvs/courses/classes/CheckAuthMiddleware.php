<?php namespace Igvs\Courses\Classes;

use Closure;
use BackendAuth;
use Session;
use Cookie;

class CheckAuthMiddleware
{
    public function handle($request, Closure $next)
    {
        $domain = Cookie::get('pilot_domain');
        $user = BackendAuth::getUser();

        Cookie::queue('pilot_domain', '', 0);

        if (!$user && $domain) {
            return redirect('/session-lost?domain='.$domain);
        }

        return $next($request);
    }
}