<?php namespace Igvs\Courses\Classes;


use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\PublicationState as State;
use Igvs\Courses\Traits\CourseHelper;

class PublicationState
{
    use CourseHelper;

    private $publication_state_models = null;

    private $course_id                = null;
    private $course                   = null;
    private $topics                   = null;
    private $acts                     = null;
    private $modules                  = null;
    private $build_id                 = null;
    private $row_course_data          = null;

    private $unempty_topics           = [];
    private $unempty_act              = [];

    private $hash                     = null;

    public function __construct($models)
    {
        $this->publication_state_models = $models;
    }

    public function create($course_id, $build_id = null)
    {
        $build_id = (int)$build_id;
        if (!$build_id) {
            $build_id = null;
        }

        $this->course_id = $course_id;
        $this->build_id  = $build_id;

        $this->init();
        $this->insertCourse();
        $this->insertTopicsActsModules();
    }

    private function init()
    {
        $this->initPublicationModels();
        $this->initHash();
        $this->initCourse();
        $this->initTopics();
        $this->initActs();
        $this->initModules();

        $this->calcUnEmptyCategories();
    }

    private function initPublicationModels()
    {
        if (empty($this->publication_state_models)) {
            $this->publication_state_models[] = State::class;
        }
    }

    private function initHash()
    {
        $microtime = microtime(true);

        $this->hash = md5(uniqid() . $this->course_id . $microtime) . '_' . $microtime;
    }

    private function initCourse()
    {
        $this->course = Course::find($this->course_id);

        if (!$this->course) {
            throw new \Exception("Course with ID={$this->course_id} was not founded");
        }
    }

    private function initTopics()
    {
        $this->topics = Category::where('course_id', $this->course_id)
            ->whereNull('parent_id')
            ->orderBy('sort')
            ->get();
    }

    private function initActs()
    {
        $this->acts = Category::where('course_id', $this->course_id)
            ->whereNotNull('parent_id')
            ->orderBy('sort')
            ->get();
    }

    private function initModules()
    {
        $course_id = $this->course_id;
        $build_id  = $this->build_id;

        $build_module_ids = ExportStructureDiffHelper::getBuildModuleContentIds($course_id, $build_id);
        $this->modules = ModuleContent::select([
            'id',
            'category_id',
            'module_id',
            'behavior',
            'name',
            'code',
            'switch_practice_mode',
            'practice_title_manage',
            'practice_index_title',
            'practice_assessment_title',
            'sort',
        ])
            ->with('module')
            ->where(function($q) use($course_id, $build_module_ids) {
                $q->where('course_id', $course_id);

                if (!is_null($build_module_ids)) {
                    $q->whereIn('id', $build_module_ids);
                }
            })
            ->orderBy('sort')
            ->get();
    }

    private function calcUnEmptyCategories()
    {
        foreach ($this->modules as $module) {
            $this->unempty_act[] = $module->category_id;
        }

        $this->unempty_act = array_unique($this->unempty_act);

        foreach ($this->acts as $act) {
            if (in_array($act->id, $this->unempty_act)) {
                $this->unempty_topics[] = $act->parent_id;
            }
        }

        $this->unempty_topics = array_unique($this->unempty_topics);
    }

    private function insertCourse()
    {
        $this->row_course_data = [
            'hash'        => $this->hash,

            'row_type'    => 'course',
            'course_id'   => $this->course_id,
            'course_code' => $this->course->code,
            'course_name' => $this->course->title,

            'order'       => 0,

            'created_at'  => date('Y-m-d H:i:s'),
            'updated_at'  => date('Y-m-d H:i:s'),
        ];

        $this->createPublicationRow($this->row_course_data);
    }

    private function insertTopicsActsModules()
    {
        foreach ($this->topics as $topic) {
            if (!$this->isNeedsToAddTopic($topic->id)) {
                continue;
            }

            $topic_data = array_merge($this->row_course_data, [
                'row_type'           => 'topic',
                'topic_id'           => $topic->id,
                'topic_code'         => $topic->code,
                'topic_name'         => self::getMenuItemName($topic->name),
                'topic_is_separator' => $topic->is_separate,
                'order'              => $topic->sort,
            ]);

            $this->createPublicationRow($topic_data);

            $this->insertActs($topic_data);
        }
    }

    private function isNeedsToAddTopic($topic_id)
    {
        return in_array($topic_id, $this->unempty_topics);
    }

    private function insertActs($topic_data)
    {
        foreach ($this->acts as $act) {
            if ($act->parent_id != $topic_data['topic_id']) {
                continue;
            }

            if (!$this->isNeedsToAddAct($act->id)) {
                continue;
            }

            $act_data = array_merge($topic_data, [
                'row_type' => 'act',
                'act_id'   => $act->id,
                'act_name' => self::getMenuItemName($act->name),
                'order'    => $act->sort,
            ]);

            $this->createPublicationRow($act_data);

            $this->insertModules($act_data);
        }
    }

    private function isNeedsToAddAct($act_id)
    {
        return in_array($act_id, $this->unempty_act);
    }

    private function insertModules($act_data)
    {
        foreach ($this->modules as $module) {
            if ($module->category_id != $act_data['act_id']) {
                continue;
            }

            switch ($module->behavior) {
                case 'practice':
                    $this->insertPractice($module, $act_data);
                    break;
                default:
                    $this->insertUsualModule($module, $act_data);
            }
        }
    }

    private function insertUsualModule($module, $act_data)
    {
        $data = $this->getModuleData($module, $act_data);

        $this->createPublicationRow($data);
    }

    private function insertPractice($module, $act_data)
    {
        $data_index
            = $data_assessment
            = $this->getModuleData($module, $act_data);

        $data_index['module_content_name']      = self::getPracticeIndexName($module);
        $data_assessment['module_content_name'] = self::getPracticeAssessmentName($module);

        // add postfix 'a' to assessment code
        $data_assessment['module_content_id']   = self::modifyAssessmentCode($data_assessment['module_content_id']);

        switch ($module->switch_practice_mode) {
            // insert index only
            case 1:
                $this->createPublicationRow($data_index);
                break;
            // insert assessment only
            case 2:
                $this->createPublicationRow($data_assessment);
                break;
            // insert both
            default:
                $this->createPublicationRow($data_index);
                $this->createPublicationRow($data_assessment);
        }
    }

    private function getModuleData($module, $act_data)
    {
        return array_merge($act_data, [
            'row_type'             => 'module_content',
            'module_template_id'   => $module->module_id,
            'module_content_id'    => $module->id,
            'module_content_code'  => $module->code,
            'module_template_name' => $module->module->path,
            'module_content_name'  => self::getMenuItemName($module->name),
            'behavior'             => $module->behavior,
            'order'                => $module->sort,
        ]);
    }

    private function createPublicationRow($data)
    {
        foreach ($this->publication_state_models as $model) {
            $model::create($data);
        }
    }
}
