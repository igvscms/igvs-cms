<?php namespace Igvs\Courses\Classes;

use Academy\Cms\Models\Comment;
use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Member;
use Igvs\Courses\Models\Status;

/**
 * Class MemberEmailNotice
 * Sending notifications on change member's role or course's status
 * @package Igvs\Courses\Classes
 */
class MemberEmailNotice
{
    /**
     * Max files size for attach to message
     * @var int bytes
     */
    static private $max_files_size = 5242880; // 5 Mb

    /**
     * Send notification about change role of member
     * @param Member $member
     */
    static public function sendEmailNoticeChangeRole($member)
    {
        $to   = $member->user->getOriginalEmail();
        $data = self::getEmailNoticeData($member);

        self::send($to, $data, 'igvs.courses::mail.member-notice');
    }

    /**
     * Send notification about change status of course
     * @param Course $course
     * @param Comment $comment
     * @return bool
     */
    static public function sendEmailNoticeChangeCourseStatus($course, $comment)
    {
        $members = $course->members;

        if (!$members) {
            return false;
        }

        $comment_description = self::getDescriptionFromComment($comment);
        $files               = self::getAttachedFilesFromComment($comment);

        $prev_status = self::getStatusName($course->getPrevStatus());
        $now_status  = self::getStatusName($course->status_id);

        foreach ($members as $member) {
            $to = $member->user->getOriginalEmail();
            $data = self::getEmailNoticeDataWithRoles($member);

            $data['prev_status']         = $prev_status;
            $data['now_status']          = $now_status;

            $data['comment_description'] = trim($comment_description);
            $data['files']               = $files;

            self::send($to, $data, 'igvs.courses::mail.course-status-notice');
        }

        return true;
    }

    /**
     * Return description property from Comment
     * @param Comment $comment
     * @return string|null
     */
    static private function getDescriptionFromComment(Comment $comment)
    {
        return $comment->description;
    }

    /**
     * Return list of Comment's files
     * @param Comment $comment
     * @return array [['path'=> '{string}', 'size'=> {int}, 'name' => '{string}']...] . Size in bytes.
     */
    static private function getAttachedFilesFromComment(Comment $comment)
    {
        if (!$comment->files) {
            return [];
        }

        $return = [];

        foreach ($comment->files as $file) {
            $return[] = [
                'path' => $file->getlocalPath(),
                'size' => $file->file_size,
                'name' => $file->file_name,
            ];
        }

        return $return;
    }

    /**
     * Returns status name by id
     * @param int $id Status id
     * @return string|null
     */
    static private function getStatusName($id)
    {
        $status = Status::getStatus($id);

        return is_null($status) ? null : $status->name;
    }

    /**
     * @param string $to Email recipient
     * @param array $data Data for composing letter
     * @param string $template Path of email template
     */
    static private function send($to, $data, $template)
    {
        $filtered_files = self::filterFiles($data);

        $data['all_files'] = self::getAllFilesCount($data);
        $data['attached_files'] = count($filtered_files);

        \Mail::queueOn('member-notice', $template, $data, function($message) use ($to, $filtered_files) {
            $message->to($to);

            foreach ($filtered_files as $file) {
                $message->attach($file['path'], ['as' => $file['name']]);
            }

        });
    }

    /**
     * Return count of all files if it defined
     * @param array $data Data for fill main template
     * @return int
     */
    static private function getAllFilesCount($data)
    {
        if (isset($data['files']) && is_array($data['files'])) {
            return count($data['files']);
        }

        return 0;
    }

    /**
     * Return list files with average size until 20Mb
     * @param array $data Data for fill main template
     * @return array
     */
    static private function filterFiles($data)
    {
        if (!isset($data['files']) || !is_array($data['files'])) {
            return [];
        }

        $files                = $data['files'];
        $attached_files_size  = 0;
        $files_for_attach     = [];

        foreach ($files as $file) {
            if (($attached_files_size + $file['size']) > self::$max_files_size) {
                continue;
            }

            $attached_files_size += $file['size'];

            $files_for_attach[] = $file;
        }

        return $files_for_attach;
    }

    /**
     * Returns data for fill letter
     * @param Member $member
     * @return array
     */
    static private function getEmailNoticeData($member)
    {
        $user      = $member->user;
        $course    = $member->course;
        $course_id = $member->course->id;

        $data = [
            'name'         => $user->first_name . ' ' . $user->last_name,
            'course_title' => $course->title,
            'course_code'  => $course->code,
        ];

        if (isset($_SERVER['SERVER_NAME'])) {
            $data['link'] = "https://{$_SERVER['SERVER_NAME']}/cp/igvs/courses/{$course_id}/categories";
        }
        if (!is_null($member->server_name)) {
            $data['link'] = "https://{$member->server_name}/cp/igvs/courses/{$course_id}/categories";
        }

        return $data;
    }

    /**
     * Returns data with member's roles for fill letter
     * @param Member $member
     * @return array
     */
    static private function getEmailNoticeDataWithRoles($member)
    {
        $data = self::getEmailNoticeData($member);

        $roles = self::getMemberRoles($member);
        $data['roles'] = implode(', ', $roles);

        return $data;
    }

    /**
     * Returns member's roles
     * @param Member $member
     * @return array
     */
    static private function getMemberRoles($member)
    {
        $roles_result = $member->roles()->get();
        $roles        = [];

        if ($roles_result && count($roles_result)) {
            foreach ($roles_result as $role) {
                $roles[] = $role->name_lang;
            }
        }

        return $roles;
    }
}
