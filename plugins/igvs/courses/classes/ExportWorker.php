<?php namespace Igvs\Courses\Classes;

use Illuminate\Console\Command;
use League\Flysystem\Exception;
use Igvs\Courses\Models\BackgroundTask;

class ExportWorker extends Command
{
    protected $name = 'igvs:worker2';

    use \Igvs\Courses\Traits\FileHelper;
    use \Igvs\Courses\Traits\Scorm;
    use \Igvs\Courses\Traits\PrepareForFtp;
    use \Igvs\Courses\Traits\ExportToReleaseCloud;
    use \Igvs\Courses\Traits\ExportToPilot;
    use \Igvs\Courses\Traits\ExportToZip;
    use \Igvs\Courses\Traits\ImportCourse;

    public function fire($job, $data)
    {

        set_time_limit(60*60);
        ini_set('memory_limit', '1500M');

        try {
            $data = unserialize($data);
        } catch (Exception $e) {
            throw new Exception('Error in data task. Can\'t unserialize data.');
        }

        if (!isset($data['act']))
            throw new Exception('Found not \'act\' option in data');

        if (!isset($data['options']))
            throw new Exception('Found not \'options\' option in data');

        echo json_encode($data) . "\n";

        $act        = isset($data['act']) ? $data['act'] : $data['act'];
        $options    = isset($data['options']) ? $data['options'] : $data['options'];
        $db_id      = isset($data['db_id']) ? $data['db_id'] : $data['db_id'];

        if (!is_null($db_id)) {
            $background_task = BackgroundTask::find($db_id);
            $background_task->started_at = date('Y-m-d H:i:s');
            $background_task->save();
        }

        if (!method_exists($this, $act)) {
            echo "Not found method '" . $act . "'\noptions:\n" . json_encode($data) . "\n";
            return false;
        }

        if (!is_numeric($db_id)) {
            echo "Param db_id is not numeric '" . $act . "'\noptions:\n" . json_encode($data) . "\n";
            return false;
        }

        if (!BackgroundTask::where('id', $db_id)->whereNotNull('deleted_at')->get()) {
            return false;
        }

        if (!is_null($db_id)) {
            BackgroundTask::find($db_id)
                ->update([
                    'progress'      => 0,
                    'progress_full' => 1,
                ]);
        }

        $result = $this->{$act}($options, $job, $db_id);

        if (!is_null($db_id)) {
            $background_task = BackgroundTask::find($db_id);
            $background_task->finished_at = date('Y-m-d H:i:s');
            $background_task->result = $result;
            $background_task->progress_full = 1;
            $background_task->save();
        }

        echo "Task success finished\r\n";

        $job->delete();

        return true;
    }
}