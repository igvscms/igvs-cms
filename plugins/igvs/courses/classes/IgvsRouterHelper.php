<?php namespace Igvs\Courses\Classes;


class IgvsRouterHelper
{
    static public function igvsGetBackendUriPrefix()
    {
        $backendUriExpertise = \Config::get('cms.backendUriExpertise', 'expertise');
        $backendUriInclusive = \Config::get('cms.backendUriInclusive', 'inclusive');

        if (strpos(\Request::getRequestUri(), $backendUriExpertise) !== false)
            return $backendUriExpertise;
        if (strpos(\Request::getRequestUri(), $backendUriInclusive) !== false)
            return $backendUriInclusive;

        return 'backend';
    }
}