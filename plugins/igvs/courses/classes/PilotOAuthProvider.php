<?php

//namespace Laravel\Socialite\One;
namespace Igvs\Courses\Classes;

use InvalidArgumentException;
use Session;
use Academy\Api\Models\OauthLog;

class PilotOAuthProvider extends \Laravel\Socialite\One\AbstractProvider
{
    /**
     * {@inheritdoc}
     */
    public function user()
    {
		$token = $this->getToken();

		$request_time_start = time();
        $user = $this->server->getUserDetails($token);
        $request_time_finish = time();

        Session::put('oauth_token', serialize($token));
        Session::put('oauth_user', serialize($user));

        UserSync::log(print_r([
            '$token' => $token,
            '$user' => $user,
            'Старт запроса' => date('Y-m-d H:i:s', $request_time_start),
            'Окончание запроса' => date('Y-m-d H:i:s', $request_time_finish),
            'Длительность запроса в секундах' => $request_time_finish - $request_time_start,
        ], true), $this,'oauth_user');

        OauthLog::saveData([
            'income' => 0,
            'type' => 'auth',
            'action' => 'userDetails',
            'client' => $this->server->api_client_id,
            'headers' => '',
            'data' => implode("\r\n", [
                "URL: {$this->server->urlUserDetails()}",
                'Старт запроса: ' . date('Y-m-d H:i:s', $request_time_start),
                'Окончание запроса: ' . date('Y-m-d H:i:s', $request_time_finish),
                'Длительность запроса в секундах: ' . ($request_time_finish - $request_time_start),
            ]),
            'request_duration' => $request_time_finish - $request_time_start,
            'answer' => print_r($user, true),
        ]);

        $extraDetails = [
            'location' => $user->location,
            'description' => $user->description,
        ];

        $instance = (new \Laravel\Socialite\One\User)->setRaw(array_merge($user->extra, $user->urls, $extraDetails))
            ->setToken($token->getIdentifier(), $token->getSecret());

        return $instance->map([
            'id' => $user->uid, 'nickname' => $user->nickname,
            'name' => $user->name, 'email' => $user->email, 'avatar' => $user->imageUrl,
            'avatar_original' => str_replace('_normal', '', $user->imageUrl),
        ]);
    }

    public function licencies($course_id)
    {
        $licencies = $this->server->getLicencies(false, $course_id);

        return $licencies;
    }

    public function useLicence($course_id, $pilot_course_id, $external_course_id)
    {
        $response = $this->server->useLicence($course_id, $pilot_course_id, $external_course_id);

        return $response;
    }

    public function systemsAndComps($url = '')
    {
        $response = $this->server->systemsAndComps($url);

        return $response;
    }

    public function structureCourse($url = '', $params)
    {
        $response = $this->server->structureCourse($url, $params);

        return $response;
    }

    /**
     * @param $course_id
     * @param $pilot_course_id идентификатор лицензии
     * @param $parent_course_id
     * @param $export_dir
     * @param $tokenCredentials
     * @param $oauth_user
     * @return mixed
     */
    public function endPoint($options)
    {
        $response = $this->server->endPoint($options);

        return $response;
    }

}