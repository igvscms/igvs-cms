<?php namespace Igvs\Courses\Classes;

use DOMElement;
use Wingu\OctopusCore\Reflection\ReflectionClass;
use Wingu\OctopusCore\Reflection\ReflectionMethod;
use \PHP2WSDL\WSDL;


class PHPClass2WSDL extends  \PHP2WSDL\PHPClass2WSDL
{
    public function generateWSDL($withAnnotation = false)
    {
        $qNameClassName = WSDL::typeToQName($this->class);

        $this->wsdl = new WSDL($qNameClassName, $this->uri, $this->xslUri);

        $port = $this->wsdl->addPortType($qNameClassName . 'Port');
        $binding = $this->wsdl->addBinding($qNameClassName . 'Binding', 'tns:' . $qNameClassName . 'Port');

        $this->wsdl->addSoapBinding($binding, $this->bindingStyle['style'], $this->bindingStyle['transport']);
        $this->wsdl->addService(
            $qNameClassName . 'Service', $qNameClassName . 'Port',
            'tns:' . $qNameClassName . 'Binding',
            $this->uri
        );

        $ref = new ReflectionClass($this->class);
        foreach ($ref->getMethods() as $method) {
            if ($withAnnotation === false || $method->getReflectionDocComment()->getAnnotationsCollection()->hasAnnotationTag('soap')) {
                $this->addMethodToWsdlWithOutAnchor($method, $port, $binding);
            }
        }
    }

    protected function addMethodToWsdlWithOutAnchor(ReflectionMethod $method, DOMElement $port, DOMElement $binding)
    {
        $qNameMethodName = \PHP2WSDL\WSDL::typeToQName($method->getName());

        $args = array();
        $annotations = array();
        $methodAnnotationsCollection = $method->getReflectionDocComment()->getAnnotationsCollection();
        if ($methodAnnotationsCollection->hasAnnotationTag('param')) {
            /** @var \Wingu\OctopusCore\Reflection\Annotation\Tags\ParamTag $param */
            foreach ($methodAnnotationsCollection->getAnnotation('param') as $param) {
                $annotations[$param->getParamName()] = $param;
            }
        }

        if ($this->bindingStyle['style'] === 'document') {
            $sequence = array();
            /** @var \Wingu\OctopusCore\Reflection\ReflectionParameter $param */
            foreach ($method->getParameters() as $param) {
                $type = 'anytype';
                if (isset($annotations['$' . $param->getName()])) {
                    $type = $annotations['$' . $param->getName()]->getParamType();
                }

                $sequenceElement = array('name' => $param->getName(), 'type' => $this->wsdl->getXSDType($type));
                if ($param->isOptional()) {
                    $sequenceElement['nillable'] = 'true';
                }

                $sequence[] = $sequenceElement;
            }

            $element = array('name' => $qNameMethodName, 'sequence' => $sequence);
            $args['parameters'] = array('element' => $this->wsdl->addElement($element));
        } else {
            /** @var \Wingu\OctopusCore\Reflection\ReflectionParameter $param */
            foreach ($method->getParameters() as $param) {
                $type = 'anytype';
                if (isset($annotations['$' . $param->getName()])) {
                    $type = $annotations['$' . $param->getName()]->getParamType();
                }

                $args[$param->getName()] = array('type' => $this->wsdl->getXSDType($type));
            }
        }

        $this->wsdl->addMessage($qNameMethodName . 'In', $args);

        $returnType = null;
        if ($methodAnnotationsCollection->hasAnnotationTag('return') === true) {
            $annotation = $methodAnnotationsCollection->getAnnotation('return');
            $annotation = reset($annotation);
            $returnType = $annotation->getReturnType();
        }

        $isOneWayMessage = ($returnType === null);

        if ($isOneWayMessage === false) {
            $args = array();
            if ($this->bindingStyle['style'] === 'document') {
                $sequence = array();
                if ($returnType !== null) {
                    $sequence[] = array(
                        'name' => $qNameMethodName . 'Result',
                        'type' => $this->wsdl->getXSDType($returnType)
                    );
                }

                $element = array('name' => $qNameMethodName . 'Response', 'sequence' => $sequence);
                $args['parameters'] = array('element' => $this->wsdl->addElement($element));
            } elseif ($returnType !== null) {
                $args['return'] = array('type' => $this->wsdl->getXSDType($returnType));
            }

            $this->wsdl->addMessage($qNameMethodName . 'Out', $args);
        }

        // Add the portType operation.
        if ($isOneWayMessage === false) {
            $portOperation = $this->wsdl->addPortOperation(
                $port,
                $qNameMethodName,
                'tns:' . $qNameMethodName . 'In',
                'tns:' . $qNameMethodName . 'Out'
            );
        } else {
            $portOperation = $this->wsdl->addPortOperation($port, $qNameMethodName, 'tns:' . $qNameMethodName . 'In');
        }

        // Add any documentation for the operation.
        $description = $method->getReflectionDocComment()->getFullDescription();
        if (strlen($description) > 0) {
            $this->wsdl->addDocumentation($portOperation, $description);
        }

        // When using the RPC style, make sure the operation style includes a 'namespace' attribute (WS-I Basic Profile 1.1 R2717).
        if ($this->bindingStyle['style'] === 'rpc' && isset($this->operationBodyStyle['namespace']) === false) {
            $this->operationBodyStyle['namespace'] = '' . htmlspecialchars($this->uri);
        }

        // Add the binding operation.
        $operation = $this->wsdl->addBindingOperation(
            $binding,
            $qNameMethodName,
            $this->operationBodyStyle,
            $this->operationBodyStyle
        );
        $this->wsdl->addSoapOperation($operation, $this->uri . '/' . $qNameMethodName);
    }
}
