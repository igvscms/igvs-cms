<?php namespace Igvs\Courses\Classes;

use \Db;
use Backend\Models\UserGroup;
use Igvs\Courses\Models\User;
use Igvs\Courses\Models\UserPilot;
use Igvs\Courses\Models\Member;
use Igvs\Courses\Models\MemberRole;

class OperatorsActions
{
    static private $operator_group_code = 'operator-elearning';
    static private $operator_group_id = 0;

    static private $course_role_administrator_id = 1;

    static public function getListByInstanceCluster($instance_id, $cluster_id)
    {
        $operator_group_id = self::getOperatorGroupId();

        $operators_db = Db::table('backend_users')
            ->select('backend_users.id AS id',
                'backend_users.last_name AS last_name',
                'backend_users.first_name AS first_name',
                'igvs_courses_user_pilot.email AS email',
                'igvs_courses_user_pilot.poo_id AS poo_id')
            ->leftjoin('igvs_courses_user_pilot', 'igvs_courses_user_pilot.user_id', '=', 'backend_users.id')
            ->leftjoin('backend_users_groups', 'backend_users_groups.user_id', '=', 'backend_users.id')
            ->where('igvs_courses_user_pilot.instance_id', $instance_id)
            ->where('igvs_courses_user_pilot.cluster_id', $cluster_id)
            ->where('backend_users_groups.user_group_id', $operator_group_id)
            ->get();

        $return = [];

        foreach ($operators_db as $operator) {
            $return[(int)$operator->id] = [
                'name' => "{$operator->last_name} {$operator->first_name} ({$operator->email})",
                'poo_id' => $operator->poo_id,
            ];
        }

        return $return;
    }

    static public function checkIdInList($id, $instance_id, $cluster_id)
    {
        $list = self::getListByInstanceCluster($instance_id, $cluster_id);

        return isset($list[(int)$id]);
    }

    static public function addOperatorToCourse($course_id, $operator_id)
    {
        $member = Member::firstOrCreate([
            'user_id' => $operator_id,
            'course_id' => $course_id,
        ]);
        $member->roles()->detach(self::$course_role_administrator_id);
        $member->roles()->attach(self::$course_role_administrator_id);

        return true;
    }

    static public function filterUsedOperators($course_id, $users_list)
    {
        $admins_course_user_ids = Db::table('backend_users')
            ->leftjoin('igvs_courses_members', 'igvs_courses_members.user_id', '=', 'backend_users.id')
            ->leftjoin('igvs_courses_members_roles', 'igvs_courses_members_roles.member_id', '=', 'igvs_courses_members.id')
            ->where('igvs_courses_members.course_id', $course_id)
            ->where('igvs_courses_members_roles.role_id', self::$course_role_administrator_id)
            ->lists('backend_users.id');

        if (!count($admins_course_user_ids))
            return $users_list;

        foreach ($admins_course_user_ids as $user_id) {
            unset($users_list[(int)$user_id]);
        }

        return $users_list;
    }

    static public function sortByPooId($poo_id, $user_list)
    {
        uasort($user_list, function ($a, $b) use ($poo_id) {
            $poo_id = (int)$poo_id;
            $a_poo = (int)$a['poo_id'];
            $b_poo = (int)$b['poo_id'];
            $a_name = $a['name'];
            $b_name = $b['name'];

            if ($a_poo == $poo_id
                && $b_poo != $poo_id) {

                return -1;
            } elseif ($a_poo != $poo_id
                && $b_poo == $poo_id) {

                return 1;
            } else {
                if ($a_name > $b_name)
                    return 1;
                elseif ($a_name < $b_name)
                    return -1;
                else
                    return 0;
            }
        });

        return $user_list;
    }

    static private function getOperatorGroupId()
    {
        if (self::$operator_group_id == 0) {
            self::setOperatorGroupId();
        }

        return self::$operator_group_id;
    }

    static private function setOperatorGroupId()
    {
        $group = UserGroup::where('code', self::$operator_group_code)->first();

        if (!$group)
            throw new \Exception('Was not found operators group');

        self::$operator_group_id = $group->id;
    }
}
