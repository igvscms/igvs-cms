<?php

namespace Igvs\Courses\Classes;

use InvalidArgumentException;
use Illuminate\Support\Manager;
use Laravel\Socialite\One\TwitterProvider;
use Laravel\Socialite\One\BitbucketProvider;
use Igvs\Courses\Classes\PilotOAuthProvider;
use League\OAuth1\Client\Server\Twitter as TwitterServer;
use League\OAuth1\Client\Server\Bitbucket as BitbucketServer;
use Igvs\Courses\Classes\PilotOAuthServer as PilotServer;
use Igvs\Courses\Classes\PilotOAuthServerTest as PilotServerTest;
use Academy\Api\Models\Client;

class SocialiteManager extends Manager implements \Laravel\Socialite\Contracts\Factory
{
    /**
     * Get a driver instance.
     *
     * @param  string  $driver
     * @return mixed
     */
    public function with($driver)
    {
        return $this->driver($driver);
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Laravel\Socialite\Two\AbstractProvider
     */
    protected function createGithubDriver()
    {
        $config = $this->app['config']['services.github'];

        return $this->buildProvider(
            'Laravel\Socialite\Two\GithubProvider', $config
        );
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Laravel\Socialite\Two\AbstractProvider
     */
    protected function createFacebookDriver()
    {
        $config = $this->app['config']['services.facebook'];

        return $this->buildProvider(
            'Laravel\Socialite\Two\FacebookProvider', $config
        );
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Laravel\Socialite\Two\AbstractProvider
     */
    protected function createGoogleDriver()
    {
        $config = $this->app['config']['services.google'];

        return $this->buildProvider(
            'Laravel\Socialite\Two\GoogleProvider', $config
        );
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Laravel\Socialite\Two\AbstractProvider
     */
    protected function createLinkedinDriver()
    {
        $config = $this->app['config']['services.linkedin'];

        return $this->buildProvider(
          'Laravel\Socialite\Two\LinkedInProvider', $config
        );
    }

    /**
     * Build an OAuth 2 provider instance.
     *
     * @param  string  $provider
     * @param  array  $config
     * @return \Laravel\Socialite\Two\AbstractProvider
     */
    public function buildProvider($provider, $config)
    {
        $redirect = ($_SERVER['SERVER_PORT']!=443 ? 'http' : 'https') . '://' . $_SERVER['SERVER_NAME'] . $config['redirect'];

        return new $provider(
            $this->app['request'], $config['client_id'],
            $config['client_secret'], $redirect
        );
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Laravel\Socialite\One\AbstractProvider
     */
    protected function createTwitterDriver()
    {
        $config = $this->app['config']['services.twitter'];

        return new TwitterProvider(
            $this->app['request'], new TwitterServer($this->formatConfig($config))
        );
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Laravel\Socialite\One\AbstractProvider
     */
    protected function createBitbucketDriver()
    {
        $config = $this->app['config']['services.bitbucket'];

        return new BitbucketProvider(
            $this->app['request'], new BitbucketServer($this->formatConfig($config))
        );
    }

    /**
     * Create an instance of the specified driver.
     *
     * @return \Laravel\Socialite\One\AbstractProvider
     */
    protected function createPilotOAuthDriver()
    {
        $config = $this->app['config']['services.pilotoauth'];

        $key_secret = $this->getClientKeySecretVersion();
        if (!is_null($key_secret)) {
            $config['client_id'] = $key_secret['key'];
            $config['client_secret'] = $key_secret['secret'];
            $config['client_version'] = $key_secret['version'];
            $config['api_client_id'] = $key_secret['api_client_id'];
            $config['end_point_action_name'] = $key_secret['end_point_action_name'];
        }

        return new PilotOAuthProvider(
            $this->app['request'], new PilotServer($this->formatConfig($config))
        );
    }

    protected function createPilotOAuthExpertiseDriver()
    {
        $config = $this->app['config']['services.pilotoauth_expertise'];

        $key_secret = $this->getClientKeySecretVersion();
        if (!is_null($key_secret)) {
            $config['client_id'] = $key_secret['key'];
            $config['client_secret'] = $key_secret['secret'];
            $config['client_version'] = $key_secret['version'];
            $config['api_client_id'] = $key_secret['api_client_id'];
            $config['end_point_action_name'] = $key_secret['end_point_action_name'];
        }

        return new PilotOAuthProvider(
            $this->app['request'], new PilotServer($this->formatConfig($config))
        );
    }

    protected function createPilotOAuthInclusiveDriver()
    {
        $config = $this->app['config']['services.pilotoauth_inclusive'];

        $key_secret = $this->getClientKeySecretVersion(true);
        if (!is_null($key_secret)) {
            $config['client_id'] = $key_secret['key'];
            $config['client_secret'] = $key_secret['secret'];
            $config['client_version'] = $key_secret['version'];
            $config['api_client_id'] = $key_secret['api_client_id'];
            $config['end_point_action_name'] = $key_secret['end_point_action_name'];
        }

        return new PilotOAuthProvider(
            $this->app['request'], new PilotOAuthServerInclusive($this->formatConfig($config))
        );
    }

    protected function createPilotOAuthTestDriver()
    {
        $config = $this->app['config']['services.pilotoauth'];

        return new PilotOAuthProvider(
            $this->app['request'], new PilotServerTest($this->formatConfig($config))
        );
    }

    private function getClientKeySecretVersion($inclusive = false)
    {
        $pilot_domain = \Session::get('igvs.courser.pilot_domain', '');

        $result = Client::where('domain', $pilot_domain)->first();

        if (!$inclusive) {

            $length = $result
                && isset($result['oauth_key'])
                && isset($result['oauth_secret'])
                && strlen($result['oauth_key'])
                && strlen($result['oauth_secret']);

            return $length ? [
                'key' => $result['oauth_key'],
                'secret' => $result['oauth_secret'],
                'version' => $result['version'],
                'api_client_id' => $result['id'],
                'end_point_action_name' => $result['endpoint_method'],
            ] : null;

        } else {

            $length = $result
                && isset($result['oauth_key_inclusive'])
                && isset($result['oauth_secret_inclusive'])
                && strlen($result['oauth_key_inclusive'])
                && strlen($result['oauth_secret_inclusive']);

            return $length ? [
                'key' => $result['oauth_key_inclusive'],
                'secret' => $result['oauth_secret_inclusive'],
                'version' => $result['version_inclusive'],
                'end_point_action_name' => $result['endpoint_method_inclusive'],
                'api_client_id' => $result['id'],
            ] : null;

        }
    }

    /**
     * Format the server configuration.
     *
     * @param  array  $config
     * @return array
     */
    public function formatConfig(array $config)
    {
        if (!isset($_SERVER['SERVER_PORT']) || !isset($_SERVER['SERVER_NAME'])) {
            $url = $this->app['config']['app.url'];
            $redirect = $url . $config['redirect'];
        } else {
            $redirect = ($_SERVER['SERVER_PORT']!=443 ? 'http' : 'https') . '://' . $_SERVER['SERVER_NAME'] . $config['redirect'];
        }

        return array_merge([
            'identifier' => $config['client_id'],
            'secret' => $config['client_secret'],
            'callback_uri' => $redirect,
        ], $config);
    }

    /**
     * Get the default driver name.
     *
     * @throws \InvalidArgumentException
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        throw new InvalidArgumentException('No Socialite driver was specified.');
    }
}
