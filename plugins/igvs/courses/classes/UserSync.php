<?php namespace Igvs\Courses\Classes;

use Illuminate\Routing\Controller as ControllerBase;
use Faker\Factory;
use Igvs\Courses\Models\UserPilot;
use Igvs\Courses\Models\Member;
use Backend\Models\UserGroup;
use Backend\Models\User;
use Academy\Api\Models\Client;
use BackendAuth;
use AccessLog;
use Session;

class UserSync extends ControllerBase {

    private $user_data_mapping = [
        'id' => 'ID',
        'login' => 'LOGIN',
        'active' => 'ACTIVE',
        'first_name' => 'NAME',
        'last_name' => 'LAST_NAME',
        'email' => 'EMAIL',
//        'photo' => 'PERSONAL_PHOTO',
        'groups' => 'GROUPS',
        'courses' => 'COURSES',
        'lang' => 'LANG'
    ];

    private $groups_mapping = [
        'Learning provider' => null,
        'Service provider' => null,
        'user' => 'pilot_user',
        'operator' => 'operator',
        'KAM' => 'KAM',
        'Teachers / Tutors' => null,
        'Teacher' => null,
        'Teachers' => null,
        'Tutor' => null,
        'Tutors' => null,
    ];

    private $errors = [];
    private $created = 0;
    private $updated = 0;

    public function saveUpdateUser($user_data, $instance_id, $class, $update = true)
    {

        $this->log("saveUpdateUser. \r\n" . print_r($user_data, true) . "\r\n", $class, 'details');

        $pilot_user_id = $user_data['ID'];

        $user_pilot = UserPilot::where('pilot_user_id', $pilot_user_id)
            ->where('instance_id', $instance_id)
            ->first();

        $faker = Factory::create();
        $login = "pilot_user.{$instance_id}.{$pilot_user_id}";
        $password = $faker->password(10, 20);

        try {

            if (!$user_pilot) {
                $this->log("create", $class,'details');
                $this->createUser($login, $password, $user_data, $instance_id, $class);
            } else if ($update) {
                $this->log("update", $class, 'details');
                $this->updateUser($user_pilot, $login, $password, $user_data, $class);
            }

            $this->log("saved/updated", $class, 'details');

        } catch (\Exception $e) {

            $this->log('error: ' . print_r([
                    $e,
                    'pilot_user_id' => $pilot_user_id,
                    'login' => $login,
                    'instance_id' => $instance_id,
                ], true), $class, 'details');

        }
    }

    private function createUser($login, $password, $user_data, $instance_id, $class)
    {
        try {
            //create user
            $backend_user = new User();

            $user_params = [
                'login' => $login,
                'first_name' => $this->getUserDataParam($user_data, 'first_name'),
                'last_name' => $this->getUserDataParam($user_data, 'last_name'),
                'email' => "$login@pilotmail.pilot",
                'password' => $password,
                'password_confirmation' => $password,
            ];

            $backend_user->fill($user_params);
            $backend_user->save();

            $this->updateUserGroups($backend_user, $user_data);

            if (isset($user_data['COURSES']))
                $this->updateUserRolesInCourses($backend_user, $user_data, $instance_id);

            if (isset($user_data['LANG']))
                $this->updateUserLanguage($backend_user, $user_data, $class);

            $user_pilot = new UserPilot();
            $user_pilot->user_id = $backend_user->id;
            $user_pilot->pilot_user_id = $this->getUserDataParam($user_data, 'id');
            $user_pilot->instance_id = $instance_id;
            $user_pilot->cluster_id = $this->getUserCluster($user_data);
            $user_pilot->poo_id = $this->getUserPooId($user_data);
            $user_pilot->email = $this->getUserDataParam($user_data, 'email');
            $user_pilot->save();

            $this->created++;
        } catch (\Exception $e) {
            $this->log("error " . print_r([
                    $e->getMessage(),
                    'pilot_user_id' => $this->getUserDataParam($user_data, 'id'),
                    'login' => $login,
                    'instance_id' => $instance_id,
                ], true), $class, 'details');
        }
        return true;
    }

    private function updateUser($user_pilot, $login, $password, $user_data, $class)
    {
        try {
            $backend_user = User::find($user_pilot->user_id);

            if (!$backend_user) {
                $this->pushError('10003 You must give array with USERS and INSTANCE_ID!');
                return false;
            }

            $user_pilot->email = $this->getUserDataParam($user_data, 'email');
            $user_pilot->cluster_id = $this->getUserCluster($user_data);
            $user_pilot->poo_id = $this->getUserPooId($user_data);
            $user_pilot->save();

            $backend_user->first_name = $this->getUserDataParam($user_data, 'first_name');
            $backend_user->last_name = $this->getUserDataParam($user_data, 'last_name');
            $backend_user->login = $login;
            $backend_user->email = "$login@pilotmail.pilot";

            $backend_user->save();

            $this->updateUserGroups($backend_user, $user_data);

            if (isset($user_data['COURSES']))
                $this->updateUserRolesInCourses($backend_user, $user_data, $user_pilot->instance_id);

            if (isset($user_data['LANG']))
                $this->updateUserLanguage($backend_user, $user_data, $class);

            $this->updated++;
        } catch (\Exception $e) {
            $this->log("error " . $e->getMessage(), $class, 'details');
        }

        return true;
    }

    public function updateOperatorGroup($client_instance_id)
    {
        // подменяем группу, если не основной пилот
        //todo эту карягу тоже убрать
        if ($client_instance_id != 1) {
            $this->groups_mapping['user'] = 'elearning';
            $this->groups_mapping['operator'] = 'operator-elearning';
        }
    }

    public function initSoapUpdate()
    {
        unset($this->user_data_mapping['courses']);
        unset($this->user_data_mapping['lang']);
    }

    private function getUserDataParam($user_data, $key)
    {
        return $user_data[$this->user_data_mapping[$key]];
    }

    private function getUserCluster($user_data)
    {
        return isset($user_data['UF_CLUSTER_ID']) ? (int)$user_data['UF_CLUSTER_ID'] : 0;
    }

    private function getUserPooId($user_data)
    {
        return isset($user_data['POO_ID']) ? (int)$user_data['POO_ID'] : 0;
    }


    public function checkUserData($user_data, $class)
    {
        $missing_keys = [];

        foreach ($this->user_data_mapping as $mapping_key => $mapping_value) {
            if (!isset($user_data[$mapping_value])) {
                $missing_keys[] = $mapping_value;
            }
        }
        //$missing_keys = array_diff($this->user_data_mapping, array_keys($user_data));

        if (count($missing_keys)) {
            $this->log("User missing key(s) " . implode(', ', $missing_keys) . ". Skipped. 10004 Data: " . print_r($user_data, true), $class, 'details');
            $this->pushError("User missing key(s) " . implode(', ', $missing_keys) . ". Skipped. Data: " . print_r($user_data, true), 10004);
            return false;
        }

        return true;
    }

    private function updateUserGroups($user, $user_data)
    {
        //отцепляем от групп pilot
        $group_codes_all = array_values($this->groups_mapping);
        //не отцепляем операторов (устаревшее)
//        $group_codes_all = array_diff($group_codes_all, ['', null, 'operator', 'operator-elearning']);
        $group_codes_all = array_map('strtolower', $group_codes_all);

        $group_codes_all_ids = UserGroup::whereIn('code', $group_codes_all)->lists('id');
        $user->groups()->detach($group_codes_all_ids);

        //смотрим какие группы пришли с пользователем, переводим в свои коды
        $input_group_codes = $this->getInputUserDataGroups($user_data);

        ////если не пришло не одной группы, то крепим к группе пользователей
        if (!count($input_group_codes)) {
            $input_group_codes = [$this->groups_mapping['user']];
        }

        $local_user_group_ids = UserGroup::whereIn('code', $input_group_codes)
            ->lists('id');

        //исключаем из найдённых групп те, которые уже есть (как правило - группы оператора)
        $local_user_group_ids = array_diff($local_user_group_ids, $user->groups()->lists('id'));
        $user->groups()->attach($local_user_group_ids);
    }

    private function updateUserRolesInCourses($user, $user_data, $client_instance_id = 0)
    {
        $member_list = Member::where('user_id', $user->id)->get();
        foreach ($member_list as $member) {
            $member->roles()->detach(5);
        }

        if ($client_instance_id != 1) {
            return;
        }

        foreach ($this->getUserDataParam($user_data, 'courses') as $course_id => $permission) {
            if (!$permission || in_array($course_id, ['access_id', 'provider_name'])) {
                continue;
            }

            // attache role
            $member = Member::firstOrCreate([
                'user_id' => $user->id,
                'course_id' => $course_id,
            ]);
            $member->roles()->attach(5);
        }
    }

    private function updateUserLanguage($user, $user_data, $class)
    {
        try {
            $locale = $this->getUserDataParam($user_data, 'lang');

            if ($locale&& in_array($locale, ['ru', 'en'])) {

                $user_preference = \Backend\Models\UserPreference::forUser($user);

                $value = $user_preference->get('backend::backend.preferences', []);
                $value['locale'] = $locale;
                $user_preference->set('backend::backend.preferences', $value);
                Session::put('igvs.courser.pilot_locale', $locale);
            }
        } catch (\Exception $e) {
            UserSync::log("\r\n Can not set locale " . print_r([
                    'getMessage' => $e->getMessage(),
                    '$user' => $user_data
                ], true), $class, 'errors');

        }
    }

    private function getInputUserDataGroups($user_data)
    {
        $input_groups = $this->getUserDataParam($user_data, 'groups');

        if (!count($input_groups)) {
            return [];
        }

        $input_group_codes = [];

        foreach ($input_groups as $input_group) {
            if (!in_array($input_group, array_keys($this->groups_mapping))) {
                $this->pushError("Unknown group code '{$input_group}'", 10005);
                continue;
            }
            if (is_null($this->groups_mapping[$input_group]) || strlen($this->groups_mapping[$input_group]) == 0) {
                continue;
            }
            $input_group_codes[] = $this->groups_mapping[$input_group];
        }

        return $input_group_codes;
    }

    private function pushError($error_text, $code = 0)
    {
        $this->errors[] = [
            'text' => $error_text,
            'code' => $code
        ];
    }

    /**
     * Пишет логи
     * @param string $text текст ошибки
     * @param string $type_log тип лога: 'all' - в лог с развёрнутыми данными, 'details' - в детальный лог; по-умолчанию - в общий лог
     */
    static public function log($text, $class, $log_name = '')
    {
        $class_name = explode('\\', get_class($class));
        $class_name = strtolower($class_name[count($class_name)-1]);

        if (!is_string($text)) {
            $text = print_r($text, true);
        }

        $log_name = "{$class_name}_{$log_name}";

        file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/storage/app/igvs/{$log_name}.log", date('Y-m-d H:i:s') . "\r\n" . $text . "\r\n", FILE_APPEND);
    }

    /**
     * Приводит клюючи массива к верхнему регистру
     * @param array $ar массив
     * @param object $class класс, из которого вызывается фун-я
     * @param string $to_case upper|lower
     * @return array|bool
     */
    static public function changeArrayKeysCase($ar, $class, $to_case = 'upper')
    {
        if (!is_array($ar)) {
            return $ar;
        }

        try {
            $case = $to_case == 'upper' ? CASE_UPPER : CASE_LOWER;

            $ar = array_change_key_case($ar, $case);
        } catch (\Exception $e) {
            self::log("arrayKeysToUpperCase Error: " . $e->getMessage() .
                'data: ' . print_r($ar, true), $class, 'errors');
        }

        return $ar;
    }

    public function getCountErrors()
    {
        return $this->errors;
    }

    public function getCountCreated()
    {
        return $this->created;
    }

    public function getCountUpdated()
    {
        return $this->updated;
    }


}