<?php namespace Igvs\Courses\Classes\Reports;

use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\ModuleContent;

class CourseQuestionAmount
{
    private $course_id = 0;
    private $act_topic_relation = [];

    public $count_questions_topics = []; // topic_id => count
    public $count_questions_course = 0; // count all questions
    public $count_questions_behavior_topic = []; // topic_id => [behavior_type (ask,check, test, practice) => count]
    public $count_questions_behavior_course = []; // behavior_type (ask,check, test, practice) => count
    public $count_modules_topic = []; // topic_id => count
    public $count_modules_behavior_topic = []; // topic_id => [behavior_type (ask,check, test, practice) => count]
    public $count_modules_behavior_course = []; // behavior_type (ask,check, test, practice) => count
    public $count_modules_course = 0; // count all modules
    public $behavior_types = [];
    public $topics = [];

    public function __construct($course_id)
    {
        $this->course_id = $course_id;
    }

    public function getReport()
    {
        $this->initTopics();

        ModuleContent::where('course_id', $this->course_id)
            ->chunk(20, function($modules_content) {

                foreach ($modules_content as $module_content) {
                    $behavior_type = $module_content->behavior;
                    $act_id = $module_content->category_id;
                    $topic_id = $this->getTopicIdByActId($act_id);

                    $this->initArrays($behavior_type, $topic_id);
                    $this->modulesCounterPlus($topic_id, $behavior_type);

                    $questions_count = $this->getCountQuestions($module_content, $topic_id, $behavior_type);
                    $this->questionsCounterPlus($topic_id, $behavior_type, $questions_count);
                }
            }
            );

        return $this;
    }

    private function initTopics()
    {
        $topics_db = Category::select('id', 'name', 'code')
            ->where('course_id', $this->course_id)
            ->whereNull('parent_id')
            ->orderBy('sort', 'asc')
            ->get();

        $topics = [];
        foreach ($topics_db as $topic_db) {
            $title = $topic_db->code . ' ' . $topic_db->name;
            $title = str_replace(['<br>'], [' '], $title);
            $topics[$topic_db->id] = $title;
        }
        $this->topics = $topics;

        foreach ($topics as $topic_id => $topic_name) {
            $this->count_questions_topics[$topic_id] = 0;
            $this->count_modules_topic[$topic_id] = 0;
            $this->count_modules_behavior_topic[$topic_id] = [];
        }

        $categories_act_ids = ModuleContent::where('course_id', $this->course_id)->lists('category_id');
        $act_topic_relation = Category::whereIn('id', $categories_act_ids)->lists('parent_id', 'id');

        $this->act_topic_relation = $act_topic_relation;
    }

    private function getTopicIdByActId($act_id)
    {
        return isset($this->act_topic_relation[$act_id]) ? $this->act_topic_relation[$act_id] : 0;
    }

    private function modulesCounterPlus($topic_id, $behavior_type)
    {
        $this->count_modules_course++;
        $this->count_modules_topic[$topic_id]++;
        $this->count_modules_behavior_topic[$topic_id][$behavior_type]++;
        $this->count_modules_behavior_course[$behavior_type]++;
    }

    private function questionsCounterPlus($topic_id, $behavior_type, $question_count)
    {
        $this->count_questions_topics[$topic_id] += $question_count;
        $this->count_questions_course += $question_count;
        $this->count_questions_behavior_course[$behavior_type] += $question_count;
        $this->count_questions_behavior_topic[$topic_id][$behavior_type] += $question_count;
    }

    private function getCountQuestions($module_content, $topic_id, $behavior_type)
    {
        $count_questions = 0;
        try {
            $data = str_replace(["\n", "\r"], ['', ''], $module_content->data);
            $data = json_decode($data);

            if (isset($data->questions)) {
                $count_questions = count($data->questions);

            }
        } catch (\Exception $e){}

        return $count_questions;
    }

    private function initArrays($behavior_type, $topic_id)
    {
        $this->addBehaviorToArray($behavior_type);

        $this->initArrayParams('count_questions_topics', [$topic_id], 0);
        $this->initArrayParams('count_modules_topic', [$topic_id], 0);
        $this->initArrayParams('count_modules_behavior_topic', [$topic_id, $behavior_type], 0);
        $this->initArrayParams('count_questions_behavior_topic', [$topic_id, $behavior_type], 0);
        $this->initArrayParams('count_modules_behavior_course', [$behavior_type], 0);
        $this->initArrayParams('count_questions_behavior_course', [$behavior_type], 0);
    }

    private function addBehaviorToArray($behavior_type)
    {
        if ($this->checkBehaviorInArray($behavior_type))
            return;

        $this->behavior_types[] = $behavior_type;
    }

    private function checkBehaviorInArray($behavior_type)
    {
        return in_array($behavior_type, $this->behavior_types);
    }

    private function initArrayParams($array_name, $params, $default_value_in)
    {
        $array_item = &$this->{$array_name};

        $i = 0;
        foreach ($params as $param) {
            $i++;

            if ($this->checkExistsArrayParam($array_item, $param)) {
                $array_item = &$array_item[$param];
                continue;
            }

            $default_value = $this->getDefaultValueParam($i, $params, $default_value_in);
            $this->setDefaultParam($array_item, $param, $default_value);
            $array_item = &$array_item[$param];
        }
    }

    private function getDefaultValueParam($i, $params, $default_value_in) {
        return $i < count($params) ? [] : $default_value_in;
    }

    private function setDefaultParam(&$item, $param, $default_value)
    {
        $item[$param] = $default_value;
    }


    private function checkExistsArrayParam($item, $param)
    {
        return isset($item[$param]);
    }
}
