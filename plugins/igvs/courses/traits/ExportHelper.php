<?php namespace Igvs\Courses\Traits;


trait ExportHelper
{
    /**
     * Return array of Courses, with ID in key and Build in value
     * @param $str
     * @return array
     */
    public function getCoursesFromCommaString($str)
    {
        $str = trim($str);

        if (!is_string($str) || !strlen($str))
            return [];

        $arg_courses = explode(",",$str);

        $courses_ids = [];

        foreach ($arg_courses as $course) {
            $course = trim($course);
            $course = explode(':', $course);

            $courses_ids[$course[0]] = isset($course[1]) ? $course[1] : null;
        }

        return $courses_ids;
    }
}
