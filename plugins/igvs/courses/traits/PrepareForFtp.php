<?php namespace Igvs\Courses\Traits;

use DB;
use Config;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\Course as CourseItem;
use Igvs\Courses\Models\Vocabulary;
use Illuminate\Database\Eloquent\Builder;
use Igvs\Courses\Models\ModuleTheme;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Helpers\Translit;

trait PrepareForFtp
{
    public function prepareForFtp($courseId, $buildId = null, $theme = '_default')
    {
        $return = 'Exported modules:<br>';

        $base_path = Config::get('igvs.courses::prepareForFtp.basePath');
        $modules_dir = Config::get('igvs.courses::prepareForFtp.modulesDir');
        $src_content = Config::get('igvs.courses::modulesContent.path');


        if (is_dir($base_path)) {
            $this->rmdir_recurse($base_path);
        }

        mkdir($base_path);
        mkdir($base_path . '/' . $modules_dir);

        $whereCallback = function (Builder $q) use ($buildId) {
            if (!is_null($buildId) && strlen($buildId))
                $q->where('build_id', $buildId);
        };

        $modules = ModuleContent::whereHas('builds', $whereCallback)
            ->where('course_id', $courseId)
            ->get();

        $templates_modules_names = Module::whereIn('id', $modules->lists('module_id'))
            ->get()
            ->lists('name', 'id');

        foreach ($modules as $module) {
            //template module
            $module_name = $templates_modules_names[$module->module_id];
            $temp_module_dest = "{$base_path}/{$modules_dir}/{$module_name}/{$module->getModuleVersion()}";
            if (!is_dir($temp_module_dest)) {
                $this->recourceCopy($module->getPathModule(), $temp_module_dest);
                if (is_file($temp_module_dest . '/logo.png')) {
                    unlink($temp_module_dest . '/logo.png');
                }
            }

            $module_theme = is_dir($temp_module_dest . '/themes/' . $theme) && strlen($theme)? 'themes/' . $theme . '/css' : 'css';

            //  modules data
            $src = "{$src_content}/{$module->course_id}/{$module->code}";
            $dest = "{$base_path}/{$module->code}";

            $this->recourceCopy($src, $dest . '/content');

            $obj = json_decode($module->data);

            if (is_object($obj)) {
                $obj->colontitle = $module->name;
                $obj->practice = $module->isPractice();
                $module->data = json_encode($obj);
            }

            $module->data = preg_replace('/\\\\\\/storage\\\\\\/app\\\\\\/igvs\\\\\\/module-content\\\\\\/(\\d+)\\\\\\/(\\d+)\\\\\\//', '', $module->data);

            if (is_file($dest . '/' .'moduleData.js'))
                unlink($dest . '/' .'moduleData.js');

            $moduleData = 'var moduleData = ' . $module->data . ';';
            file_put_contents($dest . '/' .'moduleData.js', $moduleData);

            $version = $module->getModuleVersion();

            //  index
            $index_text = file_get_contents($module->getPathModule() . '/index.html');
            $index_text = str_replace([
                '<!DOCTYPE html>',
                'content/moduleData.js',
                'themes/_default/css/style.css',
                'css/style.css',
                'js/main.js',
                'img/preloader.gif',
            ], [
                '<!DOCTYPE html>
                        <base href=\''. $module->code .'/\'>',
                'moduleData.js',
                '../__modules/' . $module_name . '/' . $version . '/' . $module_theme . '/style.css',
                '../__modules/' . $module_name . '/' . $version . '/' . $module_theme . '/style.css',
                '../__modules/' . $module_name . '/' . $version . '/js/main.js',
                '../__modules/' . $module_name . '/' . $version . '/img/preloader.gif',
            ], $index_text);
            file_put_contents($base_path . '/' . $module->code .'.htm', $index_text);

            $return .= '- ' . $module->name . '<br>';
        }

        return $return;
    }

    public function prepareForFtpBeta($courseId, $topicId = null, $theme = '_default', $language, $show_module_start_screen, $folder_name = null, $return_path_flag = false, $buildId = null, $create_db = true, $options = [])
    {
        $tmp_dir_name = time() . '_' . rand(0,9999). '_' . rand(0,9999);

        $base_path = Config::get('igvs.courses::prepareForFtpBeta.basePath') . '/' . $tmp_dir_name;
        $beta_path = Config::get('igvs.courses::prepareForFtpBeta.betaPath');
        $beta_url = Config::get('igvs.courses::prepareForFtpBeta.betaUrl');
        $src_content = Config::get('igvs.courses::modulesContent.path');

        $replace_browsed = isset($options['replace_browsed']) ? $options['replace_browsed'] : false;

        $return = 'Beta exported modules:<br>';

        $course = CourseItem::find($courseId);
        $course_pic = $course->logo && file_exists(Config::get('igvs.courses::sitePath') . $course->logo->getPath()) ? $course->logo->getPath() : '';
        $bg_hat = $course->bg_hat && file_exists(Config::get('igvs.courses::sitePath') . $course->bg_hat->getPath()) ? $course->bg_hat->getPath() : '';

        $colontitle_logo = '';
        $colontitle_logo_name = '';
        if ($course->colontitle_logo) {
            $colontitle_logo_name = $course->colontitle_logo->getPath();
            $colontitle_logo_name = substr($colontitle_logo_name, strrpos($colontitle_logo_name, '/'), strlen($colontitle_logo_name));
            $colontitle_logo = "<img src=\"../img/{$colontitle_logo_name}\"/>";
        }

        $colontitle_slogan = (string)$course->colontitle_slogan;


        if (is_dir($base_path)) {
            $this->rmdir_recurse($base_path);
        }

        mkdir($base_path);

        $categories = Category::where('parent_id', $topicId);
        if (!is_null($topicId) && strlen($topicId)) {
            $categories->whereRaw('parent_id = ? or id = ?', [$topicId, $topicId]);
        }

        $categories->orderBy('sort');
        $categories = $categories->get();

        $modules = ModuleContent::where('course_id', $courseId);
        if (!is_null($topicId) && strlen($topicId)) {
            $modules = $modules->whereIn('category_id', $categories->lists('id'));
        }
        if (!is_null($buildId) && strlen($buildId)) {
            $modules = $modules->whereHas('builds', function (Builder $q) use ($buildId) {
                $q->where('build_id', $buildId);
            });
        }
        $modules = $modules->orderBy('sort');

        $modules_ids = clone $modules;
        $modules_ids = $modules_ids->lists('id');

        $modules = $modules->get();

        $act_ids = $modules->lists('category_id');
        $topics_codes = [];
        $return_path = [];

        $html_markup_id = Module::where('path', 'html-markup')->first();
        $html_markup_id = $html_markup_id ? $html_markup_id->id : 0;

        $counter = [];
        $topic_ids = Category::where('course_id', $courseId)
            ->where('is_separate', 1)
            ->lists('id');

        foreach ($modules as $module) {
            if ($module->status && $module->status->block_export)
                continue;

            $act = Category::where('id', (int)$module->category_id)->first();
            if (!$act)
                continue;
            $topic = Category::where('id', (int)$act->parent_id)->first();

            $topic_ids[] = $topic->id;

            /* ==== translit ==== */
            $topic_dir = Translit::translit($topic->code, ['remove_special_symbol' => true]);
            /* ==== translit ==== */

            /* ==== translit ==== */
            $module_code_translit = Translit::translit($module->code, ['remove_special_symbol' => true]);
            $module_dir = substr("{$module_code_translit}", 0, 70);

            $counter[$module_dir] = isset($counter[$module_dir]) ? $counter[$module_dir]+1 : 1;
            $module_dir = $counter[$module_dir] > 1 ? "$module_dir-$counter[$module_dir]" : $module_dir;
            /* ==== translit ==== */

            $dir_name = !is_null($folder_name) && strlen($folder_name) ? strtolower($topic_dir) . strtolower($folder_name) : strtolower($topic_dir);

            $topics_codes[$topic->id] = $dir_name;
            $return_path[] = [
                'course_dir'        => $course->code,
                'topic_dir'         => $topic ? $topic_dir : 'none',
                'module_code'       => $module_dir,
                'module_path'       => "{$base_path}/{$dir_name}",
                'module_dir'        => $dir_name,
            ];

            if (!is_dir("{$base_path}/{$dir_name}")) {
                mkdir("{$base_path}/{$dir_name}");
            }

            //data
            //  modules data
            $src = "{$src_content}/{$module->course_id}/{$module->id}";
            $dest = "{$base_path}/{$dir_name}/{$module_dir}";

            mkdir($dest);

            // copy template module
            $this->recourceCopy($module->getPathModule(), $dest, null, null, true);

            // remove demo-data from template module
            if (is_dir($dest . '/content')) {
                $this->rmdir_recurse($dest . '/content');
            }

            mkdir($dest . '/content');

            //replace browsed to passed
            $main_js = ($replace_browsed && is_file($dest . '/js/main.js')) == 'true' ? file_get_contents($dest . '/js/main.js') : false;
            if ($main_js != false) {
                $main_js = str_replace(
                    '||("multitest"===scormType?doLMSSetValue("cmi.core.lesson_status","incomplete"):"theory"===scormType&&doLMSSetValue("cmi.core.lesson_status","browsed")',
                    '||("multitest"===scormType?doLMSSetValue("cmi.core.lesson_status","incomplete"):"theory"===scormType&&doLMSSetValue("cmi.core.lesson_status","completed")',
                    $main_js
                );
                file_put_contents($dest . '/js/main.js', $main_js);
            }

            // copy colontitle logo
            if (!is_dir($dest . '/img')) {
                mkdir($dest . '/img');
            }

            if ($course->colontitle_logo) {
                copy(Config::get('igvs.courses::sitePath') . $course->colontitle_logo->getPath(), "{$dest}/img/" . $colontitle_logo_name);
            }

            // making and copy moduleData.js
            $obj = json_decode($module->data);
            if (is_object($obj)) {
                $obj->colontitle = $module->name;
                $obj->templateLanguage = $language;
                $obj->selected_theme = $theme == 'all' ? $course->theme : $theme;
                $obj->view_all_themes = boolval($theme == 'all');
                $obj->show_start_screen = ((int)($show_module_start_screen === 'true') + (int)$module->startscreen_show) > 0;
                $obj->trainingUrl = $module->link_to_theory_module_form ? $module->link_to_theory_module_form->code : '';
                $obj->behavior = $module->behavior;
                $obj->logo = (strlen($colontitle_logo_name) ? "../img/{$colontitle_logo_name}" : "");
                $obj->slogan = (string)$course->colontitle_slogan;

                $obj->startscreen = [
                    'unit_title' => $module->getStartScreenData('unit_title'),
                    'session_title' => $module->getStartScreenData('session_title'),
                    'qcf_ref' => $module->getStartScreenData('qcf_ref'),
                    'etracker' => $module->getStartScreenData('etracker'),
                    'nos_ref' => $module->getStartScreenData('nos_ref'),
                    'awarding_body_code' => $module->getStartScreenData('awarding_body_code'),
                    'level' => $module->getStartScreenData('level'),
                    'credits' => $module->getStartScreenData('credits'),
                    'qualification_title' => $module->getStartScreenData('qualification_title'),
                    'learning_outcome' => $module->getStartScreenData('learning_outcome'),
                    'functional_skills' => $module->getStartScreenData('functional_skills'),
                    'publisher_note' => $module->getStartScreenData('publisher_note'),
                ];

                $obj->practice = $module->isPractice();

                //manage colontitle postfix
                $practice_index_title = '';
                $practice_assessment_title = ' (Assessment)';

                if ($module->isPractice()
                    && $module->practice_title_manage === '1') {
                    $practice_index_title = ' ' . $module->practice_index_title;
                    $practice_assessment_title = ' ' . $module->practice_assessment_title;
                }

                $obj->practice_index_title = $practice_index_title;
                $obj->practice_assessment_title = $practice_assessment_title;

                $module->data = json_encode($obj);
            }

            $module->data = preg_replace('/\\\\\\/storage\\\\\\/app\\\\\\/igvs\\\\\\/module-content\\\\\\/(\\d+)\\\\\\/(\\d+)\\\\\\//', '', $module->data);

            if (is_file($dest . '/' .'moduleData.js'))
                unlink($dest . '/' .'moduleData.js');

            $moduleData = 'var moduleData = ' . $module->data . ';';
            //записываем moduleData в конце цикла,после переноса файлов

            //  making and copy index file
            if ($theme == 'all') {
                $module_theme = is_dir($dest . '/themes/_default') ? 'themes/_default/css' : 'css';
            } else {
                if (is_dir($dest . '/themes/' . $theme) && strlen($theme)) {
                    $module_theme = 'themes/' . $theme . '/css';
                } else {
                    $module_theme = is_dir($dest . '/themes/_default') ? 'themes/_default/css' : 'css';
                }

            }

            $themes = ModuleTheme::select('name')
                ->where('module_id', $module->module_id)
                ->where('version', $module->getModuleVersion())
                ->distinct()
                ->lists('name');

            $additional_themes = [];
            if ($theme == 'all') {
                foreach ($themes as $themes_value) {
                    if ($themes_value == '_default') {
                        continue;
                    }
                    $theme_exist = is_dir($dest . '/themes/' . $themes_value) && strlen($themes_value);
                    if (!$theme_exist) {
                        continue;
                    }
                    $additional_themes[] = "<link rel=stylesheet href='themes/{$themes_value}/css/style.css' data-theme='{$themes_value}'>";
                }
            }

            //регулирование отображения номеров страниц в модулях html-markup
            $number_page_visible_css = '';
            if ($course->enable_html_markup_number_page !== '1') {
                $number_page_visible_css = '<style>.number-page { display: none; }</style>';
            }

            if ($html_markup_id != $module->module_id) {
                $index_text = file_get_contents($module->getPathModule() . '/index.html');
                $index_text = preg_replace(["/<link rel=('|\"|)stylesheet/iUum"], "<link data-theme='_default' rel=$1stylesheet", $index_text, -1, $count);
                $index_text = str_replace([
//                    '<head>',
                    ], [
//                    '<head><base href="content/">' . implode($additional_themes, "\r\n"),
                ], $index_text);
            } else {
                $index_text = str_replace([
                    '../../interface/js/content.js',
                    '</body>',
                ], [
                    '../interface/js/content2.js',
                    $number_page_visible_css . '</body>',
                ], $module->content);

//                $index_text = preg_replace('/target_id="ebook(.*)"/i', "target_id=\"ebook$1/index\"", $index_text);
            }

            $pattern = "/([^\/])(img|content|js|css)\/([a-zA-Z0-9-_]+?)\.(svg|gif|png|js|css)/iUum";
            $replace = "$1../$2/$3.$4";
            $index_text = preg_replace($pattern, $replace, $index_text);

            $index_text = str_replace([
                '../content/moduleData.js',
                '=themes/_default/css/style.css',
                '=css/style.css',
                '{{logo}}',
                '{{slogan}}',
                '<head>',
                '../../interface',
                "/storage/app/igvs/module-content/{$courseId}/{$module->id}/",
                '<script src=\"../../interface/js/content2.js\"></script>',
                '</body>'
            ], [
                'moduleData.js',
                '=' . $module_theme . '/style.css',
                '=' . $module_theme . '/style.css',
                $colontitle_logo,
                $colontitle_slogan,
                '<head><base href="content/">' . implode($additional_themes, "\r\n"),
                '../interface',
                '',
                '',
                '<script src=\"../../interface/js/content2.js\"></script></body>',
            ], $index_text);

            $index_text = preg_replace("/(themes\/.+\/.+\/.*)\.(css|jpg|gif|png)/iUum", "../$1.$2", $index_text);
            $index_text = str_replace([
                'js/main.js',
                'css/style.css',
                'content/moduleData.js'
            ], [
                'js/main.js?ac=' . time(),
                'css/style.css?ac=' . time(),
                'content/moduleData.js?ac=' . time()
            ], $index_text);

            if ($module->behavior != 'practice' || ($module->behavior == 'practice' && $module->switch_practice_mode != 2))
                file_put_contents($dest . '/index.html', $index_text);

            //  making and copy assessment file, if exist
            if (is_file($module->getPathModule() . '/assessment.html') && $module->behavior == 'practice' && $module->switch_practice_mode != 1) {
                $index_text = file_get_contents($module->getPathModule() . '/assessment.html');
                $index_text = preg_replace(["/<link rel=('|\"|)stylesheet/iUum"], "<link data-theme='_default' rel=$1stylesheet", $index_text, -1, $count);

                $pattern = "/([^\/])(img|content|js|css)\/([a-zA-Z0-9-_]+?)\.(svg|gif|png|js|css)/iUum";
                $replace = "$1../$2/$3.$4";
                $index_text = preg_replace($pattern, $replace, $index_text);

                    $index_text = str_replace([
                        '../content/moduleData.js',
                        '=themes/_default/css/style.css',
                        '=css/style.css',
                        '<head>',
                        '{{logo}}',
                        '{{slogan}}',
                    ], [
                        'moduleData.js',
                        '=' . $module_theme . '/style.css',
                        '=' . $module_theme . '/style.css',
                        '<head><base href="content/">' . implode($additional_themes, "\r\n"),
                        $colontitle_logo,
                        $colontitle_slogan,
                    ], $index_text);

                $index_text = preg_replace("/(themes\/.+\/.+\/.*)\.(css|jpg|gif|png)/iUum", "../$1.$2", $index_text);
                $index_text = str_replace([
                    'js/main.js',
                    'css/style.css',
                    'moduleData.js'
                ], [
                    'js/main.js?ac=' . time(),
                    'css/style.css?ac=' . time(),
                    'moduleData.js?ac=' . time()
                ], $index_text);

                file_put_contents($dest . '/assessment.html', $index_text);
            }

            if ($html_markup_id == $module->module_id) {
                $right_files_list = array_merge($this->getFileList($index_text), ['design/*', 'header.png', 'common/pict/*']);
            }
            else {
                $right_files_list = [];
            }
            $this->recourceCopy($src, $dest . '/content', null, $right_files_list);

            // copy content module
            file_put_contents($dest . '/content/' . 'moduleData.js', $moduleData);

            $return .= "- " . $module->name . " (<a href=\"{$beta_url}{$dir_name}\" target=\"_blank\">{$beta_url}{$dir_name}</a>)<br>";
        }


        $categories = Category::whereIn('id', $topic_ids)
                        ->orderBy('sort')
                        ->get();

        if ($create_db) {
            foreach ($categories as $cat_key => $cat_val) {
                $dir_name = !is_null($folder_name) && strlen($folder_name) ? strtolower($cat_val->code) . strtolower($folder_name) : strtolower($cat_val->code);

                if (is_null($cat_val->parent_id)) {

                    $eapp = [];

                    $categories_topic = Category::where('parent_id', $cat_val->id)
                        ->where('id', $act_ids)
                        ->orderBy('sort')
                        ->get();

                    foreach ($categories_topic as $cat_top_key => $cat_top_val) {

                        //      add to eapp
                        $eapp[] = [
                            "type" => "group",
                            "number" => $cat_top_val->sort,
                            "resource" => "",
                            "id" => $cat_top_val->id,
                            "original_id" => $cat_top_val->copy_original_id,
                            "class" => "",
                            "textName" => "",
                            "parent" => '0',
                            "textTitle" => str_replace('||', '. ', $cat_top_val->name),
                            "section" => "eapp",
                            "preview" => "",
                            "code" => $cat_top_val->code
                        ];

                        $eapp = array_merge($eapp, $this->make_js_db($courseId, $cat_top_val, $modules_ids, null, $show_module_start_screen));
                        // ---- -- -- -- -- //

                        $json = file_get_contents(Config::get('igvs.courses::resourcesPath') . '/export-beta/js_db.json');
                        $json = str_replace([
                            '{{dir}}',
                            '{{title}}',
                            '{{eapp}}',
                            '{{course_id}}',
                            '{{course_code}}',
                            '{{bg_hat}}',
                        ], [
                            $cat_top_val->code,
                            $cat_val->name,
                            json_encode($eapp),
                            $courseId,
                            $course->getCourseCodeClean(),
                            strlen($bg_hat) ? 'design/bg_hat.png' : '',
                        ], $json);

                        if (is_dir("{$base_path}/{$dir_name}/db")) {
                            $this->rmdir_recurse("{$base_path}/{$dir_name}/db");
                        }

                        mkdir("{$base_path}/{$dir_name}/db");

                        file_put_contents("{$base_path}/{$dir_name}/db/js_db.json", $json);
                    }
                }
            }
        }


        foreach ($topics_codes as $code) {
            // copy course image
            mkdir("{$base_path}/{$code}/design");
            if (strlen($course_pic)) {
                copy(Config::get('igvs.courses::sitePath') . $course_pic, "{$base_path}/{$code}/design/preview.png");
            }
            if (strlen($bg_hat)) {
                copy(Config::get('igvs.courses::sitePath') . $bg_hat, "{$base_path}/{$code}/design/bg_hat.png");
            }
        }


        if ($return_path_flag) {
            return $return_path;
        }

        // copy compiled topic to beta
        foreach ($topics_codes as $code) {
            // remove topics dirs from beta
            if (is_dir("{$beta_path}/{$code}")) {
                $this->rmdir_recurse("{$beta_path}/{$code}");
            }

            $this->recourceCopy("{$base_path}/{$code}", "{$beta_path}/{$code}");
        }

        return $return;
    }

    public function make_js_db($course_id, $category_in = null, $modules_id = null, $topic_code_module_prifix = null, $show_module_start_screen = 0)
    {
        $modules = ModuleContent::where('course_id', $course_id);
        if (!is_null($category_in)) {
            $modules = $modules->where('category_id', $category_in->id);
        }
        if (!is_null($modules_id)) {
            $modules = $modules->whereIn('id', $modules_id);
        }
        $modules = $modules->orderBy('sort');
        $modules = $modules->get();

        $counter = [];
        $eapp = [];

        $module_sort = 1;

        foreach ($modules as $mod_key => $mod_val) {
            if ($mod_val->status && $mod_val->status->block_export)
                continue;

            $class = $mod_val->behavior == 'practice' ? 'module practice' : $mod_val->getClassName();

            $category = $category_in ? $category_in : $mod_val->category;
            if (!$category)
                continue;
            /* ==== translit ==== */
            $module_code_translit = Translit::translit($mod_val->code, ['remove_special_symbol' => true]);
            $module_dir = substr("{$module_code_translit}", 0, 70);

            $counter[$module_dir] = isset($counter[$module_dir]) ? $counter[$module_dir]+1 : 1;
            $module_dir = $counter[$module_dir] > 1 ? "$module_dir-$counter[$module_dir]" : $module_dir;
            /* ==== translit ==== */

            if ($mod_val->behavior != 'practice' || ($mod_val->behavior == 'practice' && $mod_val->switch_practice_mode != 2))
            $eapp[] = [
                "type" => "item",
                "number" => $module_sort,
                "resource" => (!is_null($topic_code_module_prifix) ? $topic_code_module_prifix . '/' : '') . $module_dir . '/index',
                "id" => $mod_val->id,
                "original_id" => $mod_val->copy_original_id,
                "class" => $class,
                "behavior" => $mod_val->behavior,
                "textName" => "",
                "parent" => $mod_val->category_id,
                "textTitle" => str_replace('||', '. ', $mod_val->name) . ($mod_val->behavior == 'practice' && $mod_val->practice_title_manage === '1' ? ' ' . $mod_val->practice_index_title : ''),
                "section" => "eapp",
                "preview" => "",
                "code" => $module_dir,
                "template_code" => $mod_val->module->path,
                "startscreen" => [
                    'unit_title' => $mod_val->getStartScreenData('unit_title'),
                    'session_title' => $mod_val->getStartScreenData('session_title'),
                    'qcf_ref' => $mod_val->getStartScreenData('qcf_ref'),
                    'etracker' => $mod_val->getStartScreenData('etracker'),
                    'nos_ref' => $mod_val->getStartScreenData('nos_ref'),
                    'awarding_body_code' => $mod_val->getStartScreenData('awarding_body_code'),
                    'level' => $mod_val->getStartScreenData('level'),
                    'credits' => $mod_val->getStartScreenData('credits'),
                    'qualification_title' => $mod_val->getStartScreenData('qualification_title'),
                    'learning_outcome' => $mod_val->getStartScreenData('learning_outcome'),
                    'functional_skills' => $mod_val->getStartScreenData('functional_skills'),
                    'publisher_note' => $mod_val->getStartScreenData('publisher_note'),
                ],
                "show_start_screen" => ((int)(in_array($show_module_start_screen,['true', 1], true)) + (int)$mod_val->startscreen_show) > 0,
            ];

            if (is_file($mod_val->getPathModule() . '/assessment.html') && $mod_val->behavior == 'practice' && $mod_val->switch_practice_mode != 1) {
                $class = $mod_val->behavior == 'practice' ? 'module control' : $mod_val->getClassName();

                $module_sort++;

                $eapp[] = [
                    "type" => "item",
                    "number" => $module_sort,
                    "resource" => (!is_null($topic_code_module_prifix) ? $topic_code_module_prifix . '/' : '') . $module_dir . '/assessment',
                    "id" => $mod_val->id,
                    "original_id" => $mod_val->copy_original_id,
                    "class" => $class,
                    "behavior" => $mod_val->behavior,
                    "textName" => "",
                    "parent" => $mod_val->category_id,
                    "textTitle" => str_replace('||', '. ', $mod_val->name) . ' ' .  ($mod_val->practice_title_manage === '1' ? $mod_val->practice_assessment_title : '(Assessment)'),
                    "section" => "eapp",
                    "preview" => "",
                    "code" => $module_dir,
                    "template_code" => $mod_val->module->path
                ];
            }

            $module_sort++;
        }

        return $eapp;
    }

    public function make_vocabulary($course_id, $releaseCloudDir = '')
    {
        if (!$course_id || is_null($course_id))
            return [];

        $result = Vocabulary::where('course_id', $course_id)
            ->orderBy('term_clean')
            ->get();

        if (!$result) {
            return [];
        }

        $copy_files = (boolean)strlen($releaseCloudDir);
        $vocabulary_files_dir = "{$releaseCloudDir}/vocabulary_files";

        if ($copy_files) {
            mkdir($vocabulary_files_dir);
        }

        $return = [];
        foreach ($result as $item) {
            $image_file_path = $item->image ? $item->image->getLocalPath() : '';
            $image_filename = $item->image ? $item->image->id . '_' . $item->image->getFilename() : '';
            $image_file_exost = $item->image && is_file($image_file_path);

            if ($copy_files && $image_file_exost) {
                copy($image_file_path, $vocabulary_files_dir . '/' . $image_filename);
            }

            $return[] = [
                'term' => $item->term,
                'description' => $item->description,
                'image' => $image_filename
            ];
        }

        return $return;
    }

    public function sqlite_rubricator_insert($db_name, $params)
    {
        $return_id = 0;

        $query = "insert into navigation (`number`, `text`, `parent`, `section`, `type`, `resource`, `desc`, `class`) values (?, ?, ?, ?, ?, ?, ?, ?)";
        $result_act = DB::connection($db_name)->insert($query, [
            $params['number'],
            $params['text'],
            $params['parent'],
            $params['section'],
            $params['type'],
            $params['resource'],
            $params['desc'],
            $params['class'],
        ]);

        if ($result_act) {
            $query = "select max(id) as id from navigation";
            $return = DB::connection($db_name)->select($query);
            foreach ($return as $return_el) {
                $return_id = $return_el->id;
                break;
            }
        }

        return $return_id;
    }

    public function sqlite_vocabulary_insert($db_name, $params)
    {
        $return_id = 0;

        $query = "insert into vocabulary (`term`, `description`) values (?, ?)";
        $result_act = DB::connection($db_name)->insert($query, [
            $params['term'],
            $params['description'],
        ]);

        if ($result_act) {
            $query = "select max(id) as id from vocabulary";
            $return = DB::connection($db_name)->select($query);
            foreach ($return as $return_el) {
                $return_id = $return_el->id;
                break;
            }
        }

        return $return_id;
    }

    public function rmdir_recurse($path) {
        if (!is_dir($path))
            return false;

        $path = rtrim($path, '/').'/';
        $handle = opendir($path);
        while(false !== ($file = readdir($handle))) {
            if($file != '.' and $file != '..' ) {
                $fullpath = $path.$file;
                if(is_dir($fullpath)) $this->rmdir_recurse($fullpath); else unlink($fullpath);
            }
        }
        closedir($handle);
        rmdir($path);
    }
}
