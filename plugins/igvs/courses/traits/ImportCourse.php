<?php namespace Igvs\Courses\Traits;

use Config;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\BackgroundTask;
use Igvs\Courses\Models\LogFile;

trait ImportCourse
{
    public function shortImportCourse($options, $job, $db_id)
    {
        try {
            return $this->importCourse(
                $options['inputFileName'],
                $options['courseId'],
                $job,
                $db_id,
                $options
            );
        } catch(\Exception $e) {
            BackgroundTask::log_error('Import Course', 'ImportCourse', json_encode($options), $e, $db_id);

        }
    }

    public function importCourse($filename, $course_id, $job, $db_id, $options)
    {
        $import_tmp = Config::get('igvs.courses::zip.tmp') . '/' . time()  . '_' . microtime() . '_' . rand(0,100);

        if (!file_exists($import_tmp))
            mkdir($import_tmp, 0777, true);
        else
            throw new \Exception("tmp folder already exist \"{$import_tmp}\"");

        $exec_return = exec('tar -xf ' . $filename . ' -C "' . $import_tmp . "\"");

        if (strlen(trim($exec_return)))
            throw new \Exception("unpack error {$exec_return}");

        if (!is_file($import_tmp . '/db/js_db.json')) {
            new \Exception('js_db.json not founded');
            return 'js_db.json not founded';
        }

        $js_db = json_decode(file_get_contents($import_tmp . '/db/js_db.json'));

        $errors = [];

        usort($js_db->nav->eapp, function($a, $b) {
            $sort_a = (int)$a->number;
            $sort_b = (int)$b->number;

            if ($sort_a > $sort_b)
                return 1;
            elseif ($sort_a < $sort_b)
                return -1;
            else {
                $a_id = (int)$a->id;
                $b_id = (int)$b->id;

                if ($a_id > $b_id)
                    return 1;
                elseif ($a_id < $b_id)
                    return -1;
                else {
                    if ($a->class == 'module practice' && $b->class == 'module control') {
                        return 1;
                    } elseif ($a->class == 'module control' && $b->class == 'module practice') {
                        return -1;
                    }
                }

                return 0;
            }
        });

        foreach ($js_db->nav->eapp as $js_db_eapp_item_topic) {
            $js_db_eapp_item_topic = (array)$js_db_eapp_item_topic;

            if ($js_db_eapp_item_topic['type'] != 'separator')
                continue;

            //добавляем топики
            $category = new Category();
            $category->course_id = $course_id;
            $category->code = $this->getUniqueFieldValue(new Category(), $js_db_eapp_item_topic['code'], ['course_id' => $course_id]);
            $category->name = $js_db_eapp_item_topic['textTitle'];
            $category->save();

            foreach ($js_db->nav->eapp as $js_db_eapp_item_act) {
                $js_db_eapp_item_act = (array)$js_db_eapp_item_act;

                if ($js_db_eapp_item_act['type'] != 'group' || $js_db_eapp_item_act['parent'] != $js_db_eapp_item_topic['id'])
                    continue;

                $category_act = new Category();
                $category_act->course_id = $course_id;
                $category_act->parent_id = $category->id;
                $category_act->code = $this->getUniqueFieldValue(new Category(), $js_db_eapp_item_act['code'], ['course_id' => $course_id]);
                $category_act->name = $js_db_eapp_item_act['textTitle'];
                $category_act->save();

                $sort = 1;

                foreach ($js_db->nav->eapp as $js_db_eapp_item_item) {
                    $js_db_eapp_item_item = (array)$js_db_eapp_item_item;

                    if ($js_db_eapp_item_item['type'] != 'item' || $js_db_eapp_item_item['parent'] != $js_db_eapp_item_act['id'])
                        continue;

                    $module_template = Module::where('path', $js_db_eapp_item_item['template_code'])
                        ->first();

                    $matches = [];
                    preg_match('/(.+)\/index/', $js_db_eapp_item_item['resource'], $matches);

                    if (!isset($matches[1])) {
                        continue;
                    }

                    if ($module_template) {
                        $module_template_id = $module_template->id;
                        $module_template_version = $module_template->getLatestVersion();
                    } else {
                        new \Exception("Module template {$js_db_eapp_item_item['template_code']} is not found");
                        $errors[] = "Module template <b>{$js_db_eapp_item_item['template_code']}</b> is not found.<br>";
                        continue;
                    }

                    try {

                        $resourse = $matches[1];

                        $data = file_get_contents($import_tmp . '/' . $resourse . '/content/moduleData.js');
                        $data = str_replace('var moduleData = ', '', $data);
                        $data = trim($data);
                        $data = substr($data, 0, strlen($data)-1);

                    } catch (\Exception $e) {
                        echo $e->getMessage();
                        echo 'item: ' . print_r($js_db_eapp_item_item, true);

                        $data = '';
                    }

                    $moduleContent = new ModuleContent();
                    $moduleContent->course_id = $course_id;
                    $moduleContent->category_id = $category_act->id;
                    $moduleContent->name = trim($js_db_eapp_item_item['textTitle']);
                    $moduleContent->module_id = $module_template_id;
                    $moduleContent->behavior = $js_db_eapp_item_item['behavior'];
                    $moduleContent->version = $module_template_version;
                    $moduleContent->code = $this->getUniqueFieldValue(new ModuleContent(), $js_db_eapp_item_item['code'], ['course_id' => $course_id]);
                    $moduleContent->data = $data;
                    $moduleContent->sort = ($sort++);
                    $moduleContent->init_by_tpl = 1;
                    $moduleContent->save();

                    $storage = $moduleContent->getPathContents();

                    if (file_exists($import_tmp . '/' . $resourse . '/content')) {
                        $this->copyFilesToStorage($import_tmp . '/' . $resourse . '/content', $storage, $moduleContent);
                    } else {
                        $this->warn("resources \"{$import_tmp}/{$resourse}/content\" not found!");
                    }

                    if (file_exists($import_tmp . '/' . $resourse . "/design")) {
                        $this->copyFilesToStorage($import_tmp . '/' . $resourse . "/design", $storage . '/design', $moduleContent);
                    }
                }
            }
        }

        BackgroundTask::job_plus($job, 1, 1, $db_id);

        $job->delete();

        return $errors;
    }

    private function getUniqueFieldValue($model_name, $value, $where_cloud = [], $field = 'code')
    {
        if (!$value || !strlen($value) || $field == 'code' && strlen($value) < 3)
            $value = uniqid();

        $value_origin = $value;
        $where_cloud_origin = $where_cloud;
        $iterator = 1;

        $where_cloud = array_merge($where_cloud_origin, [
            $field => $value
        ]);

        $model = new $model_name();
        foreach ($where_cloud as $wc_k => $wc_v)
        {
            $model = $model->where($wc_k, $wc_v);
        }

        while ($model->first()) {
            $value = $value_origin . '(u-' . $iterator . ')';

            $where_cloud = array_merge($where_cloud_origin, [
                $field => $value
            ]);

            $iterator++;

            $model = new $model_name();
            foreach ($where_cloud as $wc_k => $wc_v)
            {
                $model = $model->where($wc_k, $wc_v);
            }
        }

        return $value;
    }

    private function copyFilesToStorage($path, $storage, $module, $right_files_list = null)
    {

        foreach(new \RecursiveIteratorIterator (new \RecursiveDirectoryIterator ($path)) as $file)  {

            $file_name = $file->getFilename();

            // создаем папки
            $dir = str_replace($path, $storage, $file->getPath());
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }

            if ($file->isDir()) {
                continue;
            }

            if (!is_null($right_files_list) && !$this->is_right_file($file->getRealPath(), $right_files_list))
                continue;

            LogFile::refactorFile($storage, $file_name, $module->id, 0);

            // вычисление имени файла по has-1
            $storage_file = ModuleContent::saveInStorage($file->getRealPath());

            // создаем линк на файл в module-content
            if (!is_dir($storage)) {
                mkdir($storage, 0777, true);
            }

            if (!is_file($dir.'/'.$file_name)) {
                link($storage_file, $dir.'/'.$file_name);
            }

        }
    }
}
