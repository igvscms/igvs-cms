<?php namespace Igvs\Courses\Traits;

use Igvs\Courses\Models\Member;
use Igvs\Courses\Models\MemberRole;
use Igvs\Courses\Models\Operation;
use Igvs\Courses\Models\User;
use Igvs\Courses\Models\Course;
use BackendAuth;
use DB;
use Session;
/*
    // example
    if (!$this->checkAssessCourse($course->id))
        return Response::make(View::make('backend::access_denied'), 403);
*/

trait CheckAccess
{
    public function checkAccessCourse($course_id)
    {
        if (!$user = BackendAuth::getUser()) {
            return false;
        }
        if ($user->is_superuser) {
            return true;
        }

        $members_id = Member::getMemberFew($course_id, $user->id);

        if (count($members_id)) {
            return count(MemberRole::getMemberFewRoles($members_id));
        }

        return false;
    }

    public function getPermissionsCourse($course_id, $operation = null)
    {
        if (!$user = BackendAuth::getUser()) {
            return [];
        }
        if (is_null($operation)) {
            $operation = Operation::lists('code');
        }
        else {
            $operation = array_values((array) $operation);
        }
        if ($user->is_superuser) {
            return $operation;
        }
        if (!$member = Member::getByCourse($course_id)) {
            return [];
        }
        if ($member->roles()->where('is_superuser', 1)->limit(1)->count()) {
            return $operation;
        }
        $sql = "
            SELECT
                o.code
            FROM igvs_courses_courses c
            JOIN igvs_courses_members m
                ON m.course_id = c.id AND m.user_id = :user_id
            JOIN igvs_courses_members_roles mr
                ON mr.member_id = m.id
            JOIN igvs_courses_role_operations ro
                ON ro.role_id = mr.role_id
            JOIN igvs_courses_operations o
                ON o.id = ro.operation_id
            WHERE
                c.id = :course_id
            and
                o.code in (:operation_" . implode(",:operation_", array_keys($operation)) . ")
            GROUP BY o.code";

        $ops = DB::select($sql, array_merge([$user->id, $course_id], $operation));
        $result = [];
        foreach ($ops as $op) {
            $result[] = $op->code;
        }
        return $result;
    }

    public function checkAccessOperation($course_id, $operation, $default = false)
    {
        if (!$user = BackendAuth::getUser()) {
            return false;
        }
        if ($user->is_superuser) {
            return true;
        }
        $operation = (array) $operation;
        if (!count(Operation::getOperationsIdsByCode($operation))) {
            //если искомых кодов не существует возвращаем значение по умолчанию(false)
            return $default;
        }
        $member = Member::getMembersIdHasRolesByOperations($user->id, $course_id, $operation);

        return !!count($member);
    }

    public function checkAccessRights($course_id, $right)
    {
        if (!$user = BackendAuth::getUser()) {
            return false;
        }
        $ids = Member::getMembersIdHasRolesByRight($user->id, $course_id, $right);

        return !!count($ids);
    }

    /**
     * @param $course_id идентификатор курса
     * @return bool
     * Если курс не копия (copy_original_id есть null || 0),
     * то доступ для редактирования пользователю Pilot запрещён
     */
    public function checkPilotAccess($course_id)
    {
        return !Course::isFirst($course_id) || $this->isCourseAdmin($course_id);
    }

    public function getCourseParentId($course_id)
    {
        return Course::findFirst($course_id);
    }

    public function isCourseAdmin($course_id)
    {
        if (!$user = BackendAuth::getUser()) {
            return false;
        }
        if ($user->is_superuser) {
            return true;
        }
        return $this->checkAccessRights($course_id, 'is_superuser');
    }
}
