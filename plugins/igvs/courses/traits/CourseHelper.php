<?php namespace Igvs\Courses\Traits;


use Igvs\Courses\Models\Build;

trait CourseHelper
{
    private static function getMenuItemName($name)
    {
        return str_replace('||', '. ', $name);
    }

    private static function getBuildsList()
    {
        return Build::orderBy('name', 'asc')->get()->lists('name', 'id');
    }

    private static function getPracticeIndexName($module)
    {
        $name = self::getMenuItemName($module->name);

        if ($module->practice_title_manage === '1') {
            return "{$name} {$module->practice_index_title}";
        }

        return $name;
    }

    private static function getPracticeAssessmentName($module)
    {
        $name = self::getMenuItemName($module->name);

        if ($module->practice_title_manage === '1') {
            return "{$name} {$module->practice_assessment_title}";
        }

        return "{$name} (Assessment)";
    }

    private static function modifyAssessmentCode($code)
    {
        return $code . 'a';
    }
}
