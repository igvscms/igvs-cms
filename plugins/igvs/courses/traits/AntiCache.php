<?php namespace Igvs\Courses\Traits;

use Igvs\Courses\Models\Member;
use Igvs\Courses\Models\MemberRole;
use BackendAuth;
use DB;
use Session;
/*
    // example
    if (!$this->checkAssessCourse($course->id))
        return Response::make(View::make('backend::access_denied'), 403);
*/

trait AntiCache
{
    public function addJs($name, $attributes = [])
    {
        $jsPath = $this->getAssetPath($name);

        $jsPath .= '?' . $this->getCommitHash();

        if (isset($this->controller)) {
            $this->controller->addJs($jsPath, $attributes);
        }

        if (is_string($attributes)) {
            $attributes = ['build' => $attributes];
        }

        $jsPath = $this->getAssetScheme($jsPath);

        if (!in_array($jsPath, $this->assets['js'])) {
            $this->assets['js'][] = ['path' => $jsPath, 'attributes' => $attributes];
        }
    }

    public function addCss($name, $attributes = [])
    {
        $cssPath = $this->getAssetPath($name);

        $cssPath .= '?' . $this->getCommitHash();

        if (isset($this->controller)) {
            $this->controller->addCss($cssPath, $attributes);
        }

        if (is_string($attributes)) {
            $attributes = ['build' => $attributes];
        }

        $cssPath = $this->getAssetScheme($cssPath);

        if (!in_array($cssPath, $this->assets['css'])) {
            $this->assets['css'][] = ['path' => $cssPath, 'attributes' => $attributes];
        }
    }

    public function getCommitHash()
    {
        $filename = __DIR__ . '/../resources/lastcommit.info';

        $this->setCommitHash();

        $file = fopen($filename, 'r');
        $hash = fgets($file);
        fclose($file);

        return $hash;
    }

    public function setCommitHash()
    {
        $filename = __DIR__ . '/../resources/lastcommit.info';

        if ((file_exists($filename) && time() - filemtime($filename) > 2 * 60) || !file_exists($filename)) {
            $cmd = "git -C \"{$_SERVER['DOCUMENT_ROOT']}\" log -1 --pretty=format:\"%h\"";
            $sh = trim(shell_exec($cmd));

            $file = fopen($filename, 'w');
            fwrite($file, $sh);
            fclose($file);
        }
    }

    public function getCommitDate()
    {
        $filename = __DIR__ . '/../resources/lastcommit_date.info';

        $this->setCommitDate();

        $file = fopen($filename, 'r');
        $date = fgets($file);
        fclose($file);

        return $date;
    }

    public function setCommitDate()
    {
        $filename = __DIR__ . '/../resources/lastcommit_date.info';

        if ((file_exists($filename) && time() - filemtime($filename) > 2 * 60) || !file_exists($filename)) {
            $cmd = "git -C \"{$_SERVER['DOCUMENT_ROOT']}\" log -1 --pretty=format:\"%cd\" --date=\"iso8601\"";
            $sh = trim(shell_exec($cmd));

            $file = fopen($filename, 'w');
            fwrite($file, $sh);
            fclose($file);
        }
    }


}