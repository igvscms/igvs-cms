<?php namespace Igvs\Courses\Traits;

use Igvs\Courses\Models\Course;

trait CourseProperty
{
    private static $course_property_courses_is_use_project = [];
    private static $course_property_courses_i_can_see_examination_system = [];

    public function coursePropertyIsUseProject()
    {
        $course_id = $this->course_id;

        if (!isset(self::$course_property_courses_is_use_project[$course_id])) {
            self::$course_property_courses_is_use_project[$course_id] = Course::getCourse($course_id)->isUseProject();
        }

        return self::$course_property_courses_is_use_project[$course_id];
    }

    public function coursePropertyICanSeeExaminationSystem()
    {
        $course_id = $this->course_id;

        if (!isset(self::$course_property_courses_i_can_see_examination_system[$course_id])) {
            self::$course_property_courses_i_can_see_examination_system[$course_id] = Course::getCourse($course_id)->iCanSeeExaminationSystem();
        }

        return self::$course_property_courses_i_can_see_examination_system[$course_id];
    }
}
