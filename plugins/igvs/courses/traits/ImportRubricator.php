<?php namespace Igvs\Courses\Traits;

use Config;
use Igvs\Courses\Components\Course;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\Course as CourseItem;
use Illuminate\Database\Eloquent\Builder;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Module;

use PHPExcel_IOFactory;

trait ImportRubricator
{
    public function importRubricator($filename, $course_id)
    {
        //определяем тип файла
        $inputFileType = PHPExcel_IOFactory::identify($filename);
        //создаём объект ридера
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        //читаем файл
        $objPHPExcel = $objReader->load($filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $aSheet = $objPHPExcel->getActiveSheet();
        //этот массив будет содержать массивы содержащие в себе значения ячеек каждой строки
        $array = array();
        //получим итератор строки и пройдемся по нему циклом

        $return = '';
        foreach($aSheet->getRowIterator() as $row){
            //получим итератор ячеек текущей строки
            $cellIterator = $row->getCellIterator();
            //пройдемся циклом по ячейкам строки
            //этот массив будет содержать значения каждой отдельной строки
            $item = array();
            foreach($cellIterator as $cell){
                //заносим значения ячеек одной строки в отдельный массив
                array_push($item, $cell->getCalculatedValue());
            }
            //заносим массив со значениями ячеек отдельной строки в "общий массив строк"
            array_push($array, $item);
        }

        $current_topic_id = null;
        $current_act_id = null;

        if (count($array) < 2) {
            new \Exception("No data found in rubricator file");
            return "No data found in rubricator file";
        }

        if (!isset($array[0][0]) || strtolower(trim($array[0][0])) != 'topic') {
            new \Exception("Error format data in rubricator file");
            return "Error format data in rubricator file";
        }

        $sort = 0;
        $errors = '';

        //строки
        foreach($array as $row_k => $row_v) {
            if ($row_k == 0)
                continue;

            if (strlen(trim($row_v[0]))) {
                $topic = new Category();
                $topic->course_id = $course_id;
                $topic->code = microtime() + rand(0, 9999);
                $topic->name = trim($row_v[0]);
                $topic->save();

                if ($topic->save()) {
                    $current_topic_id = $topic;
                }
            }

            if (is_null($current_topic_id))
                continue;

            if (strlen(trim($row_v[1]))) {
                $act = new Category();
                $act->course_id = $course_id;
                $act->parent_id = $current_topic_id;
                $act->name = trim($row_v[1]);
                $act->save();

                if ($act->save()) {
                    $current_act_id = $act->id;
                }
            }

            if (is_null($current_act_id))
                continue;

            $module_template_id = 0;
            $module_template_version = '';
            $metadata = null;

            if (!isset($row_v[5])) {
                new \Exception("Module template {$row_v[3]} is not found");
                $errors .= "Module template <b>{$row_v[3]}</b> is not found.<br>";
                continue;
            }

            $module_template = Module::where('path', trim($row_v[5]))
                ->first();

            if ($module_template) {
                $module_template_id = $module_template->id;
                $module_template_version = $module_template->getLatestVersion();
            } else {
                new \Exception("Module template {$row_v[3]} is not found");
                $errors .= "Module template <b>{$row_v[3]}</b> is not found.<br>";
                continue;
            }

            $metadata_path = Config::get('igvs.courses::modulesRelease.path') . "/modules/{$module_template->path}/{$module_template_version}/metadata.json";

            if (is_file($metadata_path)) {
                try {
                    $metadata = json_decode(file_get_contents($metadata_path));
                } catch(\Exception $e) {
                    new \Exception("Metadata module template not found {$metadata_path}.");
                    $errors .= "Metadata module template not found <b>{$metadata_path}</b>.<br>";
                    continue;
                }
            } else {
                new \Exception("Metadata module template not found: {$metadata_path}");
                $errors .= "Metadata module template not found: <b>{$metadata_path}</b><br>";
                continue;
            }

            if (!ModuleContent::where('code', trim($row_v[4]))->first()) {

                $moduleContent = new ModuleContent();
                $moduleContent->course_id = $course_id;
                $moduleContent->category_id = $current_act_id;
                $moduleContent->name = trim($row_v[2]);
                $moduleContent->module_id = $module_template_id;
                $moduleContent->behavior = count($metadata->behaviors_types) > 1 ? 'test' : $metadata->behaviors_types[0];
                $moduleContent->version = $module_template->getLatestVersionAttribute();
                $moduleContent->code = trim($row_v[4]);
                $moduleContent->sort = ($sort++);
                $moduleContent->init_by_tpl = 1;
                $moduleContent->save();
            } else {
                $errors .= "Code <b>{$row_v[4]}</b> already exist in this course.<br>";
                continue;
            }
        }

        return $errors;
    }
}