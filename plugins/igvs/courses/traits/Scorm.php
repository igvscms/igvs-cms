<?php namespace Igvs\Courses\Traits;

use Config;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\Module;
use Illuminate\Database\Eloquent\Builder;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Course;
use Igvs\Courses\Models\Build;
use Igvs\Courses\Helpers\Translit;

trait Scorm
{
    public $html_markup_id          = null;
    public $number_page_visible_css = '';

    public function createScorm($courseId, $buildId, $topicId, $theme = '_default', $language = null, $return_path_flag = false, $iscose = false, $options = [])
    {
        $srcCompleteUnzip   = Config::get('igvs.courses::scorm.tmp');
        $srcCompleteZip     = Config::get('igvs.courses::scorm.arhive');
        $srcContent         = Config::get('igvs.courses::modulesContent.path');
        $srcScormfiles      = Config::get('igvs.courses::scorm.scormfiles');
        $urlArhive          = Config::get('igvs.courses::scorm.url');



        $return_path = [];
        //
        $srcCompleteUnzip = "{$srcCompleteUnzip}/{$courseId}-" . uniqid();

        mkdir($srcCompleteUnzip, 0777, true);

        // readme
        $course = Course::find($courseId);
        $readme[] = "Course: {$course->title}";

        $show_module_start_screen = isset($options['show_module_start_screen']) ? $options['show_module_start_screen'] : $course->show_module_start_screen;
        $replace_browsed = isset($options['replace_browsed']) ? $options['replace_browsed'] : false;

        if (is_null($language)) {
            $language = $course->language;
        }

        if ($course->modification_name)
            $readme[] = "Modification: {$course->modification_name}";

        if ($buildId) {
            $build = Build::find($buildId);
            $readme[] = "Build: {$build->name}";
        }

        $readme[] = "Date: " . date('Y.m.d');

        file_put_contents("{$srcCompleteUnzip}/readme.txt", implode($readme, "\r\n"));
        // /readme

        $stmt = ModuleContent::where('course_id', $courseId);

        if ($buildId) {
            $stmt->whereHas('builds', function (Builder $q) use ($buildId) {
                    $q->where('build_id', $buildId);
            });
        }
        if ($topicId) {
            $stmt->whereHas('category', function (Builder $q) use ($topicId) {
                    $q->whereIn('category_id', Category::where('parent_id', $topicId)->get()->lists('id'));
            });
        }

        $modules = $stmt->get();

        $html_markup_id = Module::where('path', 'html-markup')->first();
        $this->html_markup_id = $html_markup_id ? $html_markup_id->id : 0;

        //регулирование отображения номеров страниц в модулях html-markup
        if ($course->enable_html_markup_number_page !== '1') {
            $this->number_page_visible_css = '<style>.number-page { display: none; }</style>';
        }

        $counter = [];
        foreach ($modules as $module) {
            $module_name_translit = Translit::translit($module->name, ['remove_special_symbol' => true]);
            $module_code_translit = Translit::translit($module->code, ['remove_special_symbol' => true]);
            $module_dir = substr("{$module_code_translit}-{$module_name_translit}", 0, 70);

            $counter[$module_dir] = isset($counter[$module_dir]) ? $counter[$module_dir]+1 : 1;
            $module_dir = $counter[$module_dir] > 1 ? "$module_dir-$counter[$module_dir]" : $module_dir;

            $src = $module->getPathContents(); // "{$srcContent}/{$module->course_id}/{$module->code}";
            $dest = "{$srcCompleteUnzip}/{$module_dir}";
            // $dest = "{$srcCompleteUnzip}/{$module->code}-{$module_name_translit}";

            $filter = function ($file) use ($module){
                return !(is_dir($file)
                     && in_array($file, [$module->getPathModule() . '/content']));
            };

            $topic = Category::where('id', Category::where('id', $module->category_id)->first()->parent_id)->first();

            $this->recourceCopy($module->getPathModule(), $dest, $filter);
            $this->makeTemplate($dest, $theme, $course, $module);
            $this->recourceCopy($src, "{$dest}/content");
            $this->generateModuleData("{$dest}/content", $module, $course, $language, $theme, $show_module_start_screen);
            $this->recourceCopy($srcScormfiles, $dest);

            $main_js = $replace_browsed == 'true' ? file_get_contents($dest . '/js/main.js') : false;
            if ($main_js != false) {
                $main_js = str_replace(
                    '||("multitest"===scormType?doLMSSetValue("cmi.core.lesson_status","incomplete"):"theory"===scormType&&doLMSSetValue("cmi.core.lesson_status","browsed")',
                    '||("multitest"===scormType?doLMSSetValue("cmi.core.lesson_status","incomplete"):"theory"===scormType&&doLMSSetValue("cmi.core.lesson_status","completed")',
                    $main_js
                );
                file_put_contents($dest . '/js/main.js', $main_js);
            }

            if ($module->behavior === 'practice') {
                $this->recourceCopy($dest, "{$dest}_Assessment");
                $this->overwriteManifest("{$dest}_Assessment/imsmanifest.xml", "$module->name (Assessment)");
                unlink("{$dest}/assessment.html");
                unlink("{$dest}_Assessment/index.html");
                rename("{$dest}_Assessment/assessment.html", "{$dest}_Assessment/index.html");
                $this->makeTemplate("{$dest}_Assessment", $theme, $course, $module);
                $this->zip("{$dest}_Assessment", "{$dest}_Assessment.zip", true);

                $return_path[] = [
                    'course_dir'        => $course->code,
                    'topic_dir'         => $topic ? $topic->code : 'none',
                    'module_code'       => $module->code,
                    'module_filename'   => "{$module->code}-{$module_name_translit}_Assessment.zip",
                    'archive_path'      => "{$dest}_Assessment.zip"
                ];
            }

            $this->overwriteManifest("$dest/imsmanifest.xml", $module->name);

            // система скормов почему-то считывает из метадаты версию протолока скорма, приходится заменять
            $this->overwriteTemplateVersion("$dest/metadata.json");

            if ($iscose) {
                $this->overwriteMainJsCose("$dest/js/main.js");
            }

            $this->zip("{$dest}", "{$dest}.zip", true);
            // $this->output->writeln($module->name);

            $return_path[] = [
                'course_dir'        => $course->code,
                'topic_dir'         => $topic ? $topic->code : 'none',
                'module_code'       => $module->code,
                'module_filename'   => "{$module->code}-{$module_name_translit}.zip",
                'archive_path'      => "{$dest}.zip"
            ];
        }

        if ($return_path_flag) {
            return $return_path;
        }

        if (!file_exists($srcCompleteZip))
            mkdir($srcCompleteZip, 0777, true);

        // $this->output->writeln('Zipping...');
        $this->zip("{$srcCompleteUnzip}", "{$srcCompleteZip}/".basename($srcCompleteUnzip).".zip", true);
        // $this->output->writeln("Complete!!!");

        return "{$urlArhive}/" . basename($srcCompleteUnzip).".zip";
    }

    public function makeTemplate($path, $theme, $course, $module)
    {
        if ($this->html_markup_id != $module->module_id) {
            $html = $html = file_get_contents("$path/index.html");
            $html = preg_replace(["/<link rel=('|\"|)stylesheet/iUum"], "<link data-theme='_default' rel=$1stylesheet", $html, -1, $count);
        } else {
            $html = str_replace([
                '../../interface/js/content.js',
                '</body>',
            ], [
                '../interface/js/content2.js',
                $this->number_page_visible_css . '</body>',
            ], $module->content);
        }


        if (is_dir("{$path}/themes")) {

            $theme == 'all' ? $course->theme : $theme;

            if (!is_dir("{$path}/themes/$theme")) {
                $theme = '_default';
            }

            // удаление неактуальных тем
            $array = $this->scandir("$path/themes", [$theme]);
            foreach ($array as $v) {
                if (is_dir("$path/themes/$v"))
                    $this->removeDir("$path/themes/$v");
            }

            // установка стилей
            $html = str_replace("themes/_default/css/style.css", "themes/{$theme}/css/style.css", $html);
        }

        // установка base url
        $html = str_replace("<head>", "<head><base href=\"content/\">", $html);

        // colontitle logo, slogan
        if (is_file("$path/img/logo.png")) {
            unlink("$path/img/logo.png");
        }

        $colontitle_logo = '';
        $colontitle_logo_name = '';

        if ($course->colontitle_logo) {
            $colontitle_logo_name = $course->colontitle_logo->getPath();
            $colontitle_logo_name = substr($colontitle_logo_name, strrpos($colontitle_logo_name, '/'), strlen($colontitle_logo_name));
            $colontitle_logo = "<img src=\"../img/{$colontitle_logo_name}\"/>";


            if (!is_dir($path . '/img')) {
                mkdir($path . '/img');
            }
            copy(Config::get('igvs.courses::sitePath') . $course->colontitle_logo->getPath(), "{$path}/img/" . $colontitle_logo_name);
        }
        $colontitle_slogan = (string)$course->colontitle_slogan;
        $html = str_replace(['{{logo}}', '{{slogan}}'], [$colontitle_logo, $colontitle_slogan], $html);

        $html = str_replace([
            '../content/moduleData.js',
            '{{logo}}',
            '{{slogan}}',
            '../../interface',
            "/storage/app/igvs/module-content/{$course->id}/{$module->id}/",
            '<script src=\"../../interface/js/content2.js\"></script>',
            '</body>'
        ], [
            'moduleData.js',
            $colontitle_logo,
            $colontitle_slogan,
            '../interface',
            '',
            '',
            '<script src=\"../../interface/js/content2.js\"></script></body>',
        ], $html);


        // корректировка url до файлов
        $pattern = "/([^\/])(img|content|js|css)\/([a-zA-Z0-9-_]+?)\.(svg|gif|png|js|css)/iUum";
        $replace = "$1../$2/$3.$4";
        $html = preg_replace($pattern, $replace, $html);
        $html = preg_replace("/(themes\/.+\/.+\/.*)\.(css|jpg|gif|png)/iUum", "../$1.$2", $html);

        file_put_contents("$path/index.html", $html);
    }

    private function prepareTitle ($title)
    {
        $search = ['&amp;', '&nbsp;', '‘', '“'];
        $replace = ['#x26', '#xA0', '#x27', '#x22'];
        return str_replace($search, $replace, $title);
    }

    private function overwriteManifest ($path, $title)
    {
        $text = file_get_contents($path);
        $text = str_replace("{{title}}", $this->prepareTitle($title), $text);
        file_put_contents($path, $text);
    }

    private function overwriteTemplateVersion ($path, $version = '1.3.2')
    {
        $text = file_get_contents($path);
        $text = preg_replace("/\"version\":.*?\"(.*?)\"/", '"version": "' . $version . '", "template_version": "$1"', $text);
        file_put_contents($path, $text);
    }

    private function overwriteMainJsCose ($path)
    {
        $text = file_get_contents($path);
        $text = str_replace("isCose=!1", "isCose=1", $text);
        file_put_contents($path, $text);
    }

    private function generateModuleData ($path, $module, $course, $language, $theme, $show_module_start_screen) {
        if (is_dir($path)) {
            $obj = json_decode($module->data);
            if (!$obj || is_null($obj)) {
                $obj = (object)[];
            }
            $obj->colontitle = $module->name;
            $obj->templateLanguage = $language;

            $obj->selected_theme = $theme == 'all' ? $course->theme : $theme;
            $obj->view_all_themes = boolval($theme == 'all');
            $obj->show_start_screen = ((int)($show_module_start_screen === 'true') + (int)$module->startscreen_show) > 0;
            $obj->behavior = $module->behavior;

            $obj->startscreen = [
                'unit_title' => $module->getStartScreenData('unit_title'),
                'session_title' => $module->getStartScreenData('session_title'),
                'qcf_ref' => $module->getStartScreenData('qcf_ref'),
                'etracker' => $module->getStartScreenData('etracker'),
                'nos_ref' => $module->getStartScreenData('nos_ref'),
                'awarding_body_code' => $module->getStartScreenData('awarding_body_code'),
                'level' => $module->getStartScreenData('level'),
                'credits' => $module->getStartScreenData('credits'),
                'qualification_title' => $module->getStartScreenData('qualification_title'),
                'learning_outcome' => $module->getStartScreenData('learning_outcome'),
                'functional_skills' => $module->getStartScreenData('functional_skills'),
                'publisher_note' => $module->getStartScreenData('publisher_note'),
            ];

            $obj->practice = in_array($module->behavior, ['check', 'practice']);

            $data = json_encode($obj);
            $data = preg_replace('/\\\\\\/storage\\\\\\/app\\\\\\/igvs\\\\\\/module-content\\\\\\/(\\d+)\\\\\\/(\\d+)\\\\\\//', '', $data);

            if (is_file("$path/moduleData.js"))
                unlink("$path/moduleData.js");

            $template = "var moduleData = {data};";
            file_put_contents("$path/moduleData.js", str_replace('{data}', $data, $template));
        }
    }
}
