<?php namespace Igvs\Courses\Traits;

use Db;
use Config;
use Eloquent;
use Igvs\Courses\Components\Course;
use Igvs\Courses\Helpers\Translit;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\Course as CourseItem;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\BackgroundTask;

use Illuminate\Database\Eloquent\Builder;

use Igvs\Courses\Traits\Scorm;
use Igvs\Courses\Traits\PrepareForFtp;
use League\Flysystem\Exception;

use Igvs\Courses\Models\PilotAuth;
//use Igvs\Courses\Models\UserPilot;

trait ExportToZip
{
    public function shortExportToZip($options, $job, $db_id)
    {
        try {
            return $this->exportToPilot(
                $options['courseId'],
                null,
                null,
                $job,
                $db_id,
                $options
            );
        } catch(\Exception $e) {
            BackgroundTask::log_error('Export to Pilot', 'exportToPilot', json_encode($options), $e, $db_id);
        }
    }

    public function exportToZip($courseId, $theme = '_default', $date = null, $job, $db_id, $options)
    {
        ini_set('max_execution_time', 1000);

        $instance_id = \Session::get('igvs.courser.user_instance_id');
        $apiClient = \Academy\Api\Models\Client::find($instance_id);

        $this->export_dir = $export_dir = time() . '_' . $courseId . '_' . rand(0,100);

        if ($apiClient) {
            $this->releaseCloudDir = $apiClient->export_path . '/' . $export_dir;
        } else {
            $this->releaseCloudDir = Config::get('igvs.courses::export_pilot.releaseDir') . '/' . $export_dir;
        }

        $this->releaseCloudDirTmp = Config::get('igvs.courses::export_pilot.tmp') . '/' . $export_dir . '_' . rand(0,100);
        $this->displayReleaseDir = Config::get('igvs.courses::prepareReleaseCloud.displayReleaseDir');
        $job_percent = 0;
        $job_percent_all = 3;

        $tokenCredentials = unserialize($options['oauth_token']);
        $oauth_user_uid = $options['oauth_user_uid'];

        $poo_id = isset($options['poo_id']) ? $options['poo_id'] : '';

        if (is_null($this->releaseCloudDir) || is_null($this->releaseCloudDirTmp) || is_null($this->displayReleaseDir)) {
            throw new \Exception('Error \'release cloud dir\' in configuration!');
        }

        if (is_null($date)) {
            $date = date('Y-m-d');
        }

        if (!$this->dirCheckOrCreate($this->releaseCloudDir)) {
            throw new \Exception('Can\'t create release folder');
        }
        if (!$this->dirCheckOrCreate($this->releaseCloudDirTmp)) {
            throw new \Exception('Can\'t create tmp folder');
        }


        BackgroundTask::job_plus($job, $job_percent, $job_percent_all, $db_id); //1

        $return = "Start<br>-------<br>";
        $return .= $this->Pilot_do_igvs($courseId, $theme, $job, $db_id);

        $this->rmdir_recurse($this->releaseCloudDirTmp);

        BackgroundTask::job_plus($job, $job_percent, $job_percent_all, $db_id); //5

        $url = isset($options['remote_server_domain']) ? $options['remote_server_domain'] : '';

        file_put_contents(__DIR__ . "/../../../../storage/app/igvs/export_pilot.log", date('Y-m-d H:i:s') . "\r\nexportToPilot : " . print_r([
                '$courseId' => $courseId,
                'pilot_course_id' => $options['pilot_course_id'],
                'parent_course_id' => $options['parent_course_id'],
                '$export_dir' => $export_dir
            ], true) . "\r\n", FILE_APPEND);

        $use_licence = \Socialite::with('pilotoauth')->endPoint([
            'course_id' => $courseId,
            'pilot_course_id' => $options['pilot_course_id'],
            'parent_course_id' => $options['parent_course_id'],
            'export_dir' => $export_dir,
            'token_credentials' => $tokenCredentials,
            'oauth_user_uid' => $oauth_user_uid,
            'poo_id' => $poo_id,
            'url' => $url,
            'file_md5' => $this->file_md5,
            'options' => $options,
        ]);

        $job->delete();

        return 'Success!<br>' . $return . "<br>Licence options:" . print_r([
                '$courseId' => $courseId,
                'pilot_course_id' => $options['pilot_course_id'],
                'parent_course_id' => $options['parent_course_id'],
                '$export_dir' => $export_dir,
                'tar' => "{$this->releaseCloudDirTmp}/../{$this->export_dir}.tar",
                'file_md5' => $this->file_md5,
            ], true) . "<br>Licence response:" . print_r($use_licence, true);
    }

    public function exportCourseZip($courseId, $theme = '_default', $date = null, $job, $db_id, $options)
    {
        if (is_null($date)) {
            $date = date('Y-m-d');
        }

        $course = CourseItem::find($courseId);

        if (!$course)
            return '<br>Error! Course not found. Course id: ' . $courseId . '<br>';

        $course_code = str_replace(
            [
                '*'
            ],
            [
                '_'
            ],
            $course->code);

        $export_dir = $course_code . '/' . $date;
        if (isset($options['postfix'])) {
            $export_dir .= $options['postfix'];
        }
        $storage_media_path = __DIR__ . '/../../../..' . Config::get('igvs.courses::zip.releaseDir', '/storage/app/media');
        $this->releaseCloudDir = $storage_media_path . '/' . $export_dir;
        $this->releaseCloudDirTmp = Config::get('igvs.courses::prepareReleaseCloud.tmp') . '/' . time()  . '_' . time() . '_' . rand(0,100);
        $this->displayReleaseDir = Config::get('igvs.courses::prepareReleaseCloud.displayReleaseDir');

        $this->export_dir = time() . '_' . $courseId . '_' . rand(0,100);

        $job_percent = 0;
        $job_percent_all = 3;


        if (is_null($this->releaseCloudDir) || is_null($this->releaseCloudDirTmp) || is_null($this->displayReleaseDir)) {
            throw new \Exception('Error \'release cloud dir\' in configuration!');
        }

        if (!$this->dirCheckOrCreate($this->releaseCloudDir)) {
            throw new \Exception('Can\'t create release folder');
        }
        if (!$this->dirCheckOrCreate($this->releaseCloudDirTmp)) {
            throw new \Exception('Can\'t create tmp folder');
        }


        BackgroundTask::job_plus($job, $job_percent, $job_percent_all, $db_id); //1

        $return = "Start<br>-------<br>";

        $return .= $this->Pilot_do_zip($courseId, $theme, $job, $db_id, $options);

        $this->rmdir_recurse($this->releaseCloudDirTmp);

        BackgroundTask::job_plus($job, $job_percent, $job_percent_all, $db_id); //5

//        $options = BackgroundTask::first($db_id);

        return '<br>Export Pilot: ' . $this->releaseCloudDir . '<br><br>Log:<br>' . $return;
    }

    private function Pilot_do_zip($courseId, $theme = '_default', $job, $db_id, $options = []) {

        $releaseCloudDir = $this->releaseCloudDir;
        $displayReleaseDir = $this->displayReleaseDir;

        $return = 'IGVS: <br>';

        $course = CourseItem::find($courseId);

        $theme = isset($options['theme']) ? $options['theme'] : "_default";
        $topic = isset($options['topicId']) ? $options['topicId'] : null;
        $language = isset($options['language']) ? $options['language'] : $course->language;
        $show_module_start_screen = isset($options['show_module_start_screen']) ? $options['show_module_start_screen'] : $course->show_module_start_screen;
        $export_without_module_content = isset($options['export_without_module_content']) ? $options['export_without_module_content'] : 'false';

        if (!$course) {
            return 'Course ' . $courseId . ' not found.\n';
        }
        $course_pic = $course->logo ? $course->logo->getPath() : '';
        $bg_hat = $course->bg_hat ? $course->bg_hat->getPath() : '';
        $preview = $course->image ? $course->image->getPath() : '';
        $cover = $course->cover ? $course->cover->getPath() : '';

        preg_match("/(png|jpg|jpeg|gif)/", $course_pic, $m);
        $course_pic_exp =  count($m) ? $m[0] : 'png';

        preg_match("/(png|jpg|jpeg|gif)/", $bg_hat, $m);
        $bg_hat_exp =  count($m) ? $m[0] : 'png';

        preg_match("/(png|jpg|jpeg|gif)/", $preview, $m);
        $preview_exp =  count($m) ? $m[0] : 'png';

        preg_match("/(png|jpg|jpeg|gif)/", $cover, $m);
        $cover_exp =  count($m) ? $m[0] : 'png';

        // копируем временный sqlite DB
        $sqlite_temp_path = __DIR__ . "/../../../../storage/app/igvs/db";
        if (is_dir($sqlite_temp_path)) {
            $this->rmdir_recurse($sqlite_temp_path);
        }
        mkdir($sqlite_temp_path);
        copy(Config::get('igvs.courses::html-module.path') . '/shelldb_static_template.db', "{$sqlite_temp_path}/shelldb_static.db");

        $db_name = uniqid();

        Config::set('database.connections.'.$db_name, array(
            'driver' => 'sqlite',
            'database' => "{$sqlite_temp_path}/shelldb_static.db",
        ));

        $categories = Category::where('course_id', $courseId)
            ->orderBy('sort')
            ->get();

        $eapp = [];

        foreach ($categories as $cat_key => $cat_val) {
            if (is_null($cat_val->parent_id)) {

                $category_code_translit = Translit::translit($cat_val->code, ['remove_special_symbol' => true]);

                $eapp[] = [
                    "type" => $cat_val->is_separate ? 'splitter' : 'separator',
                    "number" => $cat_val->sort,
                    "resource" => "",
                    "id" => $cat_val->id,
                    "original_id" => $cat_val->copy_original_id,
                    "class" => "",
                    "textName" => "",
                    "parent" => '0',
                    "textTitle" => str_replace('||', '. ', $cat_val->name),
                    "section" => "eapp",
                    "preview" => "",
                    "code" => $category_code_translit,
                ];

                $categories_topic = Category::where('parent_id', $cat_val->id)
                    ->orderBy('sort')
                    ->get();

                $param_text_desc = explode('|', $cat_val->name);
                $param_desc = trim(isset($param_text_desc[1]) ? $param_text_desc[0] : '');
                $param_text = trim(isset($param_text_desc[1]) ? $param_text_desc[1] : $param_text_desc[0]);

                $params = [
                    'number' => $cat_val->sort,
                    'text' => $param_text,
                    'parent' => 0,
                    'section' => 'ebook',
                    'type' => 'separator',
                    'resource' => '',
                    'desc' => $param_desc,
                    'class' => '',
                ];
                $this->sqlite_rubricator_insert($db_name, $params);

                foreach ($categories_topic as $cat_top_key => $cat_top_val) {
                    //      add to eapp
                    $act_code_translit = Translit::translit($cat_top_val->code, ['remove_special_symbol' => true]);

                    $eapp[] = [
                        "type" => "group",
                        "number" => $cat_top_val->sort,
                        "resource" => "",
                        "id" => $cat_top_val->id,
                        "original_id" => $cat_top_val->copy_original_id,
                        "class" => "",
                        "textName" => "",
                        "parent" => $cat_val->id,
                        "textTitle" => str_replace('||', '. ', $cat_top_val->name),
                        "section" => "eapp",
                        "preview" => "",
                        "code" => $act_code_translit
                    ];

                    $js_db_items = $this->make_js_db($courseId, $cat_top_val, null, $category_code_translit, $show_module_start_screen);
                    $eapp = array_merge($eapp, $js_db_items);
                    // ---- -- -- -- -- //

                    $param_text_desc = explode('||', $cat_top_val->name);
                    $param_desc = trim(isset($param_text_desc[1]) ? $param_text_desc[0] : '');
                    $param_text = trim(isset($param_text_desc[1]) ? $param_text_desc[1] : $param_text_desc[0]);

                    $params = [
                        'number' => $cat_top_val->sort,
                        'text' => $param_text,
                        'parent' => 0,
                        'section' => 'ebook',
                        'type' => 'group',
                        'resource' => '',
                        'desc' => $param_desc,
                        'class' => '',
                    ];
                    $category_id = $this->sqlite_rubricator_insert($db_name, $params);

                    foreach ($js_db_items as $js_db_item) {
                        $param_text_desc = explode('||', $js_db_item['textTitle']);
                        $param_desc = trim(isset($param_text_desc[1]) ? $param_text_desc[0] : '');
                        $param_text = trim(isset($param_text_desc[1]) ? $param_text_desc[1] : $param_text_desc[0]);

                        $params = [
                            'number' => $js_db_item['number'],
                            'text' => $param_text,
                            'parent' => $category_id,
                            'section' => 'ebook',
                            'type' => 'item',
                            'resource' => $js_db_item['resource'],
                            'desc' => $param_desc,
                            'class' => strlen($js_db_item['class']) ? $js_db_item['class'] : 'page',
                        ];
                        $this->sqlite_rubricator_insert($db_name, $params);
                    }

                }
            }
        }

        //словарь
        $vocabulary = $this->make_vocabulary($course->id, $this->releaseCloudDirTmp);

        //for installer head title (in window)
        //clean course title from special symbols and html tags
        //for &nbsp; and <br> do replace to space
        $course_title_clean = str_replace([
            '"',
            '&nbsp;',
            '<br>',
            '  ',
        ], [
            '',
            ' ',
            ' ',
            ' ',
        ], $course->title);
        $course_title_clean = strip_tags($course_title_clean);
        $course_title_clean = preg_replace("/&#?[a-z0-9]{2,8};/i","", $course_title_clean);
        $course_title_clean = preg_replace('/[^[:word:] ]+/u', '', $course_title_clean);

        $json = file_get_contents(Config::get('igvs.courses::resourcesPath') . '/export-beta/js_db.json');
        $json = str_replace([
            '{{dir}}',
            '{{title}}',
            '{{title_clean}}',
            '{{eapp}}',
            '{{vocabulary}}',
            '{{course_id}}',
            '{{course_code}}',
            '{{bg_hat}}',
            '{{preview}}',
            '{{catalogue_preview}}',
            '{{cover}}',
            '{{working_programm_files}}',
        ], [
            '',
            str_replace('"', '&#34;', $course->title),
            $course_title_clean,
            json_encode($eapp),
            json_encode($vocabulary),
            $courseId,
            $course->getCourseCodeClean(),
            strlen($bg_hat) ? 'design/bg_hat.' . $bg_hat_exp : '',
            strlen($course_pic) ? 'design/preview.' . $course_pic_exp : '',
            strlen($preview) ? 'design/catalogue_preview.' . $preview_exp : '',
            strlen($cover) ? 'design/cover.' . $cover_exp : '',
            $course->materials ? 'working_programm_files/' . $course->materials->id . '_' . $course->materials->getFilename() : '',
        ], $json);

        if (is_dir("{$this->releaseCloudDirTmp}/db")) {
            $this->rmdir_recurse("{$this->releaseCloudDirTmp}/db");
        }

        mkdir("{$this->releaseCloudDirTmp}/db");
        file_put_contents("{$this->releaseCloudDirTmp}/db/js_db.json", $json);

        copy("{$sqlite_temp_path}/shelldb_static.db", "{$this->releaseCloudDirTmp}/db/shelldb_static.db");

        // copy course image
        if (!is_dir("{$this->releaseCloudDirTmp}/design"))
            mkdir("{$this->releaseCloudDirTmp}/design");
        if (!is_dir("{$releaseCloudDir}/design"))
            mkdir("{$releaseCloudDir}/design");
        if (strlen($course_pic)) {
            copy(Config::get('igvs.courses::sitePath') . $course_pic, "{$this->releaseCloudDirTmp}/design/preview." . $course_pic_exp);
        }
        if (strlen($bg_hat)) {
            copy(Config::get('igvs.courses::sitePath') . $bg_hat, "{$this->releaseCloudDirTmp}/design/bg_hat." . $bg_hat_exp);
        }
        if (strlen($preview)) {
            copy(Config::get('igvs.courses::sitePath') . $preview, "{$this->releaseCloudDirTmp}/design/catalogue_preview." . $preview_exp);
        }
        if (strlen($cover)) {
            copy(Config::get('igvs.courses::sitePath') . $cover, "{$this->releaseCloudDirTmp}/design/catalogue_preview." . $cover_exp);
            copy(Config::get('igvs.courses::sitePath') . $cover, "{$releaseCloudDir}/design/catalogue_preview." . $cover_exp);
        }

        // copy working_programm_files
        mkdir("{$this->releaseCloudDirTmp}/working_programm_files");
        if ($course->materials) {
            $file_path = $course->materials->getLocalPath();
            copy($file_path, "{$this->releaseCloudDirTmp}/working_programm_files/" . $course->materials->id . '_' . $course->materials->getFilename());
        };

        BackgroundTask::job_plus($job, 1, 2, $db_id); //4

        if ($export_without_module_content != 'true') {
            $igvs_paths = $this->prepareForFtpBeta($courseId, $topic, $theme, $language, $show_module_start_screen, null, true, null, false, $options);
        } else {
            $igvs_paths = [];
        }
        $igvs_path_to_copy = [];

        if (count($igvs_paths)) {
            foreach ($igvs_paths as $path) {
                $topic_dir = $path['topic_dir'];

                $dest_files_path = "{$this->releaseCloudDirTmp}/{$topic_dir}/";

                if (is_dir($dest_files_path)) {
                    $this->rmdir_recurse($dest_files_path);
                }

                if (!isset($igvs_path_to_copy[$dest_files_path])) {
                    $igvs_path_to_copy[$dest_files_path] = $path;
                }
            }
        }

        $log_filename = __DIR__ . '/../resources/bash_copylog.info';

        if (count($igvs_path_to_copy)) {
            foreach ($igvs_path_to_copy as $path) {
                $course_dir = $path['course_dir'];
                $topic_dir = $path['topic_dir'];

                $dest_files = "/{$topic_dir}";
                $dest_files_path = "{$this->releaseCloudDirTmp}{$dest_files}";

                $file = fopen($log_filename, 'a+');
                fwrite($file, "{$path['module_path']} to {$dest_files_path}\r\n");
                fclose($file);

////            $bash = exec("cp -R \"{$path['module_path']}\" \"{$dest_files_path}\" ");
//                $bash = exec("cp -R \"{$path['module_path']}\" \"{$dest_files_path}\" ");
                $this->recourceCopy("{$path['module_path']}", "{$dest_files_path}");

                $bash = "\r" . exec("rm -R \"{$path['module_path']}\"");

                $file = fopen($log_filename, 'a+');
                fwrite($file, "{$bash}\r\n");
                fwrite($file, "=================================\r\n");
                fclose($file);


                $return .= $displayReleaseDir . str_replace('/', '\\', $dest_files) . '<br>';
            }
        }

        if ($export_without_module_content != 'true') {
            $command = "cd {$this->releaseCloudDirTmp} && tar -cf \"{$this->releaseCloudDirTmp}/../{$this->export_dir}.tar\" . ";

            $file = fopen($log_filename, 'a+');
            fwrite($file, "{$command}\r\n");
            fwrite($file, "=================================\r\n");
            fclose($file);

            exec($command);

            $this->file_md5 = md5_file("{$this->releaseCloudDirTmp}/../{$this->export_dir}.tar");

            exec("cp \"{$this->releaseCloudDirTmp}/../{$this->export_dir}.tar\" \"{$releaseCloudDir}/{$this->export_dir}.tar\" ");
            exec("rm \"{$this->releaseCloudDirTmp}/../{$this->export_dir}.tar\"");

            $file = fopen($log_filename, 'a+');
            fwrite($file, "{$command}\r\n");
            fwrite($file, "=================================\r\n");
            fclose($file);
        }

        BackgroundTask::job_plus($job, 2, 2, $db_id);

        return $return;
    }
}
