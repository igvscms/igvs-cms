<?php namespace Igvs\Courses\Traits;

trait CustomRoutes
{
    public static function getAfterFilters()
    {
        return [];
    }

    public static function getBeforeFilters()
    {
        return [];
    }

    public static function getMiddleware()
    {
        return [];
    }

    public function callAction($method, $parameters = false)
    {
        return $this->run($method, $parameters);
        // return call_user_func_array(array($this, $method), $parameters);
    }
}