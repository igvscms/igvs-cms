<?php namespace Igvs\Courses\Traits;

use Config;
use Igvs\Courses\Components\Course;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\Course as CourseItem;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\BackgroundTask;

use Illuminate\Database\Eloquent\Builder;

use Igvs\Courses\Traits\Scorm;
use Igvs\Courses\Traits\PrepareForFtp;
use Igvs\Courses\Traits\ExportToPilot;
use League\Flysystem\Exception;

trait ExportToReleaseCloud
{
    private $releaseCloudDir;
    private $releaseCloudDirTmp;
    private $displayReleaseDir;

    public function shortExportToReleaseCloud($options, $job, $db_id)
    {

        try {
            return $this->exportToReleaseCloud(
                $options,
                $job,
                $db_id
            );
        } catch(\Exception $e) {
            BackgroundTask::log_error('Export to Release Cloud', 'exportToReleaseCloud', json_encode($options), $e, $db_id);
        }
    }
//$courseId, $buildId, $topicId, $theme = '_default',  $language = 'en', $date = null, $postfix = '', $beta_folder = '', $to_igvs = false, $to_scorm = false, $to_cose = false, $to_beta = false, $to_pilot = false,
    public function exportToReleaseCloud($options, $job, $db_id)
    {
        $courseId                    = isset($options['courseId']) ? $options['courseId'] : 0;
        $buildId                     = isset($options['buildId']) ? $options['buildId'] : null;
        $topicId                     = isset($options['topicId']) ? $options['topicId'] : null;
        $theme                       = isset($options['theme']) ? $options['theme'] : '_default';
        $language                    = isset($options['language']) ? $options['language'] : 'en';
        $date                        = isset($options['date']) ? $options['date'] : null;
        $postfix                     = isset($options['postfix']) ? $options['postfix'] : '';
        $beta_folder                 = isset($options['beta_folder']) ? $options['beta_folder'] : '';
        $to_igvs                     = isset($options['to_igvs']) ? $options['to_igvs'] : false;
        $to_scorm                    = isset($options['to_scorm']) ? $options['to_scorm'] : false;
        $to_cose                     = isset($options['to_cose']) ? $options['to_cose'] : false;
        $to_beta                     = isset($options['to_beta']) ? $options['to_beta'] : false;
        $to_pilot                    = isset($options['to_pilot']) ? $options['to_pilot'] : false;
        $to_zip                      = isset($options['to_zip']) ? $options['to_zip'] : false;
        $to_maker                    = isset($options['to_maker']) ? $options['to_maker'] : false;
        $to_zip_local                = isset($options['to_zip_local']) ? $options['to_zip_local'] : false;
        $to_mobile                   = isset($options['to_mobile']) ? $options['to_mobile'] : false;
        $set_topic_lock              = isset($options['set_topic_lock']) ? $options['set_topic_lock'] : false;
        $set_transfer_user_to_review = isset($options['set_transfer_user_to_review']) ? $options['set_transfer_user_to_review'] : false;

        if ($set_topic_lock == 'true') {
            $this->lockTopic($this->getTopicIds($courseId, $buildId, $topicId));
        }

        if ($set_transfer_user_to_review == 'true') {
            $this->transferUserToReview($courseId);
        }

        $this->releaseCloudDir = Config::get('igvs.courses::prepareReleaseCloud.releaseDir');
        $this->releaseCloudDirTmp = Config::get('igvs.courses::prepareReleaseCloud.tmp') . '/' . time()  . '_' . microtime() . '_' . rand(0,100);
        $this->displayReleaseDir = Config::get('igvs.courses::prepareReleaseCloud.displayReleaseDir');
        $job_percent = 0;
        $job_percent_all = 1 + (int)$to_igvs * 2 + (int)$to_scorm * 2 + (int)$to_cose * 2 + (int)$to_beta * 2;

        if (is_null($this->releaseCloudDir) || is_null($this->releaseCloudDirTmp) || is_null($this->displayReleaseDir)) {
            throw new \Exception('Error \'release cloud dir\' in configuration!');
        }

        if (is_null($date)) {
            $date = date('Y-m-d');
        }

        if ($language == 'en') {
            $language = 'eng';
        }


        if (!$this->dirCheckOrCreate($this->releaseCloudDir)) {
            throw new \Exception('Can\'t create release folder');
        }
        if (!$this->dirCheckOrCreate($this->releaseCloudDirTmp)) {
            throw new \Exception('Can\'t create tmp folder');
        }


        BackgroundTask::job_plus($job, $job_percent, $job_percent_all, $db_id); //1

        $return = "Start<br>-------<br>";

        if ($to_scorm == 'true') {
            $return .= $this->ETRC_do_scorm($courseId, $buildId, $topicId, $theme, $language, $date, $postfix, false, $job, $db_id, $options);
        }

        if ($to_cose == 'true') {
            $return .= $this->ETRC_do_scorm($courseId, $buildId, $topicId, $theme, $language, $date, $postfix, true, $job, $db_id, $options);
        }

        if ($to_igvs == 'true') {
            $return .= $this->ETRC_do_igvs($courseId, $buildId, $topicId, $theme, $language, $date, $postfix, $job, $db_id, $options);
        }

        if ($to_beta == 'true') {
            $return .= $this->ETRC_do_beta($courseId, $buildId, $topicId, $theme, $language, $date, $beta_folder, $job, $db_id, $options);
        }

        if ($to_pilot == 'true' || $to_mobile == 'true') {
            $return .= $this->ETRC_do_pilot($courseId, $buildId, $topicId, $theme, $language, $date, $beta_folder, $job, $db_id, $options);
        }

        if ($to_zip == 'true') {
            $return .= $this->ETRC_do_zip($courseId, $buildId, $topicId, $theme, $language, $date, $beta_folder, $job, $db_id, $options);
        }

        if ($to_maker == 'true' || $to_zip_local == 'true') {
            $return .= $this->ETRC_do_maker($courseId, $buildId, $topicId, $theme, $language, $date, $beta_folder, $job, $db_id, $options);
        }

        $this->rmdir_recurse($this->releaseCloudDirTmp);

        BackgroundTask::job_plus($job, $job_percent, $job_percent_all, $db_id); //5

        return 'Success!<br>' . $return;
    }

    private function ETRC_do_scorm($courseId, $buildId, $topicId, $theme = '_default', $language, $date = null, $postfix = '', $iscose = false, $job, $db_id, $options = []) {

        $releaseCloudDirTmp = $this->releaseCloudDirTmp;
        $releaseCloudDir = $this->releaseCloudDir;
        $displayReleaseDir = $this->displayReleaseDir;

        $dest_dir = $iscose ? 'KOS' : 'SCORM';

        $return = "{$dest_dir}:<br>";

        // making scorm packs
        $scorm_paths = $this->createScorm($courseId, $buildId, $topicId, $theme, $language, true, $iscose, $options);

        $topics = [];

        // move scorm packs to tmp
        foreach ($scorm_paths as $path) {
            if (!$this->dirCheckOrCreate($releaseCloudDirTmp . '/' . $path['topic_dir'])) {
                throw new \Exception('Can\'t create tmp topic folder ' . ($path['topic_dir']));
            }

            copy($path['archive_path'], $releaseCloudDirTmp . '/' . $path['topic_dir'] . '/' . $path['module_filename']);

            if (!isset($topics[$path['topic_dir']])) {
                $topics[$path['topic_dir']] = $path['course_dir'];
            }
        }

        BackgroundTask::job_plus($job, 1, 2, $db_id); //2

        // zip pack scorms to release cloud
        foreach ($topics as $topic_dir => $topic_course_dir) {
            if (!$this->dirCheckOrCreate("{$releaseCloudDir}/{$topic_course_dir}")) {
                throw new \Exception('Can\'t create folder: ' . "{$releaseCloudDir}/{$topic_course_dir}");
            }
            if (!$this->dirCheckOrCreate("{$releaseCloudDir}/{$topic_course_dir}/{$topic_dir}")) {
                throw new \Exception('Can\'t create folder: ' . "{$releaseCloudDir}/{$topic_course_dir}/{$topic_dir}");
            }
            if (!$this->dirCheckOrCreate("{$releaseCloudDir}/{$topic_course_dir}/{$topic_dir}/{$dest_dir}")) {
                throw new \Exception('Can\'t create folder: ' . "{$releaseCloudDir}/{$topic_course_dir}/{$topic_dir}/{$dest_dir}");
            }

            $dest_archive = "/{$topic_course_dir}/$topic_dir/{$dest_dir}/{$date}{$postfix}.zip";
            $dest_archive_path = "{$releaseCloudDir}{$dest_archive}";

            if (file_exists($dest_archive_path)) {
                unlink($dest_archive_path);
            }

            $this->zip("{$releaseCloudDirTmp}/{$topic_dir}", $dest_archive_path, true);

            $return .= $displayReleaseDir . str_replace('/', '\\', $dest_archive) . '<br>';
        }

        BackgroundTask::job_plus($job, 2, 2, $db_id); //3

        return $return;
    }

    private function ETRC_do_igvs($courseId, $buildId, $topicId, $theme = '_default', $language, $date = null, $postfix = '', $job, $db_id, $options = []) {

        $releaseCloudDirTmp = $this->releaseCloudDirTmp;
        $releaseCloudDir = $this->releaseCloudDir;
        $displayReleaseDir = $this->displayReleaseDir;

        $return = 'IGVS: <br>';

        $igvs_paths = $this->prepareForFtpBeta($courseId, $topicId, $theme, $language, null, null, true, $buildId, $options);
        $igvs_path_to_copy = [];

        foreach ($igvs_paths as $path) {
            $course_dir = $path['course_dir'];
            $topic_dir = $path['topic_dir'];

            $dest_files_path = "{$releaseCloudDir}/{$course_dir}/{$topic_dir}/IGVS/{$date}{$postfix}";

            if (is_dir($dest_files_path)) {
                $this->rmdir_recurse($dest_files_path);
            }

            if (!isset($igvs_path_to_copy[$dest_files_path])) {
                $igvs_path_to_copy[$dest_files_path] = $path;
            }
        }

        BackgroundTask::job_plus($job, 1, 2, $db_id);

        foreach ($igvs_path_to_copy as $path) {
            $course_dir = $path['course_dir'];
            $topic_dir = $path['topic_dir'];

            if (!$this->dirCheckOrCreate("{$releaseCloudDir}/{$course_dir}")) {
                throw new \Exception('Can\'t create folder: ' . "{$releaseCloudDir}/{$course_dir}");
            }
            if (!$this->dirCheckOrCreate("{$releaseCloudDir}/{$course_dir}/{$topic_dir}")) {
                throw new \Exception('Can\'t create folder: ' . "{$releaseCloudDir}/{$course_dir}/{$topic_dir}");
            }
            if (!$this->dirCheckOrCreate("{$releaseCloudDir}/{$course_dir}/{$topic_dir}/IGVS")) {
                throw new \Exception('Can\'t create folder: ' . "{$releaseCloudDir}/{$course_dir}/{$topic_dir}/IGVS");
            }

            $dest_files = "/{$course_dir}/{$topic_dir}/IGVS/{$date}{$postfix}";
            $dest_files_path = "{$releaseCloudDir}{$dest_files}";

            $this->dirCheckOrCreate($dest_files_path);
            $this->recourceCopy("{$path['module_path']}", "{$dest_files_path}");

            $return .= $displayReleaseDir . str_replace('/', '\\', $dest_files) . '<br>';
        }

        BackgroundTask::job_plus($job, 2, 2, $db_id); //4

        return $return;
    }

    private function ETRC_do_beta($courseId, $buildId, $topicId, $theme = '_default', $language, $date = null, $beta_folder, $job, $db_id, $options = [])
    {
        BackgroundTask::job_plus($job, 1, 2, $db_id);

        $return = $this->prepareForFtpBeta($courseId, $topicId, $theme, $language, null, $beta_folder, false, $buildId, $options);

        BackgroundTask::job_plus($job, 2, 2, $db_id);

        return $return;
    }

    private function ETRC_do_pilot($courseId, $buildId, $topicId, $theme = '_default', $language, $date = null, $beta_folder, $job, $db_id, $options)
    {
        BackgroundTask::job_plus($job, 1, 2, $db_id);

        $return = $this->exportCourseIGVS($courseId, $theme, $date, $job, $db_id, $options);

        BackgroundTask::job_plus($job, 2, 2, $db_id);

        return $return;
    }

    private function ETRC_do_zip($courseId, $buildId, $topicId, $theme = '_default', $language, $date = null, $beta_folder, $job, $db_id, $options)
    {
        BackgroundTask::job_plus($job, 1, 2, $db_id);

        $return = $this->exportCourseZip($courseId, $theme, $date, $job, $db_id, $options);

        BackgroundTask::job_plus($job, 2, 2, $db_id);

        return $return;
    }

    private function ETRC_do_maker($courseId, $buildId, $topicId, $theme = '_default', $language, $date = null, $beta_folder, $job, $db_id, $options)
    {
        BackgroundTask::job_plus($job, 1, 2, $db_id);

        $return = $this->exportCourseIGVS($courseId, $theme, $date, $job, $db_id, $options);

        BackgroundTask::job_plus($job, 2, 2, $db_id);

        return $return;
    }

    private function getModuleContentIds($course_id, $build_id, $topic_id) {
        $categories = Category::where('parent_id', $topic_id);
        if (!is_null($topic_id) && strlen($topic_id)) {
            $categories->whereRaw('parent_id = ? or id = ?', [$topic_id, $topic_id]);
        }

        $categories->orderBy('sort');
        $categories = $categories->get();

        $modules = ModuleContent::where('course_id', $course_id);
        if (!is_null($topic_id) && strlen($topic_id)) {
            $modules->whereIn('category_id', $categories->lists('id'));
        }
        if (!is_null($build_id) && strlen($build_id)) {
            $modules->whereHas('builds', function (Builder $q) use ($build_id) {
                $q->where('build_id', $build_id);
            });
        }
        $modules->orderBy('sort');
        $modules = $modules->get();

        return $modules->lists('id');
    }

    /**
     * @param array $module_ids
     */
    private function compareContentModuleVersions(array $module_ids = []) {
        $count_no_last_module_version = 0;
        if (count($module_ids)) {
            foreach($module_ids as $module_id) {
                if ($moduleContent = ModuleContent::find($module_id)) {
                    if (version_compare($moduleContent->getModuleVersion(), $moduleContent->module->getLatestVersion(), '<')) {
                        $count_no_last_module_version++;
                    }
                }
            }
        }

        return strval($count_no_last_module_version);
    }
}
