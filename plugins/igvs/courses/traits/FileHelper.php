<?php namespace Igvs\Courses\Traits;

use System\Models\File;
use File as SysFile;

trait FileHelper
{
    private function scandir($path, $exclude = [])
    {
        $exclude = array_merge(['.', '..'], $exclude);
        $array = scandir($path);

        return array_filter($array, function($v) use (&$exclude) {
            return !in_array($v, $exclude);
        });
    }

    private function attachFile($model, $attr, $path)
    {
        $file = new File;
        $file->fromFile($path);
        $model->{$attr}()->add($file);
    }

    private function attachFiles($model, $attr, $path)
    {
        if (is_dir($path)) {
            $arr = $this->scandir($path);
            foreach ($arr as $v) {
                $file = new File;
                $file->fromFile($path . '/' . $v);
                $model->{$attr}()->add($file);
            }
        }
    }

    private function copyFiles($source, $dest)
    {
        if (!file_exists($source))
            return;

        if (!is_dir($dest))
            mkdir($dest, 0777, true);

        $files = $this->scandir($source);

        foreach ($files as $file) {
            copy("{$source}/$file", "{$dest}/$file");
        }
    }

    private function recourceCopy($source, $dest, $callback = null, $right_files_list = null, $without_link = false)
    {
        // filter
        if ($callback && !$callback($source)) {
            return;
        }

        if (is_dir($source)) {

            if (!file_exists($dest)) {
                $copy = mkdir($dest, 0777, true);
            }

            $files = scandir($source);

            foreach ($files as $file)
                if ($file != "." && $file != "..")
                    $this->recourceCopy("$source/$file", "$dest/$file", $callback, $right_files_list);
        }
        else if (file_exists($source) && $this->is_right_file($source, $right_files_list)) {
            $link = null;

            try {
                if ($without_link)
                    $link = link ($source, $dest);
            } catch (\Exception $e) {}

            if (!$link)
                copy($source, $dest);
        }
    }

    public function is_right_file($source, $right_files_list)
    {
        if (is_null($right_files_list) || !is_array($right_files_list) || !count($right_files_list))
            return true;

        $ext_list = [
            '.css',
            '.js',
            '.svg',
            '.ttf',
            '.eot',
            '.woff',
            '.pdf',
            '.doc',
            '.docx',
            '.xls',
            '.xlsx',
        ];

        //если файл имеет расширение js или css
        foreach ($ext_list as $ext) {
            $file_ext = substr($source, -1 * strlen($ext));
            if ($ext === $file_ext)
                return true;
        }

        foreach ($right_files_list as $file) {
            $right_file = substr($source, strlen($source) - strlen($file));

            //если каталог с файлом в переданном "белом" списке, например images/*
            if (substr($file, -1) == '*') {
                $sourse_screen = substr($file, 0, strlen($file)-1);
                $sourse_screen = str_replace([
                    '^', '$', '[', '.', '{', '*', '(', ')', '\\', '/', '+', '|', '?', '<', '>',
                ], [
                    '\^', '\$', '\[', '\.', '\{', '\*', '\(', '\)', '\\\\', '\/', '\+', '\|', '\?', '\<', '\>',
                ], $sourse_screen);

                file_put_contents(__DIR__ . '/tru_file_log.txt', print_r([
                    $right_files_list,
                    $source,
                    $sourse_screen
                ], true), FILE_APPEND);

                preg_match("/module-content\/(\d{1,})\/(\d{1,})\/({$sourse_screen})/", $source, $m);

                if (count($m))
                    return true;
            }


            //если файл в переданном "белом" списке
            if ($right_file === $file)
                return true;
        }

        return false;
    }

    public function getFileList($html)
    {
        preg_match_all("/src=\"(.*?)\"/", $html, $matches);

        if (!$matches || !is_array($matches) || !count($matches) || !isset($matches[1]))
            return [];

        return $matches[1];
    }

    private function ln($src, $dest, $filter = null)
    {
        if ($filter && !is_callable($filter) && !is_array($filter)) {
            throw new \Exception(self::class . ".ln() - third parameter must be an array or function");
        }

        if ($filter == null) {
            // file_put_contents ('hardlink.log', "$src\r\n", FILE_APPEND);
            return link($src, $dest);
        }
        else if (is_array($filter) && in_array(strtolower(SysFile::extension($src)), $filter)) {
            // file_put_contents ('hardlink.log', "$src\r\n", FILE_APPEND);
            return link($src, $dest);
        }
        else if (is_callable($filter) && $filter($src)) {
            // file_put_contents ('hardlink.log', "$src\r\n", FILE_APPEND);
            return link($src, $dest);
        }
        else {
            // file_put_contents ('simplecopy.log', "$src\r\n", FILE_APPEND);
            return copy($src, $dest);
        }
    }

    private function lnFiles($source, $dest, $filter = null)
    {
        if (!file_exists($source))
            return;

        if (!is_dir($dest))
            mkdir($dest, 0777, true);

        $files = $this->scandir($source);

        foreach ($files as $file) {
            $this->ln("{$source}/$file", "{$dest}/$file", $filter);
        }
    }

    private function recourceLn($source, $dest, $callback = null, $filter = null, $save_both = false)
    {
        $return = [];

        // filter
        if ($callback && !$callback($source)) {
            return;
        }

        if (is_dir($source) && !is_link($source)) {

            if (!file_exists($dest)) {
                mkdir($dest, 0777, true);
            } elseif ($save_both) {
                $dest_test = $dest;

                $i = 1;
                while(file_exists($dest_test)) {
                    $dest_test = $dest . " ({$i})";
                    $i++;
                }

                $dest = $dest_test;

                mkdir($dest, 0777, true);
            }

            $files = scandir($source);

            foreach ($files as $file)
                if ($file != "." && $file != "..")
                    $return = array_merge($return, $this->recourceLn("$source/$file", "$dest/$file", $callback, $filter, $save_both));
        }
        elseif (file_exists($source) && !file_exists($dest)) {

            $dest_dir = dirname($dest);

            if (!is_dir($dest_dir)) {
                mkdir($dest_dir, 0777, true);
            }

            $this->ln($source, $dest, $filter);
        }
        elseif (file_exists($source) && file_exists($dest) && $save_both) {
            $dest = preg_replace('/\s\(([0-9]+)\)(\.[a-zA-Z]+|$)/', '$2', $dest);

            $dest_test = explode('.', $dest);

            if (count($dest_test)) {
                $dest_test_ext = array_pop($dest_test);
                $dest_test_name = implode('.', $dest_test);
            } else {
                $dest_test_ext = '';
                $dest_test_name = $dest;
            }

            $dest_test = $dest;

            $i = 1;
            while(file_exists($dest_test)) {
                $dest_test = $dest_test_name . " ({$i})." . $dest_test_ext;
                $i++;
            }

            $dest = $dest_test;

            $this->ln($source, $dest, $filter);
        }

        $return[] = [
            'source' => $source,
            'dest' => $dest,
        ];

        return $return;
    }

    private function removeDir($dir)
    {
        $path = rtrim($dir, '/').'/';
        $handle = opendir($path);
        while(false !== ($file = readdir($handle))) {
            if($file != '.' and $file != '..' ) {
                $fullpath = $path.$file;
                if(is_dir($fullpath)) $this->removeDir($fullpath); else unlink($fullpath);
            }
        }
        closedir($handle);
        rmdir($path);
    }

    private function zip($source, $destination, $removeSource = false)
    {
        if (!extension_loaded('zip'))
            throw new \Exception('Not found extension zip');

        if (!file_exists($source))
            throw new \Exception('Not found folder ' . $source);

        $zip = new \ZipArchive();
        if (!$zip->open($destination, \ZipArchive::CREATE))
            throw new \Exception('Unexpected error');

       $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($source),
            \RecursiveIteratorIterator::SELF_FIRST
        );

        $source = realpath($source);
        $ds = DIRECTORY_SEPARATOR;

        foreach ($files as $key => $file) {

            if (is_dir($file) && in_array(basename($file), ['.', '..']))
                continue;

            $file = realpath($file);

            if (is_dir($file)) {
                $zip->addEmptyDir(str_replace("{$source}{$ds}" , '', $file));
            }
            else if (is_file($file)) {
                $zip->addFile($file, str_replace("{$source}{$ds}" , '', $file));
            }
        }

        $zip->close();

        if ($removeSource)
            $this->removeDir($source);
    }

    private function dirCheckOrCreate($path) {
        try {

            if (!file_exists($path))
                return mkdir($path, 0777, true);

        } catch (\Exception $e) {
            throw new \Exception("path: $path\nMessage:\n" . $e->getMessage());
        }
        return true;
    }
}
