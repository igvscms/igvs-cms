<?php namespace Igvs\Courses\Traits;

use Db;
use Config;
use Eloquent;
use Igvs\Courses\Components\Course;
use Igvs\Courses\Helpers\Translit;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\Course as CourseItem;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Module;
use Igvs\Courses\Models\BackgroundTask;
use Igvs\Courses\Models\Role;
use Igvs\Courses\Models\Member;

use Illuminate\Database\Eloquent\Builder;

use Igvs\Courses\Traits\Scorm;
use Igvs\Courses\Traits\PrepareForFtp;
use League\Flysystem\Exception;

use Igvs\Courses\Models\PilotAuth;
//use Igvs\Courses\Models\UserPilot;

trait ExportToPilot
{
//    private $releaseCloudDir;
//    private $releaseCloudDirTmp;
//    private $displayReleaseDir;
    private $file_md5 = 0;

    public $export_dir = '';

    public function shortExportToPilot($options, $job, $db_id)
    {
        if (!isset($options['pilot_course_id']))
            $options['pilot_course_id'] = '';

        try {
            return $this->exportToPilot(
                $options['courseId'],
                null,
                null,
                $job,
                $db_id,
                $options
            );
        } catch(\Exception $e) {
            BackgroundTask::log_error('Export to Pilot', 'exportToPilot', json_encode($options), $e, $db_id);
        }
    }

    public function exportToPilot($courseId, $theme = '_default', $date = null, $job, $db_id, $options)
    {
        ini_set('max_execution_time', 1000);

        $instance_id = \Session::get('igvs.courser.user_instance_id');
        $apiClient = \Academy\Api\Models\Client::find($instance_id);

        $this->export_dir = $export_dir = time() . '_' . $courseId . '_' . rand(0,100);

        file_put_contents(__DIR__ . "/../../../../storage/app/igvs/export_pilot.log", date('Y-m-d H:i:s') . "\r\nexportToPilot : " . print_r([
                '$db_id' => $db_id,
                '$courseId' => $courseId,
                'pilot_course_id' => $options['pilot_course_id'],
                'parent_course_id' => $options['parent_course_id'],
                '$export_dir' => $export_dir,
                'message' => 'start export',
            ], true) . "\r\n", FILE_APPEND);

        if ($apiClient) {
            $this->releaseCloudDir = $apiClient->export_path . '/' . $export_dir;
        } else {
            $this->releaseCloudDir = Config::get('igvs.courses::export_pilot.releaseDir') . '/' . $export_dir;
        }

        $this->releaseCloudDirTmp = Config::get('igvs.courses::export_pilot.tmp') . '/' . $export_dir . '_' . rand(0,100);
        $this->displayReleaseDir = Config::get('igvs.courses::export_pilot.displayReleaseDir');
        $job_percent = 0;
        $job_percent_all = 3;

        $tokenCredentials = unserialize($options['oauth_token']);
        $oauth_user_uid = $options['oauth_user_uid'];
        $oauth_driver = $options['oauth_driver'];
        $oauth_version = $options['oauth_version'];

        $poo_id = isset($options['poo_id']) ? $options['poo_id'] : '';

        if (is_null($this->releaseCloudDir) || is_null($this->releaseCloudDirTmp) || is_null($this->displayReleaseDir)) {
            throw new \Exception('Error \'release cloud dir\' in configuration!');
        }

        if (is_null($date)) {
            $date = date('Y-m-d');
        }

        if (!$this->dirCheckOrCreate($this->releaseCloudDir)) {
            throw new \Exception('Can\'t create release folder');
        }
        if (!$this->dirCheckOrCreate($this->releaseCloudDirTmp)) {
            throw new \Exception('Can\'t create tmp folder');
        }


        BackgroundTask::job_plus($job, $job_percent, $job_percent_all, $db_id); //1

        $return = "Exported to {$this->releaseCloudDir}";
        $return .= $this->Pilot_do_igvs($courseId, $theme, $job, $db_id);

        $this->rmdir_recurse($this->releaseCloudDirTmp);

        BackgroundTask::job_plus($job, $job_percent, $job_percent_all, $db_id); //5

        $url = isset($options['remote_server_domain']) ? $options['remote_server_domain'] : '';

        file_put_contents(__DIR__ . "/../../../../storage/app/igvs/export_pilot.log", date('Y-m-d H:i:s') . "\r\nexportToPilot : " . print_r([
                '$db_id' => $db_id,
                'message' => 'files copied. next End Point Request',
            ], true) . "\r\n", FILE_APPEND);


        $use_licence = \Socialite::with($oauth_driver)->endPoint([
            'course_id' => $courseId,
            'pilot_course_id' => $options['pilot_course_id'],
            'parent_course_id' => $options['parent_course_id'],
            'export_dir' => $export_dir,
            'token_credentials' => $tokenCredentials,
            'oauth_user_uid' => $oauth_user_uid,
            'oauth_version' => $oauth_version,
            'poo_id' => $poo_id,
            'url' => $url,
            'file_md5' => $this->file_md5,
            'options' => $options,
        ]);

        file_put_contents(__DIR__ . "/../../../../storage/app/igvs/export_pilot.log", date('Y-m-d H:i:s') . "\r\nexportToPilot : " . print_r([
                '$db_id' => $db_id,
                'message' => 'End Point Request was ended',
            ], true) . "\r\n", FILE_APPEND);

        $job->delete();

        return 'Success!<br>' . $return . "<br>Licence options:" . print_r([
                '$courseId' => $courseId,
                'pilot_course_id' => $options['pilot_course_id'],
                'parent_course_id' => $options['parent_course_id'],
                '$export_dir' => $export_dir,
                'tar' => "{$this->releaseCloudDirTmp}/../{$this->export_dir}.tar",
                'file_md5' => $this->file_md5,
            ], true) . "<br>Licence response:" . print_r($use_licence, true);
    }

    public function exportCourseIGVS($courseId, $theme = '_default', $date = null, $job, $db_id, $options)
    {
//        if (is_null($date)) {
        $date = date('Y-m-d_H-i-s');
//        }

        $course = CourseItem::find($courseId);

        if (!$course)
            return '<br>Error! Course not found. Course id: ' . $courseId . '<br>';

        $course_code = $course->getCourseCodeClean();

        $export_dir = $course_code . '/' . $date;
        if (isset($options['postfix'])) {
            $export_dir .= $options['postfix'];
        }

        $to_maker = isset($options['to_maker']) ? $options['to_maker'] : false;
        $to_zip_local = isset($options['to_zip_local']) ? $options['to_zip_local'] : false;
        $to_mobile = isset($options['to_mobile']) ? $options['to_mobile'] : false;

        $this->releaseCloudDirTmp = Config::get('igvs.courses::prepareReleaseCloud.tmp') . '/' . time()  . '_' . time() . '_' . rand(0,100);
        $this->displayReleaseDir = Config::get('igvs.courses::prepareReleaseCloud.displayReleaseDir');

        $this->releaseCloudDir = Config::get('igvs.courses::prepareReleaseCloud.releaseDir') . '/' . $export_dir;

        if ($to_maker == 'true'
            || $to_zip_local == 'true') {
            $this->releaseCloudDir = Config::get('igvs.courses::maker.releaseDir') . '/' . $date . '_' . $course_code . '_' . $courseId;
            $this->displayReleaseDir = Config::get('igvs.courses::maker.displayReleaseDir');
        }

        $this->export_dir = time() . '_' . $courseId . '_' . rand(0,100);

        $job_percent = 0;
        $job_percent_all = 3;


        if (is_null($this->releaseCloudDir) || is_null($this->releaseCloudDirTmp) || is_null($this->displayReleaseDir)) {
            throw new \Exception('Error \'release cloud dir\' in configuration!');
        }

        if (!$this->dirCheckOrCreate($this->releaseCloudDir)) {
            throw new \Exception('Can\'t create release folder');
        }
        if (!$this->dirCheckOrCreate($this->releaseCloudDirTmp)) {
            throw new \Exception('Can\'t create tmp folder');
        }


        BackgroundTask::job_plus($job, $job_percent, $job_percent_all, $db_id); //1

        $return = "Exported to {$this->releaseCloudDir}";

        $return .= $this->Pilot_do_igvs($courseId, $theme, $job, $db_id, $options);

        $this->rmdir_recurse($this->releaseCloudDirTmp);

        BackgroundTask::job_plus($job, $job_percent, $job_percent_all, $db_id); //5

//        $options = BackgroundTask::first($db_id);

        return '<br>Export Pilot: ' . $this->releaseCloudDir . '<br><br>Log:<br>' . $return;
    }

    private function Pilot_do_igvs($courseId, $theme = '_default', $job, $db_id, $options = []) {

        $to_maker = isset($options['to_maker']) ? $options['to_maker'] : false;
        $to_zip_local = isset($options['to_zip_local']) ? $options['to_zip_local'] : false;
        $to_mobile = isset($options['to_mobile']) ? $options['to_mobile'] : false;

        $releaseCloudDir = $this->releaseCloudDir;
        $displayReleaseDir = $this->displayReleaseDir;

        if (!is_dir($releaseCloudDir))
            mkdir($releaseCloudDir, 0777, true);

        $return = 'IGVS: <br>';

        $course = CourseItem::find($courseId);

        $theme = isset($options['theme']) ? $options['theme'] : "_default";
        $topic = isset($options['topicId']) ? $options['topicId'] : null;
        $build = isset($options['buildId']) ? $options['buildId'] : null;
        $language = isset($options['language']) ? $options['language'] : $course->language;
        $show_module_start_screen = isset($options['show_module_start_screen']) ? $options['show_module_start_screen'] : $course->show_module_start_screen;
        $export_without_module_content = isset($options['export_without_module_content']) ? $options['export_without_module_content'] : 'false';
        $create_maker_config = isset($options['create_maker_config']) ? $options['create_maker_config'] : 'false';

        if (!$course) {
            return 'Course ' . $courseId . ' not found.\n';
        }
        $course_pic = $course->logo && file_exists(Config::get('igvs.courses::sitePath') . $course->logo->getPath()) ? $course->logo->getPath() : '';
        $bg_hat = $course->bg_hat && file_exists(Config::get('igvs.courses::sitePath') . $course->bg_hat->getPath()) ? $course->bg_hat->getPath() : '';
        $preview = $course->image && file_exists(Config::get('igvs.courses::sitePath') . $course->image->getPath()) ? $course->image->getPath() : '';
        $cover = $course->cover && file_exists(Config::get('igvs.courses::sitePath') . $course->cover->getPath()) ? $course->cover->getPath() : '';
        $colontitle_logo = $course->colontitle_logo && file_exists(Config::get('igvs.courses::sitePath') . $course->colontitle_logo->getPath()) ? $course->colontitle_logo->getPath() : '';

        preg_match("/(png|jpg|jpeg|gif|svg)/", $course_pic, $m);
        $course_pic_exp =  count($m) ? $m[0] : 'png';

        preg_match("/(png|jpg|jpeg|gif|svg)/", $bg_hat, $m);
        $bg_hat_exp =  count($m) ? $m[0] : 'png';

        preg_match("/(png|jpg|jpeg|gif|svg)/", $preview, $m);
        $preview_exp =  count($m) ? $m[0] : 'png';

        preg_match("/(png|jpg|jpeg|gif|svg)/", $cover, $m);
        $cover_exp =  count($m) ? $m[0] : 'png';

        preg_match("/(png|jpg|jpeg|gif|svg)/", $colontitle_logo, $m);
        $colontitle_logo_exp =  count($m) ? $m[0] : 'png';

        // копируем временный sqlite DB
        $sqlite_temp_path = __DIR__ . "/../../../../storage/app/igvs/db";
        if (is_dir($sqlite_temp_path)) {
            $this->rmdir_recurse($sqlite_temp_path);
        }
        mkdir($sqlite_temp_path);
        copy(Config::get('igvs.courses::html-module.path') . '/shelldb_static_template.db', "{$sqlite_temp_path}/shelldb_static.db");

        $db_name = uniqid();

        Config::set('database.connections.'.$db_name, array(
            'driver' => 'sqlite',
            'database' => "{$sqlite_temp_path}/shelldb_static.db",
        ));


        $modules = ModuleContent::where('course_id', $courseId);
        if (!is_null($topic) && strlen($topic)) {
            $modules = $modules->whereIn('category_id', Category::where('parent_id', $topic)->lists('id'));
        }
        if (!is_null($build) && strlen($build)) {
            $modules = $modules->whereHas('builds', function (Builder $q) use ($build) {
                $q->where('build_id', $build);
            });
        }
        $modules = $modules->orderBy('sort');

        $modules_ids = clone $modules;
        $modules_ids = $modules_ids->lists('id');
        $act_ids = clone $modules;
        $act_ids = $act_ids->lists('category_id');

        $modules = $modules->get();

        $topic_ids = Category::whereIn('id', $act_ids)->lists('parent_id');

        $categories = Category::where('course_id', $courseId)
            ->where(function($q) use ($topic_ids) {
                $q->whereIn('id', $topic_ids)
                    ->orWhere('is_separate', 1);
            })
            ->orderBy('sort')
            ->get();

        $eapp = [];

        foreach ($categories as $cat_key => $cat_val) {
            if (is_null($cat_val->parent_id)) {

                $category_code_translit = Translit::translit($cat_val->code, ['remove_special_symbol' => true]);
                $cat_val_name = $cat_val->name;
                if (strpos($cat_val_name, '|') && !strpos($cat_val_name, '||')) {
                    $cat_val_name = str_replace('|', '||', $cat_val_name);
                }

                $eapp[] = [
                    "type" => $cat_val->is_separate ? 'splitter' : 'separator',
                    "number" => $cat_val->sort,
                    "resource" => "",
                    "id" => $cat_val->id,
                    "original_id" => $cat_val->copy_original_id,
                    "class" => "",
                    "textName" => "",
                    "parent" => '0',
                    "textTitle" => str_replace('||', '. ', $cat_val->name),
                    "section" => "eapp",
                    "preview" => "",
                    "code" => $category_code_translit,
                ];

                $categories_topic = Category::where('parent_id', $cat_val->id)
                    ->whereIn('id', $act_ids)
                    ->orderBy('sort')
                    ->get();

                $param_text_desc = explode('|', $cat_val_name);
                $param_desc = trim(isset($param_text_desc[1]) ? $param_text_desc[0] : '');
                $param_text = trim(isset($param_text_desc[1]) ? $param_text_desc[1] : $param_text_desc[0]);

                $params = [
                    'number' => $cat_val->sort,
                    'text' => $param_text,
                    'parent' => 0,
                    'section' => 'ebook',
                    'type' => 'separator',
                    'resource' => '',
                    'desc' => $param_desc,
                    'class' => '',
                ];
                $this->sqlite_rubricator_insert($db_name, $params);

                foreach ($categories_topic as $cat_top_key => $cat_top_val) {
                    //      add to eapp
                    $act_code_translit = Translit::translit($cat_top_val->code, ['remove_special_symbol' => true]);

                    $eapp[] = [
                        "type" => "group",
                        "number" => $cat_top_val->sort,
                        "resource" => "",
                        "id" => $cat_top_val->id,
                        "original_id" => $cat_top_val->copy_original_id,
                        "class" => "",
                        "textName" => "",
                        "parent" => $cat_val->id,
                        "textTitle" => str_replace('||', '. ', $cat_top_val->name),
                        "section" => "eapp",
                        "preview" => "",
                        "code" => $act_code_translit
                    ];

                    $js_db_items = $this->make_js_db($courseId, $cat_top_val, $modules_ids, $category_code_translit, $show_module_start_screen);
                    $eapp = array_merge($eapp, $js_db_items);
                    // ---- -- -- -- -- //

                    $param_text_desc = explode('||', $cat_top_val->name);
                    $param_desc = trim(isset($param_text_desc[1]) ? $param_text_desc[0] : '');
                    $param_text = trim(isset($param_text_desc[1]) ? $param_text_desc[1] : $param_text_desc[0]);

                    $params = [
                        'number' => $cat_top_val->sort,
                        'text' => $param_text,
                        'parent' => 0,
                        'section' => 'ebook',
                        'type' => 'group',
                        'resource' => '',
                        'desc' => $param_desc,
                        'class' => '',
                    ];
                    $category_id = $this->sqlite_rubricator_insert($db_name, $params);

                    foreach ($js_db_items as $js_db_item) {
                        $param_text_desc = explode('||', $js_db_item['textTitle']);
                        $param_desc = trim(isset($param_text_desc[1]) ? $param_text_desc[0] : '');
                        $param_text = trim(isset($param_text_desc[1]) ? $param_text_desc[1] : $param_text_desc[0]);

                        $class = $js_db_item['class'];
                        if ($js_db_item['template_code'] == 'html-markup' || !strlen($class)) {
                            $class = 'page';
                        }

                        $params = [
                            'number' => $js_db_item['number'],
                            'text' => $param_text,
                            'parent' => $category_id,
                            'section' => 'ebook',
                            'type' => 'item',
                            'resource' => $js_db_item['resource'],
                            'desc' => $param_desc,
                            'class' => $class,
                        ];
                        $this->sqlite_rubricator_insert($db_name, $params);
                    }

                }
            }
        }

        //словарь
        $vocabulary = $this->make_vocabulary($course->id, $releaseCloudDir);

        foreach($vocabulary as $vocabulary_item) {
            $this->sqlite_vocabulary_insert($db_name, $vocabulary_item);
        }

        //for installer head title (in window)
        //clean course title from special symbols and html tags
        //for &nbsp; and <br> do replace to space
        $course_title_clean = str_replace([
            '"',
            '&nbsp;',
            '<br>',
            '  ',
            '—',
        ], [
            '',
            ' ',
            ' ',
            ' ',
            '-',
        ], $course->title);
        $course_title_clean = strip_tags($course_title_clean);
        $course_title_clean = preg_replace("/&#?[a-z0-9]{2,8};/i","", $course_title_clean);
        $course_title_clean = preg_replace('/[^[:word:]\s-]+/u', '', $course_title_clean);

        $colontitle_logo_name = '';
        if ($course->colontitle_logo) {
            $colontitle_logo_name = $course->colontitle_logo->getPath();
            $colontitle_logo_name = substr($colontitle_logo_name, strrpos($colontitle_logo_name, '/'), strlen($colontitle_logo_name));
            $colontitle_logo_name = "../img/{$colontitle_logo_name}";
        }

        $json = file_get_contents(Config::get('igvs.courses::resourcesPath') . '/export-beta/js_db.json');
        $json = str_replace([
            '{{dir}}',
            '{{title}}',
            '{{title_clean}}',
            '{{eapp}}',
            '{{vocabulary}}',
            '{{course_id}}',
            '{{course_code}}',
            '{{bg_hat}}',
            '{{preview}}',
            '{{catalogue_preview}}',
            '{{cover}}',
            '{{working_programm_files}}',
            '{{colontitle_logo}}',
            '{{colontitle_slogan}}',
            '{{theme}}',
            '{{is_inclusive}}',
        ], [
            '',
            str_replace('"', '&#34;', $course->title),
            $course_title_clean,
            json_encode($eapp),
            json_encode($vocabulary),
            $courseId,
            $course->getCourseCodeClean(),
            strlen($bg_hat) ? 'design/bg_hat.' . $bg_hat_exp : '',
            strlen($course_pic) ? 'design/preview.' . $course_pic_exp : '',
            strlen($preview) ? 'design/catalogue_preview.' . $preview_exp : '',
            strlen($cover) ? 'design/cover.' . $cover_exp : '',
            $course->materials ? 'working_programm_files/' . $course->materials->id . '_' . $course->materials->getFilename() : '',
            $colontitle_logo_name,
            (string)$course->colontitle_slogan,
            $course->theme,
            (int)$course->is_inclusive,
        ], $json);

        if (is_dir("{$releaseCloudDir}/db")) {
            $this->rmdir_recurse("{$releaseCloudDir}/db");
        }

        mkdir("{$releaseCloudDir}/db");
        mkdir("{$this->releaseCloudDirTmp}/db");
        file_put_contents("{$releaseCloudDir}/db/js_db.json", $json);
        file_put_contents("{$this->releaseCloudDirTmp}/db//js_db.json", $json);

        if (!($to_mobile == 'true')) {
            copy("{$sqlite_temp_path}/shelldb_static.db", "{$releaseCloudDir}/db/shelldb_static.db");
            copy("{$sqlite_temp_path}/shelldb_static.db", "{$this->releaseCloudDirTmp}/db/shelldb_static.db");
            copy(Config::get('igvs.courses::html-module.path') . '/shelldb_dynamic.db', "{$this->releaseCloudDirTmp}/db/shelldb_dynamic.db");
        }

        // copy course image
        if (!is_dir("{$this->releaseCloudDirTmp}/design"))
            mkdir("{$this->releaseCloudDirTmp}/design");

        if (!is_dir("{$releaseCloudDir}/design"))
            mkdir("{$releaseCloudDir}/design");

        if (strlen($course_pic)) {
            copy(Config::get('igvs.courses::sitePath') . $course_pic, "{$this->releaseCloudDirTmp}/design/preview." . $course_pic_exp);
            copy(Config::get('igvs.courses::sitePath') . $course_pic, "{$releaseCloudDir}/design/preview." . $course_pic_exp);
        }
        if (strlen($bg_hat)) {
            copy(Config::get('igvs.courses::sitePath') . $bg_hat, "{$this->releaseCloudDirTmp}/design/bg_hat." . $bg_hat_exp);
            copy(Config::get('igvs.courses::sitePath') . $bg_hat, "{$releaseCloudDir}/design/bg_hat." . $bg_hat_exp);
        }
        if (strlen($preview)) {
            copy(Config::get('igvs.courses::sitePath') . $preview, "{$this->releaseCloudDirTmp}/design/catalogue_preview." . $preview_exp);
            copy(Config::get('igvs.courses::sitePath') . $preview, "{$releaseCloudDir}/design/catalogue_preview." . $preview_exp);
        }
        if (strlen($cover)) {
            copy(Config::get('igvs.courses::sitePath') . $cover, "{$this->releaseCloudDirTmp}/design/catalogue_preview." . $cover_exp);
            copy(Config::get('igvs.courses::sitePath') . $cover, "{$releaseCloudDir}/design/catalogue_preview." . $cover_exp);
        }
        if ($course->colontitle_logo) {
            copy(Config::get('igvs.courses::sitePath') . $colontitle_logo, "{$this->releaseCloudDirTmp}/design/colontitle_logo." . $colontitle_logo_exp);
            copy(Config::get('igvs.courses::sitePath') . $colontitle_logo, "{$releaseCloudDir}/design/colontitle_logo." . $colontitle_logo_exp);
        }

        // copy working_programm_files
        mkdir("{$this->releaseCloudDirTmp}/working_programm_files");
        if ($course->materials) {
            $file_path = $course->materials->getLocalPath();
            copy($file_path, "{$this->releaseCloudDirTmp}/working_programm_files/" . $course->materials->id . '_' . $course->materials->getFilename());
        };

        BackgroundTask::job_plus($job, 1, 2, $db_id); //4

        if ($export_without_module_content != 'true') {
            $igvs_paths = $this->prepareForFtpBeta($courseId, $topic, $theme, $language, $show_module_start_screen, null, true, $build, false, $options);
        } else {
            $igvs_paths = [];
        }
        $igvs_path_to_copy = [];



        if (count($igvs_paths)) {
            foreach ($igvs_paths as $path) {
                $topic_dir = $path['topic_dir'];

                $dest_files_path = "{$this->releaseCloudDirTmp}/{$topic_dir}/";

                if (is_dir($dest_files_path)) {
                    $this->rmdir_recurse($dest_files_path);
                }

                if (!isset($igvs_path_to_copy[$dest_files_path])) {
                    $igvs_path_to_copy[$dest_files_path] = $path;
                }
            }
        }

        $log_filename = __DIR__ . '/../resources/bash_copylog.info';

        $return .=  "Topic's codes:<br>";

        if (count($igvs_path_to_copy)) {
            foreach ($igvs_path_to_copy as $path) {
                $course_dir = $path['course_dir'];
                $topic_dir = $path['topic_dir'];

                $dest_files = "/{$topic_dir}";
                $dest_files_path = "{$this->releaseCloudDirTmp}{$dest_files}";

                $file = fopen($log_filename, 'a+');
                fwrite($file, "{$path['module_path']} to {$dest_files_path}\r\n");
                fclose($file);

////            $bash = exec("cp -R \"{$path['module_path']}\" \"{$dest_files_path}\" ");
//                $bash = exec("nice -n 19 ionice -c2 -n7 cp -R \"{$path['module_path']}\" \"{$dest_files_path}\" ");
                $this->recourceCopy("{$path['module_path']}", "{$dest_files_path}");

                $bash = "\r" . exec("nice -n 19 ionice -c2 -n7 rm -R \"{$path['module_path']}\"");

                $file = fopen($log_filename, 'a+');
                fwrite($file, "{$bash}\r\n");
                fwrite($file, "=================================\r\n");
                fclose($file);


                $return .=  $dest_files . '<br>';
            }
        }

        file_put_contents("{$this->releaseCloudDirTmp}/config.json", json_encode([
            'preview' => strlen($course_pic) ? 'design/preview.' . $course_pic_exp : '',
            'title' => $course->title,
            'title_clean' => $course_title_clean,
        ]));

        if ($to_maker == 'true' || $to_zip_local == 'true') {
            copy(Config::get('igvs.courses::html-module.path') . '/MakerConfig.im9', "{$this->releaseCloudDirTmp}/MakerConfig.im9");
            $maker_config_file_link = fopen("{$this->releaseCloudDirTmp}/MakerConfig-temp.im9", "a+");
            $this->create_maker_config($maker_config_file_link, $this->releaseCloudDirTmp, $course->id);
            fclose($maker_config_file_link);

            file_put_contents(
                "{$this->releaseCloudDirTmp}/MakerConfig.im9",
                chr(239) . chr(187) . chr(191) . mb_convert_encoding(
                    str_replace(
                        [
                            '{{__STRUCTURE__}}',
                            "*courseID*",
                            "*courseCode*",
                            "*CourseName*",
                            "{{GUID_PACK}}",
                            "{{GUID_PACK_RAND}}",
                        ], [
                            file_get_contents("{$this->releaseCloudDirTmp}/MakerConfig-temp.im9"),
                            "$course->id",
                            "\"$course->code\"",
                            $course_title_clean,
                            time(),
                            rand(1000, 9999),
                        ],
                        file_get_contents("{$this->releaseCloudDirTmp}/MakerConfig.im9")), 'utf-8'));

            unlink("{$this->releaseCloudDirTmp}/MakerConfig-temp.im9");
        }

        if ($export_without_module_content != 'true') {
            $archiv_ext = 'tar';
            $archiv_command = "tar -cf";

            if ($to_maker == 'true'
                || $to_zip_local == 'true'
                || $to_mobile == 'true') {

                $archiv_ext = 'zip';
                $archiv_command = "zip -2 -r";
            }

            if ($to_maker == 'true'
                || $to_zip_local == 'true') {
//                $create_maker_link_file =
                $release_cloud_dir_with_postfix = str_replace(Config::get('igvs.courses::maker.releaseDir'), Config::get('igvs.courses::maker.releaseDir') . '.src', $releaseCloudDir);

                if (!is_dir($this->releaseCloudDirTmp . '_shell')) {
                    mkdir($this->releaseCloudDirTmp . '_shell', 0777, true);
                }

                if ($to_maker == 'true'
                    || $to_zip_local == 'true') {
                    $this->addLocalShell();
                }

                if (!is_dir($this->releaseCloudDirTmp . '_shell/html/content/' . $course->id)) {
                    mkdir($this->releaseCloudDirTmp . '_shell/html/content/' . $course->id, 0777, true);
                }

                $command = "cd {$this->releaseCloudDirTmp} && nice -n 19 ionice -c2 -n7 mv * {$this->releaseCloudDirTmp}_shell/html/content/{$course->id}";
                $this->bashLog($command);
                $output = [];
                exec($command, $output);
                $this->bashLog(implode('|', $output));

                if ($to_maker == 'true' && !is_dir($release_cloud_dir_with_postfix)) {
                    mkdir($release_cloud_dir_with_postfix, 0777, true);
                }

                if ($to_maker == 'true')
                {
                    $command = "nice -n 19 ionice -c2 -n7 cp {$this->releaseCloudDirTmp}_shell/html/content/{$course->id}/MakerConfig.im9 {$releaseCloudDir}/db/MakerConfig.im9 && mv {$this->releaseCloudDirTmp}_shell/html/content/{$course->id}/MakerConfig.im9 {$release_cloud_dir_with_postfix}/MakerConfig.im9";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));
                } else {
                    $command = "nice -n 19 ionice -c2 -n7 mv {$this->releaseCloudDirTmp}_shell/html/content/{$course->id}/MakerConfig.im9 {$releaseCloudDir}/db/MakerConfig.im9";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));
                }

                $command = "cd {$this->releaseCloudDirTmp}_shell && nice -n 19 ionice -c2 -n7 {$archiv_command} {$this->releaseCloudDirTmp}/../{$this->export_dir}.{$archiv_ext} * ";
                $this->bashLog($command);
                exec($command);;

                $this->file_md5 = md5_file("{$this->releaseCloudDirTmp}/../{$this->export_dir}.{$archiv_ext}");

                if ($to_zip_local == 'true' || $to_mobile == 'true') {

                    $command = "nice -n 19 ionice -c2 -n7 cp {$this->releaseCloudDirTmp}/../{$this->export_dir}.{$archiv_ext} {$releaseCloudDir}/{$this->export_dir}";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));

                    $command = "mv {$releaseCloudDir}/{$this->export_dir} {$releaseCloudDir}/{$this->export_dir}.{$archiv_ext}";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));

                } elseif ($to_maker == 'true') {

                    $command = "nice -n 19 ionice -c2 -n7 cp {$this->releaseCloudDirTmp}/../{$this->export_dir}.{$archiv_ext} {$release_cloud_dir_with_postfix}/{$this->export_dir}";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));

                    $command = "mv {$release_cloud_dir_with_postfix}/{$this->export_dir} {$release_cloud_dir_with_postfix}/{$this->export_dir}.{$archiv_ext}";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));

                }

                if ($to_maker == 'true' && $to_zip_local == 'true') {
                    file_put_contents("{$release_cloud_dir_with_postfix}/" . trim(strrchr($releaseCloudDir, '/'), '/') . '.toexe', '');
                }

                if ($to_maker == 'true'
                    && $to_mobile != 'true'
                    && $to_zip_local != 'true') {
                    $command = "nice -n 19 ionice -c2 -n7 rm -rf {$releaseCloudDir}";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));
                }

            } else {

                if ($to_mobile == 'true') {
                    $this->addMobileShell($course);
                }

                $command = "cd {$this->releaseCloudDirTmp} && nice -n 19 ionice -c2 -n7 {$archiv_command} {$this->releaseCloudDirTmp}/../{$this->export_dir}.{$archiv_ext} * ";
                $this->bashLog($command);
                $output = [];
                exec($command, $output);
                $this->bashLog(implode('|', $output));



                if ($to_mobile) {
                    $zip_md5 = md5_file("{$this->releaseCloudDirTmp}/../{$this->export_dir}.{$archiv_ext}");

                    $command = "echo -n $zip_md5 > {$this->releaseCloudDirTmp}/../{$this->export_dir}.md5";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));

                    $command = "cd {$this->releaseCloudDirTmp} && nice -n 19 ionice -c2 -n7 tar -cf {$this->releaseCloudDirTmp}/../{$this->export_dir}.tar * {$this->releaseCloudDirTmp}/../{$this->export_dir}.{$archiv_ext} {$this->releaseCloudDirTmp}/../{$this->export_dir}.md5";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));

                    $command = "nice -n 19 ionice -c2 -n7 rm {$this->releaseCloudDirTmp}/../{$this->export_dir}.md5";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));

                    $this->file_md5 = md5_file("{$this->releaseCloudDirTmp}/../{$this->export_dir}.tar");

                    $command = "nice -n 19 ionice -c2 -n7 cp {$this->releaseCloudDirTmp}/../{$this->export_dir}.tar {$releaseCloudDir}/{$this->export_dir} && mv {$releaseCloudDir}/{$this->export_dir} {$releaseCloudDir}/{$this->export_dir}.tar";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));

                    $command = "nice -n 19 ionice -c2 -n7 rm {$this->releaseCloudDirTmp}/../{$this->export_dir}.tar";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));
                } else {
                    $this->file_md5 = md5_file("{$this->releaseCloudDirTmp}/../{$this->export_dir}.{$archiv_ext}");

                    $command = "nice -n 19 ionice -c2 -n7 cp {$this->releaseCloudDirTmp}/../{$this->export_dir}.{$archiv_ext} {$releaseCloudDir}/{$this->export_dir} && mv {$releaseCloudDir}/{$this->export_dir} {$releaseCloudDir}/{$this->export_dir}.{$archiv_ext}";
                    $this->bashLog($command);
                    $output = [];
                    exec($command, $output);
                    $this->bashLog(implode('|', $output));
                }


                $command = "touch {$releaseCloudDir}/{$this->file_md5}.md5";
                $this->bashLog($command);
                $output = [];
                exec($command, $output);
                $this->bashLog(implode('|', $output));
            }

            $command = "nice -n 19 ionice -c2 -n7 rm {$this->releaseCloudDirTmp}/../{$this->export_dir}.{$archiv_ext}";
            $this->bashLog($command);
            $output = [];
            exec($command, $output);
            $this->bashLog(implode('|', $output));

            $command = "nice -n 19 ionice -c2 -n7 rm -rf {$this->releaseCloudDirTmp}_shell";
            $this->bashLog($command);
            $output = [];
            exec($command, $output);
            $this->bashLog(implode('|', $output));

            $command = "nice -n 19 ionice -c2 -n7 rm -rf {$this->releaseCloudDirTmp}";
            $this->bashLog($command);
            $output = [];
            exec($command, $output);
            $this->bashLog(implode('|', $output));
        }

        BackgroundTask::job_plus($job, 2, 2, $db_id);

        return $return;
    }

    function addLocalShell()
    {
        $command = "nice -n 19 ionice -c2 -n7 cp " . Config::get('igvs.courses::resourcesPath') . '/shell_local.zip' . " {$this->releaseCloudDirTmp}_shell/shell_local.zip";
        $this->bashLog($command);
        exec($command);

        $command = "nice -n 19 ionice -c2 -n7 unzip {$this->releaseCloudDirTmp}_shell/shell_local.zip -d {$this->releaseCloudDirTmp}_shell";
        $this->bashLog($command);
        exec($command);

        $command = "nice -n 19 ionice -c2 -n7 mv {$this->releaseCloudDirTmp}_shell/shell/* {$this->releaseCloudDirTmp}_shell";
        $this->bashLog($command);
        $output = [];
        exec($command, $output);
        $this->bashLog(implode('|', $output));

        $command = "nice -n 19 ionice -c2 -n7 rm {$this->releaseCloudDirTmp}_shell/shell_local.zip";
        $this->bashLog($command);
        $output = [];
        exec($command, $output);
        $this->bashLog(implode('|', $output));

        $command = "nice -n 19 ionice -c2 -n7 rm -rf {$this->releaseCloudDirTmp}_shell/shell";
        $this->bashLog($command);
        $output = [];
        exec($command, $output);
        $this->bashLog(implode('|', $output));
    }

    function addMobileShell($course)
    {
        $command = "nice -n 19 ionice -c2 -n7 cp -R " . \Config::get('igvs.courses::modulesRelease.path') . '/shell-mobile' . " {$this->releaseCloudDirTmp}/shell-mobile";
        $this->bashLog($command);

        $output = [];
        exec($command, $output);

        $this->bashLog(implode('|', $output));
    }

    function bashLog($text)
    {
        $log_filename = __DIR__ . '/../resources/bash_copylog.info';

        $file = fopen($log_filename, 'a+');
        fwrite($file, "{$text}\r\n");
        fwrite($file, "=================================\r\n");
        fclose($file);
    }

    function create_maker_config($maker_config_file_link, $path, $course_id, $part_path_orig = "", $tab = 8)
    {
        if (!is_dir($path))
            return false;

        $path = rtrim($path, '/').'/';
        $handle = opendir($path);

        while(false !== ($file = readdir($handle))) {
            if(in_array($file, [
                '.',
                '..',
                'MakerConfig.im9',
                'MakerConfig-temp.im9',
                'shelldb_static.db',
                'shelldb_dynamic.db',
            ]))
                continue;

            $part_path = $part_path_orig;

            $fullpath = $path.$file;
            $hash_code = md5($path.$file);
            $hash_code = is_numeric(substr($hash_code, 0, 1)) ? '_' . substr($hash_code, 1) : $hash_code;
            $hash_code = str_replace(' ', '', $hash_code);
            if(is_dir($fullpath)) {
                $part_path = $part_path_orig . $file . '\\\\';
                $str = "{tab}folder {hash}: Flags=0x0, Component=Product, Traits=0x80000000, InstallAction=0, RemoveAction=1 {\r\n{tab}\tstr Name: Default=\"{$file}\";\r\n";
            } else {
                $str = "{tab}file  {hash}: Flags=0x0, Component=Product, Path=\".\\\\html\\\\content\\\\{$course_id}\\\\{$part_path}{$file}\", Attributes=0x0, InstallAction=4, RemoveAction=2, Registration=0, RegOrder=0, Packaging=0 {\r\n{tab}\tstr Name: Default=\"{$file}\";\r\n";
            }

            $str = str_replace([
                    "{tab}",
                    "{hash}",
//                    "(",
//                    ")",
                ], [
                    str_repeat("\t", $tab),
                    $hash_code,
//                    "",
//                    "",
                ], $str);

            fwrite($maker_config_file_link, $str);

            if(is_dir($fullpath)) {
                $this->create_maker_config($maker_config_file_link, $fullpath, $course_id, $part_path, $tab + 1);
            }

            fwrite($maker_config_file_link, str_repeat("\t", $tab) . "}\r\n");

        }
        closedir($handle);

    }

    public function maker_config_recurse($path) {

    }

    function getTopicIds($course_id = null, $build_id = null, $topic_id = null) {
        // if set topic_id - return it
        if (!is_null($topic_id) && strlen($topic_id))
            return (int)$topic_id;

        // get build modules
        $whereCallback = function (Builder $q) use ($build_id) {
            if (!is_null($build_id) && strlen($build_id))
                $q->where('build_id', $build_id);
        };

        $category_ids = new ModuleContent();

        if (!is_null($build_id) && strlen($build_id)) {
            $category_ids = $category_ids->whereHas('builds', $whereCallback);
        }

        if (!is_null($course_id) && strlen($course_id)) {
            $category_ids = $category_ids->where('course_id', $course_id);
        }

        $category_ids = $category_ids->lists('category_id');

        // get topic
        $topic_ids = Category::select('parent_id')
            ->distinct()
            ->whereIn('id', $category_ids)
            ->lists('parent_id');

        return $topic_ids;
    }

    function lockTopic($ids) {
        if (!is_array($ids) || !count($ids))
            return false;

        Category::whereIn('id', $ids)
            ->update(['readonly' => true]);

        return true;
    }

    function transferUserToReview($course_id) {
        $role_review_id = Role::where('name', 'Review')->first();
        $role_review_id = $role_review_id ? $role_review_id->id : 0;

        $member_list = Member::where('course_id', $course_id)->get();

        foreach ($member_list as $member) {
            $member->roles()->detach();
            $member->roles()->attach($role_review_id);
        }
    }
}
