<?php namespace Igvs\Courses\Traits;

use Config;
use Igvs\Courses\Components\Course;
use Igvs\Courses\Models\Category;
use Igvs\Courses\Models\Course as CourseItem;
use Illuminate\Database\Eloquent\Builder;
use Igvs\Courses\Models\ModuleContent;
use Igvs\Courses\Models\Module;
use BackendAuth;

use Igvs\Courses\Traits\ExportToPilot;
use Igvs\Courses\Models\BackgroundTask;

trait QueueCommand
{
    public function pushTask($name, $act, $options, $user_id)
    {
        $method_name = strtoupper(substr($act, 0, 1)) . substr($act, 1);
        $short_method_name = 'short' . $method_name;

        if (!method_exists($this, $short_method_name)) {
            return "pushTask: Not found method '" . $short_method_name . "'\n options:\n" . json_encode($options) . "\n";
        }

        $user = BackendAuth::getUser();

        $background_task =  new BackgroundTask();
        $background_task->name = $name;
        $background_task->act = $act;
        $background_task->options = json_encode($options);
        $background_task->job_id = 0;
        $background_task->progress = 0;
        $background_task->progress_full = 0;
        $background_task->responsible_id = intval($user ? $user->id : 0);
        $background_task->save();

        $data = serialize([
            'act'       => $short_method_name,
            'options'   => $options,
            'db_id'     => $background_task->id
        ]);

        $queue_name = 'ExportToCloud';

        if (isset($options['queue_name'])) {
            $queue_name = $options['queue_name'];
        }

        \Queue::push('Igvs\Courses\Classes\ExportWorker', $data, $queue_name);

        return $background_task->id;
    }
}