#!/bin/bash

TEMP_RELEASE_PATH=/var/www/igvs-cms-prod/storage/app/igvs/release-cloud-tmp/*
TEMP_BETA_PATH=/var/www/igvs-cms-prod/storage/app/igvs/export-beta/*
TEMP_PILOT_PATH=/var/www/igvs-cms-prod/storage/app/igvs/export-pilot-tmp/*
SCORM_PATH=/var/www/igvs-cms-prod/storage/app/igvs/scorm/arhive/*
SCORM_TMP_PATH=/var/www/igvs-cms-prod/storage/app/igvs/scorm/tmp/*

#temp release
find $TEMP_RELEASE_PATH -type f -mmin +59 -delete
find $TEMP_RELEASE_PATH -type d -empty -mmin +59 -delete

#temp beta
find $TEMP_BETA_PATH -type f -mmin +59 -delete
find $TEMP_BETA_PATH -type d -empty -mmin +59 -delete

#temp pilot
find $TEMP_PILOT_PATH -type f -mmin +59 -delete
find $TEMP_PILOT_PATH -type d -empty -mmin +59 -delete

#scorm
find $SCORM_PATH -type f -mmin +15 -delete
find $SCORM_PATH -type d -empty -mmin +15 -delete

#temp scorm
find $SCORM_TMP_PATH -type f -mmin +15 -delete
find $SCORM_TMP_PATH -type d -empty -mmin +15 -delete
